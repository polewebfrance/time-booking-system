function getArret(idArret, getArret, getFilteredArrets2){
	$('input:radio').each(function(){
		if($(this).attr('name') != idArret){
			$(this).prop('checked', false);
		}else{
			$(this).prop('checked', true);
		}
	});
	$.ajax({
		dataType: "html",
		type: "POST",
		url: url_get_arret,
		data: { idArretDeTravail : parseInt(idArret)},  
		async:false,
		cache: false,
		error: function(jqXHR,textStatus, errorThrown ) {
		},
		success: function(arret){
			$('#detailSociete').html(arret);
		},
		complete: function(){
			$("#loader").hide();
		}                                                                               
	});
}
function getFilteredArrets(idArret, getArret, getFilteredArrets){
	$('#filteredArrets').html('');
	var nomSalarie = $('#nomSalarie').val();
	var prenomSalarie = $('#prenomSalarie').val();
	var reference = $('#codeSociete').val();
	var libelle = $('#libelleSociete').val();
	var gestPaie = $('#gestPaie').val();
	var gestRH = $('#gestRH').val();	
	var isTraite = $('#isTraite').val();
	var tranche = $('#tranche').val();
	$("#loader").show();
	$.ajax({
		dataType: "text",
		type: "POST",
		url: url_arrets_filtered,
		data: { idArret : idArret, nomSalarie : nomSalarie, prenomSalarie : prenomSalarie, reference : reference, libelle : libelle, gestPaie : gestPaie, gestRH : gestRH, isTraite : isTraite, tranche : tranche },  
		async: false,
		cache: false,
		error: function(jqXHR,textStatus, errorThrown ) {
		},
		success: function(data){
			$('#filteredArrets').html(data);
		},
		complete: function(){
			$("#loader").hide();
		}                                                                            
	});
	return false;
}
function getFilteredArrets2(page, idArret, getArret, getFilteredArrets2){
		var nomSalarie = $('#nomSalarie').val();
		var prenomSalarie = $('#prenomSalarie').val();
		var reference = $('#codeSociete').val();
		var libelle = $('#libelleSociete').val();
		var gestPaie = $('#gestPaie').val();
		var gestRH = $('#gestRH').val();	
		var isTraite = $('#isTraite').val();
		var tranche = $('#tranche').val();
		page = (typeof page !== 'undefined' && page !== '0') ? page : '1';
		var paginationURL = url_arrets_filtered + '?';
		paginationURL += 'page=' + page;
		paginationURL += '&nomSalarie=' + nomSalarie;
		paginationURL += '&prenomSalarie=' + prenomSalarie;
		paginationURL += '&reference=' + reference;
		paginationURL += '&libelle=' + libelle;
		paginationURL += '&gestPaie=' + gestPaie;
		paginationURL += '&gestRH=' + gestRH;
		paginationURL += '&isTraite=' + isTraite;
		paginationURL += '&tranche=' + tranche;
		$("#loader").show();
		$.ajax({
			type: "GET",
			url: paginationURL
			})
			.done(function( msg ) {
				$('#filteredArrets').html(msg);
				$("#loader").hide();
			});
}
$(function(){
	$("#ajouterArret").on("hide.bs.modal", function(){
		$("#ajouterArretModalBody").html("");
	});
	if(selected > 0){
		getArret(selected);
		getFilteredArrets2('0');
	}else{
		$("#loader").show();
		getFilteredArrets2('0');
	}
	$('#nomSalarie, #prenomSalarie, #codeSociete, #libelleSociete, #gestPaie, #gestRH, #tranche, #isTraite').change(function(){
		$('#detailArret').html('');
		getFilteredArrets2('0');
	});
});