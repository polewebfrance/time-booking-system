/*
 * Metadata - jQuery plugin for parsing metadata from elements
 *
 * Copyright (c) 2006 John Resig, Yehuda Katz, Jörn Zaefferer, Paul McLanahan
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 *
 */

/**
 * Sets the type of metadata to use. Metadata is encoded in JSON, and each property
 * in the JSON will become a property of the element itself.
 *
 * There are three supported types of metadata storage:
 *
 *   attr:  Inside an attribute. The name parameter indicates *which* attribute.
 *
 *   class: Inside the class attribute, wrapped in curly braces: { }
 *
 *   elem:  Inside a child element (e.g. a script tag). The
 *          name parameter indicates *which* element.
 *
 * The metadata for an element is loaded the first time the element is accessed via jQuery.
 *
 * As a result, you can define the metadata type, use $(expr) to load the metadata into the elements
 * matched by expr, then redefine the metadata type and run another $(expr) for other elements.
 *
 * @name $.metadata.setType
 *
 * @example <p id="one" class="some_class {item_id: 1, item_label: 'Label'}">This is a p</p>
 * @before $.metadata.setType("class")
 * @after $("#one").metadata().item_id == 1; $("#one").metadata().item_label == "Label"
 * @desc Reads metadata from the class attribute
 *
 * @example <p id="one" class="some_class" data="{item_id: 1, item_label: 'Label'}">This is a p</p>
 * @before $.metadata.setType("attr", "data")
 * @after $("#one").metadata().item_id == 1; $("#one").metadata().item_label == "Label"
 * @desc Reads metadata from a "data" attribute
 *
 * @example <p id="one" class="some_class"><script>{item_id: 1, item_label: 'Label'}</script>This is a p</p>
 * @before $.metadata.setType("elem", "script")
 * @after $("#one").metadata().item_id == 1; $("#one").metadata().item_label == "Label"
 * @desc Reads metadata from a nested script element
 *
 * @param String type The encoding type
 * @param String name The name of the attribute to be used to get metadata (optional)
 * @cat Plugins/Metadata
 * @descr Sets the type of encoding to be used when loading metadata for the first time
 * @type undefined
 * @see metadata()
 */

(function($) {

$.extend({
	metadata : {
		defaults : {
			type: 'class',
			name: 'metadata',
			cre: /(\{.*\})/,
			single: 'metadata'
		},
		setType: function( type, name ){
			this.defaults.type = type;
			this.defaults.name = name;
		},
		get: function( elem, opts ){
			var data, m, e, attr,
				settings = $.extend({},this.defaults,opts);
			// check for empty string in single property
			if ( !settings.single.length ) { settings.single = 'metadata'; }

			data = $.data(elem, settings.single);
			// returned cached data if it already exists
			if ( data ) { return data; }

			data = "{}";

			if ( settings.type === "class" ) {
				m = settings.cre.exec( elem.className );
				if ( m ) { data = m[1]; }
			} else if ( settings.type === "elem" ) {
				if( !elem.getElementsByTagName ) { return undefined; }
				e = elem.getElementsByTagName(settings.name);
				if ( e.length ) { data = $.trim(e[0].innerHTML); }
			} else if ( elem.getAttribute !== undefined ) {
				attr = elem.getAttribute( settings.name );
				if ( attr ) { data = attr; }
			}

			if ( data.indexOf( '{' ) <0 ) { data = "{" + data + "}"; }

			data = eval("(" + data + ")");

			$.data( elem, settings.single, data );
			return data;
		}
	}
});

/**
 * Returns the metadata object for the first member of the jQuery object.
 *
 * @name metadata
 * @descr Returns element's metadata object
 * @param Object opts An object contianing settings to override the defaults
 * @type jQuery
 * @cat Plugins/Metadata
 */
$.fn.metadata = function( opts ){
	return $.metadata.get( this[0], opts );
};

})(jQuery);
/*** This file is dynamically generated ***
█████▄ ▄████▄   █████▄ ▄████▄ ██████   ███████▄ ▄████▄ █████▄ ██ ██████ ██  ██
██  ██ ██  ██   ██  ██ ██  ██   ██     ██ ██ ██ ██  ██ ██  ██ ██ ██     ██  ██
██  ██ ██  ██   ██  ██ ██  ██   ██     ██ ██ ██ ██  ██ ██  ██ ██ ██▀▀   ▀▀▀▀██
█████▀ ▀████▀   ██  ██ ▀████▀   ██     ██ ██ ██ ▀████▀ █████▀ ██ ██     █████▀
*/
/*! tablesorter (FORK) - updated 05-18-2015 (v2.22.1)*/
/* Includes widgets ( storage,uitheme,columns,filter,stickyHeaders,resizable,saveSort ) */
(function(factory) {
	if (typeof define === 'function' && define.amd) {
		define(['jquery'], factory);
	} else if (typeof module === 'object' && typeof module.exports === 'object') {
		module.exports = factory(require('jquery'));
	} else {
		factory(jQuery);
	}
}(function($) {

/*! TableSorter (FORK) v2.22.1 *//*
* Client-side table sorting with ease!
* @requires jQuery v1.2.6+
*
* Copyright (c) 2007 Christian Bach
* fork maintained by Rob Garrison
*
* Examples and docs at: http://tablesorter.com
* Dual licensed under the MIT and GPL licenses:
* http://www.opensource.org/licenses/mit-license.php
* http://www.gnu.org/licenses/gpl.html
*
* @type jQuery
* @name tablesorter (FORK)
* @cat Plugins/Tablesorter
* @author Christian Bach - christian.bach@polyester.se
* @contributor Rob Garrison - https://github.com/Mottie/tablesorter
*/
/*jshint browser:true, jquery:true, unused:false, expr: true */
/*global console:false, alert:false, require:false, define:false, module:false */
;(function($){
	'use strict';
	$.extend({
		/*jshint supernew:true */
		tablesorter: new function() {

			var ts = this;

			ts.version = '2.22.1';

			ts.parsers = [];
			ts.widgets = [];
			ts.defaults = {

				// *** appearance
				theme            : 'default',  // adds tablesorter-{theme} to the table for styling
				widthFixed       : false,      // adds colgroup to fix widths of columns
				showProcessing   : false,      // show an indeterminate timer icon in the header when the table is sorted or filtered.

				headerTemplate   : '{content}',// header layout template (HTML ok); {content} = innerHTML, {icon} = <i/> (class from cssIcon)
				onRenderTemplate : null,       // function(index, template){ return template; }, (template is a string)
				onRenderHeader   : null,       // function(index){}, (nothing to return)

				// *** functionality
				cancelSelection  : true,       // prevent text selection in the header
				tabIndex         : true,       // add tabindex to header for keyboard accessibility
				dateFormat       : 'mmddyyyy', // other options: 'ddmmyyy' or 'yyyymmdd'
				sortMultiSortKey : 'shiftKey', // key used to select additional columns
				sortResetKey     : 'ctrlKey',  // key used to remove sorting on a column
				usNumberFormat   : true,       // false for German '1.234.567,89' or French '1 234 567,89'
				delayInit        : false,      // if false, the parsed table contents will not update until the first sort
				serverSideSorting: false,      // if true, server-side sorting should be performed because client-side sorting will be disabled, but the ui and events will still be used.
				resort           : true,       // default setting to trigger a resort after an 'update', 'addRows', 'updateCell', etc has completed

				// *** sort options
				headers          : {},         // set sorter, string, empty, locked order, sortInitialOrder, filter, etc.
				ignoreCase       : true,       // ignore case while sorting
				sortForce        : null,       // column(s) first sorted; always applied
				sortList         : [],         // Initial sort order; applied initially; updated when manually sorted
				sortAppend       : null,       // column(s) sorted last; always applied
				sortStable       : false,      // when sorting two rows with exactly the same content, the original sort order is maintained

				sortInitialOrder : 'asc',      // sort direction on first click
				sortLocaleCompare: false,      // replace equivalent character (accented characters)
				sortReset        : false,      // third click on the header will reset column to default - unsorted
				sortRestart      : false,      // restart sort to 'sortInitialOrder' when clicking on previously unsorted columns

				emptyTo          : 'bottom',   // sort empty cell to bottom, top, none, zero, emptyMax, emptyMin
				stringTo         : 'max',      // sort strings in numerical column as max, min, top, bottom, zero
				textExtraction   : 'basic',    // text extraction method/function - function(node, table, cellIndex){}
				textAttribute    : 'data-text',// data-attribute that contains alternate cell text (used in default textExtraction function)
				textSorter       : null,       // choose overall or specific column sorter function(a, b, direction, table, columnIndex) [alt: ts.sortText]
				numberSorter     : null,       // choose overall numeric sorter function(a, b, direction, maxColumnValue)

				// *** widget options
				widgets: [],                   // method to add widgets, e.g. widgets: ['zebra']
				widgetOptions    : {
					zebra : [ 'even', 'odd' ]    // zebra widget alternating row class names
				},
				initWidgets      : true,       // apply widgets on tablesorter initialization
				widgetClass     : 'widget-{name}', // table class name template to match to include a widget

				// *** callbacks
				initialized      : null,       // function(table){},

				// *** extra css class names
				tableClass       : '',
				cssAsc           : '',
				cssDesc          : '',
				cssNone          : '',
				cssHeader        : '',
				cssHeaderRow     : '',
				cssProcessing    : '', // processing icon applied to header during sort/filter

				cssChildRow      : 'tablesorter-childRow', // class name indiciating that a row is to be attached to the its parent
				cssIcon          : 'tablesorter-icon', // if this class does not exist, the {icon} will not be added from the headerTemplate
				cssIconNone      : '', // class name added to the icon when there is no column sort
				cssIconAsc       : '', // class name added to the icon when the column has an ascending sort
				cssIconDesc      : '', // class name added to the icon when the column has a descending sort
				cssInfoBlock     : 'tablesorter-infoOnly', // don't sort tbody with this class name (only one class name allowed here!)
				cssNoSort        : 'tablesorter-noSort',      // class name added to element inside header; clicking on it won't cause a sort
				cssIgnoreRow     : 'tablesorter-ignoreRow',   // header row to ignore; cells within this row will not be added to c.$headers

				// *** events
				pointerClick     : 'click',
				pointerDown      : 'mousedown',
				pointerUp        : 'mouseup',

				// *** selectors
				selectorHeaders  : '> thead th, > thead td',
				selectorSort     : 'th, td',   // jQuery selector of content within selectorHeaders that is clickable to trigger a sort
				selectorRemove   : '.remove-me',

				// *** advanced
				debug            : false,

				// *** Internal variables
				headerList: [],
				empties: {},
				strings: {},
				parsers: []

				// removed: widgetZebra: { css: ['even', 'odd'] }

			};

			// internal css classes - these will ALWAYS be added to
			// the table and MUST only contain one class name - fixes #381
			ts.css = {
				table      : 'tablesorter',
				cssHasChild: 'tablesorter-hasChildRow',
				childRow   : 'tablesorter-childRow',
				colgroup   : 'tablesorter-colgroup',
				header     : 'tablesorter-header',
				headerRow  : 'tablesorter-headerRow',
				headerIn   : 'tablesorter-header-inner',
				icon       : 'tablesorter-icon',
				processing : 'tablesorter-processing',
				sortAsc    : 'tablesorter-headerAsc',
				sortDesc   : 'tablesorter-headerDesc',
				sortNone   : 'tablesorter-headerUnSorted'
			};

			// labels applied to sortable headers for accessibility (aria) support
			ts.language = {
				sortAsc  : 'Ascending sort applied, ',
				sortDesc : 'Descending sort applied, ',
				sortNone : 'No sort applied, ',
				nextAsc  : 'activate to apply an ascending sort',
				nextDesc : 'activate to apply a descending sort',
				nextNone : 'activate to remove the sort'
			};

			// These methods can be applied on table.config instance
			ts.instanceMethods = {};

			/* debuging utils */
			function log() {
				var a = arguments[0],
					s = arguments.length > 1 ? Array.prototype.slice.call(arguments) : a;
				if (typeof console !== 'undefined' && typeof console.log !== 'undefined') {
					console[ /error/i.test(a) ? 'error' : /warn/i.test(a) ? 'warn' : 'log' ](s);
				} else {
					alert(s);
				}
			}

			function benchmark(s, d) {
				log(s + ' (' + (new Date().getTime() - d.getTime()) + 'ms)');
			}

			ts.log = log;
			ts.benchmark = benchmark;

			// $.isEmptyObject from jQuery v1.4
			function isEmptyObject(obj) {
				/*jshint forin: false */
				for (var name in obj) {
					return false;
				}
				return true;
			}

			ts.getElementText = function(c, node, cellIndex) {
				if (!node) { return ''; }
				var te,
					t = c.textExtraction || '',
					// node could be a jquery object
					// http://jsperf.com/jquery-vs-instanceof-jquery/2
					$node = node.jquery ? node : $(node);
				if (typeof(t) === 'string') {
					// check data-attribute first when set to 'basic'; don't use node.innerText - it's really slow!
					// http://www.kellegous.com/j/2013/02/27/innertext-vs-textcontent/
					return $.trim(
						( t === 'basic' ? $node.attr(c.textAttribute) || node.textContent : node.textContent ) ||
						$node.text()
					);
				} else {
					if (typeof(t) === 'function') {
						return $.trim( t($node[0], c.table, cellIndex) );
					} else if (typeof (te = ts.getColumnData( c.table, t, cellIndex )) === 'function') {
						return $.trim( te($node[0], c.table, cellIndex) );
					}
				}
				// fallback
				return $.trim( $node[0].textContent || $node.text() );
			};

			function detectParserForColumn(table, rows, rowIndex, cellIndex) {
				var cur, $node,
					c = table.config,
					i = ts.parsers.length,
					node = false,
					nodeValue = '',
					keepLooking = true;
				while (nodeValue === '' && keepLooking) {
					rowIndex++;
					if (rows[rowIndex]) {
						node = rows[rowIndex].cells[cellIndex];
						nodeValue = ts.getElementText(c, node, cellIndex);
						$node = $(node);
						if (table.config.debug) {
							log('Checking if value was empty on row ' + rowIndex + ', column: ' + cellIndex + ': "' + nodeValue + '"');
						}
					} else {
						keepLooking = false;
					}
				}
				while (--i >= 0) {
					cur = ts.parsers[i];
					// ignore the default text parser because it will always be true
					if (cur && cur.id !== 'text' && cur.is && cur.is(nodeValue, table, node, $node)) {
						return cur;
					}
				}
				// nothing found, return the generic parser (text)
				return ts.getParserById('text');
			}

			// centralized function to extract/parse cell contents
			function getParsedText( c, cell, colIndex, txt ) {
				if ( typeof txt === 'undefined' ) {
					txt = ts.getElementText( c, cell, colIndex );
				}
				// if no parser, make sure to return the txt
				var val = '' + txt,
					parser = c.parsers[ colIndex ],
					extractor = c.extractors[ colIndex ];
				if ( parser ) {
					// do extract before parsing, if there is one
					if ( extractor && typeof extractor.format === 'function' ) {
						txt = extractor.format( txt, c.table, cell, colIndex );
					}
					// allow parsing if the string is empty, previously parsing would change it to zero,
					// in case the parser needs to extract data from the table cell attributes
					val = parser.id === 'no-parser' ? '' :
						// make sure txt is a string (extractor may have converted it)
						parser.format( '' + txt, c.table, cell, colIndex );
					if ( c.ignoreCase && typeof val === 'string' ) {
					 val = val.toLowerCase();
					}
				}
				return val;
			}

			function buildParserCache(table) {
				var c = table.config,
					// update table bodies in case we start with an empty table
					tb = c.$tbodies = c.$table.children('tbody:not(.' + c.cssInfoBlock + ')'),
					rows, list, l, i, h, ch, np, p, e, time,
					j = 0,
					parsersDebug = '',
					len = tb.length;
				if ( len === 0) {
					return c.debug ? log('Warning: *Empty table!* Not building a parser cache') : '';
				} else if (c.debug) {
					time = new Date();
					log('Detecting parsers for each column');
				}
				list = {
					extractors: [],
					parsers: []
				};
				while (j < len) {
					rows = tb[j].rows;
					if (rows.length) {
						l = c.columns; // rows[j].cells.length;
						for (i = 0; i < l; i++) {
							h = c.$headerIndexed[i];
							// get column indexed table cell
							ch = ts.getColumnData( table, c.headers, i );
							// get column parser/extractor
							e = ts.getParserById( ts.getData(h, ch, 'extractor') );
							p = ts.getParserById( ts.getData(h, ch, 'sorter') );
							np = ts.getData(h, ch, 'parser') === 'false';
							// empty cells behaviour - keeping emptyToBottom for backwards compatibility
							c.empties[i] = ( ts.getData(h, ch, 'empty') || c.emptyTo || (c.emptyToBottom ? 'bottom' : 'top' ) ).toLowerCase();
							// text strings behaviour in numerical sorts
							c.strings[i] = ( ts.getData(h, ch, 'string') || c.stringTo || 'max' ).toLowerCase();
							if (np) {
								p = ts.getParserById('no-parser');
							}
							if (!e) {
								// For now, maybe detect someday
								e = false;
							}
							if (!p) {
								p = detectParserForColumn(table, rows, -1, i);
							}
							if (c.debug) {
								parsersDebug += 'column:' + i + '; extractor:' + e.id + '; parser:' + p.id + '; string:' + c.strings[i] + '; empty: ' + c.empties[i] + '\n';
							}
							list.parsers[i] = p;
							list.extractors[i] = e;
						}
					}
					j += (list.parsers.length) ? len : 1;
				}
				if (c.debug) {
					log(parsersDebug ? parsersDebug : 'No parsers detected');
					benchmark('Completed detecting parsers', time);
				}
				c.parsers = list.parsers;
				c.extractors = list.extractors;
			}

			/* utils */
			function buildCache(table) {
				var cc, t, v, i, j, k, $row, cols, cacheTime,
					totalRows, rowData, prevRowData, colMax,
					c = table.config,
					$tb = c.$tbodies,
					parsers = c.parsers;
				c.cache = {};
				c.totalRows = 0;
				// if no parsers found, return - it's an empty table.
				if (!parsers) {
					return c.debug ? log('Warning: *Empty table!* Not building a cache') : '';
				}
				if (c.debug) {
					cacheTime = new Date();
				}
				// processing icon
				if (c.showProcessing) {
					ts.isProcessing(table, true);
				}
				for (k = 0; k < $tb.length; k++) {
					colMax = []; // column max value per tbody
					cc = c.cache[k] = {
						normalized: [] // array of normalized row data; last entry contains 'rowData' above
						// colMax: #   // added at the end
					};

					totalRows = ($tb[k] && $tb[k].rows.length) || 0;
					for (i = 0; i < totalRows; ++i) {
						rowData = {
							// order: original row order #
							// $row : jQuery Object[]
							child: [], // child row text (filter widget)
							raw: []    // original row text
						};
						/** Add the table data to main data array */
						$row = $( $tb[ k ].rows[ i ] );
						cols = [];
						// if this is a child row, add it to the last row's children and continue to the next row
						// ignore child row class, if it is the first row
						if ( $row.hasClass( c.cssChildRow ) && i !== 0 ) {
							t = cc.normalized.length - 1;
							prevRowData = cc.normalized[ t ][ c.columns ];
							prevRowData.$row = prevRowData.$row.add( $row );
							// add 'hasChild' class name to parent row
							if ( !$row.prev().hasClass( c.cssChildRow ) ) {
								$row.prev().addClass( ts.css.cssHasChild );
							}
							// save child row content (un-parsed!)
							v = $row.children( 'th, td' );
							t = prevRowData.child.length;
							prevRowData.child[ t ] = [];
							// child row content does not account for colspans/rowspans; so indexing may be off
							for ( j = 0; j < c.columns; j++ ) {
								prevRowData.child[ t ][ j ] = getParsedText( c, v[ j ], j );
							}
							// go to the next for loop
							continue;
						}
						rowData.$row = $row;
						rowData.order = i; // add original row position to rowCache
						for ( j = 0; j < c.columns; ++j ) {
							if (typeof parsers[ j ] === 'undefined') {
								if ( c.debug ) {
									log( 'No parser found for cell:', $row[ 0 ].cells[ j ], 'does it have a header?' );
								}
								continue;
							}
							t = ts.getElementText( c, $row[ 0 ].cells[j], j );
							rowData.raw.push( t ); // save original row text
							v = getParsedText( c, $row[ 0 ].cells[ j ], j, t );
							cols.push( v );
							if ( ( parsers[ j ].type || '' ).toLowerCase() === 'numeric' ) {
								// determine column max value (ignore sign)
								colMax[ j ] = Math.max( Math.abs( v ) || 0, colMax[ j ] || 0 );
							}
						}
						// ensure rowData is always in the same location (after the last column)
						cols[ c.columns ] = rowData;
						cc.normalized.push( cols );
					}
					cc.colMax = colMax;
					// total up rows, not including child rows
					c.totalRows += cc.normalized.length;

				}
				if ( c.showProcessing ) {
					ts.isProcessing( table ); // remove processing icon
				}
				if ( c.debug ) {
					benchmark( 'Building cache for ' + totalRows + ' rows', cacheTime );
				}
			}

			// init flag (true) used by pager plugin to prevent widget application
			function appendToTable(table, init) {
				var c = table.config,
					wo = c.widgetOptions,
					$tbodies = c.$tbodies,
					rows = [],
					cc = c.cache,
					n, totalRows, $bk, $tb,
					i, k, appendTime;
				// empty table - fixes #206/#346
				if (isEmptyObject(cc)) {
					// run pager appender in case the table was just emptied
					return c.appender ? c.appender(table, rows) :
						table.isUpdating ? c.$table.trigger('updateComplete', table) : ''; // Fixes #532
				}
				if (c.debug) {
					appendTime = new Date();
				}
				for (k = 0; k < $tbodies.length; k++) {
					$bk = $tbodies.eq(k);
					if ($bk.length) {
						// get tbody
						$tb = ts.processTbody(table, $bk, true);
						n = cc[k].normalized;
						totalRows = n.length;
						for (i = 0; i < totalRows; i++) {
							rows.push(n[i][c.columns].$row);
							// removeRows used by the pager plugin; don't render if using ajax - fixes #411
							if (!c.appender || (c.pager && (!c.pager.removeRows || !wo.pager_removeRows) && !c.pager.ajax)) {
								$tb.append(n[i][c.columns].$row);
							}
						}
						// restore tbody
						ts.processTbody(table, $tb, false);
					}
				}
				if (c.appender) {
					c.appender(table, rows);
				}
				if (c.debug) {
					benchmark('Rebuilt table', appendTime);
				}
				// apply table widgets; but not before ajax completes
				if (!init && !c.appender) { ts.applyWidget(table); }
				if (table.isUpdating) {
					c.$table.trigger('updateComplete', table);
				}
			}

			function formatSortingOrder(v) {
				// look for 'd' in 'desc' order; return true
				return (/^d/i.test(v) || v === 1);
			}

			function buildHeaders(table) {
				var ch, $t, h, i, t, lock, time, indx,
					c = table.config;
				c.headerList = [];
				c.headerContent = [];
				if (c.debug) {
					time = new Date();
				}
				// children tr in tfoot - see issue #196 & #547
				c.columns = ts.computeColumnIndex( c.$table.children('thead, tfoot').children('tr') );
				// add icon if cssIcon option exists
				i = c.cssIcon ? '<i class="' + ( c.cssIcon === ts.css.icon ? ts.css.icon : c.cssIcon + ' ' + ts.css.icon ) + '"></i>' : '';
				// redefine c.$headers here in case of an updateAll that replaces or adds an entire header cell - see #683
				c.$headers = $( $.map( $(table).find(c.selectorHeaders), function(elem, index) {
					$t = $(elem);
					// ignore cell (don't add it to c.$headers) if row has ignoreRow class
					if ($t.parent().hasClass(c.cssIgnoreRow)) { return; }
					// make sure to get header cell & not column indexed cell
					ch = ts.getColumnData( table, c.headers, index, true );
					// save original header content
					c.headerContent[index] = $t.html();
					// if headerTemplate is empty, don't reformat the header cell
					if ( c.headerTemplate !== '' && !$t.find('.' + ts.css.headerIn).length ) {
						// set up header template
						t = c.headerTemplate.replace(/\{content\}/g, $t.html()).replace(/\{icon\}/g, $t.find('.' + ts.css.icon).length ? '' : i);
						if (c.onRenderTemplate) {
							h = c.onRenderTemplate.apply($t, [index, t]);
							if (h && typeof h === 'string') { t = h; } // only change t if something is returned
						}
						$t.html('<div class="' + ts.css.headerIn + '">' + t + '</div>'); // faster than wrapInner
					}
					if (c.onRenderHeader) { c.onRenderHeader.apply($t, [index, c, c.$table]); }
					// *** remove this.column value if no conflicts found
					elem.column = parseInt( $t.attr('data-column'), 10);
					elem.order = formatSortingOrder( ts.getData($t, ch, 'sortInitialOrder') || c.sortInitialOrder ) ? [1,0,2] : [0,1,2];
					elem.count = -1; // set to -1 because clicking on the header automatically adds one
					elem.lockedOrder = false;
					lock = ts.getData($t, ch, 'lockedOrder') || false;
					if (typeof lock !== 'undefined' && lock !== false) {
						elem.order = elem.lockedOrder = formatSortingOrder(lock) ? [1,1,1] : [0,0,0];
					}
					$t.addClass(ts.css.header + ' ' + c.cssHeader);
					// add cell to headerList
					c.headerList[index] = elem;
					// add to parent in case there are multiple rows
					$t.parent().addClass(ts.css.headerRow + ' ' + c.cssHeaderRow).attr('role', 'row');
					// allow keyboard cursor to focus on element
					if (c.tabIndex) { $t.attr('tabindex', 0); }
					return elem;
				}));
				// cache headers per column
				c.$headerIndexed = [];
				for (indx = 0; indx < c.columns; indx++) {
					$t = c.$headers.filter('[data-column="' + indx + '"]');
					// target sortable column cells, unless there are none, then use non-sortable cells
					// .last() added in jQuery 1.4; use .filter(':last') to maintain compatibility with jQuery v1.2.6
					c.$headerIndexed[indx] = $t.not('.sorter-false').length ? $t.not('.sorter-false').filter(':last') : $t.filter(':last');
				}
				$(table).find(c.selectorHeaders).attr({
					scope: 'col',
					role : 'columnheader'
				});
				// enable/disable sorting
				updateHeader(table);
				if (c.debug) {
					benchmark('Built headers:', time);
					log(c.$headers);
				}
			}

			function commonUpdate(table, resort, callback) {
				var c = table.config;
				// remove rows/elements before update
				c.$table.find(c.selectorRemove).remove();
				// rebuild parsers
				buildParserCache(table);
				// rebuild the cache map
				buildCache(table);
				checkResort(c, resort, callback);
			}

			function updateHeader(table) {
				var index, s, $th, col,
					c = table.config,
					len = c.$headers.length;
				for ( index = 0; index < len; index++ ) {
					$th = c.$headers.eq( index );
					col = ts.getColumnData( table, c.headers, index, true );
					// add 'sorter-false' class if 'parser-false' is set
					s = ts.getData( $th, col, 'sorter' ) === 'false' || ts.getData( $th, col, 'parser' ) === 'false';
					$th[0].sortDisabled = s;
					$th[ s ? 'addClass' : 'removeClass' ]('sorter-false').attr('aria-disabled', '' + s);
					// aria-controls - requires table ID
					if (table.id) {
						if (s) {
							$th.removeAttr('aria-controls');
						} else {
							$th.attr('aria-controls', table.id);
						}
					}
				}
			}

			function setHeadersCss(table) {
				var f, h, i, j, $headers, $h, nextSort, txt,
					c = table.config,
					list = c.sortList,
					len = list.length,
					none = ts.css.sortNone + ' ' + c.cssNone,
					css = [ts.css.sortAsc + ' ' + c.cssAsc, ts.css.sortDesc + ' ' + c.cssDesc],
					cssIcon = [ c.cssIconAsc, c.cssIconDesc, c.cssIconNone ],
					aria = ['ascending', 'descending'],
					// find the footer
					$t = $(table).find('tfoot tr').children()
						.add( $( c.namespace + '_extra_headers' ) )
						.removeClass( css.join( ' ' ) );
				// remove all header information
				c.$headers
					.removeClass(css.join(' '))
					.addClass(none).attr('aria-sort', 'none')
					.find('.' + ts.css.icon)
					.removeClass(cssIcon.join(' '))
					.addClass(cssIcon[2]);
				for (i = 0; i < len; i++) {
					// direction = 2 means reset!
					if (list[i][1] !== 2) {
						// multicolumn sorting updating - choose the :last in case there are nested columns
						f = c.$headers.not('.sorter-false').filter('[data-column="' + list[i][0] + '"]' + (len === 1 ? ':last' : '') );
						if (f.length) {
							for (j = 0; j < f.length; j++) {
								if (!f[j].sortDisabled) {
									f.eq(j)
										.removeClass(none)
										.addClass(css[list[i][1]])
										.attr('aria-sort', aria[list[i][1]])
										.find('.' + ts.css.icon)
										.removeClass(cssIcon[2])
										.addClass(cssIcon[list[i][1]]);
								}
							}
							// add sorted class to footer & extra headers, if they exist
							if ($t.length) {
								$t.filter('[data-column="' + list[i][0] + '"]').removeClass(none).addClass(css[list[i][1]]);
							}
						}
					}
				}
				// add verbose aria labels
				len = c.$headers.length;
				$headers = c.$headers.not('.sorter-false');
				for ( i = 0; i < len; i++ ) {
					$h = $headers.eq( i );
					if ( $h.length ) {
						h = $headers[ i ];
						nextSort = h.order[ ( h.count + 1 ) % ( c.sortReset ? 3 : 2 ) ],
						txt = $.trim( $h.text() ) + ': ' +
							ts.language[ $h.hasClass( ts.css.sortAsc ) ? 'sortAsc' : $h.hasClass( ts.css.sortDesc ) ? 'sortDesc' : 'sortNone' ] +
							ts.language[ nextSort === 0 ? 'nextAsc' : nextSort === 1 ? 'nextDesc' : 'nextNone' ];
						$h.attr( 'aria-label', txt );
					}
				}
			}

			function updateHeaderSortCount( table, list ) {
				var col, dir, group, header, indx, primary, temp, val,
					c = table.config,
					sortList = list || c.sortList,
					len = sortList.length;
				c.sortList = [];
				for (indx = 0; indx < len; indx++) {
					val = sortList[indx];
					// ensure all sortList values are numeric - fixes #127
					col = parseInt(val[0], 10);
					// prevents error if sorton array is wrong
					if ( col < c.columns && c.$headerIndexed[col] ) {
						// make sure header exists
						header = c.$headerIndexed[col][0];
						// o.count = o.count + 1;
						dir = ('' + val[1]).match(/^(1|d|s|o|n)/);
						dir = dir ? dir[0] : '';
						// 0/(a)sc (default), 1/(d)esc, (s)ame, (o)pposite, (n)ext
						switch(dir) {
							case '1': case 'd': // descending
								dir = 1;
								break;
							case 's': // same direction (as primary column)
								// if primary sort is set to 's', make it ascending
								dir = primary || 0;
								break;
							case 'o':
								temp = header.order[(primary || 0) % (c.sortReset ? 3 : 2)];
								// opposite of primary column; but resets if primary resets
								dir = temp === 0 ? 1 : temp === 1 ? 0 : 2;
								break;
							case 'n':
								header.count = header.count + 1;
								dir = header.order[(header.count) % (c.sortReset ? 3 : 2)];
								break;
							default: // ascending
								dir = 0;
								break;
						}
						primary = indx === 0 ? dir : primary;
						group = [ col, parseInt(dir, 10) || 0 ];
						c.sortList.push(group);
						dir = $.inArray(group[1], header.order); // fixes issue #167
						header.count = dir >= 0 ? dir : group[1] % (c.sortReset ? 3 : 2);
					}
				}
			}

			function getCachedSortType(parsers, i) {
				return (parsers && parsers[i]) ? parsers[i].type || '' : '';
			}

			function initSort(table, cell, event){
				if (table.isUpdating) {
					// let any updates complete before initializing a sort
					return setTimeout(function(){ initSort(table, cell, event); }, 50);
				}
				var arry, indx, i, col, order, s, $header,
					c = table.config,
					key = !event[c.sortMultiSortKey],
					$table = c.$table,
					len = c.$headers.length;
				// Only call sortStart if sorting is enabled
				$table.trigger('sortStart', table);
				// get current column sort order
				cell.count = event[c.sortResetKey] ? 2 : (cell.count + 1) % (c.sortReset ? 3 : 2);
				// reset all sorts on non-current column - issue #30
				if (c.sortRestart) {
					indx = cell;
					for ( i = 0; i < len; i++ ) {
						$header = c.$headers.eq( i );
						// only reset counts on columns that weren't just clicked on and if not included in a multisort
						if ( $header[0] !== indx && ( key || !$header.is('.' + ts.css.sortDesc + ',.' + ts.css.sortAsc) ) ) {
							$header[0].count = -1;
						}
					}
				}
				// get current column index
				indx = parseInt( $(cell).attr('data-column'), 10 );
				// user only wants to sort on one column
				if (key) {
					// flush the sort list
					c.sortList = [];
					if (c.sortForce !== null) {
						arry = c.sortForce;
						for (col = 0; col < arry.length; col++) {
							if (arry[col][0] !== indx) {
								c.sortList.push(arry[col]);
							}
						}
					}
					// add column to sort list
					order = cell.order[cell.count];
					if (order < 2) {
						c.sortList.push([indx, order]);
						// add other columns if header spans across multiple
						if (cell.colSpan > 1) {
							for (col = 1; col < cell.colSpan; col++) {
								c.sortList.push([indx + col, order]);
							}
						}
					}
					// multi column sorting
				} else {
					// get rid of the sortAppend before adding more - fixes issue #115 & #523
					if (c.sortAppend && c.sortList.length > 1) {
						for (col = 0; col < c.sortAppend.length; col++) {
							s = ts.isValueInArray(c.sortAppend[col][0], c.sortList);
							if (s >= 0) {
								c.sortList.splice(s,1);
							}
						}
					}
					// the user has clicked on an already sorted column
					if (ts.isValueInArray(indx, c.sortList) >= 0) {
						// reverse the sorting direction
						for (col = 0; col < c.sortList.length; col++) {
							s = c.sortList[col];
							order = c.$headerIndexed[ s[0] ][0];
							if (s[0] === indx) {
								// order.count seems to be incorrect when compared to cell.count
								s[1] = order.order[cell.count];
								if (s[1] === 2) {
									c.sortList.splice(col,1);
									order.count = -1;
								}
							}
						}
					} else {
						// add column to sort list array
						order = cell.order[cell.count];
						if (order < 2) {
							c.sortList.push([indx, order]);
							// add other columns if header spans across multiple
							if (cell.colSpan > 1) {
								for (col = 1; col < cell.colSpan; col++) {
									c.sortList.push([indx + col, order]);
								}
							}
						}
					}
				}
				if (c.sortAppend !== null) {
					arry = c.sortAppend;
					for (col = 0; col < arry.length; col++) {
						if (arry[col][0] !== indx) {
							c.sortList.push(arry[col]);
						}
					}
				}
				// sortBegin event triggered immediately before the sort
				$table.trigger('sortBegin', table);
				// setTimeout needed so the processing icon shows up
				setTimeout(function(){
					// set css for headers
					setHeadersCss(table);
					multisort(table);
					appendToTable(table);
					$table.trigger('sortEnd', table);
				}, 1);
			}

			// sort multiple columns
			function multisort(table) { /*jshint loopfunc:true */
				var i, k, num, col, sortTime, colMax,
					rows, order, sort, x, y,
					dir = 0,
					c = table.config,
					cts = c.textSorter || '',
					sortList = c.sortList,
					l = sortList.length,
					bl = c.$tbodies.length;
				if (c.serverSideSorting || isEmptyObject(c.cache)) { // empty table - fixes #206/#346
					return;
				}
				if (c.debug) { sortTime = new Date(); }
				for (k = 0; k < bl; k++) {
					colMax = c.cache[k].colMax;
					rows = c.cache[k].normalized;

					rows.sort(function(a, b) {
						// rows is undefined here in IE, so don't use it!
						for (i = 0; i < l; i++) {
							col = sortList[i][0];
							order = sortList[i][1];
							// sort direction, true = asc, false = desc
							dir = order === 0;

							if (c.sortStable && a[col] === b[col] && l === 1) {
								return a[c.columns].order - b[c.columns].order;
							}

							// fallback to natural sort since it is more robust
							num = /n/i.test(getCachedSortType(c.parsers, col));
							if (num && c.strings[col]) {
								// sort strings in numerical columns
								if (typeof (c.string[c.strings[col]]) === 'boolean') {
									num = (dir ? 1 : -1) * (c.string[c.strings[col]] ? -1 : 1);
								} else {
									num = (c.strings[col]) ? c.string[c.strings[col]] || 0 : 0;
								}
								// fall back to built-in numeric sort
								// var sort = $.tablesorter['sort' + s](table, a[c], b[c], c, colMax[c], dir);
								sort = c.numberSorter ? c.numberSorter(a[col], b[col], dir, colMax[col], table) :
									ts[ 'sortNumeric' + (dir ? 'Asc' : 'Desc') ](a[col], b[col], num, colMax[col], col, table);
							} else {
								// set a & b depending on sort direction
								x = dir ? a : b;
								y = dir ? b : a;
								// text sort function
								if (typeof(cts) === 'function') {
									// custom OVERALL text sorter
									sort = cts(x[col], y[col], dir, col, table);
								} else if (typeof(cts) === 'object' && cts.hasOwnProperty(col)) {
									// custom text sorter for a SPECIFIC COLUMN
									sort = cts[col](x[col], y[col], dir, col, table);
								} else {
									// fall back to natural sort
									sort = ts[ 'sortNatural' + (dir ? 'Asc' : 'Desc') ](a[col], b[col], col, table, c);
								}
							}
							if (sort) { return sort; }
						}
						return a[c.columns].order - b[c.columns].order;
					});
				}
				if (c.debug) { benchmark('Sorting on ' + sortList.toString() + ' and dir ' + order + ' time', sortTime); }
			}

			function resortComplete(c, callback){
				if (c.table.isUpdating) {
					c.$table.trigger('updateComplete', c.table);
				}
				if ($.isFunction(callback)) {
					callback(c.table);
				}
			}

			function checkResort(c, resort, callback) {
				var sl = $.isArray(resort) ? resort : c.sortList,
					// if no resort parameter is passed, fallback to config.resort (true by default)
					resrt = typeof resort === 'undefined' ? c.resort : resort;
				// don't try to resort if the table is still processing
				// this will catch spamming of the updateCell method
				if (resrt !== false && !c.serverSideSorting && !c.table.isProcessing) {
					if (sl.length) {
						c.$table.trigger('sorton', [sl, function(){
							resortComplete(c, callback);
						}, true]);
					} else {
						c.$table.trigger('sortReset', [function(){
							resortComplete(c, callback);
							ts.applyWidget(c.table, false);
						}]);
					}
				} else {
					resortComplete(c, callback);
					ts.applyWidget(c.table, false);
				}
			}

			function bindMethods(table){
				var c = table.config,
					$table = c.$table,
					events = ('sortReset update updateRows updateCell updateAll addRows updateComplete sorton appendCache ' +
						'updateCache applyWidgetId applyWidgets refreshWidgets destroy mouseup mouseleave ').split(' ')
						.join(c.namespace + ' ');
				// apply easy methods that trigger bound events
				$table
				.unbind( events.replace(/\s+/g, ' ') )
				.bind('sortReset' + c.namespace, function(e, callback){
					e.stopPropagation();
					c.sortList = [];
					setHeadersCss(table);
					multisort(table);
					appendToTable(table);
					if ($.isFunction(callback)) {
						callback(table);
					}
				})
				.bind('updateAll' + c.namespace, function(e, resort, callback){
					e.stopPropagation();
					table.isUpdating = true;
					ts.refreshWidgets(table, true, true);
					buildHeaders(table);
					ts.bindEvents(table, c.$headers, true);
					bindMethods(table);
					commonUpdate(table, resort, callback);
				})
				.bind('update' + c.namespace + ' updateRows' + c.namespace, function(e, resort, callback) {
					e.stopPropagation();
					table.isUpdating = true;
					// update sorting (if enabled/disabled)
					updateHeader(table);
					commonUpdate(table, resort, callback);
				})
				.bind('updateCell' + c.namespace, function(e, cell, resort, callback) {
					e.stopPropagation();
					table.isUpdating = true;
					$table.find(c.selectorRemove).remove();
					// get position from the dom
					var t, row, icell, cache,
					$tb = c.$tbodies,
					$cell = $(cell),
					// update cache - format: function(s, table, cell, cellIndex)
					// no closest in jQuery v1.2.6 - tbdy = $tb.index( $(cell).closest('tbody') ),$row = $(cell).closest('tr');
					tbdy = $tb.index( $.fn.closest ? $cell.closest('tbody') : $cell.parents('tbody').filter(':first') ),
					tbcache = c.cache[ tbdy ],
					$row = $.fn.closest ? $cell.closest('tr') : $cell.parents('tr').filter(':first');
					cell = $cell[0]; // in case cell is a jQuery object
					// tbody may not exist if update is initialized while tbody is removed for processing
					if ($tb.length && tbdy >= 0) {
						row = $tb.eq( tbdy ).find( 'tr' ).index( $row );
						cache = tbcache.normalized[ row ];
						icell = $cell.index();
						t = getParsedText( c, cell, icell );
						cache[ icell ] = t;
						cache[ c.columns ].$row = $row;
						if ( (c.parsers[icell].type || '').toLowerCase() === 'numeric' ) {
							// update column max value (ignore sign)
							tbcache.colMax[icell] = Math.max(Math.abs(t) || 0, tbcache.colMax[icell] || 0);
						}
						t = resort !== 'undefined' ? resort : c.resort;
						if (t !== false) {
							// widgets will be reapplied
							checkResort(c, t, callback);
						} else {
							// don't reapply widgets is resort is false, just in case it causes
							// problems with element focus
							if ($.isFunction(callback)) {
								callback(table);
							}
							c.$table.trigger('updateComplete', c.table);
						}
					}
				})
				.bind('addRows' + c.namespace, function(e, $row, resort, callback) {
					e.stopPropagation();
					table.isUpdating = true;
					if (isEmptyObject(c.cache)) {
						// empty table, do an update instead - fixes #450
						updateHeader(table);
						commonUpdate(table, resort, callback);
					} else {
						$row = $($row).attr('role', 'row'); // make sure we're using a jQuery object
						var i, j, l, rowData, cells,
						rows = $row.filter('tr').length,
						tbdy = c.$tbodies.index( $row.parents('tbody').filter(':first') );
						// fixes adding rows to an empty table - see issue #179
						if (!(c.parsers && c.parsers.length)) {
							buildParserCache(table);
						}
						// add each row
						for (i = 0; i < rows; i++) {
							l = $row[i].cells.length;
							cells = [];
							rowData = {
								child: [],
								$row : $row.eq(i),
								order: c.cache[tbdy].normalized.length
							};
							// add each cell
							for (j = 0; j < l; j++) {
								cells[j] = getParsedText( c, $row[i].cells[j], j );
								if ((c.parsers[j].type || '').toLowerCase() === 'numeric') {
									// update column max value (ignore sign)
									c.cache[tbdy].colMax[j] = Math.max(Math.abs(cells[j]) || 0, c.cache[tbdy].colMax[j] || 0);
								}
							}
							// add the row data to the end
							cells.push(rowData);
							// update cache
							c.cache[tbdy].normalized.push(cells);
						}
						// resort using current settings
						checkResort(c, resort, callback);
					}
				})
				.bind('updateComplete' + c.namespace, function(){
					table.isUpdating = false;
				})
				.bind('sorton' + c.namespace, function(e, list, callback, init) {
					var c = table.config;
					e.stopPropagation();
					$table.trigger('sortStart', this);
					// update header count index
					updateHeaderSortCount(table, list);
					// set css for headers
					setHeadersCss(table);
					// fixes #346
					if (c.delayInit && isEmptyObject(c.cache)) { buildCache(table); }
					$table.trigger('sortBegin', this);
					// sort the table and append it to the dom
					multisort(table);
					appendToTable(table, init);
					$table.trigger('sortEnd', this);
					ts.applyWidget(table);
					if ($.isFunction(callback)) {
						callback(table);
					}
				})
				.bind('appendCache' + c.namespace, function(e, callback, init) {
					e.stopPropagation();
					appendToTable(table, init);
					if ($.isFunction(callback)) {
						callback(table);
					}
				})
				.bind('updateCache' + c.namespace, function(e, callback){
					// rebuild parsers
					if (!(c.parsers && c.parsers.length)) {
						buildParserCache(table);
					}
					// rebuild the cache map
					buildCache(table);
					if ($.isFunction(callback)) {
						callback(table);
					}
				})
				.bind('applyWidgetId' + c.namespace, function(e, id) {
					e.stopPropagation();
					ts.getWidgetById(id).format(table, c, c.widgetOptions);
				})
				.bind('applyWidgets' + c.namespace, function(e, init) {
					e.stopPropagation();
					// apply widgets
					ts.applyWidget(table, init);
				})
				.bind('refreshWidgets' + c.namespace, function(e, all, dontapply){
					e.stopPropagation();
					ts.refreshWidgets(table, all, dontapply);
				})
				.bind('destroy' + c.namespace, function(e, c, cb){
					e.stopPropagation();
					ts.destroy(table, c, cb);
				})
				.bind('resetToLoadState' + c.namespace, function(){
					// remove all widgets
					ts.removeWidget(table, true, false);
					// restore original settings; this clears out current settings, but does not clear
					// values saved to storage.
					c = $.extend(true, ts.defaults, c.originalSettings);
					table.hasInitialized = false;
					// setup the entire table again
					ts.setup( table, c );
				});
			}

			/* public methods */
			ts.construct = function(settings) {
				return this.each(function() {
					var table = this,
						// merge & extend config options
						c = $.extend(true, {}, ts.defaults, settings, ts.instanceMethods);
						// save initial settings
						c.originalSettings = settings;
					// create a table from data (build table widget)
					if (!table.hasInitialized && ts.buildTable && this.nodeName !== 'TABLE') {
						// return the table (in case the original target is the table's container)
						ts.buildTable(table, c);
					} else {
						ts.setup(table, c);
					}
				});
			};

			ts.setup = function(table, c) {
				// if no thead or tbody, or tablesorter is already present, quit
				if (!table || !table.tHead || table.tBodies.length === 0 || table.hasInitialized === true) {
					return c.debug ? log('ERROR: stopping initialization! No table, thead, tbody or tablesorter has already been initialized') : '';
				}

				var k = '',
					$table = $(table),
					m = $.metadata;
				// initialization flag
				table.hasInitialized = false;
				// table is being processed flag
				table.isProcessing = true;
				// make sure to store the config object
				table.config = c;
				// save the settings where they read
				$.data(table, 'tablesorter', c);
				if (c.debug) { $.data( table, 'startoveralltimer', new Date()); }

				// removing this in version 3 (only supports jQuery 1.7+)
				c.supportsDataObject = (function(version) {
					version[0] = parseInt(version[0], 10);
					return (version[0] > 1) || (version[0] === 1 && parseInt(version[1], 10) >= 4);
				})($.fn.jquery.split('.'));
				// digit sort text location; keeping max+/- for backwards compatibility
				c.string = { 'max': 1, 'min': -1, 'emptymin': 1, 'emptymax': -1, 'zero': 0, 'none': 0, 'null': 0, 'top': true, 'bottom': false };
				// ensure case insensitivity
				c.emptyTo = c.emptyTo.toLowerCase();
				c.stringTo = c.stringTo.toLowerCase();
				// add table theme class only if there isn't already one there
				if (!/tablesorter\-/.test($table.attr('class'))) {
					k = (c.theme !== '' ? ' tablesorter-' + c.theme : '');
				}
				c.table = table;
				c.$table = $table
					.addClass(ts.css.table + ' ' + c.tableClass + k)
					.attr('role', 'grid');
				c.$headers = $table.find(c.selectorHeaders);

				// give the table a unique id, which will be used in namespace binding
				if (!c.namespace) {
					c.namespace = '.tablesorter' + Math.random().toString(16).slice(2);
				} else {
					// make sure namespace starts with a period & doesn't have weird characters
					c.namespace = '.' + c.namespace.replace(/\W/g,'');
				}

				c.$table.children().children('tr').attr('role', 'row');
				c.$tbodies = $table.children('tbody:not(.' + c.cssInfoBlock + ')').attr({
					'aria-live' : 'polite',
					'aria-relevant' : 'all'
				});
				if (c.$table.children('caption').length) {
					k = c.$table.children('caption')[0];
					if (!k.id) { k.id = c.namespace.slice(1) + 'caption'; }
					c.$table.attr('aria-labelledby', k.id);
				}
				c.widgetInit = {}; // keep a list of initialized widgets
				// change textExtraction via data-attribute
				c.textExtraction = c.$table.attr('data-text-extraction') || c.textExtraction || 'basic';
				// build headers
				buildHeaders(table);
				// fixate columns if the users supplies the fixedWidth option
				// do this after theme has been applied
				ts.fixColumnWidth(table);
				// add widget options before parsing (e.g. grouping widget has parser settings)
				ts.applyWidgetOptions(table, c);
				// try to auto detect column type, and store in tables config
				buildParserCache(table);
				// start total row count at zero
				c.totalRows = 0;
				// build the cache for the tbody cells
				// delayInit will delay building the cache until the user starts a sort
				if (!c.delayInit) { buildCache(table); }
				// bind all header events and methods
				ts.bindEvents(table, c.$headers, true);
				bindMethods(table);
				// get sort list from jQuery data or metadata
				// in jQuery < 1.4, an error occurs when calling $table.data()
				if (c.supportsDataObject && typeof $table.data().sortlist !== 'undefined') {
					c.sortList = $table.data().sortlist;
				} else if (m && ($table.metadata() && $table.metadata().sortlist)) {
					c.sortList = $table.metadata().sortlist;
				}
				// apply widget init code
				ts.applyWidget(table, true);
				// if user has supplied a sort list to constructor
				if (c.sortList.length > 0) {
					$table.trigger('sorton', [c.sortList, {}, !c.initWidgets, true]);
				} else {
					setHeadersCss(table);
					if (c.initWidgets) {
						// apply widget format
						ts.applyWidget(table, false);
					}
				}

				// show processesing icon
				if (c.showProcessing) {
					$table
					.unbind('sortBegin' + c.namespace + ' sortEnd' + c.namespace)
					.bind('sortBegin' + c.namespace + ' sortEnd' + c.namespace, function(e) {
						clearTimeout(c.processTimer);
						ts.isProcessing(table);
						if (e.type === 'sortBegin') {
							c.processTimer = setTimeout(function(){
								ts.isProcessing(table, true);
							}, 500);
						}
					});
				}

				// initialized
				table.hasInitialized = true;
				table.isProcessing = false;
				if (c.debug) {
					ts.benchmark('Overall initialization time', $.data( table, 'startoveralltimer'));
				}
				$table.trigger('tablesorter-initialized', table);
				if (typeof c.initialized === 'function') { c.initialized(table); }
			};

			// automatically add a colgroup with col elements set to a percentage width
			ts.fixColumnWidth = function(table) {
				table = $(table)[0];
				var overallWidth, percent, $tbodies, len, index,
					c = table.config,
					colgroup = c.$table.children('colgroup');
				// remove plugin-added colgroup, in case we need to refresh the widths
				if (colgroup.length && colgroup.hasClass(ts.css.colgroup)) {
					colgroup.remove();
				}
				if (c.widthFixed && c.$table.children('colgroup').length === 0) {
					colgroup = $('<colgroup class="' + ts.css.colgroup + '">');
					overallWidth = c.$table.width();
					// only add col for visible columns - fixes #371
					$tbodies = c.$tbodies.find('tr:first').children(':visible'); //.each(function()
					len = $tbodies.length;
					for ( index = 0; index < len; index++ ) {
						percent = parseInt( ( $tbodies.eq( index ).width() / overallWidth ) * 1000, 10 ) / 10 + '%';
						colgroup.append( $('<col>').css('width', percent) );
					}
					c.$table.prepend(colgroup);
				}
			};

			ts.getColumnData = function(table, obj, indx, getCell, $headers){
				if (typeof obj === 'undefined' || obj === null) { return; }
				table = $(table)[0];
				var $h, k,
					c = table.config,
					$cells = ( $headers || c.$headers ),
					// c.$headerIndexed is not defined initially
					$cell = c.$headerIndexed && c.$headerIndexed[indx] || $cells.filter('[data-column="' + indx + '"]:last');
				if (obj[indx]) {
					return getCell ? obj[indx] : obj[$cells.index( $cell )];
				}
				for (k in obj) {
					if (typeof k === 'string') {
						$h = $cell
							// header cell with class/id
							.filter(k)
							// find elements within the header cell with cell/id
							.add( $cell.find(k) );
						if ($h.length) {
							return obj[k];
						}
					}
				}
				return;
			};

			// computeTableHeaderCellIndexes from:
			// http://www.javascripttoolbox.com/lib/table/examples.php
			// http://www.javascripttoolbox.com/temp/table_cellindex.html
			ts.computeColumnIndex = function(trs) {
				var i, j, k, l, $cell, cell, cells, rowIndex, cellId, rowSpan, colSpan, firstAvailCol,
					matrix = [],
					matrixrow = [],
					lookup = {};
				for (i = 0; i < trs.length; i++) {
					cells = trs[i].cells;
					for (j = 0; j < cells.length; j++) {
						cell = cells[j];
						$cell = $(cell);
						rowIndex = cell.parentNode.rowIndex;
						cellId = rowIndex + '-' + $cell.index();
						rowSpan = cell.rowSpan || 1;
						colSpan = cell.colSpan || 1;
						if (typeof(matrix[rowIndex]) === 'undefined') {
							matrix[rowIndex] = [];
						}
						// Find first available column in the first row
						for (k = 0; k < matrix[rowIndex].length + 1; k++) {
							if (typeof(matrix[rowIndex][k]) === 'undefined') {
								firstAvailCol = k;
								break;
							}
						}
						lookup[cellId] = firstAvailCol;
						// add data-column
						$cell.attr({ 'data-column' : firstAvailCol }); // 'data-row' : rowIndex
						for (k = rowIndex; k < rowIndex + rowSpan; k++) {
							if (typeof(matrix[k]) === 'undefined') {
								matrix[k] = [];
							}
							matrixrow = matrix[k];
							for (l = firstAvailCol; l < firstAvailCol + colSpan; l++) {
								matrixrow[l] = 'x';
							}
						}
					}
				}
				return matrixrow.length;
			};

			// *** Process table ***
			// add processing indicator
			ts.isProcessing = function(table, toggle, $ths) {
				table = $(table);
				var c = table[0].config,
					// default to all headers
					$h = $ths || table.find('.' + ts.css.header);
				if (toggle) {
					// don't use sortList if custom $ths used
					if (typeof $ths !== 'undefined' && c.sortList.length > 0) {
						// get headers from the sortList
						$h = $h.filter(function(){
							// get data-column from attr to keep  compatibility with jQuery 1.2.6
							return this.sortDisabled ? false : ts.isValueInArray( parseFloat($(this).attr('data-column')), c.sortList) >= 0;
						});
					}
					table.add($h).addClass(ts.css.processing + ' ' + c.cssProcessing);
				} else {
					table.add($h).removeClass(ts.css.processing + ' ' + c.cssProcessing);
				}
			};

			// detach tbody but save the position
			// don't use tbody because there are portions that look for a tbody index (updateCell)
			ts.processTbody = function(table, $tb, getIt){
				table = $(table)[0];
				var holdr;
				if (getIt) {
					table.isProcessing = true;
					$tb.before('<span class="tablesorter-savemyplace"/>');
					holdr = ($.fn.detach) ? $tb.detach() : $tb.remove();
					return holdr;
				}
				holdr = $(table).find('span.tablesorter-savemyplace');
				$tb.insertAfter( holdr );
				holdr.remove();
				table.isProcessing = false;
			};

			ts.clearTableBody = function(table) {
				$(table)[0].config.$tbodies.children().detach();
			};

			ts.bindEvents = function(table, $headers, core) {
				table = $(table)[0];
				var t, downTarget = null,
					c = table.config;
				if (core !== true) {
					$headers.addClass( c.namespace.slice(1) + '_extra_headers' );
					t = $.fn.closest ? $headers.closest('table')[0] : $headers.parents('table')[0];
					if (t && t.nodeName === 'TABLE' && t !== table) {
						$(t).addClass( c.namespace.slice(1) + '_extra_table' );
					}
				}
				t = ( c.pointerDown + ' ' + c.pointerUp + ' ' + c.pointerClick + ' sort keyup ' )
					.replace(/\s+/g, ' ')
					.split(' ')
					.join(c.namespace + ' ');
				// apply event handling to headers and/or additional headers (stickyheaders, scroller, etc)
				$headers
				// http://stackoverflow.com/questions/5312849/jquery-find-self;
				.find(c.selectorSort).add( $headers.filter(c.selectorSort) )
				.unbind(t)
				.bind(t, function(e, external) {
					var cell, temp,
						$target = $(e.target),
						// wrap event type in spaces, so the match doesn't trigger on inner words
						type = ' ' + e.type + ' ';
					// only recognize left clicks
					if ( ( ( e.which || e.button ) !== 1 && !type.match( ' ' + c.pointerClick + ' | sort | keyup ' ) ) ||
						// allow pressing enter
						( type === ' keyup ' && e.which !== 13 ) ||
						// allow triggering a click event (e.which is undefined) & ignore physical clicks
						( type.match(' ' + c.pointerClick + ' ') && typeof e.which !== 'undefined' ) ) {
						return;
					}
					// ignore mouseup if mousedown wasn't on the same target
					if ( type.match(' ' + c.pointerUp + ' ') && downTarget !== e.target && external !== true ) { return; }
					// set timer on mousedown
					if ( type.match(' ' + c.pointerDown + ' ') ) {
						downTarget = e.target;
						// needed or jQuery v1.3.2 or older throws an "Uncaught TypeError: handler.apply is not a function" error
						temp = $target.jquery.split( '.' );
						if ( temp[0] === '1' && temp[1] < 4 ) { e.preventDefault(); }
						return;
					}
					downTarget = null;
					// prevent sort being triggered on form elements
					if ( /(input|select|button|textarea)/i.test(e.target.nodeName) ||
						// nosort class name, or elements within a nosort container
						$target.hasClass(c.cssNoSort) || $target.parents('.' + c.cssNoSort).length > 0 ||
						// elements within a button
						$target.parents('button').length > 0 ) {
						return !c.cancelSelection;
					}
					if (c.delayInit && isEmptyObject(c.cache)) { buildCache(table); }
					// jQuery v1.2.6 doesn't have closest()
					cell = $.fn.closest ? $(this).closest('th, td')[0] : /TH|TD/.test(this.nodeName) ? this : $(this).parents('th, td')[0];
					// reference original table headers and find the same cell
					cell = c.$headers[ $headers.index( cell ) ];
					if (!cell.sortDisabled) {
						initSort(table, cell, e);
					}
				});
				if (c.cancelSelection) {
					// cancel selection
					$headers
						.attr('unselectable', 'on')
						.bind('selectstart', false)
						.css({
							'user-select': 'none',
							'MozUserSelect': 'none' // not needed for jQuery 1.8+
						});
				}
			};

			// restore headers
			ts.restoreHeaders = function(table){
				var index, $cell,
					c = $(table)[0].config,
					$headers = c.$table.find( c.selectorHeaders ),
					len = $headers.length;
				// don't use c.$headers here in case header cells were swapped
				for ( index = 0; index < len; index++ ) {
					// c.$table.find(c.selectorHeaders).each(function(i){
					$cell = $headers.eq( index );
					// only restore header cells if it is wrapped
					// because this is also used by the updateAll method
					if ( $cell.find( '.' + ts.css.headerIn ).length ) {
						$cell.html( c.headerContent[ index ] );
					}
				}
			};

			ts.destroy = function(table, removeClasses, callback){
				table = $(table)[0];
				if (!table.hasInitialized) { return; }
				// remove all widgets
				ts.removeWidget(table, true, false);
				var events,
					$t = $(table),
					c = table.config,
					$h = $t.find('thead:first'),
					$r = $h.find('tr.' + ts.css.headerRow).removeClass(ts.css.headerRow + ' ' + c.cssHeaderRow),
					$f = $t.find('tfoot:first > tr').children('th, td');
				if (removeClasses === false && $.inArray('uitheme', c.widgets) >= 0) {
					// reapply uitheme classes, in case we want to maintain appearance
					$t.trigger('applyWidgetId', ['uitheme']);
					$t.trigger('applyWidgetId', ['zebra']);
				}
				// remove widget added rows, just in case
				$h.find('tr').not($r).remove();
				// disable tablesorter
				events = 'sortReset update updateAll updateRows updateCell addRows updateComplete sorton appendCache updateCache ' +
					'applyWidgetId applyWidgets refreshWidgets destroy mouseup mouseleave keypress sortBegin sortEnd resetToLoadState '.split(' ')
					.join(c.namespace + ' ');
				$t
					.removeData('tablesorter')
					.unbind( events.replace(/\s+/g, ' ') );
				c.$headers.add($f)
					.removeClass( [ts.css.header, c.cssHeader, c.cssAsc, c.cssDesc, ts.css.sortAsc, ts.css.sortDesc, ts.css.sortNone].join(' ') )
					.removeAttr('data-column')
					.removeAttr('aria-label')
					.attr('aria-disabled', 'true');
				$r.find(c.selectorSort).unbind( ('mousedown mouseup keypress '.split(' ').join(c.namespace + ' ')).replace(/\s+/g, ' ') );
				ts.restoreHeaders(table);
				$t.toggleClass(ts.css.table + ' ' + c.tableClass + ' tablesorter-' + c.theme, removeClasses === false);
				// clear flag in case the plugin is initialized again
				table.hasInitialized = false;
				delete table.config.cache;
				if (typeof callback === 'function') {
					callback(table);
				}
			};

			// *** sort functions ***
			// regex used in natural sort
			ts.regex = {
				chunk : /(^([+\-]?(?:0|[1-9]\d*)(?:\.\d*)?(?:[eE][+\-]?\d+)?)?$|^0x[0-9a-f]+$|\d+)/gi, // chunk/tokenize numbers & letters
				chunks: /(^\\0|\\0$)/, // replace chunks @ ends
				hex: /^0x[0-9a-f]+$/i // hex
			};

			// Natural sort - https://github.com/overset/javascript-natural-sort (date sorting removed)
			// this function will only accept strings, or you'll see 'TypeError: undefined is not a function'
			// I could add a = a.toString(); b = b.toString(); but it'll slow down the sort overall
			ts.sortNatural = function(a, b) {
				if (a === b) { return 0; }
				var xN, xD, yN, yD, xF, yF, i, mx,
					r = ts.regex;
				// first try and sort Hex codes
				if (r.hex.test(b)) {
					xD = parseInt(a.match(r.hex), 16);
					yD = parseInt(b.match(r.hex), 16);
					if ( xD < yD ) { return -1; }
					if ( xD > yD ) { return 1; }
				}
				// chunk/tokenize
				xN = a.replace(r.chunk, '\\0$1\\0').replace(r.chunks, '').split('\\0');
				yN = b.replace(r.chunk, '\\0$1\\0').replace(r.chunks, '').split('\\0');
				mx = Math.max(xN.length, yN.length);
				// natural sorting through split numeric strings and default strings
				for (i = 0; i < mx; i++) {
					// find floats not starting with '0', string or 0 if not defined
					xF = isNaN(xN[i]) ? xN[i] || 0 : parseFloat(xN[i]) || 0;
					yF = isNaN(yN[i]) ? yN[i] || 0 : parseFloat(yN[i]) || 0;
					// handle numeric vs string comparison - number < string - (Kyle Adams)
					if (isNaN(xF) !== isNaN(yF)) { return (isNaN(xF)) ? 1 : -1; }
					// rely on string comparison if different types - i.e. '02' < 2 != '02' < '2'
					if (typeof xF !== typeof yF) {
						xF += '';
						yF += '';
					}
					if (xF < yF) { return -1; }
					if (xF > yF) { return 1; }
				}
				return 0;
			};

			ts.sortNaturalAsc = function(a, b, col, table, c) {
				if (a === b) { return 0; }
				var e = c.string[ (c.empties[col] || c.emptyTo ) ];
				if (a === '' && e !== 0) { return typeof e === 'boolean' ? (e ? -1 : 1) : -e || -1; }
				if (b === '' && e !== 0) { return typeof e === 'boolean' ? (e ? 1 : -1) : e || 1; }
				return ts.sortNatural(a, b);
			};

			ts.sortNaturalDesc = function(a, b, col, table, c) {
				if (a === b) { return 0; }
				var e = c.string[ (c.empties[col] || c.emptyTo ) ];
				if (a === '' && e !== 0) { return typeof e === 'boolean' ? (e ? -1 : 1) : e || 1; }
				if (b === '' && e !== 0) { return typeof e === 'boolean' ? (e ? 1 : -1) : -e || -1; }
				return ts.sortNatural(b, a);
			};

			// basic alphabetical sort
			ts.sortText = function(a, b) {
				return a > b ? 1 : (a < b ? -1 : 0);
			};

			// return text string value by adding up ascii value
			// so the text is somewhat sorted when using a digital sort
			// this is NOT an alphanumeric sort
			ts.getTextValue = function(a, num, mx) {
				if (mx) {
					// make sure the text value is greater than the max numerical value (mx)
					var i, l = a ? a.length : 0, n = mx + num;
					for (i = 0; i < l; i++) {
						n += a.charCodeAt(i);
					}
					return num * n;
				}
				return 0;
			};

			ts.sortNumericAsc = function(a, b, num, mx, col, table) {
				if (a === b) { return 0; }
				var c = table.config,
					e = c.string[ (c.empties[col] || c.emptyTo ) ];
				if (a === '' && e !== 0) { return typeof e === 'boolean' ? (e ? -1 : 1) : -e || -1; }
				if (b === '' && e !== 0) { return typeof e === 'boolean' ? (e ? 1 : -1) : e || 1; }
				if (isNaN(a)) { a = ts.getTextValue(a, num, mx); }
				if (isNaN(b)) { b = ts.getTextValue(b, num, mx); }
				return a - b;
			};

			ts.sortNumericDesc = function(a, b, num, mx, col, table) {
				if (a === b) { return 0; }
				var c = table.config,
					e = c.string[ (c.empties[col] || c.emptyTo ) ];
				if (a === '' && e !== 0) { return typeof e === 'boolean' ? (e ? -1 : 1) : e || 1; }
				if (b === '' && e !== 0) { return typeof e === 'boolean' ? (e ? 1 : -1) : -e || -1; }
				if (isNaN(a)) { a = ts.getTextValue(a, num, mx); }
				if (isNaN(b)) { b = ts.getTextValue(b, num, mx); }
				return b - a;
			};

			ts.sortNumeric = function(a, b) {
				return a - b;
			};

			// used when replacing accented characters during sorting
			ts.characterEquivalents = {
				'a' : '\u00e1\u00e0\u00e2\u00e3\u00e4\u0105\u00e5', // áàâãäąå
				'A' : '\u00c1\u00c0\u00c2\u00c3\u00c4\u0104\u00c5', // ÁÀÂÃÄĄÅ
				'c' : '\u00e7\u0107\u010d', // çćč
				'C' : '\u00c7\u0106\u010c', // ÇĆČ
				'e' : '\u00e9\u00e8\u00ea\u00eb\u011b\u0119', // éèêëěę
				'E' : '\u00c9\u00c8\u00ca\u00cb\u011a\u0118', // ÉÈÊËĚĘ
				'i' : '\u00ed\u00ec\u0130\u00ee\u00ef\u0131', // íìİîïı
				'I' : '\u00cd\u00cc\u0130\u00ce\u00cf', // ÍÌİÎÏ
				'o' : '\u00f3\u00f2\u00f4\u00f5\u00f6\u014d', // óòôõöō
				'O' : '\u00d3\u00d2\u00d4\u00d5\u00d6\u014c', // ÓÒÔÕÖŌ
				'ss': '\u00df', // ß (s sharp)
				'SS': '\u1e9e', // ẞ (Capital sharp s)
				'u' : '\u00fa\u00f9\u00fb\u00fc\u016f', // úùûüů
				'U' : '\u00da\u00d9\u00db\u00dc\u016e' // ÚÙÛÜŮ
			};
			ts.replaceAccents = function(s) {
				var a, acc = '[', eq = ts.characterEquivalents;
				if (!ts.characterRegex) {
					ts.characterRegexArray = {};
					for (a in eq) {
						if (typeof a === 'string') {
							acc += eq[a];
							ts.characterRegexArray[a] = new RegExp('[' + eq[a] + ']', 'g');
						}
					}
					ts.characterRegex = new RegExp(acc + ']');
				}
				if (ts.characterRegex.test(s)) {
					for (a in eq) {
						if (typeof a === 'string') {
							s = s.replace( ts.characterRegexArray[a], a );
						}
					}
				}
				return s;
			};

			// *** utilities ***
			ts.isValueInArray = function(column, arry) {
				var indx, len = arry.length;
				for (indx = 0; indx < len; indx++) {
					if (arry[indx][0] === column) {
						return indx;
					}
				}
				return -1;
			};

			ts.addParser = function(parser) {
				var i, l = ts.parsers.length, a = true;
				for (i = 0; i < l; i++) {
					if (ts.parsers[i].id.toLowerCase() === parser.id.toLowerCase()) {
						a = false;
					}
				}
				if (a) {
					ts.parsers.push(parser);
				}
			};

			// Use it to add a set of methods to table.config which will be available for all tables.
			// This should be done before table initialization
			ts.addInstanceMethods = function(methods) {
				$.extend(ts.instanceMethods, methods);
			};

			ts.getParserById = function(name) {
				/*jshint eqeqeq:false */
				if (name == 'false') { return false; }
				var i, l = ts.parsers.length;
				for (i = 0; i < l; i++) {
					if (ts.parsers[i].id.toLowerCase() === (name.toString()).toLowerCase()) {
						return ts.parsers[i];
					}
				}
				return false;
			};

			ts.addWidget = function(widget) {
				ts.widgets.push(widget);
			};

			ts.hasWidget = function(table, name){
				table = $(table);
				return table.length && table[0].config && table[0].config.widgetInit[name] || false;
			};

			ts.getWidgetById = function(name) {
				var i, w, l = ts.widgets.length;
				for (i = 0; i < l; i++) {
					w = ts.widgets[i];
					if (w && w.hasOwnProperty('id') && w.id.toLowerCase() === name.toLowerCase()) {
						return w;
					}
				}
			};

			ts.applyWidgetOptions = function( table, c ){
				var indx, widget,
					len = c.widgets.length,
					wo = c.widgetOptions;
				if (len) {
					for (indx = 0; indx < len; indx++) {
						widget = ts.getWidgetById( c.widgets[indx] );
						if ( widget && 'options' in widget ) {
							wo = table.config.widgetOptions = $.extend( true, {}, widget.options, wo );
						}
					}
				}
			};

			ts.applyWidget = function(table, init, callback) {
				table = $(table)[0]; // in case this is called externally
				var indx, len, name,
					c = table.config,
					wo = c.widgetOptions,
					tableClass = ' ' + c.table.className + ' ',
					widgets = [],
					time, time2, w, wd;
				// prevent numerous consecutive widget applications
				if (init !== false && table.hasInitialized && (table.isApplyingWidgets || table.isUpdating)) { return; }
				if (c.debug) { time = new Date(); }
				// look for widgets to apply from in table class
				// stop using \b otherwise this matches 'ui-widget-content' & adds 'content' widget
				wd = new RegExp( '\\s' + c.widgetClass.replace( /\{name\}/i, '([\\w-]+)' )+ '\\s', 'g' );
				if ( tableClass.match( wd ) ) {
					// extract out the widget id from the table class (widget id's can include dashes)
					w = tableClass.match( wd );
					if ( w ) {
						len = w.length;
						for (indx = 0; indx < len; indx++) {
							c.widgets.push( w[indx].replace( wd, '$1' ) );
						}
					}
				}
				if (c.widgets.length) {
					table.isApplyingWidgets = true;
					// ensure unique widget ids
					c.widgets = $.grep(c.widgets, function(v, k){
						return $.inArray(v, c.widgets) === k;
					});
					name = c.widgets || [];
					len = name.length;
					// build widget array & add priority as needed
					for (indx = 0; indx < len; indx++) {
						wd = ts.getWidgetById(name[indx]);
						if (wd && wd.id) {
							// set priority to 10 if not defined
							if (!wd.priority) { wd.priority = 10; }
							widgets[indx] = wd;
						}
					}
					// sort widgets by priority
					widgets.sort(function(a, b){
						return a.priority < b.priority ? -1 : a.priority === b.priority ? 0 : 1;
					});
					// add/update selected widgets
					len = widgets.length;
					for (indx = 0; indx < len; indx++) {
						if (widgets[indx]) {
							if ( init || !( c.widgetInit[ widgets[indx].id ] ) ) {
								// set init flag first to prevent calling init more than once (e.g. pager)
								c.widgetInit[ widgets[indx].id ] = true;
								if (table.hasInitialized) {
									// don't reapply widget options on tablesorter init
									ts.applyWidgetOptions( table, c );
								}
								if ( 'init' in widgets[indx] ) {
									if (c.debug) { time2 = new Date(); }
									widgets[indx].init(table, widgets[indx], c, wo);
									if (c.debug) { ts.benchmark('Initializing ' + widgets[indx].id + ' widget', time2); }
								}
							}
							if ( !init && 'format' in widgets[indx] ) {
								if (c.debug) { time2 = new Date(); }
								widgets[indx].format(table, c, wo, false);
								if (c.debug) { ts.benchmark( ( init ? 'Initializing ' : 'Applying ' ) + widgets[indx].id + ' widget', time2); }
							}
						}
					}
					// callback executed on init only
					if (!init && typeof callback === 'function') {
						callback(table);
					}
				}
				setTimeout(function(){
					table.isApplyingWidgets = false;
					$.data(table, 'lastWidgetApplication', new Date());
				}, 0);
				if (c.debug) {
					w = c.widgets.length;
					benchmark('Completed ' + (init === true ? 'initializing ' : 'applying ') + w + ' widget' + (w !== 1 ? 's' : ''), time);
				}
			};

			ts.removeWidget = function(table, name, refreshing){
				table = $(table)[0];
				var i, widget, indx, len,
					c = table.config;
				// if name === true, add all widgets from $.tablesorter.widgets
				if (name === true) {
					name = [];
					len = ts.widgets.length;
					for (indx = 0; indx < len; indx++) {
						widget = ts.widgets[indx];
						if (widget && widget.id) {
							name.push( widget.id );
						}
					}
				} else {
					// name can be either an array of widgets names,
					// or a space/comma separated list of widget names
					name = ( $.isArray(name) ? name.join(',') : name || '' ).toLowerCase().split( /[\s,]+/ );
				}
				len = name.length;
				for (i = 0; i < len; i++) {
					widget = ts.getWidgetById(name[i]);
					indx = $.inArray( name[i], c.widgets );
					if ( widget && 'remove' in widget ) {
						if (c.debug && indx >= 0) { log( 'Removing "' + name[i] + '" widget' ); }
						widget.remove(table, c, c.widgetOptions, refreshing);
						c.widgetInit[ name[i] ] = false;
					}
					// don't remove the widget from config.widget if refreshing
					if (indx >= 0 && refreshing !== true) {
						c.widgets.splice( indx, 1 );
					}
				}
			};

			ts.refreshWidgets = function(table, doAll, dontapply) {
				table = $(table)[0]; // see issue #243
				var indx,
					c = table.config,
					cw = c.widgets,
					widgets = ts.widgets,
					len = widgets.length,
					list = [],
					callback = function(table){
						$(table).trigger('refreshComplete');
					};
				// remove widgets not defined in config.widgets, unless doAll is true
				for (indx = 0; indx < len; indx++) {
					if (widgets[indx] && widgets[indx].id && (doAll || $.inArray( widgets[indx].id, cw ) < 0)) {
						list.push( widgets[indx].id );
					}
				}
				ts.removeWidget( table, list.join(','), true );
				if (dontapply !== true) {
					// call widget init if
					ts.applyWidget(table, doAll || false, callback );
					if (doAll) {
						// apply widget format
						ts.applyWidget(table, false, callback);
					}
				} else {
					callback(table);
				}
			};

			ts.getColumnText = function( table, column, callback ) {
				table = $( table )[0];
				var tbodyIndex, rowIndex, cache, row, tbodyLen, rowLen, raw, parsed, $cell, result,
					hasCallback = typeof callback === 'function',
					allColumns = column === 'all',
					data = { raw : [], parsed: [], $cell: [] },
					c = table.config;
				if ( !isEmptyObject( c ) ) {
					tbodyLen = c.$tbodies.length;
					for ( tbodyIndex = 0; tbodyIndex < tbodyLen; tbodyIndex++ ) {
						cache = c.cache[ tbodyIndex ].normalized;
						rowLen = cache.length;
						for ( rowIndex = 0; rowIndex < rowLen; rowIndex++ ) {
							result = true;
							row =	cache[ rowIndex ];
							parsed = ( allColumns ) ? row.slice(0, c.columns) : row[ column ];
							row = row[ c.columns ];
							raw = ( allColumns ) ? row.raw : row.raw[ column ];
							$cell = ( allColumns ) ? row.$row.children() : row.$row.children().eq( column );
							if ( hasCallback ) {
								result = callback({
									tbodyIndex: tbodyIndex,
									rowIndex: rowIndex,
									parsed: parsed,
									raw: raw,
									$row: row.$row,
									$cell: $cell
								});
							}
							if ( result !== false ) {
								data.parsed.push( parsed );
								data.raw.push( raw );
								data.$cell.push( $cell );
							}
						}
					}
					// return everything
					return data;
				}
			};

			// get sorter, string, empty, etc options for each column from
			// jQuery data, metadata, header option or header class name ('sorter-false')
			// priority = jQuery data > meta > headers option > header class name
			ts.getData = function(h, ch, key) {
				var val = '', $h = $(h), m, cl;
				if (!$h.length) { return ''; }
				m = $.metadata ? $h.metadata() : false;
				cl = ' ' + ($h.attr('class') || '');
				if (typeof $h.data(key) !== 'undefined' || typeof $h.data(key.toLowerCase()) !== 'undefined'){
					// 'data-lockedOrder' is assigned to 'lockedorder'; but 'data-locked-order' is assigned to 'lockedOrder'
					// 'data-sort-initial-order' is assigned to 'sortInitialOrder'
					val += $h.data(key) || $h.data(key.toLowerCase());
				} else if (m && typeof m[key] !== 'undefined') {
					val += m[key];
				} else if (ch && typeof ch[key] !== 'undefined') {
					val += ch[key];
				} else if (cl !== ' ' && cl.match(' ' + key + '-')) {
					// include sorter class name 'sorter-text', etc; now works with 'sorter-my-custom-parser'
					val = cl.match( new RegExp('\\s' + key + '-([\\w-]+)') )[1] || '';
				}
				return $.trim(val);
			};

			ts.formatFloat = function(s, table) {
				if (typeof s !== 'string' || s === '') { return s; }
				// allow using formatFloat without a table; defaults to US number format
				var i,
					t = table && table.config ? table.config.usNumberFormat !== false :
						typeof table !== 'undefined' ? table : true;
				if (t) {
					// US Format - 1,234,567.89 -> 1234567.89
					s = s.replace(/,/g,'');
				} else {
					// German Format = 1.234.567,89 -> 1234567.89
					// French Format = 1 234 567,89 -> 1234567.89
					s = s.replace(/[\s|\.]/g,'').replace(/,/g,'.');
				}
				if(/^\s*\([.\d]+\)/.test(s)) {
					// make (#) into a negative number -> (10) = -10
					s = s.replace(/^\s*\(([.\d]+)\)/, '-$1');
				}
				i = parseFloat(s);
				// return the text instead of zero
				return isNaN(i) ? $.trim(s) : i;
			};

			ts.isDigit = function(s) {
				// replace all unwanted chars and match
				return isNaN(s) ? (/^[\-+(]?\d+[)]?$/).test(s.toString().replace(/[,.'"\s]/g, '')) : s !== '';
			};

		}()
	});

	// make shortcut
	var ts = $.tablesorter;

	// extend plugin scope
	$.fn.extend({
		tablesorter: ts.construct
	});

	// add default parsers
	ts.addParser({
		id: 'no-parser',
		is: function() {
			return false;
		},
		format: function() {
			return '';
		},
		type: 'text'
	});

	ts.addParser({
		id: 'text',
		is: function() {
			return true;
		},
		format: function(s, table) {
			var c = table.config;
			if (s) {
				s = $.trim( c.ignoreCase ? s.toLocaleLowerCase() : s );
				s = c.sortLocaleCompare ? ts.replaceAccents(s) : s;
			}
			return s;
		},
		type: 'text'
	});

	ts.addParser({
		id: 'digit',
		is: function(s) {
			return ts.isDigit(s);
		},
		format: function(s, table) {
			var n = ts.formatFloat((s || '').replace(/[^\w,. \-()]/g, ''), table);
			return s && typeof n === 'number' ? n : s ? $.trim( s && table.config.ignoreCase ? s.toLocaleLowerCase() : s ) : s;
		},
		type: 'numeric'
	});

	ts.addParser({
		id: 'currency',
		is: function(s) {
			return (/^\(?\d+[\u00a3$\u20ac\u00a4\u00a5\u00a2?.]|[\u00a3$\u20ac\u00a4\u00a5\u00a2?.]\d+\)?$/).test((s || '').replace(/[+\-,. ]/g,'')); // £$€¤¥¢
		},
		format: function(s, table) {
			var n = ts.formatFloat((s || '').replace(/[^\w,. \-()]/g, ''), table);
			return s && typeof n === 'number' ? n : s ? $.trim( s && table.config.ignoreCase ? s.toLocaleLowerCase() : s ) : s;
		},
		type: 'numeric'
	});

	ts.addParser({
		id: 'url',
		is: function(s) {
			return (/^(https?|ftp|file):\/\//).test(s);
		},
		format: function(s) {
			return s ? $.trim(s.replace(/(https?|ftp|file):\/\//, '')) : s;
		},
		parsed : true, // filter widget flag
		type: 'text'
	});

	ts.addParser({
		id: 'isoDate',
		is: function(s) {
			return (/^\d{4}[\/\-]\d{1,2}[\/\-]\d{1,2}/).test(s);
		},
		format: function(s, table) {
			var date = s ? new Date( s.replace(/-/g, '/') ) : s;
			return date instanceof Date && isFinite(date) ? date.getTime() : s;
		},
		type: 'numeric'
	});

	ts.addParser({
		id: 'percent',
		is: function(s) {
			return (/(\d\s*?%|%\s*?\d)/).test(s) && s.length < 15;
		},
		format: function(s, table) {
			return s ? ts.formatFloat(s.replace(/%/g, ''), table) : s;
		},
		type: 'numeric'
	});

	// added image parser to core v2.17.9
	ts.addParser({
		id: 'image',
		is: function(s, table, node, $node){
			return $node.find('img').length > 0;
		},
		format: function(s, table, cell) {
			return $(cell).find('img').attr(table.config.imgAttr || 'alt') || s;
		},
		parsed : true, // filter widget flag
		type: 'text'
	});

	ts.addParser({
		id: 'usLongDate',
		is: function(s) {
			// two digit years are not allowed cross-browser
			// Jan 01, 2013 12:34:56 PM or 01 Jan 2013
			return (/^[A-Z]{3,10}\.?\s+\d{1,2},?\s+(\d{4})(\s+\d{1,2}:\d{2}(:\d{2})?(\s+[AP]M)?)?$/i).test(s) || (/^\d{1,2}\s+[A-Z]{3,10}\s+\d{4}/i).test(s);
		},
		format: function(s, table) {
			var date = s ? new Date( s.replace(/(\S)([AP]M)$/i, '$1 $2') ) : s;
			return date instanceof Date && isFinite(date) ? date.getTime() : s;
		},
		type: 'numeric'
	});

	ts.addParser({
		id: 'shortDate', // 'mmddyyyy', 'ddmmyyyy' or 'yyyymmdd'
		is: function(s) {
			// testing for ##-##-#### or ####-##-##, so it's not perfect; time can be included
			return (/(^\d{1,2}[\/\s]\d{1,2}[\/\s]\d{4})|(^\d{4}[\/\s]\d{1,2}[\/\s]\d{1,2})/).test((s || '').replace(/\s+/g,' ').replace(/[\-.,]/g, '/'));
		},
		format: function(s, table, cell, cellIndex) {
			if (s) {
				var date, d,
					c = table.config,
					ci = c.$headerIndexed[ cellIndex ],
					format = ci.length && ci[0].dateFormat || ts.getData( ci, ts.getColumnData( table, c.headers, cellIndex ), 'dateFormat') || c.dateFormat;
				d = s.replace(/\s+/g, ' ').replace(/[\-.,]/g, '/'); // escaped - because JSHint in Firefox was showing it as an error
				if (format === 'mmddyyyy') {
					d = d.replace(/(\d{1,2})[\/\s](\d{1,2})[\/\s](\d{4})/, '$3/$1/$2');
				} else if (format === 'ddmmyyyy') {
					d = d.replace(/(\d{1,2})[\/\s](\d{1,2})[\/\s](\d{4})/, '$3/$2/$1');
				} else if (format === 'yyyymmdd') {
					d = d.replace(/(\d{4})[\/\s](\d{1,2})[\/\s](\d{1,2})/, '$1/$2/$3');
				}
				date = new Date(d);
				return date instanceof Date && isFinite(date) ? date.getTime() : s;
			}
			return s;
		},
		type: 'numeric'
	});

	ts.addParser({
		id: 'time',
		is: function(s) {
			return (/^(([0-2]?\d:[0-5]\d)|([0-1]?\d:[0-5]\d\s?([AP]M)))$/i).test(s);
		},
		format: function(s, table) {
			var date = s ? new Date( '2000/01/01 ' + s.replace(/(\S)([AP]M)$/i, '$1 $2') ) : s;
			return date instanceof Date && isFinite(date) ? date.getTime() : s;
		},
		type: 'numeric'
	});

	ts.addParser({
		id: 'metadata',
		is: function() {
			return false;
		},
		format: function(s, table, cell) {
			var c = table.config,
			p = (!c.parserMetadataName) ? 'sortValue' : c.parserMetadataName;
			return $(cell).metadata()[p];
		},
		type: 'numeric'
	});

	// add default widgets
	ts.addWidget({
		id: 'zebra',
		priority: 90,
		format: function(table, c, wo) {
			var $tv, $tr, row, even, time, k, i, len,
				child = new RegExp(c.cssChildRow, 'i'),
				b = c.$tbodies.add( $( c.namespace + '_extra_table' ).children( 'tbody' ) );
			if (c.debug) {
				time = new Date();
			}
			for (k = 0; k < b.length; k++ ) {
				// loop through the visible rows
				row = 0;
				$tv = b.eq( k ).children( 'tr:visible' ).not( c.selectorRemove );
				len = $tv.length;
				for ( i = 0; i < len; i++ ) {
					$tr = $tv.eq( i );
					// style child rows the same way the parent row was styled
					if ( !child.test( $tr[0].className ) ) { row++; }
					even = ( row % 2 === 0 );
					$tr
						.removeClass( wo.zebra[ even ? 1 : 0 ] )
						.addClass( wo.zebra[ even ? 0 : 1 ] );
				}
			}
		},
		remove: function(table, c, wo, refreshing){
			if (refreshing) { return; }
			var k, $tb,
				b = c.$tbodies,
				rmv = (wo.zebra || [ 'even', 'odd' ]).join(' ');
			for (k = 0; k < b.length; k++ ){
				$tb = ts.processTbody(table, b.eq(k), true); // remove tbody
				$tb.children().removeClass(rmv);
				ts.processTbody(table, $tb, false); // restore tbody
			}
		}
	});

})(jQuery);

/*! Widget: storage - updated 3/26/2015 (v2.21.3) */
;(function ($, window, document) {
'use strict';

var ts = $.tablesorter || {};
// *** Store data in local storage, with a cookie fallback ***
/* IE7 needs JSON library for JSON.stringify - (http://caniuse.com/#search=json)
   if you need it, then include https://github.com/douglascrockford/JSON-js

   $.parseJSON is not available is jQuery versions older than 1.4.1, using older
   versions will only allow storing information for one page at a time

   // *** Save data (JSON format only) ***
   // val must be valid JSON... use http://jsonlint.com/ to ensure it is valid
   var val = { "mywidget" : "data1" }; // valid JSON uses double quotes
   // $.tablesorter.storage(table, key, val);
   $.tablesorter.storage(table, 'tablesorter-mywidget', val);

   // *** Get data: $.tablesorter.storage(table, key); ***
   v = $.tablesorter.storage(table, 'tablesorter-mywidget');
   // val may be empty, so also check for your data
   val = (v && v.hasOwnProperty('mywidget')) ? v.mywidget : '';
   alert(val); // "data1" if saved, or "" if not
*/
ts.storage = function(table, key, value, options) {
	table = $(table)[0];
	var cookieIndex, cookies, date,
		hasStorage = false,
		values = {},
		c = table.config,
		wo = c && c.widgetOptions,
		storageType = ( options && options.useSessionStorage ) || ( wo && wo.storage_useSessionStorage ) ?
			'sessionStorage' : 'localStorage',
		$table = $(table),
		// id from (1) options ID, (2) table "data-table-group" attribute, (3) widgetOptions.storage_tableId,
		// (4) table ID, then (5) table index
		id = options && options.id ||
			$table.attr( options && options.group || wo && wo.storage_group || 'data-table-group') ||
			wo && wo.storage_tableId || table.id || $('.tablesorter').index( $table ),
		// url from (1) options url, (2) table "data-table-page" attribute, (3) widgetOptions.storage_fixedUrl,
		// (4) table.config.fixedUrl (deprecated), then (5) window location path
		url = options && options.url ||
			$table.attr(options && options.page || wo && wo.storage_page || 'data-table-page') ||
			wo && wo.storage_fixedUrl || c && c.fixedUrl || window.location.pathname;
	// https://gist.github.com/paulirish/5558557
	if (storageType in window) {
		try {
			window[storageType].setItem('_tmptest', 'temp');
			hasStorage = true;
			window[storageType].removeItem('_tmptest');
		} catch(error) {
			if (c && c.debug) {
				ts.log( storageType + ' is not supported in this browser' );
			}
		}
	}
	// *** get value ***
	if ($.parseJSON) {
		if (hasStorage) {
			values = $.parseJSON( window[storageType][key] || 'null' ) || {};
		} else {
			// old browser, using cookies
			cookies = document.cookie.split(/[;\s|=]/);
			// add one to get from the key to the value
			cookieIndex = $.inArray(key, cookies) + 1;
			values = (cookieIndex !== 0) ? $.parseJSON(cookies[cookieIndex] || 'null') || {} : {};
		}
	}
	// allow value to be an empty string too
	if ((value || value === '') && window.JSON && JSON.hasOwnProperty('stringify')) {
		// add unique identifiers = url pathname > table ID/index on page > data
		if (!values[url]) {
			values[url] = {};
		}
		values[url][id] = value;
		// *** set value ***
		if (hasStorage) {
			window[storageType][key] = JSON.stringify(values);
		} else {
			date = new Date();
			date.setTime(date.getTime() + (31536e+6)); // 365 days
			document.cookie = key + '=' + (JSON.stringify(values)).replace(/\"/g,'\"') + '; expires=' + date.toGMTString() + '; path=/';
		}
	} else {
		return values && values[url] ? values[url][id] : '';
	}
};

})(jQuery, window, document);

/*! Widget: uitheme - updated 3/26/2015 (v2.21.3) */
;(function ($) {
'use strict';
var ts = $.tablesorter || {};

ts.themes = {
	'bootstrap' : {
		table        : 'table table-bordered table-striped',
		caption      : 'caption',
		// header class names
		header       : 'bootstrap-header', // give the header a gradient background (theme.bootstrap_2.css)
		sortNone     : '',
		sortAsc      : '',
		sortDesc     : '',
		active       : '', // applied when column is sorted
		hover        : '', // custom css required - a defined bootstrap style may not override other classes
		// icon class names
		icons        : '', // add "icon-white" to make them white; this icon class is added to the <i> in the header
		iconSortNone : 'bootstrap-icon-unsorted', // class name added to icon when column is not sorted
		iconSortAsc  : 'icon-chevron-up glyphicon glyphicon-chevron-up', // class name added to icon when column has ascending sort
		iconSortDesc : 'icon-chevron-down glyphicon glyphicon-chevron-down', // class name added to icon when column has descending sort
		filterRow    : '', // filter row class
		footerRow    : '',
		footerCells  : '',
		even         : '', // even row zebra striping
		odd          : ''  // odd row zebra striping
	},
	'jui' : {
		table        : 'ui-widget ui-widget-content ui-corner-all', // table classes
		caption      : 'ui-widget-content',
		// header class names
		header       : 'ui-widget-header ui-corner-all ui-state-default', // header classes
		sortNone     : '',
		sortAsc      : '',
		sortDesc     : '',
		active       : 'ui-state-active', // applied when column is sorted
		hover        : 'ui-state-hover',  // hover class
		// icon class names
		icons        : 'ui-icon', // icon class added to the <i> in the header
		iconSortNone : 'ui-icon-carat-2-n-s', // class name added to icon when column is not sorted
		iconSortAsc  : 'ui-icon-carat-1-n', // class name added to icon when column has ascending sort
		iconSortDesc : 'ui-icon-carat-1-s', // class name added to icon when column has descending sort
		filterRow    : '',
		footerRow    : '',
		footerCells  : '',
		even         : 'ui-widget-content', // even row zebra striping
		odd          : 'ui-state-default'   // odd row zebra striping
	}
};

$.extend(ts.css, {
	wrapper : 'tablesorter-wrapper' // ui theme & resizable
});

ts.addWidget({
	id: "uitheme",
	priority: 10,
	format: function(table, c, wo) {
		var i, hdr, icon, time, $header, $icon, $tfoot, $h, oldtheme, oldremove, oldIconRmv, hasOldTheme,
			themesAll = ts.themes,
			$table = c.$table.add( $( c.namespace + '_extra_table' ) ),
			$headers = c.$headers.add( $( c.namespace + '_extra_headers' ) ),
			theme = c.theme || 'jui',
			themes = themesAll[theme] || {},
			remove = $.trim( [ themes.sortNone, themes.sortDesc, themes.sortAsc, themes.active ].join( ' ' ) ),
			iconRmv = $.trim( [ themes.iconSortNone, themes.iconSortDesc, themes.iconSortAsc ].join( ' ' ) );
		if (c.debug) { time = new Date(); }
		// initialization code - run once
		if (!$table.hasClass('tablesorter-' + theme) || c.theme !== c.appliedTheme || !wo.uitheme_applied) {
			wo.uitheme_applied = true;
			oldtheme = themesAll[c.appliedTheme] || {};
			hasOldTheme = !$.isEmptyObject(oldtheme);
			oldremove =  hasOldTheme ? [ oldtheme.sortNone, oldtheme.sortDesc, oldtheme.sortAsc, oldtheme.active ].join( ' ' ) : '';
			oldIconRmv = hasOldTheme ? [ oldtheme.iconSortNone, oldtheme.iconSortDesc, oldtheme.iconSortAsc ].join( ' ' ) : '';
			if (hasOldTheme) {
				wo.zebra[0] = $.trim( ' ' + wo.zebra[0].replace(' ' + oldtheme.even, '') );
				wo.zebra[1] = $.trim( ' ' + wo.zebra[1].replace(' ' + oldtheme.odd, '') );
				c.$tbodies.children().removeClass( [oldtheme.even, oldtheme.odd].join(' ') );
			}
			// update zebra stripes
			if (themes.even) { wo.zebra[0] += ' ' + themes.even; }
			if (themes.odd) { wo.zebra[1] += ' ' + themes.odd; }
			// add caption style
			$table.children('caption')
				.removeClass(oldtheme.caption || '')
				.addClass(themes.caption);
			// add table/footer class names
			$tfoot = $table
				// remove other selected themes
				.removeClass( (c.appliedTheme ? 'tablesorter-' + (c.appliedTheme || '') : '') + ' ' + (oldtheme.table || '') )
				.addClass('tablesorter-' + theme + ' ' + (themes.table || '')) // add theme widget class name
				.children('tfoot');
			c.appliedTheme = c.theme;

			if ($tfoot.length) {
				$tfoot
					// if oldtheme.footerRow or oldtheme.footerCells are undefined, all class names are removed
					.children('tr').removeClass(oldtheme.footerRow || '').addClass(themes.footerRow)
					.children('th, td').removeClass(oldtheme.footerCells || '').addClass(themes.footerCells);
			}
			// update header classes
			$headers
				.removeClass( (hasOldTheme ? [oldtheme.header, oldtheme.hover, oldremove].join(' ') : '') || '' )
				.addClass(themes.header)
				.not('.sorter-false')
				.unbind('mouseenter.tsuitheme mouseleave.tsuitheme')
				.bind('mouseenter.tsuitheme mouseleave.tsuitheme', function(event) {
					// toggleClass with switch added in jQuery 1.3
					$(this)[ event.type === 'mouseenter' ? 'addClass' : 'removeClass' ](themes.hover || '');
				});

			$headers.each(function(){
				var $this = $(this);
				if (!$this.find('.' + ts.css.wrapper).length) {
					// Firefox needs this inner div to position the icon & resizer correctly
					$this.wrapInner('<div class="' + ts.css.wrapper + '" style="position:relative;height:100%;width:100%"></div>');
				}
			});
			if (c.cssIcon) {
				// if c.cssIcon is '', then no <i> is added to the header
				$headers
					.find('.' + ts.css.icon)
					.removeClass(hasOldTheme ? [oldtheme.icons, oldIconRmv].join(' ') : '')
					.addClass(themes.icons || '');
			}
			if ($table.hasClass('hasFilters')) {
				$table.children('thead').children('.' + ts.css.filterRow)
					.removeClass(hasOldTheme ? oldtheme.filterRow || '' : '')
					.addClass(themes.filterRow || '');
			}
		}
		for (i = 0; i < c.columns; i++) {
			$header = c.$headers
				.add($(c.namespace + '_extra_headers'))
				.not('.sorter-false')
				.filter('[data-column="' + i + '"]');
			$icon = (ts.css.icon) ? $header.find('.' + ts.css.icon) : $();
			$h = $headers.not('.sorter-false').filter('[data-column="' + i + '"]:last');
			if ($h.length) {
				$header.removeClass(remove);
				$icon.removeClass(iconRmv);
				if ($h[0].sortDisabled) {
					// no sort arrows for disabled columns!
					$icon.removeClass(themes.icons || '');
				} else {
					hdr = themes.sortNone;
					icon = themes.iconSortNone;
					if ($h.hasClass(ts.css.sortAsc)) {
						hdr = [themes.sortAsc, themes.active].join(' ');
						icon = themes.iconSortAsc;
					} else if ($h.hasClass(ts.css.sortDesc)) {
						hdr = [themes.sortDesc, themes.active].join(' ');
						icon = themes.iconSortDesc;
					}
					$header.addClass(hdr);
					$icon.addClass(icon || '');
				}
			}
		}
		if (c.debug) {
			ts.benchmark("Applying " + theme + " theme", time);
		}
	},
	remove: function(table, c, wo, refreshing) {
		if (!wo.uitheme_applied) { return; }
		var $table = c.$table,
			theme = c.appliedTheme || 'jui',
			themes = ts.themes[ theme ] || ts.themes.jui,
			$headers = $table.children('thead').children(),
			remove = themes.sortNone + ' ' + themes.sortDesc + ' ' + themes.sortAsc,
			iconRmv = themes.iconSortNone + ' ' + themes.iconSortDesc + ' ' + themes.iconSortAsc;
		$table.removeClass('tablesorter-' + theme + ' ' + themes.table);
		wo.uitheme_applied = false;
		if (refreshing) { return; }
		$table.find(ts.css.header).removeClass(themes.header);
		$headers
			.unbind('mouseenter.tsuitheme mouseleave.tsuitheme') // remove hover
			.removeClass(themes.hover + ' ' + remove + ' ' + themes.active)
			.filter('.' + ts.css.filterRow)
			.removeClass(themes.filterRow);
		$headers.find('.' + ts.css.icon).removeClass(themes.icons + ' ' + iconRmv);
	}
});

})(jQuery);

/*! Widget: columns */
;(function ($) {
'use strict';
var ts = $.tablesorter || {};

ts.addWidget({
	id: "columns",
	priority: 30,
	options : {
		columns : [ "primary", "secondary", "tertiary" ]
	},
	format: function(table, c, wo) {
		var $tbody, tbodyIndex, $rows, rows, $row, $cells, remove, indx,
			$table = c.$table,
			$tbodies = c.$tbodies,
			sortList = c.sortList,
			len = sortList.length,
			// removed c.widgetColumns support
			css = wo && wo.columns || [ "primary", "secondary", "tertiary" ],
			last = css.length - 1;
			remove = css.join(' ');
		// check if there is a sort (on initialization there may not be one)
		for (tbodyIndex = 0; tbodyIndex < $tbodies.length; tbodyIndex++ ) {
			$tbody = ts.processTbody(table, $tbodies.eq(tbodyIndex), true); // detach tbody
			$rows = $tbody.children('tr');
			// loop through the visible rows
			$rows.each(function() {
				$row = $(this);
				if (this.style.display !== 'none') {
					// remove all columns class names
					$cells = $row.children().removeClass(remove);
					// add appropriate column class names
					if (sortList && sortList[0]) {
						// primary sort column class
						$cells.eq(sortList[0][0]).addClass(css[0]);
						if (len > 1) {
							for (indx = 1; indx < len; indx++) {
								// secondary, tertiary, etc sort column classes
								$cells.eq(sortList[indx][0]).addClass( css[indx] || css[last] );
							}
						}
					}
				}
			});
			ts.processTbody(table, $tbody, false);
		}
		// add classes to thead and tfoot
		rows = wo.columns_thead !== false ? ['thead tr'] : [];
		if (wo.columns_tfoot !== false) {
			rows.push('tfoot tr');
		}
		if (rows.length) {
			$rows = $table.find( rows.join(',') ).children().removeClass(remove);
			if (len) {
				for (indx = 0; indx < len; indx++) {
					// add primary. secondary, tertiary, etc sort column classes
					$rows.filter('[data-column="' + sortList[indx][0] + '"]').addClass(css[indx] || css[last]);
				}
			}
		}
	},
	remove: function(table, c, wo) {
		var tbodyIndex, $tbody,
			$tbodies = c.$tbodies,
			remove = (wo.columns || [ "primary", "secondary", "tertiary" ]).join(' ');
		c.$headers.removeClass(remove);
		c.$table.children('tfoot').children('tr').children('th, td').removeClass(remove);
		for (tbodyIndex = 0; tbodyIndex < $tbodies.length; tbodyIndex++ ) {
			$tbody = ts.processTbody(table, $tbodies.eq(tbodyIndex), true); // remove tbody
			$tbody.children('tr').each(function() {
				$(this).children().removeClass(remove);
			});
			ts.processTbody(table, $tbody, false); // restore tbody
		}
	}
});

})(jQuery);

/*! Widget: filter - updated 5/17/2015 (v2.22.1) *//*
 * Requires tablesorter v2.8+ and jQuery 1.7+
 * by Rob Garrison
 */
;( function ( $ ) {
'use strict';
var ts = $.tablesorter || {},
	tscss = ts.css;

$.extend( tscss, {
	filterRow      : 'tablesorter-filter-row',
	filter         : 'tablesorter-filter',
	filterDisabled : 'disabled',
	filterRowHide  : 'hideme'
});

ts.addWidget({
	id: 'filter',
	priority: 50,
	options : {
		filter_childRows     : false, // if true, filter includes child row content in the search
		filter_childByColumn : false, // ( filter_childRows must be true ) if true = search child rows by column; false = search all child row text grouped
		filter_columnFilters : true,  // if true, a filter will be added to the top of each table column
		filter_columnAnyMatch: true,  // if true, allows using '#:{query}' in AnyMatch searches ( column:query )
		filter_cellFilter    : '',    // css class name added to the filter cell ( string or array )
		filter_cssFilter     : '',    // css class name added to the filter row & each input in the row ( tablesorter-filter is ALWAYS added )
		filter_defaultFilter : {},    // add a default column filter type '~{query}' to make fuzzy searches default; '{q1} AND {q2}' to make all searches use a logical AND.
		filter_excludeFilter : {},    // filters to exclude, per column
		filter_external      : '',    // jQuery selector string ( or jQuery object ) of external filters
		filter_filteredRow   : 'filtered', // class added to filtered rows; needed by pager plugin
		filter_formatter     : null,  // add custom filter elements to the filter row
		filter_functions     : null,  // add custom filter functions using this option
		filter_hideEmpty     : true,  // hide filter row when table is empty
		filter_hideFilters   : false, // collapse filter row when mouse leaves the area
		filter_ignoreCase    : true,  // if true, make all searches case-insensitive
		filter_liveSearch    : true,  // if true, search column content while the user types ( with a delay )
		filter_onlyAvail     : 'filter-onlyAvail', // a header with a select dropdown & this class name will only show available ( visible ) options within the drop down
		filter_placeholder   : { search : '', select : '' }, // default placeholder text ( overridden by any header 'data-placeholder' setting )
		filter_reset         : null,  // jQuery selector string of an element used to reset the filters
		filter_saveFilters   : false, // Use the $.tablesorter.storage utility to save the most recent filters
		filter_searchDelay   : 300,   // typing delay in milliseconds before starting a search
		filter_searchFiltered: true,  // allow searching through already filtered rows in special circumstances; will speed up searching in large tables if true
		filter_selectSource  : null,  // include a function to return an array of values to be added to the column filter select
		filter_startsWith    : false, // if true, filter start from the beginning of the cell contents
		filter_useParsedData : false, // filter all data using parsed content
		filter_serversideFiltering : false, // if true, server-side filtering should be performed because client-side filtering will be disabled, but the ui and events will still be used.
		filter_defaultAttrib : 'data-value', // data attribute in the header cell that contains the default filter value
		filter_selectSourceSeparator : '|' // filter_selectSource array text left of the separator is added to the option value, right into the option text
	},
	format: function( table, c, wo ) {
		if ( !c.$table.hasClass( 'hasFilters' ) ) {
			ts.filter.init( table, c, wo );
		}
	},
	remove: function( table, c, wo, refreshing ) {
		var tbodyIndex, $tbody,
			$table = c.$table,
			$tbodies = c.$tbodies,
			events = 'addRows updateCell update updateRows updateComplete appendCache filterReset filterEnd search '
				.split( ' ' ).join( c.namespace + 'filter ' );
		$table
			.removeClass( 'hasFilters' )
			// add .tsfilter namespace to all BUT search
			.unbind( events.replace( /\s+/g, ' ' ) )
			// remove the filter row even if refreshing, because the column might have been moved
			.find( '.' + tscss.filterRow ).remove();
		if ( refreshing ) { return; }
		for ( tbodyIndex = 0; tbodyIndex < $tbodies.length; tbodyIndex++ ) {
			$tbody = ts.processTbody( table, $tbodies.eq( tbodyIndex ), true ); // remove tbody
			$tbody.children().removeClass( wo.filter_filteredRow ).show();
			ts.processTbody( table, $tbody, false ); // restore tbody
		}
		if ( wo.filter_reset ) {
			$( document ).undelegate( wo.filter_reset, 'click.tsfilter' );
		}
	}
});

ts.filter = {

	// regex used in filter 'check' functions - not for general use and not documented
	regex: {
		regex     : /^\/((?:\\\/|[^\/])+)\/([mig]{0,3})?$/, // regex to test for regex
		child     : /tablesorter-childRow/, // child row class name; this gets updated in the script
		filtered  : /filtered/, // filtered (hidden) row class name; updated in the script
		type      : /undefined|number/, // check type
		exact     : /(^[\"\'=]+)|([\"\'=]+$)/g, // exact match (allow '==')
		nondigit  : /[^\w,. \-()]/g, // replace non-digits (from digit & currency parser)
		operators : /[<>=]/g, // replace operators
		query     : '(q|query)' // replace filter queries
	},
		// function( c, data ) { }
		// c = table.config
		// data.$row = jQuery object of the row currently being processed
		// data.$cells = jQuery object of all cells within the current row
		// data.filters = array of filters for all columns ( some may be undefined )
		// data.filter = filter for the current column
		// data.iFilter = same as data.filter, except lowercase ( if wo.filter_ignoreCase is true )
		// data.exact = table cell text ( or parsed data if column parser enabled )
		// data.iExact = same as data.exact, except lowercase ( if wo.filter_ignoreCase is true )
		// data.cache = table cell text from cache, so it has been parsed ( & in all lower case if c.ignoreCase is true )
		// data.cacheArray = An array of parsed content from each table cell in the row being processed
		// data.index = column index; table = table element ( DOM )
		// data.parsed = array ( by column ) of boolean values ( from filter_useParsedData or 'filter-parsed' class )
	types: {
		// Look for regex
		regex: function( c, data ) {
			if ( ts.filter.regex.regex.test( data.filter ) ) {
				var matches,
					// cache regex per column for optimal speed
					regex = data.filter_regexCache[ data.index ] || ts.filter.regex.regex.exec( data.filter ),
					isRegex = regex instanceof RegExp;
				try {
					if ( !isRegex ) {
						// force case insensitive search if ignoreCase option set?
						// if ( c.ignoreCase && !regex[2] ) { regex[2] = 'i'; }
						data.filter_regexCache[ data.index ] = regex = new RegExp( regex[1], regex[2] );
					}
					matches = regex.test( data.exact );
				} catch ( error ) {
					matches = false;
				}
				return matches;
			}
			return null;
		},
		// Look for operators >, >=, < or <=
		operators: function( c, data ) {
			// ignore empty strings... because '' < 10 is true
			if ( /^[<>]=?/.test( data.iFilter ) && data.iExact !== '' ) {
				var cachedValue, result, txt,
					table = c.table,
					index = data.index,
					parsed = data.parsed[index],
					query = ts.formatFloat( data.iFilter.replace( ts.filter.regex.operators, '' ), table ),
					parser = c.parsers[index],
					savedSearch = query;
				// parse filter value in case we're comparing numbers ( dates )
				if ( parsed || parser.type === 'numeric' ) {
					txt = $.trim( '' + data.iFilter.replace( ts.filter.regex.operators, '' ) );
					result = ts.filter.parseFilter( c, txt, index, true );
					query = ( typeof result === 'number' && result !== '' && !isNaN( result ) ) ? result : query;
				}
				// iExact may be numeric - see issue #149;
				// check if cached is defined, because sometimes j goes out of range? ( numeric columns )
				if ( ( parsed || parser.type === 'numeric' ) && !isNaN( query ) &&
					typeof data.cache !== 'undefined' ) {
					cachedValue = data.cache;
				} else {
					txt = isNaN( data.iExact ) ? data.iExact.replace( ts.filter.regex.nondigit, '' ) : data.iExact;
					cachedValue = ts.formatFloat( txt, table );
				}
				if ( />/.test( data.iFilter ) ) {
					result = />=/.test( data.iFilter ) ? cachedValue >= query : cachedValue > query;
				} else if ( /</.test( data.iFilter ) ) {
					result = /<=/.test( data.iFilter ) ? cachedValue <= query : cachedValue < query;
				}
				// keep showing all rows if nothing follows the operator
				if ( !result && savedSearch === '' ) {
					result = true;
				}
				return result;
			}
			return null;
		},
		// Look for a not match
		notMatch: function( c, data ) {
			if ( /^\!/.test( data.iFilter ) ) {
				var indx,
					txt = data.iFilter.replace( '!', '' ),
					filter = ts.filter.parseFilter( c, txt, data.index, data.parsed[data.index] ) || '';
				if ( ts.filter.regex.exact.test( filter ) ) {
					// look for exact not matches - see #628
					filter = filter.replace( ts.filter.regex.exact, '' );
					return filter === '' ? true : $.trim( filter ) !== data.iExact;
				} else {
					indx = data.iExact.search( $.trim( filter ) );
					return filter === '' ? true : !( c.widgetOptions.filter_startsWith ? indx === 0 : indx >= 0 );
				}
			}
			return null;
		},
		// Look for quotes or equals to get an exact match; ignore type since iExact could be numeric
		exact: function( c, data ) {
			/*jshint eqeqeq:false */
			if ( ts.filter.regex.exact.test( data.iFilter ) ) {
				var txt = data.iFilter.replace( ts.filter.regex.exact, '' ),
					filter = ts.filter.parseFilter( c, txt, data.index, data.parsed[data.index] ) || '';
				return data.anyMatch ? $.inArray( filter, data.rowArray ) >= 0 : filter == data.iExact;
			}
			return null;
		},
		// Look for an AND or && operator ( logical and )
		and : function( c, data ) {
			if ( ts.filter.regex.andTest.test( data.filter ) ) {
				var index = data.index,
					parsed = data.parsed[index],
					query = data.iFilter.split( ts.filter.regex.andSplit ),
					result = data.iExact.search( $.trim( ts.filter.parseFilter( c, query[0], index, parsed ) ) ) >= 0,
					indx = query.length - 1;
				while ( result && indx ) {
					result = result &&
						data.iExact.search( $.trim( ts.filter.parseFilter( c, query[indx], index, parsed ) ) ) >= 0;
					indx--;
				}
				return result;
			}
			return null;
		},
		// Look for a range ( using ' to ' or ' - ' ) - see issue #166; thanks matzhu!
		range : function( c, data ) {
			if ( ts.filter.regex.toTest.test( data.iFilter ) ) {
				var result, tmp, range1, range2,
					table = c.table,
					index = data.index,
					parsed = data.parsed[index],
					// make sure the dash is for a range and not indicating a negative number
					query = data.iFilter.split( ts.filter.regex.toSplit );

				tmp = query[0].replace( ts.filter.regex.nondigit, '' ) || '';
				range1 = ts.formatFloat( ts.filter.parseFilter( c, tmp, index, parsed ), table );
				tmp = query[1].replace( ts.filter.regex.nondigit, '' ) || '';
				range2 = ts.formatFloat( ts.filter.parseFilter( c, tmp, index, parsed ), table );
				// parse filter value in case we're comparing numbers ( dates )
				if ( parsed || c.parsers[index].type === 'numeric' ) {
					result = c.parsers[ index ].format( '' + query[0], table, c.$headers.eq( index ), index );
					range1 = ( result !== '' && !isNaN( result ) ) ? result : range1;
					result = c.parsers[ index ].format( '' + query[1], table, c.$headers.eq( index ), index );
					range2 = ( result !== '' && !isNaN( result ) ) ? result : range2;
				}
				if ( ( parsed || c.parsers[ index ].type === 'numeric' ) && !isNaN( range1 ) && !isNaN( range2 ) ) {
					result = data.cache;
				} else {
					tmp = isNaN( data.iExact ) ? data.iExact.replace( ts.filter.regex.nondigit, '' ) : data.iExact;
					result = ts.formatFloat( tmp, table );
				}
				if ( range1 > range2 ) {
					tmp = range1; range1 = range2; range2 = tmp; // swap
				}
				return ( result >= range1 && result <= range2 ) || ( range1 === '' || range2 === '' );
			}
			return null;
		},
		// Look for wild card: ? = single, * = multiple, or | = logical OR
		wild : function( c, data ) {
			if ( /[\?\*\|]/.test( data.iFilter ) || ts.filter.regex.orReplace.test( data.filter ) ) {
				var index = data.index,
					parsed = data.parsed[ index ],
					txt = data.iFilter.replace( ts.filter.regex.orReplace, '|' ),
					query = '' + ( ts.filter.parseFilter( c, txt, index, parsed ) || '' );
				// look for an exact match with the 'or' unless the 'filter-match' class is found
				if ( !c.$headerIndexed[ index ].hasClass( 'filter-match' ) && /\|/.test( query ) ) {
					// show all results while using filter match. Fixes #727
					if ( query[ query.length - 1 ] === '|' ) {
						query += '*';
					}
					query = data.anyMatch && $.isArray( data.rowArray ) ?
						'(' + query + ')' :
						'^(' + query + ')$';
				}
				// parsing the filter may not work properly when using wildcards =/
				return new RegExp( query.replace( /\?/g, '\\S{1}' ).replace( /\*/g, '\\S*' ) )
					.test( data.iExact );
			}
			return null;
		},
		// fuzzy text search; modified from https://github.com/mattyork/fuzzy ( MIT license )
		fuzzy: function( c, data ) {
			if ( /^~/.test( data.iFilter ) ) {
				var indx,
					patternIndx = 0,
					len = data.iExact.length,
					txt = data.iFilter.slice( 1 ),
					pattern = ts.filter.parseFilter( c, txt, data.index, data.parsed[data.index] ) || '';
				for ( indx = 0; indx < len; indx++ ) {
					if ( data.iExact[ indx ] === pattern[ patternIndx ] ) {
						patternIndx += 1;
					}
				}
				if ( patternIndx === pattern.length ) {
					return true;
				}
				return false;
			}
			return null;
		}
	},
	init: function( table, c, wo ) {
		// filter language options
		ts.language = $.extend( true, {}, {
			to  : 'to',
			or  : 'or',
			and : 'and'
		}, ts.language );

		var options, string, txt, $header, column, filters, val, fxn, noSelect,
			regex = ts.filter.regex;
		c.$table.addClass( 'hasFilters' );

		// define timers so using clearTimeout won't cause an undefined error
		wo.searchTimer = null;
		wo.filter_initTimer = null;
		wo.filter_formatterCount = 0;
		wo.filter_formatterInit = [];
		wo.filter_anyColumnSelector = '[data-column="all"],[data-column="any"]';
		wo.filter_multipleColumnSelector = '[data-column*="-"],[data-column*=","]';

		val = '\\{' + ts.filter.regex.query + '\\}';
		$.extend( regex, {
			child : new RegExp( c.cssChildRow ),
			filtered : new RegExp( wo.filter_filteredRow ),
			alreadyFiltered : new RegExp( '(\\s+(' + ts.language.or + '|-|' + ts.language.to + ')\\s+)', 'i' ),
			toTest : new RegExp( '\\s+(-|' + ts.language.to + ')\\s+', 'i' ),
			toSplit : new RegExp( '(?:\\s+(?:-|' + ts.language.to + ')\\s+)' ,'gi' ),
			andTest : new RegExp( '\\s+(' + ts.language.and + '|&&)\\s+', 'i' ),
			andSplit : new RegExp( '(?:\\s+(?:' + ts.language.and + '|&&)\\s+)', 'gi' ),
			orReplace : new RegExp( '\\s+(' + ts.language.or + ')\\s+', 'gi' ),
			iQuery : new RegExp( val, 'i' ),
			igQuery : new RegExp( val, 'ig' )
		});

		// don't build filter row if columnFilters is false or all columns are set to 'filter-false'
		// see issue #156
		val = c.$headers.filter( '.filter-false, .parser-false' ).length;
		if ( wo.filter_columnFilters !== false && val !== c.$headers.length ) {
			// build filter row
			ts.filter.buildRow( table, c, wo );
		}

		txt = 'addRows updateCell update updateRows updateComplete appendCache filterReset filterEnd search '
			.split( ' ' ).join( c.namespace + 'filter ' );
		c.$table.bind( txt, function( event, filter ) {
			val = wo.filter_hideEmpty &&
				$.isEmptyObject( c.cache ) &&
				!( c.delayInit && event.type === 'appendCache' );
			// hide filter row using the 'filtered' class name
			c.$table.find( '.' + tscss.filterRow ).toggleClass( wo.filter_filteredRow, val ); // fixes #450
			if ( !/(search|filter)/.test( event.type ) ) {
				event.stopPropagation();
				ts.filter.buildDefault( table, true );
			}
			if ( event.type === 'filterReset' ) {
				c.$table.find( '.' + tscss.filter ).add( wo.filter_$externalFilters ).val( '' );
				ts.filter.searching( table, [] );
			} else if ( event.type === 'filterEnd' ) {
				ts.filter.buildDefault( table, true );
			} else {
				// send false argument to force a new search; otherwise if the filter hasn't changed,
				// it will return
				filter = event.type === 'search' ? filter :
					event.type === 'updateComplete' ? c.$table.data( 'lastSearch' ) : '';
				if ( /(update|add)/.test( event.type ) && event.type !== 'updateComplete' ) {
					// force a new search since content has changed
					c.lastCombinedFilter = null;
					c.lastSearch = [];
				}
				// pass true ( skipFirst ) to prevent the tablesorter.setFilters function from skipping the first
				// input ensures all inputs are updated when a search is triggered on the table
				// $( 'table' ).trigger( 'search', [...] );
				ts.filter.searching( table, filter, true );
			}
			return false;
		});

		// reset button/link
		if ( wo.filter_reset ) {
			if ( wo.filter_reset instanceof $ ) {
				// reset contains a jQuery object, bind to it
				wo.filter_reset.click( function() {
					c.$table.trigger( 'filterReset' );
				});
			} else if ( $( wo.filter_reset ).length ) {
				// reset is a jQuery selector, use event delegation
				$( document )
					.undelegate( wo.filter_reset, 'click.tsfilter' )
					.delegate( wo.filter_reset, 'click.tsfilter', function() {
						// trigger a reset event, so other functions ( filter_formatter ) know when to reset
						c.$table.trigger( 'filterReset' );
					});
			}
		}
		if ( wo.filter_functions ) {
			for ( column = 0; column < c.columns; column++ ) {
				fxn = ts.getColumnData( table, wo.filter_functions, column );
				if ( fxn ) {
					// remove 'filter-select' from header otherwise the options added here are replaced with
					// all options
					$header = c.$headerIndexed[ column ].removeClass( 'filter-select' );
					// don't build select if 'filter-false' or 'parser-false' set
					noSelect = !( $header.hasClass( 'filter-false' ) || $header.hasClass( 'parser-false' ) );
					options = '';
					if ( fxn === true && noSelect ) {
						ts.filter.buildSelect( table, column );
					} else if ( typeof fxn === 'object' && noSelect ) {
						// add custom drop down list
						for ( string in fxn ) {
							if ( typeof string === 'string' ) {
								options += options === '' ?
									'<option value="">' +
										( $header.data( 'placeholder' ) ||
											$header.attr( 'data-placeholder' ) ||
											wo.filter_placeholder.select ||
											''
										) +
									'</option>' : '';
								val = string;
								txt = string;
								if ( string.indexOf( wo.filter_selectSourceSeparator ) >= 0 ) {
									val = string.split( wo.filter_selectSourceSeparator );
									txt = val[1];
									val = val[0];
								}
								options += '<option ' +
									( txt === val ? '' : 'data-function-name="' + string + '" ' ) +
									'value="' + val + '">' + txt + '</option>';
							}
						}
						c.$table
							.find( 'thead' )
							.find( 'select.' + tscss.filter + '[data-column="' + column + '"]' )
							.append( options );
						txt = wo.filter_selectSource;
						fxn = $.isFunction( txt ) ? true : ts.getColumnData( table, txt, column );
						if ( fxn ) {
							// updating so the extra options are appended
							ts.filter.buildSelect( c.table, column, '', true, $header.hasClass( wo.filter_onlyAvail ) );
						}
					}
				}
			}
		}
		// not really updating, but if the column has both the 'filter-select' class &
		// filter_functions set to true, it would append the same options twice.
		ts.filter.buildDefault( table, true );

		ts.filter.bindSearch( table, c.$table.find( '.' + tscss.filter ), true );
		if ( wo.filter_external ) {
			ts.filter.bindSearch( table, wo.filter_external );
		}

		if ( wo.filter_hideFilters ) {
			ts.filter.hideFilters( table, c );
		}

		// show processing icon
		if ( c.showProcessing ) {
			txt = 'filterStart filterEnd '.split( ' ' ).join( c.namespace + 'filter ' );
			c.$table
				.unbind( txt.replace( /\s+/g, ' ' ) )
				.bind( txt, function( event, columns ) {
				// only add processing to certain columns to all columns
				$header = ( columns ) ?
					c.$table
						.find( '.' + tscss.header )
						.filter( '[data-column]' )
						.filter( function() {
							return columns[ $( this ).data( 'column' ) ] !== '';
						}) : '';
				ts.isProcessing( table, event.type === 'filterStart', columns ? $header : '' );
			});
		}

		// set filtered rows count ( intially unfiltered )
		c.filteredRows = c.totalRows;

		// add default values
		txt = 'tablesorter-initialized pagerBeforeInitialized '.split( ' ' ).join( c.namespace + 'filter ' );
		c.$table
		.unbind( txt.replace( /\s+/g, ' ' ) )
		.bind( txt, function() {
			// redefine 'wo' as it does not update properly inside this callback
			var wo = this.config.widgetOptions;
			filters = ts.filter.setDefaults( table, c, wo ) || [];
			if ( filters.length ) {
				// prevent delayInit from triggering a cache build if filters are empty
				if ( !( c.delayInit && filters.join( '' ) === '' ) ) {
					ts.setFilters( table, filters, true );
				}
			}
			c.$table.trigger( 'filterFomatterUpdate' );
			// trigger init after setTimeout to prevent multiple filterStart/End/Init triggers
			setTimeout( function() {
				if ( !wo.filter_initialized ) {
					ts.filter.filterInitComplete( c );
				}
			}, 100 );
		});
		// if filter widget is added after pager has initialized; then set filter init flag
		if ( c.pager && c.pager.initialized && !wo.filter_initialized ) {
			c.$table.trigger( 'filterFomatterUpdate' );
			setTimeout( function() {
				ts.filter.filterInitComplete( c );
			}, 100 );
		}
	},
	// $cell parameter, but not the config, is passed to the filter_formatters,
	// so we have to work with it instead
	formatterUpdated: function( $cell, column ) {
		var wo = $cell.closest( 'table' )[0].config.widgetOptions;
		if ( !wo.filter_initialized ) {
			// add updates by column since this function
			// may be called numerous times before initialization
			wo.filter_formatterInit[ column ] = 1;
		}
	},
	filterInitComplete: function( c ) {
		var indx, len,
			wo = c.widgetOptions,
			count = 0,
			completed = function() {
				wo.filter_initialized = true;
				c.$table.trigger( 'filterInit', c );
				ts.filter.findRows( c.table, c.$table.data( 'lastSearch' ) || [] );
			};
		if ( $.isEmptyObject( wo.filter_formatter ) ) {
			completed();
		} else {
			len = wo.filter_formatterInit.length;
			for ( indx = 0; indx < len; indx++ ) {
				if ( wo.filter_formatterInit[ indx ] === 1 ) {
					count++;
				}
			}
			clearTimeout( wo.filter_initTimer );
			if ( !wo.filter_initialized && count === wo.filter_formatterCount ) {
				// filter widget initialized
				completed();
			} else if ( !wo.filter_initialized ) {
				// fall back in case a filter_formatter doesn't call
				// $.tablesorter.filter.formatterUpdated( $cell, column ), and the count is off
				wo.filter_initTimer = setTimeout( function() {
					completed();
				}, 500 );
			}
		}
	},

	setDefaults: function( table, c, wo ) {
		var isArray, saved, indx, col, $filters,
			// get current ( default ) filters
			filters = ts.getFilters( table ) || [];
		if ( wo.filter_saveFilters && ts.storage ) {
			saved = ts.storage( table, 'tablesorter-filters' ) || [];
			isArray = $.isArray( saved );
			// make sure we're not just getting an empty array
			if ( !( isArray && saved.join( '' ) === '' || !isArray ) ) {
				filters = saved;
			}
		}
		// if no filters saved, then check default settings
		if ( filters.join( '' ) === '' ) {
			// allow adding default setting to external filters
			$filters = c.$headers.add( wo.filter_$externalFilters )
				.filter( '[' + wo.filter_defaultAttrib + ']' );
			for ( indx = 0; indx <= c.columns; indx++ ) {
				// include data-column='all' external filters
				col = indx === c.columns ? 'all' : indx;
				filters[indx] = $filters
					.filter( '[data-column="' + col + '"]' )
					.attr( wo.filter_defaultAttrib ) || filters[indx] || '';
			}
		}
		c.$table.data( 'lastSearch', filters );
		return filters;
	},
	parseFilter: function( c, filter, column, parsed ) {
		return parsed ? c.parsers[column].format( filter, c.table, [], column ) : filter;
	},
	buildRow: function( table, c, wo ) {
		var col, column, $header, buildSelect, disabled, name, ffxn, tmp,
			// c.columns defined in computeThIndexes()
			cellFilter = wo.filter_cellFilter,
			columns = c.columns,
			arry = $.isArray( cellFilter ),
			buildFilter = '<tr role="row" class="' + tscss.filterRow + ' ' + c.cssIgnoreRow + '">';
		for ( column = 0; column < columns; column++ ) {
			buildFilter += '<td';
			if ( arry ) {
				buildFilter += ( cellFilter[ column ] ? ' class="' + cellFilter[ column ] + '"' : '' );
			} else {
				buildFilter += ( cellFilter !== '' ? ' class="' + cellFilter + '"' : '' );
			}
			buildFilter += '></td>';
		}
		c.$filters = $( buildFilter += '</tr>' )
			.appendTo( c.$table.children( 'thead' ).eq( 0 ) )
			.find( 'td' );
		// build each filter input
		for ( column = 0; column < columns; column++ ) {
			disabled = false;
			// assuming last cell of a column is the main column
			$header = c.$headerIndexed[ column ];
			ffxn = ts.getColumnData( table, wo.filter_functions, column );
			buildSelect = ( wo.filter_functions && ffxn && typeof ffxn !== 'function' ) ||
				$header.hasClass( 'filter-select' );
			// get data from jQuery data, metadata, headers option or header class name
			col = ts.getColumnData( table, c.headers, column );
			disabled = ts.getData( $header[0], col, 'filter' ) === 'false' ||
				ts.getData( $header[0], col, 'parser' ) === 'false';

			if ( buildSelect ) {
				buildFilter = $( '<select>' ).appendTo( c.$filters.eq( column ) );
			} else {
				ffxn = ts.getColumnData( table, wo.filter_formatter, column );
				if ( ffxn ) {
					wo.filter_formatterCount++;
					buildFilter = ffxn( c.$filters.eq( column ), column );
					// no element returned, so lets go find it
					if ( buildFilter && buildFilter.length === 0 ) {
						buildFilter = c.$filters.eq( column ).children( 'input' );
					}
					// element not in DOM, so lets attach it
					if ( buildFilter && ( buildFilter.parent().length === 0 ||
						( buildFilter.parent().length && buildFilter.parent()[0] !== c.$filters[column] ) ) ) {
						c.$filters.eq( column ).append( buildFilter );
					}
				} else {
					buildFilter = $( '<input type="search">' ).appendTo( c.$filters.eq( column ) );
				}
				if ( buildFilter ) {
					tmp = $header.data( 'placeholder' ) ||
						$header.attr( 'data-placeholder' ) ||
						wo.filter_placeholder.search || '';
					buildFilter.attr( 'placeholder', tmp );
				}
			}
			if ( buildFilter ) {
				// add filter class name
				name = ( $.isArray( wo.filter_cssFilter ) ?
					( typeof wo.filter_cssFilter[column] !== 'undefined' ? wo.filter_cssFilter[column] || '' : '' ) :
					wo.filter_cssFilter ) || '';
				buildFilter.addClass( tscss.filter + ' ' + name ).attr( 'data-column', column );
				if ( disabled ) {
					buildFilter.attr( 'placeholder', '' ).addClass( tscss.filterDisabled )[0].disabled = true;
				}
			}
		}
	},
	bindSearch: function( table, $el, internal ) {
		table = $( table )[0];
		$el = $( $el ); // allow passing a selector string
		if ( !$el.length ) { return; }
		var tmp,
			c = table.config,
			wo = c.widgetOptions,
			namespace = c.namespace + 'filter',
			$ext = wo.filter_$externalFilters;
		if ( internal !== true ) {
			// save anyMatch element
			tmp = wo.filter_anyColumnSelector + ',' + wo.filter_multipleColumnSelector;
			wo.filter_$anyMatch = $el.filter( tmp );
			if ( $ext && $ext.length ) {
				wo.filter_$externalFilters = wo.filter_$externalFilters.add( $el );
			} else {
				wo.filter_$externalFilters = $el;
			}
			// update values ( external filters added after table initialization )
			ts.setFilters( table, c.$table.data( 'lastSearch' ) || [], internal === false );
		}
		// unbind events
		tmp = ( 'keypress keyup search change '.split( ' ' ).join( namespace + ' ' ) );
		$el
		// use data attribute instead of jQuery data since the head is cloned without including
		// the data/binding
		.attr( 'data-lastSearchTime', new Date().getTime() )
		.unbind( tmp.replace( /\s+/g, ' ' ) )
		// include change for select - fixes #473
		.bind( 'keyup' + namespace, function( event ) {
			$( this ).attr( 'data-lastSearchTime', new Date().getTime() );
			// emulate what webkit does.... escape clears the filter
			if ( event.which === 27 ) {
				this.value = '';
			// live search
			} else if ( wo.filter_liveSearch === false ) {
				return;
				// don't return if the search value is empty ( all rows need to be revealed )
			} else if ( this.value !== '' && (
				// liveSearch can contain a min value length; ignore arrow and meta keys, but allow backspace
				( typeof wo.filter_liveSearch === 'number' && this.value.length < wo.filter_liveSearch ) ||
				// let return & backspace continue on, but ignore arrows & non-valid characters
				( event.which !== 13 && event.which !== 8 &&
					( event.which < 32 || ( event.which >= 37 && event.which <= 40 ) ) ) ) ) {
				return;
			}
			// change event = no delay; last true flag tells getFilters to skip newest timed input
			ts.filter.searching( table, true, true );
		})
		.bind( 'search change keypress '.split( ' ' ).join( namespace + ' ' ), function( event ) {
			var column = $( this ).data( 'column' );
			// don't allow 'change' event to process if the input value is the same - fixes #685
			if ( event.which === 13 || event.type === 'search' ||
				event.type === 'change' && this.value !== c.lastSearch[column] ) {
				event.preventDefault();
				// init search with no delay
				$( this ).attr( 'data-lastSearchTime', new Date().getTime() );
				ts.filter.searching( table, false, true );
			}
		});
	},
	searching: function( table, filter, skipFirst ) {
		var wo = table.config.widgetOptions;
		clearTimeout( wo.searchTimer );
		if ( typeof filter === 'undefined' || filter === true ) {
			// delay filtering
			wo.searchTimer = setTimeout( function() {
				ts.filter.checkFilters( table, filter, skipFirst );
			}, wo.filter_liveSearch ? wo.filter_searchDelay : 10 );
		} else {
			// skip delay
			ts.filter.checkFilters( table, filter, skipFirst );
		}
	},
	checkFilters: function( table, filter, skipFirst ) {
		var c = table.config,
			wo = c.widgetOptions,
			filterArray = $.isArray( filter ),
			filters = ( filterArray ) ? filter : ts.getFilters( table, true ),
			combinedFilters = ( filters || [] ).join( '' ); // combined filter values
		// prevent errors if delay init is set
		if ( $.isEmptyObject( c.cache ) ) {
			// update cache if delayInit set & pager has initialized ( after user initiates a search )
			if ( c.delayInit && c.pager && c.pager.initialized ) {
				c.$table.trigger( 'updateCache', [ function() {
					ts.filter.checkFilters( table, false, skipFirst );
				} ] );
			}
			return;
		}
		// add filter array back into inputs
		if ( filterArray ) {
			ts.setFilters( table, filters, false, skipFirst !== true );
			if ( !wo.filter_initialized ) { c.lastCombinedFilter = ''; }
		}
		if ( wo.filter_hideFilters ) {
			// show/hide filter row as needed
			c.$table
				.find( '.' + tscss.filterRow )
				.trigger( combinedFilters === '' ? 'mouseleave' : 'mouseenter' );
		}
		// return if the last search is the same; but filter === false when updating the search
		// see example-widget-filter.html filter toggle buttons
		if ( c.lastCombinedFilter === combinedFilters && filter !== false ) {
			return;
		} else if ( filter === false ) {
			// force filter refresh
			c.lastCombinedFilter = null;
			c.lastSearch = [];
		}
		if ( wo.filter_initialized ) {
			c.$table.trigger( 'filterStart', [filters] );
		}
		if ( c.showProcessing ) {
			// give it time for the processing icon to kick in
			setTimeout( function() {
				ts.filter.findRows( table, filters, combinedFilters );
				return false;
			}, 30 );
		} else {
			ts.filter.findRows( table, filters, combinedFilters );
			return false;
		}
	},
	hideFilters: function( table, c ) {
		var $filterRow, $filterRow2, timer;
		$( table )
			.find( '.' + tscss.filterRow )
			.addClass( tscss.filterRowHide )
			.bind( 'mouseenter mouseleave', function( e ) {
				// save event object - http://bugs.jquery.com/ticket/12140
				var event = e;
				$filterRow = $( this );
				clearTimeout( timer );
				timer = setTimeout( function() {
					if ( /enter|over/.test( event.type ) ) {
						$filterRow.removeClass( tscss.filterRowHide );
					} else {
						// don't hide if input has focus
						// $( ':focus' ) needs jQuery 1.6+
						if ( $( document.activeElement ).closest( 'tr' )[0] !== $filterRow[0] ) {
							// don't hide row if any filter has a value
							if ( c.lastCombinedFilter === '' ) {
								$filterRow.addClass( tscss.filterRowHide );
							}
						}
					}
				}, 200 );
			})
			.find( 'input, select' ).bind( 'focus blur', function( e ) {
				$filterRow2 = $( this ).closest( 'tr' );
				clearTimeout( timer );
				var event = e;
				timer = setTimeout( function() {
					// don't hide row if any filter has a value
					if ( ts.getFilters( c.$table ).join( '' ) === '' ) {
						$filterRow2.toggleClass( tscss.filterRowHide, event.type === 'focus' );
					}
				}, 200 );
			});
	},
	defaultFilter: function( filter, mask ) {
		if ( filter === '' ) { return filter; }
		var regex = ts.filter.regex.iQuery,
			maskLen = mask.match( ts.filter.regex.igQuery ).length,
			query = maskLen > 1 ? $.trim( filter ).split( /\s/ ) : [ $.trim( filter ) ],
			len = query.length - 1,
			indx = 0,
			val = mask;
		if ( len < 1 && maskLen > 1 ) {
			// only one 'word' in query but mask has >1 slots
			query[1] = query[0];
		}
		// replace all {query} with query words...
		// if query = 'Bob', then convert mask from '!{query}' to '!Bob'
		// if query = 'Bob Joe Frank', then convert mask '{q} OR {q}' to 'Bob OR Joe OR Frank'
		while ( regex.test( val ) ) {
			val = val.replace( regex, query[indx++] || '' );
			if ( regex.test( val ) && indx < len && ( query[indx] || '' ) !== '' ) {
				val = mask.replace( regex, val );
			}
		}
		return val;
	},
	getLatestSearch: function( $input ) {
		if ( $input ) {
			return $input.sort( function( a, b ) {
				return $( b ).attr( 'data-lastSearchTime' ) - $( a ).attr( 'data-lastSearchTime' );
			});
		}
		return $();
	},
	multipleColumns: function( c, $input ) {
		// look for multiple columns '1-3,4-6,8' in data-column
		var temp, ranges, range, start, end, singles, i, indx, len,
			wo = c.widgetOptions,
			// only target 'all' column inputs on initialization
			// & don't target 'all' column inputs if they don't exist
			targets = wo.filter_initialized || !$input.filter( wo.filter_anyColumnSelector ).length,
			columns = [],
			val = $.trim( ts.filter.getLatestSearch( $input ).attr( 'data-column' ) || '' );
		// process column range
		if ( targets && /-/.test( val ) ) {
			ranges = val.match( /(\d+)\s*-\s*(\d+)/g );
			len = ranges.length;
			for ( indx = 0; indx < len; indx++ ) {
				range = ranges[indx].split( /\s*-\s*/ );
				start = parseInt( range[0], 10 ) || 0;
				end = parseInt( range[1], 10 ) || ( c.columns - 1 );
				if ( start > end ) {
					temp = start; start = end; end = temp; // swap
				}
				if ( end >= c.columns ) {
					end = c.columns - 1;
				}
				for ( ; start <= end; start++ ) {
					columns.push( start );
				}
				// remove processed range from val
				val = val.replace( ranges[ indx ], '' );
			}
		}
		// process single columns
		if ( targets && /,/.test( val ) ) {
			singles = val.split( /\s*,\s*/ );
			len = singles.length;
			for ( i = 0; i < len; i++ ) {
				if ( singles[ i ] !== '' ) {
					indx = parseInt( singles[ i ], 10 );
					if ( indx < c.columns ) {
						columns.push( indx );
					}
				}
			}
		}
		// return all columns
		if ( !columns.length ) {
			for ( indx = 0; indx < c.columns; indx++ ) {
				columns.push( indx );
			}
		}
		return columns;
	},
	processRow: function( c, data, vars ) {
		var $cell, columnIndex, hasSelect, matches, result, val, filterMatched, excludeMatch,
			fxn, ffxn, txt,
			regex = ts.filter.regex,
			wo = c.widgetOptions,
			showRow = true;
		data.$cells = data.$row.children();

		if ( data.anyMatchFlag ) {
			// look for multiple columns '1-3,4-6,8'
			columnIndex = ts.filter.multipleColumns( c, wo.filter_$anyMatch );
			data.anyMatch = true;
			data.rowArray = data.$cells.map( function( i ) {
				if ( $.inArray( i, columnIndex ) > -1 ) {
					if ( data.parsed[ i ] ) {
						txt = data.cacheArray[ i ];
					} else {
						txt = data.rawArray[ i ];
						txt = $.trim( wo.filter_ignoreCase ? txt.toLowerCase() : txt );
						if ( c.sortLocaleCompare ) {
							txt = ts.replaceAccents( txt );
						}
					}
					return txt;
				}
			}).get();
			data.filter = data.anyMatchFilter;
			data.iFilter = data.iAnyMatchFilter;
			data.exact = data.rowArray.join( ' ' );
			data.iExact = wo.filter_ignoreCase ? data.exact.toLowerCase() : data.exact;
			data.cache = data.cacheArray.slice( 0, -1 ).join( ' ' );
			filterMatched = null;
			matches = null;
			for ( ffxn in ts.filter.types ) {
				if ( $.inArray( ffxn, vars.noAnyMatch ) < 0 && matches === null ) {
					matches = ts.filter.types[ffxn]( c, data );
					if ( matches !== null ) {
						filterMatched = matches;
					}
				}
			}
			if ( filterMatched !== null ) {
				showRow = filterMatched;
			} else {
				if ( wo.filter_startsWith ) {
					showRow = false;
					columnIndex = c.columns;
					while ( !showRow && columnIndex > 0 ) {
						columnIndex--;
						showRow = showRow || data.rowArray[ columnIndex ].indexOf( data.iFilter ) === 0;
					}
				} else {
					showRow = ( data.iExact + data.childRowText ).indexOf( data.iFilter ) >= 0;
				}
			}
			data.anyMatch = false;
			// no other filters to process
			if ( data.filters.join( '' ) === data.filter ) {
				return showRow;
			}
		}

		for ( columnIndex = 0; columnIndex < c.columns; columnIndex++ ) {
			data.filter = data.filters[ columnIndex ];
			data.index = columnIndex;

			// filter types to exclude, per column
			excludeMatch = vars.excludeFilter[ columnIndex ];

			// ignore if filter is empty or disabled
			if ( data.filter ) {
				data.cache = data.cacheArray[ columnIndex ];
				// check if column data should be from the cell or from parsed data
				if ( wo.filter_useParsedData || data.parsed[ columnIndex ] ) {
					data.exact = data.cache;
				} else {
					result = data.rawArray[ columnIndex ] || '';
					data.exact = c.sortLocaleCompare ? ts.replaceAccents( result ) : result; // issue #405
				}
				data.iExact = !regex.type.test( typeof data.exact ) && wo.filter_ignoreCase ?
					data.exact.toLowerCase() : data.exact;
				result = showRow; // if showRow is true, show that row

				// in case select filter option has a different value vs text 'a - z|A through Z'
				ffxn = wo.filter_columnFilters ?
					c.$filters.add( c.$externalFilters )
						.filter( '[data-column="'+ columnIndex + '"]' )
						.find( 'select option:selected' )
						.attr( 'data-function-name' ) || '' : '';
				// replace accents - see #357
				if ( c.sortLocaleCompare ) {
					data.filter = ts.replaceAccents( data.filter );
				}

				val = true;
				if ( wo.filter_defaultFilter && regex.iQuery.test( vars.defaultColFilter[ columnIndex ] ) ) {
					data.filter = ts.filter.defaultFilter( data.filter, vars.defaultColFilter[ columnIndex ] );
					// val is used to indicate that a filter select is using a default filter;
					// so we override the exact & partial matches
					val = false;
				}
				// data.iFilter = case insensitive ( if wo.filter_ignoreCase is true ),
				// data.filter = case sensitive
				data.iFilter = wo.filter_ignoreCase ? ( data.filter || '' ).toLowerCase() : data.filter;
				fxn = vars.functions[ columnIndex ];
				$cell = c.$headerIndexed[ columnIndex ];
				hasSelect = $cell.hasClass( 'filter-select' );
				filterMatched = null;
				if ( fxn || ( hasSelect && val ) ) {
					if ( fxn === true || hasSelect ) {
						// default selector uses exact match unless 'filter-match' class is found
						filterMatched = $cell.hasClass( 'filter-match' ) ?
							data.iExact.search( data.iFilter ) >= 0 :
							data.filter === data.exact;
					} else if ( typeof fxn === 'function' ) {
						// filter callback( exact cell content, parser normalized content,
						// filter input value, column index, jQuery row object )
						filterMatched = fxn( data.exact, data.cache, data.filter, columnIndex, data.$row, c, data );
					} else if ( typeof fxn[ ffxn || data.filter ] === 'function' ) {
						// selector option function
						txt = ffxn || data.filter;
						filterMatched =
							fxn[ txt ]( data.exact, data.cache, data.filter, columnIndex, data.$row, c, data );
					}
				}
				if ( filterMatched === null ) {
					// cycle through the different filters
					// filters return a boolean or null if nothing matches
					matches = null;
					for ( ffxn in ts.filter.types ) {
						if ( $.inArray( ffxn, excludeMatch ) < 0 && matches === null ) {
							matches = ts.filter.types[ ffxn ]( c, data );
							if ( matches !== null ) {
								filterMatched = matches;
							}
						}
					}
					if ( filterMatched !== null ) {
						result = filterMatched;
					// Look for match, and add child row data for matching
					} else {
						txt = ( data.iExact + data.childRowText )
							.indexOf( ts.filter.parseFilter( c, data.iFilter, columnIndex, data.parsed[ columnIndex ] ) );
						result = ( ( !wo.filter_startsWith && txt >= 0 ) || ( wo.filter_startsWith && txt === 0 ) );
					}
				} else {
					result = filterMatched;
				}
				showRow = ( result ) ? showRow : false;
			}
		}
		return showRow;
	},
	findRows: function( table, filters, combinedFilters ) {
		if ( table.config.lastCombinedFilter === combinedFilters ||
			!table.config.widgetOptions.filter_initialized ) {
			return;
		}
		var len, norm_rows, rowData, $rows, rowIndex, tbodyIndex, $tbody, columnIndex,
			isChild, childRow, lastSearch, showRow, time, val, indx,
			notFiltered, searchFiltered, query, injected, res, id, txt,
			storedFilters = $.extend( [], filters ),
			regex = ts.filter.regex,
			c = table.config,
			wo = c.widgetOptions,
			// data object passed to filters; anyMatch is a flag for the filters
			data = {
				anyMatch: false,
				filters: filters,
				// regex filter type cache
				filter_regexCache : [],
			},
			vars = {
				// anyMatch really screws up with these types of filters
				noAnyMatch: [ 'range', 'notMatch',  'operators' ],
				// cache filter variables that use ts.getColumnData in the main loop
				functions : [],
				excludeFilter : [],
				defaultColFilter : [],
				defaultAnyFilter : ts.getColumnData( table, wo.filter_defaultFilter, c.columns, true ) || ''
			};

		// parse columns after formatter, in case the class is added at that point
		data.parsed = c.$headers.map( function( columnIndex ) {
			return c.parsers && c.parsers[ columnIndex ] &&
				// force parsing if parser type is numeric
				c.parsers[ columnIndex ].parsed ||
				// getData won't return 'parsed' if other 'filter-' class names exist
				// ( e.g. <th class="filter-select filter-parsed"> )
				ts.getData && ts.getData( c.$headerIndexed[ columnIndex ],
					ts.getColumnData( table, c.headers, columnIndex ), 'filter' ) === 'parsed' ||
				$( this ).hasClass( 'filter-parsed' );
		}).get();

		for ( columnIndex = 0; columnIndex < c.columns; columnIndex++ ) {
			vars.functions[ columnIndex ] =
				ts.getColumnData( table, wo.filter_functions, columnIndex );
			vars.defaultColFilter[ columnIndex ] =
				ts.getColumnData( table, wo.filter_defaultFilter, columnIndex ) || '';
			vars.excludeFilter[ columnIndex ] =
				( ts.getColumnData( table, wo.filter_excludeFilter, columnIndex, true ) || '' ).split( /\s+/ );
		}

		if ( c.debug ) {
			ts.log( 'Filter: Starting filter widget search', filters );
			time = new Date();
		}
		// filtered rows count
		c.filteredRows = 0;
		c.totalRows = 0;
		// combindedFilters are undefined on init
		combinedFilters = ( storedFilters || [] ).join( '' );

		for ( tbodyIndex = 0; tbodyIndex < c.$tbodies.length; tbodyIndex++ ) {
			$tbody = ts.processTbody( table, c.$tbodies.eq( tbodyIndex ), true );
			// skip child rows & widget added ( removable ) rows - fixes #448 thanks to @hempel!
			// $rows = $tbody.children( 'tr' ).not( c.selectorRemove );
			columnIndex = c.columns;
			// convert stored rows into a jQuery object
			norm_rows = c.cache[ tbodyIndex ].normalized;
			$rows = $( $.map( norm_rows, function( el ) {
				return el[ columnIndex ].$row.get();
			}) );

			if ( combinedFilters === '' || wo.filter_serversideFiltering ) {
				$rows
					.removeClass( wo.filter_filteredRow )
					.not( '.' + c.cssChildRow )
					.css( 'display', '' );
			} else {
				// filter out child rows
				$rows = $rows.not( '.' + c.cssChildRow );
				len = $rows.length;

				if ( ( wo.filter_$anyMatch && wo.filter_$anyMatch.length ) ||
					typeof filters[c.columns] !== 'undefined' ) {
					data.anyMatchFlag = true;
					data.anyMatchFilter = '' + (
						filters[ c.columns ] ||
						wo.filter_$anyMatch && ts.filter.getLatestSearch( wo.filter_$anyMatch ).val() ||
						''
					);
					if ( wo.filter_columnAnyMatch ) {
						// specific columns search
						query = data.anyMatchFilter.split( regex.andSplit );
						injected = false;
						for ( indx = 0; indx < query.length; indx++ ) {
							res = query[ indx ].split( ':' );
							if ( res.length > 1 ) {
								// make the column a one-based index ( non-developers start counting from one :P )
								id = parseInt( res[0], 10 ) - 1;
								if ( id >= 0 && id < c.columns ) { // if id is an integer
									filters[ id ] = res[1];
									query.splice( indx, 1 );
									indx--;
									injected = true;
								}
							}
						}
						if ( injected ) {
							data.anyMatchFilter = query.join( ' && ' );
						}
					}
				}

				// optimize searching only through already filtered rows - see #313
				searchFiltered = wo.filter_searchFiltered;
				lastSearch = c.lastSearch || c.$table.data( 'lastSearch' ) || [];
				if ( searchFiltered ) {
					// cycle through all filters; include last ( columnIndex + 1 = match any column ). Fixes #669
					for ( indx = 0; indx < columnIndex + 1; indx++ ) {
						val = filters[indx] || '';
						// break out of loop if we've already determined not to search filtered rows
						if ( !searchFiltered ) { indx = columnIndex; }
						// search already filtered rows if...
						searchFiltered = searchFiltered && lastSearch.length &&
							// there are no changes from beginning of filter
							val.indexOf( lastSearch[indx] || '' ) === 0 &&
							// if there is NOT a logical 'or', or range ( 'to' or '-' ) in the string
							!regex.alreadyFiltered.test( val ) &&
							// if we are not doing exact matches, using '|' ( logical or ) or not '!'
							!/[=\"\|!]/.test( val ) &&
							// don't search only filtered if the value is negative
							// ( '> -10' => '> -100' will ignore hidden rows )
							!( /(>=?\s*-\d)/.test( val ) || /(<=?\s*\d)/.test( val ) ) &&
							// if filtering using a select without a 'filter-match' class ( exact match ) - fixes #593
							!( val !== '' && c.$filters && c.$filters.eq( indx ).find( 'select' ).length &&
								!c.$headerIndexed[indx].hasClass( 'filter-match' ) );
					}
				}
				notFiltered = $rows.not( '.' + wo.filter_filteredRow ).length;
				// can't search when all rows are hidden - this happens when looking for exact matches
				if ( searchFiltered && notFiltered === 0 ) { searchFiltered = false; }
				if ( c.debug ) {
					ts.log( 'Filter: Searching through ' +
						( searchFiltered && notFiltered < len ? notFiltered : 'all' ) + ' rows' );
				}
				if ( data.anyMatchFlag ) {
					if ( c.sortLocaleCompare ) {
						// replace accents
						data.anyMatchFilter = ts.replaceAccents( data.anyMatchFilter );
					}
					if ( wo.filter_defaultFilter && regex.iQuery.test( vars.defaultAnyFilter ) ) {
						data.anyMatchFilter = ts.filter.defaultFilter( data.anyMatchFilter, vars.defaultAnyFilter );
						// clear search filtered flag because default filters are not saved to the last search
						searchFiltered = false;
					}
					// make iAnyMatchFilter lowercase unless both filter widget & core ignoreCase options are true
					// when c.ignoreCase is true, the cache contains all lower case data
					data.iAnyMatchFilter = !( wo.filter_ignoreCase && c.ignoreCase ) ?
						data.anyMatchFilter :
						data.anyMatchFilter.toLowerCase();
				}

				// loop through the rows
				for ( rowIndex = 0; rowIndex < len; rowIndex++ ) {

					txt = $rows[ rowIndex ].className;
					// the first row can never be a child row
					isChild = rowIndex && regex.child.test( txt );
					// skip child rows & already filtered rows
					if ( isChild || ( searchFiltered && regex.filtered.test( txt ) ) ) {
						continue;
					}

					data.$row = $rows.eq( rowIndex );
					data.cacheArray = norm_rows[ rowIndex ];
					rowData = data.cacheArray[ c.columns ];
					data.rawArray = rowData.raw;
					data.childRowText = '';

					if ( !wo.filter_childByColumn ) {
						txt = '';
						// child row cached text
						childRow = rowData.child;
						// so, if 'table.config.widgetOptions.filter_childRows' is true and there is
						// a match anywhere in the child row, then it will make the row visible
						// checked here so the option can be changed dynamically
						for ( indx = 0; indx < childRow.length; indx++ ) {
							txt += ' ' + childRow[indx].join( '' ) || '';
						}
						data.childRowText = wo.filter_childRows ?
							( wo.filter_ignoreCase ? txt.toLowerCase() : txt ) :
							'';
					}

					showRow = ts.filter.processRow( c, data, vars );
					childRow = rowData.$row.filter( ':gt( 0 )' );

					if ( wo.filter_childRows && childRow.length ) {
						if ( wo.filter_childByColumn ) {
							// cycle through each child row
							for ( indx = 0; indx < childRow.length; indx++ ) {
								data.$row = childRow.eq( indx );
								data.cacheArray = rowData.child[ indx ];
								data.rawArray = data.cacheArray;
								// use OR comparison on child rows
								showRow = showRow || ts.filter.processRow( c, data, vars );
							}
						}
						childRow.toggleClass( wo.filter_filteredRow, !showRow );
					}

					rowData.$row
						.toggleClass( wo.filter_filteredRow, !showRow )[0]
						.display = showRow ? '' : 'none';
				}
			}
			c.filteredRows += $rows.not( '.' + wo.filter_filteredRow ).length;
			c.totalRows += $rows.length;
			ts.processTbody( table, $tbody, false );
		}
		c.lastCombinedFilter = combinedFilters; // save last search
		// don't save 'filters' directly since it may have altered ( AnyMatch column searches )
		c.lastSearch = storedFilters;
		c.$table.data( 'lastSearch', storedFilters );
		if ( wo.filter_saveFilters && ts.storage ) {
			ts.storage( table, 'tablesorter-filters', storedFilters );
		}
		if ( c.debug ) {
			ts.benchmark( 'Completed filter widget search', time );
		}
		if ( wo.filter_initialized ) {
			c.$table.trigger( 'filterEnd', c );
		}
		setTimeout( function() {
			c.$table.trigger( 'applyWidgets' ); // make sure zebra widget is applied
		}, 0 );
	},
	getOptionSource: function( table, column, onlyAvail ) {
		table = $( table )[0];
		var cts, indx, len,
			c = table.config,
			wo = c.widgetOptions,
			parsed = [],
			arry = false,
			source = wo.filter_selectSource,
			last = c.$table.data( 'lastSearch' ) || [],
			fxn = $.isFunction( source ) ? true : ts.getColumnData( table, source, column );

		if ( onlyAvail && last[column] !== '' ) {
			onlyAvail = false;
		}

		// filter select source option
		if ( fxn === true ) {
			// OVERALL source
			arry = source( table, column, onlyAvail );
		} else if ( fxn instanceof $ || ( $.type( fxn ) === 'string' && fxn.indexOf( '</option>' ) >= 0 ) ) {
			// selectSource is a jQuery object or string of options
			return fxn;
		} else if ( $.isArray( fxn ) ) {
			arry = fxn;
		} else if ( $.type( source ) === 'object' && fxn ) {
			// custom select source function for a SPECIFIC COLUMN
			arry = fxn( table, column, onlyAvail );
		}
		if ( arry === false ) {
			// fall back to original method
			arry = ts.filter.getOptions( table, column, onlyAvail );
		}

		// get unique elements and sort the list
		// if $.tablesorter.sortText exists ( not in the original tablesorter ),
		// then natural sort the list otherwise use a basic sort
		arry = $.grep( arry, function( value, indx ) {
			return $.inArray( value, arry ) === indx;
		});

		if ( c.$headerIndexed[ column ].hasClass( 'filter-select-nosort' ) ) {
			// unsorted select options
			return arry;
		} else {
			len = arry.length;
			// parse select option values
			for ( indx = 0; indx < len; indx++ ) {
				// parse array data using set column parser; this DOES NOT pass the original
				// table cell to the parser format function
				parsed.push({
					t : arry[ indx ],
					p : c.parsers && c.parsers[ column ].format( arry[ indx ], table, [], column )
				});
			}

			// sort parsed select options
			cts = c.textSorter || '';
			parsed.sort( function( a, b ) {
				// sortNatural breaks if you don't pass it strings
				var x = a.p.toString(),
					y = b.p.toString();
				if ( $.isFunction( cts ) ) {
					// custom OVERALL text sorter
					return cts( x, y, true, column, table );
				} else if ( typeof( cts ) === 'object' && cts.hasOwnProperty( column ) ) {
					// custom text sorter for a SPECIFIC COLUMN
					return cts[column]( x, y, true, column, table );
				} else if ( ts.sortNatural ) {
					// fall back to natural sort
					return ts.sortNatural( x, y );
				}
				// using an older version! do a basic sort
				return true;
			});
			// rebuild arry from sorted parsed data
			arry = [];
			len = parsed.length;
			for ( indx = 0; indx < len; indx++ ) {
				arry.push( parsed[indx].t );
			}
			return arry;
		}
	},
	getOptions: function( table, column, onlyAvail ) {
		table = $( table )[0];
		var rowIndex, tbodyIndex, len, row, cache,
			c = table.config,
			wo = c.widgetOptions,
			arry = [];
		for ( tbodyIndex = 0; tbodyIndex < c.$tbodies.length; tbodyIndex++ ) {
			cache = c.cache[tbodyIndex];
			len = c.cache[tbodyIndex].normalized.length;
			// loop through the rows
			for ( rowIndex = 0; rowIndex < len; rowIndex++ ) {
				// get cached row from cache.row ( old ) or row data object
				// ( new; last item in normalized array )
				row = cache.row ?
					cache.row[ rowIndex ] :
					cache.normalized[ rowIndex ][ c.columns ].$row[0];
				// check if has class filtered
				if ( onlyAvail && row.className.match( wo.filter_filteredRow ) ) {
					continue;
				}
				// get non-normalized cell content
				if ( wo.filter_useParsedData ||
					c.parsers[column].parsed ||
					c.$headerIndexed[column].hasClass( 'filter-parsed' ) ) {
					arry.push( '' + cache.normalized[ rowIndex ][ column ] );
				} else {
					// get raw cached data instead of content directly from the cells
					arry.push( cache.normalized[ rowIndex ][ c.columns ].raw[ column ] );
				}
			}
		}
		return arry;
	},
	buildSelect: function( table, column, arry, updating, onlyAvail ) {
		table = $( table )[0];
		column = parseInt( column, 10 );
		if ( !table.config.cache || $.isEmptyObject( table.config.cache ) ) {
			return;
		}
		var indx, val, txt, t, $filters, $filter,
			c = table.config,
			wo = c.widgetOptions,
			node = c.$headerIndexed[ column ],
			// t.data( 'placeholder' ) won't work in jQuery older than 1.4.3
			options = '<option value="">' +
				( node.data( 'placeholder' ) ||
					node.attr( 'data-placeholder' ) ||
					wo.filter_placeholder.select || ''
				) + '</option>',
			// Get curent filter value
			currentValue = c.$table
				.find( 'thead' )
				.find( 'select.' + tscss.filter + '[data-column="' + column + '"]' )
				.val();
		// nothing included in arry ( external source ), so get the options from
		// filter_selectSource or column data
		if ( typeof arry === 'undefined' || arry === '' ) {
			arry = ts.filter.getOptionSource( table, column, onlyAvail );
		}

		if ( $.isArray( arry ) ) {
			// build option list
			for ( indx = 0; indx < arry.length; indx++ ) {
				txt = arry[indx] = ( '' + arry[indx] ).replace( /\"/g, '&quot;' );
				val = txt;
				// allow including a symbol in the selectSource array
				// 'a-z|A through Z' so that 'a-z' becomes the option value
				// and 'A through Z' becomes the option text
				if ( txt.indexOf( wo.filter_selectSourceSeparator ) >= 0 ) {
					t = txt.split( wo.filter_selectSourceSeparator );
					val = t[0];
					txt = t[1];
				}
				// replace quotes - fixes #242 & ignore empty strings
				// see http://stackoverflow.com/q/14990971/145346
				options += arry[indx] !== '' ?
					'<option ' +
						( val === txt ? '' : 'data-function-name="' + arry[indx] + '" ' ) +
						'value="' + val + '">' + txt +
					'</option>' : '';
			}
			// clear arry so it doesn't get appended twice
			arry = [];
		}

		// update all selects in the same column ( clone thead in sticky headers &
		// any external selects ) - fixes 473
		$filters = ( c.$filters ? c.$filters : c.$table.children( 'thead' ) )
			.find( '.' + tscss.filter );
		if ( wo.filter_$externalFilters ) {
			$filters = $filters && $filters.length ?
				$filters.add( wo.filter_$externalFilters ) :
				wo.filter_$externalFilters;
		}
		$filter = $filters.filter( 'select[data-column="' + column + '"]' );

		// make sure there is a select there!
		if ( $filter.length ) {
			$filter[ updating ? 'html' : 'append' ]( options );
			if ( !$.isArray( arry ) ) {
				// append options if arry is provided externally as a string or jQuery object
				// options ( default value ) was already added
				$filter.append( arry ).val( currentValue );
			}
			$filter.val( currentValue );
		}
	},
	buildDefault: function( table, updating ) {
		var columnIndex, $header, noSelect,
			c = table.config,
			wo = c.widgetOptions,
			columns = c.columns;
		// build default select dropdown
		for ( columnIndex = 0; columnIndex < columns; columnIndex++ ) {
			$header = c.$headerIndexed[columnIndex];
			noSelect = !( $header.hasClass( 'filter-false' ) || $header.hasClass( 'parser-false' ) );
			// look for the filter-select class; build/update it if found
			if ( ( $header.hasClass( 'filter-select' ) ||
				ts.getColumnData( table, wo.filter_functions, columnIndex ) === true ) && noSelect ) {
				ts.filter.buildSelect( table, columnIndex, '', updating, $header.hasClass( wo.filter_onlyAvail ) );
			}
		}
	}
};

ts.getFilters = function( table, getRaw, setFilters, skipFirst ) {
	var i, $filters, $column, cols,
		filters = false,
		c = table ? $( table )[0].config : '',
		wo = c ? c.widgetOptions : '';
	if ( ( getRaw !== true && wo && !wo.filter_columnFilters ) ||
		// setFilters called, but last search is exactly the same as the current
		// fixes issue #733 & #903 where calling update causes the input values to reset
		( $.isArray(setFilters) && setFilters.join('') === c.lastCombinedFilter ) ) {
		return $( table ).data( 'lastSearch' );
	}
	if ( c ) {
		if ( c.$filters ) {
			$filters = c.$filters.find( '.' + tscss.filter );
		}
		if ( wo.filter_$externalFilters ) {
			$filters = $filters && $filters.length ?
				$filters.add( wo.filter_$externalFilters ) :
				wo.filter_$externalFilters;
		}
		if ( $filters && $filters.length ) {
			filters = setFilters || [];
			for ( i = 0; i < c.columns + 1; i++ ) {
				cols = ( i === c.columns ?
					// 'all' columns can now include a range or set of columms ( data-column='0-2,4,6-7' )
					wo.filter_anyColumnSelector + ',' + wo.filter_multipleColumnSelector :
					'[data-column="' + i + '"]' );
				$column = $filters.filter( cols );
				if ( $column.length ) {
					// move the latest search to the first slot in the array
					$column = ts.filter.getLatestSearch( $column );
					if ( $.isArray( setFilters ) ) {
						// skip first ( latest input ) to maintain cursor position while typing
						if ( skipFirst ) {
							$column.slice( 1 );
						}
						if ( i === c.columns ) {
							// prevent data-column='all' from filling data-column='0,1' ( etc )
							cols = $column.filter( wo.filter_anyColumnSelector );
							$column = cols.length ? cols : $column;
						}
						$column
							.val( setFilters[ i ] )
							.trigger( 'change.tsfilter' );
					} else {
						filters[i] = $column.val() || '';
						// don't change the first... it will move the cursor
						if ( i === c.columns ) {
							// don't update range columns from 'all' setting
							$column
								.slice( 1 )
								.filter( '[data-column*="' + $column.attr( 'data-column' ) + '"]' )
								.val( filters[ i ] );
						} else {
							$column
								.slice( 1 )
								.val( filters[ i ] );
						}
					}
					// save any match input dynamically
					if ( i === c.columns && $column.length ) {
						wo.filter_$anyMatch = $column;
					}
				}
			}
		}
	}
	if ( filters.length === 0 ) {
		filters = false;
	}
	return filters;
};

ts.setFilters = function( table, filter, apply, skipFirst ) {
	var c = table ? $( table )[0].config : '',
		valid = ts.getFilters( table, true, filter, skipFirst );
	if ( c && apply ) {
		// ensure new set filters are applied, even if the search is the same
		c.lastCombinedFilter = null;
		c.lastSearch = [];
		ts.filter.searching( c.table, filter, skipFirst );
		c.$table.trigger( 'filterFomatterUpdate' );
	}
	return !!valid;
};

})( jQuery );

/*! Widget: stickyHeaders - updated 3/26/2015 (v2.21.3) *//*
 * Requires tablesorter v2.8+ and jQuery 1.4.3+
 * by Rob Garrison
 */
;(function ($, window) {
'use strict';
var ts = $.tablesorter || {};

$.extend(ts.css, {
	sticky    : 'tablesorter-stickyHeader', // stickyHeader
	stickyVis : 'tablesorter-sticky-visible',
	stickyHide: 'tablesorter-sticky-hidden',
	stickyWrap: 'tablesorter-sticky-wrapper'
});

// Add a resize event to table headers
ts.addHeaderResizeEvent = function(table, disable, settings) {
	table = $(table)[0]; // make sure we're using a dom element
	var headers,
		defaults = {
			timer : 250
		},
		options = $.extend({}, defaults, settings),
		c = table.config,
		wo = c.widgetOptions,
		checkSizes = function(triggerEvent) {
			wo.resize_flag = true;
			headers = [];
			c.$headers.each(function() {
				var $header = $(this),
					sizes = $header.data('savedSizes') || [0,0], // fixes #394
					width = this.offsetWidth,
					height = this.offsetHeight;
				if (width !== sizes[0] || height !== sizes[1]) {
					$header.data('savedSizes', [ width, height ]);
					headers.push(this);
				}
			});
			if (headers.length && triggerEvent !== false) {
				c.$table.trigger('resize', [ headers ]);
			}
			wo.resize_flag = false;
		};
	checkSizes(false);
	clearInterval(wo.resize_timer);
	if (disable) {
		wo.resize_flag = false;
		return false;
	}
	wo.resize_timer = setInterval(function() {
		if (wo.resize_flag) { return; }
		checkSizes();
	}, options.timer);
};

// Sticky headers based on this awesome article:
// http://css-tricks.com/13465-persistent-headers/
// and https://github.com/jmosbech/StickyTableHeaders by Jonas Mosbech
// **************************
ts.addWidget({
	id: "stickyHeaders",
	priority: 60, // sticky widget must be initialized after the filter widget!
	options: {
		stickyHeaders : '',       // extra class name added to the sticky header row
		stickyHeaders_attachTo : null, // jQuery selector or object to attach sticky header to
		stickyHeaders_xScroll : null, // jQuery selector or object to monitor horizontal scroll position (defaults: xScroll > attachTo > window)
		stickyHeaders_yScroll : null, // jQuery selector or object to monitor vertical scroll position (defaults: yScroll > attachTo > window)
		stickyHeaders_offset : 0, // number or jquery selector targeting the position:fixed element
		stickyHeaders_filteredToTop: true, // scroll table top into view after filtering
		stickyHeaders_cloneId : '-sticky', // added to table ID, if it exists
		stickyHeaders_addResizeEvent : true, // trigger "resize" event on headers
		stickyHeaders_includeCaption : true, // if false and a caption exist, it won't be included in the sticky header
		stickyHeaders_zIndex : 2 // The zIndex of the stickyHeaders, allows the user to adjust this to their needs
	},
	format: function(table, c, wo) {
		// filter widget doesn't initialize on an empty table. Fixes #449
		if ( c.$table.hasClass('hasStickyHeaders') || ($.inArray('filter', c.widgets) >= 0 && !c.$table.hasClass('hasFilters')) ) {
			return;
		}
		var $table = c.$table,
			// add position: relative to attach element, hopefully it won't cause trouble.
			$attach = $(wo.stickyHeaders_attachTo),
			namespace = c.namespace + 'stickyheaders ',
			// element to watch for the scroll event
			$yScroll = $(wo.stickyHeaders_yScroll || wo.stickyHeaders_attachTo || window),
			$xScroll = $(wo.stickyHeaders_xScroll || wo.stickyHeaders_attachTo || window),
			$thead = $table.children('thead:first'),
			$header = $thead.children('tr').not('.sticky-false').children(),
			$tfoot = $table.children('tfoot'),
			$stickyOffset = isNaN(wo.stickyHeaders_offset) ? $(wo.stickyHeaders_offset) : '',
			stickyOffset = $stickyOffset.length ? $stickyOffset.height() || 0 : parseInt(wo.stickyHeaders_offset, 10) || 0,
			// is this table nested? If so, find parent sticky header wrapper (div, not table)
			$nestedSticky = $table.parent().closest('.' + ts.css.table).hasClass('hasStickyHeaders') ?
				$table.parent().closest('table.tablesorter')[0].config.widgetOptions.$sticky.parent() : [],
			nestedStickyTop = $nestedSticky.length ? $nestedSticky.height() : 0,
			// clone table, then wrap to make sticky header
			$stickyTable = wo.$sticky = $table.clone()
				.addClass('containsStickyHeaders ' + ts.css.sticky + ' ' + wo.stickyHeaders + ' ' + c.namespace.slice(1) + '_extra_table' )
				.wrap('<div class="' + ts.css.stickyWrap + '">'),
			$stickyWrap = $stickyTable.parent()
				.addClass(ts.css.stickyHide)
				.css({
					position   : $attach.length ? 'absolute' : 'fixed',
					padding    : parseInt( $stickyTable.parent().parent().css('padding-left'), 10 ),
					top        : stickyOffset + nestedStickyTop,
					left       : 0,
					visibility : 'hidden',
					zIndex     : wo.stickyHeaders_zIndex || 2
				}),
			$stickyThead = $stickyTable.children('thead:first'),
			$stickyCells,
			laststate = '',
			spacing = 0,
			setWidth = function($orig, $clone){
				$orig.filter(':visible').each(function(i) {
					var width, border,
						$cell = $clone.filter(':visible').eq(i),
						$this = $(this);
					// code from https://github.com/jmosbech/StickyTableHeaders
					if ($this.css('box-sizing') === 'border-box') {
						width = $this.outerWidth();
					} else {
						if ($cell.css('border-collapse') === 'collapse') {
							if (window.getComputedStyle) {
								width = parseFloat( window.getComputedStyle(this, null).width );
							} else {
								// ie8 only
								border = parseFloat( $this.css('border-width') );
								width = $this.outerWidth() - parseFloat( $this.css('padding-left') ) - parseFloat( $this.css('padding-right') ) - border;
							}
						} else {
							width = $this.width();
						}
					}
					$cell.css({
						'min-width': width,
						'max-width': width
					});
				});
			},
			resizeHeader = function() {
				stickyOffset = $stickyOffset.length ? $stickyOffset.height() || 0 : parseInt(wo.stickyHeaders_offset, 10) || 0;
				spacing = 0;
				$stickyWrap.css({
					left : $attach.length ? parseInt($attach.css('padding-left'), 10) || 0 :
							$table.offset().left - parseInt($table.css('margin-left'), 10) - $xScroll.scrollLeft() - spacing,
					width: $table.outerWidth()
				});
				setWidth( $table, $stickyTable );
				setWidth( $header, $stickyCells );
			};
		// only add a position relative if a position isn't already defined
		if ($attach.length && !$attach.css('position')) {
			$attach.css('position', 'relative');
		}
		// fix clone ID, if it exists - fixes #271
		if ($stickyTable.attr('id')) { $stickyTable[0].id += wo.stickyHeaders_cloneId; }
		// clear out cloned table, except for sticky header
		// include caption & filter row (fixes #126 & #249) - don't remove cells to get correct cell indexing
		$stickyTable.find('thead:gt(0), tr.sticky-false').hide();
		$stickyTable.find('tbody, tfoot').remove();
		$stickyTable.find('caption').toggle(wo.stickyHeaders_includeCaption);
		// issue #172 - find td/th in sticky header
		$stickyCells = $stickyThead.children().children();
		$stickyTable.css({ height:0, width:0, margin: 0 });
		// remove resizable block
		$stickyCells.find('.' + ts.css.resizer).remove();
		// update sticky header class names to match real header after sorting
		$table
			.addClass('hasStickyHeaders')
			.bind('pagerComplete' + namespace, function() {
				resizeHeader();
			});

		ts.bindEvents(table, $stickyThead.children().children('.' + ts.css.header));

		// add stickyheaders AFTER the table. If the table is selected by ID, the original one (first) will be returned.
		$table.after( $stickyWrap );

		// onRenderHeader is defined, we need to do something about it (fixes #641)
		if (c.onRenderHeader) {
			$stickyThead.children('tr').children().each(function(index){
				// send second parameter
				c.onRenderHeader.apply( $(this), [ index, c, $stickyTable ] );
			});
		}

		// make it sticky!
		$xScroll.add($yScroll)
		.unbind( ('scroll resize '.split(' ').join( namespace )).replace(/\s+/g, ' ') )
		.bind('scroll resize '.split(' ').join( namespace ), function(event) {
			if (!$table.is(':visible')) { return; } // fixes #278
			// Detect nested tables - fixes #724
			nestedStickyTop = $nestedSticky.length ? $nestedSticky.offset().top - $yScroll.scrollTop() + $nestedSticky.height() : 0;
			var offset = $table.offset(),
				yWindow = $.isWindow( $yScroll[0] ), // $.isWindow needs jQuery 1.4.3
				xWindow = $.isWindow( $xScroll[0] ),
				// scrollTop = ( $attach.length ? $attach.offset().top : $yScroll.scrollTop() ) + stickyOffset + nestedStickyTop,
				scrollTop = ( $attach.length ? ( yWindow ? $yScroll.scrollTop() : $yScroll.offset().top ) : $yScroll.scrollTop() ) + stickyOffset + nestedStickyTop,
				tableHeight = $table.height() - ($stickyWrap.height() + ($tfoot.height() || 0)),
				isVisible = ( scrollTop > offset.top ) && ( scrollTop < offset.top + tableHeight ) ? 'visible' : 'hidden',
				cssSettings = { visibility : isVisible };

			if ($attach.length) {
				cssSettings.top = yWindow ? scrollTop - $attach.offset().top : $attach.scrollTop();
			}
			if (xWindow) {
				// adjust when scrolling horizontally - fixes issue #143
				cssSettings.left = $table.offset().left - parseInt($table.css('margin-left'), 10) - $xScroll.scrollLeft() - spacing;
			}
			if ($nestedSticky.length) {
				cssSettings.top = ( cssSettings.top || 0 ) + stickyOffset + nestedStickyTop;
			}
			$stickyWrap
				.removeClass( ts.css.stickyVis + ' ' + ts.css.stickyHide )
				.addClass( isVisible === 'visible' ? ts.css.stickyVis : ts.css.stickyHide )
				.css(cssSettings);
			if (isVisible !== laststate || event.type === 'resize') {
				// make sure the column widths match
				resizeHeader();
				laststate = isVisible;
			}
		});
		if (wo.stickyHeaders_addResizeEvent) {
			ts.addHeaderResizeEvent(table);
		}

		// look for filter widget
		if ($table.hasClass('hasFilters') && wo.filter_columnFilters) {
			// scroll table into view after filtering, if sticky header is active - #482
			$table.bind('filterEnd' + namespace, function() {
				// $(':focus') needs jQuery 1.6+
				var $td = $(document.activeElement).closest('td'),
					column = $td.parent().children().index($td);
				// only scroll if sticky header is active
				if ($stickyWrap.hasClass(ts.css.stickyVis) && wo.stickyHeaders_filteredToTop) {
					// scroll to original table (not sticky clone)
					window.scrollTo(0, $table.position().top);
					// give same input/select focus; check if c.$filters exists; fixes #594
					if (column >= 0 && c.$filters) {
						c.$filters.eq(column).find('a, select, input').filter(':visible').focus();
					}
				}
			});
			ts.filter.bindSearch( $table, $stickyCells.find('.' + ts.css.filter) );
			// support hideFilters
			if (wo.filter_hideFilters) {
				ts.filter.hideFilters($stickyTable, c);
			}
		}

		$table.trigger('stickyHeadersInit');

	},
	remove: function(table, c, wo) {
		var namespace = c.namespace + 'stickyheaders ';
		c.$table
			.removeClass('hasStickyHeaders')
			.unbind( ('pagerComplete filterEnd '.split(' ').join(namespace)).replace(/\s+/g, ' ') )
			.next('.' + ts.css.stickyWrap).remove();
		if (wo.$sticky && wo.$sticky.length) { wo.$sticky.remove(); } // remove cloned table
		$(window)
			.add(wo.stickyHeaders_xScroll)
			.add(wo.stickyHeaders_yScroll)
			.add(wo.stickyHeaders_attachTo)
			.unbind( ('scroll resize '.split(' ').join(namespace)).replace(/\s+/g, ' ') );
		ts.addHeaderResizeEvent(table, false);
	}
});

})(jQuery, window);

/*! Widget: resizable - updated 5/17/2015 (v2.22.0) */
;(function ($, window) {
'use strict';
var ts = $.tablesorter || {};

$.extend(ts.css, {
	resizableContainer : 'tablesorter-resizable-container',
	resizableHandle    : 'tablesorter-resizable-handle',
	resizableNoSelect  : 'tablesorter-disableSelection',
	resizableStorage   : 'tablesorter-resizable'
});

// Add extra scroller css
$(function(){
	var s = '<style>' +
		'body.' + ts.css.resizableNoSelect + ' { -ms-user-select: none; -moz-user-select: -moz-none;' +
			'-khtml-user-select: none; -webkit-user-select: none; user-select: none; }' +
		'.' + ts.css.resizableContainer + ' { position: relative; height: 1px; }' +
		// make handle z-index > than stickyHeader z-index, so the handle stays above sticky header
		'.' + ts.css.resizableHandle + ' { position: absolute; display: inline-block; width: 8px; top: 1px;' +
			'cursor: ew-resize; z-index: 3; user-select: none; -moz-user-select: none; }' +
		'</style>';
	$(s).appendTo('body');
});

ts.resizable = {
	init : function( c, wo ) {
		if ( c.$table.hasClass( 'hasResizable' ) ) { return; }
		c.$table.addClass( 'hasResizable' );
		ts.resizableReset( c.table, true ); // set default widths

		// internal variables
		wo.resizable_ = {
			$wrap : c.$table.parent(),
			mouseXPosition : 0,
			$target : null,
			$next : null,
			overflow : c.$table.parent().css('overflow') === 'auto',
			fullWidth : Math.abs(c.$table.parent().width() - c.$table.width()) < 20,
			storedSizes : []
		};

		var noResize, $header, column, storedSizes,
			marginTop = parseInt( c.$table.css( 'margin-top' ), 10 );

		wo.resizable_.storedSizes = storedSizes = ( ( ts.storage && wo.resizable !== false ) ?
			ts.storage( c.table, ts.css.resizableStorage ) :
			[] ) || [];
		ts.resizable.setWidths( c, wo, storedSizes );

		wo.$resizable_container = $( '<div class="' + ts.css.resizableContainer + '">' )
			.css({ top : marginTop })
			.insertBefore( c.$table );
		// add container
		for ( column = 0; column < c.columns; column++ ) {
			$header = c.$headerIndexed[ column ];
			noResize = ts.getData( $header, ts.getColumnData( c.table, c.headers, column ), 'resizable' ) === 'false';
			if ( !noResize ) {
				$( '<div class="' + ts.css.resizableHandle + '">' )
					.appendTo( wo.$resizable_container )
					.attr({
						'data-column' : column,
						'unselectable' : 'on'
					})
					.data( 'header', $header )
					.bind( 'selectstart', false );
			}
		}
		c.$table.one('tablesorter-initialized', function() {
			ts.resizable.setHandlePosition( c, wo );
			ts.resizable.bindings( this.config, this.config.widgetOptions );
		});
	},

	setWidth : function( $el, width ) {
		$el.css({
			'width' : width,
			'min-width' : '',
			'max-width' : ''
		});
	},

	setWidths : function( c, wo, storedSizes ) {
		var column,
			$extra = $( c.namespace + '_extra_headers' ),
			$col = c.$table.children( 'colgroup' ).children( 'col' );
		storedSizes = storedSizes || wo.resizable_.storedSizes || [];
		// process only if table ID or url match
		if ( storedSizes.length ) {
			for ( column = 0; column < c.columns; column++ ) {
				// set saved resizable widths
				c.$headerIndexed[ column ].width( storedSizes[ column ] );
				if ( $extra.length ) {
					// stickyHeaders needs to modify min & max width as well
					ts.resizable.setWidth( $extra.eq( column ).add( $col.eq( column ) ), storedSizes[ column ] );
				}
			}
			if ( $( c.namespace + '_extra_table' ).length && !ts.hasWidget( c.table, 'scroller' ) ) {
				ts.resizable.setWidth( $( c.namespace + '_extra_table' ), c.$table.outerWidth() );
			}
		}
	},

	setHandlePosition : function( c, wo ) {
		var startPosition,
			hasScroller = ts.hasWidget( c.table, 'scroller' ),
			tableHeight = c.$table.height(),
			$handles = wo.$resizable_container.children(),
			handleCenter = Math.floor( $handles.width() / 2 );

		if ( hasScroller ) {
			tableHeight = 0;
			c.$table.closest( '.' + ts.css.scrollerWrap ).children().each(function(){
				var $this = $(this);
				// center table has a max-height set
				tableHeight += $this.filter('[style*="height"]').length ? $this.height() : $this.children('table').height();
			});
		}
		// subtract out table left position from resizable handles. Fixes #864
		startPosition = c.$table.position().left;
		$handles.each( function() {
			var $this = $(this),
				column = parseInt( $this.attr( 'data-column' ), 10 ),
				columns = c.columns - 1,
				$header = $this.data( 'header' );
			if ( !$header ) { return; } // see #859
			if ( !$header.is(':visible') ) {
				$this.hide();
			} else if ( column < columns || column === columns && wo.resizable_addLastColumn ) {
				$this.css({
					display: 'inline-block',
					height : tableHeight,
					left : $header.position().left - startPosition + $header.outerWidth() - handleCenter
				});
			}
		});
	},

	// prevent text selection while dragging resize bar
	toggleTextSelection : function( c, toggle ) {
		var namespace = c.namespace + 'tsresize';
		c.widgetOptions.resizable_.disabled = toggle;
		$( 'body' ).toggleClass( ts.css.resizableNoSelect, toggle );
		if ( toggle ) {
			$( 'body' )
				.attr( 'unselectable', 'on' )
				.bind( 'selectstart' + namespace, false );
		} else {
			$( 'body' )
				.removeAttr( 'unselectable' )
				.unbind( 'selectstart' + namespace );
		}
	},

	bindings : function( c, wo ) {
		var namespace = c.namespace + 'tsresize';
		wo.$resizable_container.children().bind( 'mousedown', function( event ) {
			// save header cell and mouse position
			var column, $this,
				vars = wo.resizable_,
				$extras = $( c.namespace + '_extra_headers' ),
				$header = $( event.target ).data( 'header' );

			column = parseInt( $header.attr( 'data-column' ), 10 );
			vars.$target = $header = $header.add( $extras.filter('[data-column="' + column + '"]') );
			vars.target = column;

			// if table is not as wide as it's parent, then resize the table
			vars.$next = event.shiftKey || wo.resizable_targetLast ?
				$header.parent().children().not( '.resizable-false' ).filter( ':last' ) :
				$header.nextAll( ':not(.resizable-false)' ).eq( 0 );

			column = parseInt( vars.$next.attr( 'data-column' ), 10 );
			vars.$next = vars.$next.add( $extras.filter('[data-column="' + column + '"]') );
			vars.next = column;

			vars.mouseXPosition = event.pageX;
			vars.storedSizes = [];
			for ( column = 0; column < c.columns; column++ ) {
				$this = c.$headerIndexed[ column ];
				vars.storedSizes[ column ] = $this.is(':visible') ? $this.width() : 0;
			}
			ts.resizable.toggleTextSelection( c, true );
		});

		$( document )
			.bind( 'mousemove' + namespace, function( event ) {
				var vars = wo.resizable_;
				// ignore mousemove if no mousedown
				if ( !vars.disabled || vars.mouseXPosition === 0 || !vars.$target ) { return; }
				if ( wo.resizable_throttle ) {
					clearTimeout( vars.timer );
					vars.timer = setTimeout( function() {
						ts.resizable.mouseMove( c, wo, event );
					}, isNaN( wo.resizable_throttle ) ? 5 : wo.resizable_throttle );
				} else {
					ts.resizable.mouseMove( c, wo, event );
				}
			})
			.bind( 'mouseup' + namespace, function() {
				if (!wo.resizable_.disabled) { return; }
				ts.resizable.toggleTextSelection( c, false );
				ts.resizable.stopResize( c, wo );
				ts.resizable.setHandlePosition( c, wo );
			});

		// resizeEnd event triggered by scroller widget
		$( window ).bind( 'resize' + namespace + ' resizeEnd' + namespace, function() {
			ts.resizable.setHandlePosition( c, wo );
		});

		// right click to reset columns to default widths
		c.$table
			.bind( 'columnUpdate' + namespace, function() {
				ts.resizable.setHandlePosition( c, wo );
			})
			.find( 'thead:first' )
			.add( $( c.namespace + '_extra_table' ).find( 'thead:first' ) )
			.bind( 'contextmenu' + namespace, function() {
				// $.isEmptyObject() needs jQuery 1.4+; allow right click if already reset
				var allowClick = wo.resizable_.storedSizes.length === 0;
				ts.resizableReset( c.table );
				ts.resizable.setHandlePosition( c, wo );
				wo.resizable_.storedSizes = [];
				return allowClick;
			});

	},

	mouseMove : function( c, wo, event ) {
		if ( wo.resizable_.mouseXPosition === 0 || !wo.resizable_.$target ) { return; }
		// resize columns
		var vars = wo.resizable_,
			$next = vars.$next,
			leftEdge = event.pageX - vars.mouseXPosition;
		if ( vars.fullWidth ) {
			vars.storedSizes[ vars.target ] += leftEdge;
			vars.storedSizes[ vars.next ] -= leftEdge;
			ts.resizable.setWidths( c, wo );

		} else if ( vars.overflow ) {
			c.$table.add( $( c.namespace + '_extra_table' ) ).width(function(i, w){
				return w + leftEdge;
			});
			if ( !$next.length ) {
				// if expanding right-most column, scroll the wrapper
				vars.$wrap[0].scrollLeft = c.$table.width();
			}
		} else {
			vars.storedSizes[ vars.target ] += leftEdge;
			ts.resizable.setWidths( c, wo );
		}
		vars.mouseXPosition = event.pageX;
	},

	stopResize : function( c, wo ) {
		var $this, column,
			vars = wo.resizable_;
		vars.storedSizes = [];
		if ( ts.storage ) {
			vars.storedSizes = [];
			for ( column = 0; column < c.columns; column++ ) {
				$this = c.$headerIndexed[ column ];
				vars.storedSizes[ column ] = $this.is(':visible') ? $this.width() : 0;
			}
			if ( wo.resizable !== false ) {
				// save all column widths
				ts.storage( c.table, ts.css.resizableStorage, vars.storedSizes );
			}
		}
		vars.mouseXPosition = 0;
		vars.$target = vars.$next = null;
		$(window).trigger('resize'); // will update stickyHeaders, just in case
	}
};

// this widget saves the column widths if
// $.tablesorter.storage function is included
// **************************
ts.addWidget({
	id: "resizable",
	priority: 40,
	options: {
		resizable : true,
		resizable_addLastColumn : false,
		resizable_widths : [],
		resizable_throttle : false, // set to true (5ms) or any number 0-10 range
		resizable_targetLast : false
	},
	init: function(table, thisWidget, c, wo) {
		ts.resizable.init( c, wo );
	},
	remove: function( table, c, wo, refreshing ) {
		if (wo.$resizable_container) {
			var namespace = c.namespace + 'tsresize';
			c.$table.add( $( c.namespace + '_extra_table' ) )
				.removeClass('hasResizable')
				.children( 'thead' ).unbind( 'contextmenu' + namespace );

				wo.$resizable_container.remove();
			ts.resizable.toggleTextSelection( c, false );
			ts.resizableReset( table, refreshing );
			$( document ).unbind( 'mousemove' + namespace + ' mouseup' + namespace );
		}
	}
});

ts.resizableReset = function( table, refreshing ) {
	$( table ).each(function(){
		var index, $t,
			c = this.config,
			wo = c && c.widgetOptions;
		if ( table && c && c.$headerIndexed.length ) {
			for ( index = 0; index < c.columns; index++ ) {
				$t = c.$headerIndexed[ index ];
				if ( wo.resizable_widths && wo.resizable_widths[ index ] ) {
					$t.css( 'width', wo.resizable_widths[ index ] );
				} else if ( !$t.hasClass( 'resizable-false' ) ) {
					// don't clear the width of any column that is not resizable
					$t.css( 'width', '' );
				}
			}
			// reset stickyHeader widths
			$( window ).trigger( 'resize' );
			if ( ts.storage && !refreshing ) {
				ts.storage( this, ts.css.resizableStorage, {} );
			}
		}
	});
};

})( jQuery, window );

/*! Widget: saveSort */
;(function ($) {
'use strict';
var ts = $.tablesorter || {};

// this widget saves the last sort only if the
// saveSort widget option is true AND the
// $.tablesorter.storage function is included
// **************************
ts.addWidget({
	id: 'saveSort',
	priority: 20,
	options: {
		saveSort : true
	},
	init: function(table, thisWidget, c, wo) {
		// run widget format before all other widgets are applied to the table
		thisWidget.format(table, c, wo, true);
	},
	format: function(table, c, wo, init) {
		var stored, time,
			$table = c.$table,
			saveSort = wo.saveSort !== false, // make saveSort active/inactive; default to true
			sortList = { "sortList" : c.sortList };
		if (c.debug) {
			time = new Date();
		}
		if ($table.hasClass('hasSaveSort')) {
			if (saveSort && table.hasInitialized && ts.storage) {
				ts.storage( table, 'tablesorter-savesort', sortList );
				if (c.debug) {
					ts.benchmark('saveSort widget: Saving last sort: ' + c.sortList, time);
				}
			}
		} else {
			// set table sort on initial run of the widget
			$table.addClass('hasSaveSort');
			sortList = '';
			// get data
			if (ts.storage) {
				stored = ts.storage( table, 'tablesorter-savesort' );
				sortList = (stored && stored.hasOwnProperty('sortList') && $.isArray(stored.sortList)) ? stored.sortList : '';
				if (c.debug) {
					ts.benchmark('saveSort: Last sort loaded: "' + sortList + '"', time);
				}
				$table.bind('saveSortReset', function(event) {
					event.stopPropagation();
					ts.storage( table, 'tablesorter-savesort', '' );
				});
			}
			// init is true when widget init is run, this will run this widget before all other widgets have initialized
			// this method allows using this widget in the original tablesorter plugin; but then it will run all widgets twice.
			if (init && sortList && sortList.length > 0) {
				c.sortList = sortList;
			} else if (table.hasInitialized && sortList && sortList.length > 0) {
				// update sort change
				$table.trigger('sorton', [sortList]);
			}
		}
	},
	remove: function(table, c) {
		c.$table.removeClass('hasSaveSort');
		// clear storage
		if (ts.storage) { ts.storage( table, 'tablesorter-savesort', '' ); }
	}
});

})(jQuery);

return $.tablesorter;
}));

/**!
* TableSorter 2.17.5 - Client-side table sorting with ease!
* @requires jQuery v1.2.6+
*
* Copyright (c) 2007 Christian Bach
* Examples and docs at: http://tablesorter.com
* Dual licensed under the MIT and GPL licenses:
* http://www.opensource.org/licenses/mit-license.php
* http://www.gnu.org/licenses/gpl.html
*
* @type jQuery
* @name tablesorter
* @cat Plugins/Tablesorter
* @author Christian Bach/christian.bach@polyester.se
* @contributor Rob Garrison/https://github.com/Mottie/tablesorter
*/
/*jshint browser:true, jquery:true, unused:false, expr: true */
/*global console:false, alert:false */
!(function($) {
	"use strict";
	$.extend({
		/*jshint supernew:true */
		tablesorter: new function() {

			var ts = this;

			ts.version = "2.17.5";

			ts.parsers = [];
			ts.widgets = [];
			ts.defaults = {

				// *** appearance
				theme            : 'default',  // adds tablesorter-{theme} to the table for styling
				widthFixed       : false,      // adds colgroup to fix widths of columns
				showProcessing   : false,      // show an indeterminate timer icon in the header when the table is sorted or filtered.

				headerTemplate   : '{content}',// header layout template (HTML ok); {content} = innerHTML, {icon} = <i/> (class from cssIcon)
				onRenderTemplate : null,       // function(index, template){ return template; }, (template is a string)
				onRenderHeader   : null,       // function(index){}, (nothing to return)

				// *** functionality
				cancelSelection  : true,       // prevent text selection in the header
				tabIndex         : true,       // add tabindex to header for keyboard accessibility
				dateFormat       : 'mmddyyyy', // other options: "ddmmyyy" or "yyyymmdd"
				sortMultiSortKey : 'shiftKey', // key used to select additional columns
				sortResetKey     : 'ctrlKey',  // key used to remove sorting on a column
				usNumberFormat   : true,       // false for German "1.234.567,89" or French "1 234 567,89"
				delayInit        : false,      // if false, the parsed table contents will not update until the first sort
				serverSideSorting: false,      // if true, server-side sorting should be performed because client-side sorting will be disabled, but the ui and events will still be used.

				// *** sort options
				headers          : {},         // set sorter, string, empty, locked order, sortInitialOrder, filter, etc.
				ignoreCase       : true,       // ignore case while sorting
				sortForce        : null,       // column(s) first sorted; always applied
				sortList         : [],         // Initial sort order; applied initially; updated when manually sorted
				sortAppend       : null,       // column(s) sorted last; always applied
				sortStable       : false,      // when sorting two rows with exactly the same content, the original sort order is maintained

				sortInitialOrder : 'asc',      // sort direction on first click
				sortLocaleCompare: false,      // replace equivalent character (accented characters)
				sortReset        : false,      // third click on the header will reset column to default - unsorted
				sortRestart      : false,      // restart sort to "sortInitialOrder" when clicking on previously unsorted columns

				emptyTo          : 'bottom',   // sort empty cell to bottom, top, none, zero
				stringTo         : 'max',      // sort strings in numerical column as max, min, top, bottom, zero
				textExtraction   : 'basic',    // text extraction method/function - function(node, table, cellIndex){}
				textAttribute    : 'data-text',// data-attribute that contains alternate cell text (used in textExtraction function)
				textSorter       : null,       // choose overall or specific column sorter function(a, b, direction, table, columnIndex) [alt: ts.sortText]
				numberSorter     : null,       // choose overall numeric sorter function(a, b, direction, maxColumnValue)

				// *** widget options
				widgets: [],                   // method to add widgets, e.g. widgets: ['zebra']
				widgetOptions    : {
					zebra : [ 'even', 'odd' ]    // zebra widget alternating row class names
				},
				initWidgets      : true,       // apply widgets on tablesorter initialization

				// *** callbacks
				initialized      : null,       // function(table){},

				// *** extra css class names
				tableClass       : '',
				cssAsc           : '',
				cssDesc          : '',
				cssNone          : '',
				cssHeader        : '',
				cssHeaderRow     : '',
				cssProcessing    : '', // processing icon applied to header during sort/filter

				cssChildRow      : 'tablesorter-childRow', // class name indiciating that a row is to be attached to the its parent 
				cssIcon          : 'tablesorter-icon',     //  if this class exists, a <i> will be added to the header automatically
				cssInfoBlock     : 'tablesorter-infoOnly', // don't sort tbody with this class name (only one class name allowed here!)

				// *** selectors
				selectorHeaders  : '> thead th, > thead td',
				selectorSort     : 'th, td',   // jQuery selector of content within selectorHeaders that is clickable to trigger a sort
				selectorRemove   : '.remove-me',

				// *** advanced
				debug            : false,

				// *** Internal variables
				headerList: [],
				empties: {},
				strings: {},
				parsers: []

				// deprecated; but retained for backwards compatibility
				// widgetZebra: { css: ["even", "odd"] }

			};

			// internal css classes - these will ALWAYS be added to
			// the table and MUST only contain one class name - fixes #381
			ts.css = {
				table      : 'tablesorter',
				cssHasChild: 'tablesorter-hasChildRow',
				childRow   : 'tablesorter-childRow',
				header     : 'tablesorter-header',
				headerRow  : 'tablesorter-headerRow',
				headerIn   : 'tablesorter-header-inner',
				icon       : 'tablesorter-icon',
				info       : 'tablesorter-infoOnly',
				processing : 'tablesorter-processing',
				sortAsc    : 'tablesorter-headerAsc',
				sortDesc   : 'tablesorter-headerDesc',
				sortNone   : 'tablesorter-headerUnSorted'
			};

			// labels applied to sortable headers for accessibility (aria) support
			ts.language = {
				sortAsc  : 'Ascending sort applied, ',
				sortDesc : 'Descending sort applied, ',
				sortNone : 'No sort applied, ',
				nextAsc  : 'activate to apply an ascending sort',
				nextDesc : 'activate to apply a descending sort',
				nextNone : 'activate to remove the sort'
			};

			/* debuging utils */
			function log() {
				var a = arguments[0],
					s = arguments.length > 1 ? Array.prototype.slice.call(arguments) : a;
				if (typeof console !== "undefined" && typeof console.log !== "undefined") {
					console[ /error/i.test(a) ? 'error' : /warn/i.test(a) ? 'warn' : 'log' ](s);
				} else {
					alert(s);
				}
			}

			function benchmark(s, d) {
				log(s + " (" + (new Date().getTime() - d.getTime()) + "ms)");
			}

			ts.log = log;
			ts.benchmark = benchmark;

			// $.isEmptyObject from jQuery v1.4
			function isEmptyObject(obj) {
				/*jshint forin: false */
				for (var name in obj) {
					return false;
				}
				return true;
			}

			function getElementText(table, node, cellIndex) {
				if (!node) { return ""; }
				var te, c = table.config,
					t = c.textExtraction || '',
					text = "";
				if (t === "basic") {
					// check data-attribute first
					text = $(node).attr(c.textAttribute) || node.textContent || node.innerText || $(node).text() || "";
				} else {
					if (typeof(t) === "function") {
						text = t(node, table, cellIndex);
					} else if (typeof (te = ts.getColumnData( table, t, cellIndex )) === 'function') {
						text = te(node, table, cellIndex);
					} else {
						// previous "simple" method
						text = node.textContent || node.innerText || $(node).text() || "";
					}
				}
				return $.trim(text);
			}

			function detectParserForColumn(table, rows, rowIndex, cellIndex) {
				var cur,
				i = ts.parsers.length,
				node = false,
				nodeValue = '',
				keepLooking = true;
				while (nodeValue === '' && keepLooking) {
					rowIndex++;
					if (rows[rowIndex]) {
						node = rows[rowIndex].cells[cellIndex];
						nodeValue = getElementText(table, node, cellIndex);
						if (table.config.debug) {
							log('Checking if value was empty on row ' + rowIndex + ', column: ' + cellIndex + ': "' + nodeValue + '"');
						}
					} else {
						keepLooking = false;
					}
				}
				while (--i >= 0) {
					cur = ts.parsers[i];
					// ignore the default text parser because it will always be true
					if (cur && cur.id !== 'text' && cur.is && cur.is(nodeValue, table, node)) {
						return cur;
					}
				}
				// nothing found, return the generic parser (text)
				return ts.getParserById('text');
			}

			function buildParserCache(table) {
				var c = table.config,
					// update table bodies in case we start with an empty table
					tb = c.$tbodies = c.$table.children('tbody:not(.' + c.cssInfoBlock + ')'),
					rows, list, l, i, h, ch, np, p, time,
					j = 0,
					parsersDebug = "",
					len = tb.length;
				if ( len === 0) {
					return c.debug ? log('Warning: *Empty table!* Not building a parser cache') : '';
				} else if (c.debug) {
					time = new Date();
					log('Detecting parsers for each column');
				}
				list = [];
				while (j < len) {
					rows = tb[j].rows;
					if (rows[j]) {
						l = c.columns; // rows[j].cells.length;
						for (i = 0; i < l; i++) {
							h = c.$headers.filter('[data-column="' + i + '"]:last');
							// get column indexed table cell
							ch = ts.getColumnData( table, c.headers, i );
							// get column parser
							p = ts.getParserById( ts.getData(h, ch, 'sorter') );
							np = ts.getData(h, ch, 'parser') === 'false';
							// empty cells behaviour - keeping emptyToBottom for backwards compatibility
							c.empties[i] = ts.getData(h, ch, 'empty') || c.emptyTo || (c.emptyToBottom ? 'bottom' : 'top' );
							// text strings behaviour in numerical sorts
							c.strings[i] = ts.getData(h, ch, 'string') || c.stringTo || 'max';
							if (np) {
								p = ts.getParserById('no-parser');
							}
							if (!p) {
								p = detectParserForColumn(table, rows, -1, i);
							}
							if (c.debug) {
								parsersDebug += "column:" + i + "; parser:" + p.id + "; string:" + c.strings[i] + '; empty: ' + c.empties[i] + "\n";
							}
							list[i] = p;
						}
					}
					j += (list.length) ? len : 1;
				}
				if (c.debug) {
					log(parsersDebug ? parsersDebug : "No parsers detected");
					benchmark("Completed detecting parsers", time);
				}
				c.parsers = list;
			}

			/* utils */
			function buildCache(table) {
				var cc, t, v, i, j, k, $row, rows, cols, cacheTime,
					totalRows, rowData, colMax,
					c = table.config,
					$tb = c.$table.children('tbody'),
				parsers = c.parsers;
				c.cache = {};
				c.totalRows = 0;
				// if no parsers found, return - it's an empty table.
				if (!parsers) {
					return c.debug ? log('Warning: *Empty table!* Not building a cache') : '';
				}
				if (c.debug) {
					cacheTime = new Date();
				}
				// processing icon
				if (c.showProcessing) {
					ts.isProcessing(table, true);
				}
				for (k = 0; k < $tb.length; k++) {
					colMax = []; // column max value per tbody
					cc = c.cache[k] = {
						normalized: [] // array of normalized row data; last entry contains "rowData" above
						// colMax: #   // added at the end
					};

					// ignore tbodies with class name from c.cssInfoBlock
					if (!$tb.eq(k).hasClass(c.cssInfoBlock)) {
						totalRows = ($tb[k] && $tb[k].rows.length) || 0;
						for (i = 0; i < totalRows; ++i) {
							rowData = {
								// order: original row order #
								// $row : jQuery Object[]
								child: [] // child row text (filter widget)
							};
							/** Add the table data to main data array */
							$row = $($tb[k].rows[i]);
							rows = [ new Array(c.columns) ];
							cols = [];
							// if this is a child row, add it to the last row's children and continue to the next row
							// ignore child row class, if it is the first row
							if ($row.hasClass(c.cssChildRow) && i !== 0) {
								t = cc.normalized.length - 1;
								cc.normalized[t][c.columns].$row = cc.normalized[t][c.columns].$row.add($row);
								// add "hasChild" class name to parent row
								if (!$row.prev().hasClass(c.cssChildRow)) {
									$row.prev().addClass(ts.css.cssHasChild);
								}
								// save child row content (un-parsed!)
								rowData.child[t] = $.trim( $row[0].textContent || $row[0].innerText || $row.text() || "" );
								// go to the next for loop
								continue;
							}
							rowData.$row = $row;
							rowData.order = i; // add original row position to rowCache
							for (j = 0; j < c.columns; ++j) {
								if (typeof parsers[j] === 'undefined') {
									if (c.debug) {
										log('No parser found for cell:', $row[0].cells[j], 'does it have a header?');
									}
									continue;
								}
								t = getElementText(table, $row[0].cells[j], j);
								// allow parsing if the string is empty, previously parsing would change it to zero,
								// in case the parser needs to extract data from the table cell attributes
								v = parsers[j].id === 'no-parser' ? '' : parsers[j].format(t, table, $row[0].cells[j], j);
								cols.push(v);
								if ((parsers[j].type || '').toLowerCase() === "numeric") {
									// determine column max value (ignore sign)
									colMax[j] = Math.max(Math.abs(v) || 0, colMax[j] || 0);
								}
							}
							// ensure rowData is always in the same location (after the last column)
							cols[c.columns] = rowData;
							cc.normalized.push(cols);
						}
						cc.colMax = colMax;
						// total up rows, not including child rows
						c.totalRows += cc.normalized.length;
					}
				}
				if (c.showProcessing) {
					ts.isProcessing(table); // remove processing icon
				}
				if (c.debug) {
					benchmark("Building cache for " + totalRows + " rows", cacheTime);
				}
			}

			// init flag (true) used by pager plugin to prevent widget application
			function appendToTable(table, init) {
				var c = table.config,
					wo = c.widgetOptions,
					b = table.tBodies,
					rows = [],
					cc = c.cache,
					n, totalRows, $bk, $tb,
					i, k, appendTime;
				// empty table - fixes #206/#346
				if (isEmptyObject(cc)) {
					// run pager appender in case the table was just emptied
					return c.appender ? c.appender(table, rows) :
						table.isUpdating ? c.$table.trigger("updateComplete", table) : ''; // Fixes #532
				}
				if (c.debug) {
					appendTime = new Date();
				}
				for (k = 0; k < b.length; k++) {
					$bk = $(b[k]);
					if ($bk.length && !$bk.hasClass(c.cssInfoBlock)) {
						// get tbody
						$tb = ts.processTbody(table, $bk, true);
						n = cc[k].normalized;
						totalRows = n.length;
						for (i = 0; i < totalRows; i++) {
							rows.push(n[i][c.columns].$row);
							// removeRows used by the pager plugin; don't render if using ajax - fixes #411
							if (!c.appender || (c.pager && (!c.pager.removeRows || !wo.pager_removeRows) && !c.pager.ajax)) {
								$tb.append(n[i][c.columns].$row);
							}
						}
						// restore tbody
						ts.processTbody(table, $tb, false);
					}
				}
				if (c.appender) {
					c.appender(table, rows);
				}
				if (c.debug) {
					benchmark("Rebuilt table", appendTime);
				}
				// apply table widgets; but not before ajax completes
				if (!init && !c.appender) { ts.applyWidget(table); }
				if (table.isUpdating) {
					c.$table.trigger("updateComplete", table);
				}
			}

			function formatSortingOrder(v) {
				// look for "d" in "desc" order; return true
				return (/^d/i.test(v) || v === 1);
			}

			function buildHeaders(table) {
				var ch, $t,
					h, i, t, lock, time,
					c = table.config;
				c.headerList = [];
				c.headerContent = [];
				if (c.debug) {
					time = new Date();
				}
				// children tr in tfoot - see issue #196 & #547
				c.columns = ts.computeColumnIndex( c.$table.children('thead, tfoot').children('tr') );
				// add icon if cssIcon option exists
				i = c.cssIcon ? '<i class="' + ( c.cssIcon === ts.css.icon ? ts.css.icon : c.cssIcon + ' ' + ts.css.icon ) + '"></i>' : '';
				c.$headers.each(function(index) {
					$t = $(this);
					// make sure to get header cell & not column indexed cell
					ch = ts.getColumnData( table, c.headers, index, true );
					// save original header content
					c.headerContent[index] = $(this).html();
					// set up header template
					t = c.headerTemplate.replace(/\{content\}/g, $(this).html()).replace(/\{icon\}/g, i);
					if (c.onRenderTemplate) {
						h = c.onRenderTemplate.apply($t, [index, t]);
						if (h && typeof h === 'string') { t = h; } // only change t if something is returned
					}
					$(this).html('<div class="' + ts.css.headerIn + '">' + t + '</div>'); // faster than wrapInner

					if (c.onRenderHeader) { c.onRenderHeader.apply($t, [index]); }
					this.column = parseInt( $(this).attr('data-column'), 10);
					this.order = formatSortingOrder( ts.getData($t, ch, 'sortInitialOrder') || c.sortInitialOrder ) ? [1,0,2] : [0,1,2];
					this.count = -1; // set to -1 because clicking on the header automatically adds one
					this.lockedOrder = false;
					lock = ts.getData($t, ch, 'lockedOrder') || false;
					if (typeof lock !== 'undefined' && lock !== false) {
						this.order = this.lockedOrder = formatSortingOrder(lock) ? [1,1,1] : [0,0,0];
					}
					$t.addClass(ts.css.header + ' ' + c.cssHeader);
					// add cell to headerList
					c.headerList[index] = this;
					// add to parent in case there are multiple rows
					$t.parent().addClass(ts.css.headerRow + ' ' + c.cssHeaderRow).attr('role', 'row');
					// allow keyboard cursor to focus on element
					if (c.tabIndex) { $t.attr("tabindex", 0); }
				}).attr({
					scope: 'col',
					role : 'columnheader'
				});
				// enable/disable sorting
				updateHeader(table);
				if (c.debug) {
					benchmark("Built headers:", time);
					log(c.$headers);
				}
			}

			function commonUpdate(table, resort, callback) {
				var c = table.config;
				// remove rows/elements before update
				c.$table.find(c.selectorRemove).remove();
				// rebuild parsers
				buildParserCache(table);
				// rebuild the cache map
				buildCache(table);
				checkResort(c.$table, resort, callback);
			}

			function updateHeader(table) {
				var s, $th,
					c = table.config;
				c.$headers.each(function(index, th){
					$th = $(th);
					s = ts.getData( th, ts.getColumnData( table, c.headers, index, true ), 'sorter' ) === 'false';
					th.sortDisabled = s;
					$th[ s ? 'addClass' : 'removeClass' ]('sorter-false').attr('aria-disabled', '' + s);
					// aria-controls - requires table ID
					if (table.id) {
						if (s) {
							$th.removeAttr('aria-controls');
						} else {
							$th.attr('aria-controls', table.id);
						}
					}
				});
			}

			function setHeadersCss(table) {
				var f, i, j,
					c = table.config,
					list = c.sortList,
					len = list.length,
					none = ts.css.sortNone + ' ' + c.cssNone,
					css = [ts.css.sortAsc + ' ' + c.cssAsc, ts.css.sortDesc + ' ' + c.cssDesc],
					aria = ['ascending', 'descending'],
					// find the footer
					$t = $(table).find('tfoot tr').children().add(c.$extraHeaders).removeClass(css.join(' '));
				// remove all header information
				c.$headers
					.removeClass(css.join(' '))
					.addClass(none).attr('aria-sort', 'none');
				for (i = 0; i < len; i++) {
					// direction = 2 means reset!
					if (list[i][1] !== 2) {
						// multicolumn sorting updating - choose the :last in case there are nested columns
						f = c.$headers.not('.sorter-false').filter('[data-column="' + list[i][0] + '"]' + (len === 1 ? ':last' : '') );
						if (f.length) {
							for (j = 0; j < f.length; j++) {
								if (!f[j].sortDisabled) {
									f.eq(j).removeClass(none).addClass(css[list[i][1]]).attr('aria-sort', aria[list[i][1]]);
								}
							}
							// add sorted class to footer & extra headers, if they exist
							if ($t.length) {
								$t.filter('[data-column="' + list[i][0] + '"]').removeClass(none).addClass(css[list[i][1]]);
							}
						}
					}
				}
				// add verbose aria labels
				c.$headers.not('.sorter-false').each(function(){
					var $this = $(this),
						nextSort = this.order[(this.count + 1) % (c.sortReset ? 3 : 2)],
						txt = $this.text() + ': ' +
							ts.language[ $this.hasClass(ts.css.sortAsc) ? 'sortAsc' : $this.hasClass(ts.css.sortDesc) ? 'sortDesc' : 'sortNone' ] +
							ts.language[ nextSort === 0 ? 'nextAsc' : nextSort === 1 ? 'nextDesc' : 'nextNone' ];
					$this.attr('aria-label', txt );
				});
			}

			// automatically add col group, and column sizes if set
			function fixColumnWidth(table) {
				if (table.config.widthFixed && $(table).find('colgroup').length === 0) {
					var colgroup = $('<colgroup>'),
						overallWidth = $(table).width();
					// only add col for visible columns - fixes #371
					$(table.tBodies[0]).find("tr:first").children("td:visible").each(function() {
						colgroup.append($('<col>').css('width', parseInt(($(this).width()/overallWidth)*1000, 10)/10 + '%'));
					});
					$(table).prepend(colgroup);
				}
			}

			function updateHeaderSortCount(table, list) {
				var s, t, o, col, primary,
					c = table.config,
					sl = list || c.sortList;
				c.sortList = [];
				$.each(sl, function(i,v){
					// ensure all sortList values are numeric - fixes #127
					col = parseInt(v[0], 10);
					// make sure header exists
					o = c.$headers.filter('[data-column="' + col + '"]:last')[0];
					if (o) { // prevents error if sorton array is wrong
						// o.count = o.count + 1;
						t = ('' + v[1]).match(/^(1|d|s|o|n)/);
						t = t ? t[0] : '';
						// 0/(a)sc (default), 1/(d)esc, (s)ame, (o)pposite, (n)ext
						switch(t) {
							case '1': case 'd': // descending
								t = 1;
								break;
							case 's': // same direction (as primary column)
								// if primary sort is set to "s", make it ascending
								t = primary || 0;
								break;
							case 'o':
								s = o.order[(primary || 0) % (c.sortReset ? 3 : 2)];
								// opposite of primary column; but resets if primary resets
								t = s === 0 ? 1 : s === 1 ? 0 : 2;
								break;
							case 'n':
								o.count = o.count + 1;
								t = o.order[(o.count) % (c.sortReset ? 3 : 2)];
								break;
							default: // ascending
								t = 0;
								break;
						}
						primary = i === 0 ? t : primary;
						s = [ col, parseInt(t, 10) || 0 ];
						c.sortList.push(s);
						t = $.inArray(s[1], o.order); // fixes issue #167
						o.count = t >= 0 ? t : s[1] % (c.sortReset ? 3 : 2);
					}
				});
			}

			function getCachedSortType(parsers, i) {
				return (parsers && parsers[i]) ? parsers[i].type || '' : '';
			}

			function initSort(table, cell, event){
				var arry, indx, col, order, s,
					c = table.config,
					key = !event[c.sortMultiSortKey],
					$table = c.$table;
				// Only call sortStart if sorting is enabled
				$table.trigger("sortStart", table);
				// get current column sort order
				cell.count = event[c.sortResetKey] ? 2 : (cell.count + 1) % (c.sortReset ? 3 : 2);
				// reset all sorts on non-current column - issue #30
				if (c.sortRestart) {
					indx = cell;
					c.$headers.each(function() {
						// only reset counts on columns that weren't just clicked on and if not included in a multisort
						if (this !== indx && (key || !$(this).is('.' + ts.css.sortDesc + ',.' + ts.css.sortAsc))) {
							this.count = -1;
						}
					});
				}
				// get current column index
				indx = cell.column;
				// user only wants to sort on one column
				if (key) {
					// flush the sort list
					c.sortList = [];
					if (c.sortForce !== null) {
						arry = c.sortForce;
						for (col = 0; col < arry.length; col++) {
							if (arry[col][0] !== indx) {
								c.sortList.push(arry[col]);
							}
						}
					}
					// add column to sort list
					order = cell.order[cell.count];
					if (order < 2) {
						c.sortList.push([indx, order]);
						// add other columns if header spans across multiple
						if (cell.colSpan > 1) {
							for (col = 1; col < cell.colSpan; col++) {
								c.sortList.push([indx + col, order]);
							}
						}
					}
					// multi column sorting
				} else {
					// get rid of the sortAppend before adding more - fixes issue #115 & #523
					if (c.sortAppend && c.sortList.length > 1) {
						for (col = 0; col < c.sortAppend.length; col++) {
							s = ts.isValueInArray(c.sortAppend[col][0], c.sortList);
							if (s >= 0) {
								c.sortList.splice(s,1);
							}
						}
					}
					// the user has clicked on an already sorted column
					if (ts.isValueInArray(indx, c.sortList) >= 0) {
						// reverse the sorting direction
						for (col = 0; col < c.sortList.length; col++) {
							s = c.sortList[col];
							order = c.$headers.filter('[data-column="' + s[0] + '"]:last')[0];
							if (s[0] === indx) {
								// order.count seems to be incorrect when compared to cell.count
								s[1] = order.order[cell.count];
								if (s[1] === 2) {
									c.sortList.splice(col,1);
									order.count = -1;
								}
							}
						}
					} else {
						// add column to sort list array
						order = cell.order[cell.count];
						if (order < 2) {
							c.sortList.push([indx, order]);
							// add other columns if header spans across multiple
							if (cell.colSpan > 1) {
								for (col = 1; col < cell.colSpan; col++) {
									c.sortList.push([indx + col, order]);
								}
							}
						}
					}
				}
				if (c.sortAppend !== null) {
					arry = c.sortAppend;
					for (col = 0; col < arry.length; col++) {
						if (arry[col][0] !== indx) {
							c.sortList.push(arry[col]);
						}
					}
				}
				// sortBegin event triggered immediately before the sort
				$table.trigger("sortBegin", table);
				// setTimeout needed so the processing icon shows up
				setTimeout(function(){
					// set css for headers
					setHeadersCss(table);
					multisort(table);
					appendToTable(table);
					$table.trigger("sortEnd", table);
				}, 1);
			}

			// sort multiple columns
			function multisort(table) { /*jshint loopfunc:true */
				var i, k, num, col, sortTime, colMax,
					cache, order, sort, x, y,
					dir = 0,
					c = table.config,
					cts = c.textSorter || '',
					sortList = c.sortList,
					l = sortList.length,
					bl = table.tBodies.length;
				if (c.serverSideSorting || isEmptyObject(c.cache)) { // empty table - fixes #206/#346
					return;
				}
				if (c.debug) { sortTime = new Date(); }
				for (k = 0; k < bl; k++) {
					colMax = c.cache[k].colMax;
					cache = c.cache[k].normalized;

					cache.sort(function(a, b) {
						// cache is undefined here in IE, so don't use it!
						for (i = 0; i < l; i++) {
							col = sortList[i][0];
							order = sortList[i][1];
							// sort direction, true = asc, false = desc
							dir = order === 0;

							if (c.sortStable && a[col] === b[col] && l === 1) {
								return a[c.columns].order - b[c.columns].order;
							}

							// fallback to natural sort since it is more robust
							num = /n/i.test(getCachedSortType(c.parsers, col));
							if (num && c.strings[col]) {
								// sort strings in numerical columns
								if (typeof (c.string[c.strings[col]]) === 'boolean') {
									num = (dir ? 1 : -1) * (c.string[c.strings[col]] ? -1 : 1);
								} else {
									num = (c.strings[col]) ? c.string[c.strings[col]] || 0 : 0;
								}
								// fall back to built-in numeric sort
								// var sort = $.tablesorter["sort" + s](table, a[c], b[c], c, colMax[c], dir);
								sort = c.numberSorter ? c.numberSorter(a[col], b[col], dir, colMax[col], table) :
									ts[ 'sortNumeric' + (dir ? 'Asc' : 'Desc') ](a[col], b[col], num, colMax[col], col, table);
							} else {
								// set a & b depending on sort direction
								x = dir ? a : b;
								y = dir ? b : a;
								// text sort function
								if (typeof(cts) === 'function') {
									// custom OVERALL text sorter
									sort = cts(x[col], y[col], dir, col, table);
								} else if (typeof(cts) === 'object' && cts.hasOwnProperty(col)) {
									// custom text sorter for a SPECIFIC COLUMN
									sort = cts[col](x[col], y[col], dir, col, table);
								} else {
									// fall back to natural sort
									sort = ts[ 'sortNatural' + (dir ? 'Asc' : 'Desc') ](a[col], b[col], col, table, c);
								}
							}
							if (sort) { return sort; }
						}
						return a[c.columns].order - b[c.columns].order;
					});
				}
				if (c.debug) { benchmark("Sorting on " + sortList.toString() + " and dir " + order + " time", sortTime); }
			}

			function resortComplete($table, callback){
				var table = $table[0];
				if (table.isUpdating) {
					$table.trigger('updateComplete');
				}
				if ($.isFunction(callback)) {
					callback($table[0]);
				}
			}

			function checkResort($table, flag, callback) {
				var sl = $table[0].config.sortList;
				// don't try to resort if the table is still processing
				// this will catch spamming of the updateCell method
				if (flag !== false && !$table[0].isProcessing && sl.length) {
					$table.trigger("sorton", [sl, function(){
						resortComplete($table, callback);
					}, true]);
				} else {
					resortComplete($table, callback);
					ts.applyWidget($table[0], false);
				}
			}

			function bindMethods(table){
				var c = table.config,
					$table = c.$table;
				// apply easy methods that trigger bound events
				$table
				.unbind('sortReset update updateRows updateCell updateAll addRows updateComplete sorton appendCache updateCache applyWidgetId applyWidgets refreshWidgets destroy mouseup mouseleave '.split(' ').join(c.namespace + ' '))
				.bind("sortReset" + c.namespace, function(e, callback){
					e.stopPropagation();
					c.sortList = [];
					setHeadersCss(table);
					multisort(table);
					appendToTable(table);
					if ($.isFunction(callback)) {
						callback(table);
					}
				})
				.bind("updateAll" + c.namespace, function(e, resort, callback){
					e.stopPropagation();
					table.isUpdating = true;
					ts.refreshWidgets(table, true, true);
					ts.restoreHeaders(table);
					buildHeaders(table);
					ts.bindEvents(table, c.$headers, true);
					bindMethods(table);
					commonUpdate(table, resort, callback);
				})
				.bind("update" + c.namespace + " updateRows" + c.namespace, function(e, resort, callback) {
					e.stopPropagation();
					table.isUpdating = true;
					// update sorting (if enabled/disabled)
					updateHeader(table);
					commonUpdate(table, resort, callback);
				})
				.bind("updateCell" + c.namespace, function(e, cell, resort, callback) {
					e.stopPropagation();
					table.isUpdating = true;
					$table.find(c.selectorRemove).remove();
					// get position from the dom
					var v, row, icell,
					$tb = $table.find('tbody'),
					$cell = $(cell),
					// update cache - format: function(s, table, cell, cellIndex)
					// no closest in jQuery v1.2.6 - tbdy = $tb.index( $(cell).closest('tbody') ),$row = $(cell).closest('tr');
					tbdy = $tb.index( $.fn.closest ? $cell.closest('tbody') : $cell.parents('tbody').filter(':first') ),
					$row = $.fn.closest ? $cell.closest('tr') : $cell.parents('tr').filter(':first');
					cell = $cell[0]; // in case cell is a jQuery object
					// tbody may not exist if update is initialized while tbody is removed for processing
					if ($tb.length && tbdy >= 0) {
						row = $tb.eq(tbdy).find('tr').index( $row );
						icell = $cell.index();
						c.cache[tbdy].normalized[row][c.columns].$row = $row;
						v = c.cache[tbdy].normalized[row][icell] = c.parsers[icell].id === 'no-parser' ? '' :
							c.parsers[icell].format( getElementText(table, cell, icell), table, cell, icell );
						if ((c.parsers[icell].type || '').toLowerCase() === "numeric") {
							// update column max value (ignore sign)
							c.cache[tbdy].colMax[icell] = Math.max(Math.abs(v) || 0, c.cache[tbdy].colMax[icell] || 0);
						}
						checkResort($table, resort, callback);
					}
				})
				.bind("addRows" + c.namespace, function(e, $row, resort, callback) {
					e.stopPropagation();
					table.isUpdating = true;
					if (isEmptyObject(c.cache)) {
						// empty table, do an update instead - fixes #450
						updateHeader(table);
						commonUpdate(table, resort, callback);
					} else {
						$row = $($row); // make sure we're using a jQuery object
						var i, j, l, rowData, cells,
						rows = $row.filter('tr').length,
						tbdy = $table.find('tbody').index( $row.parents('tbody').filter(':first') );
						// fixes adding rows to an empty table - see issue #179
						if (!(c.parsers && c.parsers.length)) {
							buildParserCache(table);
						}
						// add each row
						for (i = 0; i < rows; i++) {
							l = $row[i].cells.length;
							cells = [];
							rowData = {
								child: [],
								$row : $row.eq(i),
								order: c.cache[tbdy].normalized.length
							};
							// add each cell
							for (j = 0; j < l; j++) {
								cells[j] = c.parsers[j].id === 'no-parser' ? '' :
									c.parsers[j].format( getElementText(table, $row[i].cells[j], j), table, $row[i].cells[j], j );
								if ((c.parsers[j].type || '').toLowerCase() === "numeric") {
									// update column max value (ignore sign)
									c.cache[tbdy].colMax[j] = Math.max(Math.abs(cells[j]) || 0, c.cache[tbdy].colMax[j] || 0);
								}
							}
							// add the row data to the end
							cells.push(rowData);
							// update cache
							c.cache[tbdy].normalized.push(cells);
						}
						// resort using current settings
						checkResort($table, resort, callback);
					}
				})
				.bind("updateComplete" + c.namespace, function(){
					table.isUpdating = false;
				})
				.bind("sorton" + c.namespace, function(e, list, callback, init) {
					var c = table.config;
					e.stopPropagation();
					$table.trigger("sortStart", this);
					// update header count index
					updateHeaderSortCount(table, list);
					// set css for headers
					setHeadersCss(table);
					// fixes #346
					if (c.delayInit && isEmptyObject(c.cache)) { buildCache(table); }
					$table.trigger("sortBegin", this);
					// sort the table and append it to the dom
					multisort(table);
					appendToTable(table, init);
					$table.trigger("sortEnd", this);
					ts.applyWidget(table);
					if ($.isFunction(callback)) {
						callback(table);
					}
				})
				.bind("appendCache" + c.namespace, function(e, callback, init) {
					e.stopPropagation();
					appendToTable(table, init);
					if ($.isFunction(callback)) {
						callback(table);
					}
				})
				.bind("updateCache" + c.namespace, function(e, callback){
					// rebuild parsers
					if (!(c.parsers && c.parsers.length)) {
						buildParserCache(table);
					}
					// rebuild the cache map
					buildCache(table);
					if ($.isFunction(callback)) {
						callback(table);
					}
				})
				.bind("applyWidgetId" + c.namespace, function(e, id) {
					e.stopPropagation();
					ts.getWidgetById(id).format(table, c, c.widgetOptions);
				})
				.bind("applyWidgets" + c.namespace, function(e, init) {
					e.stopPropagation();
					// apply widgets
					ts.applyWidget(table, init);
				})
				.bind("refreshWidgets" + c.namespace, function(e, all, dontapply){
					e.stopPropagation();
					ts.refreshWidgets(table, all, dontapply);
				})
				.bind("destroy" + c.namespace, function(e, c, cb){
					e.stopPropagation();
					ts.destroy(table, c, cb);
				})
				.bind("resetToLoadState" + c.namespace, function(){
					// remove all widgets
					ts.refreshWidgets(table, true, true);
					// restore original settings; this clears out current settings, but does not clear
					// values saved to storage.
					c = $.extend(true, ts.defaults, c.originalSettings);
					table.hasInitialized = false;
					// setup the entire table again
					ts.setup( table, c );
				});
			}

			/* public methods */
			ts.construct = function(settings) {
				return this.each(function() {
					var table = this,
						// merge & extend config options
						c = $.extend(true, {}, ts.defaults, settings);
						// save initial settings
						c.originalSettings = settings;
					// create a table from data (build table widget)
					if (!table.hasInitialized && ts.buildTable && this.tagName !== 'TABLE') {
						// return the table (in case the original target is the table's container)
						ts.buildTable(table, c);
					} else {
						ts.setup(table, c);
					}
				});
			};

			ts.setup = function(table, c) {
				// if no thead or tbody, or tablesorter is already present, quit
				if (!table || !table.tHead || table.tBodies.length === 0 || table.hasInitialized === true) {
					return c.debug ? log('ERROR: stopping initialization! No table, thead, tbody or tablesorter has already been initialized') : '';
				}

				var k = '',
					$table = $(table),
					m = $.metadata;
				// initialization flag
				table.hasInitialized = false;
				// table is being processed flag
				table.isProcessing = true;
				// make sure to store the config object
				table.config = c;
				// save the settings where they read
				$.data(table, "tablesorter", c);
				if (c.debug) { $.data( table, 'startoveralltimer', new Date()); }

				// removing this in version 3 (only supports jQuery 1.7+)
				c.supportsDataObject = (function(version) {
					version[0] = parseInt(version[0], 10);
					return (version[0] > 1) || (version[0] === 1 && parseInt(version[1], 10) >= 4);
				})($.fn.jquery.split("."));
				// digit sort text location; keeping max+/- for backwards compatibility
				c.string = { 'max': 1, 'min': -1, 'emptyMin': 1, 'emptyMax': -1, 'zero': 0, 'none': 0, 'null': 0, 'top': true, 'bottom': false };
				// add table theme class only if there isn't already one there
				if (!/tablesorter\-/.test($table.attr('class'))) {
					k = (c.theme !== '' ? ' tablesorter-' + c.theme : '');
				}
				c.table = table;
				c.$table = $table
					.addClass(ts.css.table + ' ' + c.tableClass + k)
					.attr({ role : 'grid'});
				c.$headers = $(table).find(c.selectorHeaders);

				// give the table a unique id, which will be used in namespace binding
				if (!c.namespace) {
					c.namespace = '.tablesorter' + Math.random().toString(16).slice(2);
				} else {
					// make sure namespace starts with a period & doesn't have weird characters
					c.namespace = '.' + c.namespace.replace(/\W/g,'');
				}

				c.$tbodies = $table.children('tbody:not(.' + c.cssInfoBlock + ')').attr({
					'aria-live' : 'polite',
					'aria-relevant' : 'all'
				});
				if (c.$table.find('caption').length) {
					c.$table.attr('aria-labelledby', 'theCaption');
				}
				c.widgetInit = {}; // keep a list of initialized widgets
				// change textExtraction via data-attribute
				c.textExtraction = c.$table.attr('data-text-extraction') || c.textExtraction || 'basic';
				// build headers
				buildHeaders(table);
				// fixate columns if the users supplies the fixedWidth option
				// do this after theme has been applied
				fixColumnWidth(table);
				// try to auto detect column type, and store in tables config
				buildParserCache(table);
				// start total row count at zero
				c.totalRows = 0;
				// build the cache for the tbody cells
				// delayInit will delay building the cache until the user starts a sort
				if (!c.delayInit) { buildCache(table); }
				// bind all header events and methods
				ts.bindEvents(table, c.$headers, true);
				bindMethods(table);
				// get sort list from jQuery data or metadata
				// in jQuery < 1.4, an error occurs when calling $table.data()
				if (c.supportsDataObject && typeof $table.data().sortlist !== 'undefined') {
					c.sortList = $table.data().sortlist;
				} else if (m && ($table.metadata() && $table.metadata().sortlist)) {
					c.sortList = $table.metadata().sortlist;
				}
				// apply widget init code
				ts.applyWidget(table, true);
				// if user has supplied a sort list to constructor
				if (c.sortList.length > 0) {
					$table.trigger("sorton", [c.sortList, {}, !c.initWidgets, true]);
				} else {
					setHeadersCss(table);
					if (c.initWidgets) {
						// apply widget format
						ts.applyWidget(table, false);
					}
				}

				// show processesing icon
				if (c.showProcessing) {
					$table
					.unbind('sortBegin' + c.namespace + ' sortEnd' + c.namespace)
					.bind('sortBegin' + c.namespace + ' sortEnd' + c.namespace, function(e) {
						clearTimeout(c.processTimer);
						ts.isProcessing(table);
						if (e.type === 'sortBegin') {
							c.processTimer = setTimeout(function(){
								ts.isProcessing(table, true);
							}, 500);
						}
					});
				}

				// initialized
				table.hasInitialized = true;
				table.isProcessing = false;
				if (c.debug) {
					ts.benchmark("Overall initialization time", $.data( table, 'startoveralltimer'));
				}
				$table.trigger('tablesorter-initialized', table);
				if (typeof c.initialized === 'function') { c.initialized(table); }
			};

			ts.getColumnData = function(table, obj, indx, getCell){
				if (typeof obj === 'undefined' || obj === null) { return; }
				table = $(table)[0];
				var result, $h, k,
					c = table.config;
				if (obj[indx]) {
					return getCell ? obj[indx] : obj[c.$headers.index( c.$headers.filter('[data-column="' + indx + '"]:last') )];
				}
				for (k in obj) {
					if (typeof k === 'string') {
						if (getCell) {
							// get header cell
							$h = c.$headers.eq(indx).filter(k);
						} else {
							// get column indexed cell
							$h = c.$headers.filter('[data-column="' + indx + '"]:last').filter(k);
						}
						if ($h.length) {
							return obj[k];
						}
					}
				}
				return result;
			};

			// computeTableHeaderCellIndexes from:
			// http://www.javascripttoolbox.com/lib/table/examples.php
			// http://www.javascripttoolbox.com/temp/table_cellindex.html
			ts.computeColumnIndex = function(trs) {
				var matrix = [],
				lookup = {},
				cols = 0, // determine the number of columns
				i, j, k, l, $cell, cell, cells, rowIndex, cellId, rowSpan, colSpan, firstAvailCol, matrixrow;
				for (i = 0; i < trs.length; i++) {
					cells = trs[i].cells;
					for (j = 0; j < cells.length; j++) {
						cell = cells[j];
						$cell = $(cell);
						rowIndex = cell.parentNode.rowIndex;
						cellId = rowIndex + "-" + $cell.index();
						rowSpan = cell.rowSpan || 1;
						colSpan = cell.colSpan || 1;
						if (typeof(matrix[rowIndex]) === "undefined") {
							matrix[rowIndex] = [];
						}
						// Find first available column in the first row
						for (k = 0; k < matrix[rowIndex].length + 1; k++) {
							if (typeof(matrix[rowIndex][k]) === "undefined") {
								firstAvailCol = k;
								break;
							}
						}
						lookup[cellId] = firstAvailCol;
						cols = Math.max(firstAvailCol, cols);
						// add data-column
						$cell.attr({ 'data-column' : firstAvailCol }); // 'data-row' : rowIndex
						for (k = rowIndex; k < rowIndex + rowSpan; k++) {
							if (typeof(matrix[k]) === "undefined") {
								matrix[k] = [];
							}
							matrixrow = matrix[k];
							for (l = firstAvailCol; l < firstAvailCol + colSpan; l++) {
								matrixrow[l] = "x";
							}
						}
					}
				}
				// may not be accurate if # header columns !== # tbody columns
				return cols + 1; // add one because it's a zero-based index
			};

			// *** Process table ***
			// add processing indicator
			ts.isProcessing = function(table, toggle, $ths) {
				table = $(table);
				var c = table[0].config,
					// default to all headers
					$h = $ths || table.find('.' + ts.css.header);
				if (toggle) {
					// don't use sortList if custom $ths used
					if (typeof $ths !== 'undefined' && c.sortList.length > 0) {
						// get headers from the sortList
						$h = $h.filter(function(){
							// get data-column from attr to keep  compatibility with jQuery 1.2.6
							return this.sortDisabled ? false : ts.isValueInArray( parseFloat($(this).attr('data-column')), c.sortList) >= 0;
						});
					}
					table.add($h).addClass(ts.css.processing + ' ' + c.cssProcessing);
				} else {
					table.add($h).removeClass(ts.css.processing + ' ' + c.cssProcessing);
				}
			};

			// detach tbody but save the position
			// don't use tbody because there are portions that look for a tbody index (updateCell)
			ts.processTbody = function(table, $tb, getIt){
				table = $(table)[0];
				var holdr;
				if (getIt) {
					table.isProcessing = true;
					$tb.before('<span class="tablesorter-savemyplace"/>');
					holdr = ($.fn.detach) ? $tb.detach() : $tb.remove();
					return holdr;
				}
				holdr = $(table).find('span.tablesorter-savemyplace');
				$tb.insertAfter( holdr );
				holdr.remove();
				table.isProcessing = false;
			};

			ts.clearTableBody = function(table) {
				$(table)[0].config.$tbodies.children().detach();
			};

			ts.bindEvents = function(table, $headers, core){
				table = $(table)[0];
				var downTime,
					c = table.config;
				if (core !== true) {
					c.$extraHeaders = c.$extraHeaders ? c.$extraHeaders.add($headers) : $headers;
				}
				// apply event handling to headers and/or additional headers (stickyheaders, scroller, etc)
				$headers
				// http://stackoverflow.com/questions/5312849/jquery-find-self;
				.find(c.selectorSort).add( $headers.filter(c.selectorSort) )
				.unbind('mousedown mouseup sort keyup '.split(' ').join(c.namespace + ' '))
				.bind('mousedown mouseup sort keyup '.split(' ').join(c.namespace + ' '), function(e, external) {
					var cell, type = e.type;
					// only recognize left clicks or enter
					if ( ((e.which || e.button) !== 1 && !/sort|keyup/.test(type)) || (type === 'keyup' && e.which !== 13) ) {
						return;
					}
					// ignore long clicks (prevents resizable widget from initializing a sort)
					if (type === 'mouseup' && external !== true && (new Date().getTime() - downTime > 250)) { return; }
					// set timer on mousedown
					if (type === 'mousedown') {
						downTime = new Date().getTime();
						return /(input|select|button|textarea)/i.test(e.target.tagName) ? '' : !c.cancelSelection;
					}
					if (c.delayInit && isEmptyObject(c.cache)) { buildCache(table); }
					// jQuery v1.2.6 doesn't have closest()
					cell = $.fn.closest ? $(this).closest('th, td')[0] : /TH|TD/.test(this.tagName) ? this : $(this).parents('th, td')[0];
					// reference original table headers and find the same cell
					cell = c.$headers[ $headers.index( cell ) ];
					if (!cell.sortDisabled) {
						initSort(table, cell, e);
					}
				});
				if (c.cancelSelection) {
					// cancel selection
					$headers
						.attr('unselectable', 'on')
						.bind('selectstart', false)
						.css({
							'user-select': 'none',
							'MozUserSelect': 'none' // not needed for jQuery 1.8+
						});
				}
			};

			// restore headers
			ts.restoreHeaders = function(table){
				var c = $(table)[0].config;
				// don't use c.$headers here in case header cells were swapped
				c.$table.find(c.selectorHeaders).each(function(i){
					// only restore header cells if it is wrapped
					// because this is also used by the updateAll method
					if ($(this).find('.' + ts.css.headerIn).length){
						$(this).html( c.headerContent[i] );
					}
				});
			};

			ts.destroy = function(table, removeClasses, callback){
				table = $(table)[0];
				if (!table.hasInitialized) { return; }
				// remove all widgets
				ts.refreshWidgets(table, true, true);
				var $t = $(table), c = table.config,
				$h = $t.find('thead:first'),
				$r = $h.find('tr.' + ts.css.headerRow).removeClass(ts.css.headerRow + ' ' + c.cssHeaderRow),
				$f = $t.find('tfoot:first > tr').children('th, td');
				if (removeClasses === false && $.inArray('uitheme', c.widgets) >= 0) {
					// reapply uitheme classes, in case we want to maintain appearance
					$t.trigger('applyWidgetId', ['uitheme']);
					$t.trigger('applyWidgetId', ['zebra']);
				}
				// remove widget added rows, just in case
				$h.find('tr').not($r).remove();
				// disable tablesorter
				$t
					.removeData('tablesorter')
					.unbind('sortReset update updateAll updateRows updateCell addRows updateComplete sorton appendCache updateCache applyWidgetId applyWidgets refreshWidgets destroy mouseup mouseleave keypress sortBegin sortEnd resetToLoadState '.split(' ').join(c.namespace + ' '));
				c.$headers.add($f)
					.removeClass( [ts.css.header, c.cssHeader, c.cssAsc, c.cssDesc, ts.css.sortAsc, ts.css.sortDesc, ts.css.sortNone].join(' ') )
					.removeAttr('data-column')
					.removeAttr('aria-label')
					.attr('aria-disabled', 'true');
				$r.find(c.selectorSort).unbind('mousedown mouseup keypress '.split(' ').join(c.namespace + ' '));
				ts.restoreHeaders(table);
				$t.toggleClass(ts.css.table + ' ' + c.tableClass + ' tablesorter-' + c.theme, removeClasses === false);
				// clear flag in case the plugin is initialized again
				table.hasInitialized = false;
				delete table.config.cache;
				if (typeof callback === 'function') {
					callback(table);
				}
			};

			// *** sort functions ***
			// regex used in natural sort
			ts.regex = {
				chunk : /(^([+\-]?(?:0|[1-9]\d*)(?:\.\d*)?(?:[eE][+\-]?\d+)?)?$|^0x[0-9a-f]+$|\d+)/gi, // chunk/tokenize numbers & letters
				chunks: /(^\\0|\\0$)/, // replace chunks @ ends
				hex: /^0x[0-9a-f]+$/i // hex
			};

			// Natural sort - https://github.com/overset/javascript-natural-sort (date sorting removed)
			// this function will only accept strings, or you'll see "TypeError: undefined is not a function"
			// I could add a = a.toString(); b = b.toString(); but it'll slow down the sort overall
			ts.sortNatural = function(a, b) {
				if (a === b) { return 0; }
				var xN, xD, yN, yD, xF, yF, i, mx,
					r = ts.regex;
				// first try and sort Hex codes
				if (r.hex.test(b)) {
					xD = parseInt(a.match(r.hex), 16);
					yD = parseInt(b.match(r.hex), 16);
					if ( xD < yD ) { return -1; }
					if ( xD > yD ) { return 1; }
				}
				// chunk/tokenize
				xN = a.replace(r.chunk, '\\0$1\\0').replace(r.chunks, '').split('\\0');
				yN = b.replace(r.chunk, '\\0$1\\0').replace(r.chunks, '').split('\\0');
				mx = Math.max(xN.length, yN.length);
				// natural sorting through split numeric strings and default strings
				for (i = 0; i < mx; i++) {
					// find floats not starting with '0', string or 0 if not defined
					xF = isNaN(xN[i]) ? xN[i] || 0 : parseFloat(xN[i]) || 0;
					yF = isNaN(yN[i]) ? yN[i] || 0 : parseFloat(yN[i]) || 0;
					// handle numeric vs string comparison - number < string - (Kyle Adams)
					if (isNaN(xF) !== isNaN(yF)) { return (isNaN(xF)) ? 1 : -1; }
					// rely on string comparison if different types - i.e. '02' < 2 != '02' < '2'
					if (typeof xF !== typeof yF) {
						xF += '';
						yF += '';
					}
					if (xF < yF) { return -1; }
					if (xF > yF) { return 1; }
				}
				return 0;
			};

			ts.sortNaturalAsc = function(a, b, col, table, c) {
				if (a === b) { return 0; }
				var e = c.string[ (c.empties[col] || c.emptyTo ) ];
				if (a === '' && e !== 0) { return typeof e === 'boolean' ? (e ? -1 : 1) : -e || -1; }
				if (b === '' && e !== 0) { return typeof e === 'boolean' ? (e ? 1 : -1) : e || 1; }
				return ts.sortNatural(a, b);
			};

			ts.sortNaturalDesc = function(a, b, col, table, c) {
				if (a === b) { return 0; }
				var e = c.string[ (c.empties[col] || c.emptyTo ) ];
				if (a === '' && e !== 0) { return typeof e === 'boolean' ? (e ? -1 : 1) : e || 1; }
				if (b === '' && e !== 0) { return typeof e === 'boolean' ? (e ? 1 : -1) : -e || -1; }
				return ts.sortNatural(b, a);
			};

			// basic alphabetical sort
			ts.sortText = function(a, b) {
				return a > b ? 1 : (a < b ? -1 : 0);
			};

			// return text string value by adding up ascii value
			// so the text is somewhat sorted when using a digital sort
			// this is NOT an alphanumeric sort
			ts.getTextValue = function(a, num, mx) {
				if (mx) {
					// make sure the text value is greater than the max numerical value (mx)
					var i, l = a ? a.length : 0, n = mx + num;
					for (i = 0; i < l; i++) {
						n += a.charCodeAt(i);
					}
					return num * n;
				}
				return 0;
			};

			ts.sortNumericAsc = function(a, b, num, mx, col, table) {
				if (a === b) { return 0; }
				var c = table.config,
					e = c.string[ (c.empties[col] || c.emptyTo ) ];
				if (a === '' && e !== 0) { return typeof e === 'boolean' ? (e ? -1 : 1) : -e || -1; }
				if (b === '' && e !== 0) { return typeof e === 'boolean' ? (e ? 1 : -1) : e || 1; }
				if (isNaN(a)) { a = ts.getTextValue(a, num, mx); }
				if (isNaN(b)) { b = ts.getTextValue(b, num, mx); }
				return a - b;
			};

			ts.sortNumericDesc = function(a, b, num, mx, col, table) {
				if (a === b) { return 0; }
				var c = table.config,
					e = c.string[ (c.empties[col] || c.emptyTo ) ];
				if (a === '' && e !== 0) { return typeof e === 'boolean' ? (e ? -1 : 1) : e || 1; }
				if (b === '' && e !== 0) { return typeof e === 'boolean' ? (e ? 1 : -1) : -e || -1; }
				if (isNaN(a)) { a = ts.getTextValue(a, num, mx); }
				if (isNaN(b)) { b = ts.getTextValue(b, num, mx); }
				return b - a;
			};

			ts.sortNumeric = function(a, b) {
				return a - b;
			};

			// used when replacing accented characters during sorting
			ts.characterEquivalents = {
				"a" : "\u00e1\u00e0\u00e2\u00e3\u00e4\u0105\u00e5", // áàâãäąå
				"A" : "\u00c1\u00c0\u00c2\u00c3\u00c4\u0104\u00c5", // ÁÀÂÃÄĄÅ
				"c" : "\u00e7\u0107\u010d", // çćč
				"C" : "\u00c7\u0106\u010c", // ÇĆČ
				"e" : "\u00e9\u00e8\u00ea\u00eb\u011b\u0119", // éèêëěę
				"E" : "\u00c9\u00c8\u00ca\u00cb\u011a\u0118", // ÉÈÊËĚĘ
				"i" : "\u00ed\u00ec\u0130\u00ee\u00ef\u0131", // íìİîïı
				"I" : "\u00cd\u00cc\u0130\u00ce\u00cf", // ÍÌİÎÏ
				"o" : "\u00f3\u00f2\u00f4\u00f5\u00f6", // óòôõö
				"O" : "\u00d3\u00d2\u00d4\u00d5\u00d6", // ÓÒÔÕÖ
				"ss": "\u00df", // ß (s sharp)
				"SS": "\u1e9e", // ẞ (Capital sharp s)
				"u" : "\u00fa\u00f9\u00fb\u00fc\u016f", // úùûüů
				"U" : "\u00da\u00d9\u00db\u00dc\u016e" // ÚÙÛÜŮ
			};
			ts.replaceAccents = function(s) {
				var a, acc = '[', eq = ts.characterEquivalents;
				if (!ts.characterRegex) {
					ts.characterRegexArray = {};
					for (a in eq) {
						if (typeof a === 'string') {
							acc += eq[a];
							ts.characterRegexArray[a] = new RegExp('[' + eq[a] + ']', 'g');
						}
					}
					ts.characterRegex = new RegExp(acc + ']');
				}
				if (ts.characterRegex.test(s)) {
					for (a in eq) {
						if (typeof a === 'string') {
							s = s.replace( ts.characterRegexArray[a], a );
						}
					}
				}
				return s;
			};

			// *** utilities ***
			ts.isValueInArray = function(column, arry) {
				var indx, len = arry.length;
				for (indx = 0; indx < len; indx++) {
					if (arry[indx][0] === column) {
						return indx;
					}
				}
				return -1;
			};

			ts.addParser = function(parser) {
				var i, l = ts.parsers.length, a = true;
				for (i = 0; i < l; i++) {
					if (ts.parsers[i].id.toLowerCase() === parser.id.toLowerCase()) {
						a = false;
					}
				}
				if (a) {
					ts.parsers.push(parser);
				}
			};

			ts.getParserById = function(name) {
				/*jshint eqeqeq:false */
				if (name == 'false') { return false; }
				var i, l = ts.parsers.length;
				for (i = 0; i < l; i++) {
					if (ts.parsers[i].id.toLowerCase() === (name.toString()).toLowerCase()) {
						return ts.parsers[i];
					}
				}
				return false;
			};

			ts.addWidget = function(widget) {
				ts.widgets.push(widget);
			};

			ts.hasWidget = function(table, name){
				table = $(table);
				return table.length && table[0].config && table[0].config.widgetInit[name] || false;
			};

			ts.getWidgetById = function(name) {
				var i, w, l = ts.widgets.length;
				for (i = 0; i < l; i++) {
					w = ts.widgets[i];
					if (w && w.hasOwnProperty('id') && w.id.toLowerCase() === name.toLowerCase()) {
						return w;
					}
				}
			};

			ts.applyWidget = function(table, init) {
				table = $(table)[0]; // in case this is called externally
				var c = table.config,
					wo = c.widgetOptions,
					widgets = [],
					time, w, wd;
				// prevent numerous consecutive widget applications
				if (init !== false && table.hasInitialized && (table.isApplyingWidgets || table.isUpdating)) { return; }
				if (c.debug) { time = new Date(); }
				if (c.widgets.length) {
					table.isApplyingWidgets = true;
					// ensure unique widget ids
					c.widgets = $.grep(c.widgets, function(v, k){
						return $.inArray(v, c.widgets) === k;
					});
					// build widget array & add priority as needed
					$.each(c.widgets || [], function(i,n){
						wd = ts.getWidgetById(n);
						if (wd && wd.id) {
							// set priority to 10 if not defined
							if (!wd.priority) { wd.priority = 10; }
							widgets[i] = wd;
						}
					});
					// sort widgets by priority
					widgets.sort(function(a, b){
						return a.priority < b.priority ? -1 : a.priority === b.priority ? 0 : 1;
					});
					// add/update selected widgets
					$.each(widgets, function(i,w){
						if (w) {
							if (init || !(c.widgetInit[w.id])) {
								// set init flag first to prevent calling init more than once (e.g. pager)
								c.widgetInit[w.id] = true;
								if (w.hasOwnProperty('options')) {
									wo = table.config.widgetOptions = $.extend( true, {}, w.options, wo );
								}
								if (w.hasOwnProperty('init')) {
									w.init(table, w, c, wo);
								}
							}
							if (!init && w.hasOwnProperty('format')) {
								w.format(table, c, wo, false);
							}
						}
					});
				}
				setTimeout(function(){
					table.isApplyingWidgets = false;
				}, 0);
				if (c.debug) {
					w = c.widgets.length;
					benchmark("Completed " + (init === true ? "initializing " : "applying ") + w + " widget" + (w !== 1 ? "s" : ""), time);
				}
			};

			ts.refreshWidgets = function(table, doAll, dontapply) {
				table = $(table)[0]; // see issue #243
				var i, c = table.config,
					cw = c.widgets,
					w = ts.widgets, l = w.length;
				// remove previous widgets
				for (i = 0; i < l; i++){
					if ( w[i] && w[i].id && (doAll || $.inArray( w[i].id, cw ) < 0) ) {
						if (c.debug) { log( 'Refeshing widgets: Removing "' + w[i].id + '"' ); }
						// only remove widgets that have been initialized - fixes #442
						if (w[i].hasOwnProperty('remove') && c.widgetInit[w[i].id]) {
							w[i].remove(table, c, c.widgetOptions);
							c.widgetInit[w[i].id] = false;
						}
					}
				}
				if (dontapply !== true) {
					ts.applyWidget(table, doAll);
				}
			};

			// get sorter, string, empty, etc options for each column from
			// jQuery data, metadata, header option or header class name ("sorter-false")
			// priority = jQuery data > meta > headers option > header class name
			ts.getData = function(h, ch, key) {
				var val = '', $h = $(h), m, cl;
				if (!$h.length) { return ''; }
				m = $.metadata ? $h.metadata() : false;
				cl = ' ' + ($h.attr('class') || '');
				if (typeof $h.data(key) !== 'undefined' || typeof $h.data(key.toLowerCase()) !== 'undefined'){
					// "data-lockedOrder" is assigned to "lockedorder"; but "data-locked-order" is assigned to "lockedOrder"
					// "data-sort-initial-order" is assigned to "sortInitialOrder"
					val += $h.data(key) || $h.data(key.toLowerCase());
				} else if (m && typeof m[key] !== 'undefined') {
					val += m[key];
				} else if (ch && typeof ch[key] !== 'undefined') {
					val += ch[key];
				} else if (cl !== ' ' && cl.match(' ' + key + '-')) {
					// include sorter class name "sorter-text", etc; now works with "sorter-my-custom-parser"
					val = cl.match( new RegExp('\\s' + key + '-([\\w-]+)') )[1] || '';
				}
				return $.trim(val);
			};

			ts.formatFloat = function(s, table) {
				if (typeof s !== 'string' || s === '') { return s; }
				// allow using formatFloat without a table; defaults to US number format
				var i,
					t = table && table.config ? table.config.usNumberFormat !== false :
						typeof table !== "undefined" ? table : true;
				if (t) {
					// US Format - 1,234,567.89 -> 1234567.89
					s = s.replace(/,/g,'');
				} else {
					// German Format = 1.234.567,89 -> 1234567.89
					// French Format = 1 234 567,89 -> 1234567.89
					s = s.replace(/[\s|\.]/g,'').replace(/,/g,'.');
				}
				if(/^\s*\([.\d]+\)/.test(s)) {
					// make (#) into a negative number -> (10) = -10
					s = s.replace(/^\s*\(([.\d]+)\)/, '-$1');
				}
				i = parseFloat(s);
				// return the text instead of zero
				return isNaN(i) ? $.trim(s) : i;
			};

			ts.isDigit = function(s) {
				// replace all unwanted chars and match
				return isNaN(s) ? (/^[\-+(]?\d+[)]?$/).test(s.toString().replace(/[,.'"\s]/g, '')) : true;
			};

		}()
	});

	// make shortcut
	var ts = $.tablesorter;

	// extend plugin scope
	$.fn.extend({
		tablesorter: ts.construct
	});

	// add default parsers
	ts.addParser({
		id: 'no-parser',
		is: function() {
			return false;
		},
		format: function() {
			return '';
		},
		type: 'text'
	});

	ts.addParser({
		id: "text",
		is: function() {
			return true;
		},
		format: function(s, table) {
			var c = table.config;
			if (s) {
				s = $.trim( c.ignoreCase ? s.toLocaleLowerCase() : s );
				s = c.sortLocaleCompare ? ts.replaceAccents(s) : s;
			}
			return s;
		},
		type: "text"
	});

	ts.addParser({
		id: "digit",
		is: function(s) {
			return ts.isDigit(s);
		},
		format: function(s, table) {
			var n = ts.formatFloat((s || '').replace(/[^\w,. \-()]/g, ""), table);
			return s && typeof n === 'number' ? n : s ? $.trim( s && table.config.ignoreCase ? s.toLocaleLowerCase() : s ) : s;
		},
		type: "numeric"
	});

	ts.addParser({
		id: "currency",
		is: function(s) {
			return (/^\(?\d+[\u00a3$\u20ac\u00a4\u00a5\u00a2?.]|[\u00a3$\u20ac\u00a4\u00a5\u00a2?.]\d+\)?$/).test((s || '').replace(/[+\-,. ]/g,'')); // £$€¤¥¢
		},
		format: function(s, table) {
			var n = ts.formatFloat((s || '').replace(/[^\w,. \-()]/g, ""), table);
			return s && typeof n === 'number' ? n : s ? $.trim( s && table.config.ignoreCase ? s.toLocaleLowerCase() : s ) : s;
		},
		type: "numeric"
	});

	ts.addParser({
		id: "ipAddress",
		is: function(s) {
			return (/^\d{1,3}[\.]\d{1,3}[\.]\d{1,3}[\.]\d{1,3}$/).test(s);
		},
		format: function(s, table) {
			var i, a = s ? s.split(".") : '',
			r = "",
			l = a.length;
			for (i = 0; i < l; i++) {
				r += ("00" + a[i]).slice(-3);
			}
			return s ? ts.formatFloat(r, table) : s;
		},
		type: "numeric"
	});

	ts.addParser({
		id: "url",
		is: function(s) {
			return (/^(https?|ftp|file):\/\//).test(s);
		},
		format: function(s) {
			return s ? $.trim(s.replace(/(https?|ftp|file):\/\//, '')) : s;
		},
		type: "text"
	});

	ts.addParser({
		id: "isoDate",
		is: function(s) {
			return (/^\d{4}[\/\-]\d{1,2}[\/\-]\d{1,2}/).test(s);
		},
		format: function(s, table) {
			return s ? ts.formatFloat((s !== "") ? (new Date(s.replace(/-/g, "/")).getTime() || s) : "", table) : s;
		},
		type: "numeric"
	});

	ts.addParser({
		id: "percent",
		is: function(s) {
			return (/(\d\s*?%|%\s*?\d)/).test(s) && s.length < 15;
		},
		format: function(s, table) {
			return s ? ts.formatFloat(s.replace(/%/g, ""), table) : s;
		},
		type: "numeric"
	});

	ts.addParser({
		id: "usLongDate",
		is: function(s) {
			// two digit years are not allowed cross-browser
			// Jan 01, 2013 12:34:56 PM or 01 Jan 2013
			return (/^[A-Z]{3,10}\.?\s+\d{1,2},?\s+(\d{4})(\s+\d{1,2}:\d{2}(:\d{2})?(\s+[AP]M)?)?$/i).test(s) || (/^\d{1,2}\s+[A-Z]{3,10}\s+\d{4}/i).test(s);
		},
		format: function(s, table) {
			return s ? ts.formatFloat( (new Date(s.replace(/(\S)([AP]M)$/i, "$1 $2")).getTime() || s), table) : s;
		},
		type: "numeric"
	});

	ts.addParser({
		id: "shortDate", // "mmddyyyy", "ddmmyyyy" or "yyyymmdd"
		is: function(s) {
			// testing for ##-##-#### or ####-##-##, so it's not perfect; time can be included
			return (/(^\d{1,2}[\/\s]\d{1,2}[\/\s]\d{4})|(^\d{4}[\/\s]\d{1,2}[\/\s]\d{1,2})/).test((s || '').replace(/\s+/g," ").replace(/[\-.,]/g, "/"));
		},
		format: function(s, table, cell, cellIndex) {
			if (s) {
				var c = table.config,
					ci = c.$headers.filter('[data-column=' + cellIndex + ']:last'),
					format = ci.length && ci[0].dateFormat || ts.getData( ci, ts.getColumnData( table, c.headers, cellIndex ), 'dateFormat') || c.dateFormat;
				s = s.replace(/\s+/g," ").replace(/[\-.,]/g, "/"); // escaped - because JSHint in Firefox was showing it as an error
				if (format === "mmddyyyy") {
					s = s.replace(/(\d{1,2})[\/\s](\d{1,2})[\/\s](\d{4})/, "$3/$1/$2");
				} else if (format === "ddmmyyyy") {
					s = s.replace(/(\d{1,2})[\/\s](\d{1,2})[\/\s](\d{4})/, "$3/$2/$1");
				} else if (format === "yyyymmdd") {
					s = s.replace(/(\d{4})[\/\s](\d{1,2})[\/\s](\d{1,2})/, "$1/$2/$3");
				}
			}
			return s ? ts.formatFloat( (new Date(s).getTime() || s), table) : s;
		},
		type: "numeric"
	});

	ts.addParser({
		id: "time",
		is: function(s) {
			return (/^(([0-2]?\d:[0-5]\d)|([0-1]?\d:[0-5]\d\s?([AP]M)))$/i).test(s);
		},
		format: function(s, table) {
			return s ? ts.formatFloat( (new Date("2000/01/01 " + s.replace(/(\S)([AP]M)$/i, "$1 $2")).getTime() || s), table) : s;
		},
		type: "numeric"
	});

	ts.addParser({
		id: "metadata",
		is: function() {
			return false;
		},
		format: function(s, table, cell) {
			var c = table.config,
			p = (!c.parserMetadataName) ? 'sortValue' : c.parserMetadataName;
			return $(cell).metadata()[p];
		},
		type: "numeric"
	});

	// add default widgets
	ts.addWidget({
		id: "zebra",
		priority: 90,
		format: function(table, c, wo) {
			var $tb, $tv, $tr, row, even, time, k, l,
			child = new RegExp(c.cssChildRow, 'i'),
			b = c.$tbodies;
			if (c.debug) {
				time = new Date();
			}
			for (k = 0; k < b.length; k++ ) {
				// loop through the visible rows
				$tb = b.eq(k);
				l = $tb.children('tr').length;
				if (l > 1) {
					row = 0;
					$tv = $tb.children('tr:visible').not(c.selectorRemove);
					// revered back to using jQuery each - strangely it's the fastest method
					/*jshint loopfunc:true */
					$tv.each(function(){
						$tr = $(this);
						// style children rows the same way the parent row was styled
						if (!child.test(this.className)) { row++; }
						even = (row % 2 === 0);
						$tr.removeClass(wo.zebra[even ? 1 : 0]).addClass(wo.zebra[even ? 0 : 1]);
					});
				}
			}
			if (c.debug) {
				ts.benchmark("Applying Zebra widget", time);
			}
		},
		remove: function(table, c, wo){
			var k, $tb,
				b = c.$tbodies,
				rmv = (wo.zebra || [ "even", "odd" ]).join(' ');
			for (k = 0; k < b.length; k++ ){
				$tb = $.tablesorter.processTbody(table, b.eq(k), true); // remove tbody
				$tb.children().removeClass(rmv);
				$.tablesorter.processTbody(table, $tb, false); // restore tbody
			}
		}
	});

})(jQuery);

/*!
* TableSorter 2.17.5 min - Client-side table sorting with ease!
* Copyright (c) 2007 Christian Bach
*/
!function(g){g.extend({tablesorter:new function(){function d(){var b=arguments[0],a=1<arguments.length?Array.prototype.slice.call(arguments):b;if("undefined"!==typeof console&&"undefined"!==typeof console.log)console[/error/i.test(b)?"error":/warn/i.test(b)?"warn":"log"](a);else alert(a)}function v(b,a){d(b+" ("+((new Date).getTime()-a.getTime())+"ms)")}function p(b){for(var a in b)return!1;return!0}function n(b,a,c){if(!a)return"";var h,e=b.config,r=e.textExtraction||"",k="",k="basic"===r?g(a).attr(e.textAttribute)|| a.textContent||a.innerText||g(a).text()||"":"function"===typeof r?r(a,b,c):"function"===typeof(h=f.getColumnData(b,r,c))?h(a,b,c):a.textContent||a.innerText||g(a).text()||"";return g.trim(k)}function t(b){var a=b.config,c=a.$tbodies=a.$table.children("tbody:not(."+a.cssInfoBlock+")"),h,e,r,k,l,m,g,u,p,q=0,s="",t=c.length;if(0===t)return a.debug?d("Warning: *Empty table!* Not building a parser cache"):"";a.debug&&(p=new Date,d("Detecting parsers for each column"));for(e=[];q<t;){h=c[q].rows;if(h[q])for(r= a.columns,k=0;k<r;k++){l=a.$headers.filter('[data-column="'+k+'"]:last');m=f.getColumnData(b,a.headers,k);u=f.getParserById(f.getData(l,m,"sorter"));g="false"===f.getData(l,m,"parser");a.empties[k]=f.getData(l,m,"empty")||a.emptyTo||(a.emptyToBottom?"bottom":"top");a.strings[k]=f.getData(l,m,"string")||a.stringTo||"max";g&&(u=f.getParserById("no-parser"));if(!u)a:{l=b;m=h;g=-1;u=k;for(var A=void 0,x=f.parsers.length,z=!1,F="",A=!0;""===F&&A;)g++,m[g]?(z=m[g].cells[u],F=n(l,z,u),l.config.debug&&d("Checking if value was empty on row "+ g+", column: "+u+': "'+F+'"')):A=!1;for(;0<=--x;)if((A=f.parsers[x])&&"text"!==A.id&&A.is&&A.is(F,l,z)){u=A;break a}u=f.getParserById("text")}a.debug&&(s+="column:"+k+"; parser:"+u.id+"; string:"+a.strings[k]+"; empty: "+a.empties[k]+"\n");e[k]=u}q+=e.length?t:1}a.debug&&(d(s?s:"No parsers detected"),v("Completed detecting parsers",p));a.parsers=e}function x(b){var a,c,h,e,r,k,l,m,y,u,p,q=b.config,s=q.$table.children("tbody"),t=q.parsers;q.cache={};q.totalRows=0;if(!t)return q.debug?d("Warning: *Empty table!* Not building a cache"): "";q.debug&&(m=new Date);q.showProcessing&&f.isProcessing(b,!0);for(r=0;r<s.length;r++)if(p=[],a=q.cache[r]={normalized:[]},!s.eq(r).hasClass(q.cssInfoBlock)){y=s[r]&&s[r].rows.length||0;for(h=0;h<y;++h)if(u={child:[]},k=g(s[r].rows[h]),l=[],k.hasClass(q.cssChildRow)&&0!==h)c=a.normalized.length-1,a.normalized[c][q.columns].$row=a.normalized[c][q.columns].$row.add(k),k.prev().hasClass(q.cssChildRow)||k.prev().addClass(f.css.cssHasChild),u.child[c]=g.trim(k[0].textContent||k[0].innerText||k.text()|| "");else{u.$row=k;u.order=h;for(e=0;e<q.columns;++e)"undefined"===typeof t[e]?q.debug&&d("No parser found for cell:",k[0].cells[e],"does it have a header?"):(c=n(b,k[0].cells[e],e),c="no-parser"===t[e].id?"":t[e].format(c,b,k[0].cells[e],e),l.push(c),"numeric"===(t[e].type||"").toLowerCase()&&(p[e]=Math.max(Math.abs(c)||0,p[e]||0)));l[q.columns]=u;a.normalized.push(l)}a.colMax=p;q.totalRows+=a.normalized.length}q.showProcessing&&f.isProcessing(b);q.debug&&v("Building cache for "+y+" rows",m)}function z(b, a){var c=b.config,h=c.widgetOptions,e=b.tBodies,r=[],k=c.cache,d,m,y,u,n,q;if(p(k))return c.appender?c.appender(b,r):b.isUpdating?c.$table.trigger("updateComplete",b):"";c.debug&&(q=new Date);for(n=0;n<e.length;n++)if(d=g(e[n]),d.length&&!d.hasClass(c.cssInfoBlock)){y=f.processTbody(b,d,!0);d=k[n].normalized;m=d.length;for(u=0;u<m;u++)r.push(d[u][c.columns].$row),c.appender&&(!c.pager||c.pager.removeRows&&h.pager_removeRows||c.pager.ajax)||y.append(d[u][c.columns].$row);f.processTbody(b,y,!1)}c.appender&& c.appender(b,r);c.debug&&v("Rebuilt table",q);a||c.appender||f.applyWidget(b);b.isUpdating&&c.$table.trigger("updateComplete",b)}function C(b){return/^d/i.test(b)||1===b}function D(b){var a,c,h,e,r,k,l,m=b.config;m.headerList=[];m.headerContent=[];m.debug&&(l=new Date);m.columns=f.computeColumnIndex(m.$table.children("thead, tfoot").children("tr"));e=m.cssIcon?'<i class="'+(m.cssIcon===f.css.icon?f.css.icon:m.cssIcon+" "+f.css.icon)+'"></i>':"";m.$headers.each(function(d){c=g(this);a=f.getColumnData(b, m.headers,d,!0);m.headerContent[d]=g(this).html();r=m.headerTemplate.replace(/\{content\}/g,g(this).html()).replace(/\{icon\}/g,e);m.onRenderTemplate&&(h=m.onRenderTemplate.apply(c,[d,r]))&&"string"===typeof h&&(r=h);g(this).html('<div class="'+f.css.headerIn+'">'+r+"</div>");m.onRenderHeader&&m.onRenderHeader.apply(c,[d]);this.column=parseInt(g(this).attr("data-column"),10);this.order=C(f.getData(c,a,"sortInitialOrder")||m.sortInitialOrder)?[1,0,2]:[0,1,2];this.count=-1;this.lockedOrder=!1;k=f.getData(c, a,"lockedOrder")||!1;"undefined"!==typeof k&&!1!==k&&(this.order=this.lockedOrder=C(k)?[1,1,1]:[0,0,0]);c.addClass(f.css.header+" "+m.cssHeader);m.headerList[d]=this;c.parent().addClass(f.css.headerRow+" "+m.cssHeaderRow).attr("role","row");m.tabIndex&&c.attr("tabindex",0)}).attr({scope:"col",role:"columnheader"});B(b);m.debug&&(v("Built headers:",l),d(m.$headers))}function E(b,a,c){var h=b.config;h.$table.find(h.selectorRemove).remove();t(b);x(b);H(h.$table,a,c)}function B(b){var a,c,h=b.config; h.$headers.each(function(e,r){c=g(r);a="false"===f.getData(r,f.getColumnData(b,h.headers,e,!0),"sorter");r.sortDisabled=a;c[a?"addClass":"removeClass"]("sorter-false").attr("aria-disabled",""+a);b.id&&(a?c.removeAttr("aria-controls"):c.attr("aria-controls",b.id))})}function G(b){var a,c,h=b.config,e=h.sortList,r=e.length,d=f.css.sortNone+" "+h.cssNone,l=[f.css.sortAsc+" "+h.cssAsc,f.css.sortDesc+" "+h.cssDesc],m=["ascending","descending"],y=g(b).find("tfoot tr").children().add(h.$extraHeaders).removeClass(l.join(" ")); h.$headers.removeClass(l.join(" ")).addClass(d).attr("aria-sort","none");for(a=0;a<r;a++)if(2!==e[a][1]&&(b=h.$headers.not(".sorter-false").filter('[data-column="'+e[a][0]+'"]'+(1===r?":last":"")),b.length)){for(c=0;c<b.length;c++)b[c].sortDisabled||b.eq(c).removeClass(d).addClass(l[e[a][1]]).attr("aria-sort",m[e[a][1]]);y.length&&y.filter('[data-column="'+e[a][0]+'"]').removeClass(d).addClass(l[e[a][1]])}h.$headers.not(".sorter-false").each(function(){var b=g(this),a=this.order[(this.count+1)%(h.sortReset? 3:2)],a=b.text()+": "+f.language[b.hasClass(f.css.sortAsc)?"sortAsc":b.hasClass(f.css.sortDesc)?"sortDesc":"sortNone"]+f.language[0===a?"nextAsc":1===a?"nextDesc":"nextNone"];b.attr("aria-label",a)})}function L(b){if(b.config.widthFixed&&0===g(b).find("colgroup").length){var a=g("<colgroup>"),c=g(b).width();g(b.tBodies[0]).find("tr:first").children("td:visible").each(function(){a.append(g("<col>").css("width",parseInt(g(this).width()/c*1E3,10)/10+"%"))});g(b).prepend(a)}}function M(b,a){var c,h,e, f,d,l=b.config,m=a||l.sortList;l.sortList=[];g.each(m,function(b,a){f=parseInt(a[0],10);if(e=l.$headers.filter('[data-column="'+f+'"]:last')[0]){h=(h=(""+a[1]).match(/^(1|d|s|o|n)/))?h[0]:"";switch(h){case "1":case "d":h=1;break;case "s":h=d||0;break;case "o":c=e.order[(d||0)%(l.sortReset?3:2)];h=0===c?1:1===c?0:2;break;case "n":e.count+=1;h=e.order[e.count%(l.sortReset?3:2)];break;default:h=0}d=0===b?h:d;c=[f,parseInt(h,10)||0];l.sortList.push(c);h=g.inArray(c[1],e.order);e.count=0<=h?h:c[1]%(l.sortReset? 3:2)}})}function N(b,a){return b&&b[a]?b[a].type||"":""}function O(b,a,c){var h,e,d,k=b.config,l=!c[k.sortMultiSortKey],m=k.$table;m.trigger("sortStart",b);a.count=c[k.sortResetKey]?2:(a.count+1)%(k.sortReset?3:2);k.sortRestart&&(e=a,k.$headers.each(function(){this===e||!l&&g(this).is("."+f.css.sortDesc+",."+f.css.sortAsc)||(this.count=-1)}));e=a.column;if(l){k.sortList=[];if(null!==k.sortForce)for(h=k.sortForce,c=0;c<h.length;c++)h[c][0]!==e&&k.sortList.push(h[c]);h=a.order[a.count];if(2>h&&(k.sortList.push([e, h]),1<a.colSpan))for(c=1;c<a.colSpan;c++)k.sortList.push([e+c,h])}else{if(k.sortAppend&&1<k.sortList.length)for(c=0;c<k.sortAppend.length;c++)d=f.isValueInArray(k.sortAppend[c][0],k.sortList),0<=d&&k.sortList.splice(d,1);if(0<=f.isValueInArray(e,k.sortList))for(c=0;c<k.sortList.length;c++)d=k.sortList[c],h=k.$headers.filter('[data-column="'+d[0]+'"]:last')[0],d[0]===e&&(d[1]=h.order[a.count],2===d[1]&&(k.sortList.splice(c,1),h.count=-1));else if(h=a.order[a.count],2>h&&(k.sortList.push([e,h]),1<a.colSpan))for(c= 1;c<a.colSpan;c++)k.sortList.push([e+c,h])}if(null!==k.sortAppend)for(h=k.sortAppend,c=0;c<h.length;c++)h[c][0]!==e&&k.sortList.push(h[c]);m.trigger("sortBegin",b);setTimeout(function(){G(b);I(b);z(b);m.trigger("sortEnd",b)},1)}function I(b){var a,c,h,e,d,k,g,m,y,n,t,q=0,s=b.config,w=s.textSorter||"",x=s.sortList,z=x.length,B=b.tBodies.length;if(!s.serverSideSorting&&!p(s.cache)){s.debug&&(d=new Date);for(c=0;c<B;c++)k=s.cache[c].colMax,g=s.cache[c].normalized,g.sort(function(c,d){for(a=0;a<z;a++){e= x[a][0];m=x[a][1];q=0===m;if(s.sortStable&&c[e]===d[e]&&1===z)break;(h=/n/i.test(N(s.parsers,e)))&&s.strings[e]?(h="boolean"===typeof s.string[s.strings[e]]?(q?1:-1)*(s.string[s.strings[e]]?-1:1):s.strings[e]?s.string[s.strings[e]]||0:0,y=s.numberSorter?s.numberSorter(c[e],d[e],q,k[e],b):f["sortNumeric"+(q?"Asc":"Desc")](c[e],d[e],h,k[e],e,b)):(n=q?c:d,t=q?d:c,y="function"===typeof w?w(n[e],t[e],q,e,b):"object"===typeof w&&w.hasOwnProperty(e)?w[e](n[e],t[e],q,e,b):f["sortNatural"+(q?"Asc":"Desc")](c[e], d[e],e,b,s));if(y)return y}return c[s.columns].order-d[s.columns].order});s.debug&&v("Sorting on "+x.toString()+" and dir "+m+" time",d)}}function J(b,a){b[0].isUpdating&&b.trigger("updateComplete");g.isFunction(a)&&a(b[0])}function H(b,a,c){var h=b[0].config.sortList;!1!==a&&!b[0].isProcessing&&h.length?b.trigger("sorton",[h,function(){J(b,c)},!0]):(J(b,c),f.applyWidget(b[0],!1))}function K(b){var a=b.config,c=a.$table;c.unbind("sortReset update updateRows updateCell updateAll addRows updateComplete sorton appendCache updateCache applyWidgetId applyWidgets refreshWidgets destroy mouseup mouseleave ".split(" ").join(a.namespace+ " ")).bind("sortReset"+a.namespace,function(c,e){c.stopPropagation();a.sortList=[];G(b);I(b);z(b);g.isFunction(e)&&e(b)}).bind("updateAll"+a.namespace,function(c,e,d){c.stopPropagation();b.isUpdating=!0;f.refreshWidgets(b,!0,!0);f.restoreHeaders(b);D(b);f.bindEvents(b,a.$headers,!0);K(b);E(b,e,d)}).bind("update"+a.namespace+" updateRows"+a.namespace,function(a,c,d){a.stopPropagation();b.isUpdating=!0;B(b);E(b,c,d)}).bind("updateCell"+a.namespace,function(h,e,d,f){h.stopPropagation();b.isUpdating= !0;c.find(a.selectorRemove).remove();var l,m;l=c.find("tbody");m=g(e);h=l.index(g.fn.closest?m.closest("tbody"):m.parents("tbody").filter(":first"));var p=g.fn.closest?m.closest("tr"):m.parents("tr").filter(":first");e=m[0];l.length&&0<=h&&(l=l.eq(h).find("tr").index(p),m=m.index(),a.cache[h].normalized[l][a.columns].$row=p,e=a.cache[h].normalized[l][m]="no-parser"===a.parsers[m].id?"":a.parsers[m].format(n(b,e,m),b,e,m),"numeric"===(a.parsers[m].type||"").toLowerCase()&&(a.cache[h].colMax[m]=Math.max(Math.abs(e)|| 0,a.cache[h].colMax[m]||0)),H(c,d,f))}).bind("addRows"+a.namespace,function(h,e,d,f){h.stopPropagation();b.isUpdating=!0;if(p(a.cache))B(b),E(b,d,f);else{e=g(e);var l,m,v,u,x=e.filter("tr").length,q=c.find("tbody").index(e.parents("tbody").filter(":first"));a.parsers&&a.parsers.length||t(b);for(h=0;h<x;h++){m=e[h].cells.length;u=[];v={child:[],$row:e.eq(h),order:a.cache[q].normalized.length};for(l=0;l<m;l++)u[l]="no-parser"===a.parsers[l].id?"":a.parsers[l].format(n(b,e[h].cells[l],l),b,e[h].cells[l], l),"numeric"===(a.parsers[l].type||"").toLowerCase()&&(a.cache[q].colMax[l]=Math.max(Math.abs(u[l])||0,a.cache[q].colMax[l]||0));u.push(v);a.cache[q].normalized.push(u)}H(c,d,f)}}).bind("updateComplete"+a.namespace,function(){b.isUpdating=!1}).bind("sorton"+a.namespace,function(a,e,d,k){var l=b.config;a.stopPropagation();c.trigger("sortStart",this);M(b,e);G(b);l.delayInit&&p(l.cache)&&x(b);c.trigger("sortBegin",this);I(b);z(b,k);c.trigger("sortEnd",this);f.applyWidget(b);g.isFunction(d)&&d(b)}).bind("appendCache"+ a.namespace,function(a,c,d){a.stopPropagation();z(b,d);g.isFunction(c)&&c(b)}).bind("updateCache"+a.namespace,function(c,e){a.parsers&&a.parsers.length||t(b);x(b);g.isFunction(e)&&e(b)}).bind("applyWidgetId"+a.namespace,function(c,e){c.stopPropagation();f.getWidgetById(e).format(b,a,a.widgetOptions)}).bind("applyWidgets"+a.namespace,function(a,c){a.stopPropagation();f.applyWidget(b,c)}).bind("refreshWidgets"+a.namespace,function(a,c,d){a.stopPropagation();f.refreshWidgets(b,c,d)}).bind("destroy"+ a.namespace,function(a,c,d){a.stopPropagation();f.destroy(b,c,d)}).bind("resetToLoadState"+a.namespace,function(){f.refreshWidgets(b,!0,!0);a=g.extend(!0,f.defaults,a.originalSettings);b.hasInitialized=!1;f.setup(b,a)})}var f=this;f.version="2.17.5";f.parsers=[];f.widgets=[];f.defaults={theme:"default",widthFixed:!1,showProcessing:!1,headerTemplate:"{content}",onRenderTemplate:null,onRenderHeader:null,cancelSelection:!0,tabIndex:!0,dateFormat:"mmddyyyy",sortMultiSortKey:"shiftKey",sortResetKey:"ctrlKey", usNumberFormat:!0,delayInit:!1,serverSideSorting:!1,headers:{},ignoreCase:!0,sortForce:null,sortList:[],sortAppend:null,sortStable:!1,sortInitialOrder:"asc",sortLocaleCompare:!1,sortReset:!1,sortRestart:!1,emptyTo:"bottom",stringTo:"max",textExtraction:"basic",textAttribute:"data-text",textSorter:null,numberSorter:null,widgets:[],widgetOptions:{zebra:["even","odd"]},initWidgets:!0,initialized:null,tableClass:"",cssAsc:"",cssDesc:"",cssNone:"",cssHeader:"",cssHeaderRow:"",cssProcessing:"",cssChildRow:"tablesorter-childRow", cssIcon:"tablesorter-icon",cssInfoBlock:"tablesorter-infoOnly",selectorHeaders:"> thead th, > thead td",selectorSort:"th, td",selectorRemove:".remove-me",debug:!1,headerList:[],empties:{},strings:{},parsers:[]};f.css={table:"tablesorter",cssHasChild:"tablesorter-hasChildRow",childRow:"tablesorter-childRow",header:"tablesorter-header",headerRow:"tablesorter-headerRow",headerIn:"tablesorter-header-inner",icon:"tablesorter-icon",info:"tablesorter-infoOnly",processing:"tablesorter-processing",sortAsc:"tablesorter-headerAsc", sortDesc:"tablesorter-headerDesc",sortNone:"tablesorter-headerUnSorted"};f.language={sortAsc:"Ascending sort applied, ",sortDesc:"Descending sort applied, ",sortNone:"No sort applied, ",nextAsc:"activate to apply an ascending sort",nextDesc:"activate to apply a descending sort",nextNone:"activate to remove the sort"};f.log=d;f.benchmark=v;f.construct=function(b){return this.each(function(){var a=g.extend(!0,{},f.defaults,b);a.originalSettings=b;!this.hasInitialized&&f.buildTable&&"TABLE"!==this.tagName? f.buildTable(this,a):f.setup(this,a)})};f.setup=function(b,a){if(!b||!b.tHead||0===b.tBodies.length||!0===b.hasInitialized)return a.debug?d("ERROR: stopping initialization! No table, thead, tbody or tablesorter has already been initialized"):"";var c="",h=g(b),e=g.metadata;b.hasInitialized=!1;b.isProcessing=!0;b.config=a;g.data(b,"tablesorter",a);a.debug&&g.data(b,"startoveralltimer",new Date);a.supportsDataObject=function(a){a[0]=parseInt(a[0],10);return 1<a[0]||1===a[0]&&4<=parseInt(a[1],10)}(g.fn.jquery.split(".")); a.string={max:1,min:-1,emptyMin:1,emptyMax:-1,zero:0,none:0,"null":0,top:!0,bottom:!1};/tablesorter\-/.test(h.attr("class"))||(c=""!==a.theme?" tablesorter-"+a.theme:"");a.table=b;a.$table=h.addClass(f.css.table+" "+a.tableClass+c).attr({role:"grid"});a.$headers=g(b).find(a.selectorHeaders);a.namespace=a.namespace?"."+a.namespace.replace(/\W/g,""):".tablesorter"+Math.random().toString(16).slice(2);a.$tbodies=h.children("tbody:not(."+a.cssInfoBlock+")").attr({"aria-live":"polite","aria-relevant":"all"}); a.$table.find("caption").length&&a.$table.attr("aria-labelledby","theCaption");a.widgetInit={};a.textExtraction=a.$table.attr("data-text-extraction")||a.textExtraction||"basic";D(b);L(b);t(b);a.totalRows=0;a.delayInit||x(b);f.bindEvents(b,a.$headers,!0);K(b);a.supportsDataObject&&"undefined"!==typeof h.data().sortlist?a.sortList=h.data().sortlist:e&&h.metadata()&&h.metadata().sortlist&&(a.sortList=h.metadata().sortlist);f.applyWidget(b,!0);0<a.sortList.length?h.trigger("sorton",[a.sortList,{},!a.initWidgets, !0]):(G(b),a.initWidgets&&f.applyWidget(b,!1));a.showProcessing&&h.unbind("sortBegin"+a.namespace+" sortEnd"+a.namespace).bind("sortBegin"+a.namespace+" sortEnd"+a.namespace,function(c){clearTimeout(a.processTimer);f.isProcessing(b);"sortBegin"===c.type&&(a.processTimer=setTimeout(function(){f.isProcessing(b,!0)},500))});b.hasInitialized=!0;b.isProcessing=!1;a.debug&&f.benchmark("Overall initialization time",g.data(b,"startoveralltimer"));h.trigger("tablesorter-initialized",b);"function"===typeof a.initialized&& a.initialized(b)};f.getColumnData=function(b,a,c,h){if("undefined"!==typeof a&&null!==a){b=g(b)[0];var e,d=b.config;if(a[c])return h?a[c]:a[d.$headers.index(d.$headers.filter('[data-column="'+c+'"]:last'))];for(e in a)if("string"===typeof e&&(b=h?d.$headers.eq(c).filter(e):d.$headers.filter('[data-column="'+c+'"]:last').filter(e),b.length))return a[e]}};f.computeColumnIndex=function(b){var a=[],c=0,h,e,d,f,l,m,p,v,n,q;for(h=0;h<b.length;h++)for(l=b[h].cells,e=0;e<l.length;e++){d=l[e];f=g(d);m=d.parentNode.rowIndex; f.index();p=d.rowSpan||1;v=d.colSpan||1;"undefined"===typeof a[m]&&(a[m]=[]);for(d=0;d<a[m].length+1;d++)if("undefined"===typeof a[m][d]){n=d;break}c=Math.max(n,c);f.attr({"data-column":n});for(d=m;d<m+p;d++)for("undefined"===typeof a[d]&&(a[d]=[]),q=a[d],f=n;f<n+v;f++)q[f]="x"}return c+1};f.isProcessing=function(b,a,c){b=g(b);var d=b[0].config,e=c||b.find("."+f.css.header);a?("undefined"!==typeof c&&0<d.sortList.length&&(e=e.filter(function(){return this.sortDisabled?!1:0<=f.isValueInArray(parseFloat(g(this).attr("data-column")), d.sortList)})),b.add(e).addClass(f.css.processing+" "+d.cssProcessing)):b.add(e).removeClass(f.css.processing+" "+d.cssProcessing)};f.processTbody=function(b,a,c){b=g(b)[0];if(c)return b.isProcessing=!0,a.before('<span class="tablesorter-savemyplace"/>'),c=g.fn.detach?a.detach():a.remove();c=g(b).find("span.tablesorter-savemyplace");a.insertAfter(c);c.remove();b.isProcessing=!1};f.clearTableBody=function(b){g(b)[0].config.$tbodies.children().detach()};f.bindEvents=function(b,a,c){b=g(b)[0];var d, e=b.config;!0!==c&&(e.$extraHeaders=e.$extraHeaders?e.$extraHeaders.add(a):a);a.find(e.selectorSort).add(a.filter(e.selectorSort)).unbind(["mousedown","mouseup","sort","keyup",""].join(e.namespace+" ")).bind(["mousedown","mouseup","sort","keyup",""].join(e.namespace+" "),function(c,f){var l;l=c.type;if(!(1!==(c.which||c.button)&&!/sort|keyup/.test(l)||"keyup"===l&&13!==c.which||"mouseup"===l&&!0!==f&&250<(new Date).getTime()-d)){if("mousedown"===l)return d=(new Date).getTime(),/(input|select|button|textarea)/i.test(c.target.tagName)? "":!e.cancelSelection;e.delayInit&&p(e.cache)&&x(b);l=g.fn.closest?g(this).closest("th, td")[0]:/TH|TD/.test(this.tagName)?this:g(this).parents("th, td")[0];l=e.$headers[a.index(l)];l.sortDisabled||O(b,l,c)}});e.cancelSelection&&a.attr("unselectable","on").bind("selectstart",!1).css({"user-select":"none",MozUserSelect:"none"})};f.restoreHeaders=function(b){var a=g(b)[0].config;a.$table.find(a.selectorHeaders).each(function(b){g(this).find("."+f.css.headerIn).length&&g(this).html(a.headerContent[b])})}; f.destroy=function(b,a,c){b=g(b)[0];if(b.hasInitialized){f.refreshWidgets(b,!0,!0);var d=g(b),e=b.config,r=d.find("thead:first"),k=r.find("tr."+f.css.headerRow).removeClass(f.css.headerRow+" "+e.cssHeaderRow),l=d.find("tfoot:first > tr").children("th, td");!1===a&&0<=g.inArray("uitheme",e.widgets)&&(d.trigger("applyWidgetId",["uitheme"]),d.trigger("applyWidgetId",["zebra"]));r.find("tr").not(k).remove();d.removeData("tablesorter").unbind("sortReset update updateAll updateRows updateCell addRows updateComplete sorton appendCache updateCache applyWidgetId applyWidgets refreshWidgets destroy mouseup mouseleave keypress sortBegin sortEnd resetToLoadState ".split(" ").join(e.namespace+ " "));e.$headers.add(l).removeClass([f.css.header,e.cssHeader,e.cssAsc,e.cssDesc,f.css.sortAsc,f.css.sortDesc,f.css.sortNone].join(" ")).removeAttr("data-column").removeAttr("aria-label").attr("aria-disabled","true");k.find(e.selectorSort).unbind(["mousedown","mouseup","keypress",""].join(e.namespace+" "));f.restoreHeaders(b);d.toggleClass(f.css.table+" "+e.tableClass+" tablesorter-"+e.theme,!1===a);b.hasInitialized=!1;delete b.config.cache;"function"===typeof c&&c(b)}};f.regex={chunk:/(^([+\-]?(?:0|[1-9]\d*)(?:\.\d*)?(?:[eE][+\-]?\d+)?)?$|^0x[0-9a-f]+$|\d+)/gi, chunks:/(^\\0|\\0$)/,hex:/^0x[0-9a-f]+$/i};f.sortNatural=function(b,a){if(b===a)return 0;var c,d,e,g,k,l;d=f.regex;if(d.hex.test(a)){c=parseInt(b.match(d.hex),16);e=parseInt(a.match(d.hex),16);if(c<e)return-1;if(c>e)return 1}c=b.replace(d.chunk,"\\0$1\\0").replace(d.chunks,"").split("\\0");d=a.replace(d.chunk,"\\0$1\\0").replace(d.chunks,"").split("\\0");l=Math.max(c.length,d.length);for(k=0;k<l;k++){e=isNaN(c[k])?c[k]||0:parseFloat(c[k])||0;g=isNaN(d[k])?d[k]||0:parseFloat(d[k])||0;if(isNaN(e)!== isNaN(g))return isNaN(e)?1:-1;typeof e!==typeof g&&(e+="",g+="");if(e<g)return-1;if(e>g)return 1}return 0};f.sortNaturalAsc=function(b,a,c,d,e){if(b===a)return 0;c=e.string[e.empties[c]||e.emptyTo];return""===b&&0!==c?"boolean"===typeof c?c?-1:1:-c||-1:""===a&&0!==c?"boolean"===typeof c?c?1:-1:c||1:f.sortNatural(b,a)};f.sortNaturalDesc=function(b,a,c,d,e){if(b===a)return 0;c=e.string[e.empties[c]||e.emptyTo];return""===b&&0!==c?"boolean"===typeof c?c?-1:1:c||1:""===a&&0!==c?"boolean"===typeof c?c? 1:-1:-c||-1:f.sortNatural(a,b)};f.sortText=function(b,a){return b>a?1:b<a?-1:0};f.getTextValue=function(b,a,c){if(c){var d=b?b.length:0,e=c+a;for(c=0;c<d;c++)e+=b.charCodeAt(c);return a*e}return 0};f.sortNumericAsc=function(b,a,c,d,e,g){if(b===a)return 0;g=g.config;e=g.string[g.empties[e]||g.emptyTo];if(""===b&&0!==e)return"boolean"===typeof e?e?-1:1:-e||-1;if(""===a&&0!==e)return"boolean"===typeof e?e?1:-1:e||1;isNaN(b)&&(b=f.getTextValue(b,c,d));isNaN(a)&&(a=f.getTextValue(a,c,d));return b-a};f.sortNumericDesc= function(b,a,c,d,e,g){if(b===a)return 0;g=g.config;e=g.string[g.empties[e]||g.emptyTo];if(""===b&&0!==e)return"boolean"===typeof e?e?-1:1:e||1;if(""===a&&0!==e)return"boolean"===typeof e?e?1:-1:-e||-1;isNaN(b)&&(b=f.getTextValue(b,c,d));isNaN(a)&&(a=f.getTextValue(a,c,d));return a-b};f.sortNumeric=function(b,a){return b-a};f.characterEquivalents={a:"\u00e1\u00e0\u00e2\u00e3\u00e4\u0105\u00e5",A:"\u00c1\u00c0\u00c2\u00c3\u00c4\u0104\u00c5",c:"\u00e7\u0107\u010d",C:"\u00c7\u0106\u010c",e:"\u00e9\u00e8\u00ea\u00eb\u011b\u0119", E:"\u00c9\u00c8\u00ca\u00cb\u011a\u0118",i:"\u00ed\u00ec\u0130\u00ee\u00ef\u0131",I:"\u00cd\u00cc\u0130\u00ce\u00cf",o:"\u00f3\u00f2\u00f4\u00f5\u00f6",O:"\u00d3\u00d2\u00d4\u00d5\u00d6",ss:"\u00df",SS:"\u1e9e",u:"\u00fa\u00f9\u00fb\u00fc\u016f",U:"\u00da\u00d9\u00db\u00dc\u016e"};f.replaceAccents=function(b){var a,c="[",d=f.characterEquivalents;if(!f.characterRegex){f.characterRegexArray={};for(a in d)"string"===typeof a&&(c+=d[a],f.characterRegexArray[a]=new RegExp("["+d[a]+"]","g"));f.characterRegex= new RegExp(c+"]")}if(f.characterRegex.test(b))for(a in d)"string"===typeof a&&(b=b.replace(f.characterRegexArray[a],a));return b};f.isValueInArray=function(b,a){var c,d=a.length;for(c=0;c<d;c++)if(a[c][0]===b)return c;return-1};f.addParser=function(b){var a,c=f.parsers.length,d=!0;for(a=0;a<c;a++)f.parsers[a].id.toLowerCase()===b.id.toLowerCase()&&(d=!1);d&&f.parsers.push(b)};f.getParserById=function(b){if("false"==b)return!1;var a,c=f.parsers.length;for(a=0;a<c;a++)if(f.parsers[a].id.toLowerCase()=== b.toString().toLowerCase())return f.parsers[a];return!1};f.addWidget=function(b){f.widgets.push(b)};f.hasWidget=function(b,a){b=g(b);return b.length&&b[0].config&&b[0].config.widgetInit[a]||!1};f.getWidgetById=function(b){var a,c,d=f.widgets.length;for(a=0;a<d;a++)if((c=f.widgets[a])&&c.hasOwnProperty("id")&&c.id.toLowerCase()===b.toLowerCase())return c};f.applyWidget=function(b,a){b=g(b)[0];var c=b.config,d=c.widgetOptions,e=[],p,k,l;!1!==a&&b.hasInitialized&&(b.isApplyingWidgets||b.isUpdating)|| (c.debug&&(p=new Date),c.widgets.length&&(b.isApplyingWidgets=!0,c.widgets=g.grep(c.widgets,function(a,b){return g.inArray(a,c.widgets)===b}),g.each(c.widgets||[],function(a,b){(l=f.getWidgetById(b))&&l.id&&(l.priority||(l.priority=10),e[a]=l)}),e.sort(function(a,b){return a.priority<b.priority?-1:a.priority===b.priority?0:1}),g.each(e,function(e,f){if(f){if(a||!c.widgetInit[f.id])c.widgetInit[f.id]=!0,f.hasOwnProperty("options")&&(d=b.config.widgetOptions=g.extend(!0,{},f.options,d)),f.hasOwnProperty("init")&& f.init(b,f,c,d);!a&&f.hasOwnProperty("format")&&f.format(b,c,d,!1)}})),setTimeout(function(){b.isApplyingWidgets=!1},0),c.debug&&(k=c.widgets.length,v("Completed "+(!0===a?"initializing ":"applying ")+k+" widget"+(1!==k?"s":""),p)))};f.refreshWidgets=function(b,a,c){b=g(b)[0];var h,e=b.config,p=e.widgets,k=f.widgets,l=k.length;for(h=0;h<l;h++)k[h]&&k[h].id&&(a||0>g.inArray(k[h].id,p))&&(e.debug&&d('Refeshing widgets: Removing "'+k[h].id+'"'),k[h].hasOwnProperty("remove")&&e.widgetInit[k[h].id]&&(k[h].remove(b, e,e.widgetOptions),e.widgetInit[k[h].id]=!1));!0!==c&&f.applyWidget(b,a)};f.getData=function(b,a,c){var d="";b=g(b);var e,f;if(!b.length)return"";e=g.metadata?b.metadata():!1;f=" "+(b.attr("class")||"");"undefined"!==typeof b.data(c)||"undefined"!==typeof b.data(c.toLowerCase())?d+=b.data(c)||b.data(c.toLowerCase()):e&&"undefined"!==typeof e[c]?d+=e[c]:a&&"undefined"!==typeof a[c]?d+=a[c]:" "!==f&&f.match(" "+c+"-")&&(d=f.match(new RegExp("\\s"+c+"-([\\w-]+)"))[1]||"");return g.trim(d)};f.formatFloat= function(b,a){if("string"!==typeof b||""===b)return b;var c;b=(a&&a.config?!1!==a.config.usNumberFormat:"undefined"!==typeof a?a:1)?b.replace(/,/g,""):b.replace(/[\s|\.]/g,"").replace(/,/g,".");/^\s*\([.\d]+\)/.test(b)&&(b=b.replace(/^\s*\(([.\d]+)\)/,"-$1"));c=parseFloat(b);return isNaN(c)?g.trim(b):c};f.isDigit=function(b){return isNaN(b)?/^[\-+(]?\d+[)]?$/.test(b.toString().replace(/[,.'"\s]/g,"")):!0}}});var n=g.tablesorter;g.fn.extend({tablesorter:n.construct});n.addParser({id:"no-parser",is:function(){return!1}, format:function(){return""},type:"text"});n.addParser({id:"text",is:function(){return!0},format:function(d,v){var p=v.config;d&&(d=g.trim(p.ignoreCase?d.toLocaleLowerCase():d),d=p.sortLocaleCompare?n.replaceAccents(d):d);return d},type:"text"});n.addParser({id:"digit",is:function(d){return n.isDigit(d)},format:function(d,v){var p=n.formatFloat((d||"").replace(/[^\w,. \-()]/g,""),v);return d&&"number"===typeof p?p:d?g.trim(d&&v.config.ignoreCase?d.toLocaleLowerCase():d):d},type:"numeric"});n.addParser({id:"currency", is:function(d){return/^\(?\d+[\u00a3$\u20ac\u00a4\u00a5\u00a2?.]|[\u00a3$\u20ac\u00a4\u00a5\u00a2?.]\d+\)?$/.test((d||"").replace(/[+\-,. ]/g,""))},format:function(d,v){var p=n.formatFloat((d||"").replace(/[^\w,. \-()]/g,""),v);return d&&"number"===typeof p?p:d?g.trim(d&&v.config.ignoreCase?d.toLocaleLowerCase():d):d},type:"numeric"});n.addParser({id:"ipAddress",is:function(d){return/^\d{1,3}[\.]\d{1,3}[\.]\d{1,3}[\.]\d{1,3}$/.test(d)},format:function(d,g){var p,w=d?d.split("."):"",t="",x=w.length; for(p=0;p<x;p++)t+=("00"+w[p]).slice(-3);return d?n.formatFloat(t,g):d},type:"numeric"});n.addParser({id:"url",is:function(d){return/^(https?|ftp|file):\/\//.test(d)},format:function(d){return d?g.trim(d.replace(/(https?|ftp|file):\/\//,"")):d},type:"text"});n.addParser({id:"isoDate",is:function(d){return/^\d{4}[\/\-]\d{1,2}[\/\-]\d{1,2}/.test(d)},format:function(d,g){return d?n.formatFloat(""!==d?(new Date(d.replace(/-/g,"/"))).getTime()||d:"",g):d},type:"numeric"});n.addParser({id:"percent",is:function(d){return/(\d\s*?%|%\s*?\d)/.test(d)&& 15>d.length},format:function(d,g){return d?n.formatFloat(d.replace(/%/g,""),g):d},type:"numeric"});n.addParser({id:"usLongDate",is:function(d){return/^[A-Z]{3,10}\.?\s+\d{1,2},?\s+(\d{4})(\s+\d{1,2}:\d{2}(:\d{2})?(\s+[AP]M)?)?$/i.test(d)||/^\d{1,2}\s+[A-Z]{3,10}\s+\d{4}/i.test(d)},format:function(d,g){return d?n.formatFloat((new Date(d.replace(/(\S)([AP]M)$/i,"$1 $2"))).getTime()||d,g):d},type:"numeric"});n.addParser({id:"shortDate",is:function(d){return/(^\d{1,2}[\/\s]\d{1,2}[\/\s]\d{4})|(^\d{4}[\/\s]\d{1,2}[\/\s]\d{1,2})/.test((d|| "").replace(/\s+/g," ").replace(/[\-.,]/g,"/"))},format:function(d,g,p,w){if(d){p=g.config;var t=p.$headers.filter("[data-column="+w+"]:last");w=t.length&&t[0].dateFormat||n.getData(t,n.getColumnData(g,p.headers,w),"dateFormat")||p.dateFormat;d=d.replace(/\s+/g," ").replace(/[\-.,]/g,"/");"mmddyyyy"===w?d=d.replace(/(\d{1,2})[\/\s](\d{1,2})[\/\s](\d{4})/,"$3/$1/$2"):"ddmmyyyy"===w?d=d.replace(/(\d{1,2})[\/\s](\d{1,2})[\/\s](\d{4})/,"$3/$2/$1"):"yyyymmdd"===w&&(d=d.replace(/(\d{4})[\/\s](\d{1,2})[\/\s](\d{1,2})/, "$1/$2/$3"))}return d?n.formatFloat((new Date(d)).getTime()||d,g):d},type:"numeric"});n.addParser({id:"time",is:function(d){return/^(([0-2]?\d:[0-5]\d)|([0-1]?\d:[0-5]\d\s?([AP]M)))$/i.test(d)},format:function(d,g){return d?n.formatFloat((new Date("2000/01/01 "+d.replace(/(\S)([AP]M)$/i,"$1 $2"))).getTime()||d,g):d},type:"numeric"});n.addParser({id:"metadata",is:function(){return!1},format:function(d,n,p){d=n.config;d=d.parserMetadataName?d.parserMetadataName:"sortValue";return g(p).metadata()[d]}, type:"numeric"});n.addWidget({id:"zebra",priority:90,format:function(d,v,p){var w,t,x,z,C,D,E=new RegExp(v.cssChildRow,"i"),B=v.$tbodies;v.debug&&(C=new Date);for(d=0;d<B.length;d++)w=B.eq(d),D=w.children("tr").length,1<D&&(x=0,w=w.children("tr:visible").not(v.selectorRemove),w.each(function(){t=g(this);E.test(this.className)||x++;z=0===x%2;t.removeClass(p.zebra[z?1:0]).addClass(p.zebra[z?0:1])}));v.debug&&n.benchmark("Applying Zebra widget",C)},remove:function(d,n,p){var w;n=n.$tbodies;var t=(p.zebra|| ["even","odd"]).join(" ");for(p=0;p<n.length;p++)w=g.tablesorter.processTbody(d,n.eq(p),!0),w.children().removeClass(t),g.tablesorter.processTbody(d,w,!1)}})}(jQuery);

/*! Filter widget select2 formatter function - updated 7/17/2014 (v2.17.5)
 * requires: jQuery 1.7.2+, tableSorter 2.16+, filter widget 2.16+ and select2 v3.4.6+ plugin
 */
/*jshint browser:true, jquery:true, unused:false */
/*global jQuery: false */
;(function($){
"use strict";

var ts = $.tablesorter || {};
ts.filterFormatter = ts.filterFormatter || {};

/************************\
 Select2 Filter Formatter
\************************/
ts.filterFormatter.select2 = function($cell, indx, select2Def) {
	var o = $.extend({
		// select2 filter formatter options
		cellText : '', // Text (wrapped in a label element)
		match : true, // adds "filter-match" to header
		value : '',
		// include ANY select2 options below
		multiple : true,
		width : '100%'

	}, select2Def ),
	arry, data,
	c = $cell.closest('table')[0].config,
	wo = c.widgetOptions,
	// Add a hidden input to hold the range values
	$input = $('<input class="filter" type="hidden">')
	.appendTo($cell)
	// hidden filter update namespace trigger by filter widget
	.bind('change' + c.namespace + 'filter', function(){
		var val = this.value;
		val = val.replace(/[/()$^]/g, '').split('|');
		$cell.find('.select2').select2('val', val);
		updateSelect2();
	}),
	$header = c.$headers.filter('[data-column="' + indx + '"]:last'),
	onlyAvail = $header.hasClass(wo.filter_onlyAvail),
	$shcell = [],
	matchPrefix = o.match ? '' : '^',
	matchSuffix = o.match ? '' : '$',

	// this function updates the hidden input and adds the current values to the header cell text
	updateSelect2 = function() {
		var v = $cell.find('.select2').select2('val') || o.value || '';
		$input
		// add regex, so we filter exact numbers
		.val( $.isArray(v) && v.length && v.join('') !== '' ? '/(' + matchPrefix + (v || []).join(matchSuffix + '|' + matchPrefix) + matchSuffix + ')/' : '' )
		.trigger('search').end()
		.find('.select2').select2('val', v);
		// update sticky header cell
		if ($shcell.length) {
			$shcell
			.find('.select2').select2('val', v);
		}
	},

	// get options from table cell content or filter_selectSource (v2.16)
	updateOptions = function(){
		data = [];
		arry = ts.filter.getOptionSource(c.$table[0], indx, onlyAvail) || [];
		// build select2 data option
		$.each(arry, function(i,v){
			data.push({id: v, text: v});
		});
		o.data = data;
	};

	// get filter-match class from option
	$header.toggleClass('filter-match', o.match);
	if (o.cellText) {
		$cell.prepend('<label>' + o.cellText + '</label>');
	}

	// don't add default in table options if either ajax or
	// data options are already defined
	if (!(o.ajax && !$.isEmptyObject(o.ajax)) && !o.data) {
		updateOptions();
		if (onlyAvail) {
			c.$table.bind('filterEnd', function(){
				updateOptions();
				$cell.add($shcell).find('.select2').select2(o);
			});
		}
	}

	// add a select2 hidden input!
	$('<input class="select2 select2-' + indx + '" type="hidden" />')
	.val(o.value)
	.appendTo($cell)
	.select2(o)
	.bind('change', function(){
		updateSelect2();
	});

	// update select2 from filter hidden input, in case of saved filters
	c.$table.bind('filterFomatterUpdate', function(){
		// value = '/(^x$|^y$)/' => 'x,y'
		var val = c.$table.data('lastSearch')[indx] || '';
		val = val.replace(/[/()$^]/g, '').split('|');
		$cell.find('.select2').select2('val', val);
		updateSelect2();
		ts.filter.formatterUpdated($cell, indx);
	});

	// has sticky headers?
	c.$table.bind('stickyHeadersInit', function(){
		$shcell = c.widgetOptions.$sticky.find('.tablesorter-filter-row').children().eq(indx).empty();
		// add a select2!
		$('<input class="select2 select2-' + indx + '" type="hidden">')
		.val(o.value)
		.appendTo($shcell)
		.select2(o)
		.bind('change', function(){
			$cell.find('.select2').select2('val', $shcell.find('.select2').select2('val') );
			updateSelect2();
		});
		if (o.cellText) {
			$shcell.prepend('<label>' + o.cellText + '</label>');
		}

	});

	// on reset
	c.$table.bind('filterReset', function(){
		$cell.find('.select2').select2('val', o.value || '');
		setTimeout(function(){
			updateSelect2();
		}, 0);
	});

	updateSelect2();
	return $input;
};

})(jQuery);

/*! Filter widget formatter functions - updated 7/17/2014 (v2.17.5)
 * requires: tableSorter 2.15+ and jQuery 1.4.3+
 *
 * uiSpinner (jQuery UI spinner)
 * uiSlider (jQuery UI slider)
 * uiRange (jQuery UI range slider)
 * uiDateCompare (jQuery UI datepicker; 1 input)
 * uiDatepicker (jQuery UI datepicker; 2 inputs, filter range)
 * html5Number (spinner)
 * html5Range (slider)
 * html5Color (color)
 */
/*jshint browser:true, jquery:true, unused:false */
/*global jQuery: false */
;(function($){
"use strict";

var ts = $.tablesorter || {},

// compare option selector class name (jQuery selector)
compareSelect = '.compare-select',


tsff = ts.filterFormatter = {

	addCompare: function($cell, indx, options){
		if (options.compare && $.isArray(options.compare) && options.compare.length > 1) {
			var opt = '',
				compareSelectClass = [ compareSelect.slice(1), ' ' + compareSelect.slice(1), '' ],
				txt = options.cellText ? '<label class="' + compareSelectClass.join('-label') + indx + '">' + options.cellText + '</label>' : '';
			$.each(options.compare, function(i, c){
				opt += '<option ' + (options.selected === i ? 'selected' : '') + '>' + c + '</option>';
			});
			$cell
				.wrapInner('<div class="' + compareSelectClass.join('-wrapper') + indx + '" />')
				.prepend( txt + '<select class="' + compareSelectClass.join('') + indx + '" />' )
				.find('select')
				.append(opt);
		}
	},

	updateCompare : function($cell, $input, o) {
		var val = $input.val() || '',
			num = val.replace(/\s*?[><=]\s*?/g, ''),
			compare = val.match(/[><=]/g) || '';
		if (o.compare) {
			if ($.isArray(o.compare)){
				compare = (compare || []).join('') || o.compare[o.selected || 0];
			}
			$cell.find(compareSelect).val( compare );
		}
		return [ val, num ];
	},

	/**********************\
	jQuery UI Spinner
	\**********************/
	uiSpinner: function($cell, indx, spinnerDef) {
		var o = $.extend({
			// filter formatter options
			delayed : true,
			addToggle : true,
			exactMatch : true,
			value : 1,
			cellText : '',
			compare : '',
			// include ANY jQuery UI spinner options below
			min : 0,
			max : 100,
			step : 1,
			disabled : false

		}, spinnerDef ),
		c = $cell.closest('table')[0].config,
		// Add a hidden input to hold the range values
		$input = $('<input class="filter" type="hidden">')
			.appendTo($cell)
			// hidden filter update namespace trigger by filter widget
			.bind('change' + c.namespace + 'filter', function(){
				updateSpinner({ value: this.value, delayed: false });
			}),
		$shcell = [],

		// this function updates the hidden input and adds the current values to the header cell text
		updateSpinner = function(ui, notrigger) {
			var chkd = true, state,
				// ui is not undefined on create
				v = ui && ui.value && ts.formatFloat((ui.value + '').replace(/[><=]/g,'')) ||
					$cell.find('.spinner').val() || o.value,
				compare = ($.isArray(o.compare) ? $cell.find(compareSelect).val() || o.compare[ o.selected || 0] : o.compare) || '',
				searchType = ui && typeof ui.delayed === 'boolean' ? ui.delayed : c.$table[0].hasInitialized ? o.delayed || '' : true;
			if (o.addToggle) {
				chkd = $cell.find('.toggle').is(':checked');
			}
			state = o.disabled || !chkd ? 'disable' : 'enable';
			$cell.find('.filter')
				// add equal to the beginning, so we filter exact numbers
				.val( chkd ? (compare ? compare : o.exactMatch ? '=' : '') + v : '' )
				.trigger( notrigger ? '' : 'search', searchType ).end()
				.find('.spinner').spinner(state).val(v);
			// update sticky header cell
			if ($shcell.length) {
				$shcell
					.find('.spinner').spinner(state).val(v).end()
					.find(compareSelect).val( compare );
				if (o.addToggle) {
					$shcell.find('.toggle')[0].checked = chkd;
				}
			}
		};

		// add callbacks; preserve added callbacks
		o.oldcreate = o.create;
		o.oldspin = o.spin;
		o.create = function(event, ui) {
			updateSpinner(); // ui is an empty object on create
			if (typeof o.oldcreate === 'function') { o.oldcreate(event, ui); }
		};
		o.spin  = function(event, ui) {
			updateSpinner(ui);
			if (typeof o.oldspin === 'function') { o.oldspin(event, ui); }
		};
		if (o.addToggle) {
			$('<div class="button"><input id="uispinnerbutton' + indx + '" type="checkbox" class="toggle" />' +
				'<label for="uispinnerbutton' + indx + '"></label></div>')
				.appendTo($cell)
				.find('.toggle')
				.bind('change', function(){
					updateSpinner();
				});
		}
		// make sure we use parsed data
		$cell.closest('thead').find('th[data-column=' + indx + ']').addClass('filter-parsed');
		// add a jQuery UI spinner!
		$('<input class="spinner spinner' + indx + '" />')
			.val(o.value)
			.appendTo($cell)
			.spinner(o)
			.bind('change keyup', function(){
				updateSpinner();
			});

		// update spinner from hidden input, in case of saved filters
		c.$table.bind('filterFomatterUpdate', function(){
			var val = tsff.updateCompare($cell, $input, o)[0];
			$cell.find('.spinner').val( val );
			updateSpinner({ value: val }, true);
			ts.filter.formatterUpdated($cell, indx);
		});

		if (o.compare) {
			// add compare select
			tsff.addCompare($cell, indx, o);
			$cell.find(compareSelect).bind('change', function(){
				updateSpinner();
			});
		}

		// has sticky headers?
		c.$table.bind('stickyHeadersInit', function(){
			$shcell = c.widgetOptions.$sticky.find('.tablesorter-filter-row').children().eq(indx).empty();
			if (o.addToggle) {
				$('<div class="button"><input id="stickyuispinnerbutton' + indx + '" type="checkbox" class="toggle" />' +
					'<label for="stickyuispinnerbutton' + indx + '"></label></div>')
					.appendTo($shcell)
					.find('.toggle')
					.bind('change', function(){
						$cell.find('.toggle')[0].checked = this.checked;
						updateSpinner();
					});
			}
			// add a jQuery UI spinner!
			$('<input class="spinner spinner' + indx + '" />')
				.val(o.value)
				.appendTo($shcell)
				.spinner(o)
				.bind('change keyup', function(){
					$cell.find('.spinner').val( this.value );
					updateSpinner();
				});

			if (o.compare) {
				// add compare select
				tsff.addCompare($shcell, indx, o);
				$shcell.find(compareSelect).bind('change', function(){
					$cell.find(compareSelect).val( $(this).val() );
					updateSpinner();
				});
			}

		});

		// on reset
		c.$table.bind('filterReset', function(){
			if ($.isArray(o.compare)) {
				$cell.add($shcell).find(compareSelect).val( o.compare[ o.selected || 0 ] );
			}
			// turn off the toggle checkbox
			if (o.addToggle) {
				$cell.find('.toggle')[0].checked = false;
			}
			$cell.find('.spinner').spinner('value', o.value);
			setTimeout(function(){
				updateSpinner();
			}, 0);
		});

		updateSpinner();
		return $input;
	},

	/**********************\
	jQuery UI Slider
	\**********************/
	uiSlider: function($cell, indx, sliderDef) {
		var o = $.extend({
			// filter formatter options
			delayed : true,
			valueToHeader : false,
			exactMatch : true,
			cellText : '',
			compare : '',
			allText : 'all',
			// include ANY jQuery UI spinner options below
			// except values, since this is a non-range setup
			value : 0,
			min : 0,
			max : 100,
			step : 1,
			range : "min"
		}, sliderDef ),
		c = $cell.closest('table')[0].config,
		// Add a hidden input to hold the range values
		$input = $('<input class="filter" type="hidden">')
			.appendTo($cell)
			// hidden filter update namespace trigger by filter widget
			.bind('change' + c.namespace + 'filter', function(){
				updateSlider({ value: this.value });
			}),
		$shcell = [],

		// this function updates the hidden input and adds the current values to the header cell text
		updateSlider = function(ui, notrigger) {
			// ui is not undefined on create
			var v = typeof ui !== "undefined" ? ts.formatFloat((ui.value + '').replace(/[><=]/g,'')) || o.value : o.value,
				val = o.compare ? v : v === o.min ? o.allText : v,
				compare = ($.isArray(o.compare) ? $cell.find(compareSelect).val() || o.compare[ o.selected || 0] : o.compare) || '',
				result = compare + val,
				searchType = ui && typeof ui.delayed === 'boolean' ? ui.delayed : c.$table[0].hasInitialized ? o.delayed || '' : true;
			if (o.valueToHeader) {
				// add range indication to the header cell above!
				$cell.closest('thead').find('th[data-column=' + indx + ']').find('.curvalue').html(' (' + result + ')');
			} else {
				// add values to the handle data-value attribute so the css tooltip will work properly
				$cell.find('.ui-slider-handle').addClass('value-popup').attr('data-value', result);
			}
			// update the hidden input;
			// ****** ADD AN EQUAL SIGN TO THE BEGINNING! <- this makes the slide exactly match the number ******
			// when the value is at the minimum, clear the hidden input so all rows will be seen

			$cell.find('.filter')
				.val( ( compare ? compare + v : v === o.min ? '' : (o.exactMatch ? '=' : '') + v ) )
				.trigger( notrigger ? '' : 'search', searchType ).end()
				.find('.slider').slider('value', v);

			// update sticky header cell
			if ($shcell.length) {
				$shcell
					.find(compareSelect).val( compare ).end()
					.find('.slider').slider('value', v);
				if (o.valueToHeader) {
					$shcell.closest('thead').find('th[data-column=' + indx + ']').find('.curvalue').html(' (' + result + ')');
				} else {
					$shcell.find('.ui-slider-handle').addClass('value-popup').attr('data-value', result);
				}
			}

		};
		$cell.closest('thead').find('th[data-column=' + indx + ']').addClass('filter-parsed');

		// add span to header for value - only works if the line in the updateSlider() function is also un-commented out
		if (o.valueToHeader) {
			$cell.closest('thead').find('th[data-column=' + indx + ']').find('.tablesorter-header-inner').append('<span class="curvalue" />');
		}

		// add callbacks; preserve added callbacks
		o.oldcreate = o.create;
		o.oldslide = o.slide;
		o.create = function(event, ui) {
			updateSlider(); // ui is an empty object on create
			if (typeof o.oldcreate === 'function') { o.oldcreate(event, ui); }
		};
		o.slide  = function(event, ui) {
			updateSlider(ui);
			if (typeof o.oldslide === 'function') { o.oldslide(event, ui); }
		};
		// add a jQuery UI slider!
		$('<div class="slider slider' + indx + '"/>')
			.appendTo($cell)
			.slider(o);

		// update slider from hidden input, in case of saved filters
		c.$table.bind('filterFomatterUpdate', function(){
			var val = tsff.updateCompare($cell, $input, o)[0];
			$cell.find('.slider').slider('value', val );
			updateSlider({ value: val }, false);
			ts.filter.formatterUpdated($cell, indx);
		});

		if (o.compare) {
			// add compare select
			tsff.addCompare($cell, indx, o);
			$cell.find(compareSelect).bind('change', function(){
				updateSlider({ value: $cell.find('.slider').slider('value') });
			});
		}

		// on reset
		c.$table.bind('filterReset', function(){
			if ($.isArray(o.compare)) {
				$cell.add($shcell).find(compareSelect).val( o.compare[ o.selected || 0 ] );
			}
			setTimeout(function(){
				updateSlider({ value: o.value });
			}, 0);
		});

		// has sticky headers?
		c.$table.bind('stickyHeadersInit', function(){
			$shcell = c.widgetOptions.$sticky.find('.tablesorter-filter-row').children().eq(indx).empty();

			// add a jQuery UI slider!
			$('<div class="slider slider' + indx + '"/>')
				.val(o.value)
				.appendTo($shcell)
				.slider(o)
				.bind('change keyup', function(){
					$cell.find('.slider').slider('value', this.value );
					updateSlider();
				});

			if (o.compare) {
				// add compare select
				tsff.addCompare($shcell, indx, o);
				$shcell.find(compareSelect).bind('change', function(){
					$cell.find(compareSelect).val( $(this).val() );
					updateSlider();
				});
			}

		});

		return $input;
	},

	/*************************\
	jQuery UI Range Slider (2 handles)
	\*************************/
	uiRange: function($cell, indx, rangeDef) {
		var o = $.extend({
			// filter formatter options
			delayed : true,
			valueToHeader : false,
			// include ANY jQuery UI spinner options below
			// except value, since this one is range specific)
			values : [0, 100],
			min : 0,
			max : 100,
			range : true
		}, rangeDef ),
		c = $cell.closest('table')[0].config,
		// Add a hidden input to hold the range values
		$input = $('<input class="filter" type="hidden">')
			.appendTo($cell)
			// hidden filter update namespace trigger by filter widget
			.bind('change' + c.namespace + 'filter', function(){
				getRange();
			}),
		$shcell = [],

		getRange = function(){
			var val = $input.val(),
				v = val.split(' - ');
			if (val === '') { v = [ o.min, o.max ]; }
			if (v && v[1]) {
				updateUiRange({ values: v, delay: false }, true);
			}
		},

		// this function updates the hidden input and adds the current values to the header cell text
		updateUiRange = function(ui, notrigger) {
			// ui.values are undefined for some reason on create
			var val = ui && ui.values || o.values,
				result = val[0] + ' - ' + val[1],
				// make range an empty string if entire range is covered so the filter row will hide (if set)
				range = val[0] === o.min && val[1] === o.max ? '' : result,
				searchType = ui && typeof ui.delayed === 'boolean' ? ui.delayed : c.$table[0].hasInitialized ? o.delayed || '': true;
			if (o.valueToHeader) {
				// add range indication to the header cell above (if not using the css method)!
				$cell.closest('thead').find('th[data-column=' + indx + ']').find('.currange').html(' (' + result + ')');
			} else {
				// add values to the handle data-value attribute so the css tooltip will work properly
				$cell.find('.ui-slider-handle')
					.addClass('value-popup')
					.eq(0).attr('data-value', val[0]).end() // adding value to data attribute
					.eq(1).attr('data-value', val[1]);      // value popup shown via css
			}
			// update the hidden input
			$cell.find('.filter').val(range)
				.trigger(notrigger ? '' : 'search', searchType).end()
				.find('.range').slider('values', val);
			// update sticky header cell
			if ($shcell.length) {
				$shcell.find('.range').slider('values', val);
				if (o.valueToHeader) {
					$shcell.closest('thead').find('th[data-column=' + indx + ']').find('.currange').html(' (' + result + ')');
				} else {
					$shcell.find('.ui-slider-handle')
					.addClass('value-popup')
					.eq(0).attr('data-value', val[0]).end() // adding value to data attribute
					.eq(1).attr('data-value', val[1]);      // value popup shown via css
				}
			}

		};
		$cell.closest('thead').find('th[data-column=' + indx + ']').addClass('filter-parsed');

		// add span to header for value - only works if the line in the updateUiRange() function is also un-commented out
		if (o.valueToHeader) {
			$cell.closest('thead').find('th[data-column=' + indx + ']').find('.tablesorter-header-inner').append('<span class="currange"/>');
		}

		// add callbacks; preserve added callbacks
		o.oldcreate = o.create;
		o.oldslide = o.slide;
		// add a jQuery UI range slider!
		o.create = function(event, ui) {
			updateUiRange(); // ui is an empty object on create
			if (typeof o.oldcreate === 'function') { o.oldcreate(event, ui); }
		};
		o.slide  = function(event, ui) {
			updateUiRange(ui);
			if (typeof o.oldslide === 'function') { o.oldslide(event, ui); }
		};
		$('<div class="range range' + indx +'"/>')
			.appendTo($cell)
			.slider(o);

		// update slider from hidden input, in case of saved filters
		c.$table.bind('filterFomatterUpdate', function(){
			getRange();
			ts.filter.formatterUpdated($cell, indx);
		});

		// on reset
		c.$table.bind('filterReset', function(){
			$cell.find('.range').slider('values', o.values);
			setTimeout(function(){
				updateUiRange();
			}, 0);
		});

		// has sticky headers?
		c.$table.bind('stickyHeadersInit', function(){
			$shcell = c.widgetOptions.$sticky.find('.tablesorter-filter-row').children().eq(indx).empty();

		// add a jQuery UI slider!
		$('<div class="range range' + indx + '"/>')
			.val(o.value)
			.appendTo($shcell)
			.slider(o)
			.bind('change keyup', function(){
				$cell.find('.range').val( this.value );
				updateUiRange();
			});

		});

		// return the hidden input so the filter widget has a reference to it
		return $input;
	},

	/*************************\
	jQuery UI Datepicker compare (1 input)
	\*************************/
	uiDateCompare: function($cell, indx, defDate) {
		var o = $.extend({
			// filter formatter options
			cellText : '',
			compare : '',
			endOfDay : true,
			// include ANY jQuery UI spinner options below

			defaultDate : '',

			changeMonth : true,
			changeYear : true,
			numberOfMonths : 1
		}, defDate),

		$date,
		c = $cell.closest('table')[0].config,
		// make sure we're using parsed dates in the search
		$hdr = $cell.closest('thead').find('th[data-column=' + indx + ']').addClass('filter-parsed'),
		// Add a hidden input to hold the range values
		$input = $('<input class="dateCompare" type="hidden">')
			.appendTo($cell)
			// hidden filter update namespace trigger by filter widget
			.bind('change' + c.namespace + 'filter', function(){
				var v = this.value;
				if (v) {
					o.onClose(v);
				}
			}),
		t, $shcell = [],

		// this function updates the hidden input
		date1Compare = function(notrigger) {
			var date, query,
				getdate = $date.datepicker('getDate') || '',
				compare = ($.isArray(o.compare) ? $cell.find(compareSelect).val() || o.compare[ o.selected || 0] : o.compare) || '',
				searchType = c.$table[0].hasInitialized ? o.delayed || '': true;
			$date.datepicker('setDate', (getdate === '' ? '' : getdate) || null);
			if (getdate === '') { notrigger = false; }
			date = $date.datepicker('getDate');
			query = date ? ( o.endOfDay && /<=/.test(compare) ? date.setHours(23, 59, 59) : date.getTime() ) || '' : '';
			if (date && o.endOfDay && compare === '=') {
				compare = '';
				query += ' - ' + date.setHours(23, 59, 59);
				notrigger = false;
			}
			$cell.find('.dateCompare')
			// add equal to the beginning, so we filter exact numbers
				.val(compare + query)
				.trigger( notrigger ? '' : 'search', searchType ).end();
			// update sticky header cell
			if ($shcell.length) {
				$shcell
					.find('.dateCompare').val(compare + query).end()
					.find(compareSelect).val(compare);
			}
		};

		// Add date range picker
		t = '<input type="text" class="date date' + indx + '" placeholder="' +
			($hdr.data('placeholder') || $hdr.attr('data-placeholder') || c.widgetOptions.filter_placeholder.search || '') + '" />';
		$date = $(t).appendTo($cell);

		// add callbacks; preserve added callbacks
		o.oldonClose = o.onClose;

		o.onClose = function( selectedDate, ui ) {
			date1Compare();
			if (typeof o.oldonClose === 'function') { o.oldonClose(selectedDate, ui); }
		};
		$date.datepicker(o);

		// on reset
		c.$table.bind('filterReset', function(){
			if ($.isArray(o.compare)) {
				$cell.add($shcell).find(compareSelect).val( o.compare[ o.selected || 0 ] );
			}
			$cell.add($shcell).find('.date').val(o.defaultDate).datepicker('setDate', o.defaultDate || null);
			setTimeout(function(){
				date1Compare();
			}, 0);
		});

		// update date compare from hidden input, in case of saved filters
		c.$table.bind('filterFomatterUpdate', function(){
			var num, v = $input.val();
			if (/\s+-\s+/.test(v)) {
				// date range found; assume an exact match on one day
				$cell.find(compareSelect).val('=');
				num = v.split(/\s+-\s+/)[0];
				$date.datepicker( 'setDate', num || null );
			} else {
				num = (tsff.updateCompare($cell, $input, o)[1]).toString() || '';
				// differeniate 1388556000000 from 1/1/2014 using \d{5} regex
				num = num !== '' ? /\d{5}/g.test(num) ? new Date(Number(num)) : num || '' : '';
			}
			$cell.add($shcell).find('.date').datepicker( 'setDate', num || null );
			setTimeout(function(){
				date1Compare(true);
				ts.filter.formatterUpdated($cell, indx);
			}, 0);
		});

		if (o.compare) {
			// add compare select
			tsff.addCompare($cell, indx, o);
			$cell.find(compareSelect).bind('change', function(){
				date1Compare();
			});
		}

		// has sticky headers?
		c.$table.bind('stickyHeadersInit', function(){
			$shcell = c.widgetOptions.$sticky.find('.tablesorter-filter-row').children().eq(indx).empty();

			// add a jQuery datepicker!
			$shcell
				.append(t)
				.find('.date')
				.datepicker(o);

			if (o.compare) {
				// add compare select
				tsff.addCompare($shcell, indx, o);
				$shcell.find(compareSelect).bind('change', function(){
					$cell.find(compareSelect).val( $(this).val() );
					date1Compare();
				});
			}

		});

		// return the hidden input so the filter widget has a reference to it
		return $input.val( o.defaultDate ? o.defaultDate : '' );
	},

	/*************************\
	jQuery UI Datepicker (2 inputs)
	\*************************/
	uiDatepicker: function($cell, indx, defDate) {
		var o = $.extend({
			// filter formatter options
			endOfDay : true,
			textFrom : 'from',
			textTo : 'to',
			from : '', // defaultDate "from" input
			to : '', // defaultDate "to" input
			// include ANY jQuery UI spinner options below
			changeMonth : true,
			changeYear : true,
			numberOfMonths : 1
		}, defDate),
		t, closeDate, $shcell = [],
		c = $cell.closest('table')[0].config,
		validDate = function(d){
			return d instanceof Date && isFinite(d);
		},
		// Add a hidden input to hold the range values
		$input = $('<input class="dateRange" type="hidden">')
			.appendTo($cell)
			// hidden filter update namespace trigger by filter widget
			.bind('change' + c.namespace + 'filter', function(){
				var v = this.value;
				if (v.match(' - ')) {
					v = v.split(' - ');
					$cell.find('.dateTo').val(v[1]);
					closeDate(v[0]);
				} else if (v.match('>=')) {
					closeDate( v.replace('>=', '') );
				} else if (v.match('<=')) {
					closeDate( v.replace('<=', '') );
				}
			}),

		// make sure we're using parsed dates in the search
		$hdr = $cell.closest('thead').find('th[data-column=' + indx + ']').addClass('filter-parsed');
		// Add date range picker
		t = '<label>' + o.textFrom + '</label><input type="text" class="dateFrom" placeholder="' +
			($hdr.data('placeholderFrom') || $hdr.attr('data-placeholder-from') || c.widgetOptions.filter_placeholder.from || '') + '" />' +
			'<label>' + o.textTo + '</label><input type="text" class="dateTo" placeholder="' +
			($hdr.data('placeholderTo') || $hdr.attr('data-placeholder-to') || c.widgetOptions.filter_placeholder.to || '') + '" />';
		$(t).appendTo($cell);

		// add callbacks; preserve added callbacks
		o.oldonClose = o.onClose;

		closeDate = o.onClose = function( selectedDate, ui ) {
			var range,
				from = $cell.find('.dateFrom').datepicker('getDate'),
				to = $cell.find('.dateTo').datepicker('getDate');
			from = validDate(from) ? from.getTime() : '';
			to = validDate(to) ? ( o.endOfDay ? to.setHours(23, 59, 59) : to.getTime() ) || '' : '';
			range = from ? ( to ? from + ' - ' + to : '>=' + from ) : (to ? '<=' + to : '');
			$cell.add( $shcell )
				.find('.dateRange').val(range)
				.trigger('search');
			// date picker needs date objects
			from = from ? new Date(from) : '';
			to = to ? new Date(to) : '';

			if (/<=/.test(range)) {
				$cell.add( $shcell )
					.find('.dateFrom').datepicker('option', 'maxDate', to || null ).end()
					.find('.dateTo').datepicker('option', 'minDate', null).datepicker('setDate', to || null);
			} else if (/>=/.test(range)) {
				$cell.add( $shcell )
					.find('.dateFrom').datepicker('option', 'maxDate', null).datepicker('setDate', from || null).end()
					.find('.dateTo').datepicker('option', 'minDate', from || null );
			} else {
				$cell.add( $shcell )
					.find('.dateFrom').datepicker('option', 'maxDate', null).datepicker('setDate', from || null ).end()
					.find('.dateTo').datepicker('option', 'minDate', null).datepicker('setDate', to || null);
			}

			if (typeof o.oldonClose === 'function') { o.oldonClose(selectedDate, ui); }
		};

		o.defaultDate = o.from || '';
		$cell.find('.dateFrom').datepicker(o);
		o.defaultDate = o.to || '+7d'; // set to date +7 days from today (if not defined)
		$cell.find('.dateTo').datepicker(o);

		// update date compare from hidden input, in case of saved filters
		c.$table.bind('filterFomatterUpdate', function(){
			var val = $input.val() || '',
				from = '',
				to = '';
			// date range
			if (/\s+-\s+/.test(val)){
				val = val.split(/\s+-\s+/) || [];
				from = val[0] || '';
				to = val[1] || '';
			} else if (/>=/.test(val)) {
				// greater than date (to date empty)
				from = val.replace(/>=/, '') || '';
			} else if (/<=/.test(val)) {
				// less than date (from date empty)
				to = val.replace(/<=/, '') || '';
			}

			// differeniate 1388556000000 from 1/1/2014 using \d{5} regex
			from = from !== '' ? /\d{5}/g.test(from) ? new Date(Number(from)) : from || '' : '';
			to = to !== '' ? /\d{5}/g.test(to) ? new Date(Number(to)) : to || '' : '';

			$cell.add($shcell).find('.dateFrom').datepicker('setDate', from || null);
			$cell.add($shcell).find('.dateTo').datepicker('setDate', to || null);
			// give datepicker time to process
			setTimeout(function(){
				closeDate();
				ts.filter.formatterUpdated($cell, indx);
			}, 0);
		});

		// has sticky headers?
		c.$table.bind('stickyHeadersInit', function(){
			$shcell = c.widgetOptions.$sticky.find('.tablesorter-filter-row').children().eq(indx).empty();
			$shcell.append(t);

			// add a jQuery datepicker!
			o.defaultDate = o.from || '';
			$shcell.find('.dateFrom').datepicker(o);

			o.defaultDate = o.to || '+7d';
			$shcell.find('.dateTo').datepicker(o);

		});

		// on reset
		$cell.closest('table').bind('filterReset', function(){
			$cell.add($shcell).find('.dateFrom').val('').datepicker('setDate', o.from || null );
			$cell.add($shcell).find('.dateTo').val('').datepicker('setDate', o.to || null );
			setTimeout(function(){
				closeDate();
			}, 0);
		});

		// return the hidden input so the filter widget has a reference to it
		return $input.val( o.from ? ( o.to ? o.from + ' - ' + o.to : '>=' + o.from ) : (o.to ? '<=' + o.to : '') );
	},

	/**********************\
	HTML5 Number (spinner)
	\**********************/
	html5Number : function($cell, indx, def5Num) {
		var t, o = $.extend({
			value : 0,
			min : 0,
			max : 100,
			step : 1,
			delayed : true,
			disabled : false,
			addToggle : false,
			exactMatch : false,
			cellText : '',
			compare : '',
			skipTest: false
		}, def5Num),

		$input,
		// test browser for HTML5 range support
		$number = $('<input type="number" style="visibility:hidden;" value="test">').appendTo($cell),
		// test if HTML5 number is supported - from Modernizr
		numberSupported = o.skipTest || $number.attr('type') === 'number' && $number.val() !== 'test',
		$shcell = [],
		c = $cell.closest('table')[0].config,

		updateNumber = function(delayed, notrigger){
			var chkd = o.addToggle ? $cell.find('.toggle').is(':checked') : true,
				v = $cell.find('.number').val(),
				compare = ($.isArray(o.compare) ? $cell.find(compareSelect).val() || o.compare[ o.selected || 0] : o.compare) || '',
				searchType = c.$table[0].hasInitialized ? (delayed ? delayed : o.delayed) || '' : true;
			$input
				// add equal to the beginning, so we filter exact numbers
				.val( !o.addToggle || chkd ? (compare ? compare : o.exactMatch ? '=' : '') + v : '' )
				.trigger( notrigger ? '' : 'search', searchType ).end()
				.find('.number').val(v);
			if ($cell.find('.number').length) {
				$cell.find('.number')[0].disabled = (o.disabled || !chkd);
			}
			// update sticky header cell
			if ($shcell.length) {
				$shcell.find('.number').val(v)[0].disabled = (o.disabled || !chkd);
				$shcell.find(compareSelect).val(compare);
				if (o.addToggle) {
					$shcell.find('.toggle')[0].checked = chkd;
				}
			}
		};
		$number.remove();

		if (numberSupported) {
			t = o.addToggle ? '<div class="button"><input id="html5button' + indx + '" type="checkbox" class="toggle" />' +
				'<label for="html5button' + indx + '"></label></div>' : '';
			t += '<input class="number" type="number" min="' + o.min + '" max="' + o.max + '" value="' +
				o.value + '" step="' + o.step + '" />';
			// add HTML5 number (spinner)
			$cell
				.append(t + '<input type="hidden" />')
				.find('.toggle, .number').bind('change', function(){
					updateNumber();
				})
				.closest('thead').find('th[data-column=' + indx + ']')
				.addClass('filter-parsed') // get exact numbers from column
				// on reset
				.closest('table').bind('filterReset', function(){
					if ($.isArray(o.compare)) {
						$cell.add($shcell).find(compareSelect).val( o.compare[ o.selected || 0 ] );
					}
					// turn off the toggle checkbox
					if (o.addToggle) {
						$cell.find('.toggle')[0].checked = false;
						if ($shcell.length) {
							$shcell.find('.toggle')[0].checked = false;
						}
					}
					$cell.find('.number').val( o.value );
					setTimeout(function(){
						updateNumber();
					}, 0);
				});
			$input = $cell.find('input[type=hidden]').bind('change', function(){
				$cell.find('.number').val( this.value );
				updateNumber();
			});

			// update slider from hidden input, in case of saved filters
			c.$table.bind('filterFomatterUpdate', function(){
				var val = tsff.updateCompare($cell, $input, o)[0] || o.value;
				$cell.find('.number').val( ((val || '') + '').replace(/[><=]/g,'') );
				updateNumber(false, true);
				ts.filter.formatterUpdated($cell, indx);
			});

			if (o.compare) {
				// add compare select
				tsff.addCompare($cell, indx, o);
				$cell.find(compareSelect).bind('change', function(){
					updateNumber();
				});
			}

			// has sticky headers?
			c.$table.bind('stickyHeadersInit', function(){
				$shcell = c.widgetOptions.$sticky.find('.tablesorter-filter-row').children().eq(indx).empty();
				$shcell
					.append(t)
					.find('.toggle, .number').bind('change', function(){
						$cell.find('.number').val( $(this).val() );
						updateNumber();
					});

				if (o.compare) {
					// add compare select
					tsff.addCompare($shcell, indx, o);
					$shcell.find(compareSelect).bind('change', function(){
						$cell.find(compareSelect).val( $(this).val() );
						updateNumber();
					});
				}

				updateNumber();
			});

			updateNumber();

		}

		return numberSupported ? $cell.find('input[type="hidden"]') : $('<input type="search">');
	},

	/**********************\
	HTML5 range slider
	\**********************/
	html5Range : function($cell, indx, def5Range) {
		var o = $.extend({
			value : 0,
			min : 0,
			max : 100,
			step : 1,
			delayed : true,
			valueToHeader : true,
			exactMatch : true,
			cellText : '',
			compare : '',
			allText : 'all',
			skipTest : false
		}, def5Range),

		$input,
		// test browser for HTML5 range support
		$range = $('<input type="range" style="visibility:hidden;" value="test">').appendTo($cell),
		// test if HTML5 range is supported - from Modernizr (but I left out the method to detect in Safari 2-4)
		// see https://github.com/Modernizr/Modernizr/blob/master/feature-detects/inputtypes.js
		rangeSupported = o.skipTest || $range.attr('type') === 'range' && $range.val() !== 'test',
		$shcell = [],
		c = $cell.closest('table')[0].config,

		updateRange = function(v, delayed, notrigger){
			/*jshint eqeqeq:false */
			// hidden input changes may include compare symbols
			v = ( typeof v === "undefined" ? $input.val() : v ).toString().replace(/[<>=]/g,'') || o.value;
			var compare = ($.isArray(o.compare) ? $cell.find(compareSelect).val() || o.compare[ o.selected || 0] : o.compare) || '',
				t = ' (' + (compare ? compare + v : v == o.min ? o.allText : v) + ')',
				searchType =  c.$table[0].hasInitialized ? (delayed ? delayed : o.delayed) || '' : true;
			$cell.find('input[type=hidden]')
				// add equal to the beginning, so we filter exact numbers
				.val( ( compare ? compare + v : ( v == o.min ? '' : ( o.exactMatch ? '=' : '' ) + v ) ) )
				//( val == o.min ? '' : val + (o.exactMatch ? '=' : ''))
				.trigger( notrigger ? '' : 'search', searchType ).end()
				.find('.range').val(v);
			// or add current value to the header cell, if desired
			$cell.closest('thead').find('th[data-column=' + indx + ']').find('.curvalue').html(t);
			// update sticky header cell
			if ($shcell.length) {
				$shcell
					.find('.range').val(v).end()
					.find(compareSelect).val( compare );
				$shcell.closest('thead').find('th[data-column=' + indx + ']').find('.curvalue').html(t);
			}
		};
		$range.remove();

		if (rangeSupported) {
			// add HTML5 range
			$cell
				.html('<input type="hidden"><input class="range" type="range" min="' + o.min + '" max="' + o.max + '" value="' + o.value + '" />')
				.closest('thead').find('th[data-column=' + indx + ']')
				.addClass('filter-parsed') // get exact numbers from column
				// add span to header for the current slider value
				.find('.tablesorter-header-inner').append('<span class="curvalue" />');
			// hidden filter update namespace trigger by filter widget
			$input = $cell.find('input[type=hidden]').bind('change' + c.namespace + 'filter', function(){
				/*jshint eqeqeq:false */
				var v = this.value,
					compare = ($.isArray(o.compare) ? $cell.find(compareSelect).val() || o.compare[ o.selected || 0] : o.compare) || '';
				if (v !== this.lastValue) {
					this.lastValue = ( compare ? compare + v : ( v == o.min ? '' : ( o.exactMatch ? '=' : '' ) + v ) );
					this.value = this.lastValue;
					updateRange( v );
				}
			});

			$cell.find('.range').bind('change', function(){
				updateRange( this.value );
			});

			// update spinner from hidden input, in case of saved filters
			c.$table.bind('filterFomatterUpdate', function(){
				var val = tsff.updateCompare($cell, $input, o)[0];
				$cell.find('.range').val( val );
				updateRange(val, false, true);
				ts.filter.formatterUpdated($cell, indx);
			});

			if (o.compare) {
				// add compare select
				tsff.addCompare($cell, indx, o);
				$cell.find(compareSelect).bind('change', function(){
					updateRange();
				});
			}

			// has sticky headers?
			c.$table.bind('stickyHeadersInit', function(){
				$shcell = c.widgetOptions.$sticky.find('.tablesorter-filter-row').children().eq(indx).empty();
				$shcell
					.html('<input class="range" type="range" min="' + o.min + '" max="' + o.max + '" value="' + o.value + '" />')
					.find('.range').bind('change', function(){
						updateRange( $shcell.find('.range').val() );
					});
				updateRange();

				if (o.compare) {
					// add compare select
					tsff.addCompare($shcell, indx, o);
					$shcell.find(compareSelect).bind('change', function(){
						$cell.find(compareSelect).val( $(this).val() );
						updateRange();
					});
				}

			});

			// on reset
			$cell.closest('table').bind('filterReset', function(){
				if ($.isArray(o.compare)) {
					$cell.add($shcell).find(compareSelect).val( o.compare[ o.selected || 0 ] );
				}
				setTimeout(function(){
					updateRange(o.value, false, true);
				}, 0);
			});
			updateRange();

		}

		return rangeSupported ? $cell.find('input[type="hidden"]') : $('<input type="search">');
	},

	/**********************\
	HTML5 Color picker
	\**********************/
	html5Color: function($cell, indx, defColor) {
		var t, o = $.extend({
			value : '#000000',
			disabled : false,
			addToggle : true,
			exactMatch : true,
			valueToHeader : false,
			skipTest : false
		}, defColor),
		$input,
		// Add a hidden input to hold the range values
		$color = $('<input type="color" style="visibility:hidden;" value="test">').appendTo($cell),
		// test if HTML5 color is supported - from Modernizr
		colorSupported = o.skipTest || $color.attr('type') === 'color' && $color.val() !== 'test',
		$shcell = [],
		c = $cell.closest('table')[0].config,

		updateColor = function(v, notrigger){
			v = ( typeof v === "undefined" ? $input.val() : v ).toString().replace('=','') || o.value;
			var chkd = true,
				t = ' (' + v + ')';
			if (o.addToggle) {
				chkd = $cell.find('.toggle').is(':checked');
			}
			if ($cell.find('.colorpicker').length) {
				$cell.find('.colorpicker').val(v)[0].disabled = (o.disabled || !chkd);
			}

			$input
				.val( chkd ? v + (o.exactMatch ? '=' : '') : '' )
				.trigger( !c.$table[0].hasInitialized || notrigger ? '' : 'search' );
			if (o.valueToHeader) {
				// add current color to the header cell
				$cell.closest('thead').find('th[data-column=' + indx + ']').find('.curcolor').html(t);
			} else {
				// current color to span in cell
				$cell.find('.currentColor').html(t);
			}

			// update sticky header cell
			if ($shcell.length) {
				$shcell.find('.colorpicker').val(v)[0].disabled = (o.disabled || !chkd);
				if (o.addToggle) {
					$shcell.find('.toggle')[0].checked = chkd;
				}
				if (o.valueToHeader) {
					// add current color to the header cell
					$shcell.closest('thead').find('th[data-column=' + indx + ']').find('.curcolor').html(t);
				} else {
					// current color to span in cell
					$shcell.find('.currentColor').html(t);
				}
			}
		};
		$color.remove();

		if (colorSupported) {
			t = '' + indx + Math.round(Math.random() * 100);
			// add HTML5 color picker
			t = '<div class="color-controls-wrapper">' +
				(o.addToggle ? '<div class="button"><input id="colorbutton' + t + '" type="checkbox" class="toggle" /><label for="colorbutton' +
				t + '"></label></div>' : '') +
				'<input type="hidden"><input class="colorpicker" type="color" />' +
				(o.valueToHeader ? '' : '<span class="currentColor">(#000000)</span>') + '</div>';
			$cell.html(t);
			// add span to header for the current color value - only works if the line in the updateColor() function is also un-commented out
			if (o.valueToHeader) {
				$cell.closest('thead').find('th[data-column=' + indx + ']').find('.tablesorter-header-inner').append('<span class="curcolor" />');
			}

			$cell.find('.toggle, .colorpicker').bind('change', function(){
				updateColor( $cell.find('.colorpicker').val() );
			});

			// hidden filter update namespace trigger by filter widget
			$input = $cell.find('input[type=hidden]').bind('change' + c.namespace + 'filter', function(){
				updateColor( this.value );
			});

			// update slider from hidden input, in case of saved filters
			c.$table.bind('filterFomatterUpdate', function(){
				updateColor( $input.val(), true );
				ts.filter.formatterUpdated($cell, indx);
			});

			// on reset
			$cell.closest('table').bind('filterReset', function(){
				// just turn off the colorpicker
				if (o.addToggle) {
					$cell.find('.toggle')[0].checked = false;
				}
				// delay needed because default color needs to be set in the filter
				// there is no compare option here, so if addToggle = false,
				// default color is #000000 (even with no value set)
				setTimeout(function(){
					updateColor();
				}, 0);
			});

			// has sticky headers?
			c.$table.bind('stickyHeadersInit', function(){
				$shcell = c.widgetOptions.$sticky.find('.tablesorter-filter-row').children().eq(indx);
				$shcell
					.html(t)
					.find('.toggle, .colorpicker').bind('change', function(){
						updateColor( $shcell.find('.colorpicker').val() );
					});
				updateColor( $shcell.find('.colorpicker').val() );
			});

			updateColor( o.value );
		}
		return colorSupported ? $cell.find('input[type="hidden"]') : $('<input type="search">');
	}

};

})(jQuery);

/*! Filter widget formatter functions - updated 6/18/2014 (v2.17.2)
 * requires: tableSorter 2.15+ and jQuery 1.4.3+
 * jQuery UI spinner, silder, range slider & datepicker (range)
 * HTML5 number (spinner), range slider & color selector
 */
;(function(g){var r=g.tablesorter||{},n=r.filterFormatter={addCompare:function(b,f,e){if(e.compare&&g.isArray(e.compare)&&1<e.compare.length){var a="",c=[".compare-select".slice(1)," "+".compare-select".slice(1),""],l=e.cellText?'<label class="'+c.join("-label")+f+'">'+e.cellText+"</label>":"";g.each(e.compare,function(b,c){a+="<option "+(e.selected===b?"selected":"")+">"+c+"</option>"});b.wrapInner('<div class="'+c.join("-wrapper")+f+'" />').prepend(l+'<select class="'+c.join("")+f+'" />').find("select").append(a)}}, updateCompare:function(b,f,e){f=f.val()||"";var a=f.replace(/\s*?[><=]\s*?/g,""),c=f.match(/[><=]/g)||"";e.compare&&(g.isArray(e.compare)&&(c=(c||[]).join("")||e.compare[e.selected||0]),b.find(".compare-select").val(c));return[f,a]},uiSpinner:function(b,f,e){var a=g.extend({delayed:!0,addToggle:!0,exactMatch:!0,value:1,cellText:"",compare:"",min:0,max:100,step:1,disabled:!1},e),c=b.closest("table")[0].config,l=g('<input class="filter" type="hidden">').appendTo(b).bind("change"+c.namespace+"filter", function(){d({value:this.value,delayed:!1})}),h=[],d=function(f,k){var p=!0,d,q=f&&f.value&&r.formatFloat((f.value+"").replace(/[><=]/g,""))||b.find(".spinner").val()||a.value,e=(g.isArray(a.compare)?b.find(".compare-select").val()||a.compare[a.selected||0]:a.compare)||"",l=f&&"boolean"===typeof f.delayed?f.delayed:c.$table[0].hasInitialized?a.delayed||"":!0;a.addToggle&&(p=b.find(".toggle").is(":checked"));d=a.disabled||!p?"disable":"enable";b.find(".filter").val(p?(e?e:a.exactMatch?"=":"")+q:"").trigger(k? "":"search",l).end().find(".spinner").spinner(d).val(q);h.length&&(h.find(".spinner").spinner(d).val(q).end().find(".compare-select").val(e),a.addToggle&&(h.find(".toggle")[0].checked=p))};a.oldcreate=a.create;a.oldspin=a.spin;a.create=function(b,k){d();"function"===typeof a.oldcreate&&a.oldcreate(b,k)};a.spin=function(b,k){d(k);"function"===typeof a.oldspin&&a.oldspin(b,k)};a.addToggle&&g('<div class="button"><input id="uispinnerbutton'+f+'" type="checkbox" class="toggle" /><label for="uispinnerbutton'+ f+'"></label></div>').appendTo(b).find(".toggle").bind("change",function(){d()});b.closest("thead").find("th[data-column="+f+"]").addClass("filter-parsed");g('<input class="spinner spinner'+f+'" />').val(a.value).appendTo(b).spinner(a).bind("change keyup",function(){d()});c.$table.bind("filterFomatterUpdate",function(){var c=n.updateCompare(b,l,a)[0];b.find(".spinner").val(c);d({value:c},!0)});a.compare&&(n.addCompare(b,f,a),b.find(".compare-select").bind("change",function(){d()}));c.$table.bind("stickyHeadersInit", function(){h=c.widgetOptions.$sticky.find(".tablesorter-filter-row").children().eq(f).empty();a.addToggle&&g('<div class="button"><input id="stickyuispinnerbutton'+f+'" type="checkbox" class="toggle" /><label for="stickyuispinnerbutton'+f+'"></label></div>').appendTo(h).find(".toggle").bind("change",function(){b.find(".toggle")[0].checked=this.checked;d()});g('<input class="spinner spinner'+f+'" />').val(a.value).appendTo(h).spinner(a).bind("change keyup",function(){b.find(".spinner").val(this.value); d()});a.compare&&(n.addCompare(h,f,a),h.find(".compare-select").bind("change",function(){b.find(".compare-select").val(g(this).val());d()}))});c.$table.bind("filterReset",function(){g.isArray(a.compare)&&b.add(h).find(".compare-select").val(a.compare[a.selected||0]);a.addToggle&&(b.find(".toggle")[0].checked=!1);b.find(".spinner").spinner("value",a.value);setTimeout(function(){d()},0)});d();return l},uiSlider:function(b,f,e){var a=g.extend({delayed:!0,valueToHeader:!1,exactMatch:!0,cellText:"",compare:"", allText:"all",value:0,min:0,max:100,step:1,range:"min"},e),c=b.closest("table")[0].config,l=g('<input class="filter" type="hidden">').appendTo(b).bind("change"+c.namespace+"filter",function(){d({value:this.value})}),h=[],d=function(d,k){var p="undefined"!==typeof d?r.formatFloat((d.value+"").replace(/[><=]/g,""))||a.value:a.value,e=a.compare?p:p===a.min?a.allText:p,q=(g.isArray(a.compare)?b.find(".compare-select").val()||a.compare[a.selected||0]:a.compare)||"",e=q+e,l=d&&"boolean"===typeof d.delayed? d.delayed:c.$table[0].hasInitialized?a.delayed||"":!0;a.valueToHeader?b.closest("thead").find("th[data-column="+f+"]").find(".curvalue").html(" ("+e+")"):b.find(".ui-slider-handle").addClass("value-popup").attr("data-value",e);b.find(".filter").val(q?q+p:p===a.min?"":(a.exactMatch?"=":"")+p).trigger(k?"":"search",l).end().find(".slider").slider("value",p);h.length&&(h.find(".compare-select").val(q).end().find(".slider").slider("value",p),a.valueToHeader?h.closest("thead").find("th[data-column="+f+ "]").find(".curvalue").html(" ("+e+")"):h.find(".ui-slider-handle").addClass("value-popup").attr("data-value",e))};b.closest("thead").find("th[data-column="+f+"]").addClass("filter-parsed");a.valueToHeader&&b.closest("thead").find("th[data-column="+f+"]").find(".tablesorter-header-inner").append('<span class="curvalue" />');a.oldcreate=a.create;a.oldslide=a.slide;a.create=function(b,k){d();"function"===typeof a.oldcreate&&a.oldcreate(b,k)};a.slide=function(b,k){d(k);"function"===typeof a.oldslide&& a.oldslide(b,k)};g('<div class="slider slider'+f+'"/>').appendTo(b).slider(a);c.$table.bind("filterFomatterUpdate",function(){var c=n.updateCompare(b,l,a)[0];b.find(".slider").slider("value",c);d({value:c},!1)});a.compare&&(n.addCompare(b,f,a),b.find(".compare-select").bind("change",function(){d({value:b.find(".slider").slider("value")})}));c.$table.bind("filterReset",function(){g.isArray(a.compare)&&b.add(h).find(".compare-select").val(a.compare[a.selected||0]);setTimeout(function(){d({value:a.value})}, 0)});c.$table.bind("stickyHeadersInit",function(){h=c.widgetOptions.$sticky.find(".tablesorter-filter-row").children().eq(f).empty();g('<div class="slider slider'+f+'"/>').val(a.value).appendTo(h).slider(a).bind("change keyup",function(){b.find(".slider").slider("value",this.value);d()});a.compare&&(n.addCompare(h,f,a),h.find(".compare-select").bind("change",function(){b.find(".compare-select").val(g(this).val());d()}))});return l},uiRange:function(b,f,e){var a=g.extend({delayed:!0,valueToHeader:!1, values:[0,100],min:0,max:100,range:!0},e),c=b.closest("table")[0].config,l=g('<input class="filter" type="hidden">').appendTo(b).bind("change"+c.namespace+"filter",function(){d()}),h=[],d=function(){var b=l.val(),c=b.split(" - ");""===b&&(c=[a.min,a.max]);c&&c[1]&&m({values:c,delay:!1},!0)},m=function(k,p){var d=k&&k.values||a.values,e=d[0]+" - "+d[1],g=d[0]===a.min&&d[1]===a.max?"":e,l=k&&"boolean"===typeof k.delayed?k.delayed:c.$table[0].hasInitialized?a.delayed||"":!0;a.valueToHeader?b.closest("thead").find("th[data-column="+ f+"]").find(".currange").html(" ("+e+")"):b.find(".ui-slider-handle").addClass("value-popup").eq(0).attr("data-value",d[0]).end().eq(1).attr("data-value",d[1]);b.find(".filter").val(g).trigger(p?"":"search",l).end().find(".range").slider("values",d);h.length&&(h.find(".range").slider("values",d),a.valueToHeader?h.closest("thead").find("th[data-column="+f+"]").find(".currange").html(" ("+e+")"):h.find(".ui-slider-handle").addClass("value-popup").eq(0).attr("data-value",d[0]).end().eq(1).attr("data-value", d[1]))};b.closest("thead").find("th[data-column="+f+"]").addClass("filter-parsed");a.valueToHeader&&b.closest("thead").find("th[data-column="+f+"]").find(".tablesorter-header-inner").append('<span class="currange"/>');a.oldcreate=a.create;a.oldslide=a.slide;a.create=function(b,c){m();"function"===typeof a.oldcreate&&a.oldcreate(b,c)};a.slide=function(b,c){m(c);"function"===typeof a.oldslide&&a.oldslide(b,c)};g('<div class="range range'+f+'"/>').appendTo(b).slider(a);c.$table.bind("filterFomatterUpdate", function(){d()});c.$table.bind("filterReset",function(){b.find(".range").slider("values",a.values);setTimeout(function(){m()},0)});c.$table.bind("stickyHeadersInit",function(){h=c.widgetOptions.$sticky.find(".tablesorter-filter-row").children().eq(f).empty();g('<div class="range range'+f+'"/>').val(a.value).appendTo(h).slider(a).bind("change keyup",function(){b.find(".range").val(this.value);m()})});return l},uiDateCompare:function(b,f,e){var a=g.extend({cellText:"",compare:"",endOfDay:!0,defaultDate:"", changeMonth:!0,changeYear:!0,numberOfMonths:1},e),c,l=b.closest("table")[0].config;e=b.closest("thead").find("th[data-column="+f+"]").addClass("filter-parsed");var h=g('<input class="dateCompare" type="hidden">').appendTo(b).bind("change"+l.namespace+"filter",function(){var b=this.value;if(b)a.onClose(b)}),d,m=[],k=function(k){var d,f;d=c.datepicker("getDate")||"";var e=(g.isArray(a.compare)?b.find(".compare-select").val()||a.compare[a.selected||0]:a.compare)||"",h=l.$table[0].hasInitialized?a.delayed|| "":!0;c.datepicker("setDate",(""===d?"":d)||null);""===d&&(k=!1);f=(d=c.datepicker("getDate"))?(a.endOfDay&&/<=/.test(e)?d.setHours(23,59,59):d.getTime())||"":"";d&&a.endOfDay&&"="===e&&(e="",f+=" - "+d.setHours(23,59,59),k=!1);b.find(".dateCompare").val(e+f).trigger(k?"":"search",h).end();m.length&&m.find(".dateCompare").val(e+f).end().find(".compare-select").val(e)};d='<input type="text" class="date date'+f+'" placeholder="'+(e.data("placeholder")||e.attr("data-placeholder")||l.widgetOptions.filter_placeholder.search|| "")+'" />';c=g(d).appendTo(b);a.oldonClose=a.onClose;a.onClose=function(b,c){k();"function"===typeof a.oldonClose&&a.oldonClose(b,c)};c.datepicker(a);l.$table.bind("filterReset",function(){g.isArray(a.compare)&&b.add(m).find(".compare-select").val(a.compare[a.selected||0]);b.add(m).find(".date").val(a.defaultDate).datepicker("setDate",a.defaultDate||null);setTimeout(function(){k()},0)});l.$table.bind("filterFomatterUpdate",function(){var d;d=h.val();/\s+-\s+/.test(d)?(b.find(".compare-select").val("="), d=d.split(/\s+-\s+/)[0],c.datepicker("setDate",d||null)):(d=n.updateCompare(b,h,a)[1].toString()||"",d=""!==d?/\d{5}/g.test(d)?new Date(Number(d)):d||"":"");b.add(m).find(".date").datepicker("setDate",d||null);setTimeout(function(){k(!0)},0)});a.compare&&(n.addCompare(b,f,a),b.find(".compare-select").bind("change",function(){k()}));l.$table.bind("stickyHeadersInit",function(){m=l.widgetOptions.$sticky.find(".tablesorter-filter-row").children().eq(f).empty();m.append(d).find(".date").datepicker(a); a.compare&&(n.addCompare(m,f,a),m.find(".compare-select").bind("change",function(){b.find(".compare-select").val(g(this).val());k()}))});return h.val(a.defaultDate?a.defaultDate:"")},uiDatepicker:function(b,f,e){var a=g.extend({endOfDay:!0,textFrom:"from",textTo:"to",from:"",to:"",changeMonth:!0,changeYear:!0,numberOfMonths:1},e),c,l,h=[],d=b.closest("table")[0].config,m=g('<input class="dateRange" type="hidden">').appendTo(b).bind("change"+d.namespace+"filter",function(){var a=this.value;a.match(" - ")? (a=a.split(" - "),b.find(".dateTo").val(a[1]),l(a[0])):a.match(">=")?l(a.replace(">=","")):a.match("<=")&&l(a.replace("<=",""))});e=b.closest("thead").find("th[data-column="+f+"]").addClass("filter-parsed");c="<label>"+a.textFrom+'</label><input type="text" class="dateFrom" placeholder="'+(e.data("placeholderFrom")||e.attr("data-placeholder-from")||d.widgetOptions.filter_placeholder.from||"")+'" /><label>'+a.textTo+'</label><input type="text" class="dateTo" placeholder="'+(e.data("placeholderTo")|| e.attr("data-placeholder-to")||d.widgetOptions.filter_placeholder.to||"")+'" />';g(c).appendTo(b);a.oldonClose=a.onClose;l=a.onClose=function(c,d){var f,e=b.find(".dateFrom").datepicker("getDate"),g=b.find(".dateTo").datepicker("getDate"),e=e instanceof Date&&isFinite(e)?e.getTime():"",g=g instanceof Date&&isFinite(g)?(a.endOfDay?g.setHours(23,59,59):g.getTime())||"":"";f=e?g?e+" - "+g:">="+e:g?"<="+g:"";b.add(h).find(".dateRange").val(f).trigger("search");e=e?new Date(e):"";g=g?new Date(g):"";/<=/.test(f)? b.add(h).find(".dateFrom").datepicker("option","maxDate",g||null).end().find(".dateTo").datepicker("option","minDate",null).datepicker("setDate",g||null):/>=/.test(f)?b.add(h).find(".dateFrom").datepicker("option","maxDate",null).datepicker("setDate",e||null).end().find(".dateTo").datepicker("option","minDate",e||null):b.add(h).find(".dateFrom").datepicker("option","maxDate",null).datepicker("setDate",e||null).end().find(".dateTo").datepicker("option","minDate",null).datepicker("setDate",g||null); "function"===typeof a.oldonClose&&a.oldonClose(c,d)};a.defaultDate=a.from||"";b.find(".dateFrom").datepicker(a);a.defaultDate=a.to||"+7d";b.find(".dateTo").datepicker(a);d.$table.bind("filterFomatterUpdate",function(){var a=m.val()||"",c="",d="";/\s+-\s+/.test(a)?(a=a.split(/\s+-\s+/)||[],c=a[0]||"",d=a[1]||""):/>=/.test(a)?c=a.replace(/>=/,"")||"":/<=/.test(a)&&(d=a.replace(/<=/,"")||"");c=""!==c?/\d{5}/g.test(c)?new Date(Number(c)):c||"":"";d=""!==d?/\d{5}/g.test(d)?new Date(Number(d)):d||"":""; b.add(h).find(".dateFrom").datepicker("setDate",c||null);b.add(h).find(".dateTo").datepicker("setDate",d||null);setTimeout(function(){l()},0)});d.$table.bind("stickyHeadersInit",function(){h=d.widgetOptions.$sticky.find(".tablesorter-filter-row").children().eq(f).empty();h.append(c);a.defaultDate=a.from||"";h.find(".dateFrom").datepicker(a);a.defaultDate=a.to||"+7d";h.find(".dateTo").datepicker(a)});b.closest("table").bind("filterReset",function(){b.add(h).find(".dateFrom").val("").datepicker("setDate", a.from||null);b.add(h).find(".dateTo").val("").datepicker("setDate",a.to||null);setTimeout(function(){l()},0)});return m.val(a.from?a.to?a.from+" - "+a.to:">="+a.from:a.to?"<="+a.to:"")},html5Number:function(b,f,e){var a,c=g.extend({value:0,min:0,max:100,step:1,delayed:!0,disabled:!1,addToggle:!1,exactMatch:!1,cellText:"",compare:"",skipTest:!1},e),l;e=g('<input type="number" style="visibility:hidden;" value="test">').appendTo(b);var h=c.skipTest||"number"===e.attr("type")&&"test"!==e.val(),d=[], m=b.closest("table")[0].config,k=function(a,f){var e=c.addToggle?b.find(".toggle").is(":checked"):!0,k=b.find(".number").val(),h=(g.isArray(c.compare)?b.find(".compare-select").val()||c.compare[c.selected||0]:c.compare)||"",n=m.$table[0].hasInitialized?(a?a:c.delayed)||"":!0;l.val(!c.addToggle||e?(h?h:c.exactMatch?"=":"")+k:"").trigger(f?"":"search",n).end().find(".number").val(k);b.find(".number").length&&(b.find(".number")[0].disabled=c.disabled||!e);d.length&&(d.find(".number").val(k)[0].disabled= c.disabled||!e,d.find(".compare-select").val(h),c.addToggle&&(d.find(".toggle")[0].checked=e))};e.remove();h&&(a=c.addToggle?'<div class="button"><input id="html5button'+f+'" type="checkbox" class="toggle" /><label for="html5button'+f+'"></label></div>':"",a+='<input class="number" type="number" min="'+c.min+'" max="'+c.max+'" value="'+c.value+'" step="'+c.step+'" />',b.append(a+'<input type="hidden" />').find(".toggle, .number").bind("change",function(){k()}).closest("thead").find("th[data-column="+ f+"]").addClass("filter-parsed").closest("table").bind("filterReset",function(){g.isArray(c.compare)&&b.add(d).find(".compare-select").val(c.compare[c.selected||0]);c.addToggle&&(b.find(".toggle")[0].checked=!1,d.length&&(d.find(".toggle")[0].checked=!1));b.find(".number").val(c.value);setTimeout(function(){k()},0)}),l=b.find("input[type=hidden]").bind("change",function(){b.find(".number").val(this.value);k()}),m.$table.bind("filterFomatterUpdate",function(){var a=n.updateCompare(b,l,c)[0]||c.value; b.find(".number").val(((a||"")+"").replace(/[><=]/g,""));k(!1,!0)}),c.compare&&(n.addCompare(b,f,c),b.find(".compare-select").bind("change",function(){k()})),m.$table.bind("stickyHeadersInit",function(){d=m.widgetOptions.$sticky.find(".tablesorter-filter-row").children().eq(f).empty();d.append(a).find(".toggle, .number").bind("change",function(){b.find(".number").val(g(this).val());k()});c.compare&&(n.addCompare(d,f,c),d.find(".compare-select").bind("change",function(){b.find(".compare-select").val(g(this).val()); k()}));k()}),k());return h?b.find('input[type="hidden"]'):g('<input type="search">')},html5Range:function(b,f,e){var a=g.extend({value:0,min:0,max:100,step:1,delayed:!0,valueToHeader:!0,exactMatch:!0,cellText:"",compare:"",allText:"all",skipTest:!1},e),c;e=g('<input type="range" style="visibility:hidden;" value="test">').appendTo(b);var l=a.skipTest||"range"===e.attr("type")&&"test"!==e.val(),h=[],d=b.closest("table")[0].config,m=function(e,l,m){e=("undefined"===typeof e?c.val():e).toString().replace(/[<>=]/g, "")||a.value;var q=(g.isArray(a.compare)?b.find(".compare-select").val()||a.compare[a.selected||0]:a.compare)||"",n=" ("+(q?q+e:e==a.min?a.allText:e)+")";l=d.$table[0].hasInitialized?(l?l:a.delayed)||"":!0;b.find("input[type=hidden]").val(q?q+e:e==a.min?"":(a.exactMatch?"=":"")+e).trigger(m?"":"search",l).end().find(".range").val(e);b.closest("thead").find("th[data-column="+f+"]").find(".curvalue").html(n);h.length&&(h.find(".range").val(e).end().find(".compare-select").val(q),h.closest("thead").find("th[data-column="+ f+"]").find(".curvalue").html(n))};e.remove();l&&(b.html('<input type="hidden"><input class="range" type="range" min="'+a.min+'" max="'+a.max+'" value="'+a.value+'" />').closest("thead").find("th[data-column="+f+"]").addClass("filter-parsed").find(".tablesorter-header-inner").append('<span class="curvalue" />'),c=b.find("input[type=hidden]").bind("change"+d.namespace+"filter",function(){var c=this.value,d=(g.isArray(a.compare)?b.find(".compare-select").val()||a.compare[a.selected||0]:a.compare)|| "";c!==this.lastValue&&(this.value=this.lastValue=d?d+c:c==a.min?"":(a.exactMatch?"=":"")+c,m(c))}),b.find(".range").bind("change",function(){m(this.value)}),d.$table.bind("filterFomatterUpdate",function(){var d=n.updateCompare(b,c,a)[0];b.find(".range").val(d);m(d,!1,!0)}),a.compare&&(n.addCompare(b,f,a),b.find(".compare-select").bind("change",function(){m()})),d.$table.bind("stickyHeadersInit",function(){h=d.widgetOptions.$sticky.find(".tablesorter-filter-row").children().eq(f).empty();h.html('<input class="range" type="range" min="'+ a.min+'" max="'+a.max+'" value="'+a.value+'" />').find(".range").bind("change",function(){m(h.find(".range").val())});m();a.compare&&(n.addCompare(h,f,a),h.find(".compare-select").bind("change",function(){b.find(".compare-select").val(g(this).val());m()}))}),b.closest("table").bind("filterReset",function(){g.isArray(a.compare)&&b.add(h).find(".compare-select").val(a.compare[a.selected||0]);setTimeout(function(){m(a.value,!1,!0)},0)}),m());return l?b.find('input[type="hidden"]'):g('<input type="search">')}, html5Color:function(b,f,e){var a,c=g.extend({value:"#000000",disabled:!1,addToggle:!0,exactMatch:!0,valueToHeader:!1,skipTest:!1},e),l;e=g('<input type="color" style="visibility:hidden;" value="test">').appendTo(b);var h=c.skipTest||"color"===e.attr("type")&&"test"!==e.val(),d=[],m=b.closest("table")[0].config,k=function(a,e){a=("undefined"===typeof a?l.val():a).toString().replace("=","")||c.value;var g=!0,h=" ("+a+")";c.addToggle&&(g=b.find(".toggle").is(":checked"));b.find(".colorpicker").length&& (b.find(".colorpicker").val(a)[0].disabled=c.disabled||!g);l.val(g?a+(c.exactMatch?"=":""):"").trigger(!m.$table[0].hasInitialized||e?"":"search");c.valueToHeader?b.closest("thead").find("th[data-column="+f+"]").find(".curcolor").html(h):b.find(".currentColor").html(h);d.length&&(d.find(".colorpicker").val(a)[0].disabled=c.disabled||!g,c.addToggle&&(d.find(".toggle")[0].checked=g),c.valueToHeader?d.closest("thead").find("th[data-column="+f+"]").find(".curcolor").html(h):d.find(".currentColor").html(h))}; e.remove();h&&(a=""+f+Math.round(100*Math.random()),a='<div class="color-controls-wrapper">'+(c.addToggle?'<div class="button"><input id="colorbutton'+a+'" type="checkbox" class="toggle" /><label for="colorbutton'+a+'"></label></div>':"")+'<input type="hidden"><input class="colorpicker" type="color" />'+(c.valueToHeader?"":'<span class="currentColor">(#000000)</span>')+"</div>",b.html(a),c.valueToHeader&&b.closest("thead").find("th[data-column="+f+"]").find(".tablesorter-header-inner").append('<span class="curcolor" />'), b.find(".toggle, .colorpicker").bind("change",function(){k(b.find(".colorpicker").val())}),l=b.find("input[type=hidden]").bind("change"+m.namespace+"filter",function(){k(this.value)}),m.$table.bind("filterFomatterUpdate",function(){k(l.val(),!0)}),b.closest("table").bind("filterReset",function(){c.addToggle&&(b.find(".toggle")[0].checked=!1);setTimeout(function(){k()},0)}),m.$table.bind("stickyHeadersInit",function(){d=m.widgetOptions.$sticky.find(".tablesorter-filter-row").children().eq(f);d.html(a).find(".toggle, .colorpicker").bind("change", function(){k(d.find(".colorpicker").val())});k(d.find(".colorpicker").val())}),k(c.value));return h?b.find('input[type="hidden"]'):g('<input type="search">')}}})(jQuery);
/*! tableSorter 2.16+ widgets - updated 7/17/2014 (v2.17.5)
 *
 * Column Styles
 * Column Filters
 * Column Resizing
 * Sticky Header
 * UI Theme (generalized)
 * Save Sort
 * [ "columns", "filter", "resizable", "stickyHeaders", "uitheme", "saveSort" ]
 */
/*jshint browser:true, jquery:true, unused:false, loopfunc:true */
/*global jQuery: false, localStorage: false, navigator: false */
;(function($) {
"use strict";
var ts = $.tablesorter = $.tablesorter || {};

ts.themes = {
	"bootstrap" : {
		table      : 'table table-bordered table-striped',
		caption    : 'caption',
		header     : 'bootstrap-header', // give the header a gradient background
		footerRow  : '',
		footerCells: '',
		icons      : '', // add "icon-white" to make them white; this icon class is added to the <i> in the header
		sortNone   : 'bootstrap-icon-unsorted',
		sortAsc    : 'icon-chevron-up glyphicon glyphicon-chevron-up',
		sortDesc   : 'icon-chevron-down glyphicon glyphicon-chevron-down',
		active     : '', // applied when column is sorted
		hover      : '', // use custom css here - bootstrap class may not override it
		filterRow  : '', // filter row class
		even       : '', // even row zebra striping
		odd        : ''  // odd row zebra striping
	},
	"jui" : {
		table      : 'ui-widget ui-widget-content ui-corner-all', // table classes
		caption    : 'ui-widget-content ui-corner-all',
		header     : 'ui-widget-header ui-corner-all ui-state-default', // header classes
		footerRow  : '',
		footerCells: '',
		icons      : 'ui-icon', // icon class added to the <i> in the header
		sortNone   : 'ui-icon-carat-2-n-s',
		sortAsc    : 'ui-icon-carat-1-n',
		sortDesc   : 'ui-icon-carat-1-s',
		active     : 'ui-state-active', // applied when column is sorted
		hover      : 'ui-state-hover',  // hover class
		filterRow  : '',
		even       : 'ui-widget-content', // even row zebra striping
		odd        : 'ui-state-default'   // odd row zebra striping
	}
};

$.extend(ts.css, {
	filterRow : 'tablesorter-filter-row',   // filter
	filter    : 'tablesorter-filter',
	wrapper   : 'tablesorter-wrapper',      // ui theme & resizable
	resizer   : 'tablesorter-resizer',      // resizable
	sticky    : 'tablesorter-stickyHeader', // stickyHeader
	stickyVis : 'tablesorter-sticky-visible'
});

// *** Store data in local storage, with a cookie fallback ***
/* IE7 needs JSON library for JSON.stringify - (http://caniuse.com/#search=json)
   if you need it, then include https://github.com/douglascrockford/JSON-js

   $.parseJSON is not available is jQuery versions older than 1.4.1, using older
   versions will only allow storing information for one page at a time

   // *** Save data (JSON format only) ***
   // val must be valid JSON... use http://jsonlint.com/ to ensure it is valid
   var val = { "mywidget" : "data1" }; // valid JSON uses double quotes
   // $.tablesorter.storage(table, key, val);
   $.tablesorter.storage(table, 'tablesorter-mywidget', val);

   // *** Get data: $.tablesorter.storage(table, key); ***
   v = $.tablesorter.storage(table, 'tablesorter-mywidget');
   // val may be empty, so also check for your data
   val = (v && v.hasOwnProperty('mywidget')) ? v.mywidget : '';
   alert(val); // "data1" if saved, or "" if not
*/
ts.storage = function(table, key, value, options) {
	table = $(table)[0];
	var cookieIndex, cookies, date,
		hasLocalStorage = false,
		values = {},
		c = table.config,
		$table = $(table),
		id = options && options.id || $table.attr(options && options.group ||
			'data-table-group') || table.id || $('.tablesorter').index( $table ),
		url = options && options.url || $table.attr(options && options.page ||
			'data-table-page') || c && c.fixedUrl || window.location.pathname;
	// https://gist.github.com/paulirish/5558557
	if ("localStorage" in window) {
		try {
			window.localStorage.setItem('_tmptest', 'temp');
			hasLocalStorage = true;
			window.localStorage.removeItem('_tmptest');
		} catch(error) {}
	}
	// *** get value ***
	if ($.parseJSON) {
		if (hasLocalStorage) {
			values = $.parseJSON(localStorage[key] || '{}');
		} else {
			// old browser, using cookies
			cookies = document.cookie.split(/[;\s|=]/);
			// add one to get from the key to the value
			cookieIndex = $.inArray(key, cookies) + 1;
			values = (cookieIndex !== 0) ? $.parseJSON(cookies[cookieIndex] || '{}') : {};
		}
	}
	// allow value to be an empty string too
	if ((value || value === '') && window.JSON && JSON.hasOwnProperty('stringify')) {
		// add unique identifiers = url pathname > table ID/index on page > data
		if (!values[url]) {
			values[url] = {};
		}
		values[url][id] = value;
		// *** set value ***
		if (hasLocalStorage) {
			localStorage[key] = JSON.stringify(values);
		} else {
			date = new Date();
			date.setTime(date.getTime() + (31536e+6)); // 365 days
			document.cookie = key + '=' + (JSON.stringify(values)).replace(/\"/g,'\"') + '; expires=' + date.toGMTString() + '; path=/';
		}
	} else {
		return values && values[url] ? values[url][id] : '';
	}
};

// Add a resize event to table headers
// **************************
ts.addHeaderResizeEvent = function(table, disable, settings) {
	var headers,
		defaults = {
			timer : 250
		},
		options = $.extend({}, defaults, settings),
		c = table.config,
		wo = c.widgetOptions,
		checkSizes = function(triggerEvent) {
			wo.resize_flag = true;
			headers = [];
			c.$headers.each(function() {
				var $header = $(this),
					sizes = $header.data('savedSizes') || [0,0], // fixes #394
					width = this.offsetWidth,
					height = this.offsetHeight;
				if (width !== sizes[0] || height !== sizes[1]) {
					$header.data('savedSizes', [ width, height ]);
					headers.push(this);
				}
			});
			if (headers.length && triggerEvent !== false) {
				c.$table.trigger('resize', [ headers ]);
			}
			wo.resize_flag = false;
		};
	checkSizes(false);
	clearInterval(wo.resize_timer);
	if (disable) {
		wo.resize_flag = false;
		return false;
	}
	wo.resize_timer = setInterval(function() {
		if (wo.resize_flag) { return; }
		checkSizes();
	}, options.timer);
};

// Widget: General UI theme
// "uitheme" option in "widgetOptions"
// **************************
ts.addWidget({
	id: "uitheme",
	priority: 10,
	format: function(table, c, wo) {
		var i, time, classes, $header, $icon, $tfoot, $h,
			themesAll = ts.themes,
			$table = c.$table,
			$headers = c.$headers,
			theme = c.theme || 'jui',
			themes = themesAll[theme] || themesAll.jui,
			remove = themes.sortNone + ' ' + themes.sortDesc + ' ' + themes.sortAsc;
		if (c.debug) { time = new Date(); }
		// initialization code - run once
		if (!$table.hasClass('tablesorter-' + theme) || c.theme === theme || !table.hasInitialized) {
			// update zebra stripes
			if (themes.even !== '') { wo.zebra[0] += ' ' + themes.even; }
			if (themes.odd !== '') { wo.zebra[1] += ' ' + themes.odd; }
			// add caption style
			$table.find('caption').addClass(themes.caption);
			// add table/footer class names
			$tfoot = $table
				// remove other selected themes
				.removeClass( c.theme === '' ? '' : 'tablesorter-' + c.theme )
				.addClass('tablesorter-' + theme + ' ' + themes.table) // add theme widget class name
				.find('tfoot');
			if ($tfoot.length) {
				$tfoot
					.find('tr').addClass(themes.footerRow)
					.children('th, td').addClass(themes.footerCells);
			}
			// update header classes
			$headers
				.addClass(themes.header)
				.not('.sorter-false')
				.bind('mouseenter.tsuitheme mouseleave.tsuitheme', function(event) {
					// toggleClass with switch added in jQuery 1.3
					$(this)[ event.type === 'mouseenter' ? 'addClass' : 'removeClass' ](themes.hover);
				});
			if (!$headers.find('.' + ts.css.wrapper).length) {
				// Firefox needs this inner div to position the resizer correctly
				$headers.wrapInner('<div class="' + ts.css.wrapper + '" style="position:relative;height:100%;width:100%"></div>');
			}
			if (c.cssIcon) {
				// if c.cssIcon is '', then no <i> is added to the header
				$headers.find('.' + ts.css.icon).addClass(themes.icons);
			}
			if ($table.hasClass('hasFilters')) {
				$headers.find('.' + ts.css.filterRow).addClass(themes.filterRow);
			}
		}
		for (i = 0; i < c.columns; i++) {
			$header = c.$headers.add(c.$extraHeaders).filter('[data-column="' + i + '"]');
			$icon = (ts.css.icon) ? $header.find('.' + ts.css.icon) : $header;
			$h = c.$headers.filter('[data-column="' + i + '"]:last');
			if ($h.length) {
				if ($h[0].sortDisabled) {
					// no sort arrows for disabled columns!
					$header.removeClass(remove);
					$icon.removeClass(remove + ' ' + themes.icons);
				} else {
					classes = ($header.hasClass(ts.css.sortAsc)) ?
						themes.sortAsc :
						($header.hasClass(ts.css.sortDesc)) ? themes.sortDesc :
							$header.hasClass(ts.css.header) ? themes.sortNone : '';
					$header[classes === themes.sortNone ? 'removeClass' : 'addClass'](themes.active);
					$icon.removeClass(remove).addClass(classes);
				}
			}
		}
		if (c.debug) {
			ts.benchmark("Applying " + theme + " theme", time);
		}
	},
	remove: function(table, c, wo) {
		var $table = c.$table,
			theme = c.theme || 'jui',
			themes = ts.themes[ theme ] || ts.themes.jui,
			$headers = $table.children('thead').children(),
			remove = themes.sortNone + ' ' + themes.sortDesc + ' ' + themes.sortAsc;
		$table
			.removeClass('tablesorter-' + theme + ' ' + themes.table)
			.find(ts.css.header).removeClass(themes.header);
		$headers
			.unbind('mouseenter.tsuitheme mouseleave.tsuitheme') // remove hover
			.removeClass(themes.hover + ' ' + remove + ' ' + themes.active)
			.find('.' + ts.css.filterRow)
			.removeClass(themes.filterRow);
		$headers.find('.' + ts.css.icon).removeClass(themes.icons);
	}
});

// Widget: Column styles
// "columns", "columns_thead" (true) and
// "columns_tfoot" (true) options in "widgetOptions"
// **************************
ts.addWidget({
	id: "columns",
	priority: 30,
	options : {
		columns : [ "primary", "secondary", "tertiary" ]
	},
	format: function(table, c, wo) {
		var time, $tbody, tbodyIndex, $rows, rows, $row, $cells, remove, indx,
			$table = c.$table,
			$tbodies = c.$tbodies,
			sortList = c.sortList,
			len = sortList.length,
			// removed c.widgetColumns support
			css = wo && wo.columns || [ "primary", "secondary", "tertiary" ],
			last = css.length - 1;
			remove = css.join(' ');
		if (c.debug) {
			time = new Date();
		}
		// check if there is a sort (on initialization there may not be one)
		for (tbodyIndex = 0; tbodyIndex < $tbodies.length; tbodyIndex++ ) {
			$tbody = ts.processTbody(table, $tbodies.eq(tbodyIndex), true); // detach tbody
			$rows = $tbody.children('tr');
			// loop through the visible rows
			$rows.each(function() {
				$row = $(this);
				if (this.style.display !== 'none') {
					// remove all columns class names
					$cells = $row.children().removeClass(remove);
					// add appropriate column class names
					if (sortList && sortList[0]) {
						// primary sort column class
						$cells.eq(sortList[0][0]).addClass(css[0]);
						if (len > 1) {
							for (indx = 1; indx < len; indx++) {
								// secondary, tertiary, etc sort column classes
								$cells.eq(sortList[indx][0]).addClass( css[indx] || css[last] );
							}
						}
					}
				}
			});
			ts.processTbody(table, $tbody, false);
		}
		// add classes to thead and tfoot
		rows = wo.columns_thead !== false ? ['thead tr'] : [];
		if (wo.columns_tfoot !== false) {
			rows.push('tfoot tr');
		}
		if (rows.length) {
			$rows = $table.find( rows.join(',') ).children().removeClass(remove);
			if (len) {
				for (indx = 0; indx < len; indx++) {
					// add primary. secondary, tertiary, etc sort column classes
					$rows.filter('[data-column="' + sortList[indx][0] + '"]').addClass(css[indx] || css[last]);
				}
			}
		}
		if (c.debug) {
			ts.benchmark("Applying Columns widget", time);
		}
	},
	remove: function(table, c, wo) {
		var tbodyIndex, $tbody,
			$tbodies = c.$tbodies,
			remove = (wo.columns || [ "primary", "secondary", "tertiary" ]).join(' ');
		c.$headers.removeClass(remove);
		c.$table.children('tfoot').children('tr').children('th, td').removeClass(remove);
		for (tbodyIndex = 0; tbodyIndex < $tbodies.length; tbodyIndex++ ) {
			$tbody = ts.processTbody(table, $tbodies.eq(tbodyIndex), true); // remove tbody
			$tbody.children('tr').each(function() {
				$(this).children().removeClass(remove);
			});
			ts.processTbody(table, $tbody, false); // restore tbody
		}
	}
});

// Widget: filter
// **************************
ts.addWidget({
	id: "filter",
	priority: 50,
	options : {
		filter_childRows     : false, // if true, filter includes child row content in the search
		filter_columnFilters : true,  // if true, a filter will be added to the top of each table column
		filter_cssFilter     : '',    // css class name added to the filter row & each input in the row (tablesorter-filter is ALWAYS added)
		filter_external      : '',    // jQuery selector string (or jQuery object) of external filters
		filter_filteredRow   : 'filtered', // class added to filtered rows; needed by pager plugin
		filter_formatter     : null,  // add custom filter elements to the filter row
		filter_functions     : null,  // add custom filter functions using this option
		filter_hideEmpty     : true,  // hide filter row when table is empty
		filter_hideFilters   : false, // collapse filter row when mouse leaves the area
		filter_ignoreCase    : true,  // if true, make all searches case-insensitive
		filter_liveSearch    : true,  // if true, search column content while the user types (with a delay)
		filter_onlyAvail     : 'filter-onlyAvail', // a header with a select dropdown & this class name will only show available (visible) options within the drop down
		filter_placeholder   : { search : '', select : '' }, // default placeholder text (overridden by any header "data-placeholder" setting)
		filter_reset         : null,  // jQuery selector string of an element used to reset the filters
		filter_saveFilters   : false, // Use the $.tablesorter.storage utility to save the most recent filters
		filter_searchDelay   : 300,   // typing delay in milliseconds before starting a search
		filter_searchFiltered: true,  // allow searching through already filtered rows in special circumstances; will speed up searching in large tables if true
		filter_selectSource  : null,  // include a function to return an array of values to be added to the column filter select
		filter_startsWith    : false, // if true, filter start from the beginning of the cell contents
		filter_useParsedData : false, // filter all data using parsed content
		filter_serversideFiltering : false, // if true, server-side filtering should be performed because client-side filtering will be disabled, but the ui and events will still be used.
		filter_defaultAttrib : 'data-value' // data attribute in the header cell that contains the default filter value
	},
	format: function(table, c, wo) {
		if (!c.$table.hasClass('hasFilters')) {
			ts.filter.init(table, c, wo);
		}
	},
	remove: function(table, c, wo) {
		var tbodyIndex, $tbody,
			$table = c.$table,
			$tbodies = c.$tbodies;
		$table
			.removeClass('hasFilters')
			// add .tsfilter namespace to all BUT search
			.unbind('addRows updateCell update updateRows updateComplete appendCache filterReset filterEnd search '.split(' ').join(c.namespace + 'filter '))
			.find('.' + ts.css.filterRow).remove();
		for (tbodyIndex = 0; tbodyIndex < $tbodies.length; tbodyIndex++ ) {
			$tbody = ts.processTbody(table, $tbodies.eq(tbodyIndex), true); // remove tbody
			$tbody.children().removeClass(wo.filter_filteredRow).show();
			ts.processTbody(table, $tbody, false); // restore tbody
		}
		if (wo.filter_reset) {
			$(document).undelegate(wo.filter_reset, 'click.tsfilter');
		}
	}
});

ts.filter = {

	// regex used in filter "check" functions - not for general use and not documented
	regex: {
		regex     : /^\/((?:\\\/|[^\/])+)\/([mig]{0,3})?$/, // regex to test for regex
		child     : /tablesorter-childRow/, // child row class name; this gets updated in the script
		filtered  : /filtered/, // filtered (hidden) row class name; updated in the script
		type      : /undefined|number/, // check type
		exact     : /(^[\"|\'|=]+)|([\"|\'|=]+$)/g, // exact match (allow '==')
		nondigit  : /[^\w,. \-()]/g, // replace non-digits (from digit & currency parser)
		operators : /[<>=]/g // replace operators
	},
		// function( filter, iFilter, exact, iExact, cached, index, table, wo, parsed )
		// filter = array of filter input values; iFilter = same array, except lowercase
		// exact = table cell text (or parsed data if column parser enabled)
		// iExact = same as exact, except lowercase
		// cached = table cell text from cache, so it has been parsed
		// index = column index; table = table element (DOM)
		// wo = widget options (table.config.widgetOptions)
		// parsed = array (by column) of boolean values (from filter_useParsedData or "filter-parsed" class)
	types: {
		// Look for regex
		regex: function( filter, iFilter, exact, iExact ) {
			if ( ts.filter.regex.regex.test(iFilter) ) {
				var matches,
					regex = ts.filter.regex.regex.exec(iFilter);
				try {
					matches = new RegExp(regex[1], regex[2]).test( iExact );
				} catch (error) {
					matches = false;
				}
				return matches;
			}
			return null;
		},
		// Look for operators >, >=, < or <=
		operators: function( filter, iFilter, exact, iExact, cached, index, table, wo, parsed ) {
			if ( /^[<>]=?/.test(iFilter) ) {
				var cachedValue, result,
					c = table.config,
					query = ts.formatFloat( iFilter.replace(ts.filter.regex.operators, ''), table ),
					parser = c.parsers[index],
					savedSearch = query;
				// parse filter value in case we're comparing numbers (dates)
				if (parsed[index] || parser.type === 'numeric') {
					result = ts.filter.parseFilter(table, $.trim('' + iFilter.replace(ts.filter.regex.operators, '')), index, parsed[index], true);
					query = ( typeof result === "number" && result !== '' && !isNaN(result) ) ? result : query;
				}

				// iExact may be numeric - see issue #149;
				// check if cached is defined, because sometimes j goes out of range? (numeric columns)
				cachedValue = ( parsed[index] || parser.type === 'numeric' ) && !isNaN(query) && typeof cached !== 'undefined' ? cached :
					isNaN(iExact) ? ts.formatFloat( iExact.replace(ts.filter.regex.nondigit, ''), table) :
					ts.formatFloat( iExact, table );

				if ( />/.test(iFilter) ) { result = />=/.test(iFilter) ? cachedValue >= query : cachedValue > query; }
				if ( /</.test(iFilter) ) { result = /<=/.test(iFilter) ? cachedValue <= query : cachedValue < query; }
				// keep showing all rows if nothing follows the operator
				if ( !result && savedSearch === '' ) { result = true; }
				return result;
			}
			return null;
		},
		// Look for a not match
		notMatch: function( filter, iFilter, exact, iExact, cached, index, table, wo, parsed ) {
			if ( /^\!/.test(iFilter) ) {
				iFilter = ts.filter.parseFilter(table, iFilter.replace('!', ''), index, parsed[index]);
				if (ts.filter.regex.exact.test(iFilter)) {
					// look for exact not matches - see #628
					iFilter = iFilter.replace(ts.filter.regex.exact, '');
					return iFilter === '' ? true : $.trim(iFilter) !== iExact;
				} else {
					var indx = iExact.search( $.trim(iFilter) );
					return iFilter === '' ? true : !(wo.filter_startsWith ? indx === 0 : indx >= 0);
				}
			}
			return null;
		},
		// Look for quotes or equals to get an exact match; ignore type since iExact could be numeric
		exact: function( filter, iFilter, exact, iExact, cached, index, table, wo, parsed, rowArray ) {
			/*jshint eqeqeq:false */
			if (ts.filter.regex.exact.test(iFilter)) {
				var fltr = ts.filter.parseFilter(table, iFilter.replace(ts.filter.regex.exact, ''), index, parsed[index]);
				return rowArray ? $.inArray(fltr, rowArray) >= 0 : fltr == iExact;
			}
			return null;
		},
		// Look for an AND or && operator (logical and)
		and : function( filter, iFilter, exact, iExact, cached, index, table, wo, parsed ) {
			if ( ts.filter.regex.andTest.test(filter) ) {
				var query = iFilter.split( ts.filter.regex.andSplit ),
					result = iExact.search( $.trim(ts.filter.parseFilter(table, query[0], index, parsed[index])) ) >= 0,
					indx = query.length - 1;
				while (result && indx) {
					result = result && iExact.search( $.trim(ts.filter.parseFilter(table, query[indx], index, parsed[index])) ) >= 0;
					indx--;
				}
				return result;
			}
			return null;
		},
		// Look for a range (using " to " or " - ") - see issue #166; thanks matzhu!
		range : function( filter, iFilter, exact, iExact, cached, index, table, wo, parsed ) {
			if ( ts.filter.regex.toTest.test(iFilter) ) {
				var result, tmp,
					c = table.config,
					// make sure the dash is for a range and not indicating a negative number
					query = iFilter.split( ts.filter.regex.toSplit ),
					range1 = ts.formatFloat( ts.filter.parseFilter(table, query[0].replace(ts.filter.regex.nondigit, ''), index, parsed[index]), table ),
					range2 = ts.formatFloat( ts.filter.parseFilter(table, query[1].replace(ts.filter.regex.nondigit, ''), index, parsed[index]), table );
					// parse filter value in case we're comparing numbers (dates)
				if (parsed[index] || c.parsers[index].type === 'numeric') {
					result = c.parsers[index].format('' + query[0], table, c.$headers.eq(index), index);
					range1 = (result !== '' && !isNaN(result)) ? result : range1;
					result = c.parsers[index].format('' + query[1], table, c.$headers.eq(index), index);
					range2 = (result !== '' && !isNaN(result)) ? result : range2;
				}
				result = ( parsed[index] || c.parsers[index].type === 'numeric' ) && !isNaN(range1) && !isNaN(range2) ? cached :
					isNaN(iExact) ? ts.formatFloat( iExact.replace(ts.filter.regex.nondigit, ''), table) :
					ts.formatFloat( iExact, table );
				if (range1 > range2) { tmp = range1; range1 = range2; range2 = tmp; } // swap
				return (result >= range1 && result <= range2) || (range1 === '' || range2 === '');
			}
			return null;
		},
		// Look for wild card: ? = single, * = multiple, or | = logical OR
		wild : function( filter, iFilter, exact, iExact, cached, index, table, wo, parsed, rowArray ) {
			if ( /[\?|\*]/.test(iFilter) || ts.filter.regex.orReplace.test(filter) ) {
				var c = table.config,
					query = ts.filter.parseFilter(table, iFilter.replace(ts.filter.regex.orReplace, "|"), index, parsed[index]);
				// look for an exact match with the "or" unless the "filter-match" class is found
				if (!c.$headers.filter('[data-column="' + index + '"]:last').hasClass('filter-match') && /\|/.test(query)) {
					query = $.isArray(rowArray) ? '(' + query + ')' : '^(' + query + ')$';
				}
				// parsing the filter may not work properly when using wildcards =/
				return new RegExp( query.replace(/\?/g, '\\S{1}').replace(/\*/g, '\\S*') ).test(iExact);
			}
			return null;
		},
		// fuzzy text search; modified from https://github.com/mattyork/fuzzy (MIT license)
		fuzzy: function( filter, iFilter, exact, iExact, cached, index, table, wo, parsed ) {
			if ( /^~/.test(iFilter) ) {
				var indx,
					patternIndx = 0,
					len = iExact.length,
					pattern = ts.filter.parseFilter(table, iFilter.slice(1), index, parsed[index]);
				for (indx = 0; indx < len; indx++) {
					if (iExact[indx] === pattern[patternIndx]) {
						patternIndx += 1;
					}
				}
				if (patternIndx === pattern.length) {
					return true;
				}
				return false;
			}
			return null;
		}
	},
	init: function(table, c, wo) {
		// filter language options
		ts.language = $.extend(true, {}, {
			to  : 'to',
			or  : 'or',
			and : 'and'
		}, ts.language);

		var options, string, $header, column, filters, time, fxn,
			regex = ts.filter.regex;
		if (c.debug) {
			time = new Date();
		}
		c.$table.addClass('hasFilters');

		// define timers so using clearTimeout won't cause an undefined error
		wo.searchTimer = null;
		wo.filter_initTimer = null;
		wo.filter_formatterCount = 0;
		wo.filter_formatterInit = [];

		$.extend( regex, {
			child : new RegExp(c.cssChildRow),
			filtered : new RegExp(wo.filter_filteredRow),
			alreadyFiltered : new RegExp('(\\s+(' + ts.language.or + '|-|' + ts.language.to + ')\\s+)', 'i'),
			toTest : new RegExp('\\s+(-|' + ts.language.to + ')\\s+', 'i'),
			toSplit : new RegExp('(?:\\s+(?:-|' + ts.language.to + ')\\s+)' ,'gi'),
			andTest : new RegExp('\\s+(' + ts.language.and + '|&&)\\s+', 'i'),
			andSplit : new RegExp('(?:\\s+(?:' + ts.language.and + '|&&)\\s+)', 'gi'),
			orReplace : new RegExp('\\s+(' + ts.language.or + ')\\s+', 'gi')
		});

		// don't build filter row if columnFilters is false or all columns are set to "filter-false" - issue #156
		if (wo.filter_columnFilters !== false && c.$headers.filter('.filter-false').length !== c.$headers.length) {
			// build filter row
			ts.filter.buildRow(table, c, wo);
		}

		c.$table.bind('addRows updateCell update updateRows updateComplete appendCache filterReset filterEnd search '.split(' ').join(c.namespace + 'filter '), function(event, filter) {
			c.$table.find('.' + ts.css.filterRow).toggle( !(wo.filter_hideEmpty && $.isEmptyObject(c.cache) && !(c.delayInit && event.type === 'appendCache')) ); // fixes #450
			if ( !/(search|filter)/.test(event.type) ) {
				event.stopPropagation();
				ts.filter.buildDefault(table, true);
			}
			if (event.type === 'filterReset') {
				c.$table.find('.' + ts.css.filter).add(wo.filter_$externalFilters).val('');
				ts.filter.searching(table, []);
			} else if (event.type === 'filterEnd') {
				ts.filter.buildDefault(table, true);
			} else {
				// send false argument to force a new search; otherwise if the filter hasn't changed, it will return
				filter = event.type === 'search' ? filter : event.type === 'updateComplete' ? c.$table.data('lastSearch') : '';
				if (/(update|add)/.test(event.type) && event.type !== "updateComplete") {
					// force a new search since content has changed
					c.lastCombinedFilter = null;
					c.lastSearch = [];
				}
				// pass true (skipFirst) to prevent the tablesorter.setFilters function from skipping the first input
				// ensures all inputs are updated when a search is triggered on the table $('table').trigger('search', [...]);
				ts.filter.searching(table, filter, true);
			}
			return false;
		});

		// reset button/link
		if (wo.filter_reset) {
			if (wo.filter_reset instanceof $) {
				// reset contains a jQuery object, bind to it
				wo.filter_reset.click(function(){
					c.$table.trigger('filterReset');
				});
			} else if ($(wo.filter_reset).length) {
				// reset is a jQuery selector, use event delegation
				$(document)
				.undelegate(wo.filter_reset, 'click.tsfilter')
				.delegate(wo.filter_reset, 'click.tsfilter', function() {
					// trigger a reset event, so other functions (filterFormatter) know when to reset
					c.$table.trigger('filterReset');
				});
			}
		}
		if (wo.filter_functions) {
			for (column = 0; column < c.columns; column++) {
				fxn = ts.getColumnData( table, wo.filter_functions, column );
				if (fxn) {
					$header = c.$headers.filter('[data-column="' + column + '"]:last');
					options = '';
					if (fxn === true && !$header.hasClass('filter-false')) {
						ts.filter.buildSelect(table, column);
					} else if (typeof fxn === 'object' && !$header.hasClass('filter-false')) {
						// add custom drop down list
						for (string in fxn) {
							if (typeof string === 'string') {
								options += options === '' ?
									'<option value="">' + ($header.data('placeholder') || $header.attr('data-placeholder') || wo.filter_placeholder.select || '') + '</option>' : '';
								options += '<option value="' + string + '">' + string + '</option>';
							}
						}
						c.$table.find('thead').find('select.' + ts.css.filter + '[data-column="' + column + '"]').append(options);
					}
				}
			}
		}
		// not really updating, but if the column has both the "filter-select" class & filter_functions set to true,
		// it would append the same options twice.
		ts.filter.buildDefault(table, true);

		ts.filter.bindSearch( table, c.$table.find('.' + ts.css.filter), true );
		if (wo.filter_external) {
			ts.filter.bindSearch( table, wo.filter_external );
		}

		if (wo.filter_hideFilters) {
			ts.filter.hideFilters(table, c);
		}

		// show processing icon
		if (c.showProcessing) {
			c.$table.bind('filterStart' + c.namespace + 'filter filterEnd' + c.namespace + 'filter', function(event, columns) {
				// only add processing to certain columns to all columns
				$header = (columns) ? c.$table.find('.' + ts.css.header).filter('[data-column]').filter(function() {
					return columns[$(this).data('column')] !== '';
				}) : '';
				ts.isProcessing(table, event.type === 'filterStart', columns ? $header : '');
			});
		}

		// set filtered rows count (intially unfiltered)
		c.filteredRows = c.totalRows;

		if (c.debug) {
			ts.benchmark("Applying Filter widget", time);
		}
		// add default values
		c.$table.bind('tablesorter-initialized pagerInitialized', function(e) {
			// redefine "wo" as it does not update properly inside this callback
			var wo = this.config.widgetOptions;
			filters = ts.filter.setDefaults(table, c, wo) || [];
			if (filters.length) {
				// prevent delayInit from triggering a cache build if filters are empty
				if ( !(c.delayInit && filters.join('') === '') ) {
					ts.setFilters(table, filters, true);
				}
			}
			c.$table.trigger('filterFomatterUpdate');
			// trigger init after setTimeout to prevent multiple filterStart/End/Init triggers
			setTimeout(function(){
				if (!wo.filter_initialized) {
					ts.filter.filterInitComplete(c);
				}
			}, 100);
		});
		// if filter widget is added after pager has initialized; then set filter init flag
		if (c.pager && c.pager.initialized && !wo.filter_initialized) {
			c.$table.trigger('filterFomatterUpdate');
			setTimeout(function(){
				ts.filter.filterInitComplete(c);
			}, 100);
		}
	},
	// $cell parameter, but not the config, is passed to the
	// filter_formatters, so we have to work with it instead
	formatterUpdated: function($cell, column) {
		var wo = $cell.closest('table')[0].config.widgetOptions;
		if (!wo.filter_initialized) {
			// add updates by column since this function
			// may be called numerous times before initialization
			wo.filter_formatterInit[column] = 1;
		}
	},
	filterInitComplete: function(c){
		var wo = c.widgetOptions,
			count = 0;
		$.each( wo.filter_formatterInit, function(i, val) {
			if (val === 1) {
				count++;
			}
		});
		clearTimeout(wo.filter_initTimer);
		if (!wo.filter_initialized && count === wo.filter_formatterCount) {
			// filter widget initialized
			wo.filter_initialized = true;
			c.$table.trigger('filterInit', c);
		} else if (!wo.filter_initialized) {
			// fall back in case a filter_formatter doesn't call
			// $.tablesorter.filter.formatterUpdated($cell, column), and the count is off
			wo.filter_initTimer = setTimeout(function(){
				wo.filter_initialized = true;
				c.$table.trigger('filterInit', c);
			}, 500);
		}
	},
	setDefaults: function(table, c, wo) {
		var isArray, saved, indx,
			// get current (default) filters
			filters = ts.getFilters(table) || [];
		if (wo.filter_saveFilters && ts.storage) {
			saved = ts.storage( table, 'tablesorter-filters' ) || [];
			isArray = $.isArray(saved);
			// make sure we're not just getting an empty array
			if ( !(isArray && saved.join('') === '' || !isArray) ) { filters = saved; }
		}
		// if no filters saved, then check default settings
		if (filters.join('') === '') {
			for (indx = 0; indx < c.columns; indx++) {
				filters[indx] = c.$headers.filter('[data-column="' + indx + '"]:last').attr(wo.filter_defaultAttrib) || filters[indx];
			}
		}
		c.$table.data('lastSearch', filters);
		return filters;
	},
	parseFilter: function(table, filter, column, parsed, forceParse){
		var c = table.config;
		return forceParse || parsed ?
			c.parsers[column].format( filter, table, [], column ) :
			filter;
	},
	buildRow: function(table, c, wo) {
		var column, $header, buildSelect, disabled, name, ffxn,
			// c.columns defined in computeThIndexes()
			columns = c.columns,
			buildFilter = '<tr class="' + ts.css.filterRow + '">';
		for (column = 0; column < columns; column++) {
			buildFilter += '<td></td>';
		}
		c.$filters = $(buildFilter += '</tr>').appendTo( c.$table.children('thead').eq(0) ).find('td');
		// build each filter input
		for (column = 0; column < columns; column++) {
			disabled = false;
			// assuming last cell of a column is the main column
			$header = c.$headers.filter('[data-column="' + column + '"]:last');
			ffxn = ts.getColumnData( table, wo.filter_functions, column );
			buildSelect = (wo.filter_functions && ffxn && typeof ffxn !== "function" ) ||
				$header.hasClass('filter-select');
			// get data from jQuery data, metadata, headers option or header class name
			disabled = ts.getData($header[0], ts.getColumnData( table, c.headers, column ), 'filter') === 'false';

			if (buildSelect) {
				buildFilter = $('<select>').appendTo( c.$filters.eq(column) );
			} else {
				ffxn = ts.getColumnData( table, wo.filter_formatter, column );
				if (ffxn) {
					wo.filter_formatterCount++;
					buildFilter = ffxn( c.$filters.eq(column), column );
					// no element returned, so lets go find it
					if (buildFilter && buildFilter.length === 0) {
						buildFilter = c.$filters.eq(column).children('input');
					}
					// element not in DOM, so lets attach it
					if ( buildFilter && (buildFilter.parent().length === 0 ||
						(buildFilter.parent().length && buildFilter.parent()[0] !== c.$filters[column])) ) {
						c.$filters.eq(column).append(buildFilter);
					}
				} else {
					buildFilter = $('<input type="search">').appendTo( c.$filters.eq(column) );
				}
				if (buildFilter) {
					buildFilter.attr('placeholder', $header.data('placeholder') || $header.attr('data-placeholder') || wo.filter_placeholder.search || '');
				}
			}
			if (buildFilter) {
				// add filter class name
				name = ( $.isArray(wo.filter_cssFilter) ?
					(typeof wo.filter_cssFilter[column] !== 'undefined' ? wo.filter_cssFilter[column] || '' : '') :
					wo.filter_cssFilter ) || '';
				buildFilter.addClass( ts.css.filter + ' ' + name ).attr('data-column', column);
				if (disabled) {
					buildFilter.attr('placeholder', '').addClass('disabled')[0].disabled = true; // disabled!
				}
			}
		}
	},
	bindSearch: function(table, $el, internal) {
		table = $(table)[0];
		$el = $($el); // allow passing a selector string
		if (!$el.length) { return; }
		var c = table.config,
			wo = c.widgetOptions,
			$ext = wo.filter_$externalFilters;
		if (internal !== true) {
			// save anyMatch element
			wo.filter_$anyMatch = $el.filter('[data-column="all"]');
			if ($ext && $ext.length) {
				wo.filter_$externalFilters = wo.filter_$externalFilters.add( $el );
			} else {
				wo.filter_$externalFilters = $el;
			}
			// update values (external filters added after table initialization)
			ts.setFilters(table, c.$table.data('lastSearch') || [], internal === false);
		}
		$el
		// use data attribute instead of jQuery data since the head is cloned without including the data/binding
		.attr('data-lastSearchTime', new Date().getTime())
		.unbind('keypress keyup search change '.split(' ').join(c.namespace + 'filter '))
		// include change for select - fixes #473
		.bind('keyup' + c.namespace + 'filter', function(event) {
			$(this).attr('data-lastSearchTime', new Date().getTime());
			// emulate what webkit does.... escape clears the filter
			if (event.which === 27) {
				this.value = '';
			// live search
			} else if ( wo.filter_liveSearch === false ) {
				return;
				// don't return if the search value is empty (all rows need to be revealed)
			} else if ( this.value !== '' && (
				// liveSearch can contain a min value length; ignore arrow and meta keys, but allow backspace
				( typeof wo.filter_liveSearch === 'number' && this.value.length < wo.filter_liveSearch ) ||
				// let return & backspace continue on, but ignore arrows & non-valid characters
				( event.which !== 13 && event.which !== 8 && ( event.which < 32 || (event.which >= 37 && event.which <= 40) ) ) ) ) {
				return;
			}
			// change event = no delay; last true flag tells getFilters to skip newest timed input
			ts.filter.searching( table, true, true );
		})
		.bind('search change keypress '.split(' ').join(c.namespace + 'filter '), function(event){
			var column = $(this).data('column');
			// don't allow "change" event to process if the input value is the same - fixes #685
			if (event.which === 13 || event.type === 'search' || event.type === 'change' && this.value !== c.lastSearch[column]) {
				event.preventDefault();
				// init search with no delay
				$(this).attr('data-lastSearchTime', new Date().getTime());
				ts.filter.searching( table, false, true );
			}
		});
	},
	searching: function(table, filter, skipFirst) {
		var wo = table.config.widgetOptions;
		clearTimeout(wo.searchTimer);
		if (typeof filter === 'undefined' || filter === true) {
			// delay filtering
			wo.searchTimer = setTimeout(function() {
				ts.filter.checkFilters(table, filter, skipFirst );
			}, wo.filter_liveSearch ? wo.filter_searchDelay : 10);
		} else {
			// skip delay
			ts.filter.checkFilters(table, filter, skipFirst);
		}
	},
	checkFilters: function(table, filter, skipFirst) {
		var c = table.config,
			wo = c.widgetOptions,
			filterArray = $.isArray(filter),
			filters = (filterArray) ? filter : ts.getFilters(table, true),
			combinedFilters = (filters || []).join(''); // combined filter values
		// prevent errors if delay init is set
		if ($.isEmptyObject(c.cache)) {
			// update cache if delayInit set & pager has initialized (after user initiates a search)
			if (c.delayInit && c.pager && c.pager.initialized) {
				c.$table.trigger('updateCache', [function(){
					ts.filter.checkFilters(table, false, skipFirst);
				}] );
			}
			return;
		}
		// add filter array back into inputs
		if (filterArray) {
			ts.setFilters( table, filters, false, skipFirst !== true );
			if (!wo.filter_initialized) { c.lastCombinedFilter = ''; }
		}
		if (wo.filter_hideFilters) {
			// show/hide filter row as needed
			c.$table.find('.' + ts.css.filterRow).trigger( combinedFilters === '' ? 'mouseleave' : 'mouseenter' );
		}
		// return if the last search is the same; but filter === false when updating the search
		// see example-widget-filter.html filter toggle buttons
		if (c.lastCombinedFilter === combinedFilters && filter !== false) {
			return;
		} else if (filter === false) {
			// force filter refresh
			c.lastCombinedFilter = null;
			c.lastSearch = [];
		}
		if (wo.filter_initialized) { c.$table.trigger('filterStart', [filters]); }
		if (c.showProcessing) {
			// give it time for the processing icon to kick in
			setTimeout(function() {
				ts.filter.findRows(table, filters, combinedFilters);
				return false;
			}, 30);
		} else {
			ts.filter.findRows(table, filters, combinedFilters);
			return false;
		}
	},
	hideFilters: function(table, c) {
		var $filterRow, $filterRow2, timer;
		$(table)
			.find('.' + ts.css.filterRow)
			.addClass('hideme')
			.bind('mouseenter mouseleave', function(e) {
				// save event object - http://bugs.jquery.com/ticket/12140
				var event = e;
				$filterRow = $(this);
				clearTimeout(timer);
				timer = setTimeout(function() {
					if ( /enter|over/.test(event.type) ) {
						$filterRow.removeClass('hideme');
					} else {
						// don't hide if input has focus
						// $(':focus') needs jQuery 1.6+
						if ( $(document.activeElement).closest('tr')[0] !== $filterRow[0] ) {
							// don't hide row if any filter has a value
							if (c.lastCombinedFilter === '') {
								$filterRow.addClass('hideme');
							}
						}
					}
				}, 200);
			})
			.find('input, select').bind('focus blur', function(e) {
				$filterRow2 = $(this).closest('tr');
				clearTimeout(timer);
				var event = e;
				timer = setTimeout(function() {
					// don't hide row if any filter has a value
					if (ts.getFilters(c.$table).join('') === '') {
						$filterRow2[ event.type === 'focus' ? 'removeClass' : 'addClass']('hideme');
					}
				}, 200);
			});
	},
	findRows: function(table, filters, combinedFilters) {
		if (table.config.lastCombinedFilter === combinedFilters) { return; }
		var cached, len, $rows, rowIndex, tbodyIndex, $tbody, $cells, columnIndex,
			childRow, childRowText, exact, iExact, iFilter, lastSearch, matches, result,
			notFiltered, searchFiltered, filterMatched, showRow, time, val, indx,
			anyMatch, iAnyMatch, rowArray, rowText, iRowText, rowCache, fxn,
			regex = ts.filter.regex,
			c = table.config,
			wo = c.widgetOptions,
			columns = c.columns,
			$tbodies = c.$table.children('tbody'), // target all tbodies #568
			// anyMatch really screws up with these types of filters
			anyMatchNotAllowedTypes = [ 'range', 'notMatch',  'operators' ],
			// parse columns after formatter, in case the class is added at that point
			parsed = c.$headers.map(function(columnIndex) {
				return c.parsers && c.parsers[columnIndex] && c.parsers[columnIndex].parsed ||
					// getData won't return "parsed" if other "filter-" class names exist (e.g. <th class="filter-select filter-parsed">)
					ts.getData && ts.getData(c.$headers.filter('[data-column="' + columnIndex + '"]:last'), ts.getColumnData( table, c.headers, columnIndex ), 'filter') === 'parsed' ||
					$(this).hasClass('filter-parsed');
			}).get();
		if (c.debug) { time = new Date(); }
		// filtered rows count
		c.filteredRows = 0;
		c.totalRows = 0;
		for (tbodyIndex = 0; tbodyIndex < $tbodies.length; tbodyIndex++ ) {
			if ($tbodies.eq(tbodyIndex).hasClass(c.cssInfoBlock || ts.css.info)) { continue; } // ignore info blocks, issue #264
			$tbody = ts.processTbody(table, $tbodies.eq(tbodyIndex), true);
			// skip child rows & widget added (removable) rows - fixes #448 thanks to @hempel!
			// $rows = $tbody.children('tr').not(c.selectorRemove);
			columnIndex = c.columns;
			// convert stored rows into a jQuery object
			$rows = $( $.map(c.cache[tbodyIndex].normalized, function(el){ return el[columnIndex].$row.get(); }) );

			if (combinedFilters === '' || wo.filter_serversideFiltering) {
				$rows.removeClass(wo.filter_filteredRow).not('.' + c.cssChildRow).show();
			} else {
				// filter out child rows
				$rows = $rows.not('.' + c.cssChildRow);
				len = $rows.length;
				// optimize searching only through already filtered rows - see #313
				searchFiltered = wo.filter_searchFiltered;
				lastSearch = c.lastSearch || c.$table.data('lastSearch') || [];
				if (searchFiltered) {
					// cycle through all filters; include last (columnIndex + 1 = match any column). Fixes #669
					for (indx = 0; indx < columnIndex + 1; indx++) {
						val = filters[indx] || '';
						// break out of loop if we've already determined not to search filtered rows
						if (!searchFiltered) { indx = columnIndex; }
						// search already filtered rows if...
						searchFiltered = searchFiltered && lastSearch.length &&
							// there are no changes from beginning of filter
							val.indexOf(lastSearch[indx] || '') === 0 &&
							// if there is NOT a logical "or", or range ("to" or "-") in the string
							!regex.alreadyFiltered.test(val) &&
							// if we are not doing exact matches, using "|" (logical or) or not "!"
							!/[=\"\|!]/.test(val) &&
							// don't search only filtered if the value is negative ('> -10' => '> -100' will ignore hidden rows)
							!(/(>=?\s*-\d)/.test(val) || /(<=?\s*\d)/.test(val)) && 
							// if filtering using a select without a "filter-match" class (exact match) - fixes #593
							!( val !== '' && c.$filters && c.$filters.eq(indx).find('select').length && !c.$headers.filter('[data-column="' + indx + '"]:last').hasClass('filter-match') );
					}
				}
				notFiltered = $rows.not('.' + wo.filter_filteredRow).length;
				// can't search when all rows are hidden - this happens when looking for exact matches
				if (searchFiltered && notFiltered === 0) { searchFiltered = false; }
				if (c.debug) {
					ts.log( "Searching through " + ( searchFiltered && notFiltered < len ? notFiltered : "all" ) + " rows" );
				}
				if ((wo.filter_$anyMatch && wo.filter_$anyMatch.length) || filters[c.columns]) {
					anyMatch = wo.filter_$anyMatch && wo.filter_$anyMatch.val() || filters[c.columns] || '';
					if (c.sortLocaleCompare) {
						// replace accents
						anyMatch = ts.replaceAccents(anyMatch);
					}
					iAnyMatch = anyMatch.toLowerCase();
				}
				// loop through the rows
				for (rowIndex = 0; rowIndex < len; rowIndex++) {
					childRow = $rows[rowIndex].className;
					// skip child rows & already filtered rows
					if ( regex.child.test(childRow) || (searchFiltered && regex.filtered.test(childRow)) ) { continue; }
					showRow = true;
					// *** nextAll/nextUntil not supported by Zepto! ***
					childRow = $rows.eq(rowIndex).nextUntil('tr:not(.' + c.cssChildRow + ')');
					// so, if "table.config.widgetOptions.filter_childRows" is true and there is
					// a match anywhere in the child row, then it will make the row visible
					// checked here so the option can be changed dynamically
					childRowText = (childRow.length && wo.filter_childRows) ? childRow.text() : '';
					childRowText = wo.filter_ignoreCase ? childRowText.toLocaleLowerCase() : childRowText;
					$cells = $rows.eq(rowIndex).children();

					if (anyMatch) {
						rowArray = $cells.map(function(i){
							var txt;
							if (parsed[i]) {
								txt = c.cache[tbodyIndex].normalized[rowIndex][i];
							} else {
								txt = wo.filter_ignoreCase ? $(this).text().toLowerCase() : $(this).text();
								if (c.sortLocaleCompare) {
									txt = ts.replaceAccents(txt);
								}
							}
							return txt;
						}).get();
						rowText = rowArray.join(' ');
						iRowText = rowText.toLowerCase();
						rowCache = c.cache[tbodyIndex].normalized[rowIndex].slice(0,-1).join(' ');
						filterMatched = null;
						$.each(ts.filter.types, function(type, typeFunction) {
							if ($.inArray(type, anyMatchNotAllowedTypes) < 0) {
								matches = typeFunction( anyMatch, iAnyMatch, rowText, iRowText, rowCache, columns, table, wo, parsed, rowArray );
								if (matches !== null) {
									filterMatched = matches;
									return false;
								}
							}
						});
						if (filterMatched !== null) {
							showRow = filterMatched;
						} else {
							showRow = (iRowText + childRowText).indexOf(iAnyMatch) >= 0;
						}
					}

					for (columnIndex = 0; columnIndex < columns; columnIndex++) {
						// ignore if filter is empty or disabled
						if (filters[columnIndex]) {
							cached = c.cache[tbodyIndex].normalized[rowIndex][columnIndex];
							// check if column data should be from the cell or from parsed data
							if (wo.filter_useParsedData || parsed[columnIndex]) {
								exact = cached;
							} else {
							// using older or original tablesorter
								exact = $.trim($cells.eq(columnIndex).text());
								exact = c.sortLocaleCompare ? ts.replaceAccents(exact) : exact; // issue #405
							}
							iExact = !regex.type.test(typeof exact) && wo.filter_ignoreCase ? exact.toLocaleLowerCase() : exact;
							result = showRow; // if showRow is true, show that row

							// replace accents - see #357
							filters[columnIndex] = c.sortLocaleCompare ? ts.replaceAccents(filters[columnIndex]) : filters[columnIndex];
							// val = case insensitive, filters[columnIndex] = case sensitive
							iFilter = wo.filter_ignoreCase ? (filters[columnIndex] || '').toLocaleLowerCase() : filters[columnIndex];
							fxn = ts.getColumnData( table, wo.filter_functions, columnIndex );
							if (fxn) {
								if (fxn === true) {
									// default selector; no "filter-select" class
									result = (c.$headers.filter('[data-column="' + columnIndex + '"]:last').hasClass('filter-match')) ?
										iExact.search(iFilter) >= 0 : filters[columnIndex] === exact;
								} else if (typeof fxn === 'function') {
									// filter callback( exact cell content, parser normalized content, filter input value, column index, jQuery row object )
									result = fxn(exact, cached, filters[columnIndex], columnIndex, $rows.eq(rowIndex));
								} else if (typeof fxn[filters[columnIndex]] === 'function') {
									// selector option function
									result = fxn[filters[columnIndex]](exact, cached, filters[columnIndex], columnIndex, $rows.eq(rowIndex));
								}
							} else {
								filterMatched = null;
								// cycle through the different filters
								// filters return a boolean or null if nothing matches
								$.each(ts.filter.types, function(type, typeFunction) {
									matches = typeFunction( filters[columnIndex], iFilter, exact, iExact, cached, columnIndex, table, wo, parsed );
									if (matches !== null) {
										filterMatched = matches;
										return false;
									}
								});
								if (filterMatched !== null) {
									result = filterMatched;
								// Look for match, and add child row data for matching
								} else {
									exact = (iExact + childRowText).indexOf( ts.filter.parseFilter(table, iFilter, columnIndex, parsed[columnIndex]) );
									result = ( (!wo.filter_startsWith && exact >= 0) || (wo.filter_startsWith && exact === 0) );
								}
							}
							showRow = (result) ? showRow : false;
						}
					}
					$rows.eq(rowIndex)
						.toggle(showRow)
						.toggleClass(wo.filter_filteredRow, !showRow);
					if (childRow.length) {
						childRow.toggleClass(wo.filter_filteredRow, !showRow);
					}
				}
			}
			c.filteredRows += $rows.not('.' + wo.filter_filteredRow).length;
			c.totalRows += $rows.length;
			ts.processTbody(table, $tbody, false);
		}
		c.lastCombinedFilter = combinedFilters; // save last search
		c.lastSearch = filters;
		c.$table.data('lastSearch', filters);
		if (wo.filter_saveFilters && ts.storage) {
			ts.storage( table, 'tablesorter-filters', filters );
		}
		if (c.debug) {
			ts.benchmark("Completed filter widget search", time);
		}
		if (wo.filter_initialized) { c.$table.trigger('filterEnd', c ); }
		setTimeout(function(){
			c.$table.trigger('applyWidgets'); // make sure zebra widget is applied
		}, 0);
	},
	getOptionSource: function(table, column, onlyAvail) {
		var cts,
			c = table.config,
			wo = c.widgetOptions,
			parsed = [],
			arry = false,
			source = wo.filter_selectSource,
			fxn = $.isFunction(source) ? true : ts.getColumnData( table, source, column );

		// filter select source option
		if (fxn === true) {
			// OVERALL source
			arry = source(table, column, onlyAvail);
		} else if ($.type(source) === 'object' && fxn) {
			// custom select source function for a SPECIFIC COLUMN
			arry = fxn(table, column, onlyAvail);
		}
		if (arry === false) {
			// fall back to original method
			arry = ts.filter.getOptions(table, column, onlyAvail);
		}

		// get unique elements and sort the list
		// if $.tablesorter.sortText exists (not in the original tablesorter),
		// then natural sort the list otherwise use a basic sort
		arry = $.grep(arry, function(value, indx) {
			return $.inArray(value, arry) === indx;
		});

		if (c.$headers.filter('[data-column="' + column + '"]:last').hasClass('filter-select-nosort')) {
			// unsorted select options
			return arry;
		} else {
			// parse select option values
			$.each(arry, function(i, v){
				// parse array data using set column parser; this DOES NOT pass the original
				// table cell to the parser format function
				parsed.push({ t : v, p : c.parsers && c.parsers[column].format( v, table, [], column ) });
			});

			// sort parsed select options
			cts = c.textSorter || '';
			parsed.sort(function(a, b){
				// sortNatural breaks if you don't pass it strings
				var x = a.p.toString(), y = b.p.toString();
				if ($.isFunction(cts)) {
					// custom OVERALL text sorter
					return cts(x, y, true, column, table);
				} else if (typeof(cts) === 'object' && cts.hasOwnProperty(column)) {
					// custom text sorter for a SPECIFIC COLUMN
					return cts[column](x, y, true, column, table);
				} else if (ts.sortNatural) {
					// fall back to natural sort
					return ts.sortNatural(x, y);
				}
				// using an older version! do a basic sort
				return true;
			});
			// rebuild arry from sorted parsed data
			arry = [];
			$.each(parsed, function(i, v){
				arry.push(v.t);
			});
			return arry;
		}
	},
	getOptions: function(table, column, onlyAvail) {
		var rowIndex, tbodyIndex, len, row, cache, cell,
			c = table.config,
			wo = c.widgetOptions,
			$tbodies = c.$table.children('tbody'),
			arry = [];
		for (tbodyIndex = 0; tbodyIndex < $tbodies.length; tbodyIndex++ ) {
			if (!$tbodies.eq(tbodyIndex).hasClass(c.cssInfoBlock)) {
				cache = c.cache[tbodyIndex];
				len = c.cache[tbodyIndex].normalized.length;
				// loop through the rows
				for (rowIndex = 0; rowIndex < len; rowIndex++) {
					// get cached row from cache.row (old) or row data object (new; last item in normalized array)
					row = cache.row ? cache.row[rowIndex] : cache.normalized[rowIndex][c.columns].$row[0];
					// check if has class filtered
					if (onlyAvail && row.className.match(wo.filter_filteredRow)) { continue; }
					// get non-normalized cell content
					if (wo.filter_useParsedData || c.parsers[column].parsed || c.$headers.filter('[data-column="' + column + '"]:last').hasClass('filter-parsed')) {
						arry.push( '' + cache.normalized[rowIndex][column] );
					} else {
						cell = row.cells[column];
						if (cell) {
							arry.push( $.trim( cell.textContent || cell.innerText || $(cell).text() ) );
						}
					}
				}
			}
		}
		return arry;
	},
	buildSelect: function(table, column, updating, onlyAvail) {
		if (!table.config.cache || $.isEmptyObject(table.config.cache)) { return; }
		column = parseInt(column, 10);
		var indx, txt, $filters,
			c = table.config,
			wo = c.widgetOptions,
			node = c.$headers.filter('[data-column="' + column + '"]:last'),
			// t.data('placeholder') won't work in jQuery older than 1.4.3
			options = '<option value="">' + ( node.data('placeholder') || node.attr('data-placeholder') || wo.filter_placeholder.select || '' ) + '</option>',
			arry = ts.filter.getOptionSource(table, column, onlyAvail),
			// Get curent filter value
			currentValue = c.$table.find('thead').find('select.' + ts.css.filter + '[data-column="' + column + '"]').val();

		// build option list
		for (indx = 0; indx < arry.length; indx++) {
			txt = arry[indx].replace(/\"/g, "&quot;");
			// replace quotes - fixes #242 & ignore empty strings - see http://stackoverflow.com/q/14990971/145346
			options += arry[indx] !== '' ? '<option value="' + txt + '"' + (currentValue === txt ? ' selected="selected"' : '') +
				'>' + arry[indx] + '</option>' : '';
		}
		// update all selects in the same column (clone thead in sticky headers & any external selects) - fixes 473
		$filters = ( c.$filters ? c.$filters : c.$table.children('thead') ).find('.' + ts.css.filter);
		if (wo.filter_$externalFilters) {
			$filters = $filters && $filters.length ? $filters.add(wo.filter_$externalFilters) : wo.filter_$externalFilters;
		}
		$filters.filter('select[data-column="' + column + '"]')[ updating ? 'html' : 'append' ](options);
		if (!wo.filter_functions) { wo.filter_functions = {}; }
		wo.filter_functions[column] = true;
	},
	buildDefault: function(table, updating) {
		var columnIndex, $header,
			c = table.config,
			wo = c.widgetOptions,
			columns = c.columns;
		// build default select dropdown
		for (columnIndex = 0; columnIndex < columns; columnIndex++) {
			$header = c.$headers.filter('[data-column="' + columnIndex + '"]:last');
			// look for the filter-select class; build/update it if found
			if (($header.hasClass('filter-select') || ts.getColumnData( table, wo.filter_functions, columnIndex ) === true) && !$header.hasClass('filter-false')) {
				ts.filter.buildSelect(table, columnIndex, updating, $header.hasClass(wo.filter_onlyAvail));
			}
		}
	}
};

ts.getFilters = function(table, getRaw, setFilters, skipFirst) {
	var i, $filters, $column,
		filters = false,
		c = table ? $(table)[0].config : '',
		wo = c ? c.widgetOptions : '';
	if (getRaw !== true && wo && !wo.filter_columnFilters) {
		return $(table).data('lastSearch');
	}
	if (c) {
		if (c.$filters) {
			$filters = c.$filters.find('.' + ts.css.filter);
		}
		if (wo.filter_$externalFilters) {
			$filters = $filters && $filters.length ? $filters.add(wo.filter_$externalFilters) : wo.filter_$externalFilters;
		}
		if ($filters && $filters.length) {
			filters = setFilters || [];
			for (i = 0; i < c.columns + 1; i++) {
				$column = $filters.filter('[data-column="' + (i === c.columns ? 'all' : i) + '"]');
				if ($column.length) {
					// move the latest search to the first slot in the array
					$column = $column.sort(function(a, b){
						return $(b).attr('data-lastSearchTime') - $(a).attr('data-lastSearchTime');
					});
					if ($.isArray(setFilters)) {
						// skip first (latest input) to maintain cursor position while typing
						(skipFirst ? $column.slice(1) : $column).val( setFilters[i] ).trigger('change.tsfilter');
					} else {
						filters[i] = $column.val() || '';
						// don't change the first... it will move the cursor
						$column.slice(1).val( filters[i] );
					}
					// save any match input dynamically
					if (i === c.columns && $column.length) {
						wo.filter_$anyMatch = $column;
					}
				}
			}
		}
	}
	if (filters.length === 0) {
		filters = false;
	}
	return filters;
};

ts.setFilters = function(table, filter, apply, skipFirst) {
	var c = table ? $(table)[0].config : '',
		valid = ts.getFilters(table, true, filter, skipFirst);
	if (c && apply) {
		// ensure new set filters are applied, even if the search is the same
		c.lastCombinedFilter = null;
		c.lastSearch = [];
		ts.filter.searching(c.$table[0], filter, skipFirst);
		c.$table.trigger('filterFomatterUpdate');
	}
	return !!valid;
};

// Widget: Sticky headers
// based on this awesome article:
// http://css-tricks.com/13465-persistent-headers/
// and https://github.com/jmosbech/StickyTableHeaders by Jonas Mosbech
// **************************
ts.addWidget({
	id: "stickyHeaders",
	priority: 60, // sticky widget must be initialized after the filter widget!
	options: {
		stickyHeaders : '',       // extra class name added to the sticky header row
		stickyHeaders_attachTo : null, // jQuery selector or object to attach sticky header to
		stickyHeaders_offset : 0, // number or jquery selector targeting the position:fixed element
		stickyHeaders_filteredToTop: true, // scroll table top into view after filtering
		stickyHeaders_cloneId : '-sticky', // added to table ID, if it exists
		stickyHeaders_addResizeEvent : true, // trigger "resize" event on headers
		stickyHeaders_includeCaption : true, // if false and a caption exist, it won't be included in the sticky header
		stickyHeaders_zIndex : 2 // The zIndex of the stickyHeaders, allows the user to adjust this to their needs
	},
	format: function(table, c, wo) {
		// filter widget doesn't initialize on an empty table. Fixes #449
		if ( c.$table.hasClass('hasStickyHeaders') || ($.inArray('filter', c.widgets) >= 0 && !c.$table.hasClass('hasFilters')) ) {
			return;
		}
		var $table = c.$table,
			$attach = $(wo.stickyHeaders_attachTo),
			$thead = $table.children('thead:first'),
			$win = $attach.length ? $attach : $(window),
			$header = $thead.children('tr').not('.sticky-false').children(),
			innerHeader = '.' + ts.css.headerIn,
			$tfoot = $table.find('tfoot'),
			$stickyOffset = isNaN(wo.stickyHeaders_offset) ? $(wo.stickyHeaders_offset) : '',
			stickyOffset = $attach.length ? 0 : $stickyOffset.length ?
				$stickyOffset.height() || 0 : parseInt(wo.stickyHeaders_offset, 10) || 0,
			$stickyTable = wo.$sticky = $table.clone()
				.addClass('containsStickyHeaders')
				.css({
					position   : $attach.length ? 'absolute' : 'fixed',
					margin     : 0,
					top        : stickyOffset,
					left       : 0,
					visibility : 'hidden',
					zIndex     : wo.stickyHeaders_zIndex ? wo.stickyHeaders_zIndex : 2
				}),
			$stickyThead = $stickyTable.children('thead:first').addClass(ts.css.sticky + ' ' + wo.stickyHeaders),
			$stickyCells,
			laststate = '',
			spacing = 0,
			nonwkie = $table.css('border-collapse') !== 'collapse' && !/(webkit|msie)/i.test(navigator.userAgent),
			resizeHeader = function() {
				stickyOffset = $stickyOffset.length ? $stickyOffset.height() || 0 : parseInt(wo.stickyHeaders_offset, 10) || 0;
				spacing = 0;
				// yes, I dislike browser sniffing, but it really is needed here :(
				// webkit automatically compensates for border spacing
				if (nonwkie) {
					// Firefox & Opera use the border-spacing
					// update border-spacing here because of demos that switch themes
					spacing = parseInt($header.eq(0).css('border-left-width'), 10) * 2;
				}
				$stickyTable.css({
					left : $attach.length ? (parseInt($attach.css('padding-left'), 10) || 0) + parseInt(c.$table.css('padding-left'), 10) +
						parseInt(c.$table.css('margin-left'), 10) + parseInt($table.css('border-left-width'), 10) :
						$thead.offset().left - $win.scrollLeft() - spacing,
					width: $table.width()
				});
				$stickyCells.filter(':visible').each(function(i) {
					var $cell = $header.filter(':visible').eq(i),
						// some wibbly-wobbly... timey-wimey... stuff, to make columns line up in Firefox
						offset = nonwkie && $(this).attr('data-column') === ( '' + parseInt(c.columns/2, 10) ) ? 1 : 0;
					$(this)
						.css({ width: $cell.width() - spacing })
						.find(innerHeader).width( $cell.find(innerHeader).width() - offset );
				});
			};
		// fix clone ID, if it exists - fixes #271
		if ($stickyTable.attr('id')) { $stickyTable[0].id += wo.stickyHeaders_cloneId; }
		// clear out cloned table, except for sticky header
		// include caption & filter row (fixes #126 & #249) - don't remove cells to get correct cell indexing
		$stickyTable.find('thead:gt(0), tr.sticky-false').hide();
		$stickyTable.find('tbody, tfoot').remove();
		if (!wo.stickyHeaders_includeCaption) {
			$stickyTable.find('caption').remove();
		} else {
			$stickyTable.find('caption').css( 'margin-left', '-1px' );
		}
		// issue #172 - find td/th in sticky header
		$stickyCells = $stickyThead.children().children();
		$stickyTable.css({ height:0, width:0, padding:0, margin:0, border:0 });
		// remove resizable block
		$stickyCells.find('.' + ts.css.resizer).remove();
		// update sticky header class names to match real header after sorting
		$table
			.addClass('hasStickyHeaders')
			.bind('pagerComplete.tsSticky', function() {
				resizeHeader();
			});

		ts.bindEvents(table, $stickyThead.children().children('.tablesorter-header'));

		// add stickyheaders AFTER the table. If the table is selected by ID, the original one (first) will be returned.
		$table.after( $stickyTable );
		// make it sticky!
		$win.bind('scroll.tsSticky resize.tsSticky', function(event) {
			if (!$table.is(':visible')) { return; } // fixes #278
			var prefix = 'tablesorter-sticky-',
				offset = $table.offset(),
				captionHeight = (wo.stickyHeaders_includeCaption ? 0 : $table.find('caption').outerHeight(true)),
				scrollTop = ($attach.length ? $attach.offset().top : $win.scrollTop()) + stickyOffset - captionHeight,
				tableHeight = $table.height() - ($stickyTable.height() + ($tfoot.height() || 0)),
				isVisible = (scrollTop > offset.top) && (scrollTop < offset.top + tableHeight) ? 'visible' : 'hidden',
				cssSettings = { visibility : isVisible };
			if ($attach.length) {
				cssSettings.top = $attach.scrollTop();
			} else {
				// adjust when scrolling horizontally - fixes issue #143
				cssSettings.left = $thead.offset().left - $win.scrollLeft() - spacing;
			}
			$stickyTable
				.removeClass(prefix + 'visible ' + prefix + 'hidden')
				.addClass(prefix + isVisible)
				.css(cssSettings);
			if (isVisible !== laststate || event.type === 'resize') {
				// make sure the column widths match
				resizeHeader();
				laststate = isVisible;
			}
		});
		if (wo.stickyHeaders_addResizeEvent) {
			ts.addHeaderResizeEvent(table);
		}

		// look for filter widget
		if ($table.hasClass('hasFilters')) {
			// scroll table into view after filtering, if sticky header is active - #482
			$table.bind('filterEnd', function() {
				// $(':focus') needs jQuery 1.6+
				var $td = $(document.activeElement).closest('td'),
					column = $td.parent().children().index($td);
				// only scroll if sticky header is active
				if ($stickyTable.hasClass(ts.css.stickyVis) && wo.stickyHeaders_filteredToTop) {
					// scroll to original table (not sticky clone)
					window.scrollTo(0, $table.position().top);
					// give same input/select focus; check if c.$filters exists; fixes #594
					if (column >= 0 && c.$filters) {
						c.$filters.eq(column).find('a, select, input').filter(':visible').focus();
					}
				}
			});
			ts.filter.bindSearch( $table, $stickyCells.find('.' + ts.css.filter) );
			// support hideFilters
			if (wo.filter_hideFilters) {
				ts.filter.hideFilters($stickyTable, c);
			}
		}

		$table.trigger('stickyHeadersInit');

	},
	remove: function(table, c, wo) {
		c.$table
			.removeClass('hasStickyHeaders')
			.unbind('pagerComplete.tsSticky')
			.find('.' + ts.css.sticky).remove();
		if (wo.$sticky && wo.$sticky.length) { wo.$sticky.remove(); } // remove cloned table
		// don't unbind if any table on the page still has stickyheaders applied
		if (!$('.hasStickyHeaders').length) {
			$(window).unbind('scroll.tsSticky resize.tsSticky');
		}
		ts.addHeaderResizeEvent(table, false);
	}
});

// Add Column resizing widget
// this widget saves the column widths if
// $.tablesorter.storage function is included
// **************************
ts.addWidget({
	id: "resizable",
	priority: 40,
	options: {
		resizable : true,
		resizable_addLastColumn : false,
		resizable_widths : [],
		resizable_throttle : false // set to true (5ms) or any number 0-10 range
	},
	format: function(table, c, wo) {
		if (c.$table.hasClass('hasResizable')) { return; }
		c.$table.addClass('hasResizable');
		ts.resizableReset(table, true); // set default widths
		var $rows, $columns, $column, column, timer,
			storedSizes = {},
			$table = c.$table,
			mouseXPosition = 0,
			$target = null,
			$next = null,
			fullWidth = Math.abs($table.parent().width() - $table.width()) < 20,
			mouseMove = function(event){
				if (mouseXPosition === 0 || !$target) { return; }
				// resize columns
				var leftEdge = event.pageX - mouseXPosition,
					targetWidth = $target.width();
				$target.width( targetWidth + leftEdge );
				if ($target.width() !== targetWidth && fullWidth) {
					$next.width( $next.width() - leftEdge );
				}
				mouseXPosition = event.pageX;
			},
			stopResize = function() {
				if (ts.storage && $target && $next) {
					storedSizes = {};
					storedSizes[$target.index()] = $target.width();
					storedSizes[$next.index()] = $next.width();
					$target.width( storedSizes[$target.index()] );
					$next.width( storedSizes[$next.index()] );
					if (wo.resizable !== false) {
						// save all column widths
						ts.storage(table, 'tablesorter-resizable', c.$headers.map(function(){ return $(this).width(); }).get() );
					}
				}
				mouseXPosition = 0;
				$target = $next = null;
				$(window).trigger('resize'); // will update stickyHeaders, just in case
			};
		storedSizes = (ts.storage && wo.resizable !== false) ? ts.storage(table, 'tablesorter-resizable') : {};
		// process only if table ID or url match
		if (storedSizes) {
			for (column in storedSizes) {
				if (!isNaN(column) && column < c.$headers.length) {
					c.$headers.eq(column).width(storedSizes[column]); // set saved resizable widths
				}
			}
		}
		$rows = $table.children('thead:first').children('tr');
		// add resizable-false class name to headers (across rows as needed)
		$rows.children().each(function() {
			var canResize,
				$column = $(this);
			column = $column.attr('data-column');
			canResize = ts.getData( $column, ts.getColumnData( table, c.headers, column ), 'resizable') === "false";
			$rows.children().filter('[data-column="' + column + '"]')[canResize ? 'addClass' : 'removeClass']('resizable-false');
		});
		// add wrapper inside each cell to allow for positioning of the resizable target block
		$rows.each(function() {
			$column = $(this).children().not('.resizable-false');
			if (!$(this).find('.' + ts.css.wrapper).length) {
				// Firefox needs this inner div to position the resizer correctly
				$column.wrapInner('<div class="' + ts.css.wrapper + '" style="position:relative;height:100%;width:100%"></div>');
			}
			// don't include the last column of the row
			if (!wo.resizable_addLastColumn) { $column = $column.slice(0,-1); }
			$columns = $columns ? $columns.add($column) : $column;
		});
		$columns
		.each(function() {
			var $column = $(this),
				padding = parseInt($column.css('padding-right'), 10) + 10; // 10 is 1/2 of the 20px wide resizer
			$column
				.find('.' + ts.css.wrapper)
				.append('<div class="' + ts.css.resizer + '" style="cursor:w-resize;position:absolute;z-index:1;right:-' +
					padding + 'px;top:0;height:100%;width:20px;"></div>');
		})
		.find('.' + ts.css.resizer)
		.bind('mousedown', function(event) {
			// save header cell and mouse position
			$target = $(event.target).closest('th');
			var $header = c.$headers.filter('[data-column="' + $target.attr('data-column') + '"]');
			if ($header.length > 1) { $target = $target.add($header); }
			// if table is not as wide as it's parent, then resize the table
			$next = event.shiftKey ? $target.parent().find('th').not('.resizable-false').filter(':last') : $target.nextAll(':not(.resizable-false)').eq(0);
			mouseXPosition = event.pageX;
		});
		$(document)
		.bind('mousemove.tsresize', function(event) {
			// ignore mousemove if no mousedown
			if (mouseXPosition === 0 || !$target) { return; }
			if (wo.resizable_throttle) {
				clearTimeout(timer);
				timer = setTimeout(function(){
					mouseMove(event);
				}, isNaN(wo.resizable_throttle) ? 5 : wo.resizable_throttle );
			} else {
				mouseMove(event);
			}
		})
		.bind('mouseup.tsresize', function() {
			stopResize();
		});

		// right click to reset columns to default widths
		$table.find('thead:first').bind('contextmenu.tsresize', function() {
			ts.resizableReset(table);
			// $.isEmptyObject() needs jQuery 1.4+; allow right click if already reset
			var allowClick = $.isEmptyObject ? $.isEmptyObject(storedSizes) : true;
			storedSizes = {};
			return allowClick;
		});
	},
	remove: function(table, c) {
		c.$table
			.removeClass('hasResizable')
			.children('thead')
			.unbind('mouseup.tsresize mouseleave.tsresize contextmenu.tsresize')
			.children('tr').children()
			.unbind('mousemove.tsresize mouseup.tsresize')
			// don't remove "tablesorter-wrapper" as uitheme uses it too
			.find('.' + ts.css.resizer).remove();
		ts.resizableReset(table);
	}
});
ts.resizableReset = function(table, nosave) {
	$(table).each(function(){
		var $t,
			c = this.config,
			wo = c && c.widgetOptions;
		if (table && c) {
			c.$headers.each(function(i){
				$t = $(this);
				if (wo.resizable_widths[i]) {
					$t.css('width', wo.resizable_widths[i]);
				} else if (!$t.hasClass('resizable-false')) {
					// don't clear the width of any column that is not resizable
					$t.css('width','');
				}
			});
			if (ts.storage && !nosave) { ts.storage(this, 'tablesorter-resizable', {}); }
		}
	});
};

// Save table sort widget
// this widget saves the last sort only if the
// saveSort widget option is true AND the
// $.tablesorter.storage function is included
// **************************
ts.addWidget({
	id: 'saveSort',
	priority: 20,
	options: {
		saveSort : true
	},
	init: function(table, thisWidget, c, wo) {
		// run widget format before all other widgets are applied to the table
		thisWidget.format(table, c, wo, true);
	},
	format: function(table, c, wo, init) {
		var stored, time,
			$table = c.$table,
			saveSort = wo.saveSort !== false, // make saveSort active/inactive; default to true
			sortList = { "sortList" : c.sortList };
		if (c.debug) {
			time = new Date();
		}
		if ($table.hasClass('hasSaveSort')) {
			if (saveSort && table.hasInitialized && ts.storage) {
				ts.storage( table, 'tablesorter-savesort', sortList );
				if (c.debug) {
					ts.benchmark('saveSort widget: Saving last sort: ' + c.sortList, time);
				}
			}
		} else {
			// set table sort on initial run of the widget
			$table.addClass('hasSaveSort');
			sortList = '';
			// get data
			if (ts.storage) {
				stored = ts.storage( table, 'tablesorter-savesort' );
				sortList = (stored && stored.hasOwnProperty('sortList') && $.isArray(stored.sortList)) ? stored.sortList : '';
				if (c.debug) {
					ts.benchmark('saveSort: Last sort loaded: "' + sortList + '"', time);
				}
				$table.bind('saveSortReset', function(event) {
					event.stopPropagation();
					ts.storage( table, 'tablesorter-savesort', '' );
				});
			}
			// init is true when widget init is run, this will run this widget before all other widgets have initialized
			// this method allows using this widget in the original tablesorter plugin; but then it will run all widgets twice.
			if (init && sortList && sortList.length > 0) {
				c.sortList = sortList;
			} else if (table.hasInitialized && sortList && sortList.length > 0) {
				// update sort change
				$table.trigger('sorton', [sortList]);
			}
		}
	},
	remove: function(table) {
		// clear storage
		if (ts.storage) { ts.storage( table, 'tablesorter-savesort', '' ); }
	}
});

})(jQuery);

/*! tableSorter 2.16+ widgets - updated 7/17/2014 (v2.17.5) */
;(function(k){
var c=k.tablesorter=k.tablesorter||{};
c.themes={bootstrap:{table:"table table-bordered table-striped",caption:"caption",header:"bootstrap-header",footerRow:"",footerCells:"",icons:"",sortNone:"bootstrap-icon-unsorted",sortAsc:"icon-chevron-up glyphicon glyphicon-chevron-up",sortDesc:"icon-chevron-down glyphicon glyphicon-chevron-down",active:"",hover:"",filterRow:"",even:"",odd:""},jui:{table:"ui-widget ui-widget-content ui-corner-all",caption:"ui-widget-content ui-corner-all",header:"ui-widget-header ui-corner-all ui-state-default", footerRow:"",footerCells:"",icons:"ui-icon",sortNone:"ui-icon-carat-2-n-s",sortAsc:"ui-icon-carat-1-n",sortDesc:"ui-icon-carat-1-s",active:"ui-state-active",hover:"ui-state-hover",filterRow:"",even:"ui-widget-content",odd:"ui-state-default"}};k.extend(c.css,{filterRow:"tablesorter-filter-row",filter:"tablesorter-filter",wrapper:"tablesorter-wrapper",resizer:"tablesorter-resizer",sticky:"tablesorter-stickyHeader",stickyVis:"tablesorter-sticky-visible"});
c.storage=function(b,a,e,d){b=k(b)[0];var c, f,h=!1;c={};f=b.config;var l=k(b);b=d&&d.id||l.attr(d&&d.group||"data-table-group")||b.id||k(".tablesorter").index(l);d=d&&d.url||l.attr(d&&d.page||"data-table-page")||f&&f.fixedUrl||window.location.pathname;if("localStorage"in window)try{window.localStorage.setItem("_tmptest","temp"),h=!0,window.localStorage.removeItem("_tmptest")}catch(m){}k.parseJSON&&(h?c=k.parseJSON(localStorage[a]||"{}"):(f=document.cookie.split(/[;\s|=]/),c=k.inArray(a,f)+1,c=0!==c?k.parseJSON(f[c]||"{}"):{}));if((e||""=== e)&&window.JSON&&JSON.hasOwnProperty("stringify"))c[d]||(c[d]={}),c[d][b]=e,h?localStorage[a]=JSON.stringify(c):(e=new Date,e.setTime(e.getTime()+31536E6),document.cookie=a+"="+JSON.stringify(c).replace(/\"/g,'"')+"; expires="+e.toGMTString()+"; path=/");else return c&&c[d]?c[d][b]:""};
c.addHeaderResizeEvent=function(b,a,c){var d;c=k.extend({},{timer:250},c);var g=b.config,f=g.widgetOptions,h=function(a){f.resize_flag=!0;d=[];g.$headers.each(function(){var a=k(this),b=a.data("savedSizes")||[0,0], c=this.offsetWidth,e=this.offsetHeight;if(c!==b[0]||e!==b[1])a.data("savedSizes",[c,e]),d.push(this)});d.length&&!1!==a&&g.$table.trigger("resize",[d]);f.resize_flag=!1};h(!1);clearInterval(f.resize_timer);if(a)return f.resize_flag=!1;f.resize_timer=setInterval(function(){f.resize_flag||h()},c.timer)};
c.addWidget({id:"uitheme",priority:10,format:function(b,a,e){var d,g,f,h=c.themes;d=a.$table;f=a.$headers;var l=a.theme||"jui",m=h[l]||h.jui,h=m.sortNone+" "+m.sortDesc+" "+m.sortAsc;a.debug&&(g=new Date); d.hasClass("tablesorter-"+l)&&a.theme!==l&&b.hasInitialized||(""!==m.even&&(e.zebra[0]+=" "+m.even),""!==m.odd&&(e.zebra[1]+=" "+m.odd),d.find("caption").addClass(m.caption),b=d.removeClass(""===a.theme?"":"tablesorter-"+a.theme).addClass("tablesorter-"+l+" "+m.table).find("tfoot"),b.length&&b.find("tr").addClass(m.footerRow).children("th, td").addClass(m.footerCells),f.addClass(m.header).not(".sorter-false").bind("mouseenter.tsuitheme mouseleave.tsuitheme",function(a){k(this)["mouseenter"===a.type? "addClass":"removeClass"](m.hover)}),f.find("."+c.css.wrapper).length||f.wrapInner('<div class="'+c.css.wrapper+'" style="position:relative;height:100%;width:100%"></div>'),a.cssIcon&&f.find("."+c.css.icon).addClass(m.icons),d.hasClass("hasFilters")&&f.find("."+c.css.filterRow).addClass(m.filterRow));for(d=0;d<a.columns;d++)f=a.$headers.add(a.$extraHeaders).filter('[data-column="'+d+'"]'),b=c.css.icon?f.find("."+c.css.icon):f,e=a.$headers.filter('[data-column="'+d+'"]:last'),e.length&&(e[0].sortDisabled? (f.removeClass(h),b.removeClass(h+" "+m.icons)):(e=f.hasClass(c.css.sortAsc)?m.sortAsc:f.hasClass(c.css.sortDesc)?m.sortDesc:f.hasClass(c.css.header)?m.sortNone:"",f[e===m.sortNone?"removeClass":"addClass"](m.active),b.removeClass(h).addClass(e)));a.debug&&c.benchmark("Applying "+l+" theme",g)},remove:function(b,a,e){b=a.$table;a=a.theme||"jui";e=c.themes[a]||c.themes.jui;var d=b.children("thead").children(),g=e.sortNone+" "+e.sortDesc+" "+e.sortAsc;b.removeClass("tablesorter-"+a+" "+e.table).find(c.css.header).removeClass(e.header); d.unbind("mouseenter.tsuitheme mouseleave.tsuitheme").removeClass(e.hover+" "+g+" "+e.active).find("."+c.css.filterRow).removeClass(e.filterRow);d.find("."+c.css.icon).removeClass(e.icons)}});
c.addWidget({id:"columns",priority:30,options:{columns:["primary","secondary","tertiary"]},format:function(b,a,e){var d,g,f,h,l,m,p,n,s=a.$table,r=a.$tbodies,t=a.sortList,v=t.length,w=e&&e.columns||["primary","secondary","tertiary"],x=w.length-1;p=w.join(" ");a.debug&&(d=new Date);for(f=0;f<r.length;f++)g=c.processTbody(b, r.eq(f),!0),h=g.children("tr"),h.each(function(){l=k(this);if("none"!==this.style.display&&(m=l.children().removeClass(p),t&&t[0]&&(m.eq(t[0][0]).addClass(w[0]),1<v)))for(n=1;n<v;n++)m.eq(t[n][0]).addClass(w[n]||w[x])}),c.processTbody(b,g,!1);b=!1!==e.columns_thead?["thead tr"]:[];!1!==e.columns_tfoot&&b.push("tfoot tr");if(b.length&&(h=s.find(b.join(",")).children().removeClass(p),v))for(n=0;n<v;n++)h.filter('[data-column="'+t[n][0]+'"]').addClass(w[n]||w[x]);a.debug&&c.benchmark("Applying Columns widget", d)},remove:function(b,a,e){var d=a.$tbodies,g=(e.columns||["primary","secondary","tertiary"]).join(" ");a.$headers.removeClass(g);a.$table.children("tfoot").children("tr").children("th, td").removeClass(g);for(a=0;a<d.length;a++)e=c.processTbody(b,d.eq(a),!0),e.children("tr").each(function(){k(this).children().removeClass(g)}),c.processTbody(b,e,!1)}});
c.addWidget({id:"filter",priority:50,options:{filter_childRows:!1,filter_columnFilters:!0,filter_cssFilter:"",filter_external:"",filter_filteredRow:"filtered", filter_formatter:null,filter_functions:null,filter_hideEmpty:!0,filter_hideFilters:!1,filter_ignoreCase:!0,filter_liveSearch:!0,filter_onlyAvail:"filter-onlyAvail",filter_placeholder:{search:"",select:""},filter_reset:null,filter_saveFilters:!1,filter_searchDelay:300,filter_searchFiltered:!0,filter_selectSource:null,filter_startsWith:!1,filter_useParsedData:!1,filter_serversideFiltering:!1,filter_defaultAttrib:"data-value"},format:function(b,a,e){a.$table.hasClass("hasFilters")||c.filter.init(b,a, e)},remove:function(b,a,e){var d,g=a.$tbodies;a.$table.removeClass("hasFilters").unbind("addRows updateCell update updateRows updateComplete appendCache filterReset filterEnd search ".split(" ").join(a.namespace+"filter ")).find("."+c.css.filterRow).remove();for(a=0;a<g.length;a++)d=c.processTbody(b,g.eq(a),!0),d.children().removeClass(e.filter_filteredRow).show(),c.processTbody(b,d,!1);e.filter_reset&&k(document).undelegate(e.filter_reset,"click.tsfilter")}});
c.filter={regex:{regex:/^\/((?:\\\/|[^\/])+)\/([mig]{0,3})?$/, child:/tablesorter-childRow/,filtered:/filtered/,type:/undefined|number/,exact:/(^[\"|\'|=]+)|([\"|\'|=]+$)/g,nondigit:/[^\w,. \-()]/g,operators:/[<>=]/g},types:{regex:function(b,a,e,d){if(c.filter.regex.regex.test(a)){var g;b=c.filter.regex.regex.exec(a);try{g=(new RegExp(b[1],b[2])).test(d)}catch(f){g=!1}return g}return null},operators:function(b,a,e,d,g,f,h,l,m){if(/^[<>]=?/.test(a)){var p;e=h.config;b=c.formatFloat(a.replace(c.filter.regex.operators,""),h);l=e.parsers[f];e=b;if(m[f]||"numeric"=== l.type)p=c.filter.parseFilter(h,k.trim(""+a.replace(c.filter.regex.operators,"")),f,m[f],!0),b="number"!==typeof p||""===p||isNaN(p)?b:p;d=!m[f]&&"numeric"!==l.type||isNaN(b)||"undefined"===typeof g?isNaN(d)?c.formatFloat(d.replace(c.filter.regex.nondigit,""),h):c.formatFloat(d,h):g;/>/.test(a)&&(p=/>=/.test(a)?d>=b:d>b);/</.test(a)&&(p=/<=/.test(a)?d<=b:d<b);p||""!==e||(p=!0);return p}return null},notMatch:function(b,a,e,d,g,f,h,l,m){if(/^\!/.test(a)){a=c.filter.parseFilter(h,a.replace("!",""),f, m[f]);if(c.filter.regex.exact.test(a))return a=a.replace(c.filter.regex.exact,""),""===a?!0:k.trim(a)!==d;b=d.search(k.trim(a));return""===a?!0:!(l.filter_startsWith?0===b:0<=b)}return null},exact:function(b,a,e,d,g,f,h,l,m,p){return c.filter.regex.exact.test(a)?(b=c.filter.parseFilter(h,a.replace(c.filter.regex.exact,""),f,m[f]),p?0<=k.inArray(b,p):b==d):null},and:function(b,a,e,d,g,f,h,l,m){if(c.filter.regex.andTest.test(b)){b=a.split(c.filter.regex.andSplit);a=0<=d.search(k.trim(c.filter.parseFilter(h, b[0],f,m[f])));for(e=b.length-1;a&&e;)a=a&&0<=d.search(k.trim(c.filter.parseFilter(h,b[e],f,m[f]))),e--;return a}return null},range:function(b,a,e,d,g,f,h,k,m){if(c.filter.regex.toTest.test(a)){b=h.config;var p=a.split(c.filter.regex.toSplit);e=c.formatFloat(c.filter.parseFilter(h,p[0].replace(c.filter.regex.nondigit,""),f,m[f]),h);k=c.formatFloat(c.filter.parseFilter(h,p[1].replace(c.filter.regex.nondigit,""),f,m[f]),h);if(m[f]||"numeric"===b.parsers[f].type)a=b.parsers[f].format(""+p[0],h,b.$headers.eq(f), f),e=""===a||isNaN(a)?e:a,a=b.parsers[f].format(""+p[1],h,b.$headers.eq(f),f),k=""===a||isNaN(a)?k:a;a=!m[f]&&"numeric"!==b.parsers[f].type||isNaN(e)||isNaN(k)?isNaN(d)?c.formatFloat(d.replace(c.filter.regex.nondigit,""),h):c.formatFloat(d,h):g;e>k&&(d=e,e=k,k=d);return a>=e&&a<=k||""===e||""===k}return null},wild:function(b,a,e,d,g,f,h,l,m,p){return/[\?|\*]/.test(a)||c.filter.regex.orReplace.test(b)?(b=h.config,a=c.filter.parseFilter(h,a.replace(c.filter.regex.orReplace,"|"),f,m[f]),!b.$headers.filter('[data-column="'+ f+'"]:last').hasClass("filter-match")&&/\|/.test(a)&&(a=k.isArray(p)?"("+a+")":"^("+a+")$"),(new RegExp(a.replace(/\?/g,"\\S{1}").replace(/\*/g,"\\S*"))).test(d)):null},fuzzy:function(b,a,e,d,g,f,h,k,m){if(/^~/.test(a)){b=0;e=d.length;f=c.filter.parseFilter(h,a.slice(1),f,m[f]);for(a=0;a<e;a++)d[a]===f[b]&&(b+=1);return b===f.length?!0:!1}return null}},init:function(b,a,e){c.language=k.extend(!0,{},{to:"to",or:"or",and:"and"},c.language);var d,g,f,h,l,m,p;d=c.filter.regex;a.debug&&(m=new Date);a.$table.addClass("hasFilters"); e.searchTimer=null;e.filter_initTimer=null;e.filter_formatterCount=0;e.filter_formatterInit=[];k.extend(d,{child:new RegExp(a.cssChildRow),filtered:new RegExp(e.filter_filteredRow),alreadyFiltered:new RegExp("(\\s+("+c.language.or+"|-|"+c.language.to+")\\s+)","i"),toTest:new RegExp("\\s+(-|"+c.language.to+")\\s+","i"),toSplit:new RegExp("(?:\\s+(?:-|"+c.language.to+")\\s+)","gi"),andTest:new RegExp("\\s+("+c.language.and+"|&&)\\s+","i"),andSplit:new RegExp("(?:\\s+(?:"+c.language.and+"|&&)\\s+)", "gi"),orReplace:new RegExp("\\s+("+c.language.or+")\\s+","gi")});!1!==e.filter_columnFilters&&a.$headers.filter(".filter-false").length!==a.$headers.length&&c.filter.buildRow(b,a,e);a.$table.bind("addRows updateCell update updateRows updateComplete appendCache filterReset filterEnd search ".split(" ").join(a.namespace+"filter "),function(d,f){a.$table.find("."+c.css.filterRow).toggle(!(e.filter_hideEmpty&&k.isEmptyObject(a.cache)&&(!a.delayInit||"appendCache"!==d.type)));/(search|filter)/.test(d.type)|| (d.stopPropagation(),c.filter.buildDefault(b,!0));"filterReset"===d.type?(a.$table.find("."+c.css.filter).add(e.filter_$externalFilters).val(""),c.filter.searching(b,[])):"filterEnd"===d.type?c.filter.buildDefault(b,!0):(f="search"===d.type?f:"updateComplete"===d.type?a.$table.data("lastSearch"):"",/(update|add)/.test(d.type)&&"updateComplete"!==d.type&&(a.lastCombinedFilter=null,a.lastSearch=[]),c.filter.searching(b,f,!0));return!1});e.filter_reset&&(e.filter_reset instanceof k?e.filter_reset.click(function(){a.$table.trigger("filterReset")}): k(e.filter_reset).length&&k(document).undelegate(e.filter_reset,"click.tsfilter").delegate(e.filter_reset,"click.tsfilter",function(){a.$table.trigger("filterReset")}));if(e.filter_functions)for(h=0;h<a.columns;h++)if(p=c.getColumnData(b,e.filter_functions,h))if(f=a.$headers.filter('[data-column="'+h+'"]:last'),d="",!0===p&&!f.hasClass("filter-false"))c.filter.buildSelect(b,h);else if("object"===typeof p&&!f.hasClass("filter-false")){for(g in p)"string"===typeof g&&(d+=""===d?'<option value="">'+ (f.data("placeholder")||f.attr("data-placeholder")||e.filter_placeholder.select||"")+"</option>":"",d+='<option value="'+g+'">'+g+"</option>");a.$table.find("thead").find("select."+c.css.filter+'[data-column="'+h+'"]').append(d)}c.filter.buildDefault(b,!0);c.filter.bindSearch(b,a.$table.find("."+c.css.filter),!0);e.filter_external&&c.filter.bindSearch(b,e.filter_external);e.filter_hideFilters&&c.filter.hideFilters(b,a);a.showProcessing&&a.$table.bind("filterStart"+a.namespace+"filter filterEnd"+a.namespace+ "filter",function(d,e){f=e?a.$table.find("."+c.css.header).filter("[data-column]").filter(function(){return""!==e[k(this).data("column")]}):"";c.isProcessing(b,"filterStart"===d.type,e?f:"")});a.filteredRows=a.totalRows;a.debug&&c.benchmark("Applying Filter widget",m);a.$table.bind("tablesorter-initialized pagerInitialized",function(d){var e=this.config.widgetOptions;l=c.filter.setDefaults(b,a,e)||[];l.length&&(a.delayInit&&""===l.join("")||c.setFilters(b,l,!0));a.$table.trigger("filterFomatterUpdate"); setTimeout(function(){e.filter_initialized||c.filter.filterInitComplete(a)},100)});a.pager&&a.pager.initialized&&!e.filter_initialized&&(a.$table.trigger("filterFomatterUpdate"),setTimeout(function(){c.filter.filterInitComplete(a)},100))},formatterUpdated:function(b,a){var c=b.closest("table")[0].config.widgetOptions;c.filter_initialized||(c.filter_formatterInit[a]=1)},filterInitComplete:function(b){var a=b.widgetOptions,c=0;k.each(a.filter_formatterInit,function(a,b){1===b&&c++});clearTimeout(a.filter_initTimer); a.filter_initialized||c!==a.filter_formatterCount?a.filter_initialized||(a.filter_initTimer=setTimeout(function(){a.filter_initialized=!0;b.$table.trigger("filterInit",b)},500)):(a.filter_initialized=!0,b.$table.trigger("filterInit",b))},setDefaults:function(b,a,e){var d,g=c.getFilters(b)||[];e.filter_saveFilters&&c.storage&&(d=c.storage(b,"tablesorter-filters")||[],(b=k.isArray(d))&&""===d.join("")||!b||(g=d));if(""===g.join(""))for(b=0;b<a.columns;b++)g[b]=a.$headers.filter('[data-column="'+b+'"]:last').attr(e.filter_defaultAttrib)|| g[b];a.$table.data("lastSearch",g);return g},parseFilter:function(b,a,c,d,g){var f=b.config;return g||d?f.parsers[c].format(a,b,[],c):a},buildRow:function(b,a,e){var d,g,f,h,l=a.columns;f='<tr class="'+c.css.filterRow+'">';for(d=0;d<l;d++)f+="<td></td>";a.$filters=k(f+"</tr>").appendTo(a.$table.children("thead").eq(0)).find("td");for(d=0;d<l;d++)g=a.$headers.filter('[data-column="'+d+'"]:last'),f=c.getColumnData(b,e.filter_functions,d),f=e.filter_functions&&f&&"function"!==typeof f||g.hasClass("filter-select"), h="false"===c.getData(g[0],c.getColumnData(b,a.headers,d),"filter"),f?f=k("<select>").appendTo(a.$filters.eq(d)):((f=c.getColumnData(b,e.filter_formatter,d))?(e.filter_formatterCount++,(f=f(a.$filters.eq(d),d))&&0===f.length&&(f=a.$filters.eq(d).children("input")),f&&(0===f.parent().length||f.parent().length&&f.parent()[0]!==a.$filters[d])&&a.$filters.eq(d).append(f)):f=k('<input type="search">').appendTo(a.$filters.eq(d)),f&&f.attr("placeholder",g.data("placeholder")||g.attr("data-placeholder")|| e.filter_placeholder.search||"")),f&&(g=(k.isArray(e.filter_cssFilter)?"undefined"!==typeof e.filter_cssFilter[d]?e.filter_cssFilter[d]||"":"":e.filter_cssFilter)||"",f.addClass(c.css.filter+" "+g).attr("data-column",d),h&&(f.attr("placeholder","").addClass("disabled")[0].disabled=!0))},bindSearch:function(b,a,e){b=k(b)[0];a=k(a);if(a.length){var d=b.config,g=d.widgetOptions,f=g.filter_$externalFilters;!0!==e&&(g.filter_$anyMatch=a.filter('[data-column="all"]'),g.filter_$externalFilters=f&&f.length? g.filter_$externalFilters.add(a):a,c.setFilters(b,d.$table.data("lastSearch")||[],!1===e));a.attr("data-lastSearchTime",(new Date).getTime()).unbind(["keypress","keyup","search","change",""].join(d.namespace+"filter ")).bind("keyup"+d.namespace+"filter",function(a){k(this).attr("data-lastSearchTime",(new Date).getTime());if(27===a.which)this.value="";else if(!1===g.filter_liveSearch||""!==this.value&&("number"===typeof g.filter_liveSearch&&this.value.length<g.filter_liveSearch||13!==a.which&&8!== a.which&&(32>a.which||37<=a.which&&40>=a.which)))return;c.filter.searching(b,!0,!0)}).bind(["search","change","keypress",""].join(d.namespace+"filter "),function(a){var e=k(this).data("column");if(13===a.which||"search"===a.type||"change"===a.type&&this.value!==d.lastSearch[e])a.preventDefault(),k(this).attr("data-lastSearchTime",(new Date).getTime()),c.filter.searching(b,!1,!0)})}},searching:function(b,a,e){var d=b.config.widgetOptions;clearTimeout(d.searchTimer);"undefined"===typeof a||!0===a?d.searchTimer= setTimeout(function(){c.filter.checkFilters(b,a,e)},d.filter_liveSearch?d.filter_searchDelay:10):c.filter.checkFilters(b,a,e)},checkFilters:function(b,a,e){var d=b.config,g=d.widgetOptions,f=k.isArray(a),h=f?a:c.getFilters(b,!0),l=(h||[]).join("");if(k.isEmptyObject(d.cache))d.delayInit&&d.pager&&d.pager.initialized&&d.$table.trigger("updateCache",[function(){c.filter.checkFilters(b,!1,e)}]);else if(f&&(c.setFilters(b,h,!1,!0!==e),g.filter_initialized||(d.lastCombinedFilter="")),g.filter_hideFilters&& d.$table.find("."+c.css.filterRow).trigger(""===l?"mouseleave":"mouseenter"),d.lastCombinedFilter!==l||!1===a)if(!1===a&&(d.lastCombinedFilter=null,d.lastSearch=[]),g.filter_initialized&&d.$table.trigger("filterStart",[h]),d.showProcessing)setTimeout(function(){c.filter.findRows(b,h,l);return!1},30);else return c.filter.findRows(b,h,l),!1},hideFilters:function(b,a){var e,d,g;k(b).find("."+c.css.filterRow).addClass("hideme").bind("mouseenter mouseleave",function(b){e=k(this);clearTimeout(g);g=setTimeout(function(){/enter|over/.test(b.type)? e.removeClass("hideme"):k(document.activeElement).closest("tr")[0]!==e[0]&&""===a.lastCombinedFilter&&e.addClass("hideme")},200)}).find("input, select").bind("focus blur",function(b){d=k(this).closest("tr");clearTimeout(g);g=setTimeout(function(){if(""===c.getFilters(a.$table).join(""))d["focus"===b.type?"removeClass":"addClass"]("hideme")},200)})},findRows:function(b,a,e){if(b.config.lastCombinedFilter!==e){var d,g,f,h,l,m,p,n,s,r,t,v,w,x,z,y,A,B,L,C,G,H,I,J,M,D,F=c.filter.regex,q=b.config,u=q.widgetOptions, N=q.columns,K=q.$table.children("tbody"),O=["range","notMatch","operators"],E=q.$headers.map(function(a){return q.parsers&&q.parsers[a]&&q.parsers[a].parsed||c.getData&&"parsed"===c.getData(q.$headers.filter('[data-column="'+a+'"]:last'),c.getColumnData(b,q.headers,a),"filter")||k(this).hasClass("filter-parsed")}).get();q.debug&&(L=new Date);q.filteredRows=0;for(l=q.totalRows=0;l<K.length;l++)if(!K.eq(l).hasClass(q.cssInfoBlock||c.css.info)){m=c.processTbody(b,K.eq(l),!0);n=q.columns;f=k(k.map(q.cache[l].normalized, function(a){return a[n].$row.get()}));if(""===e||u.filter_serversideFiltering)f.removeClass(u.filter_filteredRow).not("."+q.cssChildRow).show();else{f=f.not("."+q.cssChildRow);g=f.length;y=u.filter_searchFiltered;p=q.lastSearch||q.$table.data("lastSearch")||[];if(y)for(r=0;r<n+1;r++)s=a[r]||"",y||(r=n),y=y&&p.length&&0===s.indexOf(p[r]||"")&&!F.alreadyFiltered.test(s)&&!/[=\"\|!]/.test(s)&&!(/(>=?\s*-\d)/.test(s)||/(<=?\s*\d)/.test(s))&&!(""!==s&&q.$filters&&q.$filters.eq(r).find("select").length&& !q.$headers.filter('[data-column="'+r+'"]:last').hasClass("filter-match"));p=f.not("."+u.filter_filteredRow).length;y&&0===p&&(y=!1);q.debug&&c.log("Searching through "+(y&&p<g?p:"all")+" rows");if(u.filter_$anyMatch&&u.filter_$anyMatch.length||a[q.columns])C=u.filter_$anyMatch&&u.filter_$anyMatch.val()||a[q.columns]||"",q.sortLocaleCompare&&(C=c.replaceAccents(C)),G=C.toLowerCase();for(h=0;h<g;h++)if(s=f[h].className,!(F.child.test(s)||y&&F.filtered.test(s))){B=!0;s=f.eq(h).nextUntil("tr:not(."+ q.cssChildRow+")");r=s.length&&u.filter_childRows?s.text():"";r=u.filter_ignoreCase?r.toLocaleLowerCase():r;p=f.eq(h).children();C&&(H=p.map(function(a){E[a]?a=q.cache[l].normalized[h][a]:(a=u.filter_ignoreCase?k(this).text().toLowerCase():k(this).text(),q.sortLocaleCompare&&(a=c.replaceAccents(a)));return a}).get(),I=H.join(" "),J=I.toLowerCase(),M=q.cache[l].normalized[h].slice(0,-1).join(" "),A=null,k.each(c.filter.types,function(a,c){if(0>k.inArray(a,O)&&(x=c(C,G,I,J,M,N,b,u,E,H),null!==x))return A= x,!1}),B=null!==A?A:0<=(J+r).indexOf(G));for(n=0;n<N;n++)a[n]&&(d=q.cache[l].normalized[h][n],u.filter_useParsedData||E[n]?t=d:(t=k.trim(p.eq(n).text()),t=q.sortLocaleCompare?c.replaceAccents(t):t),v=!F.type.test(typeof t)&&u.filter_ignoreCase?t.toLocaleLowerCase():t,z=B,a[n]=q.sortLocaleCompare?c.replaceAccents(a[n]):a[n],w=u.filter_ignoreCase?(a[n]||"").toLocaleLowerCase():a[n],(D=c.getColumnData(b,u.filter_functions,n))?!0===D?z=q.$headers.filter('[data-column="'+n+'"]:last').hasClass("filter-match")? 0<=v.search(w):a[n]===t:"function"===typeof D?z=D(t,d,a[n],n,f.eq(h)):"function"===typeof D[a[n]]&&(z=D[a[n]](t,d,a[n],n,f.eq(h))):(A=null,k.each(c.filter.types,function(c,e){x=e(a[n],w,t,v,d,n,b,u,E);if(null!==x)return A=x,!1}),null!==A?z=A:(t=(v+r).indexOf(c.filter.parseFilter(b,w,n,E[n])),z=!u.filter_startsWith&&0<=t||u.filter_startsWith&&0===t)),B=z?B:!1);f.eq(h).toggle(B).toggleClass(u.filter_filteredRow,!B);s.length&&s.toggleClass(u.filter_filteredRow,!B)}}q.filteredRows+=f.not("."+u.filter_filteredRow).length; q.totalRows+=f.length;c.processTbody(b,m,!1)}q.lastCombinedFilter=e;q.lastSearch=a;q.$table.data("lastSearch",a);u.filter_saveFilters&&c.storage&&c.storage(b,"tablesorter-filters",a);q.debug&&c.benchmark("Completed filter widget search",L);u.filter_initialized&&q.$table.trigger("filterEnd",q);setTimeout(function(){q.$table.trigger("applyWidgets")},0)}},getOptionSource:function(b,a,e){var d,g=b.config,f=[],h=!1,l=g.widgetOptions.filter_selectSource,m=k.isFunction(l)?!0:c.getColumnData(b,l,a);!0=== m?h=l(b,a,e):"object"===k.type(l)&&m&&(h=m(b,a,e));!1===h&&(h=c.filter.getOptions(b,a,e));h=k.grep(h,function(a,b){return k.inArray(a,h)===b});g.$headers.filter('[data-column="'+a+'"]:last').hasClass("filter-select-nosort")||(k.each(h,function(c,d){f.push({t:d,p:g.parsers&&g.parsers[a].format(d,b,[],a)})}),d=g.textSorter||"",f.sort(function(e,f){var g=e.p.toString(),h=f.p.toString();return k.isFunction(d)?d(g,h,!0,a,b):"object"===typeof d&&d.hasOwnProperty(a)?d[a](g,h,!0,a,b):c.sortNatural?c.sortNatural(g, h):!0}),h=[],k.each(f,function(a,b){h.push(b.t)}));return h},getOptions:function(b,a,c){var d,g,f,h,l=b.config,m=l.widgetOptions,p=l.$table.children("tbody"),n=[];for(d=0;d<p.length;d++)if(!p.eq(d).hasClass(l.cssInfoBlock))for(h=l.cache[d],g=l.cache[d].normalized.length,b=0;b<g;b++)f=h.row?h.row[b]:h.normalized[b][l.columns].$row[0],c&&f.className.match(m.filter_filteredRow)||(m.filter_useParsedData||l.parsers[a].parsed||l.$headers.filter('[data-column="'+a+'"]:last').hasClass("filter-parsed")?n.push(""+ h.normalized[b][a]):(f=f.cells[a])&&n.push(k.trim(f.textContent||f.innerText||k(f).text())));return n},buildSelect:function(b,a,e,d){if(b.config.cache&&!k.isEmptyObject(b.config.cache)){a=parseInt(a,10);var g;g=b.config;var f=g.widgetOptions,h=g.$headers.filter('[data-column="'+a+'"]:last'),h='<option value="">'+(h.data("placeholder")||h.attr("data-placeholder")||f.filter_placeholder.select||"")+"</option>",l=c.filter.getOptionSource(b,a,d),m=g.$table.find("thead").find("select."+c.css.filter+'[data-column="'+ a+'"]').val();for(b=0;b<l.length;b++)d=l[b].replace(/\"/g,"&quot;"),h+=""!==l[b]?'<option value="'+d+'"'+(m===d?' selected="selected"':"")+">"+l[b]+"</option>":"";g=(g.$filters?g.$filters:g.$table.children("thead")).find("."+c.css.filter);f.filter_$externalFilters&&(g=g&&g.length?g.add(f.filter_$externalFilters):f.filter_$externalFilters);g.filter('select[data-column="'+a+'"]')[e?"html":"append"](h);f.filter_functions||(f.filter_functions={});f.filter_functions[a]=!0}},buildDefault:function(b,a){var e, d,g=b.config,f=g.widgetOptions,h=g.columns;for(e=0;e<h;e++)d=g.$headers.filter('[data-column="'+e+'"]:last'),!d.hasClass("filter-select")&&!0!==c.getColumnData(b,f.filter_functions,e)||d.hasClass("filter-false")||c.filter.buildSelect(b,e,a,d.hasClass(f.filter_onlyAvail))}};
c.getFilters=function(b,a,e,d){var g,f=!1,h=b?k(b)[0].config:"",l=h?h.widgetOptions:"";if(!0!==a&&l&&!l.filter_columnFilters)return k(b).data("lastSearch");if(h&&(h.$filters&&(g=h.$filters.find("."+c.css.filter)),l.filter_$externalFilters&& (g=g&&g.length?g.add(l.filter_$externalFilters):l.filter_$externalFilters),g&&g.length))for(f=e||[],b=0;b<h.columns+1;b++)a=g.filter('[data-column="'+(b===h.columns?"all":b)+'"]'),a.length&&(a=a.sort(function(a,b){return k(b).attr("data-lastSearchTime")-k(a).attr("data-lastSearchTime")}),k.isArray(e)?(d?a.slice(1):a).val(e[b]).trigger("change.tsfilter"):(f[b]=a.val()||"",a.slice(1).val(f[b])),b===h.columns&&a.length&&(l.filter_$anyMatch=a));0===f.length&&(f=!1);return f};
c.setFilters=function(b,a, e,d){var g=b?k(b)[0].config:"";b=c.getFilters(b,!0,a,d);g&&e&&(g.lastCombinedFilter=null,g.lastSearch=[],c.filter.searching(g.$table[0],a,d),g.$table.trigger("filterFomatterUpdate"));return!!b};
c.addWidget({id:"stickyHeaders",priority:60,options:{stickyHeaders:"",stickyHeaders_attachTo:null,stickyHeaders_offset:0,stickyHeaders_filteredToTop:!0,stickyHeaders_cloneId:"-sticky",stickyHeaders_addResizeEvent:!0,stickyHeaders_includeCaption:!0,stickyHeaders_zIndex:2},format:function(b,a,e){if(!(a.$table.hasClass("hasStickyHeaders")|| 0<=k.inArray("filter",a.widgets)&&!a.$table.hasClass("hasFilters"))){var d=a.$table,g=k(e.stickyHeaders_attachTo),f=d.children("thead:first"),h=g.length?g:k(window),l=f.children("tr").not(".sticky-false").children(),m="."+c.css.headerIn,p=d.find("tfoot"),n=isNaN(e.stickyHeaders_offset)?k(e.stickyHeaders_offset):"",s=g.length?0:n.length?n.height()||0:parseInt(e.stickyHeaders_offset,10)||0,r=e.$sticky=d.clone().addClass("containsStickyHeaders").css({position:g.length?"absolute":"fixed",margin:0,top:s, left:0,visibility:"hidden",zIndex:e.stickyHeaders_zIndex?e.stickyHeaders_zIndex:2}),t=r.children("thead:first").addClass(c.css.sticky+" "+e.stickyHeaders),v,w="",x=0,z="collapse"!==d.css("border-collapse")&&!/(webkit|msie)/i.test(navigator.userAgent),y=function(){s=n.length?n.height()||0:parseInt(e.stickyHeaders_offset,10)||0;x=0;z&&(x=2*parseInt(l.eq(0).css("border-left-width"),10));r.css({left:g.length?(parseInt(g.css("padding-left"),10)||0)+parseInt(a.$table.css("padding-left"),10)+parseInt(a.$table.css("margin-left"), 10)+parseInt(d.css("border-left-width"),10):f.offset().left-h.scrollLeft()-x,width:d.width()});v.filter(":visible").each(function(b){b=l.filter(":visible").eq(b);var c=z&&k(this).attr("data-column")===""+parseInt(a.columns/2,10)?1:0;k(this).css({width:b.width()-x}).find(m).width(b.find(m).width()-c)})};r.attr("id")&&(r[0].id+=e.stickyHeaders_cloneId);r.find("thead:gt(0), tr.sticky-false").hide();r.find("tbody, tfoot").remove();e.stickyHeaders_includeCaption?r.find("caption").css("margin-left","-1px"): r.find("caption").remove();v=t.children().children();r.css({height:0,width:0,padding:0,margin:0,border:0});v.find("."+c.css.resizer).remove();d.addClass("hasStickyHeaders").bind("pagerComplete.tsSticky",function(){y()});c.bindEvents(b,t.children().children(".tablesorter-header"));d.after(r);h.bind("scroll.tsSticky resize.tsSticky",function(a){if(d.is(":visible")){var b=d.offset(),c=e.stickyHeaders_includeCaption?0:d.find("caption").outerHeight(!0),c=(g.length?g.offset().top:h.scrollTop())+s-c,k=d.height()- (r.height()+(p.height()||0)),b=c>b.top&&c<b.top+k?"visible":"hidden",c={visibility:b};g.length?c.top=g.scrollTop():c.left=f.offset().left-h.scrollLeft()-x;r.removeClass("tablesorter-sticky-visible tablesorter-sticky-hidden").addClass("tablesorter-sticky-"+b).css(c);if(b!==w||"resize"===a.type)y(),w=b}});e.stickyHeaders_addResizeEvent&&c.addHeaderResizeEvent(b);d.hasClass("hasFilters")&&(d.bind("filterEnd",function(){var b=k(document.activeElement).closest("td"),b=b.parent().children().index(b);r.hasClass(c.css.stickyVis)&& e.stickyHeaders_filteredToTop&&(window.scrollTo(0,d.position().top),0<=b&&a.$filters&&a.$filters.eq(b).find("a, select, input").filter(":visible").focus())}),c.filter.bindSearch(d,v.find("."+c.css.filter)),e.filter_hideFilters&&c.filter.hideFilters(r,a));d.trigger("stickyHeadersInit")}},remove:function(b,a,e){a.$table.removeClass("hasStickyHeaders").unbind("pagerComplete.tsSticky").find("."+c.css.sticky).remove();e.$sticky&&e.$sticky.length&&e.$sticky.remove();k(".hasStickyHeaders").length||k(window).unbind("scroll.tsSticky resize.tsSticky"); c.addHeaderResizeEvent(b,!1)}});
c.addWidget({id:"resizable",priority:40,options:{resizable:!0,resizable_addLastColumn:!1,resizable_widths:[],resizable_throttle:!1},format:function(b,a,e){if(!a.$table.hasClass("hasResizable")){a.$table.addClass("hasResizable");c.resizableReset(b,!0);var d,g,f,h,l,m={},p=a.$table,n=0,s=null,r=null,t=20>Math.abs(p.parent().width()-p.width()),v=function(a){if(0!==n&&s){var b=a.pageX-n,c=s.width();s.width(c+b);s.width()!==c&&t&&r.width(r.width()-b);n=a.pageX}},w=function(){c.storage&& s&&r&&(m={},m[s.index()]=s.width(),m[r.index()]=r.width(),s.width(m[s.index()]),r.width(m[r.index()]),!1!==e.resizable&&c.storage(b,"tablesorter-resizable",a.$headers.map(function(){return k(this).width()}).get()));n=0;s=r=null;k(window).trigger("resize")};if(m=c.storage&&!1!==e.resizable?c.storage(b,"tablesorter-resizable"):{})for(h in m)!isNaN(h)&&h<a.$headers.length&&a.$headers.eq(h).width(m[h]);d=p.children("thead:first").children("tr");d.children().each(function(){var e;e=k(this);h=e.attr("data-column"); e="false"===c.getData(e,c.getColumnData(b,a.headers,h),"resizable");d.children().filter('[data-column="'+h+'"]')[e?"addClass":"removeClass"]("resizable-false")});d.each(function(){f=k(this).children().not(".resizable-false");k(this).find("."+c.css.wrapper).length||f.wrapInner('<div class="'+c.css.wrapper+'" style="position:relative;height:100%;width:100%"></div>');e.resizable_addLastColumn||(f=f.slice(0,-1));g=g?g.add(f):f});g.each(function(){var a=k(this),b=parseInt(a.css("padding-right"),10)+10; a.find("."+c.css.wrapper).append('<div class="'+c.css.resizer+'" style="cursor:w-resize;position:absolute;z-index:1;right:-'+b+'px;top:0;height:100%;width:20px;"></div>')}).find("."+c.css.resizer).bind("mousedown",function(b){s=k(b.target).closest("th");var c=a.$headers.filter('[data-column="'+s.attr("data-column")+'"]');1<c.length&&(s=s.add(c));r=b.shiftKey?s.parent().find("th").not(".resizable-false").filter(":last"):s.nextAll(":not(.resizable-false)").eq(0);n=b.pageX});k(document).bind("mousemove.tsresize", function(a){0!==n&&s&&(e.resizable_throttle?(clearTimeout(l),l=setTimeout(function(){v(a)},isNaN(e.resizable_throttle)?5:e.resizable_throttle)):v(a))}).bind("mouseup.tsresize",function(){w()});p.find("thead:first").bind("contextmenu.tsresize",function(){c.resizableReset(b);var a=k.isEmptyObject?k.isEmptyObject(m):!0;m={};return a})}},remove:function(b,a){a.$table.removeClass("hasResizable").children("thead").unbind("mouseup.tsresize mouseleave.tsresize contextmenu.tsresize").children("tr").children().unbind("mousemove.tsresize mouseup.tsresize").find("."+ c.css.resizer).remove();c.resizableReset(b)}});
c.resizableReset=function(b,a){k(b).each(function(){var e,d=this.config,g=d&&d.widgetOptions;b&&d&&(d.$headers.each(function(a){e=k(this);g.resizable_widths[a]?e.css("width",g.resizable_widths[a]):e.hasClass("resizable-false")||e.css("width","")}),c.storage&&!a&&c.storage(this,"tablesorter-resizable",{}))})};
c.addWidget({id:"saveSort",priority:20,options:{saveSort:!0},init:function(b,a,c,d){a.format(b,c,d,!0)},format:function(b,a,e,d){var g,f=a.$table; e=!1!==e.saveSort;var h={sortList:a.sortList};a.debug&&(g=new Date);f.hasClass("hasSaveSort")?e&&b.hasInitialized&&c.storage&&(c.storage(b,"tablesorter-savesort",h),a.debug&&c.benchmark("saveSort widget: Saving last sort: "+a.sortList,g)):(f.addClass("hasSaveSort"),h="",c.storage&&(h=(e=c.storage(b,"tablesorter-savesort"))&&e.hasOwnProperty("sortList")&&k.isArray(e.sortList)?e.sortList:"",a.debug&&c.benchmark('saveSort: Last sort loaded: "'+h+'"',g),f.bind("saveSortReset",function(a){a.stopPropagation(); c.storage(b,"tablesorter-savesort","")})),d&&h&&0<h.length?a.sortList=h:b.hasInitialized&&h&&0<h.length&&f.trigger("sorton",[h]))},remove:function(b){c.storage&&c.storage(b,"tablesorter-savesort","")}})
})(jQuery);

function getSociete(){
	var idsociete=$('input[type=radio][name=id_societe]:checked').val();
	if (typeof idsociete != 'undefined') {
		$.ajax({
			dataType: "html",
			type: "POST",
			url: url_get_societe,
			data: { idsociete : idsociete},
			async:false,
			cache: false,
			error: function(jqXHR,textStatus, errorThrown ) {
				alert(jqXHR.responseText); return false;
			},
			success: function(societe){
				$('#detailSociete').html(societe);
				$('#enregistrerSociete').click(function(){
					$("#loader").show();
					var datain = $('form[name="societe"]').serialize();
					$('#formulaireSociete').html("");
					$.ajax({
						dataType: "html",
						type: "POST",
						url: url_update_societe,
						data: datain, 
						success: function(dataout){
							$('#formulaireSociete').html(dataout);
						},
						complete: function(){
							$("#loader").hide();
						}
					});
				});
			}                                                                            
		});
	}
}

function getListeSociete(){
	$('#listeSocietes').html('');
	var direction = $('#societe_search_direction').val();
	var gestionnairePaie = $('#societe_search_gestionnairePaie').val();
	var gestionnaireContrat = $('#societe_search_gestionnaireContrat').val();
	var gestionnaireRH = $('#societe_search_gestionnaireRH').val();
	$("#loader").show();
	$.ajax({
		dataType: "html",
		type: "POST",
		url: url_get_liste_societe,
		data: { direction : direction, gestionnairePaie : gestionnairePaie, gestionnaireContrat : gestionnaireContrat, gestionnaireRH : gestionnaireRH },  
		async: false,
		cache: false,
		error: function(jqXHR,textStatus, errorThrown ) {
		},
		success: function(data){
			$('#listeSocietes').html(data);
			$("#listSociete").tablesorter({
				widthFixed : true,
				widgets:['filter']
			});
			$('.societe').click(function() {
				var selectedId = $(this).parent('tr').attr('id');
				$("#loader").show();
				$('input:radio').each(function(){
					if($(this).attr('value') != selectedId){
						$(this).prop('checked', false);
					}else{
						$(this).prop('checked', true);
					}
				});
				getSociete();
				$("#loader").hide();
			});
		},
		complete: function(){
			$("#loader").hide();
		}
	});
	return false;
}

$(document).ready(function() {
	
	getSociete();
	getListeSociete();
	
	 $('.widgetSearch').change(function(){
		 getListeSociete();
		 });
	
	 $('input[name=id_societe]').change(function(){
		getSociete();
	 });
});