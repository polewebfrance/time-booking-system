function prochaineJours(roomNb){
	$.ajax({
		type: "POST",
		url: url_prochaine_100_jours_chambre,
		data: { roomNb : parseInt(roomNb)},
		async:false,
		cache: false,
		error: function(jqXHR,textStatus, errorThrown ) {
			alert(jqXHR.responseText); return false;
		},
		success: function(data){
			if (data !== 'pas de Societe trouve!') {
				for(var k in data){
					if (data.hasOwnProperty(k)) {
						var modificare = data[k];
						for(var z in modificare){
							if (modificare.hasOwnProperty(z)) {
								$('#jour'+k).html(z);
								if(modificare[z]){
									$('#jour'+k).css('backgroundColor', '#F2D7FF');
								}else{
									$('#jour'+k).css('backgroundColor', '#BBFFD2');
								}
							}
						}
				   }
				}
			}
		},
		complete: function(){
			$("#loader").hide();
		}
	});
}
function dateDifference() {
	if($("#reservations_reservation_arrivee").val() == '' || $("#reservations_reservation_depart").val() == ''){
		return 0;
	}else{
		var nuitsNombre = ($("#reservations_reservation_depart").datepicker("getDate") -
					$("#reservations_reservation_arrivee").datepicker("getDate")) /
				   1000 / 60 / 60 / 24;
		return nuitsNombre;
	}
}
function clonerOuEffacerNuits(toClone){
	var line = $('.reservationNuits')[0].outerHTML;
//			var line = $('.reservationNuits').eq(0).clone();
	var nuits = $('.reservationNuits').length;
	var pattern = /_0/ig;
	for(var z=0;z<nuits;z++){
		$(".reservation tr:last").remove();
	}
	if(toClone > 0){
		for(var i=0;i<toClone;i++){
			var replacement = line.replace(pattern, "_"+i);
			$(".reservation").append(replacement);
		}
	}else{
		var replacement = line.replace(pattern, '_0');
		$(".reservation").append(replacement);
	}
}
/* utility functions */
function nationalDays(date) {
	var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();
	//console.log('Checking (raw): ' + m + '-' + d + '-' + y);
	for (i = 0; i < disabledDays.length; i++) {
//		if($.inArray((m+1) + '-' + d + '-' + y,disabledDays) != -1 || new Date() > date) {
		if($.inArray(d + '-' + (m+1) + '-' + y,disabledDays) != -1 || new Date() > date) {
			//console.log('bad:  ' + (m+1) + '-' + d + '-' + y + ' / ' + disabledDays[i]);
			return [false];
		}
	}
	//console.log('good:  ' + (m+1) + '-' + d + '-' + y);
	return [true];
}
function noWeekendsOrHolidays(date) {
	var noWeekend = jQuery.datepicker.noWeekends(date);
	return noWeekend[0] ? nationalDays(date) : noWeekend;
}
$(function () {	
	$('*[data-poload]').click(function() {
		var e=$(this);
//		e.off('hover');
		$.post(e.data('poload'),{journee : e.html()},function(data) {
			numero = 0;
			var reponse = ' : ';
			if (data !== 'pas de results trouve!') {
				for(var k in data){
					if (data.hasOwnProperty(k)) {
						numero++;
						reponse += data[k] + ', ';
					}
				}
				var reponse1 = 'Chambres disponibles ' + numero + reponse;
	//			alert(reponse1);
			}
			e.popover({content: reponse1}).popover('show');
			e.mouseout(function(){
				e.popover('hide');
			});
		});
	});
	
	prochaineJours($('#reservations_reservation_roomNb').val());
	$('#reservations_reservation_roomNb').change(function(){
		var roomNb = $(this).val();
		prochaineJours(roomNb);
	});

	$(".date" ).datepicker({
		altField: "#datepicker",
//		minDate:-30,
		showAnim:'slideDown',
		showWeek:true,
//		numberOfMonths: 2,
		closeText: 'Fermer',
		prevText: 'Précédent',
		nextText: 'Suivant',
		currentText: 'Aujourd\'hui',
		monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
		monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
		dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
		dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
		dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
		weekHeader: '',
		dateFormat: 'dd/mm/yy',
//		firstDay: 1,
//				minDate: new Date(2015, 10, 1),
//				maxDate: new Date(2015, 11, 31),
//				dateFormat: 'DD, MM, d, yy',
//				constrainInput: true,
		beforeShowDay: noWeekendsOrHolidays
	});
});

$(document).ready(function(){
	$("#reservations_reservation_arrivee").change(function(){
		toClone = dateDifference();
		clonerOuEffacerNuits(toClone);
	});
	$("#reservations_reservation_depart").change(function(){
		toClone = dateDifference();
		clonerOuEffacerNuits(toClone);
	});
});