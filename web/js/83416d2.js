		$(function () {
			$(".date" ).datepicker({
				altField: "#datepicker",
		        minDate:-30,
		        showAnim:'slideDown',
		        showWeek:true,
		        numberOfMonths: 2,
		        closeText: 'Fermer',
		        prevText: 'Précédent',
		        nextText: 'Suivant',
		        currentText: 'Aujourd\'hui',
		        monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
		        monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
		        dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
		        dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
		        dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
		        weekHeader: '',
		        dateFormat: 'dd/mm/yy',
		        firstDay: 1
			});
		});

function getArretDetails(idArret){
	$.ajax({
		type: "POST",
		url: url_get_arret_details,
		data: { idArret : parseInt(idArret)},
		async:false,
		cache: false,
		error: function(jqXHR,textStatus, errorThrown ) {
//			alert(jqXHR.responseText); return false;
		},
		success: function(data){
			if (data !== 'pas de Arret trouve!') {
				var html = '';
				for(var k in data){
					if (data.hasOwnProperty(k)) {
						$('#'+k).html(data[k]);
				   }
				}
			}
		},
		complete: function(){
			$("#loader").hide();
		}
	});
}

function ouvrirCalendar(){
	$(".date" ).removeClass('hasDatepicker').datepicker({
		altField: "#datepicker",
		minDate:-30,
		showAnim:'slideDown',
		showWeek:true,
		numberOfMonths: 2,
		closeText: 'Fermer',
		prevText: 'Précédent',
		nextText: 'Suivant',
		currentText: 'Aujourd\'hui',
		monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
		monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
		dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
		dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
		dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
		weekHeader: '',
		dateFormat: 'dd/mm/yy',
		firstDay: 1
	});
}
ouvrirCalendar();
$(function(){	
	$('.chosen').removeClass('chosen').chosen();
	$('#pourModifier').click(function(){
		$('#detailArret #saveAjouterEtModifier').show();
		$('#detailArret').find('#paie_arret_travail_nature').prop('disabled', false);
		$('#detailArret').find('#paie_arret_travail_dateDebut').prop('disabled', false);
		$('#detailArret').find('#paie_arret_travail_dateFin').prop('disabled', false);
		$('#detailArret').find('#paie_arret_travail_gestPaie').prop('disabled', false);
		$('#detailArret').find('#paie_arret_travail_gestRH').prop('disabled', false);
		$('#detailArret').find('#paie_arret_travail_gestContrat').prop('disabled', false);		
		$('#detailArret').find('#paie_arret_travail_dateReception').prop('disabled', false);
		$('#detailArret').find('#paie_arret_travail_typeReception').prop('disabled', false);
		$('#detailArret').find('#paie_arret_travail_commentaire').prop('disabled', false);
		$('#detailArret').find('#paie_arret_travail_categorie').prop('disabled', false);
		$('#detailArret').find('#paie_arret_travail_grossesse').prop('disabled', false);
		$(this).hide();
	});
	$('#saveAjouterEtModifier').click(function(){
		$("#loader").show();
		$.ajax({
			type: "POST",
			url: url_get_arret,
			data: $('form[name="paie_arret_travail"]').serialize(),
			success: function(arret){
				$('#detailArret').html(arret);
			}
		});
		$("#loader").hide();

	});
	$('#saveTraitement').click(function(){
		$("#loader").show();
		$.ajax({
			dataType: "html",
			type: "POST",
			url: url_traitement_arret,
			data: $('form[name="traiter_arret"]').serialize(),
			success: function(arret){
				$('#detailArret').html(arret);
			}
		});
		$("#loader").hide();

	});
	$('#PrendreEnCompte').click(function(){
		$("#loader").show();
		$.ajax({
			dataType: "html",
			type: "POST",
			url: url_prendre_compte_arret,
			data: $('form[name="paie_arret_travail"]').serialize(),
			success: function(arret){
				$('#detailArret').html(arret);
			}
		});
		$("#loader").hide();
	});
	if($('#detailArret').html() === '' &&
			errors === 0){
			$('#detailArret').html($('#ajouterArretModalBody').html());
			$('#ajouterArretModalBody').html('');
			$('#ajouterArret').modal('hide');
			getFilteredArrets();
	}
	$("#loader").hide();
});
$(document).ready(function() {
      
	$(function () {
		$('[data-toggle="tooltip"]').tooltip();
    });


	// On récupère la balise <div> en question qui contient l'attribut « data-prototype » qui nous intéresse.
	var $container = $('.div_piecejointes');
    
	// On ajoute des liens pour ajouter une nouvelle pièce jointe
	var $addLink = $('<a href="#" id="add_piecejointe" class="btn btn-default">Ajouter un document</a>');
	$container.append($addLink);
    
	// On ajoute un nouveau champ à chaque clic sur un lien d'ajout.
	$addLink.click(function(e) {
		addPieceJointe($container);
		e.preventDefault(); // évite qu'un # apparaisse dans l'URL
		return false;
	});

	// On définit un compteur unique pour nommer les champs qu'on va ajouter dynamiquement
	var index = $container.find(':input').length;
	
	// Pour chaque pièce jointe déjà existante, on ajoute un lien de suppression
	$container.children('div').each(function() {
		addDeleteLink($(this));
	});
    
	// La fonction qui ajoute un formulaire Categorie
	function addPieceJointe($container) {
	    // Dans le contenu de l'attribut « data-prototype », on remplace :
		// - le texte "__name__label__" qu'il contient par le label du champ
		// - le texte "__name__" qu'il contient par le numéro du champ
	    var $prototype = $($container.attr('data-prototype').replace(/__name__label__/g, '')
	    		.replace(/__name__/g, index++));
	    // On ajoute au prototype un lien pour pouvoir supprimer la pièce jointe
	    addDeleteLink($prototype);
	    // On ajoute les prototype modifié à la fin de leur balise <div>
	    $container.append($prototype);
	    // Enfin, on incrémente les compteurs pour que les prochains ajouts se fassent avec un autre numéro
	    index++;
	}

	// La fonction qui ajoute un lien de suppression d'une catégorie
	function addDeleteLink($prototype) {
		// Création du lien
	    $deleteLink = $('<a href="#" class="">Supprimer</a>');
	    // Ajout du lien
		$prototype.append($deleteLink);
		
		// Ajout du listener sur le clic du lien
		$deleteLink.click(function(e) {
			$prototype.remove();
		    e.preventDefault(); // évite qu'un # apparaisse dans l'URL
		    return false;
		});
    }
});
