
		function dateDifference() {
			if($("#hotel_booking_hotel_booking_dateArrivee").val() == '' || $("#hotel_booking_hotel_booking_dateDepart").val() == ''){
				return 0;
			}else{
				var nuitsNombre = ($("#hotel_booking_hotel_booking_dateDepart").datepicker("getDate") -
							$("#hotel_booking_hotel_booking_dateArrivee").datepicker("getDate")) /
						   1000 / 60 / 60 / 24;
				return nuitsNombre;
			}
		}
		function clonerOuEffacerNuits(toClone){
			var line = $('.reservationNuits')[0].outerHTML;
		//			var line = $('.reservationNuits').eq(0).clone();
			var nuits = $('.reservationNuits').length;
			var pattern = /_0/ig;
			var pattern2 = /no. 1/ig;
			for(var z=0;z<nuits;z++){
				$(".reservation tr:last").remove();
			}
			if(toClone > 0){
				for(var i=0;i<toClone;i++){
					var replacement = line.replace(pattern, "_"+i).replace(pattern2, "no. "+(i+1));
					$(".reservation").append(replacement);
				}
			}else{
				var replacement = line.replace(pattern, '_0');
				$(".reservation").append(replacement);
			}
		}
		function calculatePrix(numeroDeJours){
			var dinerSelected = 0;
			$('.reservationNuits :radio').each(function(){
				if($(this).is(':checked')){
					dinerSelected += parseInt($(this).val());
				}
			});
			$('#laValoire').html('<p class="text-success">' + numeroDeJours + ' nuits, dont '+dinerSelected+' avec dîner. Total: '+(53*numeroDeJours+6*dinerSelected) + '€</p>');
		}
		function updateService() {
			var direction=$("#hotel_booking_hotel_booking_direction").val();
			$.ajax({
				dataType: "json",
				type: "POST",
				url: url_update_service,
				data: { direction : direction},  
				async:false,
				cache: false,
//				error: function(jqXHR,textStatus, errorThrown ) {
//					alert(jqXHR.responseText); return false;
//				},
				success: function(directionServices){
					var servicesSelect = '';
					for(var k in directionServices){
						if (directionServices.hasOwnProperty(k)) {
							servicesSelect += '<option value="'+k+'">'+directionServices[k]+'</option>';
					   }				
					}
					$("#hotel_booking_hotel_booking_service").html(servicesSelect);
				}                                                                            
			});
		};
$(function(){
	$('.chosen').chosen();
	$(".date" ).datepicker({
		altField: "#datepicker",
//		minDate:-30,
		showAnim:'slideDown',
		showWeek:true,
//		numberOfMonths: 2,
		closeText: 'Fermer',
		prevText: 'Précédent',
		nextText: 'Suivant',
		currentText: 'Aujourd\'hui',
		monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
		monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
		dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
		dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
		dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
		weekHeader: '',
		dateFormat: 'dd/mm/yy'
//		firstDay: 1,
//				minDate: new Date(2015, 10, 1),
//				maxDate: new Date(2015, 11, 31),
//				dateFormat: 'DD, MM, d, yy',
//				constrainInput: true,
	});
});
		$(document).ready(function(){
			updateService();
			$('#hotel_booking_hotel_booking_direction').change(function(){
				updateService();
			});
			$("#hotel_booking_hotel_booking_dateArrivee, #hotel_booking_hotel_booking_dateDepart").change(function(){
				toClone = dateDifference();
				$('#hotel_booking_hotel_booking_nombreDeNuits').val(toClone);
				clonerOuEffacerNuits(toClone);
				calculatePrix(toClone);
			});
		});


