    var config = { 
      delay : 500, //Durée des animations.
    };

function DisplayFormulaire(typeFormation) {
	
	if(typeFormation==""){
	    //on cache les champs qui ne doivent apparaitre que sous condition
	    $('#descriptionDemande').hide(config.delay);    
	    $('#inscriptionDemande').hide(config.delay);    
	} else if (typeFormation==1) {    
	    $('#descriptionDemande').hide(config.delay);    
	    $('#inscriptionDemande').show(config.delay);    
	} else {
	    $('#descriptionDemande').show(config.delay);    
	    $('#inscriptionDemande').hide(config.delay);    
	}
}



$(document).ready(function() {
        
    $(':input[data-loading-text]').click(function () {
        var btn = $(this);
        btn.button('loading');
    });
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
        
    $(document).ready(function() {
    	$(".chosen-select").chosen({no_results_text: "Aucun résultat trouvé"});
    });
    
    //on cache les champs qui ne doivent apparaitre que sous condition
    DisplayFormulaire($('#efn_demandeformation_demande_type').val());
    
	$('#efn_demandeformation_demande_type').on("change", function() {
		 DisplayFormulaire($(this).val());
	});    
    
    
    
    
    // On récupère la balise <div> en question qui contient l'attribut « data-prototype » qui nous intéresse.
    var $container = $('div.demande_formation_participants');
    // On ajoute des liens pour ajouter un nouveau participant
    var $addLink = $('<a href="#" id="add_participant" class="btn btn-default">Ajouter un participant</a>');
    $container.append($addLink);
	// On ajoute un nouveau champ à chaque clic sur un lien d'ajout.
	$addLink.click(function(e) {
		addParticipant($container);
		e.preventDefault(); // évite qu'un # apparaisse dans l'URL
		return false;
	});
	// On définit un compteur unique pour nommer les champs qu'on va ajouter dynamiquement
	var index = $container.find(':input').length;
	// Pour chaque participant déjà existant, on ajoute un lien de suppression
	$container.children('fieldset').each(function() {
		addDeleteLink($(this));
	}); 
	// La fonction qui ajoute un formulaire Participant
	function addParticipant($container) {
	    // Dans le contenu de l'attribut « data-prototype », on remplace :
		// - le texte "__name__label__" qu'il contient par le label du champ
		// - le texte "__name__" qu'il contient par le numéro du champ
	    var $prototype = $($container.attr('data-prototype').replace(/__name__label__/g, '')
	    		.replace(/__name__/g, index++));
	    // On ajoute au prototype un lien pour pouvoir supprimer le participant
	    // On ajoute les prototype modifié à la fin de leur balise <div>
	    $container.append($prototype);
	    addDeleteLink($prototype);
	}
	// La fonction qui ajoute un lien de suppression d'une catégorie
	function addDeleteLink($prototype) {
		// Création du lien
	    $deleteLink = $('<a href="#" class="">Supprimer</a>');
	    // Ajout du lien
		$prototype.append($deleteLink);
		
		// Ajout du listener sur le clic du lien
		$deleteLink.click(function(e) {
			$prototype.remove();
		    e.preventDefault(); // évite qu'un # apparaisse dans l'URL
		    return false;
		});
    	$(".chosen-select").chosen({no_results_text: "Aucun résultat trouvé"});
    }	
	
  });