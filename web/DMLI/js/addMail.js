function upListDirection() {
    $("select#gestion_adminbundle_profil_direction option").remove();
    $.ajax({
        async: false,
        dataType: "json",
        type: "POST",
        url: "adddirection",
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
            $output = '<option value="-1"></option>';
        },
        success: function (html) {
            $("select#gestion_adminbundle_profil_direction").html(html);
        }
    });
}

$(document).ready(function () {

    $('#myform').bootstrapValidator({
        err: {
            container: '#messages'
        },
        fields: {


     "gestion_adminbundle_profil[direction]": {
                validators: {
                    notEmpty: {
                        message: 'Ce champ est obligatoire!'
                    },
                }
            },
            "gestion_adminbundle_profil[mailHolder]": {
                validators: {
                    notEmpty: {
                        message: 'Ce champ est obligatoire!'
                    },

                }
            },







        }
    });
    upListDirection();

});










