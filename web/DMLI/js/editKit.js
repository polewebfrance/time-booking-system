
function initDisplay() {

    var valueButtonHard = 0;
    var nb = 0;
    upListHardware();
    upPackSelect();
    hideInput();

}
function upListHardware() {
    $.ajax({
        async: false,
        dataType: "json",
        type: "POST",
        url: "../hardware",
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (dataJson) {
            $("#divHardwareList").append("<tr><th></th><th>libelle</th><th>prix/u</th></tr>");
            for (var y = 0; y < dataJson.length; y++) {
                if (dataJson[y].har_prix == null) {
                    $("#divHardwareList").append("<tr><td><input type=\"checkbox\" name=\"" + dataJson[y].har_libelle + "\" value=" + dataJson[y].har_id + " </td><td>" + dataJson[y].har_libelle + " </td><td> NA </td>");
                } else {
                        $("#divHardwareList").append("<tr><td><input type=\"checkbox\" name=\"" + dataJson[y].har_libelle + "\" value=" + dataJson[y].har_id + " </td><td>" + dataJson[y].har_libelle + " </td><td>   " + dataJson[y].har_prix + " &#8364 </td>");
                }
            }
            window.hardwareData = dataJson;
        }
    });
}
function upPackSelect() {
    uncheckCheckHardware();
    var str=window.location.href;
    var bdg=str.split("/");
    var pack = bdg[bdg.length-1];
    $.ajax({
        async: false,
        dataType: "json",
        type: "POST",
        url: "../listHardwareInPack",
        data: {pack: pack},
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (data) {
            $("#divHardwareSelected").empty();
            for (var y = 0; y < data.length; y++) {
                $('input:checkbox[name="' + data[y].har_libelle + '"]').prop('checked', true);
            }
            saveListCheckHardware();
            window.listHardwareForProfilData = data;
        }
    });
}

function saveListCheckHardware() {
    $("#divHardwareSelected").empty();
    var y = 0;
    $('#divHardwareList input:checked').each(function () {
        var idHardDB = $(this).val();
        var i = 0;
        $(window.hardwareData).each(function () { // window.hardwareData parcour le tableau dans son integralité
            if (window.hardwareData[i]['har_id'] == idHardDB) {
                $("#divHardwareSelected").append('<tr><td> ' + window.hardwareData[i]['har_libelle'] + '<input type="text" class="hardwareListeSelected" name="hardware[]" value="' + window.hardwareData[i]['har_id'] + '" </td><td> ' + window.hardwareData[i]['har_prix'] + ' &#8364 </td></tr>');
            }
            i++;
        });
        y++;
    });
}
function uncheckCheckHardware() {
    $('#divHardwareList input:checked').each(function () {
        $(this).prop('checked', false);
    });
}
function hideInput() {
    $('.hardwareListeSelected').hide();
    $('#loader').hide();

}


$(document).ready(function () {

    initDisplay();

    $('#selectHardwarePack').on("change", function () {
        upPackSelect();
        hideInput();
    });

    $('#buttonChangeHardwareSelected').on("click", function () {
        hideInput();
    });
    $('#buttonValidateHardwareList').on("click", function () {
        saveListCheckHardware();
        hideInput();
    });


});

function doNotSubmit() {
    var test =($('#divHardwareSelected tbody').length <= 0);
    if (test) {
        alert('Veuillez saisir un materiel pour former un pack');
        return false;
    }
}










