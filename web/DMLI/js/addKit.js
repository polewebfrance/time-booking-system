
function initDisplay() {

    var valueButtonHard = 0;

    var nb = 0;
    upListHardware();
    displayListHardware(valueButtonHard);
    hideInput();
    $('#box').hide();
    $('#buttonCloseHelp').hide();
}
function upListHardware() {
    $.ajax({
        async: false,
        dataType: "json",
        type: "POST",
        url: "hardware",
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (dataJson) {
            $("#divHardwareList").append("<tr><th></th><th>libelle</th><th>prix/u</th></tr>");
            for (var y = 0; y < dataJson.length; y++) {
                if (dataJson[y].har_prix == null) {
                    $("#divHardwareList").append("<tr><td><input type=\"checkbox\" name=\"" + dataJson[y].har_libelle + "\" value=" + dataJson[y].har_id + " </td><td>" + dataJson[y].har_libelle + " </td><td> NA </td>");
                } else {
                    if (dataJson[y].har_prix >= 20) {
                        $("#divHardwareList").append("<tr><td><input type=\"checkbox\" name=\"" + dataJson[y].har_libelle + "\" value=" + dataJson[y].har_id + " </td><td>" + dataJson[y].har_libelle + " </td><td>   " + dataJson[y].har_prix + " &#8364 </td>");
                    } else {
                        $("#divHardwareList").append("<tr><td><input type=\"checkbox\" name=\"" + dataJson[y].har_libelle + "\" value=" + dataJson[y].har_id + " </td><td>" + dataJson[y].har_libelle + " </td><td>   " + dataJson[y].har_prix + " &#8364 </td>");
                    }
                }
            }
            window.hardwareData = dataJson;
        }
    });
}


function saveListCheckHardware() {
    $("#divHardwareSelected").empty();
    var y = 0;
    $("#divHardwareSelected").append('<tr><td><strong>Nom de matériel </strong></td><td><strong>Prix</strong></td></tr>');
    $('#divHardwareList input:checked').each(function () {
        var idHardDB = $(this).val();
        var i = 0;
        $(window.hardwareData).each(function () { // window.hardwareData parcour le tableau dans son integralité
            $('#gestion_adminbundle_profil_Valider').removeAttr('disabled');
            if (window.hardwareData[i]['har_id'] == idHardDB) {
                $("#divHardwareSelected").append('<tr><td> ' + window.hardwareData[i]['har_libelle'] + '<input type="text" class="hardwareListeSelected" name="hardware[]" value="' + window.hardwareData[i]['har_id'] + '" </td><td> ' + window.hardwareData[i]['har_prix'] + ' &#8364 </td></tr>');
            }
            i++;
        });
        y++;
    });
}
function uncheckCheckHardware() {
    $('#divHardwareList input:checked').each(function () {
        $(this).prop('checked', false);
    });
}
function hideInput() {
    $('.hardwareListeSelected').hide();
    $('#loader').hide();
    $('#dialog-confirm').hide();
}
function displayListHardware(valueButton) {

    if (valueButton == 1) {
        $('#hardwareList').show();
    }
    else {
        $('#hardwareList').hide();
    }
}

$(document).ready(function () {

        $('#myform').bootstrapValidator({
            err: {
                container: '#messages'
            },
            fields: {

                "libelleKit": {
                    validators: {
                        notEmpty: {
                            message: 'Ce champ est obligatoire!'
                        },
                    }
                },
            }});

    initDisplay();

    $('#buttonChangeHardwareSelected').on("click", function () {
        displayListHardware(1);
        hideInput();
    });
    $('#buttonValidateHardwareList').on("click", function () {
        saveListCheckHardware();
        displayListHardware(0);
        hideInput();
    });


});
function doNotSubmit() {
    var test =($('#divHardwareSelected tbody').length <= 0);
    if (test) {
        alert('Veuillez saisir un materiel pour former un pack');
        return false;
    }
}











