var tab = [];
var tab1=[];
function initDisplay() {
    var valueButtonSoft = 0;
    var valueButtonHard = 0;
    var nb = 0;
    upListDirection();
    upListCompany();
    upListHardware();
    upListSoftware();
    upMail();
    displayCategorie();
    displayNewCompany();
    displayNewPost();
    displayDepartment();
    $('#divDivision').hide();
    displayPost(0, 0);
    $('#fieldsetCodeUser').hide();
    $('#spanPosteNonAffect').hide();
    displaySap();
    displayCrm();
    displayCognos();
    displayCrmFunctionAndCategory();
    displayListSoftware(valueButtonSoft);
    displayListHardware(valueButtonHard);
    $('#formulaire_dmlibundle_admdirection_remarque').show();
    $('#formulaire_dmlibundle_admdirection_remarque1').hide();
    $('#box').hide();
    $('#mat').hide();
    $('#buttonCloseHelp').hide();
}
function displayCategorie() {
    $('#mercato1').removeClass('active');
    $('#nouvel1').addClass('active');
    $('#userGen1').removeClass('active');
    $('#renouvellement1').removeClass('active');

    $('#categorie').text('Nouvel Arrivant');
    $('#nouvel').prop('checked', true);
    $('#mercato').prop('checked', false);
    $('#userGen').prop('checked', false);
    $('#renouvellement').prop('checked', false);
}
function displayNewCompany() {
    var newSociety = $('#formulaire_dmlibundle_admdirection_societe option:selected').val();
    if (newSociety == "-1") {
        $('#divNewCompany').show();
    }
    else {
        $('#divNewCompany').hide();
    }
}
function upListCompany() {
    $("select#formulaire_dmlibundle_admdirection_societe option").remove();
    $.ajax({
        dataType: "json",
        type: "POST",
        url: "remplirSociete",
        async: false,
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (html) {
            $("select#formulaire_dmlibundle_admdirection_societe").html(html);
        }
    });
}
function displayNewPost() {
    var newPost = $('#formulaire_dmlibundle_admdirection_poste option:selected').val();
    if (newPost == "-1") {
        $('#divNewPost').show();
    }
    else {
        $('#divNewPost').hide();
    }
}
function displayDepartment() {
    var direction = $('#formulaire_dmlibundle_admdirection_direction option:selected').val();
    if (direction != '') {
        $('#divDepartment').show();
        upListDepartment();
    }
    else {
        $('#divDepartment').hide();
        displayDivision();
    }
}
function displayDivision() {
    var service = $('#formulaire_dmlibundle_admdirection_service option:selected').val();
    if (service != '') {
        $('#divDivision').show();
        upListDivision();
    }
    else {
        $('#divDivision').hide();
        displayPost(0, 0);
    }
}
function displayPost(boolShow, hierarchyLevel) {
    if (boolShow == "1") {
        $('#divPost').show();
        upListPost(hierarchyLevel);
    }
    else {
        $('#divPost').hide();
    }
}
function upListDirection() {
    $("select#formulaire_dmlibundle_admdirection_direction option").remove();
    $.ajax({
        async: false,
        dataType: "json",
        type: "POST",
        url: "direction",
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
            $output = '<option value="-1"></option>';
        },
        success: function (html) {
            $("select#formulaire_dmlibundle_admdirection_direction").html(html);
        }
    });
}
function upListDepartment() {
    var direction = $('select#formulaire_dmlibundle_admdirection_direction option:selected').val();
    $("select#formulaire_dmlibundle_admdirection_service option").remove();
    

    if (direction != "0") {
        $.ajax({
            async: false,
            dataType: "json",
            type: "POST",
            url: "department",
            data: {direction: direction},
            cache: false,
            error: function (jqXHR, textStatus, errorThrown) {
                $('#divDepartment').hide();
                $('#divDivision').hide();
                $('#loader').hide();
            },
            success: function (html) {
                $("select#formulaire_dmlibundle_admdirection_service").html(html);
                displayPost(0, 0);
$('#loader').hide();
            }
        });
    }
}
function upListDivision() {
    var service = $('select#formulaire_dmlibundle_admdirection_service option:selected').val();
    $("select#formulaire_dmlibundle_admdirection_pole option").remove();
    if (service != "0") {
        $.ajax({
            async: false,
            dataType: "json",
            type: "POST",
            url: "division",
            data: {service: service},
            cache: false,
            error: function (jqXHR, textStatus, errorThrown) {
                $('#divDivision').hide();
                displayPost(1, 2);
            },
            success: function (html) {
                $("select#formulaire_dmlibundle_admdirection_pole").html(html);
            }
        });
    }
}
function upListPost($hierarchyLevel) {
    var idBridge = [];
    if ($hierarchyLevel == "2") {
        idBridge.push($('select#formulaire_dmlibundle_admdirection_direction option:selected').val()); // direction
        idBridge.push($('select#formulaire_dmlibundle_admdirection_service option:selected').val()); // service
        idBridge.push('NULL'); // pole
    }
    else if ($hierarchyLevel == "3") {
        idBridge.push($('select#formulaire_dmlibundle_admdirection_direction option:selected').val()); // direction
        idBridge.push($('select#formulaire_dmlibundle_admdirection_service option:selected').val()); // service
        idBridge.push($('select#formulaire_dmlibundle_admdirection_pole option:selected').val()); // pole

    }
    $("select#formulaire_dmlibundle_admdirection_poste option").remove();
    $.ajax({
        async: false,
        dataType: "json",
        type: "POST",
        url: "post",
        data: {idBridge: idBridge},
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (html) {
            $("select#formulaire_dmlibundle_admdirection_poste").html(html);
        }
    });
}
function displayNonAffectCheckbox() {
    var direction = $('select#formulaire_dmlibundle_admdirection_direction option:selected').val();
    if (direction == 1) {
        $('#spanPosteNonAffect').show();
    }
}
function getBdgId() {
    var idBridge = [];
    var divisionExist = 0; //var bool
    if ($('select#formulaire_dmlibundle_admdirection_pole option:selected').length >= 1) {
        divisionExist = 1;
    }
    if (divisionExist == 1) {
        idBridge.push($('select#formulaire_dmlibundle_admdirection_direction option:selected').val()); // direction
        idBridge.push($('select#formulaire_dmlibundle_admdirection_service option:selected').val()); // service
        idBridge.push($('select#formulaire_dmlibundle_admdirection_pole option:selected').val()); // pole
        idBridge.push($('select#formulaire_dmlibundle_admdirection_poste option:selected').val());// poste
    }
    else {
        idBridge.push($('select#formulaire_dmlibundle_admdirection_direction option:selected').val()); // direction
        idBridge.push($('select#formulaire_dmlibundle_admdirection_service option:selected').val()); // service
        idBridge.push('NULL'); // pole
        idBridge.push($('select#formulaire_dmlibundle_admdirection_poste option:selected').val());// poste
    }
    $.ajax({
        async: false,
        dataType: "json",
        type: "POST",
        url: "getBdgId",
        data: {idBridge: idBridge},
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (data) {
            $("input#idBdg").empty();
            window.bdgId = data;
            $("input#idBdg").html(data);
        }
    });

}
function displayAcheteur() {
    //mise a jour des champs
    getClePerso();
    upListCodeUserClePays();
    upListCodeUserCommentaire();
    //affichage des infos
    $('#divCodeUserKeyPerso').show();
    $('#divCodeUserKeyPays').show();
    $('#divCodeUserComment').show();
    $('#formulaire_dmlibundle_admdirection_selectCodeUserKeyPays').attr('novalidate', true);
    $('#formulaire_dmlibundle_admdirection_selectCodeUserComment').attr('novalidate', true);
    $('#divCodeUserPole').hide();
    $('#divCodeUserSSPole').hide();
}
function displayCP() {
    upListCodeUserSsPole();
    getClePerso();
    $('#divCodeUserKeyPerso').show();
    $('#divCodeUserSSPole').show();
    $('#formulaire_dmlibundle_admdirection_selectCodeUserSSPole').attr('novalidate', true);
    $('#divCodeUserTypeAchat').hide();
    $('#divCodeUserKeyPays').hide();
    $('#divCodeUserComment').hide();
}
function displayAsscoAssMarket() {
    $('#divCodeUserKeyPerso').show();
    getClePerso();
    $('#divCodeUserTypeAchat').hide();
    $('#divCodeUserKeyPays').hide();
    $('#divCodeUserComment').hide();
    $('#divCodeUserPole').hide();
    $('#divCodeUserSSPole').hide();
}
function getClePerso() {
    var pstId = $('select#formulaire_dmlibundle_admdirection_poste option:selected').val();
    $.ajax({
        async: false,
        dataType: "json",
        type: "POST",
        url: "dam_clePerso",
        data: {pstId: pstId},
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
            alert("error");
        },
        success: function (html) {
            $('#formulaire_dmlibundle_admdirection_inputCodeUserKeyPerso').val(html);
        }
    });
}
function displayFieldsetCodeUser() {
    var pstId = $("select#formulaire_dmlibundle_admdirection_poste option:selected").val();
    $.ajax({
        async: false,
        dataType: "json",
        type: "POST",
        url: "codeUser",
        data: {pstId: pstId},
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
            alert("error");
        },
        success: function (dataJson) {
            if (dataJson['pst_id'] != '') {
                $('#fieldsetCodeUser').show();
                //acheteur
                if (dataJson['dam_prs_id'] == 3) {
                    displayCP();
                }
                //cp
                else if (dataJson['dam_prs_id'] == 4 || dataJson['dam_prs_id'] == 2) {
                    displayAsscoAssMarket();
                }
                //ass co ou ass market
                else if (dataJson['dam_prs_id'] == 1) {
                    displayAcheteur();
                }
            }
            else {
                $('#fieldsetCodeUser').hide();
                $('#formulaire_dmlibundle_admdirection_selectCodeUserSSPole').val("");
                $('#formulaire_dmlibundle_admdirection_inputCodeUserKeyPerso').val("");
                $('#formulaire_dmlibundle_admdirection_selectCodeUserTypeAchat').val("");
                $('#formulaire_dmlibundle_admdirection_selectCodeUserKeyPays').val("");
                $('#formulaire_dmlibundle_admdirection_selectCodeUserComment').val("");
                $('#formulaire_dmlibundle_admdirection_selectCodeUserKeyPays').removeAttr('novalidate');
                $('#formulaire_dmlibundle_admdirection_selectCodeUserComment').removeAttr('novalidate');
                $('#formulaire_dmlibundle_admdirection_selectCodeUserSSPole').removeAttr('novalidate');
            }
        }
    });


}
function upListCodeUserClePays() {
    $.ajax({
        async: false,
        dataType: "json",
        type: "POST",
        url: "dam_clePays",
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
            alert("error");
        },
        success: function (html) {
            $("select#formulaire_dmlibundle_admdirection_selectCodeUserKeyPays").html(html);
        }
    });
}
function upListCodeUserCommentaire() {
    $.ajax({
        async: false,
        dataType: "json",
        type: "POST",
        url: "dam_commentaire",
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
            alert("error");
        },
        success: function (html) {
            $("select#formulaire_dmlibundle_admdirection_selectCodeUserComment").html(html);
        }
    });

}
function upListCodeUserSsPole() {
    var polId = $('select#formulaire_dmlibundle_admdirection_pole option:selected').val();
    $.ajax({
        async: false,
        dataType: "json",
        type: "POST",
        url: "dam_ssPole",
        data: {polId: polId},
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
            alert("error of -- upListCodeUserSsPole");
        },
        success: function (html) {
            $("select#formulaire_dmlibundle_admdirection_selectCodeUserSSPole").html(html);
        }
    });
}
function upListCrmProfil() {
    $("select#formulaire_dmlibundle_admdirection_selectCrmProfil option:selected").remove();
    $.ajax({
        async: false,
        dataType: "json",
        type: "POST",
        url: "crmProfil",
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {

        },
        success: function (html) {
            $("select#formulaire_dmlibundle_admdirection_selectCrmProfil").html(html);
        }
    });
}
function upListCrmFunction() {
    $("label#labelCrmFunction").empty();
    var profil = $('select#formulaire_dmlibundle_admdirection_selectCrmProfil option:selected').val();
    if (profil != "0") {
        $.ajax({
            async: false,
            dataType: "json",
            type: "POST",
            url: "crmFunction",
            data: {profil: profil},
            cache: false,
            error: function (jqXHR, textStatus, errorThrown) {
                $('#crmFunction').hide();
            },
            success: function (html) {
                $("span#spanCrmFunction").html(html);
            }
        });
    }
}
function upListCrmCategory() {
    var profil = $('select#formulaire_dmlibundle_admdirection_selectCrmProfil option:selected').val();
    if (profil != "0") {
        $.ajax({
            async: false,
            dataType: "json",
            type: "POST",
            url: "crmCategory",
            data: {profil: profil},
            cache: false,
            error: function (jqXHR, textStatus, errorThrown) {
                $('#crmCategory').hide();
            },
            success: function (html) {
                $("span#spanCrmCategory").html(html);
            }
        });
    }
    else {
        $('#crmFunction').hide();
        $('#crmCategory').hide();
        $('#crmTeam').hide();
    }
}
function upListCrmTeam() {
    var profil = $('select#formulaire_dmlibundle_admdirection_selectCrmProfil option:selected').val();
    var service = $("select#formulaire_dmlibundle_admdirection_service option:selected").val();
    $("select#formulaire_dmlibundle_admdirection_selectCrmTeam option").remove();
    if (profil != "0") {
        $.ajax({
            async: false,
            dataType: "json",
            type: "POST",
            url: "crmTeam",
            data: {service: service},
            cache: false,
            error: function (jqXHR, textStatus, errorThrown) {
                $('#crmTeam').hide();
            },
            success: function (html) {
                $("#formulaire_dmlibundle_admdirection_selectCrmTeam").html(html);
            }
        });
    }
    else {
        $('#crmFunction').hide();
        $('#crmCategory').hide();
        $('#crmTeam').hide();
    }
}
function upListHardware() {
    $.ajax({
        async: false,
        dataType: "json",
        type: "POST",
        url: "hardware",
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (dataJson) {
            $("#divHardwareList").append("<tr><th></th><th>libelle</th><th>prix/u</th><th>quantité</th></tr>");
            for (var y = 0; y < dataJson.length; y++) {
                if (dataJson[y].har_prix == null) {
                    $("#divHardwareList").append("<tr><td><input type=\"checkbox\" name=\"" + dataJson[y].har_libelle + "\" value=" + dataJson[y].har_id + " </td><td>" + dataJson[y].har_libelle + " </td><td> NA </td><td> <input class=\"form-control input-sm\" id=\"demo_vertical" + y + "\" type=\"text\" value=\"1\" name=\"demo_vertical2\"> </td>");
                } else {
                    if (dataJson[y].har_prix >= 20) {
                        $("#divHardwareList").append("<tr><td><input type=\"checkbox\" name=\"" + dataJson[y].har_libelle + "\" value=" + dataJson[y].har_id + " </td><td>" + dataJson[y].har_libelle + " </td><td>   " + dataJson[y].har_prix + " &#8364 </td><td> <input class=\"form-control input-sm\" id=\"demo_vertical" + y + "\"type=\"text\" value=\"1\" name=\"demo_vertical2\"> </td>");
                    } else {
                        $("#divHardwareList").append("<tr><td><input type=\"checkbox\" name=\"" + dataJson[y].har_libelle + "\" value=" + dataJson[y].har_id + " </td><td>" + dataJson[y].har_libelle + " </td><td>   " + dataJson[y].har_prix + " &#8364 </td><td> <input class=\"form-control input-sm\" id=\"demo_vertical" + y + "\"type=\"text\" value=\"1\" name=\"demo_vertical\"> </td>");
                    }
                }
            }
            window.hardwareData = dataJson;
        }
    });
}
function upListPackHardware() {
    $.ajax({
        async: false,
        dataType: "json",
        type: "POST",
        url: "packHardware",
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (html) {
            $("select#selectHardwarePack").html(html);
        }
    });
}
function upPackSelect() {
    uncheckCheckHardware();
    var pack = $('select#selectHardwarePack option:selected').val();
    $.ajax({
        async: false,
        dataType: "json",
        type: "POST",
        url: "listHardwareInPack",
        data: {pack: pack},
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (data) {
            $("#divHardwareSelected").empty();
            for (var y = 0; y < data.length; y++) {
                $('input:checkbox[name="' + data[y].har_libelle + '"]').prop('checked', true);
            }
            saveListCheckHardware();
            window.listHardwareForProfilData = data;
        }
    });
}
function upListHardwareForProfil(bdgId) {
    if(window.bdgId != null){
    var bdgId = window.bdgId[0]['bdg_id'];
    $.ajax({
        async: false,
        dataType: "json",
        type: "POST",
        url: "listeHardware",
        data: {bdgId: bdgId},
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
            $("#divHardwareSelected").empty();
        },
        success: function (data) {
            $("#divHardwareSelected").empty();
            for (var y = 0; y < data.length; y++) {
                $('input:checkbox[name="' + data[y].har_libelle + '"]').prop('checked', true);
            }
            saveListCheckHardware();
            window.listHardwareForProfilData = data;
        }
    });}
}
function saveListCheckHardware() {
    $("#divHardwareSelected").empty();
    var y = 0;
    $('#divHardwareList input:checked').each(function () {
        var idHardDB = $(this).val();
        var i = 0;
        $(window.hardwareData).each(function () { // window.hardwareData parcour le tableau dans son integralité
            $('#formulaire_dmlibundle_admdirection_Valider').removeAttr('disabled');
            if (window.hardwareData[i]['har_id'] == idHardDB) {
                $("#divHardwareSelected").append('<tr><td> ' + window.hardwareData[i]['har_libelle'] + '<input type="text" class="hardwareListeSelected" name="hardware[]" value="' + window.hardwareData[i]['har_id'] + '" </td><td> ' + window.hardwareData[i]['har_prix'] + ' &#8364 </td><td>' + $("#demo_vertical" + i).val() + '<input type="text" class="hardwareListeSelected" name="qte[]" value="' + $("#demo_vertical" + i).val() + '"/></td></tr>');
            }
            i++;
        });
        y++;
    });
}
function uncheckCheckHardware() {
    $('#divHardwareList input:checked').each(function () {
        $(this).prop('checked', false);
    });
}
function upListSoftware() {
    $.ajax({
        async: false,
        dataType: "json",
        type: "POST",
        url: "software",
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (dataJson) {
            $("#divSoftwareList").append("<tr><th></th><th>libelle</th><th>prix/u</th></tr>");
            for (var y = 0; y < dataJson.length; y++) {
                if (dataJson[y].sft_prix == null) {
                    $("#divSoftwareList").append("<tr><td><input type=\"checkbox\" name=\"" + dataJson[y].sft_libelle + "\" value=" + dataJson[y].sft_id + " </td><td>" + dataJson[y].sft_libelle + " </td><td>NA</td>");
                } else {
                    $("#divSoftwareList").append("<tr><td><input type=\"checkbox\" name=\"" + dataJson[y].sft_libelle + "\" value=" + dataJson[y].sft_id + " </td><td>" + dataJson[y].sft_libelle + " </td><td>   " + dataJson[y].sft_prix + " &#8364 </td>");
                }
            }
            window.softwareData = dataJson;
        }
    });
}
function upListSoftwareForProfil() {
    if(window.bdgId != null){
    var bdgId = window.bdgId[0]['bdg_id'];
    $.ajax({
        async: false,
        dataType: "json",
        type: "POST",
        url: "listeSoftware",
        data: {bdgId: bdgId},
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
            $("#divSoftwareSelected").empty();
        },
        success: function (data) {
            $("#divSoftwareSelected").empty();
            for (var y = 0; y < data.length; y++) {
                $('input:checkbox[name="' + data[y].sft_libelle + '"]').prop('checked', true);
            }
            saveListCheckSoftware();
            window.listSoftwareForProfilData = data;
        }
    });}
}
function saveListCheckSoftware() {
    $("#divSoftwareSelected").empty();
    var y = 0;
    $('#divSoftwareList input:checked').each(function () {
        var idSoftDB = $(this).val();
        var i = 0;
        $(window.softwareData).each(function () { // window.softwareData parcour le tableau dans son integralité
            $('#formulaire_dmlibundle_admdirection_Valider').removeAttr('disabled');
            if (window.softwareData[i]['sft_id'] == idSoftDB) {
                $("#divSoftwareSelected").append('<tr><td> ' + window.softwareData[i]['sft_libelle'] + '<input type="text" class="softwareListeSelected" name="software[]" value="' + window.softwareData[i]['sft_id'] + '" </td><td> ' + window.softwareData[i]['sft_prix'] + ' &#8364 </td></tr>');
            }
            i++;
        });
        y++;
    });
}
function uncheckCheckSoftware() {
    $('#divSoftwareList input:checked').each(function () {
        $(this).prop('checked', false);
    });
}
function hideInput() {
    $('.hardwareListeSelected').hide();
    $('.quantiteListeSelected').hide();
    $('.softwareListeSelected').hide();
    $('.inputCrm').hide();
    $('.inputListMail').hide();
    $('#loader').hide();
    $('#dialog-confirm').hide();
    $('.mailingListeSelected').hide();
}
function displaySap() {
    var check = $('#formulaire_dmlibundle_admdirection_sapProf:checked').val();
    if (check == 1) {
        $('fieldset#sap').prop('disabled', false);
        $('#formulaire_dmlibundle_admdirection_inputProfilSap').prop('disabled', false);
        $('#formulaire_dmlibundle_admdirection_sapUserGen').prop('disabled', false);
        $('#formulaire_dmlibundle_admdirection_textareaComment').prop('disabled', false);
        $('#formulaire_dmlibundle_admdirection_inputProfilSap').prop('novalidate', true);
        $('#formulaire_dmlibundle_admdirection_Valider').prop('disabled', false);
    }
    else {
        $('fieldset#sap').prop('disabled', true);
        $('#formulaire_dmlibundle_admdirection_inputProfilSap').prop('disabled', true);
        $('#formulaire_dmlibundle_admdirection_sapUserGen').prop('disabled', true);
        $('#formulaire_dmlibundle_admdirection_textareaComment').prop('disabled', true);
        $('#formulaire_dmlibundle_admdirection_inputProfilSap').prop('novalidate', false);
        $('#formulaire_dmlibundle_admdirection_Valider').prop('disabled', false);

    }
}
function displayCrm() {
    var check = $('#formulaire_dmlibundle_admdirection_crmProf:checked').val();
    if (check == 1) {
        $('fieldset#crm').prop('disabled', false);
        $('#formulaire_dmlibundle_admdirection_crmUserGen').prop('disabled', false);
        $('#formulaire_dmlibundle_admdirection_selectCrmProfil').prop('disabled', false);
        $('#formulaire_dmlibundle_admdirection_selectCrmTeam').prop('disabled', false);
        upListCrmProfil();
        displayCrmFunctionAndCategory();
        $('#formulaire_dmlibundle_admdirection_selectCrmProfil').prop('novalidate', true);
        $('#formulaire_dmlibundle_admdirection_selectCrmTeam').prop('novalidate', true);
        $('#formulaire_dmlibundle_admdirection_Valider').prop('disabled', false);
    }
    else {
        $('fieldset#crm').prop('disabled', true);
        $('#formulaire_dmlibundle_admdirection_crmUserGen').prop('disabled', true);
        $('#formulaire_dmlibundle_admdirection_selectCrmProfil').prop('disabled', true);
        $('#formulaire_dmlibundle_admdirection_selectCrmTeam').prop('disabled', true);
        $('#formulaire_dmlibundle_admdirection_selectCrmProfil').prop('novalidate', false);
        $('#formulaire_dmlibundle_admdirection_selectCrmTeam').prop('novalidate', false);
        $('#formulaire_dmlibundle_admdirection_Valider').prop('disabled', false);
    }
}
function displayCognos() {
    var check = $('#formulaire_dmlibundle_admdirection_cognosProf:checked').val();
    if (check == 1) {
        $('fieldset#cognos').prop('disabled', false);
        $('#formulaire_dmlibundle_admdirection_cognosProfil').prop('disabled', false);
        $('#formulaire_dmlibundle_admdirection_cognosComment').prop('disabled', false);
        $('#formulaire_dmlibundle_admdirection_cognosProfil').prop('novalidate', true);
        $('#formulaire_dmlibundle_admdirection_Valider').prop('disabled', false);
    }
    else {
        $('fieldset#cognos').prop('disabled', true);
        $('#formulaire_dmlibundle_admdirection_cognosProfil').prop('disabled', true);
        $('#formulaire_dmlibundle_admdirection_cognosComment').prop('disabled', true);
        $('#formulaire_dmlibundle_admdirection_cognosProfil').prop('novalidate', false);
        $('#formulaire_dmlibundle_admdirection_Valider').prop('disabled', false);
    }
}
function displayCrmFunctionAndCategory() {
    var profil = $('#formulaire_dmlibundle_admdirection_selectCrmProfil option:selected').val();

    if (profil != "") {
        $('#crmFunction').show();
        $('#crmCategory').show();
        $('#crmTeam').show();
    }
    else {
        $('#crmFunction').hide();
        $('#crmCategory').hide();
        $('#crmTeam').hide();
    }
}
function displayListHardware(valueButton) {

    if (valueButton == 1) {
        $('#softwareList').hide();
        $('#hardwareList').show();
    }
    else {
        $('#hardwareList').hide();
        $('#softwareList').hide();
    }
}
function displayListSoftware(valueButton) {

    if (valueButton == 1) {
        $('#hardwareList').hide();
        $('#softwareList').show();
    }
    else {
        $('#hardwareList').hide();
        $('#softwareList').hide();
    }
}
$(document).ready(function () {
    $(function () {
        $("input[name='demo_vertical2']").TouchSpin({
            verticalbuttons: true,
            verticalupclass: 'glyphicon glyphicon-plus',
            verticaldownclass: 'glyphicon glyphicon-minus',
            min: 1,
            max: 1
        });
        $('[data-toggle="popover"]').popover({html:true});
        $('#timepicker').timepicki({
            start_time: ["07", "00"],
            show_meridian: false,
            min_hour_value: 7,
            max_hour_value: 20,
            step_size_minutes: 15,
            overflow_minutes: true,
            increase_direction: 'up',
            reset: false,
        });
        $('#timepicker1').timepicki({
            start_time: ["20", "30"],
            show_meridian: false,
            min_hour_value: 7,
            max_hour_value: 20,
            step_size_minutes: 15,
            overflow_minutes: true,
            increase_direction: 'up',
            reset: false,
        });
        $("input[name='demo_vertical']").TouchSpin({
            verticalbuttons: true,
            verticalupclass: 'glyphicon glyphicon-plus',
            verticaldownclass: 'glyphicon glyphicon-minus',
            min: 1,
            max: 5
        });
        $('#inputDate').keypress(function(e) {
            return false;
        });

        $('#inputDate').datepicker({
            minDate: 0,

            closeText: 'Fermer',
            prevText: 'Précédent',
            nextText: 'Suivant',
            currentText: 'Aujourd\'hui',
            monthNames: ['janvier', 'février', 'mars', 'avril', 'mai', 'juin',
                'juillet', 'aout', 'septembre', 'octobre', 'novembre', 'décembre'],
            monthNamesShort: ['janv.', 'févr.', 'mars', 'avril', 'mai', 'juin',
                'juil.', 'aout', 'sept.', 'oct.', 'nov.', 'déc.'],
            dayNames: ['dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'],
            dayNamesShort: ['dim.', 'lun.', 'mar.', 'mer.', 'jeu.', 'ven.', 'sam.'],
            dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
            weekHeader: 'Sem.',
            dateFormat: 'dd-mm-yy',
            firstDay: 1,
            beforeShowDay: $.datepicker.noWeekends,
            onSelect: function () {
                var dateChoise = jQuery("#inputDate").datepicker('getDate');
                var today = new Date();
                var dayOfWeek = dateChoise.getUTCDay();
                var diffInDays = dateChoise.getDate() - today.getDate();
                if (diffInDays == 0) {
                    alert("Vous venez de choisir une date de livraison pour aujourd'hui. Il sera difficile de traiter votre demande si rapidement.");
                }
                else if ((diffInDays > 0) && (diffInDays < 5)) {
                    alert("Vous venez de choisir une date de livraison inférieure à 5 jours. On ne peut pas garantir le traitement de votre demande dans les délais  souhaités.");
                }
                $('#myform').bootstrapValidator('revalidateField', 'deliveryDate');

            },

        });
    });
   $('#myform').bootstrapValidator({
        err: {
            container: '#messages'
        },
        fields: {
            "formulaire_dmlibundle_admdirection[nom]": {
                validators: {
                    notEmpty: {
                        message: 'Ce champ est obligatoire!'
                    },
                    regexp: {
                        regexp: "^[a-zA-Z\u00C0-\u017F0-9-' ]{2,50}$",
                        message: 'Veuillez fournir un nom valide'
                    }
                }
            },
            "formulaire_dmlibundle_admdirection[matricule]": {
                validators: {
                    notEmpty: {
                        message: 'Ce champ est obligatoire!'
                    },
                    regexp: {
                        regexp: "^([0-9]|[0-9][0-9]|[0-9][0-9][0-9]|[0-9][0-9][0-9][0-9]|[0-9][0-9][0-9][0-9][0-9])$",
                        message: 'Matricule doit etre compris entre 1 et 99999'
                    }
                }
            },
            "formulaire_dmlibundle_admdirection[prenom]": {
                validators: {
                    notEmpty: {
                        message: 'Ce champ est obligatoire!'
                    },
                    regexp: {
                        regexp: "^[a-zA-Z\u00C0-\u017F0-9-' ]{2,50}$",
                        message: 'Veuillez fournir un prénom valide'
                    }
                }
            },
            "formulaire_dmlibundle_admdirection[remarque]": {
                validators: {
                    stringLength: {
                        max: 300,
                        message: 'le message ne dépasse pas 300 caractéres'
                    }
                }
            },
            "formulaire_dmlibundle_admdirection[societe]": {
                validators: {
                    notEmpty: {
                        message: 'Ce champ est obligatoire!'
                    },
                }
            },
            "formulaire_dmlibundle_admdirection[cognosProfil]": {
                validators: {
                    notEmpty: {
                        message: 'Ce champ est obligatoire!'
                    },
                    regexp: {
                        regexp: "^[a-zA-Z\u00C0-\u017F0-9-' ]{2,50}$",
                        message: 'Veuillez fournir un profil cognos valide'
                    }
                }
            },
            "formulaire_dmlibundle_admdirection[cognosComment]": {
                validators: {
                    stringLength: {
                        max: 200,
                        message: 'le message ne dépasse pas 200 caractéres'
                    }
                }
            },
            "userNewCompany": {
                validators: {
                    notEmpty: {
                        message: 'Ce champ est obligatoire!'
                    },
                    regexp: {
                        regexp: "^[a-zA-Z\u00C0-\u017F0-9-' ]{2,50}$",
                        message: 'Veuillez fournir une société valide'
                    }
                }
            },
            "inputNewDivision": {
                validators: {
                    notEmpty: {
                        message: 'Ce champ est obligatoire!'
                    },
                    regexp: {
                        regexp: "^[a-zA-Z\u00C0-\u017F0-9-' ]{2,50}$",
                        message: 'Veuillez fournir un pole valide'
                    }
                }
            },
            "userNewPost": {
                validators: {
                    notEmpty: {
                        message: 'Ce champ est obligatoire!'
                    },
                    regexp: {
                        regexp: "^[a-zA-Z\u00C0-\u017F0-9-' ]{2,50}$",
                        message: 'Veuillez fournir un poste valide'
                    }
                }
            },
            "formulaire_dmlibundle_admdirection[direction]": {
                validators: {
                    notEmpty: {
                        message: 'Ce champ est obligatoire!'
                    },
                }
            },
            "formulaire_dmlibundle_admdirection[selectCrmProfil]": {
                validators: {
                    notEmpty: {
                        message: 'Ce champ est obligatoire!'
                    },
                }
            },
            "formulaire_dmlibundle_admdirection[selectCrmTeam]": {
                validators: {
                    notEmpty: {
                        message: 'Ce champ est obligatoire!'
                    },
                }
            },
            "formulaire_dmlibundle_admdirection[service]": {
                validators: {
                    notEmpty: {
                        message: 'Ce champ est obligatoire!'
                    },
                }
            },
            "formulaire_dmlibundle_admdirection[pole]": {
                validators: {
                    notEmpty: {
                        message: 'Ce champ est obligatoire!'
                    },
                }
            },
            "formulaire_dmlibundle_admdirection[poste]": {
                validators: {
                    notEmpty: {
                        message: 'Ce champ est obligatoire!'
                    },
                }
            },
            "NewList": {
                validators: {
                    notEmpty: {
                        message: 'Veuillez fournir un email valide'
                    },
                    regexp: {
                        regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                        message: 'Veuillez fournir un email valide'
                    }
                }
            },
            "formulaire_dmlibundle_admdirection[inputProfilSap]": {
                validators: {
                    notEmpty: {
                        message: 'Ce champ est obligatoire!'
                    },
                    regexp: {
                        regexp: "^[a-zA-Z\u00C0-\u017F-_ ]{2,50}$",
                        message: 'Veuillez fournir un profil sap valide'
                    }
                }
            },
            "formulaire_dmlibundle_admdirection[textareaComment]": {
                validators: {
                    stringLength: {
                        max: 200,
                        message: 'le message ne dépasse pas 200 caractéres'
                    }
                }
            },


            "formulaire_dmlibundle_admdirection[selectCodeUserTypeAchat]": {
                validators: {
                    notEmpty: {
                        message: 'Ce champ est obligatoire!'
                    },
                }
            },
            "formulaire_dmlibundle_admdirection[selectCodeUserKeyPays]": {
                validators: {
                    notEmpty: {
                        message: 'Ce champ est obligatoire!'
                    },
                }
            },
            "formulaire_dmlibundle_admdirection[selectCodeUserComment]": {
                validators: {
                    notEmpty: {
                        message: 'Ce champ est obligatoire!'
                    },
                }
            },
            "deliveryDate": {
                validators: {
                    notEmpty: {
                        message: 'ce champ est obligatoire'
                    },
                    date: {
                        format: 'DD-MM-YYYY',
                        message: 'veuillez entrer une date valide'
                    }
                }
            }

        }
    });
    initDisplay();

    $('#formulaire_dmlibundle_admdirection_societe').on("change", function () {
        displayNewCompany();
        hideInput();
    });
    $('#formulaire_dmlibundle_admdirection_nom').on("change", function () {
        $('#myform').bootstrapValidator('revalidateField', 'formulaire_dmlibundle_admdirection[nom]');
        $('#formulaire_dmlibundle_admdirection_Valider').prop('disabled', false);
    });
    $(".chosen-select").chosen({disable_search_threshold: 10});
    $('#formulaire_dmlibundle_admdirection_matricule').on("change", function () {
        $('#myform').bootstrapValidator('revalidateField', 'formulaire_dmlibundle_admdirection[matricule]');
        $('#formulaire_dmlibundle_admdirection_Valider').prop('disabled', false);
    });
    $('#formulaire_dmlibundle_admdirection_prenom').on("change", function () {
        $('#myform').bootstrapValidator('revalidateField', 'formulaire_dmlibundle_admdirection[prenom]');
        $('#formulaire_dmlibundle_admdirection_Valider').prop('disabled', false);
    });
    $('#formulaire_dmlibundle_admdirection_inputProfilSap').on("change", function () {
        $('#myform').bootstrapValidator('revalidateField', 'formulaire_dmlibundle_admdirection[inputProfilSap]');
        $('#formulaire_dmlibundle_admdirection_Valider').prop('disabled', false);
    });
    $('#formulaire_dmlibundle_admdirection_cognosProfil').on("change", function () {
        $('#myform').bootstrapValidator('revalidateField', 'formulaire_dmlibundle_admdirection[cognosProfil]');
        $('#formulaire_dmlibundle_admdirection_Valider').prop('disabled', false);
    });
    $('#inputNewlist').on("change", function () {
        $('#myform').bootstrapValidator('revalidateField', 'NewList');
        $('#formulaire_dmlibundle_admdirection_Valider').prop('disabled', false);
    });
    $('#inputNewCompany').on("change", function () {
        $('#myform').bootstrapValidator('revalidateField', 'userNewCompany');
        $('#formulaire_dmlibundle_admdirection_Valider').prop('disabled', false);
    });
    $('#inputNewPost').on("change", function () {
        $('#myform').bootstrapValidator('revalidateField', 'userNewPost');
        $('#formulaire_dmlibundle_admdirection_Valider').prop('disabled', false);
    });
    $('#inputNewDivision').on("change", function () {
        $('#formulaire_dmlibundle_admdirection_Valider').prop('disabled', false);
        $('#myform').bootstrapValidator('revalidateField', 'inputNewDivision');
    });
    $('#formulaire_dmlibundle_admdirection_direction').on("change", function () {
$('#loader').show();       
	   displayDepartment();
        displayDivision();
        upListMailingDirection();
        hideInput();
		
    });
    $('#formulaire_dmlibundle_admdirection_service' + '').on("change", function () {
        displayDivision();
        hideInput();
        upListCrmTeam();
        $('#newDivision').hide();
        $('#inputNewDivision').hide();
    });
    $('#formulaire_dmlibundle_admdirection_pole').on("change", function () {
        displayPost(1, 3);
        hideInput();
        displayNonAffectCheckbox();

        upListCrmTeam();

        //---------------//
        //Cas spec DAM   //
        //---------------//
        //Cahmp autre dans les pole
        var service = $('select#formulaire_dmlibundle_admdirection_pole option:selected').val();

        $('#newDivision').hide();
        $('#inputNewDivision').hide();

        if (service == -1) {
            $('#formulaire_dmlibundle_admdirection_poste').prop('disabled', true); // selection poste
            $('#divNewPost').show();
            $('#labelPrecisePost').hide();
            $('#newDivision').show();
            $('#inputNewDivision').show();
        }
        else {
            $('#formulaire_dmlibundle_admdirection_poste').prop('disabled', false);
            $('#divNewPost').hide();
            $('#newDivision').hide();
            $('#inputNewDivision').hide();
        }
        //---------------//
        //Fin spec DAM   //
        //---------------//
    });
    $('#formulaire_dmlibundle_admdirection_sapProf').on("change", function () {
        displaySap();
        hideInput();
    });
    $('#formulaire_dmlibundle_admdirection_cognosProf').on("change", function () {
        displayCognos();
        hideInput();
    });
    $('#formulaire_dmlibundle_admdirection_crmProf').on("change", function () {
        displayCrm();
        hideInput();

    });
    $('#formulaire_dmlibundle_admdirection_selectCrmProfil').on("change", function () {
        displayCrmFunctionAndCategory();
        upListCrmFunction();
        upListCrmCategory();
        upListCrmTeam();
        hideInput();
    });
    $('#selectHardwarePack').on("change", function () {
        upPackSelect();
        hideInput();
    });
    $('#formulaire_dmlibundle_admdirection_poste').on("change", function () {
        displayNewPost();
        getBdgId();
        uncheckCheckMailing();
        uncheckCheckHardware();
        uncheckCheckSoftware();
        upListMailingForProfil();
        upListHardwareForProfil();
        upListSoftwareForProfil();
        displayNewPost();
        hideInput();
        displayFieldsetCodeUser();
    });
    $('#buttonChangeHardwareSelected').on("click", function () {
        displayListHardware(1);
        upListPackHardware();
        hideInput();
    });
    $('#buttonValidateHardwareList').on("click", function () {
        saveListCheckHardware();
        displayListHardware(0);
        hideInput();
    });
    $('#buttonChangeSoftwareSelected').on("click", function () {
        displayListSoftware(1);
        hideInput();
    });
    $('#buttonValidateSoftwareList').on("click", function () {
        saveListCheckSoftware();
        displayListSoftware(0);
        hideInput();
    });
    $('#formulaire_dmlibundle_admdirection_buttonAddListMail').on("click", function () {
        saveSelectListMail();
        hideInput();
    });
    $('#buttonAddNewListMail').on("click", function () {
        if ($('#inputNewlist').val() != '') {
            if (!(tab1.indexOf('<tr><td> ' + $('#inputNewlist').val() + '<input type="text" class="mailingListeSelected" name="mailingNew[]" value="' + $('#inputNewlist').val() + '" ></td></tr>') > -1)) {
                tab1.push('<tr><td> ' + $('#inputNewlist').val() + '<input type="text" class="mailingListeSelected" name="mailingNew[]" value="' + $('#inputNewlist').val() + '" ></td></tr>');
               // $("#MailNew").append('<tr><td> ' + $('#inputNewlist').val() + '<input type="text" class="mailingListeSelected" name="mailingNew[]" value="' + $('#inputNewlist').val() + '" ></td></tr>');
                hideInput();
            }
            hideInput();
        }

    });
	
    $('#buttonHelp').on("click", function () {
        $('#box').show();
        $('#buttonHelp').hide();
        $('#buttonCloseHelp').show();
    });
    $('#buttonCloseHelp').on("click", function () {
        $('#buttonHelp').show();
        $('#box').hide();
        $('#buttonCloseHelp').hide();
    });
    $('#buttonValidateMailingList').on("click", function () {
        saveListCheckMailing();
        hideInput();
    });
    $('#nouvel1').on("click", function () {
        $('#mercato1').removeClass('active');
        $('#nouvel1').addClass('active');
        $('#userGen1').removeClass('active');
        $('#renouvellement1').removeClass('active');
        $('#nouvel').prop('checked', true);
        $('#userGen').prop('checked', false);
        $('#mercato').prop('checked', false);
        $('#renouvellement').prop('checked', false);
        $('#divUserGen').show();
        $("#categorie").text("Nouvel Arrivant");
        $('#mat').hide();
        $('#listeMateriel').show();
        $('#listeLogiciel').show();
        $('#formulaire_dmlibundle_admdirection_matricule').prop('novalidate', false);;
    });
    $('#mercato1').on("click", function () {
        $('#mercato1').addClass('active');
        $('#nouvel1').removeClass('active');
        $('#userGen1').removeClass('active');
        $('#renouvellement1').removeClass('active');
        $('#userGen').prop('checked', false);
        $('#mercato').prop('checked', true);
        $('#nouvel').prop('checked', false);
        $('#renouvellement').prop('checked', false);
        $('#divUserGen').show();
        $("#categorie").text("Mercato");
        $('#mat').hide();
        $('#listeMateriel').show();
        $('#listeLogiciel').show();
        $('#formulaire_dmlibundle_admdirection_matricule').prop('novalidate', false);;
    });
    $('#userGen1').on("click", function () {
        $('#mercato1').removeClass('active');
        $('#nouvel1').removeClass('active');
        $('#userGen1').addClass('active');
        $('#renouvellement1').removeClass('active');
        $('#userGen').prop('checked', true);
        $('#nouvel').prop('checked', false);
        $('#mercato').prop('checked', false);
        $('#renouvellement').prop('checked', false);
        $('#divUserGen').hide();
        $('#formulaire_dmlibundle_admdirection_nom').prop('novalidate', false);
        $('#formulaire_dmlibundle_admdirection_prenom').prop('novalidate', false);
        $('#listeMateriel').hide();
        $('#listeLogiciel').hide();
        $("#categorie").text("Utilisateur Générique");
        $('#mat').hide();
        $('#formulaire_dmlibundle_admdirection_matricule').prop('novalidate', false);;
    });
    $('#renouvellement1').on("click", function () {
        $('#mercato1').removeClass('active');
        $('#nouvel1').removeClass('active');
        $('#userGen1').removeClass('active');
        $('#renouvellement1').addClass('active');
        $('#renouvellement').prop('checked', true);
        $('#mat').show();
        $('#formulaire_dmlibundle_admdirection_matricule').prop('novalidate', true);
        $('#userGen').prop('checked', false);
        $('#nouvel').prop('checked', false);
        $('#mercato').prop('checked', false);
        $('#divUserGen').show();
        $('#formulaire_dmlibundle_admdirection_nom').prop('novalidate', true);
        $('#formulaire_dmlibundle_admdirection_prenom').prop('novalidate', true);
        $('#listeMateriel').show();
        $('#listeLogiciel').show();
        $("#categorie").text("Renouvellement de Matériel");
    });
});
function upMail() {
    $.ajax({
        async: false,
        dataType: "json",
        type: "POST",
        url: "upmail",
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {

        },
        success: function (dataJson) {
            $("#divMailingList").append("<tr><th>Direction Achats Marketing</th></tr>");
            for (var y = 0; y < dataJson[0].length; y++) {

                $("#divMailingList").append("<tr><td><input type=\"checkbox\" name=\"" + dataJson[0][y].libelle + "\" value=" + dataJson[0][y].id + " </td><td>" + dataJson[0][y].libelle + " </td>");
            }

            $("#divMailingList").append("<tr><th>Direction des Ventes</th></tr>");
            for (var y = 0; y < dataJson[1].length; y++) {

                $("#divMailingList").append("<tr><td><input type=\"checkbox\" name=\"" + dataJson[1][y].libelle + "\" value=" + dataJson[1][y].id + " </td><td>" + dataJson[1][y].libelle + " </td>");
            }

            $("#divMailingList").append("<tr><th>Direction des Travaux</th></tr>");
            for (var y = 0; y < dataJson[2].length; y++) {

                $("#divMailingList").append("<tr><td><input type=\"checkbox\" name=\"" + dataJson[2][y].libelle + "\" value=" + dataJson[2][y].id + " </td><td>" + dataJson[2][y].libelle + " </td>");
            }

            $("#divMailingList").append("<tr><th>Expansion</th></tr>");
            for (var y = 0; y < dataJson[3].length; y++) {

                $("#divMailingList").append("<tr><td><input type=\"checkbox\" name=\"" + dataJson[3][y].libelle + "\" value=" + dataJson[3][y].id + " </td><td>" + dataJson[3][y].libelle + " </td>");
            }

            $("#divMailingList").append("<tr><th>Direction Logistique</th></tr>");
            for (var y = 0; y < dataJson[4].length; y++) {

                $("#divMailingList").append("<tr><td><input type=\"checkbox\" name=\"" + dataJson[4][y].libelle + "\" value=" + dataJson[4][y].id + " </td><td>" + dataJson[4][y].libelle + " </td>");
            }

            $("#divMailingList").append("<tr><th>Direction Support</th></tr>");
            for (var y = 0; y < dataJson[5].length; y++) {

                $("#divMailingList").append("<tr><td><input type=\"checkbox\" name=\"" + dataJson[5][y].libelle + "\" value=" + dataJson[5][y].id + " </td><td>" + dataJson[5][y].libelle + " </td>");
            }

            $("#divMailingList").append("<tr><th>Diversification</th></tr>");
            for (var y = 0; y < dataJson[6].length; y++) {

                $("#divMailingList").append("<tr><td><input type=\"checkbox\" name=\"" + dataJson[6][y].libelle + "\" value=" + dataJson[6][y].id + " </td><td>" + dataJson[6][y].libelle + " </td>");
            }

            $("#divMailingList").append("<tr><th>Autres listes</th></tr>");
            for (var y = 0; y < dataJson[7].length; y++) {

                $("#divMailingList").append("<tr><td><input type=\"checkbox\" name=\"" + dataJson[7][y].libelle + "\" value=" + dataJson[7][y].id + " </td><td>" + dataJson[7][y].libelle + " </td>");
            }


            window.MailingData = dataJson;
        }
    });
}
function saveListCheckMailing() {
    for (var x = 0; x < tab.length; x++) {
        delete tab[x];
        $("#MailNew tr").remove();
hideInput();
    }
    $("#divMailingSelected").empty();

    var y = 0; // le name="" des hardware

    $('#divMailingList input:checked').each(function () {
        var idHardDB = $(this).val();
        var j = 0;
        $(window.MailingData).each(function () { // window.hardwareData parcour le tableau dans son integralité
            $('#formulaire_dmlibundle_admdirection_Valider').removeAttr('disabled');
            for (var i = 0; i < window.MailingData[j].length; i++)
                if (window.MailingData[j][i].id == idHardDB) {
                    if (!(tab.indexOf('<tr><td> ' + window.MailingData[j][i].libelle + '<input type="text" class="mailingListeSelected" name="mailing[]" value="' + window.MailingData[j][i].id + '" </td></tr>') > -1)) {
                        tab.push('<tr><td> ' + window.MailingData[j][i].libelle + '<input type="text" class="mailingListeSelected" name="mailing[]" value="' + window.MailingData[j][i].id + '" </td></tr>');
                    }
                }
            j++;
        });
        y++;
    });
    for(var x= 0; x<tab1.length; x++){
        $("#divMailingSelected").append(tab1[x]);
        hideInput();
    }
    for (var x = 0; x < tab.length; x++) {
        $("#divMailingSelected").append(tab[x]);
        hideInput();
    }
}
function uncheckCheckMailing() {
    for (var x = 0; x < tab.length; x++) {
        delete tab[x];
        $("#MailNew tr").remove();
        hideInput();
    }
    for (var x = 0; x < tab1.length; x++) {
        delete tab1[x];

        $("#MailNew tr").remove();
        hideInput();
    }
    //pour chaque checkbox coché, réécriture de ligne
    $('#divMailingList input:checked').each(function () {

        $(this).prop('checked', false);

    });
}
function upListMailingDirection() {

    var direction = $('select#formulaire_dmlibundle_admdirection_direction option:selected').val();
    $("#divMailingList tr").remove();
    for (var x = 0; x < tab.length; x++) {
        delete tab[x];

        $("#MailNew tr").remove();
        hideInput();
    }
    for (var x = 0; x < tab1.length; x++) {
        delete tab1[x];

        $("#MailNew tr").remove();
        hideInput();
    }
    saveListCheckMailing();

    $("#divMailingList tr").remove();


    $.ajax({
        async: false,
        dataType: "json",
        type: "POST",
        url: "listeMailingDirection",
        data: {direction: direction},
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {

        },
        success: function (dataJson) {
            $("#divMailingList").append("<tr><th>" + dataJson[0][0].direction + "</th></tr>");
            for (var y = 1; y < dataJson[0].length; y++) {

                $("#divMailingList").append("<tr><td><input type=\"checkbox\" name=\"" + dataJson[0][y].libelle + "\" value=" + dataJson[0][y].id + " </td><td>" + dataJson[0][y].libelle + " </td>");
            }

            $("#divMailingList").append("<tr><th>" + dataJson[1][0].Autres + "</th></tr>");
            for (var y = 1; y < dataJson[1].length; y++) {

                $("#divMailingList").append("<tr><td><input type=\"checkbox\" name=\"" + dataJson[1][y].libelle + "\" value=" + dataJson[1][y].id + " </td><td>" + dataJson[1][y].libelle + " </td>");
            }

            window.MailingData = dataJson;
        }
    });


}
function upListMailingForProfil() {
if(window.bdgId != null){
    var idBridge = window.bdgId[0]['bdg_id'];
    for (var x = 0; x < tab1.length; x++) {
        delete tab1[x];

        $("#MailNew tr").remove();
        hideInput();
    }
    for (var x = 0; x < tab.length; x++) {
        delete tab[x];
        $("#MailNew tr").remove();
        hideInput();
    }


    $.ajax({
        async: false,
        dataType: "json",
        type: "POST",
        url: "listeMailingWithPost",
        data: {idBridge: idBridge},
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {

        },
        success: function (data) {
            $("#divMailingSelected").empty();

            for (var y = 0; y < data.length; y++) {
                $('input:checkbox[name="' + data[y].mel_libelle + '"]').prop('checked', true);
            }
            saveListCheckMailing();
        }
    });}
}
function doNotSubmit() {
    if ($("#userGen:checked").length == 1) {
        var testGen = (!($("#formulaire_dmlibundle_admdirection_remarque").val()) && ($("#formulaire_dmlibundle_admdirection_sapProf:checked").length == 0)) && ($("#formulaire_dmlibundle_admdirection_crmProf:checked").length == 0) && ($("#formulaire_dmlibundle_admdirection_cognosProf:checked").length == 0) && ($('#divMailingSelected tbody').length <= 0);
    } else {
        testGen = false;
    }
    if ($("#renouvellement:checked").length == 1) {
        var testRen = !($("#formulaire_dmlibundle_admdirection_remarque").val());
        if (testRen) {
            alert('Laissez une remarque');
            return false;
        }
    }
    var test = (!($("#formulaire_dmlibundle_admdirection_remarque").val()) && ($("#formulaire_dmlibundle_admdirection_sapProf:checked").length == 0)) && ($("#formulaire_dmlibundle_admdirection_crmProf:checked").length == 0) && ($("#formulaire_dmlibundle_admdirection_cognosProf:checked").length == 0) && ($('#divMailingSelected tbody').length <= 0) && ($('#divHardwareSelected tbody').length <= 0) && ($('#divSoftwareSelected tbody').length <= 0);
    var testSAP = (!($("#formulaire_dmlibundle_admdirection_inputProfilSap").val()) && ($("#formulaire_dmlibundle_admdirection_sapProf:checked").length == 1));
    var testCRM = ((!($("#formulaire_dmlibundle_admdirection_selectCrmTeam").val()) || !($("#formulaire_dmlibundle_admdirection_selectCrmProfil").val()) ) && ($("#formulaire_dmlibundle_admdirection_crmProf:checked").length == 1));
    var testCognos = (!($("#formulaire_dmlibundle_admdirection_cognosProfil").val()) && ($("#formulaire_dmlibundle_admdirection_cognosProf:checked").length == 1));
    if (test || testGen) {
        alert('Veuillez saisir un profil SAP/CRM/COGNOS ou une remarque ou un groupe outlook ou un materiel ou un logiciel');

        return false;
    }
    if (testSAP || testCRM || testCognos) {
        alert('Veuillez saisir un profil SAP/CRM/COGNOS');
        $('#myform').bootstrapValidator('revalidateField', 'formulaire_dmlibundle_admdirection[inputProfilSap]');
        $('#myform').bootstrapValidator('revalidateField', 'formulaire_dmlibundle_admdirection[textareaComment]');
        $('#myform').bootstrapValidator('revalidateField', 'formulaire_dmlibundle_admdirection[selectCrmProfil]');
        $('#myform').bootstrapValidator('revalidateField', 'formulaire_dmlibundle_admdirection[selectCrmTeam]');
        $('#myform').bootstrapValidator('revalidateField', 'formulaire_dmlibundle_admdirection[cognosProfil]');
        $('#myform').bootstrapValidator('revalidateField', 'formulaire_dmlibundle_admdirection[cognosComment]');
        return false;
    }

}






