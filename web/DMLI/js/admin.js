
function initDisplay() {
    $('#loading').show();
    var valueButtonSoft = 0;
    var valueButtonHard = 0;
    var nb = 0;
    upListHardware();
    upListSoftware();
    upListPackHardware();
    upListHardwareForProfil();
    upListSoftwareForProfil();
    upListMailingDirection();
    upListMailingForProfil();
    displayListSoftware(valueButtonSoft);
    displayListHardware(valueButtonHard);
    hideInput();
	    $('#loading').hide();
    $('#affectProfil').show();

    $('#box').hide();
    $('#buttonCloseHelp').hide();
}
function getBdgId() {

    $.ajax({
        async: false,
        dataType: "json",
        type: "GET",
        url: "../getBdgId",
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (data) {
            $("input#idBdg").empty();
            window.bdgId = data;
            $("input#idBdg").html(data);
        }
    });

}
function upListHardware() {
    $.ajax({
        async: false,
        dataType: "json",
        type: "POST",
        url: "../hardware",
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (dataJson) {
            $("#divHardwareList").append("<tr><th></th><th>libelle</th><th>prix/u</th></tr>");
            for (var y = 0; y < dataJson.length; y++) {
                if (dataJson[y].har_prix == null) {
                    $("#divHardwareList").append("<tr><td><input type=\"checkbox\" name=\"" + dataJson[y].har_libelle + "\" value=" + dataJson[y].har_id + " </td><td>" + dataJson[y].har_libelle + " </td><td> NA </td>");
                } else {
                    if (dataJson[y].har_prix >= 20) {
                        $("#divHardwareList").append("<tr><td><input type=\"checkbox\" name=\"" + dataJson[y].har_libelle + "\" value=" + dataJson[y].har_id + " </td><td>" + dataJson[y].har_libelle + " </td><td>   " + dataJson[y].har_prix + " &#8364 </td>");
                    } else {
                        $("#divHardwareList").append("<tr><td><input type=\"checkbox\" name=\"" + dataJson[y].har_libelle + "\" value=" + dataJson[y].har_id + " </td><td>" + dataJson[y].har_libelle + " </td><td>   " + dataJson[y].har_prix + " &#8364 </td>");
                    }
                }
            }
            window.hardwareData = dataJson;
        }
    });
}
function upListPackHardware() {
    $.ajax({
        async: false,
        dataType: "json",
        type: "POST",
        url: "../packHardware",
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (html) {
            $("select#selectHardwarePack").html(html);
        }
    });
}
function upPackSelect() {
    uncheckCheckHardware();
    var pack = $('select#selectHardwarePack option:selected').val();
    $.ajax({
        async: false,
        dataType: "json",
        type: "POST",
        url: "../listHardwareInPack",
        data: {pack: pack},
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (data) {
            $("#divHardwareSelected").empty();
            for (var y = 0; y < data.length; y++) {
                $('input:checkbox[name="' + data[y].har_libelle + '"]').prop('checked', true);
            }
            saveListCheckHardware();
            window.listHardwareForProfilData = data;
        }
    });
}
function upListHardwareForProfil() {
    var str=window.location.href;
    var bdg=str.split("/");
    var bdgId = bdg[bdg.length-1];
    $.ajax({
        async: false,
        dataType: "json",
        type: "POST",
        url: "../listeHardware",
        data: {bdgId: bdgId},
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
            $("#divHardwareSelected").empty();
        },
        success: function (data) {
            $("#divHardwareSelected").empty();
            for (var y = 0; y < data.length; y++) {
                $('input:checkbox[name="' + data[y].har_libelle + '"]').prop('checked', true);
            }
            saveListCheckHardware();
            window.listHardwareForProfilData = data;
        }
    });
}
function saveListCheckHardware() {
    $("#divHardwareSelected").empty();
    var y = 0;
    $('#divHardwareList input:checked').each(function () {
        var idHardDB = $(this).val();
        var i = 0;
        $(window.hardwareData).each(function () { // window.hardwareData parcour le tableau dans son integralité
            $('#gestion_adminbundle_profil_Valider').removeAttr('disabled');
            if (window.hardwareData[i]['har_id'] == idHardDB) {
                $("#divHardwareSelected").append('<tr><td> ' + window.hardwareData[i]['har_libelle'] + '<input type="text" class="hardwareListeSelected" name="hardware[]" value="' + window.hardwareData[i]['har_id'] + '" </td><td> ' + window.hardwareData[i]['har_prix'] + ' &#8364 </td></tr>');
            }
            i++;
        });
        y++;
    });
}
function uncheckCheckHardware() {
    $('#divHardwareList input:checked').each(function () {
        $(this).prop('checked', false);
    });
}
function upListSoftware() {
    $.ajax({
        async: false,
        dataType: "json",
        type: "POST",
        url: "../software",
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (dataJson) {
            $("#divSoftwareList").append("<tr><th></th><th>libelle</th><th>prix/u</th></tr>");
            for (var y = 0; y < dataJson.length; y++) {
                if (dataJson[y].sft_prix == null) {
                    $("#divSoftwareList").append("<tr><td><input type=\"checkbox\" name=\"" + dataJson[y].sft_libelle + "\" value=" + dataJson[y].sft_id + " </td><td>" + dataJson[y].sft_libelle + " </td><td>NA</td>");
                } else {
                    $("#divSoftwareList").append("<tr><td><input type=\"checkbox\" name=\"" + dataJson[y].sft_libelle + "\" value=" + dataJson[y].sft_id + " </td><td>" + dataJson[y].sft_libelle + " </td><td>   " + dataJson[y].sft_prix + " &#8364 </td>");
                }
            }
            window.softwareData = dataJson;
        }
    });
}
function upListSoftwareForProfil() {
    var str=window.location.href;
    var bdg=str.split("/");

    var bdgId = bdg[bdg.length-1];
    $.ajax({
        async: false,
        dataType: "json",
        type: "POST",
        url: "../listeSoftware",
        data: {bdgId: bdgId},
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
            $("#divSoftwareSelected").empty();
        },
        success: function (data) {
            $("#divSoftwareSelected").empty();
            for (var y = 0; y < data.length; y++) {
                $('input:checkbox[name="' + data[y].sft_libelle + '"]').prop('checked', true);
            }
            saveListCheckSoftware();
            window.listSoftwareForProfilData = data;
        }
    });
}
function saveListCheckSoftware() {
    $("#divSoftwareSelected").empty();
    var y = 0;
    $('#divSoftwareList input:checked').each(function () {
        var idSoftDB = $(this).val();
        var i = 0;
        $(window.softwareData).each(function () {
            $('#gestion_adminbundle_profil_Valider').removeAttr('disabled');
            if (window.softwareData[i]['sft_id'] == idSoftDB) {
                $("#divSoftwareSelected").append('<tr><td> ' + window.softwareData[i]['sft_libelle'] + '<input type="text" class="softwareListeSelected" name="software[]" value="' + window.softwareData[i]['sft_id'] + '" </td><td> ' + window.softwareData[i]['sft_prix'] + ' &#8364 </td></tr>');
            }
            i++;
        });
        y++;
    });
}
function uncheckCheckSoftware() {
    $('#divSoftwareList input:checked').each(function () {
        $(this).prop('checked', false);
    });
}
function hideInput() {
    $('.hardwareListeSelected').hide();
    $('.softwareListeSelected').hide();
    $('.inputListMail').hide();
    $('#loader').hide();
    $('#dialog-confirm').hide();
    $('.mailingListeSelected').hide();
}
function displayListHardware(valueButton) {

    if (valueButton == 1) {
        $('#softwareList').hide();
        $('#hardwareList').show();
    }
    else {
        $('#hardwareList').hide();
        $('#softwareList').hide();
    }
}
function displayListSoftware(valueButton) {

    if (valueButton == 1) {
        $('#hardwareList').hide();
        $('#softwareList').show();
    }
    else {
        $('#hardwareList').hide();
        $('#softwareList').hide();
    }
}
$(document).ready(function () {
    $(function () {
        $('[data-toggle="popover"]').popover();
        $('#idBdg').hide();
    });
    initDisplay();
    $('#selectHardwarePack').on("change", function () {
        upPackSelect();
        hideInput();
    });

    $('#buttonChangeHardwareSelected').on("click", function () {
        displayListHardware(1);
        upListPackHardware();
        hideInput();
    });
    $('#buttonValidateHardwareList').on("click", function () {
        saveListCheckHardware();
        displayListHardware(0);
        hideInput();
    });
    $('#buttonChangeSoftwareSelected').on("click", function () {
        displayListSoftware(1);
        hideInput();
    });
    $('#buttonValidateSoftwareList').on("click", function () {
        saveListCheckSoftware();
        displayListSoftware(0);
        hideInput();
    });
    $('#gestion_adminbundle_profil_buttonAddListMail').on("click", function () {
        saveSelectListMail();
        hideInput();
    });
    $('#buttonHelp').on("click", function () {
        $('#box').show();
        $('#buttonHelp').hide();
        $('#buttonCloseHelp').show();
    });
    $('#buttonCloseHelp').on("click", function () {
        $('#buttonHelp').show();
        $('#box').hide();
        $('#buttonCloseHelp').hide();
    });
    $('#buttonValidateMailingList').on("click", function () {
        saveListCheckMailing();
        hideInput();
    });
});
function upMail() {
    $.ajax({
        async: false,
        dataType: "json",
        type: "POST",
        url: "../upmail",
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {

        },
        success: function (dataJson) {
            $("#divMailingList").append("<tr><th>Direction Achats Marketing</th></tr>");
            for (var y = 0; y < dataJson[0].length; y++) {

                $("#divMailingList").append("<tr><td><input type=\"checkbox\" name=\"" + dataJson[0][y].libelle + "\" value=" + dataJson[0][y].id + " </td><td>" + dataJson[0][y].libelle + " </td>");
            }

            $("#divMailingList").append("<tr><th>Direction des Ventes</th></tr>");
            for (var y = 0; y < dataJson[1].length; y++) {

                $("#divMailingList").append("<tr><td><input type=\"checkbox\" name=\"" + dataJson[1][y].libelle + "\" value=" + dataJson[1][y].id + " </td><td>" + dataJson[1][y].libelle + " </td>");
            }

            $("#divMailingList").append("<tr><th>Direction des Travaux</th></tr>");
            for (var y = 0; y < dataJson[2].length; y++) {

                $("#divMailingList").append("<tr><td><input type=\"checkbox\" name=\"" + dataJson[2][y].libelle + "\" value=" + dataJson[2][y].id + " </td><td>" + dataJson[2][y].libelle + " </td>");
            }

            $("#divMailingList").append("<tr><th>Expansion</th></tr>");
            for (var y = 0; y < dataJson[3].length; y++) {

                $("#divMailingList").append("<tr><td><input type=\"checkbox\" name=\"" + dataJson[3][y].libelle + "\" value=" + dataJson[3][y].id + " </td><td>" + dataJson[3][y].libelle + " </td>");
            }

            $("#divMailingList").append("<tr><th>Direction Logistique</th></tr>");
            for (var y = 0; y < dataJson[4].length; y++) {

                $("#divMailingList").append("<tr><td><input type=\"checkbox\" name=\"" + dataJson[4][y].libelle + "\" value=" + dataJson[4][y].id + " </td><td>" + dataJson[4][y].libelle + " </td>");
            }

            $("#divMailingList").append("<tr><th>Direction Support</th></tr>");
            for (var y = 0; y < dataJson[5].length; y++) {

                $("#divMailingList").append("<tr><td><input type=\"checkbox\" name=\"" + dataJson[5][y].libelle + "\" value=" + dataJson[5][y].id + " </td><td>" + dataJson[5][y].libelle + " </td>");
            }

            $("#divMailingList").append("<tr><th>Diversification</th></tr>");
            for (var y = 0; y < dataJson[6].length; y++) {

                $("#divMailingList").append("<tr><td><input type=\"checkbox\" name=\"" + dataJson[6][y].libelle + "\" value=" + dataJson[6][y].id + " </td><td>" + dataJson[6][y].libelle + " </td>");
            }

            $("#divMailingList").append("<tr><th>Autres listes</th></tr>");
            for (var y = 0; y < dataJson[7].length; y++) {

                $("#divMailingList").append("<tr><td><input type=\"checkbox\" name=\"" + dataJson[7][y].libelle + "\" value=" + dataJson[7][y].id + " </td><td>" + dataJson[7][y].libelle + " </td>");
            }


            window.MailingData = dataJson;
        }
    });
}
function saveListCheckMailing() {

    $("#divMailingSelected").empty();

    var y = 0;

    $('#divMailingList input:checked').each(function () {
        var idHardDB = $(this).val();
        var j = 0;
        $(window.MailingData).each(function () {
            $('#gestion_adminbundle_profil_Valider').removeAttr('disabled');
            for (var i = 0; i < window.MailingData[j].length; i++)
                if (window.MailingData[j][i].id == idHardDB) {
                        $("#divMailingSelected").append('<tr><td> ' + window.MailingData[j][i].libelle + '<input type="text" class="mailingListeSelected" name="mailing[]" value="' + window.MailingData[j][i].id + '" </td></tr>');
                    hideInput();
                }
            j++;
        });
        y++;
    });
}
function uncheckCheckMailing() {

    $('#divMailingList input:checked').each(function () {

        $(this).prop('checked', false);

    });
}
function upListMailingDirection() {
    var str=window.location.href;
    var bdg=str.split("/");

    var direction = bdg[bdg.length-1];
    $("#divMailingList tr").remove();
    saveListCheckMailing();
    $("#divMailingList tr").remove();
    $.ajax({
        async: false,
        dataType: "json",
        type: "POST",
        url: "../listeMailingDirection",
        data: {direction: direction},
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (dataJson) {
            $("#divMailingList").append("<tr><th>" + dataJson[0][0].direction + "</th></tr>");
            for (var y = 1; y < dataJson[0].length; y++) {
                $("#divMailingList").append("<tr><td><input type=\"checkbox\" name=\"" + dataJson[0][y].libelle + "\" value=" + dataJson[0][y].id + " </td><td>" + dataJson[0][y].libelle + " </td>");
            }
            $("#divMailingList").append("<tr><th>" + dataJson[1][0].Autres + "</th></tr>");
            for (var y = 1; y < dataJson[1].length; y++) {
                $("#divMailingList").append("<tr><td><input type=\"checkbox\" name=\"" + dataJson[1][y].libelle + "\" value=" + dataJson[1][y].id + " </td><td>" + dataJson[1][y].libelle + " </td>");
            }
            window.MailingData = dataJson;
        }
    });
}
function upListMailingForProfil() {
    var str=window.location.href;
    var bdg=str.split("/");

    var idBridge = bdg[bdg.length-1];

    $.ajax({
        async: false,
        dataType: "json",
        type: "POST",
        url: "../listeMailingWithPost",
        data: {idBridge: idBridge},
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {

        },
        success: function (data) {
            $("#divMailingSelected").empty();

            for (var y = 0; y < data.length; y++) {
                $('input:checkbox[name="' + data[y].mel_libelle + '"]').prop('checked', true);
            }
            saveListCheckMailing();
        }
    });
}
function doNotSubmit() {
    var test =($('#divMailingSelected tbody').length <= 0) && ($('#divHardwareSelected tbody').length <= 0) && ($('#divSoftwareSelected tbody').length <= 0);
    if (test) {
        alert('Veuillez saisir un groupe outlook ou un materiel ou un logiciel');
        return false;
    }
}







