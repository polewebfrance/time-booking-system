function upListService() {
    $("select#gestion_adminbundle_profil_service option").remove();
    $.ajax({
        async: false,
        dataType: "json",
        type: "POST",
        url: "addservice",
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
            $output = '<option value="-1"></option>';
        },
        success: function (html) {
            $("select#gestion_adminbundle_profil_service").html(html);
        }
    });
}

function displayNewCrm() {
    var newPost = $('#gestion_adminbundle_profil_crm option:selected').val();
    if (newPost == "-1") {
        $('#divCrm').show();
    }
    else {
        $('#divCrm').hide();
    }
}

function upListEquipe() {
    $("select#gestion_adminbundle_profil_crm option").remove();
    $.ajax({
        async: false,
        dataType: "json",
        type: "POST",
        url: "addequipe",
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
            $output = '<option value="-1"></option>';
        },
        success: function (html) {
            $("select#gestion_adminbundle_profil_crm").html(html);
        }
    });
}
$(document).ready(function () {

    $('#myform').bootstrapValidator({
        err: {
            container: '#messages'
        },
        fields: {
            "gestion_adminbundle_profil[crm]": {
        validators: {
            notEmpty: {
                message: 'Ce champ est obligatoire!'
            },
        }
    },
            "gestion_adminbundle_profil[crmHolder]": {
                validators: {
                    notEmpty: {
                        message: 'Ce champ est obligatoire!'
                    },
                }
            },
        }
    });
    upListService();
    displayNewCrm();
    upListEquipe();
    $('#box').hide();
    $('#buttonCloseHelp').hide();

    $('#gestion_adminbundle_profil_crm').on("change", function () {
        displayNewCrm();
    });


    $('#buttonHelp').on("click", function () {
        $('#box').show();
        $('#buttonHelp').hide();
        $('#buttonCloseHelp').show();
    });
    $('#buttonCloseHelp').on("click", function () {
        $('#buttonHelp').show();
        $('#box').hide();
        $('#buttonCloseHelp').hide();
    });



});










