
function initDisplay() {
    $('#loading').show();
    upListDirection();
    displayNewPost();
    displayDepartment();
    $('#divDivision').hide();
    displayPost(0, 0);
    $('#box').hide();
    $('#buttonCloseHelp').hide();
}

function displayNewPost() {
    var newPost = $('#gestion_adminbundle_profil_poste option:selected').val();
    if (newPost == "-1") {
        $('#divNewPost').show();
    }
    else {
        $('#divNewPost').hide();
    }
}
function displayDepartment() {
    var direction = $('#gestion_adminbundle_profil_direction option:selected').val();
    if (direction != '') {
        $('#divDepartment').show();
        upListDepartment();
    }
    else {
        $('#divDepartment').hide();
        displayDivision();
    }
}
function displayDivision() {
    var service = $('#gestion_adminbundle_profil_service option:selected').val();
    if (service != '') {
        $('#divDivision').show();
        upListDivision();
    }
    else {
        $('#divDivision').hide();
        displayPost(0, 0);
    }
}
function displayPost(boolShow, hierarchyLevel) {
    if (boolShow == "1") {
        $('#divPost').show();
        upListPost(hierarchyLevel);
    }
    else {
        $('#divPost').hide();
    }
}
function upListDirection() {
    $("select#gestion_adminbundle_profil_direction option").remove();
    $.ajax({
        async: false,
        dataType: "json",
        type: "POST",
        url: "adddirection",
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
            $output = '<option value="-1"></option>';
        },
        success: function (html) {
			$('#addProfil').show();
            $('#loading').hide();
            $("select#gestion_adminbundle_profil_direction").html(html);
        }
    });
}
function upListDepartment() {
    var direction = $('select#gestion_adminbundle_profil_direction option:selected').val();
    $("select#gestion_adminbundle_profil_service option").remove();
    if (direction != "0") {
        $.ajax({
            async: false,
            dataType: "json",
            type: "POST",
            url: "adddepartment",
            data: {direction: direction},
            cache: false,
            error: function (jqXHR, textStatus, errorThrown) {
                $('#divDepartment').hide();
                $('#divDivision').hide();
            },
            success: function (html) {
                $("select#gestion_adminbundle_profil_service").html(html);
                displayPost(0, 0);
            }
        });
    }
}
function upListDivision() {
    var service = $('select#gestion_adminbundle_profil_service option:selected').val();
    $("select#gestion_adminbundle_profil_pole option").remove();
    if (service != "0") {
        $.ajax({
            async: false,
            dataType: "json",
            type: "POST",
            url: "adddivision",
            data: {service: service},
            cache: false,
            error: function (jqXHR, textStatus, errorThrown) {
                $('#divDivision').hide();
                displayPost(1, 2);
            },
            success: function (html) {
                $("select#gestion_adminbundle_profil_pole").html(html);
            }
        });
    }
}
function upListPost($hierarchyLevel) {
    var idBridge = [];
    if ($hierarchyLevel == "2") {
        idBridge.push($('select#gestion_adminbundle_profil_direction option:selected').val()); // direction
        idBridge.push($('select#gestion_adminbundle_profil_service option:selected').val()); // service
        idBridge.push('NULL'); // pole
    }
    else if ($hierarchyLevel == "3") {
        idBridge.push($('select#gestion_adminbundle_profil_direction option:selected').val()); // direction
        idBridge.push($('select#gestion_adminbundle_profil_service option:selected').val()); // service
        idBridge.push($('select#gestion_adminbundle_profil_pole option:selected').val()); // pole

    }
    $("select#gestion_adminbundle_profil_poste option").remove();
    $.ajax({
        async: false,
        dataType: "json",
        type: "POST",
        url: "addpost",
        data: {idBridge: idBridge},
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (html) {
            $("select#gestion_adminbundle_profil_poste").html(html);
        }
    });
}

function hideInput() {
    $('#loader').hide();
    $('#dialog-confirm').hide();
}

$(document).ready(function () {

    $('#addformprofil').bootstrapValidator({
        err: {
            container: '#messages'
        },
        fields: {

            "userNewPost": {
                validators: {
                    notEmpty: {
                        message: 'Ce champ est obligatoire!'
                    },

                }
            },
            "gestion_adminbundle_profil[direction]": {
                validators: {
                    notEmpty: {
                        message: 'Ce champ est obligatoire!'
                    },
                }
            },

            "gestion_adminbundle_profil[service]": {
                validators: {
                    notEmpty: {
                        message: 'Ce champ est obligatoire!'
                    },
                }
            },
            "gestion_adminbundle_profil[pole]": {
                validators: {
                    notEmpty: {
                        message: 'Ce champ est obligatoire!'
                    },
                }
            },
            "gestion_adminbundle_profil[poste]": {
                validators: {
                    notEmpty: {
                        message: 'Ce champ est obligatoire!'
                    },
                }
            },






        }
    });
    initDisplay();
    $('#gestion_adminbundle_profil_direction').on("change", function () {
        displayDepartment();
        displayDivision();

        hideInput();
    });
    $('#gestion_adminbundle_profil_service' + '').on("change", function () {
        displayDivision();
        hideInput();

        $('#newDivision').hide();
        $('#inputNewDivision').hide();
    });
    $('#gestion_adminbundle_profil_pole').on("change", function () {
        displayPost(1, 3);
        hideInput();


        //---------------//
        //Cas spec DAM   //
        //---------------//
        //Cahmp autre dans les pole
        var service = $('select#gestion_adminbundle_profil_pole option:selected').val();

        $('#newDivision').hide();
        $('#inputNewDivision').hide();

        if (service == -1) {
            $('#gestion_adminbundle_profil_poste').prop('disabled', true); // selection poste
            $('#divNewPost').show();
            $('#labelPrecisePost').hide();
            $('#newDivision').show();
            $('#inputNewDivision').show();
        }
        else {
            $('#gestion_adminbundle_profil_poste').prop('disabled', false);
            $('#divNewPost').hide();
            $('#newDivision').hide();
            $('#inputNewDivision').hide();
        }
        //---------------//
        //Fin spec DAM   //
        //---------------//
    });

    $('#gestion_adminbundle_profil_poste').on("change", function () {
        displayNewPost();


        displayNewPost();
        hideInput();
    });





});









