<?php

namespace SAP\SelfServiceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use SAP\SelfServiceBundle\Form\SAPSelfServiceType; 
use Symfony\Component\HttpFoundation\Response;

class IndexController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {    	
    	$form=$this->createForm(new SAPSelfServiceType());	
    	return $this->render('SAPSelfServiceBundle:Default:index.html.twig',
    		array( 'form' => $form->createView(),
    		)
    	);
    }
    /**
     * @Route("/submit/{choix}")
     */
    public function submitAction($choix)
    {
    	$url="/sap/bc/srt/wsdl/flv_10002A101AD1/bndg_url/sap/bc/srt/rfc/sap/zsap_acc_manag/300/zsap_acc_manag/zsap_acc_manag?sap-client=300";
		if($this->container->get('kernel')->getEnvironment()=='prod'){
			$serveur= "http://srv-scs-prd.noz.local:8000";
		} else {
			$serveur= "http://srv-sap-dev.noz.local:8000";	
		}
		$wsdl= $serveur.$url;
    	$service = new \SoapClient($wsdl);
    	$user=$this->container->get('security.context')->getToken()->getUser();
    	$SAPResponse = $service->__soapCall("ZsapCheckUsername", array(array(
    			'IvNickname'=>$user->getUsername()
    	)));
    	$userSAP=$SAPResponse->EvUsername;
    	$response = new Response();
    	if (!$userSAP){
    		return $this->render('SAPSelfServiceBundle:Default:findAccountKO.html.twig');
    	} else {
			$SAPResponse = $service->__soapCall("ZsapAccUnlockReset", array(array(
	    			'IvAction'=>$choix,
	    			'IvUser'=>$userSAP
	    	)));
	    	return $this->render('SAPSelfServiceBundle:Default:SOAPResponse.html.twig', array (
	    			'SOAPResponse'=>$SAPResponse->EvResult
	    		)	
			);				
	    }
    }
    
    
}
