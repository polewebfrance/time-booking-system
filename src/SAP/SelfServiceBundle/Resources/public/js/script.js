$(document).ready(function () {
	displayContact();
	$('#sap_self_service_choix').change(function(){
		displayContact();
	});
	
	$('#submit').click(function(){
		$('.elementHiden').hide();
		$('#submit').show();
		var choix = $('#sap_self_service_choix').val();
		$.ajax({
			async: false,
			dataType: "html",
			type: "POST",
			url: "./submit/"+choix,
			cache: false,
			error: function (jqXHR, textStatus, errorThrown) {
				alert(errorThrown)
			},
			success: function (reponse) {
				$("#reponse" ).html(reponse);
				$('#reponse').show();
			}
		});
	});
	
	function displayContact() {
		$('.elementHiden').hide();
		var choix = $('#sap_self_service_choix').val();
		if ( choix == 'contact'){
			$('#contact').show();
		} else {
			$('#submit').show();
		}
	}
})