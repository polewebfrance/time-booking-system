<?php

/**
 * Description of AdmMagasinType
 *
 * @author pana_cr
 */

namespace SAP\SelfServiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class SAPSelfServiceType extends AbstractType {
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('choix','choice', array(
        		'choices' => array('' => '-',
        		'R' => 'Changer de mot de passe SAP',
        		'U' => 'Faire débloquer son compte SAP',
        		'contact' => 'Contacter le support SAP'
                ))) ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Administration\SocieteBundle\Entity\AdmMagasin',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sap_self_service';
    }
}
