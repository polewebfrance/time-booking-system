<?php

namespace HotelBookingBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Regex;

use Symfony\Component\Form\FormView; 
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Extension\Core\View\ChoiceView;


class HotelBookingType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('civilite', 'choice', array(
				'choices' => array(
					1 => 'Monsieur',
					0 => 'Madame'
					),
				'expanded' => true,				
				'label'     => 'Civilité',
				'attr' => array('class' => 'civilite'),
				'constraints' => array(
					new NotBlank(array('message' => 'Civilité obligatoire!'))
				)
			))
			->add('nom', 'text', array(
				'required'=> false,
				'constraints' => array(
					new NotBlank(array('message' => 'Nom obligatoire!'))
				)
			))
			->add('prenom', 'text', array(
				'required'=> false,		
				'label'     => 'Prénom',
				'constraints' => array(
					new NotBlank(array('message' => 'Prénom obligatoire!'))
				)
			))
			->add('adresseMail', 'email', array(
				'required'=> false,
				'constraints' => array(
					new Email(array('message' => 'Pas une email valide')),
					new NotBlank(array('message' => 'Adresse mail obligatoire!'))
				)
			))
			->add('telephone', 'text', array(
				'required'=> false,		
				'label'     => 'Téléphone',
				'constraints' => array(
					new NotBlank(array('groups' => 'telephoneNumerique', 'message' => 'Numerique obligatoire!'))
				)					
			))
			->add(
				'direction', 'text', array(
				'required'=> false,		
				'label'     => 'Direction',
				'constraints' => array(
					new NotBlank(array('message' => 'Direction obligatoire!'))
				)
			))
			->add(
				'service', 'text', array(
				'required'=> false,		
				'label'     => 'Service',
				'constraints' => array(
					new NotBlank(array('message' => 'Service obligatoire!'))				)
			))
			->add('nomDuResponsible', 'text', array(
				'required'=> false,		
				'label'     => 'Nom du responsable',
				'constraints' => array(
					new NotBlank(array('message' => 'Nom du responsable obligatoire!'))
				)
			))
	/*		->add(
				'societe',
				'entity',
				array(
					'class' => 'Administration\SocieteBundle\Entity\Societe',
					'em' => 'default',
					'property' => 'uniqueName',
					'label' => 'Société',
					'empty_value' => 'autre',
					'attr' => array('class' => 'chosen', 'data-placeholder' => 'Autre'),
					'multiple' => false,
					'expanded' => false,
					'required' => false,
//					'constraints' => array(
//						new NotBlank(array('groups' => 'Default', 'message' => 'Obligatoire!'))
//					)
				)
			) */
			->add('autreSociete', 'text', array(
				'required'=> false,		
				'label'     => 'Société',
				'constraints' => array(
					new NotBlank(array('groups' => 'autreSociete', 'message' => 'Obligatoire!'))
				)
			))
	/*		->add('adresse', 'text', array(
				'required'=> false,
				'constraints' => array(
					new NotBlank(array('message' => 'Adresse obligatoire!'))
				)
			)) */
			->add('dateArrivee', 'date', array(
				'widget' => 'single_text',
				'format' => 'dd/MM/yyyy',
				'read_only'=> true,
				'label' => 'Date d’arrivée',
				'constraints' => array(
					new NotBlank(array('message' => 'Date d’arrivée obligatoire!'))
				)
			))
			->add('dateDepart', 'date', array(
				'widget' => 'single_text',
				'format' => 'dd/MM/yyyy',
				'read_only'=> true,
				'label' => 'Date de départ',
				'constraints' => array(
					new NotBlank(array('message' => 'Date de départ'))
				)
			))
			->add('nombreDeNuits', 'text', array(
				'read_only'=> true,
				'constraints' => array(
					new NotBlank(array('message' => 'Obligatoire!'))
				)
			))
			->add('heureArrivee', 'time', array(
//				'widget' => 'single_text',
				'empty_value' => '',
				'read_only' => true,
				'required'  => true,
				'hours' => array(12,13,14,15,16,17,18,19,20,21,22,23),
				'label' => 'Heure d’arrivée',				
				'constraints' => array(
					new NotBlank(array('message' => 'Heure d’arrivée obligatoire!'))
				)
			))
//			->add('transportGareHotel', 'checkbox', array(
//				'label'     => 'Transport “gare / hôtel”',
//			))
			->add('transportGareHotel', 'choice', array(
				'label'     => 'Transport “gare / hôtel”',
				'choices' => array(0 => 'Non', 1 => 'Oui'),
				'expanded' => true,
				'multiple' => false,				
				'constraints' => array(
					new NotBlank(array('message' => 'Obligatoire!'))
				)
			))
			->add('commentaire', 'textarea', array(
				'required'=> false,
				'attr' => array('rows' => '2', 'cols' => '60'),
			))
            ->add('save', 'submit', array(
				'label' => 'Réserver',
				'attr' => array('class' => 'btn btn-info', 'aria-label' => 'Left Align', 'type' => 'button')
			));
	}

//	public function finishView(FormView $view, FormInterface $form, array $options)
//{
//    $new_choice = new ChoiceView(array('attr' => array('selected' => 'selected')), 999, 'Autre'); // <- new option
////    $view->children['societe']->vars['choices'][] = $new_choice;//<- adding the new option 
//    array_unshift($view->children['societe']->vars['choices'], $new_choice);//<- adding the new option 
//}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'HotelBookingBundle\Entity\HotelBooking',
            'validation_groups' => function(FormInterface $form) {
				$data = $form->getData();
				$result = array('Default');
				if (!$data->getSociete()) {
					$result[] = 'autreSociete';
				}
				if (!is_numeric($data->getTelephone())) {
					$result[] = 'telephoneNumerique';
				}
				return $result;
			}
		));
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'hotel_booking_hotel_booking';
	}
}