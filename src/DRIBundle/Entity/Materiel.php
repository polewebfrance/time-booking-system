<?php

namespace DRIBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Materiel
 *
 * @ORM\Table(name="dri_materiel")
 * @ORM\Entity
 */
class Materiel
{
    
    
     /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="materiels")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;
    
    
    /**
     * @ORM\ManyToOne(targetEntity="Status", inversedBy="materiels")
     * @ORM\JoinColumn(name="status_id", referencedColumnName="id")
     */
    protected $status;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;



    /**
     * @var integer
     *
     * @ORM\Column(name="user_id_glpi", type="integer")
     */
    private $userIdGlpi;

    /**
     * @var integer
     *
     * @ORM\Column(name="status_id", type="integer")
     */
    private $statusId;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="username_glpi", type="string", length=255)
     */
    private $usernameGlpi;

    /**
     * @var string
     *
     * @ORM\Column(name="manufacturer", type="string", length=255)
     */
    private $manufacturer;

    /**
     * @var string
     *
     * @ORM\Column(name="computermodel", type="string", length=255)
     */
    private $computermodel;

    /**
     * @var string
     *
     * @ORM\Column(name="monitormodel", type="string", length=255)
     */
    private $monitormodel;

    /**
     * @var string
     *
     * @ORM\Column(name="peripheralmodel", type="string", length=255)
     */
    private $peripheralmodel;

    /**
     * @var string
     *
     * @ORM\Column(name="printermodel", type="string", length=255)
     */
    private $printermodel;

    /**
     * @var string
     *
     * @ORM\Column(name="phonemodel", type="string", length=255)
     */
    private $phonemodel;

    /**
     * @var string
     *
     * @ORM\Column(name="designation", type="string", length=255)
     */
    private $designation;

    /**
     * @var string
     *
     * @ORM\Column(name="numserie", type="string", length=255)
     */
    private $numserie;

    /**
     * @var string
     *
     * @ORM\Column(name="codebarre", type="string", length=255)
     */
    private $codebarre;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="string", length=255)
     */
    private $comment;

    /**
     * @var \Date
     *
     * @ORM\Column(name="date_restitution", type="date")
     */
    private $dateRestitution;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return Materiel
     */
//    public function setUserId($userId)
//    {
//        $this->userId = $userId;
//    
//        return $this;
//    }

    /**
     * Get userId
     *
     * @return integer
     */
//    public function getUserId()
//    {
//        return $this->userId;
//    }
    
    
    
    

    /**
     * Set userIdGlpi
     *
     * @param integer $userIdGlpi
     *
     * @return Materiel
     */
    public function setUserIdGlpi($userIdGlpi)
    {
        $this->userIdGlpi = $userIdGlpi;
    
        return $this;
    }

    /**
     * Get userIdGlpi
     *
     * @return integer
     */
    public function getUserIdGlpi()
    {
        return $this->userIdGlpi;
    }

    /**
     * Set statusId
     *
     * @param integer $statusId
     *
     * @return Materiel
     */
    public function setStatusId($statusId)
    {
        $this->statusId = $statusId;
    
        return $this;
    }

    /**
     * Get statusId
     *
     * @return integer
     */
    public function getStatusId()
    {
        return $this->statusId;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return Materiel
     */
    public function setUsername($username)
    {
        $this->username = $username;
    
        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set usernameGlpi
     *
     * @param string $usernameGlpi
     *
     * @return Materiel
     */
    public function setUsernameGlpi($usernameGlpi)
    {
        $this->usernameGlpi = $usernameGlpi;
    
        return $this;
    }

    /**
     * Get usernameGlpi
     *
     * @return string
     */
    public function getUsernameGlpi()
    {
        return $this->usernameGlpi;
    }

    /**
     * Set manufacturer
     *
     * @param string $manufacturer
     *
     * @return Materiel
     */
    public function setManufacturer($manufacturer)
    {
        $this->manufacturer = $manufacturer;
    
        return $this;
    }

    /**
     * Get manufacturer
     *
     * @return string
     */
    public function getManufacturer()
    {
        return $this->manufacturer;
    }

    /**
     * Set computermodel
     *
     * @param string $computermodel
     *
     * @return Materiel
     */
    public function setComputermodel($computermodel)
    {
        $this->computermodel = $computermodel;
    
        return $this;
    }

    /**
     * Get computermodel
     *
     * @return string
     */
    public function getComputermodel()
    {
        return $this->computermodel;
    }

    /**
     * Set monitormodel
     *
     * @param string $monitormodel
     *
     * @return Materiel
     */
    public function setMonitormodel($monitormodel)
    {
        $this->monitormodel = $monitormodel;
    
        return $this;
    }

    /**
     * Get monitormodel
     *
     * @return string
     */
    public function getMonitormodel()
    {
        return $this->monitormodel;
    }

    /**
     * Set peropheralmodel
     *
     * @param string $peropheralmodel
     *
     * @return Materiel
     */
    public function setPeripheralmodel($peripheralmodel)
    {
        $this->peripheralmodel = $peripheralmodel;
    
        return $this;
    }

    /**
     * Get peropheralmodel
     *
     * @return string
     */
    public function getPeripheralmodel()
    {
        return $this->peripheralmodel;
    }

    /**
     * Set printermodel
     *
     * @param string $printermodel
     *
     * @return Materiel
     */
    public function setPrintermodel($printermodel)
    {
        $this->printermodel = $printermodel;
    
        return $this;
    }

    /**
     * Get printermodel
     *
     * @return string
     */
    public function getPrintermodel()
    {
        return $this->printermodel;
    }

    /**
     * Set phonemodel
     *
     * @param string $phonemodel
     *
     * @return Materiel
     */
    public function setPhonemodel($phonemodel)
    {
        $this->phonemodel = $phonemodel;
    
        return $this;
    }

    /**
     * Get phonemodel
     *
     * @return string
     */
    public function getPhonemodel()
    {
        return $this->phonemodel;
    }

    /**
     * Set designation
     *
     * @param string $designation
     *
     * @return Materiel
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;
    
        return $this;
    }

    /**
     * Get designation
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set numserie
     *
     * @param string $numserie
     *
     * @return Materiel
     */
    public function setNumserie($numserie)
    {
        $this->numserie = $numserie;
    
        return $this;
    }

    /**
     * Get numserie
     *
     * @return string
     */
    public function getNumserie()
    {
        return $this->numserie;
    }

    /**
     * Set codebarre
     *
     * @param string $codebarre
     *
     * @return Materiel
     */
    public function setCodebarre($codebarre)
    {
        $this->codebarre = $codebarre;
    
        return $this;
    }

    /**
     * Get codebarre
     *
     * @return string
     */
    public function getCodebarre()
    {
        return $this->codebarre;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Materiel
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    
        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set dateRestitution
     *
     * @param \Date $dateRestitution
     *
     * @return Materiel
     */
    public function setDateRestitution($dateRestitution)
    {
        $this->dateRestitution = $dateRestitution;
    
        return $this;
    }

    /**
     * Get dateRestitution
     *
     * @return \Date
     */
    public function getDateRestitution()
    {
        return $this->dateRestitution;
    }

    /**
     * Set user
     *
     * @param \DRIBundle\Entity\User $user
     *
     * @return Materiel
     */
    public function setUser(\DRIBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \DRIBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set status
     *
     * @param \DRIBundle\Entity\Status $status
     *
     * @return Materiel
     */
    public function setStatus(\DRIBundle\Entity\Status $status = null)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return \DRIBundle\Entity\Status
     */
    public function getStatus()
    {
        return $this->status;
    }


}
