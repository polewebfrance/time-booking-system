<?php

namespace DRIBundle\Entity;

use Symfony\Component\Validator\Mapping\ClassMetadata;

use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Users
 *
 * @ORM\Table(name="dri_user")
 * @ORM\Entity(repositoryClass="DRIBundle\Entity\UserRepository")
 */
class User
{
    
    
    
    
    /**
     * @ORM\OneToOne(targetEntity="Computedstatus", mappedBy="user")
     */
    protected $computedstatus;
    
    
  
    /**
     * @ORM\OneToMany(targetEntity="Materiel", mappedBy="user")
     */
    protected $materiels;
    
    /**
     * @ORM\OneToMany(targetEntity="Computers", mappedBy="user")
     */
    protected $computers;
    
     /**
     * @ORM\OneToMany(targetEntity="Phones", mappedBy="user")
     */
    protected $phones;
    
    /**
     * @ORM\OneToMany(targetEntity="Peripherals", mappedBy="user")
     */
    protected $peripherals;
    
    /**
     * @ORM\OneToMany(targetEntity="Printers", mappedBy="user")
     */
    protected $printers;
    
    /**
     * @ORM\OneToMany(targetEntity="Monitors", mappedBy="user")
     */
    protected $monitors;
    

    
     /**
     * @ORM\OneToMany(targetEntity="Materials", mappedBy="user")
     */
    protected $materials; 
    
    
    
    public function __construct()
    {
        $this->materials = new ArrayCollection();
        
        $this->computers = new ArrayCollection();
        $this->phones = new ArrayCollection();
        $this->peripherals = new ArrayCollection();
        $this->printers = new ArrayCollection();
        $this->monitors = new ArrayCollection();
    }
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="givenname", type="string", length=255)
     */
    private $givenname;

    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string", length=255)
     */
    private $surname;
    
    /**
     * @var string
     *
     * @ORM\Column(name="email_canonical", type="string", length=255)
     */
    private $email_canonical;
    
    /**
     * @var string
     *
     * @ORM\Column(name="username_canonical", type="string", length=255)
     */
    private $username_canonical;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="displayname", type="string", length=255)
     */
    private $displayname;
    
    
     /**
     * @var string
     *
     * @ORM\Column(name="matricule", type="string", length=6)
     */
    private $matricule;
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return Users
     */
    public function setUsername($username)
    {
        $this->username = $username;
    
        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Users
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set givenname
     *
     * @param string $givenname
     *
     * @return Users
     */
    public function setGivenname($givenname)
    {
        $this->givenname = $givenname;
    
        return $this;
    }

    /**
     * Get givenname
     *
     * @return string
     */
    public function getGivenname()
    {
        return $this->givenname;
    }

    /**
     * Set surname
     *
     * @param string $surname
     *
     * @return Users
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    
        return $this;
    }

    /**
     * Get surname
     *
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }
    
    
    
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
                  
        $metadata->addPropertyConstraint('username', new Assert\NotBlank());    
        $metadata->addPropertyConstraint('email', new Assert\NotBlank());     
        $metadata->addPropertyConstraint('givenname', new Assert\NotBlank());
        $metadata->addPropertyConstraint('surname', new Assert\NotBlank());
        
        $metadata->addPropertyConstraint('email', new Assert\Email(array(
               'message' => 'The email "{{ value }}" is not a valid email.',
               'checkMX' => true,
        )));



//        $metadata->addPropertyConstraint('gender', new Assert\Choice(array(
//                                        'choices' => array('Male', 'Female'),
//                                        'message' => 'Choose a valid gender.',
//        )));
    }
    
    
    

    /**
     * Set emailCanonical
     *
     * @param string $emailCanonical
     *
     * @return User
     */
    public function setEmailCanonical($emailCanonical)
    {
        $this->email_canonical = $emailCanonical;
    
        return $this;
    }

    /**
     * Get emailCanonical
     *
     * @return string
     */
    public function getEmailCanonical()
    {
        return $this->email_canonical;
    }

    /**
     * Set usernameCanonical
     *
     * @param string $usernameCanonical
     *
     * @return User
     */
    public function setUsernameCanonical($usernameCanonical)
    {
        $this->username_canonical = $usernameCanonical;
    
        return $this;
    }

    /**
     * Get usernameCanonical
     *
     * @return string
     */
    public function getUsernameCanonical()
    {
        return $this->username_canonical;
    }
    
      

    /**
     * Add materiel
     *
     * @param \DRIBundle\Entity\Materiel $materiel
     *
     * @return User
     */
    public function addMateriel(\DRIBundle\Entity\Materiel $materiel)
    {
        $this->materiels[] = $materiel;
    
        return $this;
    }

    /**
     * Remove materiel
     *
     * @param \DRIBundle\Entity\Materiel $materiel
     */
    public function removeMateriel(\DRIBundle\Entity\Materiel $materiel)
    {
        $this->materiels->removeElement($materiel);
    }

    /**
     * Get materiels
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMateriels()
    {
        return $this->materiels;
    }

    /**
     * Set displayname
     *
     * @param string $displayname
     *
     * @return User
     */
    public function setDisplayname($displayname)
    {
        $this->displayname = $displayname;
    
        return $this;
    }

    /**
     * Get displayname
     *
     * @return string
     */
    public function getDisplayname()
    {
        return $this->displayname;
    }

    /**
     * Add computer
     *
     * @param \DRIBundle\Entity\Computers $computer
     *
     * @return User
     */
    public function addComputer(\DRIBundle\Entity\Computers $computer)
    {
        $this->computers[] = $computer;
    
        return $this;
    }

    /**
     * Remove computer
     *
     * @param \DRIBundle\Entity\Computers $computer
     */
    public function removeComputer(\DRIBundle\Entity\Computers $computer)
    {
        $this->computers->removeElement($computer);
    }

    /**
     * Get computers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComputers()
    {
        return $this->computers;
    }

    /**
     * Add phone
     *
     * @param \DRIBundle\Entity\Phones $phone
     *
     * @return User
     */
    public function addPhone(\DRIBundle\Entity\Phones $phone)
    {
        $this->phones[] = $phone;
    
        return $this;
    }

    /**
     * Remove phone
     *
     * @param \DRIBundle\Entity\Phones $phone
     */
    public function removePhone(\DRIBundle\Entity\Phones $phone)
    {
        $this->phones->removeElement($phone);
    }

    /**
     * Get phones
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPhones()
    {
        return $this->phones;
    }

    /**
     * Add peripheral
     *
     * @param \DRIBundle\Entity\Peripherals $peripheral
     *
     * @return User
     */
    public function addPeripheral(\DRIBundle\Entity\Peripherals $peripheral)
    {
        $this->peripherals[] = $peripheral;
    
        return $this;
    }

    /**
     * Remove peripheral
     *
     * @param \DRIBundle\Entity\Peripherals $peripheral
     */
    public function removePeripheral(\DRIBundle\Entity\Peripherals $peripheral)
    {
        $this->peripherals->removeElement($peripheral);
    }

    /**
     * Get peripherals
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPeripherals()
    {
        return $this->peripherals;
    }

    /**
     * Add printer
     *
     * @param \DRIBundle\Entity\Printers $printer
     *
     * @return User
     */
    public function addPrinter(\DRIBundle\Entity\Printers $printer)
    {
        $this->printers[] = $printer;
    
        return $this;
    }

    /**
     * Remove printer
     *
     * @param \DRIBundle\Entity\Printers $printer
     */
    public function removePrinter(\DRIBundle\Entity\Printers $printer)
    {
        $this->printers->removeElement($printer);
    }

    /**
     * Get printers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPrinters()
    {
        return $this->printers;
    }

    /**
     * Add monitor
     *
     * @param \DRIBundle\Entity\Monitors $monitor
     *
     * @return User
     */
    public function addMonitor(\DRIBundle\Entity\Monitors $monitor)
    {
        $this->monitors[] = $monitor;
    
        return $this;
    }

    /**
     * Remove monitor
     *
     * @param \DRIBundle\Entity\Monitors $monitor
     */
    public function removeMonitor(\DRIBundle\Entity\Monitors $monitor)
    {
        $this->monitors->removeElement($monitor);
    }

    /**
     * Get monitors
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMonitors()
    {
        return $this->monitors;
    }

    /**
     * Add material
     *
     * @param \DRIBundle\Entity\Materials $material
     *
     * @return User
     */
    public function addMaterial(\DRIBundle\Entity\Materials $material)
    {
        $this->materials[] = $material;
    
        return $this;
    }

    /**
     * Remove material
     *
     * @param \DRIBundle\Entity\Materials $material
     */
    public function removeMaterial(\DRIBundle\Entity\Materials $material)
    {
        $this->materials->removeElement($material);
    }

    /**
     * Get materials
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMaterials()
    {
        return $this->materials;
    }

    /**
     * Set computedstatus
     *
     * @param \DRIBundle\Entity\Computedstatus $computedstatus
     *
     * @return User
     */
    public function setComputedstatus(\DRIBundle\Entity\Computedstatus $computedstatus = null)
    {
        $this->computedstatus = $computedstatus;
    
        return $this;
    }

    /**
     * Get computedstatus
     *
     * @return \DRIBundle\Entity\Computedstatus
     */
    public function getComputedstatus()
    {
        return $this->computedstatus;
    }

    /**
     * Set matricule
     *
     * @param string $matricule
     *
     * @return User
     */
    public function setMatricule($matricule)
    {
        $this->matricule = $matricule;
    
        return $this;
    }

    /**
     * Get matricule
     *
     * @return string
     */
    public function getMatricule()
    {
        return $this->matricule;
    }
}
