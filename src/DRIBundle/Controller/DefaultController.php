<?php

namespace DRIBundle\Controller;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use DRIBundle\Entity\User;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    

    
    public function getConn(){
       return $this->get('database_connection'); 
    }
    
   public function getConnClient(){
       return $this->get('doctrine.dbal.client_connection'); 
    }
    
    public function indexAction()
    {
        
//         $statement = $this->getConnClient()->executeQuery('SELECT * FROM users',array());
//        $result= $statement->fetchAll();
        
//        echo "<pre>";
//          var_dump($result);
//        echo "</pre>";die();
        //var_dump($this->getUser());die();
        return $this->render('DRIBundle::base.html.php', array('user'=>$this->getUser()));
    }
    
    public function formularAction()
    {
        //die('ok');
        return $this->render('DRIBundle:Default:formular.html.php', array('user'=>$this->getUser()));
    }
    
    public function contactAction()
    {
        //die('ok');
        return $this->render('DRIBundle:Default:contact.html.php', array('user'=>$this->getUser()));
    }
    
//    public function showuserformAction(Request $request)
//    {
//        //die('ok');
//        return $this->render('DRIBundle:Default:adduser.html.php', array('user'=>$this->getUser()));
//    }
    
    public function newuserAction(Request $request)
    {
        //var_dump($request);
        // create a task and give it some dummy data for this example
        $user = new User();
        
        $form = $this->createFormBuilder($user)
                     //->setAction($this->generateUrl('_adduser'))
                    // ->setMethod('POST')
                //  
                     ->add('username', 'text', array('label' => 'User Name','required'=>false))
                     ->add('email', 'text', array('label' => 'Email','required'=>false))
                     ->add('givenname', 'text', array('label' => 'Given Name','required'=>false))
                     ->add('surname', 'text', array('label' => 'Surname','required'=>false))            
                     ->add('save', 'submit', array('label' => 'Create User'))
                     ->getForm();
        
        $form->handleRequest($request);
        
        $validator = $this->get('validator');
        $errors = $validator->validate($user);
        
        if ($form->isSubmitted()) {
            
            if ($form->isValid()) {
                // the validation passed, do something with the $author object
         //  $request->query->get("username");
//                echo "<pre>";
//                 var_dump($form->getData());
//                 //$form->getData();
//                echo "</pre>";  die();

//              $request->getSession()->getFlashBag()->add(
//                          'notice',
//                           'Your changes were saved!'
//                        );
//                $em = $this->getDoctrine()->getManager();
//                $em->persist($user);
//                $em->flush();

                 //$form->getData()->getUsername();
                //echo "</pre>";  
                die();
                //return $this->redirectToRoute('task_success');
                return $this->redirect($this->generateUrl('dri_homepage'));
                
            } else {

                return $this->render('DRIBundle:Default:adduser.html.php', array(
                            'form' => $form->createView(),
                            'user' => $this->getUser(),
                            'errors' => $errors,
                ));
            }
        }

        return $this->render('DRIBundle:Default:adduser.html.php', array(
                             'form' => $form->createView(),
                             'user'=>$this->getUser()
                            ));
    }
    

    
    
    public function listusersAction(Request $request)
    {
      $paginator  = $this->get('knp_paginator');
     // var_dump($this->getUser());die();
     $em = $this->getDoctrine()->getManager();
     $repository = $em->getRepository('DRIBundle:User');
     $results = $repository->findAll();
//
//     
//     return $this->render('DRIBundle:Default:listusers.html.php', array('users'=>$users,'user'=>$this->getUser()));
        
//     $em    = $this->get('doctrine.orm.entity_manager');
//    $dql   = "SELECT a FROM DRIBundle:User a";
//    $query = $em->createQuery($dql);
        
//        $statement = $this->getConn()->executeQuery('SELECT u.displayname, spu.*, spuc.* FROM user u 
//                                  LEFT JOIN se_projet_user spu                                        
//                                  ON u.id = spu.user_id
//                                  LEFT JOIN se_projet_user_capacite spuc
//                                  ON spu.user_id = spuc.user_id 
//                                  WHERE u.id=?',array(2));
//        $result= $statement->fetchAll();

  
    $users = $paginator->paginate(
                            $results, 
                            $request->query->getInt('page', 1)/*page number*/,
                            10/*limit per page*/
                        );
    
     $users->setTemplate('DRIBundle:Pagination:twitter_bootstrap_v3_pagination.html.php ');
 
    
//       echo "<pre>";
//         var_dump($users);
//       echo "</pre>";  die();

    // parameters to template
    return $this->render('DRIBundle:Default:listusers.html.php', array('users'=>$users,
                                                                       'user'=>$this->getUser(),
                                                                    ));
    
    }
    
    
    
    
    public function userdetailsAction($id)
    {
               
        $statement = $this->getConn()->executeQuery('SELECT u.displayname, spu.*, spuc.* FROM user u 
                                          LEFT JOIN se_projet_user spu                                        
                                          ON u.id = spu.user_id
                                          LEFT JOIN se_projet_user_capacite spuc
                                          ON spu.user_id = spuc.user_id 
                                          WHERE u.id=?',array($id));
        $result= $statement->fetchAll();
       

        
//        echo "<pre>";
//          var_dump($result);
//        echo "</pre>";die();
        return $this->render('DRIBundle:Default:listdetails.html.php', array('attributes'=>$result,
                                                                             'user'=>$this->getUser()));
        

        

         //return new Response("<p>ok ".$id."</p>");
    }
}
