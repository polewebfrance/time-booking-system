<?php

namespace DPDCBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormInterface;

use Symfony\Component\Validator\Constraints\NotBlank;

class CommentaireN2PDCType extends AbstractType
{

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{		
		$builder
			->add('commentaire', 'textarea', array(
				'attr' => array(
					'placeholder' => $options['data']->getPointDeControle()->getTitre(),
					'cols' => 55,
					'rows' => 3,
				),
				'label' => $options['data']->getPointDeControle()->getTitre()
			));
	}

	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'DPDCBundle\Entity\CommentaireN2Mois',
			'csrf_protection' => false
		));
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'dpdc_commentaire_N2_PDC_type';
	}
}