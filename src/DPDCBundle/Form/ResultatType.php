<?php

namespace DPDCBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormInterface;

use Symfony\Component\Validator\Constraints\NotBlank;

class ResultatType extends AbstractType
{
	private $idPoint;
	private $month;
	private $niveau;

	public function __construct($idPoint, $month, $niveau){
		$this->idPoint = $idPoint;
		$this->month = $month;
		$this->niveau = $niveau;
	}

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('idPoint', 'hidden', array(
				"mapped" => false,
				'attr' => array('value' => $this->idPoint)
			))
			->add('month', 'hidden', array(
				"mapped" => false,
				'attr' => array('value' => $this->month)
			))
			->add('niveau', 'hidden', array(
				"mapped" => false,
				'attr' => array('value' => $this->niveau)
			))
			->add('id', 'hidden', array(
				"mapped" => false
			))
			->add('reponse', 'choice', array(
				'choices' => array(
					0 => 'Non',
					1 => 'Oui'
				),
				'label' => 'Réponse',
				'multiple' => false,
				'expanded' => true,
				'constraints' => array(
					new NotBlank(array('message' => 'Réponse obligatoire!'))
				)
			))
			->add('commentaire', 'textarea', array(
				'attr' => array('placeholder' => 'details ...'),
				'constraints' => array(
					new NotBlank(array('groups' => 'reponseNon', 'message' => 'Commentaire obligatoire!'))
				)
			))
			->add('save', 'submit', array(
				'label' => 'Enregistrer',
				'attr' => array('class' => 'btn btn-info', 'aria-label' => 'Left Align', 'type' => 'button'),
			));
	}

	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'DPDCBundle\Entity\Resultat',
			'csrf_protection' => false,
            'validation_groups' => function(FormInterface $form) {
				$data = $form->getData();
				$result = array('Default');
				if($data->getReponse() === 0){
					$result[] = 'reponseNon';					
				}
				return $result;
			}
		));
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'dpdc_resultat';
	}
}