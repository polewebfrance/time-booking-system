<?php

namespace DPDCBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\Email;
use \DateTime;
use \Date;

class PointDeControleType extends AbstractType {

	private $modifie;

	public function __construct($modifie) {
		$this->modifie = $modifie;
	}

	private function englishToFrenchDate($english) {
		$leMois = array("Jan" => "Janvier", "Feb" => "Février", "Mar" => 'Mars', "Apr" => 'Avril', "May" => 'Mai', "Jun" => 'Juin',
			"Jul" => 'Juillet', "Aug" => 'Août', "Sep" => 'Septembre', "Oct" => 'Octobre', "Nov" => 'Novembre', "Dec" => 'Décembre');
		$date = explode(' ', $english);
		$french = str_replace($date[0], $leMois[$date[0]], $english);
		return $french;
	}

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
//		if ($this->modifie) {
//			$builder
//					->add('nom', 'text', array(
//						'constraints' => array(new NotBlank(array('message' => 'Pourriez-vous renseigner votre nom?'))),
//						'attr' => array('readonly' => 'readonly', 'placeholder' => 'Votre nom', 'class' => 'fixed-width'),
//						'label' => 'Votre nom'
//					))
//					->add('prenom', 'text', array(
//						'constraints' => array(new NotBlank(array('message' => 'Pourriez-vous renseigner votre prénom?'))),
//						'attr' => array('readonly' => 'readonly', 'placeholder' => 'Votre prénom', 'class' => 'fixed-width'),
//						'label' => 'Votre Prénom'
//					))
//					->add('email', 'text', array(
//						'constraints' => array(
//							new NotBlank(array('message' => 'Pourriez-vous renseigner votre adresse mail?')),
//							new Email(array('message' => 'Pourriez-vous renseigner le champs adresse mail?'))
//						),
//						'attr' => array('readonly' => 'readonly', 'placeholder' => 'Votre adresse mail', 'class' => 'fixed-width'),
//						'label' => 'Votre adresse mail'
//			));
//		} else {
			$builder
					->add('nom', 'text', array(
						'constraints' => array(new NotBlank(array('message' => 'Pourriez-vous renseigner votre nom?'))),
						'attr' => array('placeholder' => 'Votre nom', 'class' => 'fixed-width'),
						'label' => 'Votre nom'
					))
					->add('prenom', 'text', array(
						'constraints' => array(new NotBlank(array('message' => 'Pourriez-vous renseigner votre prénom?'))),
						'attr' => array('placeholder' => 'Votre prénom', 'class' => 'fixed-width'),
						'label' => 'Votre prénom'
					))
					->add('email', 'text', array(
						'constraints' => array(
							new NotBlank(array('message' => 'Pourriez-vous renseigner votre adresse mail?')),
							new Email(array('message' => 'Pourriez-vous renseigner le champs adresse mail?'))
						),
						'attr' => array('placeholder' => 'Votre adresse mail', 'class' => 'fixed-width'),
						'label' => 'Votre adresse mail'
			));
//		}
		$builder
//			->add('direction',	'entity', array(
//				'class' => 'Administration\DirectionBundle\Entity\Direction',
//				'property' => 'libelle',
//				'constraints' => array(new NotBlank(array('message' => 'La direction est obligatoire!'))),
//				'empty_value' => '',
//				'attr' => array('class' => 'chosen fixed-width', 'data-placeholder' => 'choisir la direction'),
//				'label' => 'La Direction',
//			))
//			->add('service',	'entity', array(
//				'class' => 'Administration\DirectionBundle\Entity\Service',
//				'property' => 'libelle',
//				'constraints' => array(new NotBlank(array('message' => 'Le service est obligatoire!'))),
//				'empty_value' => '',
//				'attr' => array('class' => 'chosen fixed-width', 'data-placeholder' => 'choisir le service'),
//				'label' => 'Le Service',
//			))
//			->add('pole',	'entity', array(
//				'class' => 'Administration\DirectionBundle\Entity\Pole',
//				'property' => 'libelle',
//				'empty_value' => '',
//				'attr' => array('class' => 'chosen fixed-width', 'data-placeholder' => 'choisir le pôle'),
//				'label' => 'Le Pôle'
//			))		
				->add('titre', 'text', array(
					'constraints' => array(new NotBlank(array('message' => 'Pourriez-vous renseigner l\'intitulé du contrôle?'))),
					'attr' => array('placeholder' => 'Intitulé du contrôle', 'class' => 'fixed-width'),
					'label' => 'Intitulé du contrôle',
				))
				->add('description', 'textarea', array(
					'attr' => array('placeholder' => 'Description du contrôle', 'class' => 'fixed-width'),
					'label' => 'Description du contrôle',
				))
				->add('frequence', 'choice', array(
					'choices' => array(
						30 => 'Mensuelle',
						60 => 'Bimestrielle',
						90 => 'Trimestrielle',
						120 => 'Semestrielle',
						360 => 'Annuelle',
					),
					'empty_value' => '',
					'attr' => array('class' => 'chosen fixed-width', 'data-placeholder' => 'Fréquence'),
					'label' => 'Fréquence du contrôle'
				))
//			->add('lePremierMois', 'date', array(
//				'widget' => 'single_text',
//				'format' => 'M YYYY',	
//				'attr' => array('class' => 'date', 'data-placeholder' => 'choisir le premier mois'),
//				'label' => 'Le premier mois',
//				'invalid_message' => 'la date n\'est pas valide',
//				'constraints' => array(
//					new GreaterThanOrEqual(array('value' => new DateTime('first day of this month 00:00:00'), 'message' => 'la date n\'est pas valide'))
//				))
//			)
				->add('lePremierMois', 'text', array(
					'attr' => array('class' => 'date', 'data-placeholder' => 'Le premier mois', 'readonly' => 'readonly'),
					'label' => 'Premier mois',
					'invalid_message' => 'la date n\'est pas valide',
					'data' => is_object($options['data']->getLePremierMois()) ? $this->englishToFrenchDate($options['data']->getLePremierMois()->format('M Y')) : ''
				))
				->add('nomAAppeler', 'text', array(
					'attr' => array('placeholder' => 'Personne à contacter lors du contrôle', 'class' => 'fixed-width'),
					'label' => 'Personne à contacter lors du contrôle'
				))
//			->add('prenomAAppeler', 'text', array(
//				'attr' => array('placeholder' => 'le prénom a appeler', 'class' => 'fixed-width'),
//				'label' => 'Le prénom a appeler'
//			))	
				->add('telephoneAAppeler', 'text', array(
					'attr' => array('placeholder' => 'Téléphone de la personne à contacter', 'class' => 'fixed-width'),
					'label' => 'Téléphone de la personne à contacter'
				))
				->add('type', 'choice', array(
					'choices' => array(
						1 => 'Croisés',
						2 => 'Suivi process'
					),
					'empty_value' => '',
					'attr' => array('class' => 'chosen fixed-width', 'data-placeholder' => 'Type de contrôle'),
					'label' => 'Type de contrôle'
				))
				->add('niveau1', 'entity', array(
					'class' => 'User\UserBundle\Entity\User',
					'property' => 'Displayname',
					'required' => false,
					'attr' => array('class' => 'chosen fixed-width', 'data-placeholder' => 'choisir user'),
					'label' => 'Niveau 1'
				))
				->add('niveau2', 'entity', array(
					'class' => 'User\UserBundle\Entity\User',
//					'query_builder' => function(\Doctrine\ORM\EntityRepository $er) {
//						return $er->createQueryBuilder('u')
//								->where('u.roles LIKE :roles')
//								->orWhere('u.roles LIKE :roles2')
//								->setParameter('roles', '%ROLE_DPDC_NIVEAU2%')
//								->setParameter('roles2', '%ROLE_DPDC_NIVEAU3%');
//					},
					'property' => 'Displayname',
					'required' => false,
					'attr' => array('class' => 'chosen fixed-width', 'data-placeholder' => 'choisir user'),
					'label' => 'Niveau 2'
				))
				->add('niveau3', 'entity', array(
					'class' => 'User\UserBundle\Entity\User',
					'query_builder' => function(\Doctrine\ORM\EntityRepository $er) {
						return $er->createQueryBuilder('u')
								->where('u.roles LIKE :roles')
								->setParameter('roles', '%ROLE_DPDC_NIVEAU3%');
					},
					'property' => 'Displayname',
					'required' => false,
					'attr' => array('class' => 'chosen fixed-width', 'data-placeholder' => 'choisir user'),
					'label' => 'Niveau 3'
				))
				->add('save', 'submit', array(
					'label' => ' Enregistrer',
					'attr' => array('class' => 'btn btn-info glyphicon glyphicon-floppy-save', 'aria-label' => 'Left Align', 'type' => 'button'),
				))
		;
	}

	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver) {
		$resolver->setDefaults(array(
			'data_class' => 'DPDCBundle\Entity\PointDeControle'
		));
	}

	/**
	 * @return string
	 */
	public function getName() {
		return 'dpdc_point_de_controle';
	}

}
