<?php

namespace DPDCBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormInterface;

use Symfony\Component\Validator\Constraints\NotBlank;

class CommentaireN2Type extends AbstractType
{
	private $mois;
	private $niveau1;
	private $niveau2;

	public function __construct($mois, $niveau1, $niveau2){
		$this->mois = $mois;
		$this->niveau1 = $niveau1;
		$this->niveau2 = $niveau2;
	}

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('mois', 'hidden', array(
				"mapped" => false,
				'attr' => array('value' => $this->mois)
			))
			->add('niveau1', 'hidden', array(
				'attr' => array('value' => $this->niveau1)
			))
			->add('niveau2', 'hidden', array(
				'attr' => array('value' => $this->niveau2)
			))
			->add('commentaire', 'textarea', array(
				'attr' => array('placeholder' => 'details ...'),
				'constraints' => array(
					new NotBlank(array('message' => 'Obligatoire!'))
				)
			))
			->add('save', 'submit', array(
				'label' => 'Enregistrer',
				'attr' => array('class' => 'btn btn-info', 'aria-label' => 'Left Align', 'type' => 'button'),
			));
	}

	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'DPDCBundle\Entity\CommentaireN2',
			'csrf_protection' => false
		));
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'dpdc_commentaireN2';
	}
}