<?php

namespace DPDCBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use DPDCBundle\Entity\PointDeControle;
use DPDCBundle\Form\PointDeControleType;
use Doctrine\Common\Collections\ArrayCollection as ArrayCollection;
use Administration\DirectionBundle\Entity\Direction;
use Administration\DirectionBundle\Entity\Service;
use \DateTime;
use \DateInterval;
use \DatePeriod;

class PointDeControleController extends Controller {

	private function isMenuV1V2Visible($niveau){
		$user = $this->getUser();
		if(!$user){
			return 0;
		}
		$userId = $user->getId();
		$conditions = array(
			'type' => \DPDCBundle\Entity\PointDeControle::CROISES,
			'statut' => \DPDCBundle\Entity\PointDeControle::ACTIF
		);
		$conditions[$niveau] = (int) $userId;
		$pointsDeControle = $this->getDoctrine()
			->getRepository('DPDCBundle:PointDeControle')
			->findBy($conditions);
		$lastAndNext12Months = $this->lastAndNext12Months();
		$allMonths = $lastAndNext12Months['allMonths'];
		$plannedPDC = $lastAndNext12Months['plannedPDC'];
		$end = $lastAndNext12Months['end'];
		foreach ($allMonths as $mon) {
			foreach ($pointsDeControle as $point) {
				$point->getPointPlanning($plannedPDC, $mon, $end);
			}
		}
		foreach($plannedPDC as $month => $planned){
			if(count($planned)){
				return 1;
			}
		}
		return 0;
	}

	private function nombrePointsActifsCalcule(){
		$criteria = new \Doctrine\Common\Collections\Criteria();
		$criteria->where($criteria->expr()->neq('statut', \DPDCBundle\Entity\PointDeControle::INACTIF));
		$criteria->orderBy(array('dateAdded' => 'DESC'));
		$pointsDeControle = $this->getDoctrine()
			->getRepository('DPDCBundle:PointDeControle')
			->matching($criteria);
		return new Response(count($pointsDeControle));
	}

	/**
	 * @Route("/menu-visible-N1", name="dpdc_menu_visible_n1")
	 * @Method({"GET"})
	 */
	public function menuVisibleN1Action(){
		$isV1Visible = $this->isMenuV1V2Visible('niveau1');
		return new Response($isV1Visible);
	}

	/**
	 * @Route("/menu-visible-N2", name="dpdc_menu_visible_n2")
	 * @Method({"GET"})
	 */
	public function menuVisibleN2Action(){
		$isV1Visible = $this->isMenuV1V2Visible('niveau2');
		return new Response($isV1Visible);
	}

	/**
	 * @Route("/nombrePointsActifs", name="dpdc_nombre_points_actifs")
	 * @Method({"GET"})
	 */
	public function nombrePointsActifsAction(){
		$user = $this->getUser();
//		if (!$user || !$user->isGranted("ROLE_DPDC_NIVEAU3") && !$user->isGranted("ROLE_COPIL")) {
//			return $this->redirect($this->generateUrl('dpdc_point_de_controle_ajouter'));
//		}
		return $this->nombrePointsActifsCalcule();
	}

	/**
	 * @Route("/setSessionMonth", name="dpdc_session_month")
	 * @Method({"POST"})
	 */
	public function setSessionMonth(Request $request) {
		$leMois = $request->request->get('leMois');
		if ($leMois) {
			$session = $this->getRequest()->getSession();
			$session->set('leMois', $leMois);
			return new JsonResponse('session!');
		} else {
			return new JsonResponse('pas session!');
		}
	}

	/**
	 * @Route("/getDirections", name="dpdc_get_directions")
	 * @Method({"POST"})
	 */
	public function getDirections() {
		$directions = $this->getDoctrine()
				->getRepository('AdministrationDirectionBundle:Direction')
				->findAll();
		if ($directions) {
			$results = array(0 => '');
			foreach ($directions as $result) {
				$results[$result->getId()] = $result->getLibelle();
			}
//			return new JsonResponse(json_encode(serialize($aretsDeTravail)));
			return new JsonResponse($results);
		} else {
			return new JsonResponse('pas de directions!');
		}
	}

	/**
	 * @Route("/getServiceByDirection", name="dpdc_get_service_by_direction")
	 * @Method({"POST"})
	 */
	public function getServiceByDirection() {
		$request = $this->container->get('request');
//		$directionLibelle = $request->request->get('directionLibelle');
		$direction = $request->request->get('direction');
//		if($directionLibelle !== ''){
		if ($direction) {
//			$direction = $this->getDoctrine()
//				->getRepository('AdministrationDirectionBundle:Direction')
//				->findOneBy(array('libelle' => html_entity_decode($directionLibelle)));
			$services = $this->getDoctrine()
					->getRepository('AdministrationDirectionBundle:Service')
					->findBy(array('direction' => (int) $direction));
		} else {
			$services = null;
		}

		if ($services) {
			$results = array(0 => '');
			foreach ($services as $result) {
				$results[$result->getId()] = $result->getLibelle();
			}
//			return new JsonResponse(json_encode(serialize($aretsDeTravail)));
			return new JsonResponse($results);
		} else {
			return new JsonResponse('pas de services!');
		}
	}

	/**
	 * @Route("/getPoleByService", name="dpdc_get_pole_by_service")
	 * @Method({"POST"})
	 */
	public function getPoleByService() {
		$request = $this->container->get('request');
		$service = $request->request->get('service');
		if ($service) {
			$poles = $this->getDoctrine()
					->getRepository('AdministrationDirectionBundle:Pole')
					->findBy(array('service' => (int) $service));
		} else {
			$poles = null;
		}

		if ($poles) {
			$results = array(0 => '');
			foreach ($poles as $result) {
				$results[$result->getId()] = $result->getLibelle();
			}
//			return new JsonResponse(json_encode(serialize($aretsDeTravail)));
			return new JsonResponse($results);
		} else {
			return new JsonResponse('pas de poles!');
		}
	}

	/**
	 * @Route("/point-de-controle/activer", name="dpdc_activer_PDC")
	 * @Method({"POST"})
	 */
	public function activerPDCAction(Request $request) {
		$user = $this->getUser();
		if (!$user->isGranted("ROLE_DPDC_NIVEAU3")) {
			$this->get('session')->getFlashBag()->add('dpdcPointsSuccess', 'Pas des droits!');
			return $this->redirect($this->generateUrl('dpdc_points_de_controle'));
		}
		$PDC = $request->request->get('PDC');
		$em = $this->getDoctrine()->getManager();
		$pointDeControle = $em->getRepository('DPDCBundle:PointDeControle')->find((int) $PDC);
		if (empty($pointDeControle)) {
			$this->get('session')->getFlashBag()->add('dpdcPointsSuccess', 'Aucun Point de contrôle trouvé');
			return $this->redirect($this->generateUrl('dpdc_points_de_controle'));
		}
		if ($pointDeControle->getNiveau1() !== null && $pointDeControle->getNiveau2() !== null && $pointDeControle->getLePremierMois() !== null && $pointDeControle->getFrequence() !== null && $pointDeControle->getType() === \DPDCBundle\Entity\PointDeControle::CROISES) {
			$pointDeControle->setStatut(2);
		} else {
			$pointDeControle->setStatut(1);
		}
		$em->persist($pointDeControle);
		$em->flush();
		$this->get('session')->getFlashBag()->add('dpdcPointDetails', 'Les modifications sont enregistrées!');
		return new JsonResponse('Success!');
	}

	/**
	 * @Route("/point-de-controle/annuler", name="dpdc_annuler_PDC")
	 * @Method({"POST"})
	 */
	public function annulerPDCAction(Request $request) {
		$user = $this->getUser();
		if (!$user->isGranted("ROLE_DPDC_NIVEAU3")) {
			$this->get('session')->getFlashBag()->add('dpdcPointsSuccess', 'Pas des droits!');
			return $this->redirect($this->generateUrl('dpdc_points_de_controle'));
		}
		$PDC = $request->request->get('PDC');
		$em = $this->getDoctrine()->getManager();
		$pointDeControle = $em->getRepository('DPDCBundle:PointDeControle')->find((int) $PDC);
		if (empty($pointDeControle)) {
			$this->get('session')->getFlashBag()->add('dpdcPointsSuccess', 'Aucun Point de contrôle trouvé');
			return $this->redirect($this->generateUrl('dpdc_points_de_controle'));
		}
		$pointDeControle->setStatut(3);
		$em->persist($pointDeControle);
		$em->flush();
		$this->get('session')->getFlashBag()->add('dpdcPointsSuccess', 'Les modifications sont enregistrées!');
		return new JsonResponse('Success!');
	}

	/**
	 * @Route("/point-de-controle/valider/{id}", name="dpdc_point_de_controle_valider")
	 * @Method({"GET", "POST"})
	 */
	public function validerPointDeControleAction($id, Request $request) {
		$user = $this->getUser();
		if (!$user->isGranted("ROLE_DPDC_NIVEAU3") && !$user->isGranted("ROLE_COPIL")) {
			return $this->redirect($this->generateUrl('dpdc_vision_n1'));
		}
		$em = $this->getDoctrine()->getManager();
		$pointDeControle = $em->getRepository('DPDCBundle:PointDeControle')->find($id);
		if (empty($pointDeControle)) {
			$this->get('session')->getFlashBag()->add('dpdcPointsSuccess', 'Aucun Point de contrôle trouvé');
			return $this->redirect($this->generateUrl('dpdc_points_de_controle'));
		}
		$formValider = $this->createFormBuilder()
				->add('valider', 'submit', array(
					'label' => 'Valider',
					'attr' => array('class' => 'btn btn-success', 'aria-label' => 'Left Align', 'type' => 'button'),
				))
				->add('refuser', 'submit', array(
					'label' => 'Refuser',
					'attr' => array('class' => 'btn btn-danger', 'aria-label' => 'Left Align', 'type' => 'button'),
				))
				->getForm();
		$formValider->handleRequest($request);
		if ($formValider->isValid()) {
			if ($formValider->get('valider')->isClicked()) {
				$pointDeControle->setReponse(1);
				$subject = 'validé';
			}
			if ($formValider->get('refuser')->isClicked()) {
				$pointDeControle->setReponse(0);
				$pointDeControle->setStatut(\DPDCBundle\Entity\PointDeControle::INACTIF);
				$subject = 'refusé';
			}
			$pointDeControle->setDateReponse(new Datetime());
			$em->persist($pointDeControle);
			$em->flush();			
			$mailMessage = \Swift_Message::newInstance()
					->setContentType('text/html')
					->setSubject('Le directeur a ' . $subject . ' le contrôle n° ' . $id . '.')
					->setFrom('controlescroises@noz.fr', 'Contrôles Croisés')
					->setTo(array('controlescroises@noz.fr', 'clanger@noz.fr'))
					->setBcc('poleweb@noz.fr')
					->setBody(
					$this->renderView(
							'DPDCBundle:email:validerRefuser.html.twig', array(
						'pointDeControle' => $pointDeControle,
						'subject' => $subject,
						'ficheRoute' => 'http://' . $this->getRequest()->getHost() . $this->generateUrl('dpdc_point_de_controle_details', array('id' => $pointDeControle->getId()))
							)
					)
			);
			$this->get('mailer')->send($mailMessage);
			$this->get('session')->getFlashBag()->add('dpdcPointsSuccess', 'Votre réponse a bien été prise en compte');
			return $this->redirect($this->generateUrl('dpdc_points_de_controle'));
		}
		return $this->render('DPDCBundle:pointDeControle:validerPointDeControle.html.twig', array(
					'formValider' => $formValider->createView(),
					'pointDeControle' => $pointDeControle,
					'title' => 'Valider point de contrôle ' . $pointDeControle->getTitre()
		));
	}

	/**
	 * @Route("/point-de-controle/modifier/{id}", name="dpdc_point_de_controle_modifier")
	 * @Method({"GET", "POST"})
	 */
	public function modifierPointDeControleAction($id, Request $request) {
		$user = $this->getUser();
		if (!$user->isGranted("ROLE_DPDC_NIVEAU3")) {
			return $this->redirect($this->generateUrl('dpdc_vision_n1'));
		}
		$em = $this->getDoctrine()->getManager();
		$pointDeControle = $em->getRepository('DPDCBundle:PointDeControle')->find($id);
		if (empty($pointDeControle)) {
			$this->get('session')->getFlashBag()->add('dpdcPointsSuccess', 'Aucun Point de contrôle trouvé');
			return $this->redirect($this->generateUrl('dpdc_points_de_controle'));
		}
		$form = $this->createForm(new PointDeControleType(true), $pointDeControle);
		$oldN1Id = $pointDeControle->getNiveau1() ? $pointDeControle->getNiveau1()->getId() : null;
		$oldN2Id = $pointDeControle->getNiveau2() ? $pointDeControle->getNiveau2()->getId() : null;
		$oldLePremierMois = $pointDeControle->getLePremierMois() ? $pointDeControle->getLePremierMois() : null;
		$oldFrequence = $pointDeControle->getFrequence() ? $pointDeControle->getFrequence() : null;
		$oldType = $pointDeControle->getType() ? $pointDeControle->getType() : null;
		$leMois = array("Janvier" => '01', "Février" => '02', "Mars" => '03', "Avril" => '04', "Mai" => '05', "Juin" => '06',
			"Juillet" => '07', "Août" => '08', "Septembre" => '09', "Octobre" => 10, "Novembre" => 11, "Décembre" => 12,
			"January" => '01', "February" => '02', "March" => '03', "April" => '04', "May" => '05', "June" => '06',
			"July" => '07', "August" => '08', "September" => '09', "October" => 10, "November" => 11, "December" => 12);
		if ($this->getRequest()->isMethod('GET')) {
			$direction = $pointDeControle->getDirection() ? $pointDeControle->getDirection()->getId() : 0;
			$service = $pointDeControle->getService() ? $pointDeControle->getService()->getId() : 0;
			$pole = $pointDeControle->getPole() ? $pointDeControle->getPole()->getId() : 0;
		} else {
			$direction = $request->request->get('direction') ? $request->request->get('direction') : 0;
			$service = $request->request->get('service') ? $request->request->get('service') : 0;
			$pole = $request->request->get('pole') ? $request->request->get('pole') : 0;
			if ($direction) {
				$ledirection = $this->getDoctrine()
						->getRepository('AdministrationDirectionBundle:Direction')
						->find((int) $direction);
				$pointDeControle->setDirection($ledirection ? $ledirection : null);
			}
			if ($service) {
				$leservice = $this->getDoctrine()
						->getRepository('AdministrationDirectionBundle:Service')
						->find((int) $service);
				$pointDeControle->setService($leservice ? $leservice : null);
			}
			if ($pole) {
				$lepole = $this->getDoctrine()
						->getRepository('AdministrationDirectionBundle:Pole')
						->find((int) $pole);
				$pointDeControle->setPole($lepole ? $lepole : null);
			}
		}
		$errordirection = '';
		$errorservice = '';
		$validator = $this->get('validator');
		$errors = $validator->validate($pointDeControle);
		if ($this->getRequest()->isMethod('POST')) {
			$areErrors = 0;
			foreach ($errors as $error) {
				$errorName = 'error' . $error->getPropertyPath();
				$$errorName = $error->getMessage();
				$areErrors++;
			}
		}
		$form->handleRequest($request);
		if ($form->isValid() && !$areErrors) {

			if ($form['lePremierMois']->getData() !== null) {
				$date = explode(' ', $form['lePremierMois']->getData());
				$pointDeControle->setLePremierMois(new \DateTime('01-' . $leMois[$date[0]] . '-' . $date[01]));
			} else {
				$pointDeControle->setLePremierMois(null);
			}
			if ($form['niveau1']->getData() !== null && $form['niveau2']->getData() !== null && $form['lePremierMois']->getData() !== null && $form['frequence']->getData() !== null && $form['type']->getData() === 1) {
				$isValid = true;
				$pointDeControle->setStatut(\DPDCBundle\Entity\PointDeControle::ACTIF);
			} else {
				$isValid = false;
			}
			$pointDeControle->setDateModified(new Datetime());
			$pointDeControle->setModifiedBy($user->getDisplayName());
			$em->persist($pointDeControle);
			$em->flush();
			// CHANGES N1 OR N2 (?? TO SEND EMAILS ALSO TO OLD N1 AND/OR N2 ??)
			if (
				($form['niveau1']->getData() !== $oldN1Id || $form['niveau2']->getData() !== $oldN2Id)
				&& $isValid
			) {
				$mailNiveau1 = \Swift_Message::newInstance()
						->setContentType('text/html')
						->setSubject('Vous avez été affecté à un contrôle croisé.')
						->setFrom('controlescroises@noz.fr', 'Contrôles Croisés')
						->setTo(array($pointDeControle->getNiveau1()->getEmail(), $pointDeControle->getNiveau2()->getEmail()))
						->setBcc('poleweb@noz.fr')
						->setBody(
						$this->renderView(
							'DPDCBundle:email:choisiN1N2.html.twig', array(
								'pointDeControle' => $pointDeControle,
								'ficheRoute' => 'http://' . $this->getRequest()->getHost() . $this->generateUrl('dpdc_point_de_controle_details', array('id' => $pointDeControle->getId())),
								'visionN1' => 'http://' . $this->getRequest()->getHost() . $this->generateUrl('dpdc_vision_n1', array('niveau1' => $pointDeControle->getNiveau1()->getId(), 'niveau2' => 0)),
								'visionN2' => 'http://' . $this->getRequest()->getHost() . $this->generateUrl('dpdc_vision_n2', array('niveau2' => $pointDeControle->getNiveau2()->getId()))
							)
						)
				);
				$this->get('mailer')->send($mailNiveau1);
			}
			$this->get('session')->getFlashBag()->add('dpdcPointsSuccess', 'Les modifications sont enregistrées!');
			return $this->redirect($this->generateUrl('dpdc_points_de_controle'));
		}
		return $this->render('DPDCBundle:PointDeControle:ajouterPointDeControle.html.twig', array(
					'form' => $form->createView(),
					'pointDeControle' => $pointDeControle,
					'title' => 'Modifier point de contrôle ' . $pointDeControle->getTitre(),
					'direction' => $direction,
					'service' => $service,
					'pole' => $pole,
					'errordirection' => $errordirection,
					'errorservice' => $errorservice
		));
	}

	/**
	 * @Route("/point-de-controle/ajouter", name="dpdc_point_de_controle_ajouter")
	 * @Method({"GET", "POST"})
	 */
	public function ajouterPointDeControleAction(Request $request) {
		$user = $this->getUser();
		$pointDeControle = new PointDeControle();
		if ($user) {
			$pointDeControle->setNom($user->getGivenName());
			$pointDeControle->setPrenom($user->getSurname());
			$pointDeControle->setEmail($user->getEmail());
		}
		$form = $this->createForm(new PointDeControleType(false), $pointDeControle);
		$leDirection = $request->request->get('direction') ? $request->request->get('direction') : 0;
		$leService = $request->request->get('service') ? $request->request->get('service') : 0;
		$lePole = $request->request->get('pole') ? $request->request->get('pole') : 0;
		if ($leDirection) {
			$direction = $this->getDoctrine()
					->getRepository('AdministrationDirectionBundle:Direction')
					->find((int) $leDirection);
			$pointDeControle->setDirection($direction);
		}
		if ($leService) {
			$service = $this->getDoctrine()
					->getRepository('AdministrationDirectionBundle:Service')
					->find((int) $leService);
			$pointDeControle->setService($service);
		}
		if ($lePole) {
			$pole = $this->getDoctrine()
					->getRepository('AdministrationDirectionBundle:Pole')
					->find((int) $lePole);
			$pointDeControle->setPole($pole);
		}
		$errordirection = '';
		$errorservice = '';
		$validator = $this->get('validator');
		$errors = $validator->validate($pointDeControle);
		if ($this->getRequest()->isMethod('POST')) {
			$areErrors = 0;
			foreach ($errors as $error) {
				$errorName = 'error' . $error->getPropertyPath();
				$$errorName = $error->getMessage();
				$areErrors++;
			}
		}
		$form->handleRequest($request);
		if ($form->isValid() && !$areErrors) {
			$pointDeControle->setStatut(1);
			$pointDeControle->setDateAdded(new Datetime());
			if ($user) {
				$pointDeControle->setAddedBy($user->getDisplayName());
			} else {
				$pointDeControle->setAddedBy($form['nom']->getData() . $form['prenom']->getData());
			}
			$em = $this->getDoctrine()->getManager();
			$em->persist($pointDeControle);
			$em->flush();
			$mailMessage = \Swift_Message::newInstance()
					->setContentType('text/html')
					->setSubject('Votre demande a été enregistrée.')
					->setFrom('controlescroises@noz.fr', 'Contrôles Croisés')
					->setTo($form['email']->getData())
					->setBcc('poleweb@noz.fr')
					->setBody(
					$this->renderView(
							'DPDCBundle:email:pointDeControle.html.twig', array(
						'pointDeControle' => $pointDeControle,
						'ficheRoute' => 'http://' . $this->getRequest()->getHost() . $this->generateUrl('dpdc_point_de_controle_details', array('id' => $pointDeControle->getId()))
							)
					)
			);
			$this->get('mailer')->send($mailMessage);
			// THE EMAIL TO THE DIRECTORS WILL BE SENT AFTERWARDS BY THE QUALITY DEPARTMENT
			$mailQualite = \Swift_Message::newInstance()
					->setContentType('text/html')
					->setSubject('Un contrôle a été ajouté.')
					->setFrom('controlescroises@noz.fr', 'Contrôles Croisés')
					->setTo(array('controlescroises@noz.fr', 'clanger@noz.fr'))
					->setBcc('poleweb@noz.fr')
					->setBody(
					$this->renderView(
							'DPDCBundle:email:pointDeControleQualite.html.twig', array(
								'pointDeControle' => $pointDeControle,
								'validerRoute' => 'http://' . $this->getRequest()->getHost() . $this->generateUrl('dpdc_point_de_controle_valider', array('id' => $pointDeControle->getId())),
								'ficheRoute' => 'http://' . $this->getRequest()->getHost() . $this->generateUrl('dpdc_point_de_controle_details', array('id' => $pointDeControle->getId()))
							)
					)
			);
			$this->get('mailer')->send($mailQualite);
			$this->get('session')->getFlashBag()->add('dpdcPointSuccess', 'Votre proposition de contrôle a bien été enregistrée');
			return $this->redirect($this->generateUrl('dpdc_point_de_controle_ajouter'));
		}
		return $this->render('DPDCBundle:PointDeControle:ajouterPointDeControle.html.twig', array(
			'form' => $form->createView(),
			'title' => 'Ajouter point de contrôle',
			'direction' => $leDirection,
			'service' => $leService,
			'pole' => $lePole,
			'errordirection' => $errordirection,
			'errorservice' => $errorservice
		));
	}

	/**
	 * @Route("/point-de-controle-details/{id}", name="dpdc_point_de_controle_details")
	 * @Method({"GET"})
	 */
	public function pointDeControleDetailsAction($id) {
		$user = $this->getUser();
		$isNiveau3 = $user && $user->isGranted("ROLE_DPDC_NIVEAU3") ? true : false;
		$em = $this->getDoctrine()->getManager('default');
		$pointDeControle = $em->getRepository('DPDCBundle:PointDeControle')->find($id);
		if (empty($pointDeControle)) {
			$message = 'Aucun Point de contrôle trouvé';
			$this->get('session')->getFlashBag()->add('dpdcPointsSuccess', $message);
			return $this->redirect($this->generateUrl('dpdc_points_de_controle'));
		}
		return $this->render('DPDCBundle:PointDeControle:pointDeControle2.html.twig', array(
									'isNiveau3' => $isNiveau3,
									'pointDeControle' => $pointDeControle,
									'title' => 'Point de contrôle ' . $pointDeControle->getTitre()
								)
							);
	}

	/**
	 * @Route("/point-de-controle", name="dpdc_point_de_controle")
	 * @Method({"POST"})
	 */
	public function pointDeControleAction() {
		$user = $this->getUser();
		$isNiveau3 = $user && $user->isGranted("ROLE_DPDC_NIVEAU3") ? true : false;
		$em = $this->getDoctrine()->getManager('default');
		$request = $this->container->get('request');
		$idPoint = $request->request->get('idPoint');
		$pointDeControle = $em->getRepository('DPDCBundle:PointDeControle')->find($idPoint);
		if (empty($pointDeControle)) {
			$message = 'Aucun Point de contrôle trouvé';
			$this->get('session')->getFlashBag()->add('dpdcPoints', $message);
			return $this->redirect($this->generateUrl('dpdc_points_de_controle'));
		}
		return $this->render('DPDCBundle:pointDeControle:pointDeControle.html.twig', array(
									'isNiveau3' => $isNiveau3,
									'pointDeControle' => $pointDeControle,
									'title' => 'Point de contrôle ' . $pointDeControle->getTitre()
								)
							);
	}

	/**
	 * @Route("/points-de-controle", name="dpdc_points_de_controle")
	 * @Method({"GET", "POST"})
	 */
	public function pointsDeControleAction() {
		$user = $this->getUser();
		if (!$user) {
			return $this->redirect($this->generateUrl('dpdc_point_de_controle_ajouter'));
		}
		if (!$user->isGranted("ROLE_DPDC_NIVEAU3") && !$user->isGranted("ROLE_COPIL")) {
			$userId = $user->getId();
			return $this->redirect($this->generateUrl('dpdc_vision_n1', array('niveau1' => $userId, 'niveau2' => 0)));
		}
		$form = $this->createFormBuilder()
				->add('afficheDesactive', 'checkbox', array(
					'label' => 'Afficher les points des contrôles désactivés ?',
					'required' => false,
				))
				->add('save', 'submit', array(
					'label' => 'rechercher',
					'attr' => array('class' => 'btn btn-info', 'aria-label' => 'Left Align', 'type' => 'button', 'title' => 'Afficher les points des contrôles désactivés ?'),
				))
				->getForm();
		$request = $this->container->get('request');
		$form->handleRequest($request);
		if ($form["afficheDesactive"]->getData()) {
			$pointsDeControle = $this->getDoctrine()
				->getRepository('DPDCBundle:PointDeControle')
				->findBy(array(), array('dateAdded' => 'DESC'));
		} else {
			$criteria = new \Doctrine\Common\Collections\Criteria();
			$criteria->where($criteria->expr()->neq('statut', \DPDCBundle\Entity\PointDeControle::INACTIF));
			$criteria->orderBy(array('dateAdded' => 'DESC'));
			$pointsDeControle = $this->getDoctrine()
				->getRepository('DPDCBundle:PointDeControle')
				->matching($criteria);
		}
		$lastAndNext12Months = $this->lastAndNext12Months();
		$allMonths = $lastAndNext12Months['allMonths'];
		$plannedPDC = $lastAndNext12Months['plannedPDC'];
		$plannedN2 = $lastAndNext12Months['plannedPDC'];
		$end = $lastAndNext12Months['end'];
		if ($request->request->get('sendEmails') && $this->getUser()->isGranted("ROLE_DPDC_NIVEAU3")) {
			$PDCvalide = $this->getDoctrine()
				->getRepository('DPDCBundle:PointDeControle')
				->findBy(array(
								'type' => \DPDCBundle\Entity\PointDeControle::CROISES,
								'statut' => \DPDCBundle\Entity\PointDeControle::ACTIF
							)
				);
			foreach ($allMonths as $mon) {
				foreach ($PDCvalide as $point) {
					$point->getPointPlanning($plannedPDC, $mon, $end);
					$point->getPointManager($plannedN2, $mon, $end);
				}
			}
			$resultats = $this->getDoctrine()
					->getRepository('DPDCBundle:Resultat')
					->getResultsForManager(DateTime::createFromFormat('m-y', key($plannedPDC)), 0);
			$resultatsMoisPointNiveau = array();
			foreach ($resultats as $resultat) {
				$resultatsMoisPointNiveau[$resultat['date']][$resultat['niveau1']]['niveau1'] = $resultat['niveau1'];
				$resultatsMoisPointNiveau[$resultat['date']][$resultat['niveau1']]['results'][$resultat['pointDeControle']]['reponse'] = $resultat['reponse'];
				$resultatsMoisPointNiveau[$resultat['date']][$resultat['niveau1']]['results'][$resultat['pointDeControle']]['commentaire'] = $resultat['commentaire'];
				@$resultatsMoisPointNiveau[$resultat['date']][$resultat['niveau1']]['oui'] += $resultat['reponse'];
			}
			$this->get('session')->getFlashBag()->add('dpdcPointsSuccess', 'Les e-mails ont été envoyés!');
			$this->emailReminder($plannedPDC, $plannedN2, $resultatsMoisPointNiveau, $allMonths);
			return;
		}
		return $this->render('DPDCBundle:PointDeControle:pointsDeControle.html.twig', array(
					'form' => $form->createView(),
					'pointsDeControle' => $pointsDeControle,
					'allMonths' => $allMonths,
		));
	}

	/**
	 * @Route("/Vision-N1/{niveau1}/{niveau2}", name="dpdc_vision_n1", defaults={"niveau1"=NULL, "niveau2"=NULL})
	 */
	public function visionN1Action($niveau1, $niveau2) {
		$user = $this->getUser();
		$userId = $user->getId();
		$session = $this->getRequest()->getSession();
		$leMois = $session->get('leMois') ? $session->get('leMois') : 0;
		$niveau = $user->isGranted("ROLE_DPDC_NIVEAU3") ? 3 : 2;
		$isNumericN1 = is_numeric($niveau1);
		$isNumericN2 = is_numeric($niveau2);
		$conditions = array(
			'type' => \DPDCBundle\Entity\PointDeControle::CROISES,
			'statut' => \DPDCBundle\Entity\PointDeControle::ACTIF
		);
		if($niveau !== 3){
			if(!$this->isMenuV1V2Visible('niveau1') && !$this->isMenuV1V2Visible('niveau2')){
				return $this->redirect($this->generateUrl('dpdc_point_de_controle_ajouter'));
			}
			if(!$isNumericN1 || $niveau1 < 0){
				return $this->redirect($this->generateUrl('dpdc_vision_n1', array('niveau1' => (string) $userId, 'niveau2' => 0)));
			}
			if(!$isNumericN2 || $niveau2 < 0){
				return $this->redirect($this->generateUrl('dpdc_vision_n1', array('niveau1' => $niveau1, 'niveau2' => 0)));
			}
			if((int)$niveau1 !== $userId && (int)$niveau2 !== $userId){
				return $this->redirect($this->generateUrl('dpdc_vision_n1', array('niveau1' => (string) $userId, 'niveau2' => 0)));
			}
			(int) $niveau1 > 0 ? $conditions['niveau1'] = (int) $niveau1 : true;
			(int) $niveau2 > 0 ? $conditions['niveau2'] = (int) $niveau2 : true;
		}else{
			if(!$isNumericN1 || $niveau1 < 0){
				if(!$isNumericN2 || $niveau2 < 0){
					return $this->redirect($this->generateUrl('dpdc_vision_n1', array('niveau1' => '0', 'niveau2' => '0')));
				}
				return $this->redirect($this->generateUrl('dpdc_vision_n1', array('niveau1' => '0', 'niveau2' => (string) $niveau2)));
			} else if (!$isNumericN2 || $niveau2 < 0) {
				return $this->redirect($this->generateUrl('dpdc_vision_n1', array('niveau1' => $niveau1, 'niveau2' => '0')));
			}
			(int) $niveau1 > 0 ? $conditions['niveau1'] = (int) $niveau1 : true;
			(int) $niveau2 > 0 ? $conditions['niveau2'] = (int) $niveau2 : true;
		}
		$pointsDeControle = $this->getDoctrine()
			->getRepository('DPDCBundle:PointDeControle')
			->findBy($conditions);
		$lastAndNext12Months = $this->lastAndNext12Months();
		$allMonths = $lastAndNext12Months['allMonths'];
		$plannedPDC = $lastAndNext12Months['plannedPDC'];
		$end = $lastAndNext12Months['end'];
		foreach ($allMonths as $mon) {
			foreach ($pointsDeControle as $point) {
				$point->getPointPlanning($plannedPDC, $mon, $end);
			}
		}
		$resultats = $this->getDoctrine()
				->getRepository('DPDCBundle:Resultat')
				->getResultsForPlanning(DateTime::createFromFormat('m-y', key($plannedPDC)), (int)$niveau1, (int)$niveau2);
		$resultatsMoisPointNiveau = array();
		foreach ($resultats as $resultat) {
			$leNiveau = $resultat['niveau1'] ? array('niveau1' => $resultat['niveau1']) : array('niveau1' => $resultat['niveau1']);
			$resultatsMoisPointNiveau[$resultat['date']][$resultat['pointDeControle']][key($leNiveau)]['reponse'] = $resultat['reponse'];
			$resultatsMoisPointNiveau[$resultat['date']][$resultat['pointDeControle']][key($leNiveau)]['commentaire'] = $resultat['commentaire'];
		}
		$commentaires = $this->getDoctrine()
				->getRepository('DPDCBundle:CommentaireN2Mois')
				->getCommentairesForPlanning(DateTime::createFromFormat('m-y', key($plannedPDC)), (int)$niveau1, (int)$niveau2);
		$commentairesMoisPointN2N1 = array();
		foreach ($commentaires as $commentaire) {
			$commentairesMoisPointN2N1[$commentaire['date']][$commentaire['pointDeControle']] = $commentaire['commentaire'];
		}
		
		return $this->render('DPDCBundle:PointDeControle:planning.html.twig', array(
			'allMonths' => $allMonths,
			'plannedPDC' => $plannedPDC,
			'resultatsMoisPointNiveau' => $resultatsMoisPointNiveau,
			'commentairesMoisPointN2N1' => $commentairesMoisPointN2N1,
			'niveau' => 'niveau1',
			'leMois' => $leMois,
			'userId' => $userId
		));
	}

	/**
	 * @Route("/Vision-N2/{niveau2}", name="dpdc_vision_n2", defaults={"niveau2"=NULL})
	 */
	public function visionN2Action($niveau2) {
		$session = $this->getRequest()->getSession();
		$leMois = $session->get('leMois') ? $session->get('leMois') : 0;
		$em = $this->getDoctrine()->getManager('default');
		$user = $this->getUser();
		$userId = $user->getId();
		$niveau = $user->isGranted("ROLE_DPDC_NIVEAU3") ? 3 : 2;
		$isNumericN2 = is_numeric($niveau2);
		if($niveau !== 3){
			if(!$this->isMenuV1V2Visible('niveau2')){
				return $this->redirect($this->generateUrl('dpdc_point_de_controle_ajouter'));
			}
			if (!$isNumericN2 || $niveau2 < 0 || $userId !== (int) $niveau2) {
				return $this->redirect($this->generateUrl('dpdc_vision_n2', array('niveau2' => (string) $userId)));
			}
		}else{
			if (!$isNumericN2 || $niveau2 < 0) {
				return $this->redirect($this->generateUrl('dpdc_vision_n2', array('niveau2' => '0')));
			}
		}
		$qb = $em->createQueryBuilder();
		if ($niveau2 > 0) {
			$qb->select('p')
				->from('\DPDCBundle\Entity\PointDeControle', 'p')
				->join('\User\UserBundle\Entity\User', 'u')
				->where('u.id = p.niveau1')
				->andWhere('p.niveau2 = ' . (int) $niveau2)
				->andWhere('p.type = ' . \DPDCBundle\Entity\PointDeControle::CROISES)
				->andWhere('p.statut = ' . \DPDCBundle\Entity\PointDeControle::ACTIF);
		}else{
			$qb->select('p')
				->from('\DPDCBundle\Entity\PointDeControle', 'p')
				->join('\User\UserBundle\Entity\User', 'u')
				->where('u.id = p.niveau1')
				->andWhere('p.type = ' . \DPDCBundle\Entity\PointDeControle::CROISES)
				->andWhere('p.statut = ' . \DPDCBundle\Entity\PointDeControle::ACTIF);
		}
		$niveau1s = $qb->getQuery()->getResult();
		$lastAndNext12Months = $this->lastAndNext12Months();
		$allMonths = $lastAndNext12Months['allMonths'];
		$plannedPDC = $lastAndNext12Months['plannedPDC'];
		$end = $lastAndNext12Months['end'];
		foreach ($allMonths as $mon) {
			foreach ($niveau1s as $point) {
				$point->getPointManager($plannedPDC, $mon, $end);
			}
		}
		$resultats = $this->getDoctrine()
				->getRepository('DPDCBundle:Resultat')
				->getResultsForManager(DateTime::createFromFormat('m-y', key($plannedPDC)), (int)$niveau2);
		$resultatsMoisPointNiveau = array();
		foreach ($resultats as $resultat) {
			$resultatsMoisPointNiveau[$resultat['date']][$resultat['niveau1']]['niveau1'] = $resultat['niveau1'];
			$resultatsMoisPointNiveau[$resultat['date']][$resultat['niveau1']]['results'][$resultat['pointDeControle']]['reponse'] = $resultat['reponse'];
			$resultatsMoisPointNiveau[$resultat['date']][$resultat['niveau1']]['results'][$resultat['pointDeControle']]['commentaire'] = $resultat['commentaire'];
			@$resultatsMoisPointNiveau[$resultat['date']][$resultat['niveau1']]['oui'] += $resultat['reponse'];
		}
		
//		var_dump($resultatsMoisPointNiveau['03-16'][1]);
//		die;
		
		return $this->render('DPDCBundle:PointDeControle:manager.html.twig', array(
			'allMonths' => $allMonths,
			'plannedPDC' => $plannedPDC ? $plannedPDC : array(),
			'resultatsMoisPointNiveau' => $resultatsMoisPointNiveau ? $resultatsMoisPointNiveau : array(),
			'userId' => $userId,
			'leMois' => $leMois,
			'niveau2' => $niveau2
		));
	}

	/**
	 * @Route("/Vision-N3", name="dpdc_vision_n3")
	 */
	public function visionN3Action() {
		$session = $this->getRequest()->getSession();
		$leMois = $session->get('leMois') ? $session->get('leMois') : 0;
		$em = $this->getDoctrine()->getManager('default');
		$user = $this->getUser();
		$userId = $user->getId();
		$niveau = $user->isGranted("ROLE_DPDC_NIVEAU3") ? 3 : 2;
		if($niveau !== 3){
			return $this->redirect($this->generateUrl('dpdc_vision_n1', array('niveau1' => (string) $userId, 'niveau2' => null)));
		}
		$qb = $em->createQueryBuilder();
		$qb->select('p')
				->from('\DPDCBundle\Entity\PointDeControle', 'p')
				->join('\User\UserBundle\Entity\User', 'u')
				->where('u.id = p.niveau2')
				->andWhere('p.type = ' . \DPDCBundle\Entity\PointDeControle::CROISES)
				->andWhere('p.statut = ' . \DPDCBundle\Entity\PointDeControle::ACTIF);
		$niveau2s = $qb->getQuery()->getResult();
		$lastAndNext12Months = $this->lastAndNext12Months();
		$allMonths = $lastAndNext12Months['allMonths'];
		$plannedPDC = $lastAndNext12Months['plannedPDC'];
		$end = $lastAndNext12Months['end'];
		foreach ($allMonths as $mon) {
			foreach ($niveau2s as $point) {
				$point->getPointPlanning2($plannedPDC, $mon, $end);
			}
		}
		$resultats = $this->getDoctrine()
				->getRepository('DPDCBundle:PointDeControle')
				->getResultsForQualiteId(DateTime::createFromFormat('m-y', key($plannedPDC)), $userId);
		$resultatsMoisPointNiveau = array();
		foreach ($resultats as $resultat) {
			$resultatsMoisPointNiveau[$resultat['date']][$resultat['niveau2']]['results'][$resultat['niveau1']][$resultat['pointDeControle']] = $resultat['pointDeControle'];
		}
		$commentairesMoisNiveau2Niveau1 = $this->getDoctrine()
				->getRepository('DPDCBundle:CommentaireN2')
				->getCommentairesForQualitu(DateTime::createFromFormat('m-y', key($plannedPDC)));
		$commentairesMoisN2N1 = array();
		foreach ($commentairesMoisNiveau2Niveau1 as $comment) {
			$commentairesMoisN2N1[$comment['date']][$comment['niveau2']]['commentaires'][$comment['niveau1']][$comment['id']] = $comment['commentaire'];
		}
		$qualite = array();
		foreach ($plannedPDC as $mois => $niveau2) {
			if (count($niveau2)) {
				foreach ($niveau2 as $key => $data) {
					$qualite[$mois][$key] = 0;
					foreach ($data['planifies'] as $niveau1 => $planifies) {
						$results = (isset($resultatsMoisPointNiveau[$mois]) && isset($resultatsMoisPointNiveau[$mois][$key]) &&
								isset($resultatsMoisPointNiveau[$mois][$key]['results'][$niveau1])) ?
								count($resultatsMoisPointNiveau[$mois][$key]['results'][$niveau1]) : 0;
						$comments = (isset($commentairesMoisN2N1[$mois]) && isset($commentairesMoisN2N1[$mois][$key]) &&
								isset($commentairesMoisN2N1[$mois][$key]['commentaires'][$niveau1])) ?
								count($commentairesMoisN2N1[$mois][$key]['commentaires'][$niveau1]) : 0;
						$temp = ($results < count($planifies)) ? ($comments > 0 ? 1 : 2) : 0;
						$qualite[$mois][$key] = max($qualite[$mois][$key], $temp);
					}
				}
			}
		}
		return $this->render('DPDCBundle:PointDeControle:qualite.html.twig', array(
			'allMonths' => $allMonths,
			'plannedPDC' => $plannedPDC ? $plannedPDC : array(),
			'resultatsMoisPointNiveau' => $resultatsMoisPointNiveau ? $resultatsMoisPointNiveau : array(),
			'commentairesMoisN2N1' => $commentairesMoisN2N1 ? $commentairesMoisN2N1 : array(),
			'qualite' => $qualite,
			'leMois' => $leMois
		));
	}

	private function lastAndNext12Months() {
		$plannedPDC = array();
		$allMonths = array();
		$end = (new DateTime('+1 year'))->modify('first day of this month');
		$period = new DatePeriod((new DateTime('1 year ago'))->modify('first day of this month'), new DateInterval('P1M'), $end);
		foreach ($period as $dt) {
			$allMonths[$dt->format('m-y')] = $dt->format('m-y');
			$plannedPDC[$dt->format('m-y')] = array();
		}
		return array('plannedPDC' => $plannedPDC, 'allMonths' => $allMonths, 'end' => $end);
	}

	private function emailReminder($plannedPDC, $plannedN2, $resultatsPourN2, $allMonths) {
		$todayTest = new Datetime();
		$resultats = $this->getDoctrine()
				->getRepository('DPDCBundle:Resultat')
				->getResultsForEmailReminder(DateTime::createFromFormat('m-y', key($plannedPDC)));
		$resultatsMoisPointNiveau = array();
		foreach ($resultats as $resultat) {
			$resultatsMoisPointNiveau[$resultat['date']][$resultat['pointDeControle']]['niveau1'] = $resultat['niveau1'];
		}
		$emailsByUser = array();
		$emailToId = array();
		foreach ($plannedPDC as $month => $PDC) {
			// GET ONLY CURRENT MONTH
			if($month === $todayTest->format('m-y')){
				$dateIterator = DateTime::createFromFormat('m-y', $month);
				if ($dateIterator <= $todayTest && count($PDC)) {
					foreach ($PDC as $idPoint => $point) {
						$mailN1 = $point->getNiveau1()->getEmail();
						$idN1 = $point->getNiveau1()->getId();
						$emailToId[$mailN1] = $idN1;
						if(empty($resultatsMoisPointNiveau[$month])
						|| !array_key_exists($idPoint, $resultatsMoisPointNiveau[$month])){
							$emailsByUser[$mailN1][$month][$idPoint] = $point;
						}
					}
				}
			}
		}
		$part2 = array();
		foreach($allMonths as $oneMonth){
			// GET ONLY CURRENT MONTH
			if($oneMonth === $todayTest->format('m-y')){
				$dateIterator = DateTime::createFromFormat('m-y', $oneMonth);
				if(($dateIterator <= $todayTest) && !empty($plannedN2[$oneMonth])){
					foreach($plannedN2[$oneMonth] as $idNiveau1S => $niveau1S){
						foreach($niveau1S as $idNiveau2S => $niveau2S){
							$plannedNR = count($niveau2S['results']);
							$resultsNR = 0;
							if(empty($resultatsPourN2[$oneMonth])
							||empty($resultatsPourN2[$oneMonth][$idNiveau1S][$idNiveau2S])){
								$resultsNR = 0;
							}else{
								$resultsNR = count($resultatsPourN2[$oneMonth][$idNiveau1S][$idNiveau2S]['results']);
							}
							if($resultsNR < $plannedNR){
								$part2[$niveau2S['niveau2email']][$oneMonth][$niveau2S['niveau1']]['planned'] = $plannedNR;
								$part2[$niveau2S['niveau2email']][$oneMonth][$niveau2S['niveau1']]['results'] = $resultsNR;
							}
						}
					}
				}
			}
		}
		$combinedInfo = array();
		foreach($emailsByUser as $emailAdresse => $utilisateurMultipleReminder){
			foreach($utilisateurMultipleReminder as $mois => $point){
				foreach($point as $idPoint => $point){
					if(is_object($point)){
						$combinedInfo[$emailAdresse][$mois][$idPoint] = $utilisateurMultipleReminder[$mois][$idPoint];
					}
					if(!empty($part2[$emailAdresse]) && !empty($part2[$emailAdresse][$mois])){
						$combinedInfo[$emailAdresse][$mois]['niveau1S'] = $part2[$emailAdresse][$mois];
					}
				}				
			}
		}
		foreach ($combinedInfo as $emailAdresse => $utilisateurMultipleReminder) {
			$urlVisionN1 = 'http://' . $this->getRequest()->getHost() . $this->generateUrl('dpdc_vision_n1', array('niveau1' => $emailToId[$emailAdresse], 'niveau2' => null));
			$this->emailReminderNotChecked($emailAdresse, $utilisateurMultipleReminder, $urlVisionN1);
		}
//		return;
	}

	private function emailReminderNotChecked($to, $utilisateurMultipleReminder, $urlVisionN1) {
		$mailMessage = \Swift_Message::newInstance()
				->setContentType('text/html')
				->setSubject('Rappel Contrôles Croisés!')
				->setFrom('controlescroises@noz.fr')
				->setTo($to)
				->setBcc('poleweb@noz.fr')
				->setBody($this->renderView('DPDCBundle:email:emailReminder.html.twig', array(
													'utilisateurMultipleReminder' => $utilisateurMultipleReminder,
													'urlVisionN1' => $urlVisionN1
												)
											)
						);
		$this->get('mailer')->send($mailMessage);
	}

}
