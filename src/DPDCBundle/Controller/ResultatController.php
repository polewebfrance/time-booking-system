<?php

namespace DPDCBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use DPDCBundle\Entity\Resultat;
use DPDCBundle\Form\ResultatType;
use \DateTime;
use \DateInterval;
use \DatePeriod;

class ResultatController extends Controller
{
    /**
     * @Route("/resultat/modifier/{id}", name="dpdc_resultat_modifier")
     */
	public function modifierResultatAction($id, Request $request)
	{
		$user = $this->getUser();
		$userId = $user->getId();
    	$em = $this->getDoctrine()->getManager('default');
		$resultat = $this->getDoctrine()
			->getRepository('DPDCBundle:Resultat')
			->find((int)$id);
		if(!$resultat){
			return new JsonResponse('pas de resultat!');
		}
		$idPoint = $resultat->getPointDeControle();
		$month = $resultat->getDate()->format('m-y');
		$niveau = $resultat->getNiveau1() ? 1 : 0;
		$pointDeControle = $em->getRepository('DPDCBundle:PointDeControle')->find($idPoint);
		if(!$pointDeControle){
			return new JsonResponse('pas de point!');
		}
		if(!$pointDeControle->getNiveau1() || $pointDeControle->getNiveau1()->getId() !== $userId){
			return new JsonResponse('pas assigné a vous!');
		}
		if(!$pointDeControle->isValid()){
			return new JsonResponse('pas possible!');
		}
		$lePremierMois = $pointDeControle->getLePremierMois();
		$leMois = DateTime::createFromFormat('d-m-y H:i:s', '01-' . $month . ' 00:00:00');
		if($leMois < $lePremierMois){
			return new JsonResponse('pas en advance!');
		}
		$years = (int)$leMois->format('y') - (int)$lePremierMois->format('y');
		$months = $years > 0 ?
					12 - (int)$lePremierMois->format('m') + (int)$leMois->format('m') :
					(int)$leMois->format('m') - (int)$lePremierMois->format('m');
		$months += 12 * $years;
		$frequenceMois = $pointDeControle->getFrequence()/30;
		if((int)$months % $frequenceMois !== 0){
			return new JsonResponse('pas programme!');
		}
		switch($pointDeControle->getFrequence()){
			case 30 : $toModify = 0; break;
			case 60 :
			case 90 : $toModify = 7; break;
			case 120 :
			case 360 : $toModify = 15; break;
			default : $toModify = 0;
		}
		$now = new DateTime('now');
		$minimum = clone $leMois;
		$maximum = clone $leMois;
		$minimum->modify('-'.$toModify.' days');
		$maximum->modify('+1 month')->modify('+'.$toModify.' days');
		if($now < $minimum ||
			$now >= $maximum){
			return new JsonResponse('ni advance, ni apres!');
		}
		$form = $this->createForm(new ResultatType($idPoint, $month, $niveau), $resultat);
		$errors = 0;
		$form->handleRequest($request);
		if ($form->isValid()) {
			$resultat->setDateModified(new Datetime());
			$resultat->setModifiedBy($user->getDisplayName());
			$em = $this->getDoctrine()->getManager();
			$em->persist($resultat);
			$em->flush();
//			$this->get('session')->getFlashBag()->add('dpdcResultatSuccess', 'Le resultat est enregistré!');
//			return $this->redirect($this->generateUrl('dpdc_resultat_modifier', array('id' => $resultat->getId())));
			$this->get('session')->getFlashBag()->add('dpdcPlanningSuccess', 'Le resultat est modifié!');
			$errors = 0;
		}else{
			$errors = 1;
		}
        return $this->render('DPDCBundle:resultat:ajouterResultat.html.twig', array(
			'formResultat' => $form->createView(),
			'idResultat' => $resultat->getId(),
			'pointDeControle' => $pointDeControle,
			'user' => $user,
			'month' => $month,
			'niveau' => $niveau,
			'errors' => $errors,
		));
	}

    /**
     * @Route("/resultat/ajouter", name="dpdc_resultat_ajouter")
	 * @Method({"POST"})
     */
	public function ajouterResultatAction(Request $request)
	{
		$user = $this->getUser();
    	$em = $this->getDoctrine()->getManager('default');
		$userId = $user->getId();
		$resultat = new Resultat();
		$criteria = 'niveau1';
		if($request->request->get('dpdc_resultat')){
			$data = $request->request->get('dpdc_resultat');
			$idPoint = (int)$data['idPoint'];
			$month = $data['month'];
			$niveau = $data['niveau'];
		}else{
			$idPoint = $request->request->get('idPoint');
			$month = $request->request->get('month');
			$niveau = $request->request->get('niveau');
			$myData = explode('-', $month);
			$theMonth = $myData[0];
			$theYear = '20' . $myData[1];
			$oldResultat = $this->getDoctrine()
				->getRepository('DPDCBundle:Resultat')
				->findOneByYearMonth($theYear, $theMonth, (int)$idPoint, $criteria);
			if(count($oldResultat) > 0){
				return $this->redirect($this->generateUrl('dpdc_resultat_modifier', array('id' => $oldResultat[0]->getId())));
			}
		}
		$pointDeControle = $em->getRepository('DPDCBundle:PointDeControle')->find($idPoint);
		if(!$pointDeControle){
			return new JsonResponse('pas de point!');
		}
		if(!$pointDeControle->getNiveau1() || $pointDeControle->getNiveau1()->getId() !== $userId){
			return new JsonResponse('pas assigné a vous!');
		}
		if(!$pointDeControle->isValid()){
			return new JsonResponse('pas possible!');
		}
		$lePremierMois = $pointDeControle->getLePremierMois();
		$leMois = DateTime::createFromFormat('d-m-y H:i:s', '01-' . $month . ' 00:00:00');
		if(!$leMois){
			return new JsonResponse('pas de mois!');
		}
		if($leMois < $lePremierMois){
			return new JsonResponse('pas en advance!');
		}
		$years = (int)$leMois->format('y') - (int)$lePremierMois->format('y');
		$months = $years > 0 ?
					12 - (int)$lePremierMois->format('m') + (int)$leMois->format('m') :
					(int)$leMois->format('m') - (int)$lePremierMois->format('m');
		$months += 12 * $years;
		$frequenceMois = $pointDeControle->getFrequence()/30;
		if($months % $frequenceMois !== 0){
			return new JsonResponse('pas programme!');
		}
		switch($pointDeControle->getFrequence()){
			case 30 : $toModify = 0; break;
			case 60 :
			case 90 : $toModify = 7; break;
			case 120 :
			case 360 : $toModify = 15; break;
			default : $toModify = 0;
		}
		$now = new DateTime('now');
		$minimum = clone $leMois;
		$maximum = clone $leMois;
		$minimum->modify('-'.$toModify.' days');
		$maximum->modify('+1 month')->modify('+'.$toModify.' days');
		if($now < $minimum ||
			$now >= $maximum){
			return new JsonResponse('ni advance, ni apres!');
		}
		$commentaireNiveauN2 = $this->getDoctrine()
			->getRepository('DPDCBundle:CommentaireN2Mois')
			->findBy(array(
							'pointDeControle' => $idPoint,
							'niveau1' => $userId,
							'mois' => $leMois
						)
					);
		if($commentaireNiveauN2){
			return new JsonResponse('commentaire Niveau N2!');
		}
		$form = $this->createForm(new ResultatType($idPoint, $month, $criteria), $resultat);
		$errors = 0;
		$form->handleRequest($request);
		if ($form->isValid()) {
			$data = $request->request->get('dpdc_resultat');
			$resultat->setDateAdded(new Datetime());
			$resultat->setAddedBy($user->getDisplayName());
			$reponse = isset($data['reponse']) ? (int)$data['reponse'] : 0;
			$resultat->setReponse($reponse);
			$resultat->setPointDeControle($idPoint);
			$resultat->setDate(DateTime::createFromFormat('m-y', $month));
			$resultat->setNiveau1($userId);
			$em = $this->getDoctrine()->getManager();
			$em->persist($resultat);
			$em->flush();
//			$this->get('session')->getFlashBag()->add('dpdcResultatSuccess', 'Le resultat est enregistré!');
//			return $this->redirect($this->generateUrl('dpdc_resultat_modifier', array('id' => $resultat->getId())));
			$this->get('session')->getFlashBag()->add('dpdcPlanningSuccess', 'Le resultat est enregistré!');
			$errors = 0;
		}else{
			$errors = 1;
		}
        return $this->render('DPDCBundle:resultat:ajouterResultat.html.twig', array(
			'formResultat' => $form->createView(),
			'idResultat' => $resultat->getId(),
			'pointDeControle' => $pointDeControle,
			'user' => $user,
			'month' => $month,
			'niveau' => $criteria,
			'errors' => $errors,
		));
	}

}
