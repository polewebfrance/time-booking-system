function updateDirection(laDirection){
	$("#loader").show();
	$.ajax({
		dataType: "json",
		type: "POST",
		url: url_get_directions,
		data: { a : 1 },
		async:false,
		cache: false,
		error: function(jqXHR,textStatus, errorThrown ) {
//				alert(jqXHR.responseText); return false;
		},
		success: function(data){
			if (data !== 'pas de directions!') {
				var html = '';
				for(var k in data){
					if (data.hasOwnProperty(k)) {
						var selected = (laDirection == k ? 'selected=\'selected\'' : '');
						html += '<option value="' + k + '"' + selected + '>' + data[k] + '</option>';
					}
				}
				$('#direction').html(html);
			}
			else {
//					$('#detailSociete').html('pas de Arret trouve!');
			}
			$("#loader").hide();
		}
	});
	return false;
}
function updateService(leService){
	$("#loader").show();
//		var directionLibelle = $('#dpdc_point_de_controle_direction_chosen a span').html();
//		var directionLibelle = $('#direction option:selected').text();
	direction = $('#direction').val();
	$.ajax({
		dataType: "json",
		type: "POST",
		url: url_get_service_by_direction,
//			data: { directionLibelle : directionLibelle},
		data: { direction : direction},
		async:false,
		cache: false,
		error: function(jqXHR,textStatus, errorThrown ) {
//				alert(jqXHR.responseText); return false;
		},
		success: function(data){
			if (data !== 'pas de services!') {
				var html = '';
				for(var k in data){
					if (data.hasOwnProperty(k)) {
						var selected = (leService == k ? 'selected=\'selected\'' : '');
						html += '<option value="' + k + '"' + selected + '>' + data[k] + '</option>';
					}
				}
				$('#service').html(html);
			}
			else {
				$('#service').html('');
			}
			$("#loader").hide();
		}
	});
	return false;
}
function updatePole(lePole){
	$("#loader").show();
	service = $('#service').val();
	$.ajax({
		dataType: "json",
		type: "POST",
		url: url_get_pole_by_service,
		data: { service : service},
		async:false,
		cache: false,
		error: function(jqXHR,textStatus, errorThrown ) {
//				alert(jqXHR.responseText); return false;
		},
		success: function(data){
			if (data !== 'pas de poles!') {
				var html = '';
				for(var k in data){
					if (data.hasOwnProperty(k)) {
						var selected = (lePole == k ? 'selected=\'selected\'' : '');
						html += '<option value="' + k + '"' + selected + '>' + data[k] + '</option>';
					}
				}
				$('#pole').html(html);
			}
			else {
				$('#pole').html('');
			}
			$("#loader").hide();
		}
	});
	return false;
}

$(function(){
	$('.chosen').chosen();

		updateDirection(laDirection);
		updateService(leService);
		updatePole(lePole);
//		$('#dpdc_point_de_controle_direction_chosen').mouseup(function(){
		$('#direction').change(function(){
			updateService(0);
			updatePole(0);
		});
		$('#service').change(function(){
			updatePole(0);
		});

//	$(".date" ).datepicker({
//		altField: "#datepicker",
//		minDate:0,
//		showAnim:'slideDown',
//		showWeek:true,
////		numberOfMonths: 2,
//		closeText: 'Fermer',
//		prevText: 'Précédent',
//		nextText: 'Suivant',
//		currentText: 'Aujourd\'hui',
//		monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
//		monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
//		dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
//		dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
//		dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
//		weekHeader: '',
//		dateFormat: 'dd/mm/yy',
//		firstDay: 1
////				minDate: new Date(2015, 10, 1),
////				maxDate: new Date(2015, 11, 31),
////				dateFormat: 'DD, MM, d, yy',
////				constrainInput: true,
//	});


    $(".date").datepicker({
//        dateFormat: 'MM yy',
//        dateFormat: 'yy/mm/dd',
        dateFormat: 'yy MM dd',
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,

		minDate:0,
		showAnim:'slideDown',
		closeText: 'OK',
		prevText: 'Précédent',
		nextText: 'Suivant',
		currentText: 'Aujourd\'hui',
		monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
		monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
		dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
		dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
		dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
		weekHeader: '',
		firstDay: 1,

        onClose: function(dateText, inst) {
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
//            $(this).val($.datepicker.formatDate('MM yy', new Date(year, month, 1)));
            $(this).val(
				$.datepicker.formatDate(
				'MM yy', new Date(year, month, 1), {
						monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre']
					}
				)
			);
        }
    },$.datepicker.regional[ "fr" ]);

    $(".date").focus(function () {
        $(".ui-datepicker-calendar").hide();
        $("#ui-datepicker-div").position({
            my: "center top",
            at: "center bottom",
            of: $(this)
        });
    });
});
