<?php

namespace DPDCBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Resultat
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="DPDCBundle\Entity\ResultatRepository")
 * @ORM\Table(name="dpdc_resultats"))
 */
class Resultat
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="pointDeControle", type="integer")
     */
    private $pointDeControle;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=true)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="niveau1", type="integer", nullable=true)
     */
    private $niveau1;

    /**
     * @var boolean
     *
     * @ORM\Column(name="reponse", type="boolean")
     */
    private $reponse;

    /**
     * @var string
     *
     * @ORM\Column(name="commentaire", type="text", nullable=true)
     */
    private $commentaire;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateAdded", type="date", nullable=true)
     */
    private $dateAdded;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateModified", type="date", nullable=true)
     */
    private $dateModified;

    /**
     * @var string
     *
     * @ORM\Column(name="addedBy", type="string", length=255, nullable=true)
     */
    private $addedBy;

    /**
     * @var string
     *
     * @ORM\Column(name="modifiedBy", type="string", length=255, nullable=true)
     */
    private $modifiedBy;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pointDeControle
     *
     * @param integer $pointDeControle
     *
     * @return Resultat
     */
    public function setPointDeControle($pointDeControle)
    {
        $this->pointDeControle = $pointDeControle;

        return $this;
    }

    /**
     * Get pointDeControle
     *
     * @return integer
     */
    public function getPointDeControle()
    {
        return $this->pointDeControle;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Resultat
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set niveau1
     *
     * @param string $niveau1
     *
     * @return Resultat
     */
    public function setNiveau1($niveau1)
    {
        $this->niveau1 = $niveau1;

        return $this;
    }

    /**
     * Get niveau1
     *
     * @return string
     */
    public function getNiveau1()
    {
        return $this->niveau1;
    }

    /**
     * Set reponse
     *
     * @param boolean $reponse
     *
     * @return Resultat
     */
    public function setReponse($reponse)
    {
        $this->reponse = $reponse;

        return $this;
    }

    /**
     * Get reponse
     *
     * @return boolean
     */
    public function getReponse()
    {
        return $this->reponse;
    }

    /**
     * Set commentaire
     *
     * @param string $commentaire
     *
     * @return Resultat
     */
    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    /**
     * Get commentaire
     *
     * @return string
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }

    /**
     * Set dateAdded
     *
     * @param \DateTime $dateAdded
     *
     * @return Resultat
     */
    public function setDateAdded($dateAdded)
    {
        $this->dateAdded = $dateAdded;
        return $this;
    }

    /**
     * Get dateAdded
     *
     * @return \DateTime
     */
    public function getDateAdded()
    {
        return $this->dateAdded;
    }

    /**
     * Set dateModified
     *
     * @param \DateTime $dateModified
     *
     * @return Resultat
     */
    public function setDateModified($dateModified)
    {
        $this->dateModified = $dateModified;
        return $this;
    }

    /**
     * Get dateModified
     *
     * @return \DateTime
     */
    public function getDateModified()
    {
        return $this->dateModified;
    }

    /**
     * Set addedBy
     *
     * @param string $addedBy
     *
     * @return Resultat
     */
    public function setAddedBy($addedBy)
    {
        $this->addedBy = $addedBy;
        return $this;
    }

    /**
     * Get addedBy
     *
     * @return string
     */
    public function getAddedBy()
    {
        return $this->addedBy;
    }

    /**
     * Set modifiedBy
     *
     * @param string $modifiedBy
     *
     * @return Resultat
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;
        return $this;
    }

    /**
     * Get modifiedBy
     *
     * @return string
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }
}

