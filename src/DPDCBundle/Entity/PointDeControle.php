<?php

namespace DPDCBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * PointDeControle
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="DPDCBundle\Entity\PointDeControleRepository")
 * @ORM\Table(name="dpdc_point_de_controles"))
 */
class PointDeControle
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    const CROISES		= 1;
    const SUIVI_PROCESS	= 2;
	
	const NOUVEAU = 1;
	const ACTIF = 2;
	const INACTIF = 3;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=255)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

	 /**
     * @Assert\NotNull(
     *      message = "Pourriez-vous renseigner la Direction concernée par le contrôle?",
     * )
     * @ORM\ManyToOne(targetEntity="Administration\DirectionBundle\Entity\Direction")
     * @ORM\JoinColumn(name="direction", referencedColumnName="id")
     **/
    private $direction;

	 /**
     * @ORM\ManyToOne(targetEntity="Administration\DirectionBundle\Entity\Service")
     * @ORM\JoinColumn(name="service", referencedColumnName="id")
     **/
    private $service;

	 /**
     * @ORM\ManyToOne(targetEntity="Administration\DirectionBundle\Entity\Pole")
     * @ORM\JoinColumn(name="pole", referencedColumnName="id")
     **/
    private $pole;

    /**
     * @var string
     * @ORM\Column(name="titre", type="string", length=255)
     */
    private $titre;

    /**
     * @var string
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="frequence", type="integer", nullable=true)
     */
    private $frequence;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="lePremierMois", type="date", nullable=true)
     */
    private $lePremierMois;

    /**
     * @var string
     *
     * @ORM\Column(name="nomAAppeler", type="string", length=255, nullable=true)
     */
    private $nomAAppeler;

    /**
     * @var string
     *
     * @ORM\Column(name="prenomAAppeler", type="string", length=255, nullable=true)
     */
    private $prenomAAppeler;

    /**
     * @var string
     *
     * @ORM\Column(name="telephoneAAppeler", type="string", length=255, nullable=true)
     */
    private $telephoneAAppeler;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="integer", nullable=true)
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="statut", type="integer")
     */
    private $statut;

    /**
     * @var integer
     *
     * @ORM\Column(name="reponse", type="integer", nullable=true)
     */
    private $reponse;

	 /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="niveau1", referencedColumnName="id")
     **/
    private $niveau1;

	 /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="niveau2", referencedColumnName="id")
     **/
    private $niveau2;

	 /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="niveau3", referencedColumnName="id")
     **/
    private $niveau3;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateReponse", type="date", nullable=true)
     */
    private $dateReponse;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateAdded", type="date", nullable=true)
     */
    private $dateAdded;

    /**
     * @var string
     *
     * @ORM\Column(name="addedBy", type="string", length=255, nullable=true)
     */
    private $addedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateModified", type="date", nullable=true)
     */
    private $dateModified;

    /**
     * @var string
     *
     * @ORM\Column(name="modifiedBy", type="string", length=255, nullable=true)
     */
    private $modifiedBy;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->niveau1 = new \Doctrine\Common\Collections\ArrayCollection();
        $this->niveau2 = new \Doctrine\Common\Collections\ArrayCollection();
        $this->niveau3 = new \Doctrine\Common\Collections\ArrayCollection();
//        $this->direction = new \Doctrine\Common\Collections\ArrayCollection();
//        $this->service = new \Doctrine\Common\Collections\ArrayCollection();
//        $this->pole = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return PointDeControle
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return PointDeControle
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return PointDeControle
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set direction
     *
     * @param integer $direction
     *
     * @return PointDeControle
     */
    public function setDirection($direction)
    {
        $this->direction = $direction;
        return $this;
    }

    /**
     * Get direction
     *
     * @return integer
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * Set service
     *
     * @param integer $service
     *
     * @return PointDeControle
     */
    public function setService($service)
    {
        $this->service = $service;
        return $this;
    }

    /**
     * Get service
     *
     * @return integer
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Set pole
     *
     * @param integer $pole
     *
     * @return PointDeControle
     */
    public function setPole($pole)
    {
        $this->pole = $pole;
        return $this;
    }

    /**
     * Get pole
     *
     * @return integer
     */
    public function getPole()
    {
        return $this->pole;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return PointDeControle
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;
        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return PointDeControle
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set frequence
     *
     * @param integer $frequence
     *
     * @return PointDeControle
     */
    public function setFrequence($frequence)
    {
        $this->frequence = $frequence;
        return $this;
    }

    /**
     * Get frequence
     *
     * @return integer
     */
    public function getFrequence()
    {
        return $this->frequence;
    }

    /**
     * Set nomAAppeler
     *
     * @param string $nomAAppeler
     *
     * @return PointDeControle
     */
    public function setNomAAppeler($nomAAppeler)
    {
        $this->nomAAppeler = $nomAAppeler;
        return $this;
    }

    /**
     * Get nomAAppeler
     *
     * @return string
     */
    public function getNomAAppeler()
    {
        return $this->nomAAppeler;
    }

    /**
     * Set telephoneAAppeler
     *
     * @param string $telephoneAAppeler
     *
     * @return PointDeControle
     */
    public function setTelephoneAAppeler($telephoneAAppeler)
    {
        $this->telephoneAAppeler = $telephoneAAppeler;
        return $this;
    }

    /**
     * Get telephoneAAppeler
     *
     * @return string
     */
    public function getTelephoneAAppeler()
    {
        return $this->telephoneAAppeler;
    }

    /**
     * Set prenomAAppeler
     *
     * @param string $prenomAAppeler
     *
     * @return PointDeControle
     */
    public function setPrenomAAppeler($prenomAAppeler)
    {
        $this->prenomAAppeler = $prenomAAppeler;
        return $this;
    }

    /**
     * Get prenomAAppeler
     *
     * @return string
     */
    public function getPrenomAAppeler()
    {
        return $this->prenomAAppeler;
    }

    /**
     * Set lePremierMois
     *
     * @param \DateTime $lePremierMois
     *
     * @return PointDeControle
     */
    public function setLePremierMois($lePremierMois)
    {
        $this->lePremierMois = $lePremierMois;
        return $this;
    }

    /**
     * Get lePremierMois
     *
     * @return \DateTime
     */
    public function getLePremierMois()
    {
        return $this->lePremierMois;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return PointDeControle
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set reponse
     *
     * @param integer $reponse
     *
     * @return PointDeControle
     */
    public function setReponse($reponse)
    {
        $this->reponse = $reponse;
        return $this;
    }

    /**
     * Get reponse
     *
     * @return integer
     */
    public function getReponse()
    {
        return $this->reponse;
    }

    /**
     * Set statut
     *
     * @param integer $statut
     *
     * @return PointDeControle
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;
        return $this;
    }

    /**
     * Get statut
     *
     * @return integer
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * Set niveau1
     *
     * @param User\UserBundle\Entity\User
     * @return PointDeControle
     */
    public function setNiveau1($niveau1)
    {
        $this->niveau1 = $niveau1;
        return $this;
    }

    /**
     * Get niveau1
     *
     * @return \User\UserBundle\Entity\User
     */
    public function getNiveau1()
    {
        return $this->niveau1;
    }

    /**
     * Set niveau2
     *
     * @param User\UserBundle\Entity\User
     * @return PointDeControle
     */
    public function setNiveau2($niveau2)
    {
        $this->niveau2 = $niveau2;
        return $this;
    }

    /**
     * Get niveau2
     *
     * @return \User\UserBundle\Entity\User
     */
    public function getNiveau2()
    {
        return $this->niveau2;
    }

    /**
     * Set niveau3
     *
     * @param User\UserBundle\Entity\User
     * @return PointDeControle
     */
    public function setNiveau3($niveau3)
    {
        $this->niveau3 = $niveau3;
        return $this;
    }

    /**
     * Get niveau3
     *
     * @return \User\UserBundle\Entity\User
     */
    public function getNiveau3()
    {
        return $this->niveau3;
    }

    /**
     * Set dateReponse
     *
     * @param \DateTime $dateReponse
     *
     * @return PointDeControle
     */
    public function setDateReponse($dateReponse)
    {
        $this->dateReponse = $dateReponse;
        return $this;
    }

    /**
     * Get dateReponse
     *
     * @return \DateTime
     */
    public function getDateReponse()
    {
        return $this->dateReponse;
    }

    /**
     * Set dateAdded
     *
     * @param \DateTime $dateAdded
     *
     * @return PointDeControle
     */
    public function setDateAdded($dateAdded)
    {
        $this->dateAdded = $dateAdded;
        return $this;
    }

    /**
     * Get dateAdded
     *
     * @return \DateTime
     */
    public function getDateAdded()
    {
        return $this->dateAdded;
    }

    /**
     * Set addedBy
     *
     * @param string $addedBy
     *
     * @return PointDeControle
     */
    public function setAddedBy($addedBy)
    {
        $this->addedBy = $addedBy;
        return $this;
    }

    /**
     * Get addedBy
     *
     * @return string
     */
    public function getAddedBy()
    {
        return $this->addedBy;
    }

    /**
     * Set dateModified
     *
     * @param \DateTime $dateModified
     *
     * @return PointDeControle
     */
    public function setDateModified($dateModified)
    {
        $this->dateModified = $dateModified;
        return $this;
    }

    /**
     * Get dateModified
     *
     * @return \DateTime
     */
    public function getDateModified()
    {
        return $this->dateModified;
    }

    /**
     * Set modifiedBy
     *
     * @param string $modifiedBy
     *
     * @return PointDeControle
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;
        return $this;
    }

    /**
     * Get modifiedBy
     *
     * @return string
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }
	
	public function isValid(){		
		if(
			!$this->getNiveau1() ||
			!$this->getNiveau2() ||
			!is_object($this->getLePremierMois()) ||
			!$this->getFrequence() ||
			$this->getType() !== self::CROISES ||
			$this->getStatut() !== self::ACTIF
		){
			return false;
		}
		return true;
	}
	
	public function getPointPlanning2(&$plannedPDC, $mon, $end){
		if(is_object($this->getLePremierMois())
			&& $this->type === \DPDCBundle\Entity\PointDeControle::CROISES
			&& $this->statut === \DPDCBundle\Entity\PointDeControle::ACTIF){
			$plusOneMonth = clone $this->getLePremierMois();
			$nbMonths = $this->getFrequence() / 30;	
			while($plusOneMonth < $end){
				if($plusOneMonth->format('m-y') === $mon){
					$plannedPDC[$mon][$this->getNiveau2()->getId()]['nom'] = $this->getNiveau2()->getDisplayName();
					$plannedPDC[$mon][$this->getNiveau2()->getId()]['email'] = $this->getNiveau2()->getEmail();
					$plannedPDC[$mon][$this->getNiveau2()->getId()]['planifies'][$this->getNiveau1()->getId()][$this->getId()] = $this->getId();
				}
				$plusOneMonth->modify('+'.$nbMonths.' month');
			}
		}
	}
	
	public function getPointPlanning(&$plannedPDC, $mon, $end){
		if(is_object($this->getLePremierMois())
			&& $this->type === \DPDCBundle\Entity\PointDeControle::CROISES
			&& $this->statut === \DPDCBundle\Entity\PointDeControle::ACTIF){
			$plusOneMonth = clone $this->getLePremierMois();
			$nbMonths = $this->getFrequence() / 30;	
			while($plusOneMonth < $end){
				if($plusOneMonth->format('m-y') === $mon){
					$plannedPDC[$mon][(int)$this->getId()] = $this;
				}
				$plusOneMonth->modify('+'.$nbMonths.' month');
			}
		}
	}
	
	public function getPointManager(&$plannedPDC, $mon, $end){
		if(is_object($this->getLePremierMois())
			&& $this->getType() === \DPDCBundle\Entity\PointDeControle::CROISES
			&& $this->getStatut() === \DPDCBundle\Entity\PointDeControle::ACTIF){
			$plusOneMonth = clone $this->getLePremierMois();
			$nbMonths = $this->getFrequence() / 30;	
			while($plusOneMonth < $end){
				if($plusOneMonth->format('m-y') === $mon){
					$plannedPDC[$mon][(int)$this->getNiveau1()->getId()][(int)$this->getNiveau2()->getId()]['niveau2'] = $this->getNiveau2()->getId();
					$plannedPDC[$mon][(int)$this->getNiveau1()->getId()][(int)$this->getNiveau2()->getId()]['niveau2display'] = $this->getNiveau2()->getDisplayname();
					$plannedPDC[$mon][(int)$this->getNiveau1()->getId()][(int)$this->getNiveau2()->getId()]['niveau2email'] = $this->getNiveau2()->getEmail();
					$plannedPDC[$mon][(int)$this->getNiveau1()->getId()][(int)$this->getNiveau2()->getId()]['niveau1'] = $this->getNiveau1()->getDisplayname();
					$plannedPDC[$mon][(int)$this->getNiveau1()->getId()][(int)$this->getNiveau2()->getId()]['email'] = $this->getNiveau1()->getEmail();
					$plannedPDC[$mon][(int)$this->getNiveau1()->getId()][(int)$this->getNiveau2()->getId()]['results'][(int)$this->getId()] = (int)$this->getId();
				}
				$plusOneMonth->modify('+'.$nbMonths.' month');
			}
		}
	}
}

