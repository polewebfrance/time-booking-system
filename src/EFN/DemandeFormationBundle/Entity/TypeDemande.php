<?php

namespace EFN\DemandeFormationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TypeDemande
 *
 * @ORM\Table(name="efn_demandetype")
 * @ORM\Entity(repositoryClass="EFN\DemandeFormationBundle\Entity\TypeDemandeRepository")
 */
class TypeDemande
{
	
	
	
	const FORMATION_EXISTANTE       = 1;

	
	
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=255)
     */
    private $libelle;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return TypeDemande
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
    
        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }
    
    public function __toString()
    {
    	return $this->getLibelle();
    }
}
