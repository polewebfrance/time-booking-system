<?php

namespace User\UserBundle\Authentication;

use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Router;

/**
 * Class SuccessHandler
 * @package User\UserBundle\Authentication
 *
 * this class is the responsable for the redirection after login
 */
class SuccessHandler implements AuthenticationSuccessHandlerInterface
{

    protected $router;
    protected $security;

    public function __construct(Router $router, SecurityContext $security)
    {
        $this->router = $router;
        $this->security = $security;
    }

    /**
     * @param Request $request
     * @param TokenInterface $token
     * @return RedirectResponse
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {

        if (($this->security->isGranted('ROLE_TBS_ETUDES_USER')) or
            ($this->security->isGranted('ROLE_TBS_ETUDES_MANAGER'))
            or ($this->security->isGranted('ROLE_TBS_ETUDES_ADMIN'))) {
            return new RedirectResponse($this->router->generate('time_tbs_redirect'));
        } else
            return new RedirectResponse($this->router->generate('suivi_etudes_accueil'));

    }


}

