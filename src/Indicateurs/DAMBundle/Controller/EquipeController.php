<?php

namespace Indicateurs\DAMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Indicateurs\DAMBundle\Entity\Acheteur;
use Indicateurs\DAMBundle\Entity\AcheteurRepository;
use Indicateurs\DAMBundle\Entity\Team;
use Indicateurs\DAMBundle\Entity\Crm;
use Ob\HighchartsBundle\Highcharts\Highchart;
use Doctrine\Common\Util\Debug;


class EquipeController extends Controller
{
    protected $data = [];
    //protected $url = 'C:/wamp/www/data_dam_prod/';
    protected $url; // = 'C:/wamp/www/data_dam3/';
    protected $categories = [
        'sedentaires' => ['code' => 'SED', 'titre' => 'Sédentaire ', 'subcategs' => ['EUR', 'FR']],
        'terrains' =>    ['code' => 'TER', 'titre' => 'Terrain ',    'subcategs' => []],
        'roumanie' =>    ['code' => 'ROU', 'titre' => 'Sédentaire ', 'subcategs' => ['ROU', 'POL', 'BUL']],
        'france' =>      ['code' => 'FRA', 'titre' => 'Terrain ']
    ];
    protected $titres_tab = [
        'FRANCE' => 'FRANCE ',
        'EUROPE' => 'EUROPE ',
        'ROUMANIE' => 'ROUMANIE ',
        'MEDITERRANE' => 'Méditerranée'
    ];
    protected $categ_reverse = [
        'SED' => 'sedentaires',
        'FRA' => 'terrains',
        'TER' => 'terrains',
        'ROU' => 'roumanie'
    ];
    protected $last_week;

    public function __construct() {
        /*$this->categ_reverse = array_flip(array_map(function($element){
            return $element['code'];
        }, $this->categories));*/
        $this->last_week = time() - 7 * 24 * 3600;
    }

    /**
    * Calcul de pourcentage en prévoyant le cas $objectif = 0
    */
    private function pourcentage($objectif, $realisation) {
        if ($objectif === 0) return $realisation > 0 ? 1000 : 0;
        return round(100 * $realisation / $objectif);
    }

    /**
    * Retourne la catégorie
    */
    protected function getCategory($team_code) {
        if (substr($team_code, 0, 10) === 'SEDENTAIRE') {
            return 'SED';
        } elseif (substr($team_code, 0, 6) === 'FRANCE' || substr($team_code, 0, 3) === 'TER') {
            return 'TER';
        } elseif (substr($team_code, 0, 3) === 'SED') {
            return 'ROU';
        }
        return '';
    }

    /**
    * Crée et renvoie une liste des équipes auxquelles un manager a accès
    */
    private function findAllTeams($category) {
        $access = [];
        if (strlen ($category) > 1 && strpos($category, '_', 1)) {
            list($category, $subcat) = explode('_', $category);
        } else {
            $subcat = false;
        }
        //var_dump($this->categ_reverse);
        foreach ($this->categ_reverse as $categ_code => $categ) {
            //var_dump($category, $categ_code);
            if ($category === 'AllAccess' || $category === $categ) {
                $teams = $this->getDoctrine()->getRepository('IndicateursDAMBundle:Team')
                    ->findBy([
                        'category' => $categ_code,
                        'active'   => true
                    ]);
                if ($teams) {
                    //var_dump($teams);
                    if ($subcat) {
                        $teams2 = [];
                        //var_dump($teams);
                        if ($categ === 'terrains') {
                            if ($subcat === 'EUR') $subcat = 'TER';
                            foreach ($teams as $team) {
                                if (strpos($team->getTeamCode(), $subcat) === 0) $teams2[] = $team;
                            }
                        } elseif ($categ === 'roumanie') {
                            foreach ($teams as $team) {
                                $team_code = $team->getTeamCode();
                                if (strpos($team_code, '_')) {
                                    list(, $eastern_place) = explode('_', $team_code);
                                    //var_dump($eastern_place);
                                    if ($eastern_place === $subcat) $teams2[] = $team;
                                }
                            }
                        }
                        $teams = $teams2;
                    }
                    $access[$categ] = array_map(function($element){
                        return [
                            'code' => $element->getTeamCode(),
                            'name' => $element->getTeamName()
                        ];
                    }, $teams);
                    //die(var_dump($access));
                }
            }
        }
        //die(var_dump($access));
        return $access;
    }

    /**
    * Renvoie les données nécessaires pour adapter la barre latérale selon les droits de l'utilisateur
    */
    private function setMenu() {
        $em = $this->getDoctrine()->getManager();
        //die(var_dump($this->getUser()));
        $user_email = $this->getUser() ?
            //'adrion_re'
            //$this->getUser()->getEmailCanonical()
            //'mlolive@noz.fr'           //'lolive_mi';
            //'fxduslip@noz.fr'          //'xduslip_fl';
            //'kbellet@noz.fr'           //'bellet_ka';  // Ach Sed
            //'cpereira@noz.fr'          //'pereira_ca'; // Ach Sed
            //'agierek@veoworldwide.com' //'gierek_ag';  // Ach Rou
            //'iadjeri@noz.fr'           //'adjeri_in';  // Ach Ter
            //'non@noz.fr'               //'galben_co';  // Man Rou
            'clanger@noz.fr'           //'langer_ca';  // Man Sed
            //'yyaya@noz.fr'             //'yaya_yo';    // Man Ter
            //'tvesselinova@veoworldwide.com' //'vesselinova_th'; // Man Rou Bg
            //'mnicarel@zoom-corporation.ro'  //'nicarel_ma';     // Man Rou Ct
            //'okosa@zoom-corporation.ro'//              // TeamMan Allemagne BV
            //'istaver@veoworldwide.ro'  //              // TeamMan
            //'pjouin@noz.fr' //             // TeamMan Sed GC
            //'fadobritei@veoworldwide.ro'
            : '';
        //$this->getUser()->addRole('ROLE_PONEY');
        $crmUsers = $this->getDoctrine()->getRepository('IndicateursDAMBundle:Crm')
            ->findBy(['email' => $user_email]);
        if (!$crmUsers) {
            /*$pieceOfEmail = explode('@', $user_email)[0] . '@%';
            $crmQuery = $this->getDoctrine()->getRepository('IndicateursDAMBundle:Crm')->createQueryBuilder('crm')
                ->where('crm.email LIKE :pieceOfEmail')
                ->setParameter('pieceOfEmail', $pieceOfEmail);
            $crmUsers = $crmQuery->getQuery()->getResult();
            //die(var_dump($crmUsers));
            if(!$crmUsers) {*/
                return ['access' => []];
            /*}*/
        // s'il y en a plusieurs, sélectionne l'actif et/ou le dernier en date
        } elseif (count($crmUsers) > 1) {
            usort($crmUsers, function($crm1, $crm2) {
                if ($crm1->getActive() == $crm2->getActive()) {
                    return strcasecmp($crm1->getName(), $crm2->getName());
                } else {
                    return $crm1->getActive() ? 1 : -1;
                }
            });
        }
        $crmUser = array_pop($crmUsers);
        //die(var_dump($crmUser));
        //var_dump($crmUser);
        // valeurs par défaut
        $user_category = null;
        $access = [];
        // determination de la catégorie (et de l'équipe, le cas échéant) selon la fonction
        // si acheteur, retrouve son équipe
        $user_fonction = $crmUser->getFonction();
        if ($user_fonction === 'Acheteur') {
            $acheteur = $this->getDoctrine()->getRepository('IndicateursDAMBundle:Acheteur')
                ->findOneBy(['codeSap' => $crmUser->getCodeSap()]);
            //die(var_dump($acheteur));
            if (!$acheteur) {
                $acheteur = $this->getDoctrine()->getRepository('IndicateursDAMBundle:Acheteur')
                    ->findOneBy(['email' => $crmUser->getEmail()]);
                if (!$acheteur) return ['access' => []];
                $acheteur->setCodeSap($crmUser->getCodeSap());
                $acheteur->setTeam(null);
            }
            if ($acheteur->getTeam()) {
                $user_team = $acheteur->getTeam();
                $user_category = $this->categ_reverse[$user_team->getCategory()];
                $access[$user_category][] = [
                    'code' => $user_team->getTeamCode(),
                    'name' => $user_team->getTeamName()
                ];
            }
        // si manager, renvoie les équipes auxquelles il a accès
        } elseif ($user_fonction === 'Manager') {
            $team_manager = $this->getDoctrine()->getRepository('IndicateursDAMBundle:Acheteur')
                ->findOneBy(['email' => $crmUser->getEmail()]);
            //die(var_dump($team_manager));
            if ($team_manager && $team_manager->getTeam()) {
                $user_team = $team_manager->getTeam();
                $user_category = $this->categ_reverse[$user_team->getCategory()];
                $access[$user_category][] = [
                    'code' => $user_team->getTeamCode(),
                    'name' => $user_team->getTeamName()
                ];
            } else {
                $user_team = $crmUser->getTeamName();
                $arr1 = explode('_', $user_team);
                if ($arr1[0] === 'AllAccess') {
                    $user_category = 'AllAccess';
                } elseif ($arr1[0] === 'SED') {
                    $sed_subcats = $this->categories['sedentaires']['subcategs'];
                    $rou_subcats = $this->categories['roumanie']['subcategs'];
                    if (isset($arr1[1]) && in_array($arr1[1], $sed_subcats)) {
                        $user_category = 'sedentaires';
                    } elseif (isset($arr1[1]) && in_array($arr1[1], $rou_subcats)) {
                        $user_category = 'roumanie';
                        if (isset($arr1[3])) $subcat = $arr1[3];
                    }
                } elseif (in_array($arr1[0], ['ROU', 'TER', 'TR'])) {
                    //var_dump($user_team);
                    $category_code = $arr1[0] == 'TR' ? 'TER' : $arr1[0];
                    $user_category = $this->categ_reverse[$category_code];
                    if (isset($arr1[1])) $subcat = $arr1[1];
                    //die(var_dump($user_category, $subcat));
                }
                $access = isset($subcat) ? $this->findAllTeams($user_category . '_' . $subcat) : $this->findAllTeams($user_category);
            }
        } elseif ($user_fonction === 'Administrateur') {
            $user_category = 'AllAccess';
            $access = $this->findAllTeams('AllAccess');
        }
        //die(var_dump($user_category, $access));
        return [
            'email'    => $user_email,
            'fonction' => $user_fonction,
            'category' => $user_category,
            'access'   => $access
        ];
    }

    /**
    * Collecte et renvoie les données globales du pôle Achats
    */
    public function getGlobalData() {
        $menu_data = $this->setMenu();
        $page_global = "";
        $file_global = fopen($this->get('kernel')->getRootDir() . "/../web/data_dam/alpha_var.txt", "r"); //lecture du fichier
        while (!feof($file_global)) { //on parcourt toutes les lignes
            $page_global .= fgets($file_global, 4096) . "<br>";  // lecture du contenu de la ligne
        }
        fclose($file_global);
        $arr1 = explode(":", $page_global); //premiere division de la ligne
        //Récupération de l'objectif 
        $objectif_global = intval($arr1[1]);
        //récupération du réalisé
        $realise_brut = ltrim($arr1[2]);
        $realisation_global = intval(substr($realise_brut, stripos($realise_brut, " "))); 
        if (count($arr1) > 3) {
            $dossierCodac = intval($arr1[4]);
            $objdossierCodac = intval($arr1[3]);
        } else {
            $dossierCodac = 0;
            $objdossierCodac = 0;
        }
        $neg_global = stripos($realisation_global, "-");
        if ($neg_global != false) {
            $arr2 = explode("-", $realisation_global);
            $realisation_global = $arr2[0] * -1;
        }
        $pourcentage_global = $this->pourcentage($objectif_global, $realisation_global);
        return [
            'menu'               => $menu_data,
            'realisation_global' => $realisation_global,
            'objectif_global'    => $objectif_global,
            'pourcentage_global' => $pourcentage_global,
            'dossier_codac'      => $dossierCodac,
            'objdossier_codac'   => $objdossierCodac
        ];
    }

    /**
    * Renvoie les données globales du pôle Achats pour affichage de la page d'accueil
    */
    // indicateurs/dam/equipe/accueil
    public function accueilAction() {
        $this->data = $this->getGlobalData();
     	return $this->render('IndicateursDAMBundle:Equipe:accueil.html.twig', $this->data);
    }

    /**
    * Collecte et renvoie les données d'une catégorie d'équipes pour affichage
    */
    // indicateurs/dam/equipe/{category}
    public function voirEquipeAction($category){
        // la variable $category peut prendre les valeurs suivantes :
            // sedentaires
            // terrains
            // roumanie
        $em = $this->getDoctrine()->getManager();
        $data_global = $this->getGlobalData();
        $access = $data_global['menu']['access'];
        if (
            !array_key_exists($category, $this->categories)
            || !isset($access[$category])
        ) return $this->render('IndicateursDAMBundle:Equipe:accueil.html.twig', $data_global);
        $category_code = substr(strtoupper($category), 0, 3);
        // initialisation des valeurs totales
        $total_objectif = $total_realisation = 0;
        $total_obj_dossiers = $total_real_dossiers = 0;
        $liste_equipe = [];
        // recherche des équipes de la catégorie
        $teams = $this->getDoctrine()->getRepository('IndicateursDAMBundle:Team')
            ->findBy([
                'category' => $category_code,
                'active' => true
            ]);
        $access_short = array_map(function($element){
            return $element['code'];
        }, $access[$category]);
        foreach ($teams as $team) {
            $team =          $team->setTeamPourcentage();
            $objectif =      $team->getTeamObjectif();
            $realisation =   $team->getTeamRealisation();
            $obj_dossiers =  $team->getTeamObjDossiers();
            $real_dossiers = $team->getTeamRealDossiers();
            $liste_equipe[] = [
                'titre'            => $team->getTeamName(),
                'equipe'           => $team->getTeamCode(),
                'categorie'        => $category,
                'is_granted'       => in_array($team->getTeamCode(), $access_short),
                'objectif'         => $objectif,
                'realisation'      => $realisation,
                'pourcentage'      => $team->getTeamPourcentage(),
                'dossier_deposes'  => $real_dossiers,
                'dossier_objectif' => $obj_dossiers
            ];
            $total_objectif += $objectif;
            $total_realisation += $realisation;
            $total_obj_dossiers += $obj_dossiers;
            $total_real_dossiers += $real_dossiers;
        }
        // Tri du tableau
        $rang = [];
        foreach ($liste_equipe as $key => $row) {
            $rang[$key]  = $row['pourcentage'];
        }
        array_multisort($rang, SORT_DESC, $liste_equipe);
        //die(var_dump($liste_equipe));
        $this->data = $data_global + [
            'titre'                  => $category,
            'realisation_tt'         => $total_realisation,
            'objectif_tt'            => $total_objectif,
            'pourcentage_tt'         => $this->pourcentage($total_objectif, $total_realisation),
            'total_dossier_deposes'  => $total_real_dossiers,
            'total_objectif_dossier' => $total_obj_dossiers,
            'listeEquipe'            => $liste_equipe
        ];
        return $this->render('IndicateursDAMBundle:Equipe:equipeGeneral.html.twig', $this->data);
    }
    
    /**
    * Collecte et renvoie les données détaillées d'une équipe pour affichage
    */
    // indicateurs/dam/equipe/{category}/{equipe}
    public function voirDetailsAction ($equipe) {
        $data_global = $this->getGlobalData();
        $team = $this->getDoctrine()->getRepository('IndicateursDAMBundle:Team')
            ->findOneBy(['teamCode' => $equipe]);
        if(!$team) return $this->render('IndicateursDAMBundle:Equipe:accueil.html.twig', $data_global);
        $access = $data_global['menu']['access'];
        $category = $this->categ_reverse[$team->getCategory()];
        //die(var_dump($access, $category));
        if (!isset($access[$category])) return $this->render('IndicateursDAMBundle:Equipe:accueil.html.twig', $data_global);
        $access_short = array_map(function($element){
            return $element['code'];
        }, $access[$category]);
        //die(var_dump($access_short));
        if (!in_array($equipe, $access_short)) return $this->render('IndicateursDAMBundle:Equipe:accueil.html.twig', $data_global);
        $liste1 = $this->getDoctrine()->getRepository('IndicateursDAMBundle:Acheteur')
            ->findBy([
                'team' => $team,
                'active' => 1
            ]);
        $listeAcheteurs = [];
        foreach ($liste1 as $acheteur) {
            if (!$acheteur->isManager()) {
                $acheteur = $acheteur->setPourcentage();
                $listeAcheteurs[] = $acheteur;
            }
        }
        //regroupement des tableaux en un seul, pour l'affichage
        $team = $team->setTeamPourcentage();
        $this->data = $data_global + [
            'equipe'         => $equipe,
            'total_dossier'  => $team->getTeamRealDossiers(),
            'obj_dossiers'   => $team->getTeamObjDossiers(),
            'realisation_eq' => $team->getTeamRealisation(),
            'objectif_eq'    => $team->getTeamObjectif(),
            'pourcentage_eq' => $team->getTeamPourcentage(),
            'listeAcheteurs' => $listeAcheteurs,
            'titre'          => $team->getTeamName()
        ];
        return $this->render('IndicateursDAMBundle:Equipe:equipe.html.twig', $this->data);
    }
}