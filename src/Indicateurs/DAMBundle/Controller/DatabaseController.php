<?php

namespace Indicateurs\DAMBundle\Controller;

use Indicateurs\DAMBundle\Controller\EquipeController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Indicateurs\DAMBundle\Entity\Acheteur;
use Indicateurs\DAMBundle\Entity\AcheteurRepository;
use Indicateurs\DAMBundle\Entity\Crm;
use Indicateurs\DAMBundle\Entity\Team;
use Doctrine\Common\Util\Debug;

class DatabaseController extends EquipeController
{
    private $file_array = [];

    /**
    * Vide les tables adm_teams et adm_acheteurs de la base de données
    */
    public function resetDBAction() {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            'DELETE FROM IndicateursDAMBundle:Acheteur p'
        );
        $result1 = $query->getResult();

        $query = $em->createQuery(
            'DELETE FROM IndicateursDAMBundle:Team p'
        );
        $result2 = $query->getResult();

        return $this->render('IndicateursDAMBundle:Default:index.html.twig', array('name' => $result1 . ' et ' . $result2 . ' '));
    }

    /**
    * Vide la table adm_crm de la base de données
    */
    public function resetCrmDBAction() {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            'DELETE FROM IndicateursDAMBundle:Crm p
            WHERE p.id > 1'
        );
        $result1 = $query->getResult();

        return $this->render('IndicateursDAMBundle:Default:index.html.twig', array('name' => $result1));
    }

    /**
    * Entre les données utiles du fichier crmFile dans la table adm_crm de la base de données
    */
/*    private function getCrmToDB() {
        $lines = file($this->url . "crm.txt");
        $em = $this->getDoctrine()->getManager();
        foreach ($lines as $line) {
            // création d'un tableau
            $arr1 = explode(";", $line);
            if (count($arr1) < 8) continue;
            //$active = $arr1[5];
            if (!$arr1[5]) continue;
            $fonction = $arr1[3];
            // elimine ceux qui ne sont ni acheteurs ni managers
            if (!in_array($fonction, ['Manager', 'Acheteur'])) continue;
            // elimine les acheteurs hors terrain et sédentaires
            $team_arr = explode("_", $arr1[7], 2);
            if ($team_arr[0] === 'TR') {
                $arr1[7] = substr_replace($arr1[7], 'TER', 0, 2);
            } elseif (!array_key_exists($team_arr[0], $this->categ_reverse)) {
                continue;
            }
            // elimine ceux qui n'ont pas d'id Win
            $idWin = strtolower($arr1[2]);
            if (!trim($idWin) || $idWin === 'xxx') continue;
            // inactive les acheteurs sans code SAP
            // if ($fonction === 'Acheteur' && strtoupper($arr1[1]) === 'XXX') $active = 0; //continue;
            // elimine les doublons éventuels
            $crm = $this->getDoctrine()->getRepository('IndicateursDAMBundle:Crm')
                ->findOneBy(['nrId' => $arr1[0]]);
            if (!$crm) {
                $crm = new Crm();
                $crm->setNrId($arr1[0]);
                $crm->setCodeSap($arr1[1]);
                $crm->setIdWin($idWin);
                $crm->setFonction($fonction);
                $crm->setName($arr1[4]);
                $crm->setActive($arr1[5]);
                $crm->setEmail(strtolower($arr1[6]));
                $crm->setTeamName($arr1[7]);
            }

            $em->persist($crm);
            $em->flush();
        }
    }*/

    /**
    * Remplit et met à jour les tables de la base de données
    */
    /*public function getCrmDBAction() {
        $this->url = $this->get('kernel')->getRootDir() . "/../web/data_dam/";
        $this->getCrmToDB();
        return new RedirectResponse($this->generateUrl('indicateurs_codac_accueil'));
    }*/

}