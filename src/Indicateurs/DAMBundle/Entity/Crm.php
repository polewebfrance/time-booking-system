<?php

namespace Indicateurs\DAMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Crm
 *
 * @ORM\Table(name="adm_crm")
 * @ORM\Entity(repositoryClass="Indicateurs\DAMBundle\Entity\AcheteurRepository")
 */
class Crm
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="nrid", type="integer")
     */
    private $nrId;

    /**
     * @var string
     *
     * @ORM\Column(name="code_sap", type="string", length=10)
     */
    private $codeSap;

    /**
     * @var string
     *
     * @ORM\Column(name="id_win", type="string", length=50)
     */
    private $idWin;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", length=255)
     */
    private $fonction;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $active;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="team_name", type="string", length=255)
     */
    private $teamName;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nrId
     *
     * @param integer $nrId
     *
     * @return Crm
     */
    public function setNrId($nrId)
    {
        $this->nrId = $nrId;

        return $this;
    }

    /**
     * Get nrId
     *
     * @return integer
     */
    public function getNrId()
    {
        return $this->nrId;
    }


    /**
     * Set codeSap
     *
     * @param string $codeSap
     *
     * @return Crm
     */
    public function setCodeSap($codeSap)
    {
        $this->codeSap = $codeSap;

        return $this;
    }

    /**
     * Get codeSap
     *
     * @return string
     */
    public function getCodeSap()
    {
        return $this->codeSap;
    }

    /**
     * Set idWin
     *
     * @param string $idWin
     *
     * @return Crm
     */
    public function setIdWin($idWin)
    {
        $this->idWin = $idWin;

        return $this;
    }

    /**
     * Get idWin
     *
     * @return string
     */
    public function getIdWin()
    {
        return $this->idWin;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Crm
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set fonction
     *
     * @param string $fonction
     *
     * @return Crm
     */
    public function setFonction($fonction)
    {
        $this->fonction = $fonction;

        return $this;
    }

    /**
     * Get fonction
     *
     * @return string
     */
    public function getFonction()
    {
        return $this->fonction;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Crm
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Crm
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set teamName
     *
     * @param string $teamName
     *
     * @return Crm
     */
    public function setTeamName($teamName)
    {
        $this->teamName = $teamName;

        return $this;
    }

    /**
     * Get teamName
     *
     * @return string
     */
    public function getTeamName()
    {
        return $this->teamName;
    }

    
}