<?php

namespace Indicateurs\DAMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Team
 *
 * @ORM\Table(name="adm_teams")
 * @ORM\Entity
 */
class Team
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="categorie", type="string", length=10)
     */
    private $category;

    /**
     * @var string
     *
     * @ORM\Column(name="team_name", type="string", length=50)
     */
    private $teamName;

    /**
     * @var string
     *
     * @ORM\Column(name="team_code", type="string", length=100)
     */
    private $teamCode;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $active;

    /**
     * @var integer
     *
     * @ORM\Column(name="objectif", type="integer")
     */
    private $teamObjectif;

    /**
     * @var integer
     *
     * @ORM\Column(name="realisation", type="integer")
     */
    private $teamRealisation;

    private $teamPourcentage;

    /**
     * @var integer
     *
     * @ORM\Column(name="obj_dossiers", type="integer")
     */
    private $teamObjDossiers;

    /**
     * @var integer
     *
     * @ORM\Column(name="real_dossiers", type="integer")
     */
    private $teamRealDossiers;

    public function __construct() {
        $this->setTeamPourcentage();
    }

    public function __toString() {
        return '' . $this->id;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set category
     *
     * @param string $category
     *
     * @return Team
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set teamName
     *
     * @param string $teamName
     *
     * @return Team
     */
    public function setTeamName($teamName)
    {
        $this->teamName = $teamName;

        return $this;
    }

    /**
     * Get teamName
     *
     * @return string
     */
    public function getTeamName()
    {
        return $this->teamName;
    }

    /**
     * Set teamCode
     *
     * @param string $teamCode
     *
     * @return Team
     */
    public function setTeamCode($teamCode)
    {
        $this->teamCode = $teamCode;

        return $this;
    }

    /**
     * Get teamCode
     *
     * @return string
     */
    public function getTeamCode()
    {
        return $this->teamCode;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Team
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set teamObjectif
     *
     * @param integer $teamObjectif
     *
     * @return Team
     */
    public function setTeamObjectif($teamObjectif)
    {
        $this->teamObjectif = $teamObjectif;

        return $this;
    }

    /**
     * Get teamObjectif
     *
     * @return integer
     */
    public function getTeamObjectif()
    {
        return $this->teamObjectif;
    }

    /**
     * Set teamRealisation
     *
     * @param integer $teamRealisation
     *
     * @return Team
     */
    public function setTeamRealisation($teamRealisation)
    {
        $this->teamRealisation = $teamRealisation;

        return $this;
    }

    /**
     * Get teamRealisation
     *
     * @return integer
     */
    public function getTeamRealisation()
    {
        return $this->teamRealisation;
    }

    /**
     * Set teamPourcentage
     *
     * @return Acheteur
     */
    public function setTeamPourcentage()
    {
        if ($this->teamObjectif) {
            $this->teamPourcentage = round(100 * $this->teamRealisation / $this->teamObjectif);
        } else {
            $this->teamPourcentage = $this->teamRealisation > 0 ? 1000 : 0;
        }

        return $this;
    }

    /**
     * Get teamPourcentage
     *
     * @return integer
     */
    public function getTeamPourcentage()
    {
        return $this->teamPourcentage;
    }

    /**
     * Set teamObjDossiers
     *
     * @param integer $teamObjDossiers
     *
     * @return Team
     */
    public function setTeamObjDossiers($teamObjDossiers)
    {
        $this->teamObjDossiers = $teamObjDossiers;

        return $this;
    }

    /**
     * Get teamObjDossiers
     *
     * @return integer
     */
    public function getTeamObjDossiers()
    {
        return $this->teamObjDossiers;
    }

    /**
     * Set teamRealDossiers
     *
     * @param integer $teamRealDossiers
     *
     * @return Team
     */
    public function setTeamRealDossiers($teamRealDossiers)
    {
        $this->teamRealDossiers = $teamRealDossiers;

        return $this;
    }

    /**
     * Get teamRealDossiers
     *
     * @return integer
     */
    public function getTeamRealDossiers()
    {
        return $this->teamRealDossiers;
    }
}

