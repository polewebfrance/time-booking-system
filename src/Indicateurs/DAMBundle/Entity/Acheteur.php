<?php

namespace Indicateurs\DAMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Acheteur
 *
 * @ORM\Table(name="adm_acheteurs")
 * @ORM\Entity(repositoryClass="Indicateurs\DAMBundle\Entity\AcheteurRepository")
 */
class Acheteur
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User")
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="code_sap", type="string", length=10)
     */
    private $codeSap;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_manager", type="boolean")
     */
    private $fonction;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $active;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=100)
     */
    private $email;

    /**
     * @ORM\ManyToOne(targetEntity="Team")
     */
    private $team;

    /**
     * @var integer
     *
     * @ORM\Column(name="objectif", type="integer")
     */
    private $objectif;

    /**
     * @var integer
     *
     * @ORM\Column(name="realisation", type="integer")
     */
    private $realisation;

    /**
     * @var integer
     */
    private $pourcentage;

    /**
     * @var integer
     *
     * @ORM\Column(name="obj_dossiers", type="integer")
     */
    private $objDossiers;

    /**
     * @var integer
     *
     * @ORM\Column(name="real_dossiers", type="integer")
     */
    private $realDossiers;

    public function __construct() {
        $this->setPourcentage();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \User\UserBundle\Entity\User $user
     *
     * @return Acheteur
     */
    public function setUser(\User\UserBundle\Entity\User $user = null) {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \User\UserBundle\Entity\User
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Set codeSap
     *
     * @param string $codeSap
     *
     * @return Acheteur
     */
    public function setCodeSap($codeSap) {
        $this->codeSap = $codeSap;

        return $this;
    }

    /**
     * Get codeSap
     *
     * @return string
     */
    public function getCodeSap() {
        return $this->codeSap;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Acheteur
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set fonction
     *
     * @param string $fonc_code
     *
     * @return Acheteur
     */
    public function setFonction($fonc_code) {
        $this->fonction = $fonc_code === 'PM';

        return $this;
    }

    /**
     * Get fonction
     *
     * @return string
     */
    public function getFonction() {
        return $this->fonction ? 'PM' : 'AC';
    }

    /**
     * Get fonction
     *
     * @return boolean
     */
    public function isManager() {
        return $this->fonction;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Acheteur
     */
    public function setActive($active) {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive() {
        return $this->active;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Acheteur
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Set team
     *
     * @param \Indicateurs\DAMBundle\Entity\Team $team
     *
     * @return Acheteur
     */
    public function setTeam(\Indicateurs\DAMBundle\Entity\Team $team = null) {
        $this->team = $team;

        return $this;
    }

    /**
     * Get team
     *
     * @return \Indicateurs\DAMBundle\Entity\Team
     */
    public function getTeam() {
        return $this->team;
    }

    /**
     * Set objectif
     *
     * @param integer $objectif
     *
     * @return Acheteur
     */
    public function setObjectif($objectif)
    {
        $this->objectif = $objectif;

        return $this;
    }

    /**
     * Get objectif
     *
     * @return integer
     */
    public function getObjectif()
    {
        return $this->objectif;
    }

    /**
     * Set realisation
     *
     * @param integer $realisation
     *
     * @return Acheteur
     */
    public function setRealisation($realisation)
    {
        $this->realisation = $realisation;

        return $this;
    }

    /**
     * Get realisation
     *
     * @return integer
     */
    public function getRealisation()
    {
        return $this->realisation;
    }

    /**
     * Set pourcentage
     *
     * @return Acheteur
     */
    public function setPourcentage()
    {
        if ($this->objectif) {
            $this->pourcentage = round(100 * $this->realisation / $this->objectif);
        } else {
            $this->pourcentage = $this->realisation > 0 ? 1000 : 0;
        }

        return $this;
    }

    /**
     * Get pourcentage
     *
     * @return integer
     */
    public function getPourcentage()
    {
        return $this->pourcentage;
    }

    /**
     * Set objDossiers
     *
     * @param integer $objDossiers
     *
     * @return Acheteur
     */
    public function setObjDossiers($objDossiers)
    {
        $this->objDossiers = $objDossiers;

        return $this;
    }

    /**
     * Get objDossiers
     *
     * @return integer
     */
    public function getObjDossiers()
    {
        return $this->objDossiers;
    }

    /**
     * Set realDossiers
     *
     * @param integer $realDossiers
     *
     * @return Acheteur
     */
    public function setRealDossiers($realDossiers)
    {
        $this->realDossiers = $realDossiers;

        return $this;
    }

    /**
     * Get realDossiers
     *
     * @return integer
     */
    public function getRealDossiers()
    {
        return $this->realDossiers;
    }

}

