<?php

namespace Formulaire\dmliBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PolePol
 *
 * @ORM\Table(name="pole_pol", indexes={@ORM\Index(name="fk_pole_pol_service_srv1_idx", columns={"srv_id"})})
 * @ORM\Entity
 */
class PolePol
{
    /**
     * @var string
     *
     * @ORM\Column(name="pol_libelle", type="string", length=70, nullable=false)
     */
    private $polLibelle;

    /**
     * @var integer
     *
     * @ORM\Column(name="pol_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $polId;

    /**
     * @ORM\ManyToOne(targetEntity="\Administration\DirectionBundle\Entity\Service", inversedBy="poles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $srv;


    /**
     * Set polLibelle
     *
     * @param string $polLibelle
     *
     * @return PolePol
     */
    public function setPolLibelle($polLibelle)
    {
        $this->polLibelle = $polLibelle;
    
        return $this;
    }

    /**
     * Get polLibelle
     *
     * @return string
     */
    public function getPolLibelle()
    {
        return $this->polLibelle;
    }

    /**
     * Get polId
     *
     * @return integer
     */
    public function getPolId()
    {
        return $this->polId;
    }

    /**
     * Set srv
     *
     * @param \Administration\DirectionBundle\Entity\Service $srv
     *
     * @return PolePol
     */
    public function setSrv(\Administration\DirectionBundle\Entity\Service $srv = null)
    {
        $this->srv = $srv;
    
        return $this;
    }

    /**
     * Get srv
     *
     * @return \Administration\DirectionBundle\Entity\Service
     */
    public function getSrv()
    {
        return $this->srv;
    }
}

