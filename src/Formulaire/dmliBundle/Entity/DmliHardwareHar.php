<?php

namespace Formulaire\dmliBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DmliHardwareHar
 *
 * @ORM\Table(name="dmli_hardware_har", indexes={@ORM\Index(name="fk_mat_typ_id", columns={"typ_id"})})
 * @ORM\Entity
 */
class DmliHardwareHar
{
    /**
     * @var string
     *
     * @ORM\Column(name="har_libelle", type="string", length=70, nullable=false)
     */
    private $harLibelle;

    /**
     * @var string
     *
     * @ORM\Column(name="har_photo", type="string", length=70, nullable=true)
     */
    private $harPhoto;

    /**
     * @var string
     *
     * @ORM\Column(name="har_description", type="string", length=500, nullable=true)
     */
    private $harDescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="seuilalerte", type="integer", nullable=true)
     */
    private $seuilalerte;

    /**
     * @var integer
     *
     * @ORM\Column(name="seuilsecurite", type="integer", nullable=true)
     */
    private $seuilsecurite;

    /**
     * @var string
     *
     * @ORM\Column(name="har_prix", type="decimal", precision=5, scale=2, nullable=true)
     */
    private $harPrix;

    /**
     * @var integer
     *
     * @ORM\Column(name="stockdisponible", type="integer", nullable=false)
     */
    private $stockdisponible;

    /**
     * @var integer
     *
     * @ORM\Column(name="stockreel", type="integer", nullable=false)
     */
    private $stockreel;

    /**
     * @var boolean
     *
     * @ORM\Column(name="visible", type="boolean", nullable=false)
     */
    private $visible = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="supprimer", type="boolean", nullable=false)
     */
    private $supprimer = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="har_svp", type="integer", nullable=false)
     */
    private $harSvp = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="har_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $harId;

    /**
     * @var \Formulaire\dmliBundle\Entity\Typemateriel
     *
     * @ORM\ManyToOne(targetEntity="Formulaire\dmliBundle\Entity\Typemateriel")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="typ_id", referencedColumnName="id")
     * })
     */
    private $typ;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Formulaire\dmliBundle\Entity\DmliPackhardwarePkh", mappedBy="har")
     */
    private $pkh;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Formulaire\dmliBundle\Entity\PosteBridge", inversedBy="har")
     * @ORM\JoinTable(name="dmli_bdg_has_har",
     *   joinColumns={
     *     @ORM\JoinColumn(name="har_id", referencedColumnName="har_id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="bdg_id", referencedColumnName="bdg_id")
     *   }
     * )
     */
    private $bdg;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pkh = new \Doctrine\Common\Collections\ArrayCollection();
        $this->bdg = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set harLibelle
     *
     * @param string $harLibelle
     *
     * @return DmliHardwareHar
     */
    public function setHarLibelle($harLibelle)
    {
        $this->harLibelle = $harLibelle;
    
        return $this;
    }

    /**
     * Get harLibelle
     *
     * @return string
     */
    public function getHarLibelle()
    {
        return $this->harLibelle;
    }

    /**
     * Set harPhoto
     *
     * @param string $harPhoto
     *
     * @return DmliHardwareHar
     */
    public function setHarPhoto($harPhoto)
    {
        $this->harPhoto = $harPhoto;
    
        return $this;
    }

    /**
     * Get harPhoto
     *
     * @return string
     */
    public function getHarPhoto()
    {
        return $this->harPhoto;
    }

    /**
     * Set harDescription
     *
     * @param string $harDescription
     *
     * @return DmliHardwareHar
     */
    public function setHarDescription($harDescription)
    {
        $this->harDescription = $harDescription;
    
        return $this;
    }

    /**
     * Get harDescription
     *
     * @return string
     */
    public function getHarDescription()
    {
        return $this->harDescription;
    }

    /**
     * Set seuilalerte
     *
     * @param integer $seuilalerte
     *
     * @return DmliHardwareHar
     */
    public function setSeuilalerte($seuilalerte)
    {
        $this->seuilalerte = $seuilalerte;
    
        return $this;
    }

    /**
     * Get seuilalerte
     *
     * @return integer
     */
    public function getSeuilalerte()
    {
        return $this->seuilalerte;
    }

    /**
     * Set seuilsecurite
     *
     * @param integer $seuilsecurite
     *
     * @return DmliHardwareHar
     */
    public function setSeuilsecurite($seuilsecurite)
    {
        $this->seuilsecurite = $seuilsecurite;
    
        return $this;
    }

    /**
     * Get seuilsecurite
     *
     * @return integer
     */
    public function getSeuilsecurite()
    {
        return $this->seuilsecurite;
    }

    /**
     * Set harPrix
     *
     * @param string $harPrix
     *
     * @return DmliHardwareHar
     */
    public function setHarPrix($harPrix)
    {
        $this->harPrix = $harPrix;
    
        return $this;
    }

    /**
     * Get harPrix
     *
     * @return string
     */
    public function getHarPrix()
    {
        return $this->harPrix;
    }

    /**
     * Set stockdisponible
     *
     * @param integer $stockdisponible
     *
     * @return DmliHardwareHar
     */
    public function setStockdisponible($stockdisponible)
    {
        $this->stockdisponible = $stockdisponible;
    
        return $this;
    }

    /**
     * Get stockdisponible
     *
     * @return integer
     */
    public function getStockdisponible()
    {
        return $this->stockdisponible;
    }

    /**
     * Set stockreel
     *
     * @param integer $stockreel
     *
     * @return DmliHardwareHar
     */
    public function setStockreel($stockreel)
    {
        $this->stockreel = $stockreel;
    
        return $this;
    }

    /**
     * Get stockreel
     *
     * @return integer
     */
    public function getStockreel()
    {
        return $this->stockreel;
    }

    /**
     * Set visible
     *
     * @param boolean $visible
     *
     * @return DmliHardwareHar
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;
    
        return $this;
    }

    /**
     * Get visible
     *
     * @return boolean
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * Set supprimer
     *
     * @param boolean $supprimer
     *
     * @return DmliHardwareHar
     */
    public function setSupprimer($supprimer)
    {
        $this->supprimer = $supprimer;
    
        return $this;
    }

    /**
     * Get supprimer
     *
     * @return boolean
     */
    public function getSupprimer()
    {
        return $this->supprimer;
    }

    /**
     * Set harSvp
     *
     * @param integer $harSvp
     *
     * @return DmliHardwareHar
     */
    public function setHarSvp($harSvp)
    {
        $this->harSvp = $harSvp;
    
        return $this;
    }

    /**
     * Get harSvp
     *
     * @return integer
     */
    public function getHarSvp()
    {
        return $this->harSvp;
    }

    /**
     * Get harId
     *
     * @return integer
     */
    public function getHarId()
    {
        return $this->harId;
    }

    /**
     * Set typ
     *
     * @param \Formulaire\dmliBundle\Entity\Typemateriel $typ
     *
     * @return DmliHardwareHar
     */
    public function setTyp(\Formulaire\dmliBundle\Entity\Typemateriel $typ = null)
    {
        $this->typ = $typ;
    
        return $this;
    }

    /**
     * Get typ
     *
     * @return \Formulaire\dmliBundle\Entity\Typemateriel
     */
    public function getTyp()
    {
        return $this->typ;
    }

    /**
     * Add pkh
     *
     * @param \Formulaire\dmliBundle\Entity\DmliPackhardwarePkh $pkh
     *
     * @return DmliHardwareHar
     */
    public function addPkh(\Formulaire\dmliBundle\Entity\DmliPackhardwarePkh $pkh)
    {
        $this->pkh[] = $pkh;
    
        return $this;
    }

    /**
     * Remove pkh
     *
     * @param \Formulaire\dmliBundle\Entity\DmliPackhardwarePkh $pkh
     */
    public function removePkh(\Formulaire\dmliBundle\Entity\DmliPackhardwarePkh $pkh)
    {
        $this->pkh->removeElement($pkh);
    }

    /**
     * Get pkh
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPkh()
    {
        return $this->pkh;
    }

    /**
     * Add bdg
     *
     * @param \Formulaire\dmliBundle\Entity\PosteBridge $bdg
     *
     * @return DmliHardwareHar
     */
    public function addBdg(\Formulaire\dmliBundle\Entity\PosteBridge $bdg)
    {
        $this->bdg[] = $bdg;
    
        return $this;
    }

    /**
     * Remove bdg
     *
     * @param \Formulaire\dmliBundle\Entity\PosteBridge $bdg
     */
    public function removeBdg(\Formulaire\dmliBundle\Entity\PosteBridge $bdg)
    {
        $this->bdg->removeElement($bdg);
    }

    /**
     * Get bdg
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBdg()
    {
        return $this->bdg;
    }
}

