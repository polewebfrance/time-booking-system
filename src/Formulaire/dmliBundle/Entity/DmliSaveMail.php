<?php

namespace Formulaire\dmliBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DmliSaveMail
 *
 * @ORM\Table(name="dmli_save_mail", indexes={@ORM\Index(name="sav_dem_id_idx", columns={"dem_id"}), @ORM\Index(name="fk_dmli_save_mail_dmli_mail_mel1_idx", columns={"mel_id"})})
 * @ORM\Entity
 */
class DmliSaveMail
{
    /**
     * @var integer
     *
     * @ORM\Column(name="mel_id", type="integer", nullable=true)
     */
    private $melId;

    /**
     * @var string
     *
     * @ORM\Column(name="mel_libelle", type="string", length=70, nullable=true)
     */
    private $melLibelle;

    /**
     * @var integer
     *
     * @ORM\Column(name="sav_mel_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $savMelId;

    /**
     * @var \Formulaire\dmliBundle\Entity\DmliSaveDemandeDem
     *
     * @ORM\ManyToOne(targetEntity="Formulaire\dmliBundle\Entity\DmliSaveDemandeDem")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="dem_id", referencedColumnName="dem_id")
     * })
     */
    private $dem;


    /**
     * Set melId
     *
     * @param integer $melId
     *
     * @return DmliSaveMail
     */
    public function setMelId($melId)
    {
        $this->melId = $melId;
    
        return $this;
    }

    /**
     * Get melId
     *
     * @return integer
     */
    public function getMelId()
    {
        return $this->melId;
    }

    /**
     * Set melLibelle
     *
     * @param string $melLibelle
     *
     * @return DmliSaveMail
     */
    public function setMelLibelle($melLibelle)
    {
        $this->melLibelle = $melLibelle;
    
        return $this;
    }

    /**
     * Get melLibelle
     *
     * @return string
     */
    public function getMelLibelle()
    {
        return $this->melLibelle;
    }

    /**
     * Get savMelId
     *
     * @return integer
     */
    public function getSavMelId()
    {
        return $this->savMelId;
    }

    /**
     * Set dem
     *
     * @param \Formulaire\dmliBundle\Entity\DmliSaveDemandeDem $dem
     *
     * @return DmliSaveMail
     */
    public function setDem(\Formulaire\dmliBundle\Entity\DmliSaveDemandeDem $dem = null)
    {
        $this->dem = $dem;
    
        return $this;
    }

    /**
     * Get dem
     *
     * @return \Formulaire\dmliBundle\Entity\DmliSaveDemandeDem
     */
    public function getDem()
    {
        return $this->dem;
    }
}

