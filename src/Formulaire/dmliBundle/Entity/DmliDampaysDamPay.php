<?php

namespace Formulaire\dmliBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DmliDampaysDamPay
 *
 * @ORM\Table(name="dmli_dampays_dam_pay")
 * @ORM\Entity
 */
class DmliDampaysDamPay
{
    /**
     * @var string
     *
     * @ORM\Column(name="dam_pay_libelle", type="string", length=45, nullable=false)
     */
    private $damPayLibelle;

    /**
     * @var string
     *
     * @ORM\Column(name="dam_pay_cle", type="string", length=45, nullable=false)
     */
    private $damPayCle;

    /**
     * @var integer
     *
     * @ORM\Column(name="dam_pay_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $damPayId;


    /**
     * Set damPayLibelle
     *
     * @param string $damPayLibelle
     *
     * @return DmliDampaysDamPay
     */
    public function setDamPayLibelle($damPayLibelle)
    {
        $this->damPayLibelle = $damPayLibelle;
    
        return $this;
    }

    /**
     * Get damPayLibelle
     *
     * @return string
     */
    public function getDamPayLibelle()
    {
        return $this->damPayLibelle;
    }

    /**
     * Set damPayCle
     *
     * @param string $damPayCle
     *
     * @return DmliDampaysDamPay
     */
    public function setDamPayCle($damPayCle)
    {
        $this->damPayCle = $damPayCle;
    
        return $this;
    }

    /**
     * Get damPayCle
     *
     * @return string
     */
    public function getDamPayCle()
    {
        return $this->damPayCle;
    }

    /**
     * Get damPayId
     *
     * @return integer
     */
    public function getDamPayId()
    {
        return $this->damPayId;
    }
}

