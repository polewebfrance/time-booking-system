<?php

namespace Formulaire\dmliBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DmliSaveDemHasHar
 *
 * @ORM\Table(name="dmli_save_dem_has_har", indexes={@ORM\Index(name="fk_dmli_save_demande_dem_has_dmli_hardware_har_dmli_hardwar_idx", columns={"har_id"}), @ORM\Index(name="fk_dmli_save_demande_dem_has_dmli_hardware_har_dmli_save_de_idx", columns={"dem_id"})})
 * @ORM\Entity
 */
class DmliSaveDemHasHar
{
    /**
     * @var integer
     *
     * @ORM\Column(name="dem_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $demId;

    /**
     * @var \Formulaire\dmliBundle\Entity\DmliHardwareHar
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Formulaire\dmliBundle\Entity\DmliHardwareHar")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="har_id", referencedColumnName="har_id")
     * })
     */
    private $har;


    /**
     * Set demId
     *
     * @param integer $demId
     *
     * @return DmliSaveDemHasHar
     */
    public function setDemId($demId)
    {
        $this->demId = $demId;
    
        return $this;
    }

    /**
     * Get demId
     *
     * @return integer
     */
    public function getDemId()
    {
        return $this->demId;
    }

    /**
     * Set har
     *
     * @param \Formulaire\dmliBundle\Entity\DmliHardwareHar $har
     *
     * @return DmliSaveDemHasHar
     */
    public function setHar(\Formulaire\dmliBundle\Entity\DmliHardwareHar $har)
    {
        $this->har = $har;
    
        return $this;
    }

    /**
     * Get har
     *
     * @return \Formulaire\dmliBundle\Entity\DmliHardwareHar
     */
    public function getHar()
    {
        return $this->har;
    }
}

