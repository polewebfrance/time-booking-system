<?php

namespace Formulaire\dmliBundle\Controller;


use Formulaire\dmliBundle\Form\FormulaireType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;


class DefaultController extends Controller
{
    public function indexAction()
    {

        $form = $this->get('form.factory')->create(new FormulaireType());

        return $this->render('dmliBundle:Default:index.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function remplirSocieteAction()
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        if ($request->isXmlHttpRequest()) {
            $result = array();
            $sql = "SELECT DISTINCT id, libelle
				FROM adm_societe
				ORDER BY libelle;";
        $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
        $query->execute();
        $result = $query->fetchAll();
            if (count($result) > 1) {
                $output = '<option style="visibility: hidden;" value=""> --- liste --- </option>';
                foreach ($result as $row) {
                    $output .= '<option value="' . $row['id'] . '">' . $row['libelle'] . '</option>';
                }
                $output .= '<option value="-1"> -- AUTRE -- </option>';
                $return = $output;
                $response = new Response();
                $data = json_encode($return);
                $response->headers->set('Content-Type', 'application/json');
                $response->setContent($data);
                return $response;
            }
        }
        return new Response('Erreur');
    }

    public function crmCategoryAction()
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        if ($request->isXmlHttpRequest()) {
            $profil = $request->get('profil');
            $result = $em->createQuery('SELECT t.crmCatLibelle, t.crmCatId FROM dmliBundle:DmliCrmprofilCrmPro t1
              JOIN t1.crmCat t
              WHERE
                t1.crmProId =:id')
                ->setParameter('id', $profil)
                ->getResult();
            if (count($result) > 0) {

                foreach ($result as $row) {
                    $output = '<span id="spanCrmCategory"><b>' . $row['crmCatLibelle'] . '</b></span>';
                }
                $return = $output;
                $response = new Response();
                $data = json_encode($return);
                $response->headers->set('Content-Type', 'application/json');
                $response->setContent($data);
                return $response;
            }
        }
        return new Response('Erreur');

    }

    public function crmFunctionAction()
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        if ($request->isXmlHttpRequest()) {
            $profil = $request->get('profil');
            $result = $em->createQuery('SELECT t.crmFonLibelle, t.crmFonId FROM dmliBundle:DmliCrmprofilCrmPro t1
              JOIN t1.crmFon t
              WHERE
                t1.crmProId =:id')
                ->setParameter('id', $profil)
                ->getResult();
            if (count($result) > 0) {
                foreach ($result as $row) {
                    $output = '<span id="spanCrmFunction"><b>' . $row['crmFonLibelle'] . '</b></label>';
                }
                $return = $output;
                $response = new Response();
                $data = json_encode($return);
                $response->headers->set('Content-Type', 'application/json');
                $response->setContent($data);
                return $response;
            }
        }
        return new Response('Erreur');

    }

    public function crmProfilAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()) {
            $profil = $request->get('data');
            $result = array();
            $services = $em->getRepository('dmliBundle:DmliCrmprofilCrmPro')->findAll();
            $i = 0;
            foreach ($services as $service) {
                $result[$i]['id'] = $service->getCrmProId();
                $result[$i]['libelle'] = $service->getCrmProLibelle();
                $i++;
            }
            if (count($result) > 1) {
                $output = '<option style="visibility: hidden;" value=""> --- liste --- </option>';
                foreach ($result as $row) {
                    $output .= '<option value="' . $row['id'] . '">' . $row['libelle'] . '</option>';
                }
                $return = $output;
                $response = new Response();
                $data = json_encode($return);
                $response->headers->set('Content-Type', 'application/json');
                $response->setContent($data);
                return $response;
            }
        }
        return new Response('Erreur');
    }

    public function crmTeamAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();
        $return = array();
        if ($request->isXmlHttpRequest()) {
            $service = $request->get('service');
            if (($service == 1) or ($service == 2) or ($service == 4)) {
// 		var_dump("dans if");die();
                $sql = "SELECT DISTINCT crm_equ_id, crm_equ_libelle
				FROM dmli_crmequipe_crm_equ
				WHERE dmli_crmequipe_crm_equ.srv_id = " . $service . "
				OR dmli_crmequipe_crm_equ.srv_id = null
				ORDER BY crm_equ_libelle;";

                /* $em->createQuery('SELECT DISTINCT crmEquId, crmEquLibelle
                 FROM DmliCrmequipeCrmEqu t
                 WHERE t.srvId =: service
                 OR t.srvId = null
                 ORDER BY crmEquLibelle')
                     ->setParameter('service', $service)->getResult();*/

            } else {
// 		var_dump("dans else");die();
                $sql = "SELECT DISTINCT crm_equ_id, crm_equ_libelle
				FROM dmli_crmequipe_crm_equ
				ORDER BY crm_equ_libelle;";

                /*$em->createQuery('SELECT DISTINCT crmEquId, crmEquLibelle
				FROM DmliCrmequipeCrmEqu
				ORDER BY crmEquLibelle')
                    ->getResult();*/
            }


            $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll();

            $output = '<option style="visibility: hidden;" value=""> --- liste --- </option>';

            foreach ($result as $row) {
                $output .= '<option value="' . $row['crm_equ_id'] . '">' . $row['crm_equ_libelle'] . '</option>';
            }

            $return = $output;
            $response = new Response();
            $data = json_encode($return);
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent($data);
            return $response;
        }
        return new Response('Erreur');
    }

    public function dam_clePaysAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()) {
            $services = $em->getRepository('dmliBundle:DmliDampaysDamPay')->findAll();
            $i = 0;
            foreach ($services as $service) {
                $result[$i]['id'] = $service->getDamPayId();
                $result[$i]['libelle'] = $service->getDamPayLibelle();
                $result[$i]['dam_pay_cle'] = $service->getDamPayCle();
                $i++;
            }
            if (count($result) > 0) {
                $output = '<option style="visibility: hidden;" value=""> -- liste -- </option>';
                foreach ($result as $row) {
                    $output .= '<option value="' . $row['id'] . '">' . $row['libelle'] . ' (' . $row['dam_pay_cle'] . ')</option>';
                }
                $return = $output;
                $response = new Response();
                $data = json_encode($return);
                $response->headers->set('Content-Type', 'application/json');
                $response->setContent($data);
                return $response;
            }
        }
        return new Response('Erreur');
    }

    public function dam_clePersoAction()
    {
        $request = $this->getRequest();
        $return = array();
        if ($request->isXmlHttpRequest()) {
            $pstId = $request->get('pstId');

            $sql = "	SELECT dam_prs_libelle
			FROM dmli_dampersonnel_dam_prs
			WHERE dam_prs_id = (
			    SELECT dam_prs_id
			    FROM dmli_dam_prs_has_pst
			    WHERE pst_id = " . $pstId . "
			    );
			;";

            $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll();

            if (count($result) > 0) {

                $return = $result[0]['dam_prs_libelle'];
                $response = new Response();
                $data = json_encode($return);
                $response->headers->set('Content-Type', 'application/json');
                $response->setContent($data);
                return $response;
            }
        }
        return new Response('Erreur');
    }

    public function dam_commentaireAction()
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        if ($request->isXmlHttpRequest()) {
            $services = $em->getRepository('dmliBundle:DmliDamcommentaireDamCmt')->findAll();
            $i = 0;
            foreach ($services as $service) {
                $result[$i]['id'] = $service->getdamCmtId();
                $result[$i]['libelle'] = $service->getDamCmtLibelle();
                $i++;
            }
            if (count($result) > 0) {
                $output = '<option style="visibility: hidden;" value=""> -- liste -- </option>';

                foreach ($result as $row) {
                    $output .= '<option value="' . $row['id'] . '">' . $row['libelle'] . '</option>';
                }
                $return = $output;
                $response = new Response();
                $data = json_encode($return);
                $response->headers->set('Content-Type', 'application/json');
                $response->setContent($data);
                return $response;
            }
        }
        return new Response('Erreur');
    }

    public function dam_ssPoleAction()
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        if ($request->isXmlHttpRequest()) {
            $polId = $request->get('polId');


            $result = $em->createQuery('SELECT t1.damSspId, t1.damSspLibelle
                 FROM dmliBundle:DmliDamsspolDamSsp t1
                WHERE t1.pol =:id')
                ->setParameter('id', $polId)
                ->getResult();

            if (count($result) > 0) {
                $output = '<option style="visibility: hidden;" value=""> --- liste --- </option>';
                foreach ($result as $row) {
                    $output .= '<option value="' . $row['damSspId'] . '">' . $row['damSspLibelle'] . '</option>';
                }

                $return = $output;
                $response = new Response();
                $data = json_encode($return);
                $response->headers->set('Content-Type', 'application/json');
                $response->setContent($data);
                return $response;
            }
        }
        return new Response('Erreur');
    }

    //eléminer
   /* public function dam_typeAcheteurAction()
    {
        $request = $this->getRequest();
        $return = array();
        if ($request->isXmlHttpRequest()) {
            $sql = "	SELECT dam_ach_id, dam_ach_libelle
			FROM dmli_damacheteur_dam_ach
			ORDER BY dam_ach_libelle;";

            $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll();

            if (count($result) > 0) {

                $output = '<option style="visibility: hidden;" value=""> -- liste -- </option>';


                foreach ($result as $row) {
                    $output .= '<option value="' . $row['dam_ach_id'] . '">' . $row['dam_ach_libelle'] . '</option>';
                }

                $return = $output;
                $response = new Response();
                $data = json_encode($return);
                $response->headers->set('Content-Type', 'application/json');
                $response->setContent($data);
                return $response;
            }
        }
        return new Response('Erreur');
    }
       public function mailAction()
    {
        $request = $this->getRequest();
        $return = array();
        if ($request->isXmlHttpRequest()) {


//DAM
            //conexion du database
            $sql = "		SELECT mel_id, mel_libelle
					FROM dmli_mail_mel
					WHERE dmli_mail_mel.dir_id = 1
					ORDER BY mel_libelle;";

            //Envoi de la requete
            $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll();

            $output = '<option style="visibility: hidden;" value="0"> --- liste --- </option>';
            $output .= '<option value="-1"> -- AUTRE -- </option>';
            $output .= '<optgroup label="Direction Achats Marketing">';

            foreach ($result as $row) {
                $output .= '<option value="' . $row['mel_id'] . '">' . $row['mel_libelle'] . '</option>';
            }
            $output .= '</optgroup>';


            //Dir vente
            //conexion du database
            $sql = "		SELECT mel_id, mel_libelle
					FROM dmli_mail_mel
					WHERE dmli_mail_mel.dir_id = 2
					ORDER BY mel_libelle;";

            $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll();

            $output .= '<optgroup label="Direction des Ventes">';

            foreach ($result as $row) {
                $output .= '<option value="' . $row['mel_id'] . '">' . $row['mel_libelle'] . '</option>';
            }
            $output .= '</optgroup>';


            //Travaux
            //conexion du database
            $sql = "		SELECT mel_id, mel_libelle
					FROM dmli_mail_mel
					WHERE dmli_mail_mel.dir_id = 3
					ORDER BY mel_libelle;";

            $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll();

            $output .= '<optgroup label="Direction des Travaux">';

            foreach ($result as $row) {
                $output .= '<option value="' . $row['mel_id'] . '">' . $row['mel_libelle'] . '</option>';
            }
            $output .= '</optgroup>';


            //Expansion
            //conexion du database
            $sql = "		SELECT mel_id, mel_libelle
					FROM dmli_mail_mel
					WHERE dmli_mail_mel.dir_id = 4
					ORDER BY mel_libelle;";

            $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll();

            $output .= '<optgroup label="Expansion">';

            foreach ($result as $row) {
                $output .= '<option value="' . $row['mel_id'] . '">' . $row['mel_libelle'] . '</option>';
            }
            $output .= '</optgroup>';


            //Logistique
            //conexion du database
            $sql = "		SELECT mel_id, mel_libelle
					FROM dmli_mail_mel
					WHERE dmli_mail_mel.dir_id = 5
					ORDER BY mel_libelle;";

            $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll();

            $output .= '<optgroup label="Direction Logistique">';

            foreach ($result as $row) {
                $output .= '<option value="' . $row['mel_id'] . '">' . $row['mel_libelle'] . '</option>';
            }
            $output .= '</optgroup>';


            //Support
            //conexion du database
            $sql = "		SELECT mel_id, mel_libelle
					FROM dmli_mail_mel
					WHERE dmli_mail_mel.dir_id = 6
					ORDER BY mel_libelle;";

            $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll();

            $output .= '<optgroup label="Direction Support">';

            foreach ($result as $row) {
                $output .= '<option value="' . $row['mel_id'] . '">' . $row['mel_libelle'] . '</option>';
            }
            $output .= '</optgroup>';

            //Travaux
            //conexion du database
            $sql = "		SELECT mel_id, mel_libelle
					FROM dmli_mail_mel
					WHERE dmli_mail_mel.dir_id = 7
					ORDER BY mel_libelle;";

            $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll();

            $output .= '<optgroup label="Diversification">';

            foreach ($result as $row) {
                $output .= '<option value="' . $row['mel_id'] . '">' . $row['mel_libelle'] . '</option>';
            }
            $output .= '</optgroup>';


            //Travaux
            //conexion du database
            $sql = "		SELECT mel_id, mel_libelle
					FROM dmli_mail_mel
					WHERE dmli_mail_mel.dir_id = 0
					ORDER BY mel_libelle;";

            $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll();

            $output .= '<optgroup label="Autres listes">';

            foreach ($result as $row) {
                $output .= '<option value="' . $row['mel_id'] . '">' . $row['mel_libelle'] . '</option>';
            }
            $output .= '</optgroup>';


            $return = $output;
            $response = new Response();
            $data = json_encode($return);
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent($data);
            return $response;
        }
        return new Response('Erreur');
    }
   //eliminer
    public function listeMailDirectionAction()
    {
        $request = $this->getRequest();
        $return = array();
        if ($request->isXmlHttpRequest()) {
            $idDir = $request->get('direction');

            $sql = "SELECT mel_id, mel_libelle
				FROM dmli_mail_mel
				WHERE dmli_mail_mel.dir_id = " . $idDir . "
				ORDER BY mel_libelle;";


            $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll();

            $dir = "SELECT libelle
				FROM adm_direction
				WHERE adm_direction.id = " . $idDir . ";";

            $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($dir);
            $query->execute();
            $libDir = $query->fetchAll();


            $output = '<option style="visibility: hidden;" value="0"> --- liste --- </option>';
            $output .= '<option value="-1"> -- AUTRE -- </option>';
            $output .= '<optgroup label="' . $libDir[0]['libelle'] . '">';

            foreach ($result as $row) {
                $output .= '<option value="' . $row['mel_id'] . '">' . $row['mel_libelle'] . '</option>';
            }
            $output .= '</optgroup>';

            $sql = "SELECT mel_id, mel_libelle
				FROM dmli_mail_mel
				WHERE dmli_mail_mel.dir_id = 0
				ORDER BY mel_libelle;";

            $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll();

            $output .= '<optgroup label="Autres listes">';

            foreach ($result as $row) {
                $output .= '<option value="' . $row['mel_id'] . '">' . $row['mel_libelle'] . '</option>';
            }
            $output .= '</optgroup>';

            $return = $output;
            $response = new Response();
            $data = json_encode($return);
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent($data);
            return $response;
        }
        return new Response('Erreur');
    }
//eliminer
    public function listeMailWithPostAction()
    {
        $request = $this->getRequest();
        $return = array();
        if ($request->isXmlHttpRequest()) {
            $idBridge = $request->get('idBridge');

            $sql = "
				SELECT dmli_mail_mel.mel_id, dmli_mail_mel.mel_libelle
				FROM dmli_mail_mel
				WHERE mel_id IN (
					SELECT mel_id
					FROM dmli_bdg_has_mel
					WHERE bdg_id = " . $idBridge . ")";

            $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll();

            if (count($result) > 0) {
                $return = $result;
                $response = new Response();
                $data = json_encode($return);
                $response->headers->set('Content-Type', 'application/json');
                $response->setContent($data);
                return $response;
            }
        }
        return new Response('Erreur');
    }
*/
    public function departmentAction()
    {
        $request = $this->getRequest();

        $em = $this->getDoctrine()->getManager();
        if ($request->isXmlHttpRequest()) {
            $direction = $request->get('direction');
            $result = array();
            $services = $em->getRepository('AdministrationDirectionBundle:Service')->findBy(array('direction' => $direction));
            $i = 0;
            foreach ($services as $service) {
                $result[$i]['id'] = $service->getId();
                $result[$i]['libelle'] = $service->getLibelle();
                $i++;
            }
            if (count($result) >= 1) {
                $output = '<option style="visibility: hidden;" value=""> --- liste --- </option>';
                foreach ($result as $row) {
                    $output .= '<option value="' . $row['id'] . '">' . $row['libelle'] . '</option>';
                }

                $return = $output;
                $response = new Response();
                $data = json_encode($return);
                $response->headers->set('Content-Type', 'application/json');
                $response->setContent($data);
                return $response;
            }
        }
        return new Response('Erreur');
    }

    public function directionAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();
        $return = array();
        if ($request->isXmlHttpRequest()) {

            $result = array();
            $services = $em->getRepository('AdministrationDirectionBundle:Direction')->findAll();
            $i = 0;
            foreach ($services as $service) {
                $result[$i]['id'] = $service->getId();
                $result[$i]['libelle'] = $service->getLibelle();
                $i++;
            }

            $output = '<option style="visibility: hidden;" value=""> --- liste --- </option>';
            foreach ($result as $row) {
                $output .= '<option value="' . $row['id'] . '">' . $row['libelle'] . '</option>';
            }

            $return = $output;
            $response = new Response();
            $data = json_encode($return);
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent($data);
            return $response;
        }
        return new Response('Erreur');
    }

    public function divisionAction()
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        if ($request->isXmlHttpRequest()) {
            $serv = $request->get('service');
            $result = array();
            $services = $em->getRepository('dmliBundle:PolePol')->findBy(array('srv' => $serv));
            $i = 0;
            foreach ($services as $service) {
                $result[$i]['id'] = $service->getPolId();
                $result[$i]['libelle'] = $service->getPolLibelle();
                $i++;
            }
            if (count($result) > 0) {
                $output = '<option style="visibility: hidden;" value=""> --- liste --- </option>';
                foreach ($result as $row) {
                    $output .= '<option value="' . $row['id'] . '">' . $row['libelle'] . '</option>';
                }
                $output .= '<option value="-1">-- AUTRE -- </option>';
                $return = $output;
                $response = new Response();
                $data = json_encode($return);
                $response->headers->set('Content-Type', 'application/json');
                $response->setContent($data);
                return $response;
            }
        }
        return new Response('Erreur');
    }

    public function getBdgIdAction()
    {
        $request = $this->getRequest();
        $return = array();
        if ($request->isXmlHttpRequest()) {
            $idBridge = $request->get('idBridge');

            if ($idBridge[2] == 'NULL') {
                $lineSql = " AND pol_id IS NULL ";
            } else {
                $lineSql = " AND pol_id = " . $idBridge[2];
            }
if($idBridge[3] == "APPRENTI(E)" || $idBridge[3] == "STAGIAIRE"){
    return new Response('Erreur');
        }else{      $sql = "
				SELECT bdg_id
				FROM poste_bridge
				WHERE dir_id = " . $idBridge[0] . "
				AND srv_id = " . $idBridge[1] .
    $lineSql . "
				AND pst_id = " . $idBridge[3] .
    ";";
    $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
    $query->execute();
    $result = $query->fetchAll();

    if (count($result) > 0) {

        $return = $result;
        $response = new Response();
        $data = json_encode($return);
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent($data);
        return $response;
    }}
        }

    }

    public function hardwareAction()
    {
        $request = $this->getRequest();
        $return = array();
        if ($request->isXmlHttpRequest()) {

            $sql = "SELECT har_id, har_libelle, har_prix
				FROM dmli_hardware_har d inner join typemateriel t on d.typ_id=t.id where t.eta_id=1
			";
            $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll();

            $return = $result;
            $response = new Response();
            $data = json_encode($return);
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent($data);
            return $response;
        }
        return new Response('Erreur');
    }

    public function hardwareRenouvellementAction()
    {
        $request = $this->getRequest();
        $return = array();
        if ($request->isXmlHttpRequest()) {

            $sql = "SELECT har_id, har_libelle, har_prix
				FROM dmli_hardware_har d inner join dmli_save_dem_has_har t on d.har_id=t.har_id where t.eta_id=1
			";
            $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll();

            $return = $result;
            $response = new Response();
            $data = json_encode($return);
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent($data);
            return $response;
        }
        return new Response('Erreur');
    }

    public function listeHardwareAction()
    {
        $request = $this->getRequest();
        $return = array();
        if ($request->isXmlHttpRequest()) {
            $bdgId = $request->get('bdgId');

            $sql = "	SELECT dmli_hardware_har.har_id, dmli_hardware_har.har_libelle
				FROM dmli_hardware_har inner join typemateriel on dmli_hardware_har.typ_id=typemateriel.id where typemateriel.eta_id=1 and dmli_hardware_har.har_id IN (
					SELECT dmli_bdg_has_har.har_id
					FROM dmli_bdg_has_har
					WHERE dmli_bdg_has_har.bdg_id = " . $bdgId .
                "	);";

            $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll();

            if (count($result) > 1) {

                $return = $result;
                $response = new Response();
                $data = json_encode($return);
                $response->headers->set('Content-Type', 'application/json');
                $response->setContent($data);
                return $response;
            }
        }
        return new Response('Erreur');
    }

    public function listeMailAction()
    {
        $em = $this->getDoctrine()->getManager();

        $request = $this->getRequest();
        $return = array();
        if ($request->isXmlHttpRequest()) {
            $idListMail = $request->get('idListMail');

            $services = $em->getRepository('dmliBundle:DmliMailMel')->findBy(array('melId' => $idListMail));
            $i = 0;
            foreach ($services as $service) {
                $result[$i]['id'] = $service->getMelId();
                $result[$i]['libelle'] = $service->getMelLibelle();
                $i++;
            }

            if (count($result) > 0) {

                $return = $result;
                $response = new Response();
                $data = json_encode($return);
                $response->headers->set('Content-Type', 'application/json');
                $response->setContent($data);
                return $response;
            }
        }
        return new Response('Erreur');
    }


    public function listeSoftwareAction()
    {
        $request = $this->getRequest();
        $return = array();
        if ($request->isXmlHttpRequest()) {
            $bdgId = $request->get('bdgId');

            $sql = "	SELECT dmli_software_sft.sft_id, dmli_software_sft.sft_libelle
				FROM dmli_software_sft
				WHERE dmli_software_sft.sft_id IN (
					SELECT dmli_bdg_has_sft.sft_id
					FROM dmli_bdg_has_sft
					WHERE dmli_bdg_has_sft.bdg_id = " . $bdgId .
                "	);";

            $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll();

            if (count($result) > 1) {

                $return = $result;
                $response = new Response();
                $data = json_encode($return);
                $response->headers->set('Content-Type', 'application/json');
                $response->setContent($data);
                return $response;
            }
        }
        return new Response('Erreur');
    }

    public function listHardwareInPackAction()
    {
        $request = $this->getRequest();
        $return = array();
        if ($request->isXmlHttpRequest()) {
            $idPack = $request->get('pack');

            $sql = "	SELECT har_id, har_libelle, har_prix
			FROM dmli_hardware_har
			WHERE har_id IN (
				SELECT har_id
				FROM dmli_pkh_has_har
				WHERE pkh_id = " . $idPack . "
			)
		";

            $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll();

            $return = $result;
            $response = new Response();
            $data = json_encode($return);
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent($data);
            return $response;
        }
        return new Response('Erreur');
    }



    public function packHardwareAction()
    {
        $request = $this->getRequest();
        $return = array();
        if ($request->isXmlHttpRequest()) {


            $sql = "	SELECT pkh_id, pkh_libelle
			FROM dmli_packhardware_pkh
			ORDER BY pkh_libelle;
		";

            $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll();

            if (count($result) > 1) {

                $output = '<option style="visibility: hidden;" value=""> --- liste --- </option>';


                foreach ($result as $row) {
                    $output .= '<option value="' . $row['pkh_id'] . '">' . $row['pkh_libelle'] . '</option>';
                }

                $return = $output;
                $response = new Response();
                $data = json_encode($return);
                $response->headers->set('Content-Type', 'application/json');
                $response->setContent($data);
                return $response;
            }
        }
        return new Response('Erreur');
    }

    public function postAction()
    {
        $request = $this->getRequest();
        $return = array();
        if ($request->isXmlHttpRequest()) {

            $idBridge = $request->get('idBridge');
            if ($idBridge[1] != '') {
                if ($idBridge[2] == 'NULL') {
                    $lineSql = " AND pol_id IS NULL ";
                } else {
                    $lineSql = " AND pol_id = " . $idBridge[2];
                }


                $sql = "	SELECT pst_id, pst_libelle
				FROM poste_pst
				WHERE pst_id IN (
					SELECT pst_id
					FROM poste_bridge
					WHERE dir_id =" . $idBridge[0] . "
					AND srv_id =" . $idBridge[1] .
                    $lineSql . "
				)
				ORDER BY pst_libelle;
			";

                $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
                $query->execute();
                $result = $query->fetchAll();

                if (count($result) > 0) {

                    $output = '<option style="visibility: hidden;" value=""> --- liste --- </option>';

                    $output .= '<option value="STAGIAIRE">STAGIAIRE</option>';
                    $output .= '<option value="APPRENTI(E)">APPRENTI(E)</option>';

                    foreach ($result as $row) {
                        $output .= '<option value="' . $row['pst_id'] . '">' . $row['pst_libelle'] . '</option>';
                    }

                    $output .= '<option value="-1"> -- AUTRE -- </option>';


                    $return = $output;
                    $response = new Response();
                    $data = json_encode($return);
                    $response->headers->set('Content-Type', 'application/json');
                    $response->setContent($data);
                    return $response;
                }
            } else {
                return new Response('Erreur');
            }
        }
    }

    public function softwareAction()
    {
        $request = $this->getRequest();

        if ($request->isXmlHttpRequest()) {

            $sql = "	SELECT sft_id, sft_libelle, sft_prix
				FROM dmli_software_sft
				ORDER BY sft_libelle;";

            $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll();


            $response = new Response();
            $data = json_encode($result);
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent($data);
            return $response;
        }
        return new Response('Erreur');
    }

    public function upMailAction()
    {
        //$request = $this->getRequest();
        $return = array();
        //if ($request->isXmlHttpRequest()) {
//DAM
        //conexion du database
        $sql = "		SELECT mel_id, mel_libelle
					FROM dmli_mail_mel
					WHERE dmli_mail_mel.dir_id = 1
					ORDER BY mel_libelle;";
        //Envoi de la requete
        $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
        $query->execute();
        $result = $query->fetchAll();
        //Direction Achats Marketing
        $i = 0;
        foreach ($result as $row) {
            $output[0][$i]['id'] = $row['mel_id'];
            $output[0][$i]['libelle'] = $row['mel_libelle'];
            $i++;
        }
        //Dir vente
        //conexion du database
        $sql = "		SELECT mel_id, mel_libelle
					FROM dmli_mail_mel
					WHERE dmli_mail_mel.dir_id = 2
					ORDER BY mel_libelle;";
        $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
        $query->execute();
        $result = $query->fetchAll();
        //$output .= '<optgroup label="Direction des Ventes">';
        $i = 0;
        foreach ($result as $row) {
            $output[1][$i]['id'] = $row['mel_id'];
            $output[1][$i]['libelle'] = $row['mel_libelle'];
            $i++;
        }
        //Travaux
        //conexion du database
        $sql = "		SELECT mel_id, mel_libelle
					FROM dmli_mail_mel
					WHERE dmli_mail_mel.dir_id = 3
					ORDER BY mel_libelle;";

        $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
        $query->execute();
        $result = $query->fetchAll();
        //$output .= '<optgroup label="Direction des Travaux">';
        $i = 0;
        foreach ($result as $row) {
            $output[2][$i]['id'] = $row['mel_id'];
            $output[2][$i]['libelle'] = $row['mel_libelle'];
            $i++;
        }
        //Expansion
        //conexion du database
        $sql = "		SELECT mel_id, mel_libelle
					FROM dmli_mail_mel
					WHERE dmli_mail_mel.dir_id = 4
					ORDER BY mel_libelle;";

        $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
        $query->execute();
        $result = $query->fetchAll();
        //$output .= '<optgroup label="Expansion">';
        $i = 0;
        foreach ($result as $row) {
            $output[3][$i]['id'] = $row['mel_id'];
            $output[3][$i]['libelle'] = $row['mel_libelle'];
            $i++;
        }
        // var_dump($output);die;
        //Logistique
        //conexion du database
        $sql = "		SELECT mel_id, mel_libelle
					FROM dmli_mail_mel
					WHERE dmli_mail_mel.dir_id = 5
					ORDER BY mel_libelle;";

        $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
        $query->execute();
        $result = $query->fetchAll();

        //$output .= '<optgroup label="Direction Logistique">';

        $i = 0;
        foreach ($result as $row) {
            $output[4][$i]['id'] = $row['mel_id'];
            $output[4][$i]['libelle'] = $row['mel_libelle'];
            $i++;
        }

        //var_dump($output);die;


        //Support
        //conexion du database
        $sql = "		SELECT mel_id, mel_libelle
					FROM dmli_mail_mel
					WHERE dmli_mail_mel.dir_id = 6
					ORDER BY mel_libelle;";

        $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
        $query->execute();
        $result = $query->fetchAll();

        //$output .= '<optgroup label="Direction Support">';

        $i = 0;
        foreach ($result as $row) {
            $output[5][$i]['id'] = $row['mel_id'];
            $output[5][$i]['libelle'] = $row['mel_libelle'];
            $i++;
        }

        //var_dump($output);die;

        //Travaux
        //conexion du database
        $sql = "		SELECT mel_id, mel_libelle
					FROM dmli_mail_mel
					WHERE dmli_mail_mel.dir_id = 7
					ORDER BY mel_libelle;";

        $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
        $query->execute();
        $result = $query->fetchAll();

        // $output .= '<optgroup label="Diversification">';

        $i = 0;
        foreach ($result as $row) {
            $output[6][$i]['id'] = $row['mel_id'];
            $output[6][$i]['libelle'] = $row['mel_libelle'];
            $i++;
        }

        //var_dump($output);die;


        //Travaux
        //conexion du database
        $sql = "		SELECT mel_id, mel_libelle
					FROM dmli_mail_mel
					WHERE dmli_mail_mel.dir_id = 0
					ORDER BY mel_libelle;";

        $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
        $query->execute();
        $result = $query->fetchAll();

        // $output .= '<optgroup label="Autres listes">';

        $i = 0;
        foreach ($result as $row) {
            $output[7][$i]['id'] = $row['mel_id'];
            $output[7][$i]['libelle'] = $row['mel_libelle'];
            $i++;
        }

        // var_dump($output);die;


        $return = $output;
        $response = new Response();
        $data = json_encode($return);
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent($data);
        return $response;
        // }
        //return new Response('Erreur');
    }

    public function listeMailingDirectionAction()
    {
        $request = $this->getRequest();
        $return = array();
        if ($request->isXmlHttpRequest()) {
            $idDir = $request->get('direction');

            $sql = "SELECT mel_id, mel_libelle
				FROM dmli_mail_mel
				WHERE dmli_mail_mel.dir_id = " . $idDir . "
				ORDER BY mel_libelle;";


            $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll();

            $dir = "SELECT  libelle
				FROM adm_direction
				WHERE adm_direction.id = " . $idDir . ";";

            $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($dir);
            $query->execute();
            $libDir = $query->fetchAll();


            $i = 0;
            $output[0][$i]['direction'] = $libDir[0]['libelle'];
            $i++;
            foreach ($result as $row) {
                $output[0][$i]['id'] = $row['mel_id'];
                $output[0][$i]['libelle'] = $row['mel_libelle'];
                $i++;
            }

            $sql = "SELECT mel_id, mel_libelle
				FROM dmli_mail_mel
				WHERE dmli_mail_mel.dir_id = 0
				ORDER BY mel_libelle;";

            $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll();
            $i = 0;
            $output[1][$i]['Autres'] = "Autres listes";

            $i++;
            foreach ($result as $row) {
                $output[1][$i]['id'] = $row['mel_id'];
                $output[1][$i]['libelle'] = $row['mel_libelle'];
                $i++;
            }

            $return = $output;
            $response = new Response();
            $data = json_encode($return);
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent($data);
            return $response;
        }
        return new Response('Erreur');
    }

    public function listeMailingWithPostAction()
    {
        $request = $this->getRequest();
        $return = array();
        if ($request->isXmlHttpRequest()) {
            $idBridge = $request->get('idBridge');

            $sql = "
				SELECT dmli_mail_mel.mel_id, dmli_mail_mel.mel_libelle
				FROM dmli_mail_mel
				WHERE mel_id IN (
					SELECT mel_id
					FROM dmli_bdg_has_mel
					WHERE bdg_id = " . $idBridge . ")";

            $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll();
            // var_dump($result);die;
            if (count($result) > 0) {
                $return = $result;
                $response = new Response();
                $data = json_encode($return);
                $response->headers->set('Content-Type', 'application/json');
                $response->setContent($data);
                return $response;
            }
        }
        return new Response('Erreur');
    }

    public function codeUserAction(){

        $request = $this->getRequest();
        $return = array();

            $pstId = $request->get('pstId');

            $sql = "
				SELECT  dmli_dam_prs_has_pst.pst_id, dmli_dam_prs_has_pst.dam_prs_id FROM dmli_dam_prs_has_pst
					WHERE pst_id = " . $pstId ."";

            $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll();

            if (count($result) > 0) {
                $return = $result[0];
            }else{
                $return['pst_id']='';
                $return['dam_prs_id']='';
            }
               // var_dump($return);die;
                $response = new Response();
                $data = json_encode($return);
                $response->headers->set('Content-Type', 'application/json');
                $response->setContent($data);
                return $response;


    }
}

