<?php

namespace Formulaire\DDRCBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Formulaire\DDRCBundle\Validator\FilesTotalSize;

/**
 * @ORM\Entity
 * @FilesTotalSize
 * CBOver80
 *
 * This class extends DdrcRefunds and represents a Payback by CB with value over 80 euros
 */

class CBOver80 extends DdrcRefunds
{
    /**
     * @var float
     *
     * @ORM\Column(name="payback_amount", type="float", precision=10, scale=0, nullable=false)
     * @Assert\NotNull(message="Vous devez spécifier le montant du remboursement!" )
     * @Assert\GreaterThan(
     *     value = 79,
     *	   message = "La valeur doit être égale ou supérieure à 80"
     * )
     */
    protected $paybackAmount;

    /**
     * @var string
     *
     * @ORM\Column(name="attachement_d_1", type="text", length=65535, nullable=false)
     * @Assert\NotNull( message="S'il vous plaît sélectionner un fichier!" )
     * @Assert\File(
     *     maxSize = "3000k",
     *     mimeTypes = {"application/pdf", "application/x-pdf", "image/jpeg", "image/pjpeg", "image/png", "application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document" },
     *     mimeTypesMessage = "Fichier invalide, veuillez recommencer."
     * )
     */
    protected $attachement1;

    /**
     * @var string
     *
     * @ORM\Column(name="attachement_d_2", type="text", length=65535, nullable=true)
     * @Assert\NotNull(message="S'il vous plaît sélectionner un fichier!" )
     * @Assert\File(
     *     maxSize = "3000k",
     *     mimeTypes = {"application/pdf", "application/x-pdf", "image/jpeg", "image/pjpeg", "image/png", "application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document" },
     *     mimeTypesMessage = "Fichier invalide, veuillez recommencer."
     * )
     */
    protected $attachement2;

    /**
     * @var string
     *
     * @ORM\Column(name="attachement_d_3", type="text", length=65535, nullable=true)
     * @Assert\NotNull( message="S'il vous plaît sélectionner un fichier!" )
     * @Assert\File(
     *     maxSize = "3000k",
     *     mimeTypes = {"application/pdf", "application/x-pdf", "image/jpeg", "image/pjpeg", "image/png", "application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document" },
     *     mimeTypesMessage = "Fichier invalide, veuillez recommencer."
     * )
     */
    protected $attachement3;

    /**
     * @return float
     */
    public function getPaybackAmount()
    {
        return $this->paybackAmount;
    }

    /**
     * @param float $paybackAmount
     */
    public function setPaybackAmount($paybackAmount)
    {
        $this->paybackAmount = $paybackAmount;
    }

    /**
     * @return string
     */
    public function getAttachement1()
    {
        return $this->attachement1;
    }

    /**
     * @param string $attachement1
     */
    public function setAttachement1($attachement1)
    {
        $this->attachement1 = $attachement1;
    }

    /**
     * @return string
     */
    public function getAttachement2()
    {
        return $this->attachement2;
    }

    /**
     * @param string $attachement2
     */
    public function setAttachement2($attachement2)
    {
        $this->attachement2 = $attachement2;
    }

    /**
     * @return string
     */
    public function getAttachement3()
    {
        return $this->attachement3;
    }

    /**
     * @param string $attachement3
     */
    public function setAttachement3($attachement3)
    {
        $this->attachement3 = $attachement3;
    }


}

