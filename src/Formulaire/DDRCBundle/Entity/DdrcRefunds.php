<?php

namespace Formulaire\DDRCBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Formulaire\DDRCBundle\Validator\CodePostal;

/**
 * DdrcRefunds
 *
 * @ORM\Table(name="ddrc_refunds", indexes={ @ORM\Index(name="fk_id_magasin_refund", columns={"id_magasin"})})
 * @ORM\Entity
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="refund_type", type="string")
 * @ORM\DiscriminatorMap( {"over80" = "Over80", "less80" = "Less80", "reparation" = "Reparation", "cbover80" = "CBOver80" } )
 */
abstract class DdrcRefunds
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;
    
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=false)
     * @Assert\Date()
     */
    protected $date;
    
    /**
     * @var string
     *
     * @ORM\Column(name="civilite", type="string", length=20, nullable=false)
     * @Assert\NotNull(message="S'il vous plaît selectionner civilite" )
     */
    protected $civilite;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=255, nullable=false)
     * @Assert\NotNull(message="S'il vous plaît remplir prénom!" )
     */
    protected $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255, nullable=false)
     * @Assert\NotNull(message="S'il vous plaît remplir nom!" )
     */
    protected $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="text", length=65535, nullable=false)
     * @Assert\NotNull(message="S'il vous plaît remplir adresse!" )
     */
    protected $address;
    
     /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=false)
     * @Assert\NotNull(message="S'il vous plaît remplir ville!" )
     */
    protected $city;
    
     /**
     * @var string
     *
     * @ORM\Column(name="zipcode", type="string", length=100, nullable=false)
     * @Assert\NotNull(message="S'il vous plaît remplir code postal!" )
     */
    protected $zipcode;

    
    /**
     * @var \ChkMagasins
     *
     * @ORM\ManyToOne(targetEntity="Administration\SocieteBundle\Entity\AdmMagasin")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_magasin", referencedColumnName="mag_id")
     * })
     * @Assert\NotNull(message="S'il vous plaît sélectionnez un magasin!" )
     */
    protected $idMagasin;

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return DdrcRefunds
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set civilite
     *
     * @param string $civilite
     *
     * @return DdrcRefunds
     */
    public function setCivilite($civilite)
    {
        $this->civilite = $civilite;

        return $this;
    }

    /**
     * Get civilite
     *
     * @return string
     */
    public function getCivilite()
    {
        return $this->civilite;
    }
    
    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return DdrcRefunds
     */
    public function setFirstName($firstName)
    {
        $this->firstName = ucfirst ($firstName);

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return DdrcRefunds
     */
    public function setLastName($lastName)
    {
        $this->lastName = ucfirst ($lastName);

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return DdrcRefunds
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }
    
    
    /**
     * Set city
     *
     * @param string $city
     *
     * @return DdrcRefunds
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    
    /**
     * Set zipcode
     *
     * @param string $zipcode
     *
     * @return DdrcRefunds
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    /**
     * Get zipcode
     *
     * @return string
     * @CodePostal
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idMagasin
     *
     * @param \Administration\SocieteBundle\Entity\AdmMagasin $idMagasin
     *
     * @return DdrcRefunds
     */
    public function setIdMagasin(\Administration\SocieteBundle\Entity\AdmMagasin $idMagasin = null)
    {
        $this->idMagasin = $idMagasin;

        return $this;
    }

    /**
     * Get idMagasin
     *
     * @return \Administration\SocieteBundle\Entity\AdmMagasin
     */
    public function getIdMagasin()
    {
        return $this->idMagasin;
    }

    
}

