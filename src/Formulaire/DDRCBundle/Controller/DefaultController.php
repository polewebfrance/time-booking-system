<?php

namespace Formulaire\DDRCBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;




class DefaultController extends Controller
{
    /**
     * @Route("/", name="ddrc_home")
     * @Template()
     * 
     */
    public function indexAction(Request $request)
    {
      
        $data = array();
        $form = $this->createFormBuilder($data)
            ->add('category', 'choice',
                array('choices' => array(
                    'a' => "Remboursement par CB d'un ticket > à 80€",
                    'b'   => 'Remboursement du produit > a 80€',
                    'c' => 'Remboursement du devis + remboursement du produit < à 80€',
                    'd'   => 'Remboursement de la facture de reparation et/ou du devis payant'),
                    'multiple' => false,
                    'expanded'=> true,
                 ))
        ->getForm();
        
        return array('form' => $form->createView());
         
    }
    
    
    
}
