<?php

namespace Formulaire\DDRCBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Formulaire\DDRCBundle\Entity\Over80;
use Formulaire\DDRCBundle\Form\Over80Type;
use Formulaire\DDRCBundle\Email\Email;
use Symfony\Component\HttpFoundation\Response;
use Formulaire\DDRCBundle\Word\WordGenerator;

/**
 * Over80 controller.
 *
 * @Route("/over80")
 */
class Over80Controller extends Controller
{

    /**
     * Creates a new Over80 entity.
     *
     * @Route("/", name="over80_create")
     * @Method("POST")
     * @Template("DDRCBundle:Over80:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Over80();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        
        if ($form->isValid()) {
            
            //Retrieve the service (object) that handles moving files to uploadDir and returns the file name to be saved in Database
            $attachement_service = $this->get('attachment_handling_service');           
            
            $fileName = $attachement_service->addAttachments($entity->getAttachement1(),$entity, 'Fiche de remboursement');
            $entity->setAttachement1($fileName);
            
            $fileName = $attachement_service->addAttachments($entity->getAttachement2(),$entity, 'Ticket achat');
            $entity->setAttachement2($fileName);
            
            $fileName = $attachement_service->addAttachments($entity->getAttachement3(),$entity, 'Ticket retour');
            $entity->setAttachement3($fileName);

            // ... persist the $entity variable or any other work
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            //------- Generate word --------------
            $societe= $em->getRepository('Administration\SocieteBundle\Entity\PandoreSociete')->find($entity->getIdMagasin()->getSociete());
            $wordGenerator = $this->get('word_gen');
            $pathWordFile = $wordGenerator->generate($entity, $societe);
            
            //------- Send email------------------
            $files=array('la fiche de remboursement','le ticket achat','le ticket retour');
            $Email = $this->get('email_service');
            $Email->send($entity, $files, $pathWordFile);
            
            return $this->render('DDRCBundle:Default:success.html.twig');
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Over80 entity.
     *
     * @param Over80 $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Over80 $entity)
    {
        $form = $this->createForm(new Over80Type(), $entity, array(
            'action' => $this->generateUrl('over80_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Envoyer la demande'));

        return $form;
    }

    /**
     * Displays a form to create a new Over80 entity.
     *
     * @Route("/new", name="over80_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Over80();
        $form   = $this->createCreateForm($entity);
        
        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

}
