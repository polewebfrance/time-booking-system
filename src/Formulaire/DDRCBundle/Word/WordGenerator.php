<?php

namespace Formulaire\DDRCBundle\Word;

use DocxTemplate\TemplateFactory;
use Formulaire\DDRCBundle\Entity\DdrcSociete;
use Formulaire\DDRCBundle\Utils\NameGenerator;

/**
 * Contains a  method used for generating the .docx document
 *
 * @author pana_cr
 */
class WordGenerator {
 
    private $generatedPath;
    private $templatePath;
    
    public function __construct($generatedPath,$templatePath)
    {
        $this->generatedPath = $generatedPath;
        $this->templatePath = $templatePath;
    }
    public  function generate($entity, $societe)
    {
       setlocale(LC_TIME, "");
       $date = utf8_encode(strftime("%d %B %Y"));
       $capitalRaw = (string)number_format((float)$societe->getCapital(),2,",",".");
       $capitalDisplay = str_replace('.', ' ', $capitalRaw);
       
       $template = TemplateFactory::load($this->templatePath . 'template.docx');
       $template->assign([
            'civilite'    => $entity->getCivilite(),
            'firstName'   => $entity->getFirstName(),
            'lastName'    => $entity->getLastName(),
            'adresse'     => $entity->getAddress(),
            'zipcode'     => $entity->getZipcode(),
            'city'        => $entity->getCity(),
            'payback_amount'=>(string)number_format((float)$entity->getPaybackAmount(),2,",","."),
            'date'        => $date,
            'formjur'     => $societe->getFormjur(),
            'raison'      => $societe->getRaison(),
            'numvoie'     => $societe->getNumvoie(),
            'adresse1'    => $this->formatText($societe->getAdresse1()),
            'codepostal'  => $societe->getCodepostal(),
            'ville'       => $societe->getVille(),
            'siren'       => $this->sirenFormatText($societe->getSiren()),
            'capital'     => $capitalDisplay
           
        ]);
        
       $pathWordFile = $this->generatedPath . NameGenerator::createNameForGeneratedWord($entity);
       
       $template->save($pathWordFile);
       
       return $pathWordFile;
    }
    //function used for formatting the address text
    public  function formatText($inputText)
    {
        $inputTextLower=  strtolower($inputText);
        $exploded= explode(' ', $inputTextLower);
        foreach ($exploded as &$value) {
            if (($value!='de') || ($value!='du') ||($value != 'le')||($value !='la')||($value!='des')){
                if (($value=='za')||($value=='zac')||($value=='zi')||($value=='zat')){
                    $value=  strtoupper($value);
                }else{
                    $value = ucfirst($value);
                }
            }
        }
        $imploded = implode(' ', $exploded);
        
        return $imploded;
            
    }
    
    //format how SIREN is displayed. because in the database table SIREN is varchar(45) make sure not containing blank spaces
    public function sirenFormatText($siren)
    {
        $siren = str_replace(' ', '', trim($siren)); //first eliminate whitespaces
        $siren = substr_replace($siren, ' ', 3, 0);
        $formatted_siren = substr_replace($siren, ' ', 7, 0);
        
        return $formatted_siren;
    }
    
}

