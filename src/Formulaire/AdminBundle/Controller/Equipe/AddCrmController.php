<?php

namespace Formulaire\AdminBundle\Controller\Equipe;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Formulaire\AdminBundle\Form\AdminType;
use Symfony\Component\HttpFoundation\Response;

class AddCrmController extends Controller
{
    /**
     * @Route("/crm/add", name="add_crm")
     * @Template()
     */
    public function addAction()
    {
        $form = $this->get('form.factory')->create(new AdminType());

        return $this->render('AdminBundle:Equipe:AjouterCrm.html.twig', array(
            'form' => $form->createView()
        ));

    }

    /**
     * @Route("/crm/addservice")
     * @Template()
     */
    public function serviceAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();
        $return = array();
        if ($request->isXmlHttpRequest()) {

            $result = array();
            $services = $em->getRepository('dmliBundle:AdmService')->findAll();
            $i = 0;
            foreach ($services as $service) {
                $result[$i]['id'] = $service->getId();
                $result[$i]['libelle'] = $service->getLibelle();
                $i++;
            }
            $output = '<option style="visibility: hidden;" value=""> --- liste --- </option>';
            foreach ($result as $row) {
                $output .= '<option value="' . $row['id'] . '">' . $row['libelle'] . '</option>';
            }
            $return = $output;
            $response = new Response();
            $data = json_encode($return);
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent($data);
            return $response;
        }
        return new Response('Erreur');
    }
    /**
     * @Route("/crm/addequipe")
     * @Template()
     */
    public function equipeAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();
        $return = array();
        if ($request->isXmlHttpRequest()) {

            $result = array();
            $services = $em->getRepository('dmliBundle:DmliCrmequipeCrmEqu')->findAll();
            $i = 0;
            foreach ($services as $service) {
                $result[$i]['id'] = $service->getCrmEquId();
                $result[$i]['libelle'] = $service->getCrmEquLibelle();
                $i++;
            }
            $output = '<option style="visibility: hidden;" value=""> --- liste --- </option>';
            foreach ($result as $row) {
                $output .= '<option value="' . $row['id'] . '">' . $row['libelle'] . '</option>';
            }
            $output .= '<option value="-1">Autres</option>';
            $return = $output;
            $response = new Response();
            $data = json_encode($return);
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent($data);
            return $response;
        }
        return new Response('Erreur');
    }
}