<?php

namespace Formulaire\AdminBundle\Controller\Equipe;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Formulaire\AdminBundle\Form\AdminType;
use Symfony\Component\HttpFoundation\Response;

class CrmController extends Controller
{
    /**
     * @Route("/crm", name="index_crm")
     * @Template()
     */
    public function indexAction()
    {

        $em = $this->getDoctrine()->getManager();
        $services = $em->getRepository('dmliBundle:DmliCrmequipeCrmEqu')->findAll();
//var_dump($services);die;
        $i = 0;
        foreach ($services as $service) {
            $result[$i]['id']=$service->getCrmEquId();
            $result[$i]['libelle'] = $service->getCrmEquLibelle();
            if($service->getSrvId() != ''){
                $direction=$em->getRepository('dmliBundle:AdmService')->findBy(
                    array('id'=> $service->getSrvId()));
                //var_dump($direction[0]->getLibelle());die;
                $result[$i]['service']= $direction[0]->getLibelle();
            }else{
                $result[$i]['service']= 'Autres';
            }

            $i++;
        }
        return $this->render('AdminBundle:Equipe:ListeCrm.html.twig', array('result'=> $result));

    }
    /**
     * @Route("/crm/remove/{id}", name="remove_crm")
     * @Template()
     */
    public function removeAction($id)
    {  $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository('dmliBundle:DmliCrmequipeCrmEqu')->findBy(array('crmEquId'=>$id));

        $em->remove($product[0]);
        $em->flush();
        $request->getSession()->getFlashBag()->add(
            'success',
            'Equipe CRM supprimée!'
        );
        return $this->redirect($this->generateUrl('index_crm'));
    }
}
