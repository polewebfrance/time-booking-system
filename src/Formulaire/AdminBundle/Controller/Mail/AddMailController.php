<?php

namespace Formulaire\AdminBundle\Controller\Mail;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Formulaire\AdminBundle\Form\AdminType;
use Symfony\Component\HttpFoundation\Response;

class AddMailController extends Controller
{
    /**
     * @Route("/mail/add", name="add_mail")
     * @Template()
     */
    public function addAction()
    {
        $form = $this->get('form.factory')->create(new AdminType());

        return $this->render('AdminBundle:Mail:AjouterMail.html.twig', array(
            'form' => $form->createView()
        ));

    }

    /**
     * @Route("/mail/adddirection")
     * @Template()
     */
    public function directionAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();
        $return = array();
        if ($request->isXmlHttpRequest()) {

            $result = array();
            $services = $em->getRepository('dmliBundle:AdmDirection')->findAll();
            $i = 0;
            foreach ($services as $service) {
                $result[$i]['id'] = $service->getId();
                $result[$i]['libelle'] = $service->getLibelle();
                $i++;
            }

            $output = '<option style="visibility: hidden;" value=""> --- liste --- </option>';
            foreach ($result as $row) {
                $output .= '<option value="' . $row['id'] . '">' . $row['libelle'] . '</option>';
            }
            $output .= '<option value="0">Autres</option>';
            $return = $output;
            $response = new Response();
            $data = json_encode($return);
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent($data);
            return $response;
        }
        return new Response('Erreur');
    }
}