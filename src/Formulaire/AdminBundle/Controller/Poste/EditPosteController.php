<?php

namespace Formulaire\AdminBundle\Controller\Poste;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Formulaire\AdminBundle\Form\AdminType;
use Symfony\Component\HttpFoundation\Response;

class EditPosteController extends Controller
{
    /**
     * @Route("/poste/edit/{id}", name="edit_poste")
     * @Template()
     */
    public function editAction($id)
    {$em = $this->getDoctrine()->getManager();
        $services = $em->getRepository('dmliBundle:PostePst')->findBy(
            array('pstId'=>$id));
//var_dump($services);die;

        $result['id']=$services[0]->getPstId();
        $result['libelle'] = $services[0]->getPstLibelle();

        $form = $this->get('form.factory')->create(new AdminType());

        return $this->render('AdminBundle:Poste:ModifierPoste.html.twig', array('result'=>$result,
            'form' => $form->createView()
        ));

    }
}