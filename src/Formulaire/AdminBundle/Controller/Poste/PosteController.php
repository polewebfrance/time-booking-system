<?php

namespace Formulaire\AdminBundle\Controller\Poste;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Formulaire\AdminBundle\Form\AdminType;
use Symfony\Component\HttpFoundation\Response;

class PosteController extends Controller
{
    /**
     * @Route("/poste", name="index_poste")
     * @Template()
     */
    public function indexAction()
    {

        $em = $this->getDoctrine()->getManager();
        $services = $em->getRepository('dmliBundle:PostePst')->findAll();
//var_dump($services);die;
        $i = 0;
        foreach ($services as $service) {
            $result[$i]['id']=$service->getPstId();
            $result[$i]['libelle'] = $service->getPstLibelle();
            $i++;
        }
        return $this->render('AdminBundle:Poste:ListePoste.html.twig', array('result'=> $result));

    }
    /**
     * @Route("/poste/remove/{id}", name="remove_poste")
     * @Template()
     */
    public function removeAction($id)
    {  $request = $this->getRequest();

        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository('dmliBundle:PostePst')->findBy(array('pstId'=>$id));

        $product1 = $em->getRepository('dmliBundle:PosteBridge')->findBy(array('pst'=>$product[0]));

        if(count($product1) == 0 ){
            $em->remove($product[0]);
            $em->flush();
            $request->getSession()->getFlashBag()->add(
                'success',
                'poste supprimé!'
            );
        }else{
            $request->getSession()->getFlashBag()->add(
                'myError',
                'Vous ne pouvez pas supprimer ce poste!'
            );
        }



        return $this->redirect($this->generateUrl('index_poste'));
    }
}
