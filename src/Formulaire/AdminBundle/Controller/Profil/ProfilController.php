<?php

namespace Formulaire\AdminBundle\Controller\Profil;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Formulaire\dmliBundle\Entity;
use Formulaire\AdminBundle\Form\AdminType;
use Symfony\Component\HttpFoundation\Response;

class ProfilController extends Controller
{

    /**
     * @Route("/profil", name="index_profil")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
            $services = $em->getRepository('dmliBundle:PosteBridge')->findAll();
//var_dump($services[0]->getPol()->getPolLibelle());die;
            $i = 0;
            foreach ($services as $service) {
                $result[$i]['id']=$service->getBdgId();
                $result[$i]['dir_id'] = $service->getDir()->getLibelle();
                $result[$i]['srv_id'] = $service->getSrv()->getLibelle();
                if($service->getPol() !=""){
                $result[$i]['pol_id']= $service->getPol()->getPolLibelle();
            }else{ $result[$i]['pol_id']= '';}
                $result[$i]['pst_id']= $service->getPst()->getPstLibelle();
                $i++;
            }
        return $this->render('AdminBundle:Profil:ListeProfil.html.twig', array('result'=> $result));
    }

    /**
     * @Route("/profil/affect/{id}", name="affect_profil")
     * @Template()
     */
    public function affectAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $services = $em->getRepository('dmliBundle:PosteBridge')->findBy(array('bdgId'=>$id));
        $result['id']=$services[0]->getBdgId();
        $result['dir_id'] = $services[0]->getDir()->getLibelle();
        $result['srv_id'] = $services[0]->getSrv()->getLibelle();
        if($services[0]->getPol() != ""){
                $result['pol_id']= $services[0]->getPol()->getPolLibelle();}else{ $result['pol_id']= 'NULL';}
        $result['pst_id']= $services[0]->getPst()->getPstLibelle();
        $form = $this->get('form.factory')->create(new AdminType());

        return $this->render('AdminBundle:Profil:AffecterProfil.html.twig', array(
            'form' => $form->createView(), 'result'=>$result
        ));

    }

    /**
     * @Route("/profil/hardware")
     *
     */
    public function hardwareAction()
    {
        $request = $this->getRequest();
        $return = array();
        if ($request->isXmlHttpRequest()) {

            $sql = "SELECT har_id, har_libelle, har_prix
				FROM dmli_hardware_har d inner join typemateriel t on d.typ_id=t.id where t.eta_id=1
			";
            $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll();

            $return = $result;
            $response = new Response();
            $data = json_encode($return);
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent($data);
            return $response;
        }
        return new Response('Erreur');
    }
    /**
     * @Route("/profil/listeHardware")
     *
     */
    public function listeHardwareAction()
    {
        $request = $this->getRequest();
        $return = array();
        if ($request->isXmlHttpRequest()) {
            $bdgId = $request->get('bdgId');

            $sql = "	SELECT dmli_hardware_har.har_id, dmli_hardware_har.har_libelle
				FROM dmli_hardware_har inner join typemateriel on dmli_hardware_har.typ_id=typemateriel.id where typemateriel.eta_id=1 and dmli_hardware_har.har_id IN (
					SELECT dmli_bdg_has_har.har_id
					FROM dmli_bdg_has_har
					WHERE dmli_bdg_has_har.bdg_id = " . $bdgId .
                "	);";

            $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll();

            if (count($result) > 1) {

                $return = $result;
                $response = new Response();
                $data = json_encode($return);
                $response->headers->set('Content-Type', 'application/json');
                $response->setContent($data);
                return $response;
            }
        }
        return new Response('Erreur');
    }
    /**
     * @Route("/profil/listeMail")
     *
     */
    public function listeMailAction()
    {
        $em = $this->getDoctrine()->getManager();

        $request = $this->getRequest();
        $return = array();
        if ($request->isXmlHttpRequest()) {
            $idListMail = $request->get('idListMail');

            $services = $em->getRepository('dmliBundle:DmliMailMel')->findBy(array('melId' => $idListMail));
            $i = 0;
            foreach ($services as $service) {
                $result[$i]['id'] = $service->getMelId();
                $result[$i]['libelle'] = $service->getMelLibelle();
                $i++;
            }

            if (count($result) > 0) {

                $return = $result;
                $response = new Response();
                $data = json_encode($return);
                $response->headers->set('Content-Type', 'application/json');
                $response->setContent($data);
                return $response;
            }
        }
        return new Response('Erreur');
    }
    /**
     * @Route("/profil/listeSoftware")
     *
     */
    public function listeSoftwareAction()
    {
        $request = $this->getRequest();
        $return = array();
        if ($request->isXmlHttpRequest()) {
            $bdgId = $request->get('bdgId');

            $sql = "	SELECT dmli_software_sft.sft_id, dmli_software_sft.sft_libelle
				FROM dmli_software_sft
				WHERE dmli_software_sft.sft_id IN (
					SELECT dmli_bdg_has_sft.sft_id
					FROM dmli_bdg_has_sft
					WHERE dmli_bdg_has_sft.bdg_id = " . $bdgId .
                "	);";

            $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll();

            if (count($result) > 1) {

                $return = $result;
                $response = new Response();
                $data = json_encode($return);
                $response->headers->set('Content-Type', 'application/json');
                $response->setContent($data);
                return $response;
            }
        }
        return new Response('Erreur');
    }
    /**
     * @Route("/profil/listHardwareInPack")
     *
     */
    public function listHardwareInPackAction()
    {
        $request = $this->getRequest();
        $return = array();
        if ($request->isXmlHttpRequest()) {
            $idPack = $request->get('pack');

            $sql = "	SELECT har_id, har_libelle, har_prix
			FROM dmli_hardware_har
			WHERE har_id IN (
				SELECT har_id
				FROM dmli_pkh_has_har
				WHERE pkh_id = " . $idPack . "
			)
		";

            $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll();

            $return = $result;
            $response = new Response();
            $data = json_encode($return);
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent($data);
            return $response;
        }
        return new Response('Erreur');
    }
    /**
     * @Route("/profil/packHardware")
     *
     */
    public function packHardwareAction()
    {
        $request = $this->getRequest();
        $return = array();
        if ($request->isXmlHttpRequest()) {


            $sql = "	SELECT pkh_id, pkh_libelle
			FROM dmli_packhardware_pkh
			ORDER BY pkh_libelle;
		";

            $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll();

            if (count($result) > 1) {

                $output = '<option style="visibility: hidden;" value=""> --- liste --- </option>';


                foreach ($result as $row) {
                    $output .= '<option value="' . $row['pkh_id'] . '">' . $row['pkh_libelle'] . '</option>';
                }

                $return = $output;
                $response = new Response();
                $data = json_encode($return);
                $response->headers->set('Content-Type', 'application/json');
                $response->setContent($data);
                return $response;
            }
        }
        return new Response('Erreur');
    }
    /**
     * @Route("/profil/software")
     *
     */
    public function softwareAction()
    {
        $request = $this->getRequest();

        if ($request->isXmlHttpRequest()) {

            $sql = "	SELECT sft_id, sft_libelle, sft_prix
				FROM dmli_software_sft
				ORDER BY sft_libelle;";

            $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll();


            $response = new Response();
            $data = json_encode($result);
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent($data);
            return $response;
        }
        return new Response('Erreur');
    }
    /**
     * @Route("/profil/upmail")
     *
     */
    public function upMailAction()
    {
        //$request = $this->getRequest();
        $return = array();
        //if ($request->isXmlHttpRequest()) {
//DAM
        //conexion du database
        $sql = "		SELECT mel_id, mel_libelle
					FROM dmli_mail_mel
					WHERE dmli_mail_mel.dir_id = 1
					ORDER BY mel_libelle;";
        //Envoi de la requete
        $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
        $query->execute();
        $result = $query->fetchAll();
        //Direction Achats Marketing
        $i = 0;
        foreach ($result as $row) {
            $output[0][$i]['id'] = $row['mel_id'];
            $output[0][$i]['libelle'] = $row['mel_libelle'];
            $i++;
        }
        //Dir vente
        //conexion du database
        $sql = "		SELECT mel_id, mel_libelle
					FROM dmli_mail_mel
					WHERE dmli_mail_mel.dir_id = 2
					ORDER BY mel_libelle;";
        $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
        $query->execute();
        $result = $query->fetchAll();
        //$output .= '<optgroup label="Direction des Ventes">';
        $i = 0;
        foreach ($result as $row) {
            $output[1][$i]['id'] = $row['mel_id'];
            $output[1][$i]['libelle'] = $row['mel_libelle'];
            $i++;
        }
        //Travaux
        //conexion du database
        $sql = "		SELECT mel_id, mel_libelle
					FROM dmli_mail_mel
					WHERE dmli_mail_mel.dir_id = 3
					ORDER BY mel_libelle;";

        $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
        $query->execute();
        $result = $query->fetchAll();
        //$output .= '<optgroup label="Direction des Travaux">';
        $i = 0;
        foreach ($result as $row) {
            $output[2][$i]['id'] = $row['mel_id'];
            $output[2][$i]['libelle'] = $row['mel_libelle'];
            $i++;
        }
        //Expansion
        //conexion du database
        $sql = "		SELECT mel_id, mel_libelle
					FROM dmli_mail_mel
					WHERE dmli_mail_mel.dir_id = 4
					ORDER BY mel_libelle;";

        $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
        $query->execute();
        $result = $query->fetchAll();
        //$output .= '<optgroup label="Expansion">';
        $i = 0;
        foreach ($result as $row) {
            $output[3][$i]['id'] = $row['mel_id'];
            $output[3][$i]['libelle'] = $row['mel_libelle'];
            $i++;
        }
        // var_dump($output);die;
        //Logistique
        //conexion du database
        $sql = "		SELECT mel_id, mel_libelle
					FROM dmli_mail_mel
					WHERE dmli_mail_mel.dir_id = 5
					ORDER BY mel_libelle;";

        $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
        $query->execute();
        $result = $query->fetchAll();

        //$output .= '<optgroup label="Direction Logistique">';

        $i = 0;
        foreach ($result as $row) {
            $output[4][$i]['id'] = $row['mel_id'];
            $output[4][$i]['libelle'] = $row['mel_libelle'];
            $i++;
        }

        //var_dump($output);die;


        //Support
        //conexion du database
        $sql = "		SELECT mel_id, mel_libelle
					FROM dmli_mail_mel
					WHERE dmli_mail_mel.dir_id = 6
					ORDER BY mel_libelle;";

        $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
        $query->execute();
        $result = $query->fetchAll();

        //$output .= '<optgroup label="Direction Support">';

        $i = 0;
        foreach ($result as $row) {
            $output[5][$i]['id'] = $row['mel_id'];
            $output[5][$i]['libelle'] = $row['mel_libelle'];
            $i++;
        }

        //var_dump($output);die;

        //Travaux
        //conexion du database
        $sql = "		SELECT mel_id, mel_libelle
					FROM dmli_mail_mel
					WHERE dmli_mail_mel.dir_id = 7
					ORDER BY mel_libelle;";

        $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
        $query->execute();
        $result = $query->fetchAll();

        // $output .= '<optgroup label="Diversification">';

        $i = 0;
        foreach ($result as $row) {
            $output[6][$i]['id'] = $row['mel_id'];
            $output[6][$i]['libelle'] = $row['mel_libelle'];
            $i++;
        }

        //var_dump($output);die;


        //Travaux
        //conexion du database
        $sql = "		SELECT mel_id, mel_libelle
					FROM dmli_mail_mel
					WHERE dmli_mail_mel.dir_id = 0
					ORDER BY mel_libelle;";

        $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
        $query->execute();
        $result = $query->fetchAll();



        $i = 0;
        foreach ($result as $row) {
            $output[7][$i]['id'] = $row['mel_id'];
            $output[7][$i]['libelle'] = $row['mel_libelle'];
            $i++;
        }

        // var_dump($output);die;


        $return = $output;
        $response = new Response();
        $data = json_encode($return);
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent($data);
        return $response;
        // }
        //return new Response('Erreur');
    }
    /**
     * @Route("/profil/listeMailingDirection")
     *
     */
    public function listeMailingDirectionAction()
    {
        $request = $this->getRequest();
        $return = array();
        $em = $this->getDoctrine()->getManager();
        if ($request->isXmlHttpRequest()) {
            $idDire = $request->get('direction');
            $services = $em->getRepository('dmliBundle:PosteBridge')->findBy(array('bdgId'=>$idDire));

            $idDir= $services[0]->getDir()->getId();
            $sql = "SELECT mel_id, mel_libelle
				FROM dmli_mail_mel
				WHERE dmli_mail_mel.dir_id = " . $idDir . "
				ORDER BY mel_libelle;";


            $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll();

            $dir = "SELECT  libelle
				FROM adm_direction
				WHERE adm_direction.id = " . $idDir . ";";

            $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($dir);
            $query->execute();
            $libDir = $query->fetchAll();


            $i = 0;
            $output[0][$i]['direction'] = $libDir[0]['libelle'];
            $i++;
            foreach ($result as $row) {
                $output[0][$i]['id'] = $row['mel_id'];
                $output[0][$i]['libelle'] = $row['mel_libelle'];
                $i++;
            }

            $sql = "SELECT mel_id, mel_libelle
				FROM dmli_mail_mel
				WHERE dmli_mail_mel.dir_id = 0
				ORDER BY mel_libelle;";

            $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll();
            $i = 0;
            $output[1][$i]['Autres'] = "Autres listes";

            $i++;
            foreach ($result as $row) {
                $output[1][$i]['id'] = $row['mel_id'];
                $output[1][$i]['libelle'] = $row['mel_libelle'];
                $i++;
            }

            $return = $output;
            $response = new Response();
            $data = json_encode($return);
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent($data);
            return $response;
        }
        return new Response('Erreur');
    }
    /**
     * @Route("/profil/listeMailingWithPost")
     *
     */
    public function listeMailingWithPostAction()
    {
        $request = $this->getRequest();
        $return = array();
        if ($request->isXmlHttpRequest()) {
            $idBridge = $request->get('idBridge');

            $sql = "
				SELECT dmli_mail_mel.mel_id, dmli_mail_mel.mel_libelle
				FROM dmli_mail_mel
				WHERE mel_id IN (
					SELECT mel_id
					FROM dmli_bdg_has_mel
					WHERE bdg_id = " . $idBridge . ")";

            $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll();
            // var_dump($result);die;
            if (count($result) > 0) {
                $return = $result;
                $response = new Response();
                $data = json_encode($return);
                $response->headers->set('Content-Type', 'application/json');
                $response->setContent($data);
                return $response;
            }
        }
        return new Response('Erreur');
    }


    /**
     * @Route("/profil/remove/{id}", name="remove_profil")
     * @Template()
     */
    public function removeAction($id)
    {  $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();

        $product = $em->getRepository('dmliBundle:PosteBridge')->find($id);

        $em->remove($product);
        $em->flush();
        $request->getSession()->getFlashBag()->add(
            'success',
            'profil supprimé!'
        );
        return $this->redirect($this->generateUrl('index_profil'));

    }
}
