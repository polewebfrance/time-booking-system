<?php

namespace Formulaire\AdminBundle\Controller\Profil;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Formulaire\dmliBundle\Entity;
use Formulaire\AdminBundle\Form\AdminType;
use Symfony\Component\HttpFoundation\Response;

class EditProfilController extends Controller
{

/**
* @Route("/profil/edit/{id}", name="edit_profil")
* @Template()
*/
public function editAction($id)
{
    $em = $this->getDoctrine()->getManager();
    $services = $em->getRepository('dmliBundle:PosteBridge')->findBy(array('bdgId'=>$id));
    $result['id']=$services[0]->getBdgId();
    $result['dir_id'] = $services[0]->getDir()->getLibelle();
    $result['srv_id'] = $services[0]->getSrv()->getLibelle();
    if(!($services[0]->getPol() != "")){
        $result['pol_id']= $services[0]->getPol()->getPolLibelle();}else{ $result['pol_id']= 'NULL';}
    $result['pst_id']= $services[0]->getPst()->getPstLibelle();
    $form = $this->get('form.factory')->create(new AdminType());
    return $this->render('AdminBundle:Profil:ModifierProfil.html.twig', array(
        'form' => $form->createView(), 'result'=>$result
    ));

}



}