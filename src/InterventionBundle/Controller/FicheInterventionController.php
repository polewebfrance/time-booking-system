<?php

namespace InterventionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use InterventionBundle\Form\TrvInterventionType;
use Symfony\Component\HttpFoundation\Response;
use InterventionBundle\Entity\TrvAgents;
use InterventionBundle\Entity\TrvCout;
use InterventionBundle\Entity\TrvFacture;
use InterventionBundle\Entity\TrvIntervention;
use InterventionBundle\Entity\TrvObservateur;
use InterventionBundle\Entity\TrvPieceJointe;
use InterventionBundle\Entity\TrvTournee;
use InterventionBundle\Form\TrvFactureType;
use InterventionBundle\Entity\TrvFournisseur;
/**
 * FicheIntervention controller.
 *
 * @Route("/fiche")
 */
class FicheInterventionController extends Controller
{
    /**
     * @Route("/fiche_intervention/{id}", name="fiche_intervention")
     */
    public function ficheAction($id){
        $form = $this->createForm(new TrvInterventionType());
        $interv= $this->getDoctrine()->getRepository("InterventionBundle:TrvIntervention")-> findOneBy(array('id'=>$id));
        $couts=  $this->getDoctrine()->getRepository("InterventionBundle:TrvCout")-> findBy(array('idIntervention'=>$id));
        $natures= $this->getDoctrine()->getRepository("InterventionBundle:TrvNature")-> findAll();
        $types= $this->getDoctrine()->getRepository("InterventionBundle:TrvType")-> findAll();
        $agents= $this->getDoctrine()->getRepository("InterventionBundle:TrvAgents")-> findBy(array('idIntervention'=>$id));
        $agents_all = $this->getDoctrine()
            ->getRepository('UserUserBundle:User')
            ->findByRoles('ROLE_INTERVENTION_TRAVAUX_AGENT');
        $pieces= $this->getDoctrine()->getRepository("InterventionBundle:TrvPieceJointe")-> findBy(array('idIntervention'=>$id, 'idTournee'=> null, 'idFacture'=>null));
        $role1='ROLE_INTERVENTION_TRAVAUX_ASSISTANTE';
        $role2='ROLE_INTERVENTION_TRAVAUX_OBSERVATEUR';
        $qb = $this->getDoctrine()->getManager()->createQueryBuilder();
        $qb->add('select', 'u')
            ->from('User\UserBundle\Entity\User', 'u')
            ->where('u.roles LIKE :role1')
            ->ORwhere('u.roles LIKE :role2')
            ->setParameter('role1', '%"' . $role1 . '"%')
            ->setParameter('role2', '%"' . $role2 . '"%');
        $observateurs_all =$qb->getQuery()
            ->setFetchMode("User\UserBundle\Entity\User", "elements", "EAGER")
            ->getResult();
        $observateurs= $this->getDoctrine()->getRepository("InterventionBundle:TrvObservateur")-> findBy(array('idIntervention'=>$id));
        $tab= array();
        foreach ($observateurs as $observateur){
            array_push($tab, $observateur->getIdUser()->getId());
        }
        if ($interv->getIdTournee()!=null){
            $piece_tr= $this->getDoctrine()->getRepository("InterventionBundle:TrvPieceJointe")-> findOneBy(array('idTournee'=>$interv->getIdTournee()->getId(),'idIntervention'=>$interv->getId()));
        }
        else{
            $piece_tr=null;
        }
        if($interv->getIdMagasin()!=null){
            $lieu=$interv->getIdMagasin()->getMagId();
        }
        elseif ($interv->getIdBatiment()!=null){
            $lieu=$interv->getIdBatiment()->getId();
        }
        $q = $this->getDoctrine()->getManager()->createQueryBuilder();
        $q->add('select', 'p')
            ->from('InterventionBundle\Entity\TrvPieceJointe', 'p')
            ->where('p.idFacture IS NOT NULL')
            ->ANDwhere('p.idIntervention = :i')
            ->setParameter('i', $id);
        $piece_fact =$q->getQuery()
            ->setFetchMode("InterventionBundle\Entity\TrvPieceJointe", "elements", "EAGER")
            ->getResult();
        $tabPieceFact=array();
        foreach ($piece_fact as $p){
            array_push($tabPieceFact, $p->getIdFacture()->getId());
        }
        $factures= $this->getDoctrine()->getRepository("InterventionBundle:TrvFacture")-> findBy(array('idIntervention'=>$id));
        $fournisseurs= $this->getDoctrine()->getRepository("InterventionBundle:TrvFournisseur")-> findAll();
        $form->bind($this->getRequest());
        if ($this->getRequest()->isMethod('POST')) {
            $em = $this->getDoctrine()->getManager();
            $interv->setDateDemande(new \DateTime($_POST["interventionbundle_trvintervention"]["dateDemande"]));
            $interv->setDateDebut(new \DateTime($_POST["interventionbundle_trvintervention"]["dateDebut"]));
            $interv->setDateFin(new \DateTime($_POST["interventionbundle_trvintervention"]["dateFin"]));
            $interv->setTravaux($_POST["interventionbundle_trvintervention"]["travaux"]);
            $raison=$em->getRepository('InterventionBundle:TrvRaison')->findOneBy(array('id'=>$_POST["interventionbundle_trvintervention"]["idRaison"]));
            $interv->setIdRaison($raison);
            $mag=$em->getRepository('AdministrationSocieteBundle:AdmMagasin')->findOneBy(array('magId'=>$_POST["interventionbundle_trvintervention"]["lieux"]));
            if($mag== null){
                $bat= $em->getRepository('InterventionBundle:TrvBatiments')->findOneBy(array('id'=>$_POST["interventionbundle_trvintervention"]["lieux"]));
                $interv->setIdBatiment($bat);
                $interv->setIdMagasin(null);
            }
            else{
                $interv->setIdMagasin($mag);
                $interv->setIdBatiment(null);
            }
            foreach ($couts as $cout){
                $type=$em->getRepository('InterventionBundle:TrvType')->findOneBy(array('id'=>$_POST["interventionbundle_trvintervention"]["type".$cout->getId()]));
                $cout->setMontant($_POST["interventionbundle_trvintervention"]["cout".$cout->getId()]);
                $cout->setIdType($type);
                $em->persist($cout);
                $em->flush();
            }
            foreach ($agents as $agent){
                $u= $em->getRepository('UserUserBundle:User')->findOneBy(array('id'=>$_POST["interventionbundle_trvintervention"]["agent".$agent->getId()]));
                $agent->setIdUser($u);
                $agent->setNbHeures($_POST["interventionbundle_trvintervention"]["nbHeures".$agent->getId()]);
                $em->persist($agent);
                $em->flush();
            }
            $dir = $this->get('kernel')->getRootDir().'/../web/uploads/attachments/intervention/'.$interv->getId().'/';
            if($pieces!=null){
                $i=1;
                foreach ($pieces as $piece){
                    if (isset($form['file'.$i])) {
                        $fichier= $form['file'.$i]->getData();
                        if (!empty($fichier)){
                            $origLongName = $fichier->getClientOriginalName();
                            $origExtension = $fichier->getClientOriginalExtension();
                            $origShortName = substr($origLongName, 0, strlen($origLongName) - strlen($origExtension) - 1);
                            $finalFileName = $origShortName . rand(1, 99999) . '.' . $origExtension;
                            $fichier->move($dir, $finalFileName);
                            $piece->setAttachement($finalFileName);
                            $em->persist($piece);
                            $em->flush();
                        }
                    }
                    $i++;
                }
            }else{
                if (isset($form['file1'])) {
                    $fichier= $form['file1']->getData();
                    if (!empty($fichier)){
                        $piece = new TrvPieceJointe();
                        $origLongName = $fichier->getClientOriginalName();
                        $origExtension = $fichier->getClientOriginalExtension();
                        $origShortName = substr($origLongName, 0, strlen($origLongName) - strlen($origExtension) - 1);
                        $finalFileName = $origShortName . rand(1, 99999) . '.' . $origExtension;
                        $fichier->move($dir, $finalFileName);
                        $piece->setAttachement($finalFileName);
                        $piece->setIdIntervention($interv);
                        $em->persist($piece);
                        $em->flush();
                    }
                }
            }
            $tab1=array();
            foreach ($observateurs as $o){
                array_push($tab1, $o->getIdUser()->getId());
            }
            if(isset($_POST["interventionbundle_trvintervention"]["observateur"])) {
                foreach ($_POST["interventionbundle_trvintervention"]["observateur"] as $observateur) {
                    if (!(in_array($observateur, $tab1))) {
                        $observateurAajouter = new TrvObservateur();
                        $user = $this->getDoctrine()->getRepository("UserUserBundle:User")->findOneBy(array('id' => $observateur));
                        $observateurAajouter->setIdIntervention($interv);
                        $observateurAajouter->setIdUser($user);
                        $em->persist($observateurAajouter);
                        $em->flush();

                    }
                }
                foreach ($tab1 as $t){
                    //var_dump($t);die;
                    if(!(in_array($t, $_POST["interventionbundle_trvintervention"]["observateur"] ))){
                        $observateurAsupprimer=$this->getDoctrine()->getRepository("InterventionBundle:TrvObservateur")-> findOneBy(array('idUser'=>$t, 'idIntervention'=> $interv->getId()));
                        $em->remove($observateurAsupprimer);
                        $em->flush();

                    }
                }
            }
            else{
                foreach ($tab1 as $t){
                    $observateurAsupprimer=$this->getDoctrine()->getRepository("InterventionBundle:TrvObservateur")-> findOneBy(array('idUser'=>$t, 'idIntervention'=> $interv->getId()));
                    $em->remove($observateurAsupprimer);
                    $em->flush();
                }
            }
            if( $interv->getIdTournee()!=null){
                $interv->setDepart($_POST["interventionbundle_trvintervention"]["villeDep"]);
                $interv->setArrivee($_POST["interventionbundle_trvintervention"]["villeArrivee"]);
                $tournee= $interv->getIdTournee();
                $tournee->setNbvehicule($_POST["interventionbundle_trvintervention"]["nbrVehicules"]);
                $tournee->setNbkm($_POST["interventionbundle_trvintervention"]["nbrKm"]);
                $tournee->setHebergement($_POST["interventionbundle_trvintervention"]["hebergement"]);
                $tournee->setRepas($_POST["interventionbundle_trvintervention"]["repas"]);
                $tournee->setCommentaire($_POST["interventionbundle_trvintervention"]["commentaire"]);
                $em->persist($tournee);
                $em->flush();
                if (isset($form['fileTournee'])) {
                    $fichier= $form['fileTournee']->getData();
                    if (!empty($fichier)){
                        if($piece_tr==null){
                            $piece_tr = new TrvPieceJointe();
                            $piece_tr->setIdIntervention($interv);
                            $piece_tr->setIdTournee($interv->getIdTournee());
                        }
                        $origLongName = $fichier->getClientOriginalName();
                        $origExtension = $fichier->getClientOriginalExtension();
                        $origShortName = substr($origLongName, 0, strlen($origLongName) - strlen($origExtension) - 1);
                        $finalFileName = $origShortName . rand(1, 99999) . '.' . $origExtension;
                        $fichier->move($dir, $finalFileName);
                        $piece_tr->setAttachement($finalFileName);
                        $em->persist($piece_tr);
                        $em->flush();
                    }
                }
            }
            $interv->setCommentaireAgent($_POST["interventionbundle_trvintervention"]["commentaireAgent"]);
            $statut=$em->getRepository('InterventionBundle:TrvStatus')->findOneBy(array('id'=>5));
            $interv->setIdStatus($statut);
            $em->persist($interv);
            $em->flush();
            $tab=array();
            $obs= $this->getDoctrine()->getRepository("InterventionBundle:TrvObservateur")-> findBy(array('idIntervention'=>$id));
            foreach ($obs as $observateur){
                array_push($tab, $observateur->getIdUser()->getId());
            }
            $i=0;
            foreach ($factures as $fact){
                $i++;
                if(isset($_POST["interventionbundle_trvintervention"]["facture".$i])) {
                    $fact->setBonLivraison($_POST["interventionbundle_trvintervention"]["facture".$i]);
                    $fact->setReference($_POST["interventionbundle_trvintervention"]["reference".$i]);
                    $f=$em->getRepository('InterventionBundle:TrvFournisseur')->findOneBy(array('id'=>$_POST["interventionbundle_trvintervention"]["fournisseur".$i]));
                    $fact->setFournisseur($f);
                    $fact->setMontant($_POST["interventionbundle_trvintervention"]["montant".$i]);
                    $em->persist($fact);
                    $em->flush();
                    if (isset($form['filefact'.$i])) {
                        $fichier= $form['filefact'.$i]->getData();
                        if (!empty($fichier)){
                            $piece = $this->getDoctrine()->getRepository("InterventionBundle:TrvPieceJointe")-> findOneBy(array('idIntervention'=>$id, 'idFacture'=>$fact->getId()));
                            if($piece==null){
                                $piece=new TrvPieceJointe();
                            }
                            $origLongName = $fichier->getClientOriginalName();
                            $origExtension = $fichier->getClientOriginalExtension();
                            $origShortName = substr($origLongName, 0, strlen($origLongName) - strlen($origExtension) - 1);
                            $finalFileName = $origShortName . rand(1, 99999) . '.' . $origExtension;
                            $fichier->move($dir, $finalFileName);
                            $piece->setAttachement($finalFileName);
                            $piece->setIdIntervention($interv);
                            $piece->setIdFacture($fact);
                            $em->persist($piece);
                            $em->flush();
                        }
                    }
                }
            }
            while ($i <= $_POST["interventionbundle_trvintervention"]["countfacture"] ){
                $i++;
                if(isset($_POST["interventionbundle_trvintervention"]["facture".$i])) {
                    $fact= new TrvFacture();
                    $fact->setBonLivraison($_POST["interventionbundle_trvintervention"]["facture".$i]);
                    $fact->setReference($_POST["interventionbundle_trvintervention"]["reference".$i]);
                    $f=$em->getRepository('InterventionBundle:TrvFournisseur')->findOneBy(array('id'=>$_POST["interventionbundle_trvintervention"]["fournisseur".$i]));
                    $fact->setFournisseur($f);
                    $fact->setMontant($_POST["interventionbundle_trvintervention"]["montant".$i]);
                    $fact->setIdIntervention($interv);
                    $em->persist($fact);
                    $em->flush();
                    if (isset($form['filefact'.$i])) {
                        $fichier= $form['filefact'.$i]->getData();
                        if (!empty($fichier)){
                            $piece = new TrvPieceJointe();
                            $origLongName = $fichier->getClientOriginalName();
                            $origExtension = $fichier->getClientOriginalExtension();
                            $origShortName = substr($origLongName, 0, strlen($origLongName) - strlen($origExtension) - 1);
                            $finalFileName = $origShortName . rand(1, 99999) . '.' . $origExtension;
                            $fichier->move($dir, $finalFileName);
                            $piece->setAttachement($finalFileName);
                            $piece->setIdIntervention($interv);
                            $piece->setIdFacture($fact);
                            $em->persist($piece);
                            $em->flush();
                        }
                    }
                }
            }

            $q = $this->getDoctrine()->getManager()->createQueryBuilder();
            $q->add('select', 'p')
                ->from('InterventionBundle\Entity\TrvPieceJointe', 'p')
                ->where('p.idFacture IS NOT NULL')
                ->ANDwhere('p.idIntervention = :i')
                ->setParameter('i', $id);
            $piece_fact =$q->getQuery()
                ->setFetchMode("InterventionBundle\Entity\TrvPieceJointe", "elements", "EAGER")
                ->getResult();
            $tabPieceFact=array();
            foreach ($piece_fact as $p){
                array_push($tabPieceFact, $p->getIdFacture()->getId());
            }
            $factures= $this->getDoctrine()->getRepository("InterventionBundle:TrvFacture")-> findBy(array('idIntervention'=>$id));
        }

        return $this->render('InterventionBundle:Default:fiche_intervention.html.twig',
            array('form' => $form->createView(), 'tab'=> $tab, 'observateurs_all'=>$observateurs_all, 'interv'=> $interv,
                'natures'=> $natures, 'couts'=>$couts, 'types'=>$types, 'agents'=> $agents, 'pieces'=> $pieces,
                'factures'=>$factures, 'lieu'=>$lieu, 'piece_tr'=> $piece_tr, 'tabPieceFact'=>$tabPieceFact,
                'agents_all'=> $agents_all, 'fournisseurs'=> $fournisseurs, 'piece_fact'=>$piece_fact));
    }

    /**
     * @Route("/nv_fournisseurs", name="nv_fournisseurs")
     */

    public function nvFournisseurAction()
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $nv_fournisseur = $request->get('nv_fournisseur');
        $fournisseur= new TrvFournisseur();
        $fournisseur->setnom($nv_fournisseur);
        $em->persist($fournisseur);
        $em->flush();
        $res=$fournisseur->getId();
        $return = $res;
        $response = new Response();
        $data = json_encode($return);
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent($data);
        return $response;
    }

    /**
     * @Route("/lieux/{id}", name="lieux")
     */

    public function lieuxAction($id){
        $result = array();
        $em = $this->getDoctrine()->getManager();
        $qb = $this->getDoctrine()->getManager()->createQueryBuilder();
        $today= new \DateTime();
        $qb->add('select', 'mag')
            ->from('Administration\SocieteBundle\Entity\AdmMagasin', 'mag')
            ->where('mag.magFermeture > :date')
            ->setParameter('date', $today);
        $mags =$qb->getQuery()
            ->setFetchMode("Administration\SocieteBundle\Entity\AdmMagasin", "elements", "EAGER")
            ->getResult();
        $batis = $em->getRepository('InterventionBundle:TrvBatiments')->findBy(array('actif'=>1));
        $i = 0;

        foreach ($mags as $mag) {
            $result[$i]['id'] = $mag->getMagId();
            $result[$i]['name'] = $mag->getMagLibelle();
            $i++;
        }

        foreach ($batis as $bat) {
            $result[$i]['id'] = $bat->getId();
            $result[$i]['name'] = $bat->getNom();
            $i++;
        }
        $output = '<option value="-1"> </option>';
        foreach ($result as $row) {
            if ($row['id']== $id){
                $output .= '<option selected value="' . $row['id'] . '">' . $row['name'] . '</option>';
            }
            else{
                $output .= '<option value="' . $row['id'] . '">' . $row['name'] . '</option>';
            }
        }

        $return = $output;
        $response = new Response();
        $data = json_encode($return);
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent($data);
        return $response;
    }

    /**
     * @Route("/type", name="type")
     */

    public function typeAction()
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $nat = $request->get('nat');
        $result = array();
        $types = $em->getRepository('InterventionBundle:TrvType')->findBy(array('idNature' => $nat, 'actif'=>'1'));
        $i = 0;
        foreach ($types as $type) {
            $result[$i]['id'] = $type->getId();
            $result[$i]['libelle'] = $type->getLibelle();
            $i++;
        }
        if($types!=null){
            $output = '<option  value="0">Liste des types</option>';
            foreach ($result as $row) {
                $output .= '<option value="' . $row['id'] . '">' . $row['libelle'] . '</option>';
            }
        }
        else{
            $output='<option value="0">Aucun type</option>';
        }
        $return = $output;
        $response = new Response();
        $data = json_encode($return);
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent($data);
        return $response;
    }
}
