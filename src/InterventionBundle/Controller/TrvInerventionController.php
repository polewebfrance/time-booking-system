<?php

namespace InterventionBundle\Controller;

use InterventionBundle\Entity\TrvAgents;
use InterventionBundle\Entity\TrvCout;
use InterventionBundle\Entity\TrvFacture;
use InterventionBundle\Entity\TrvIntervention;
use InterventionBundle\Entity\TrvObservateur;
use InterventionBundle\Entity\TrvPieceJointe;
use InterventionBundle\Entity\TrvTournee;
use InterventionBundle\Form\TrvFactureType;
use InterventionBundle\Form\TrvInterventionType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use InterventionBundle\Entity\TrvFournisseur;
use Swift_Validate;
use Symfony\Component\Validator\Constraints\File;


/**
 * TrvIntervention controller.
 *
 * @Route("/ajout")
 */
class TrvInerventionController extends Controller
{
    /**
     * @Route("/", name="intervention")
     */

    public function addInterventionAction(Request $request){
        $form = $this->createForm(new TrvInterventionType());
        $session = $this->getRequest()->getSession();
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
            $interv= new TrvIntervention();
            $interv = $form->getData();
            $interv->setDateDemande(new \DateTime($_POST["interventionbundle_trvintervention"]["dateDemande"]));
            $interv->setDateDebut(new \DateTime($_POST["interventionbundle_trvintervention"]["dateDebut"]));
            $interv->setDateFin(new \DateTime($_POST["interventionbundle_trvintervention"]["dateFin"]));
            $interv->setAssistante($this->get('security.context')->getToken()->getUser());
            $statut=$em->getRepository('InterventionBundle:TrvStatus')->findOneBy(array('id'=>1));
            $interv->setIdStatus($statut);
            $mag=$em->getRepository('AdministrationSocieteBundle:AdmMagasin')->findOneBy(array('magId'=>$_POST["interventionbundle_trvintervention"]["lieux"]));
            if($mag== null){
                $bat= $em->getRepository('InterventionBundle:TrvBatiments')->findOneBy(array('id'=>$_POST["interventionbundle_trvintervention"]["lieux"]));
                $interv->setIdBatiment($bat);
                $interv->setIdMagasin(null);
            }
            else{
                $interv->setIdMagasin($mag);
                $interv->setIdBatiment(null);
            }
            $statut=$em->getRepository('InterventionBundle:TrvStatus')->findOneBy(array('id'=>5));
            $interv->setIdStatus($statut);
            $em->persist($interv);
            $em->flush();
            //Enregistrer tous les types et les natures
            for($i=1; $i<=$_POST["interventionbundle_trvintervention"]["countNat"];$i++){
                if(isset($_POST["interventionbundle_trvintervention"]["type".$i])){
                    $type=$em->getRepository('InterventionBundle:TrvType')->findOneBy(array('id'=>$_POST["interventionbundle_trvintervention"]["type".$i]));
                    $t = new TrvCout();
                    $t->setMontant(0);
                    $t->setIdType($type);
                    $t->setIdIntervention($interv);
                    $em->persist($t);
                    $em->flush();

                }
            }
            $dir = $this->get('kernel')->getRootDir().'/../web/uploads/attachments/intervention/'.$interv->getId().'/';
            //Enregistrer tous les pieces jointes
            for($i=1; $i<=$_POST["interventionbundle_trvintervention"]["countFile"];$i++){
                if (isset($form['file'.$i])) {
                    $fichier= $form['file'.$i]->getData();
                    if (!empty($fichier)){
                        $piece = new TrvPieceJointe();
                        $origLongName = $fichier->getClientOriginalName();
                        $origExtension = $fichier->getClientOriginalExtension();
                        $origShortName = substr($origLongName, 0, strlen($origLongName) - strlen($origExtension) - 1);
                        $finalFileName = $origShortName . rand(1, 99999) . '.' . $origExtension;
                        $fichier->move($dir, $finalFileName);
                        $piece->setAttachement($finalFileName);
                        $piece->setIdIntervention($interv);
                        $em->persist($piece);
                        $em->flush();
                    }
                }
            }
            //Enregistrer tous les observateurs
            if(isset($_POST["interventionbundle_trvintervention"]["observateur"])){
                $obs=array($_POST["interventionbundle_trvintervention"]["observateur"]);
                foreach ( $obs[0] as $o){
                    $u= $em->getRepository('UserUserBundle:User')->findOneBy(array('id'=> $o));
                    $observateur= new TrvObservateur();
                    $observateur->setIdUser($u);
                    $observateur->setIdIntervention($interv);
                    $em->persist($observateur);
                    $em->flush();

                }
            }
            //Enregistrer tous les agents
            for($i=1; $i<=$_POST["interventionbundle_trvintervention"]["countAgent"];$i++){
                if(isset($_POST["interventionbundle_trvintervention"]["agent".$i])){
                    $u= $em->getRepository('UserUserBundle:User')->findOneBy(array('id'=> $_POST["interventionbundle_trvintervention"]["agent".$i]));

                    $agent= new TrvAgents();
                    $agent->setIdUser($u);
                    $agent->setIdIntervention($interv);
                        if($_POST["interventionbundle_trvintervention"]["nbHeures".$i]!= ''){
                            $agent->setNbheures($_POST["interventionbundle_trvintervention"]["nbHeures".$i]);
                        }
                        else{
                            $agent->setNbheures(0);
                        }

                    $em->persist($agent);
                    $em->flush();
                }
            }
            //Envoyer l'email aux agents sélectionnés
            $this->mailInterv($interv);
            if(!$session->has('success')) $session->set('success',"L'intervention est ajoutée avec succés");
            $form = $this->createForm(new TrvInterventionType());
        }
        $role1='ROLE_INTERVENTION_TRAVAUX_ASSISTANTE';
        $role2='ROLE_INTERVENTION_TRAVAUX_OBSERVATEUR';
        $qb = $this->getDoctrine()->getManager()->createQueryBuilder();
        $qb->add('select', 'u')
            ->from('User\UserBundle\Entity\User', 'u')
            ->where('u.roles LIKE :role1')
            ->ORwhere('u.roles LIKE :role2')
            ->setParameter('role1', '%"' . $role1 . '"%')
            ->setParameter('role2', '%"' . $role2 . '"%');
        $users =$qb->getQuery()
            ->setFetchMode("User\UserBundle\Entity\User", "elements", "EAGER")
            ->getResult();
        return $this->render('InterventionBundle:Default:intervention.html.twig',
            array('form' => $form->createView(), 'users'=> $users));
    }


    /**
     * @Route("/mail_interv/{interv}", name="mail_interv")
     */

    public function mailInterv($interv){
        $agents= $this->getDoctrine()->getRepository("InterventionBundle:TrvAgents")-> findBy(array('idIntervention'=>$interv->getId()));
        $couts= $this->getDoctrine()->getRepository("InterventionBundle:TrvCout")-> findBy(array('idIntervention'=>$interv->getId()));
        foreach ( $agents as $agent){
            $template = 'InterventionBundle:Mail:nv_intervention.html.twig';
            if($this->container->get('kernel')->getEnvironment()!='prod'){
				$url = $this->container->getParameter('url_serveur_recette');
				$body = $this->render($template, array('interv' => $interv, 'agent'=>$agent, 'couts'=>$couts, 'url'=>$url));
				$message = \Swift_Message::newInstance()
                    ->setSubject("TEST: Demande d'intervention")
                    ->setFrom("interventiontravaux@noz.fr")
                    ->addTo($agent->getIdUser()->getEmail())
                    ->setBody($body."\nCECI EST UN MAIL DE TEST, MERCI DE NE PAS EN TENIR COMPTE")
                    ->setContentType('text/html');
            }else{
				$url = $this->container->getParameter('url_serveur');
				$body = $this->render($template, array('interv' => $interv, 'agent'=>$agent, 'couts'=>$couts, 'url'=>$url));
				$message = \Swift_Message::newInstance()
                    ->setSubject("Demande d'intervention")
                    ->setFrom("interventiontravaux@noz.fr")
                    ->addTo($agent->getIdUser()->getEmail())
                    ->setBody($body)
                    ->setContentType('text/html');
            }
            $mailer = $this->get('mailer');
            $mailer->send($message);
            $spool = $mailer->getTransport()->getSpool();
            $transport = $this->get('swiftmailer.transport.real');
            $spool->flushQueue($transport);
        }


    }


    /**
     * @Route("/lieu", name="lieu")
     */

    public function lieuAction(){
        $result = array();
        $em = $this->getDoctrine()->getManager();
        $qb = $this->getDoctrine()->getManager()->createQueryBuilder();
        $today= new \DateTime();
        $qb->add('select', 'mag')
            ->from('Administration\SocieteBundle\Entity\AdmMagasin', 'mag')
            ->where('mag.magFermeture > :date')
            ->setParameter('date', $today);
        $mags =$qb->getQuery()
            ->setFetchMode("Administration\SocieteBundle\Entity\AdmMagasin", "elements", "EAGER")
            ->getResult();
        $batis = $em->getRepository('InterventionBundle:TrvBatiments')->findBy(array('actif'=>1));
        $i = 0;

        foreach ($mags as $mag) {
            $result[$i]['id'] = $mag->getMagId();
            $result[$i]['name'] = $mag->getMagLibelle();
            $i++;
        }

        foreach ($batis as $bat) {
            $result[$i]['id'] = $bat->getId();
            $result[$i]['name'] = $bat->getNom();
            $i++;
        }
        $output = '<option value> </option>';
        foreach ($result as $row) {
                $output .= '<option value="' . $row['id'] . '">' . $row['name'] . '</option>';
        }

        $return = $output;
        $response = new Response();
        $data = json_encode($return);
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent($data);
        return $response;
    }
    /**
     * @Route("/types", name="types")
     */

    public function typesAction()
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $nat = $request->get('nat');
        $result = array();
        $types = $em->getRepository('InterventionBundle:TrvType')->findBy(array('idNature' => $nat, 'actif'=>'1'));
        $i = 0;
        foreach ($types as $type) {
            $result[$i]['id'] = $type->getId();
            $result[$i]['libelle'] = $type->getLibelle();
            $i++;
        }
        if($types!=null){
            $output = '<option  value="0">Liste des types</option>';
            foreach ($result as $row) {
                $output .= '<option value="' . $row['id'] . '">' . $row['libelle'] . '</option>';
            }
        }
        else{
            $output='<option value="0">Aucun type</option>';
        }
        $return = $output;
        $response = new Response();
        $data = json_encode($return);
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent($data);
        return $response;
    }

    /**
     * @Route("/agent_maint_tr", name="agent_maint_tr")
     */

    public function agentAction(){

        $result = array();
        $agents = $this->getDoctrine()
            ->getRepository('UserUserBundle:User')
            ->findByRoles('ROLE_INTERVENTION_TRAVAUX_AGENT');

        $i = 0;

        foreach ($agents as $agent) {
            $result[$i]['id'] = $agent->getId();
            $result[$i]['name'] = $agent->getDisplayname();
            $i++;
        }
        $output = '<option value="0"> </option>';
        foreach ($result as $row) {
            $output .= '<option value="' . $row['id'] . '">' . $row['name'] . '</option>';
        }
        $return = $output;
        $response = new Response();
        $data = json_encode($return);
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent($data);
        return $response;
    }

    /**
     * @Route("/nature", name="nature")
     */

    public function natureAction(){

        $result = array();
        $em = $this->getDoctrine()->getManager();
        $natures = $em->getRepository('InterventionBundle:TrvNature')->findBy(array('actif'=>'1'));

        $i = 0;

        foreach ($natures as $nature) {
            $result[$i]['id'] = $nature->getId();
            $result[$i]['name'] = $nature->getLibelle();
            $i++;
        }
        $output = '<option value="0"> </option>';
        foreach ($result as $row) {
            $output .= '<option value="' . $row['id'] . '">' . $row['name'] . '</option>';
        }
        $return = $output;
        $response = new Response();
        $data = json_encode($return);
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent($data);
        return $response;
    }


}
