<?php

namespace InterventionBundle\Controller;

use InterventionBundle\Form\RechercheType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
/**
 * TrvIntervention controller.
 *
 * @Route("/liste_interventions")
 */
class ListeInterventionsController extends Controller
{
    /**
     * @Route("/", name="tab_interventions")
     */

    public function tabAction(){
        $form = $this->createForm(new RechercheType());
        $agents = $this->getDoctrine()
            ->getRepository('UserUserBundle:User')
            ->findByRoles('ROLE_INTERVENTION_TRAVAUX_AGENT');
        $qb = $this->getDoctrine()->getManager()->createQueryBuilder();
        $qb->add('select', 'interv')
            ->from('InterventionBundle\Entity\TrvIntervention', 'interv');
        $qb->setMaxResults(100);
        $interventions =$qb->getQuery()
            ->setFetchMode("InterventionBundle\Entity\TrvIntervention", "elements", "EAGER")
            ->getResult();
        $session = $this->getRequest()->getSession();
        $session->remove('statut');
        $session->remove('agent');
        $nature='';
        $agentMaintenance='';
        $interv=array();
        $i=0;
        foreach ($interventions as $intervention){
            $nature='';
            $agentMaintenance='';
            $couts=$this->getDoctrine()->getRepository('InterventionBundle:TrvCout')->findBy(array('idIntervention'=>$intervention->getId()));
            foreach ($couts as $cout){
                $nature.= $cout->getIdType()->getIdNature()->getLibelle().' - '.$cout->getIdType()->getLibelle().' - ';
            }
            $nature=rtrim($nature,' - ');
            $lieu='';
            if($intervention->getIdMagasin()!=null){
                $mag=$this->getDoctrine()->getRepository('AdministrationSocieteBundle:AdmMagasin')->findOneBy(array('magId'=>$intervention->getIdMagasin()));
                $lieu=$mag->getMagId().' '.$mag->getMagLibelle();
            }
            elseif ($intervention->getIdBatiment()!=null){
                $bat= $this->getDoctrine()->getRepository('InterventionBundle:TrvBatiments')->findOneBy(array('id'=>$intervention->getIdBatiment()));
                $lieu=$bat->getNom();

            }
            $agents_interv=$this->getDoctrine()->getRepository('InterventionBundle:TrvAgents')->findBy(array('idIntervention'=>$intervention->getId()));
            foreach ($agents_interv as $agent_interv){
                $agentMaintenance.=  $agent_interv->getIdUser()->getDisplayName().' - ';
            }
            $agentMaintenance=rtrim($agentMaintenance,' - ');
            $interv[$i]=array('id'=>$intervention->getId(),'assistante'=>($intervention->getAssistante()!=null) ? $interv[$i]["assistante"]=$intervention->getAssistante()->getDisplayName() : '',
                'statut'=>$intervention->getIdStatus()->getLibelle(),'date'=> $intervention->getDateDemande()->format('Y-m-d'),
                'lieu'=> $lieu, 'nature'=> $nature, 'raison'=> ($intervention->getIdRaison()!=null)? $interv[$i]["raison"]= $intervention->getIdRaison()->getLibelle(): '','agent'=>$agentMaintenance,
                'date_intervention'=>$intervention->getDateDebut());
            $i++;
        }
        $result = array();
        $em = $this->getDoctrine()->getManager();
        $statuts=$em->getRepository('InterventionBundle:TrvStatus')->findAll();
        $mags = $em->getRepository('AdministrationSocieteBundle:AdmMagasin')->findAll();
        $batis = $em->getRepository('InterventionBundle:TrvBatiments')->findAll();
        $i = 0;
        foreach ($mags as $mag) {
            $lieux[$i]['id'] = $mag->getMagId();
            $lieux[$i]['name'] = $mag->getMagLibelle();
            $i++;
        }
        foreach ($batis as $bat) {
            $lieux[$i]['id'] = $bat->getId();
            $lieux[$i]['name'] = $bat->getNom();
            $i++;
        }
        $raisons=$em->getRepository('InterventionBundle:TrvRaison')->findAll();
        $natures= $em->getRepository('InterventionBundle:TrvNature')->findAll();
        return $this->render('InterventionBundle:Default:liste_interventions.html.twig',
            array('form' => $form->createView(), 'interventions'=>$interv, 'agents'=>$agents, 'lieux'=>$lieux,
                'raisons'=>$raisons, 'natures'=>$natures, 'statuts'=>$statuts));
    }
    /**
     * @Route("/fetch_all", name="fetch_all_interventions")
     */

    public function allInterventionsAction(){
        $form = $this->createForm(new RechercheType());
        $agents = $this->getDoctrine()
            ->getRepository('UserUserBundle:User')
            ->findByRoles('ROLE_INTERVENTION_TRAVAUX_AGENT');
        $interventions= $this->getDoctrine()->getRepository("InterventionBundle:TrvIntervention")-> findAll();
        $nature='';
        $agentMaintenance='';
        $interv=array();
        $i=0;
        foreach ($interventions as $intervention){
            $nature='';
            $agentMaintenance='';
            $couts=$this->getDoctrine()->getRepository('InterventionBundle:TrvCout')->findBy(array('idIntervention'=>$intervention->getId()));
            foreach ($couts as $cout){
                $nature.= $cout->getIdType()->getIdNature()->getLibelle().' - '.$cout->getIdType()->getLibelle().' - ';
            }
            $nature=rtrim($nature,' - ');
            $lieu='';
            if($intervention->getIdMagasin()!=null){
                $mag=$this->getDoctrine()->getRepository('AdministrationSocieteBundle:AdmMagasin')->findOneBy(array('magId'=>$intervention->getIdMagasin()));
                $lieu=$mag->getMagId().' '.$mag->getMagLibelle();
            }
            elseif ($intervention->getIdBatiment()!=null){
                $bat= $this->getDoctrine()->getRepository('InterventionBundle:TrvBatiments')->findOneBy(array('id'=>$intervention->getIdBatiment()));
                $lieu=$bat->getNom();

            }
            $agents_interv=$this->getDoctrine()->getRepository('InterventionBundle:TrvAgents')->findBy(array('idIntervention'=>$intervention->getId()));
            foreach ($agents_interv as $agent_interv){
                $agentMaintenance.=  $agent_interv->getIdUser()->getDisplayName().' - ';
            }
            $agentMaintenance=rtrim($agentMaintenance,' - ');
            $interv[$i]=array('id'=>$intervention->getId(),'assistante'=>($intervention->getAssistante()!=null) ? $interv[$i]["assistante"]=$intervention->getAssistante()->getDisplayName() : '',
                'statut'=>$intervention->getIdStatus()->getLibelle(),'date'=> $intervention->getDateDemande()->format('Y-m-d'),
                'lieu'=> $lieu, 'nature'=> $nature, 'raison'=> ($intervention->getIdRaison()!=null)? $interv[$i]["raison"]= $intervention->getIdRaison()->getLibelle(): '','agent'=>$agentMaintenance,
                'date_intervention'=>$intervention->getDateDebut());
            $i++;
        }
        $result = array();
        $em = $this->getDoctrine()->getManager();
        $mags = $em->getRepository('AdministrationSocieteBundle:AdmMagasin')->findAll();
        $batis = $em->getRepository('InterventionBundle:TrvBatiments')->findAll();
        $i = 0;
        foreach ($mags as $mag) {
            $lieux[$i]['id'] = $mag->getMagId();
            $lieux[$i]['name'] = $mag->getMagLibelle();
            $i++;
        }
        foreach ($batis as $bat) {
            $lieux[$i]['id'] = $bat->getId();
            $lieux[$i]['name'] = $bat->getNom();
            $i++;
        }
        $raisons=$em->getRepository('InterventionBundle:TrvRaison')->findAll();
        $natures= $em->getRepository('InterventionBundle:TrvNature')->findAll();
        $statuts=$em->getRepository('InterventionBundle:TrvStatus')->findAll();
        return $this->render('InterventionBundle:Default:liste_interventions.html.twig',
            array('form' => $form->createView(), 'interventions'=>$interv, 'agents'=>$agents, 'lieux'=>$lieux,
                'raisons'=>$raisons, 'natures'=>$natures,'statuts'=>$statuts));
    }
    /**
     * @Route("/filtrer", name="filtrer")
     */

    public function filterAction(){
        $agents = $this->getDoctrine()
            ->getRepository('UserUserBundle:User')
            ->findByRoles('ROLE_INTERVENTION_TRAVAUX_AGENT');
        $em = $this->getDoctrine()->getManager();
        if ($this->getRequest()->isMethod('POST')) {
            $session = $this->getRequest()->getSession();
            $session->set('assistante',$_POST["interventionbundle_recherche"]["assistante"]);
            $session->set('lieu',$_POST["interventionbundle_recherche"]["lieu"]);
            $session->set('date',$_POST["interventionbundle_recherche"]["date"]);
            $session->set('nature',$_POST["interventionbundle_recherche"]["nature"]);
            $session->set('raison',$_POST["interventionbundle_recherche"]["raison"]);
            $session->set('date_interv',$_POST["interventionbundle_recherche"]["dateIntervention"]);
            if(isset($_POST["interventionbundle_recherche"]["statut"])==false){
                $stat=array(1,2,3,4,5);
                $t=array();
                $session->set('statut',$t);

            }
            else{
                $stat=$_POST["interventionbundle_recherche"]["statut"];
                $session->set('statut',$_POST["interventionbundle_recherche"]["statut"]);
            }
            if(isset($_POST["interventionbundle_recherche"]["agent"])==false){
                $ag=array();
                foreach ($agents as $agent){
                    array_push($ag, $agent->getId());
                }
                $t=array();
                $session->set('agent',$t);
            }
            else{
                $ag=$_POST["interventionbundle_recherche"]["agent"];
                $session->set('agent',$_POST["interventionbundle_recherche"]["agent"]);
            }

            if($_POST["interventionbundle_recherche"]["date"]!=null){
                $date1=substr($_POST["interventionbundle_recherche"]["date"],0,10);
                $date2=substr($_POST["interventionbundle_recherche"]["date"],13,10);
                $date1=new \DateTime($date1);
                $date2=new \DateTime($date2);
            }
            else{
                $dates = $em ->createQuery("SELECT MIN (inter.dateDemande), MAX(inter.dateDemande) FROM InterventionBundle:TrvIntervention inter")
                    ->getScalarResult();
                $date1=new\DateTime($dates[0][1]);
                $date2=new\DateTime($dates[0][2]);
            }
            if($_POST["interventionbundle_recherche"]["dateIntervention"]!=null){
                $date_deb1=substr($_POST["interventionbundle_recherche"]["dateIntervention"],0,10);
                $date_deb2=substr($_POST["interventionbundle_recherche"]["dateIntervention"],13,10);
                $date_deb1=new \DateTime($date_deb1);
                $date_deb2=new \DateTime($date_deb2);
            }
            else{
                $dates_dmd = $em ->createQuery("SELECT MIN (inter.dateDebut), MAX(inter.dateDebut) FROM InterventionBundle:TrvIntervention inter")
                    ->getScalarResult();
                $date_deb1=new\DateTime($dates_dmd[0][1]);
                $date_deb2=new\DateTime($dates_dmd[0][2]);
            }
            $qb = $this->getDoctrine()->getManager()->createQueryBuilder();

            if ($_POST["interventionbundle_recherche"]["nature"]!=null) {
                if ($_POST["interventionbundle_recherche"]["raison"]!=null) {
                    if (isset($_POST["interventionbundle_recherche"]["agent"])==false) {
                        if ($_POST["interventionbundle_recherche"]["lieu"]!=null) {
                            $bat= $this->getDoctrine()->getRepository('InterventionBundle:TrvBatiments')->findOneBy(array('id'=>$_POST["interventionbundle_recherche"]["lieu"]));
                            $mag=$this->getDoctrine()->getRepository('AdministrationSocieteBundle:AdmMagasin')->findOneBy(array('magId'=>$_POST["interventionbundle_recherche"]["lieu"]));
                            if ($bat != null) {
                                $qb->add('select', 'interv')
                                    ->from('InterventionBundle\Entity\TrvIntervention', 'interv')
                                    ->leftjoin('InterventionBundle\Entity\TrvCout', 'c', 'with', 'interv.id = c.idIntervention')
                                    ->join('interv.idStatus', 'stm')
                                    ->join('interv.assistante', 'assist')
                                    ->join('interv.idBatiment', 'batiment')
                                    ->join('interv.idRaison', 'raison')
                                    ->join('c.idType', 'type')
                                    ->join('type.idNature', 'nature')
                                    ->where('interv.idStatus = stm.id')
                                    ->andWhere('stm.id in (:st)')
                                    ->andWhere("assist.displayname LIKE :usr")
                                    ->andWhere("interv.dateDemande BETWEEN :date1 AND :date2")
                                    ->andWhere("batiment.id = :bat")
                                    ->andWhere("raison.id = :r")
                                    ->andWhere("interv.dateDebut BETWEEN :date_deb1 AND :date_deb2")
                                    ->andWhere("nature.id = :nat")
                                    ->orderBy('interv.id', 'DESC')
                                    ->setParameter('st', $stat)
                                    ->setParameter('usr', '%'.$_POST["interventionbundle_recherche"]["assistante"].'%')
                                    ->setParameter('date1', $date1->format('Y-m-d'))
                                    ->setParameter('date2', $date2->format('Y-m-d'))
                                    ->setParameter('r', $_POST["interventionbundle_recherche"]["raison"])
                                    ->setParameter('bat', $_POST["interventionbundle_recherche"]["lieu"])
                                    ->setParameter('nat', $_POST["interventionbundle_recherche"]["nature"])
                                    ->setParameter('date_deb1', $date_deb1->format('Y-m-d'))
                                    ->setParameter('date_deb2', $date_deb2->format('Y-m-d'));

                            } elseif ($mag != null) {
                                $qb->add('select', 'interv')
                                    ->from('InterventionBundle\Entity\TrvIntervention', 'interv')
                                    ->leftjoin('InterventionBundle\Entity\TrvCout', 'c', 'with', 'interv.id = c.idIntervention')
                                    ->join('interv.idStatus', 'stm')
                                    ->join('interv.assistante', 'assist')
                                    ->join('interv.idMagasin', 'mag')
                                    ->join('interv.idRaison', 'raison')
                                    ->join('c.idType', 'type')
                                    ->join('type.idNature', 'nature')
                                    ->where('interv.idStatus = stm.id')
                                    ->andWhere('stm.id in (:st)')
                                    ->andWhere("assist.displayname LIKE :usr")
                                    ->andWhere("interv.dateDemande BETWEEN :date1 AND :date2")
                                    ->andWhere("mag.magId = :mag")
                                    ->andWhere("raison.id = :r")
                                    ->andWhere("interv.dateDebut BETWEEN :date_deb1 AND :date_deb2")
                                    ->andWhere("nature.id = :nat")
                                    ->orderBy('interv.id', 'DESC')
                                    ->setParameter('st', $stat)
                                    ->setParameter('usr', '%'.$_POST["interventionbundle_recherche"]["assistante"].'%')
                                    ->setParameter('date1', $date1->format('Y-m-d'))
                                    ->setParameter('date2', $date2->format('Y-m-d'))
                                    ->setParameter('r', $_POST["interventionbundle_recherche"]["raison"])
                                    ->setParameter('mag', $_POST["interventionbundle_recherche"]["lieu"])
                                    ->setParameter('nat', $_POST["interventionbundle_recherche"]["nature"])
                                    ->setParameter('date_deb1', $date_deb1->format('Y-m-d'))
                                    ->setParameter('date_deb2', $date_deb2->format('Y-m-d'));
                            }
                        } else {
                            $qb->add('select', 'interv')
                                ->from('InterventionBundle\Entity\TrvIntervention', 'interv')
                                ->leftjoin('InterventionBundle\Entity\TrvCout', 'c', 'with', 'interv.id = c.idIntervention')
                                ->join('interv.idStatus', 'stm')
                                ->join('interv.assistante', 'assist')
                                ->join('interv.idRaison', 'raison')
                                ->join('c.idType', 'type')
                                ->join('type.idNature', 'nature')
                                ->where('interv.idStatus = stm.id')
                                ->andWhere('stm.id in (:st)')
                                ->andWhere("assist.displayname LIKE :usr")
                                ->andWhere("interv.dateDemande BETWEEN :date1 AND :date2")
                                ->andWhere("raison.id = :r")
                                ->andWhere("interv.dateDebut BETWEEN :date_deb1 AND :date_deb2")
                                ->andWhere("nature.id = :nat")
                                ->orderBy('interv.id', 'DESC')
                                ->setParameter('st', $stat)
                                ->setParameter('usr', '%'.$_POST["interventionbundle_recherche"]["assistante"].'%')
                                ->setParameter('date1', $date1->format('Y-m-d'))
                                ->setParameter('date2', $date2->format('Y-m-d'))
                                ->setParameter('r', $_POST["interventionbundle_recherche"]["raison"])
                                ->setParameter('nat', $_POST["interventionbundle_recherche"]["nature"])
                                ->setParameter('date_deb1', $date_deb1->format('Y-m-d'))
                                ->setParameter('date_deb2', $date_deb2->format('Y-m-d'));
                        }
                    } else {
                        if ($_POST["interventionbundle_recherche"]["lieu"]!=null) {
                            $bat= $this->getDoctrine()->getRepository('InterventionBundle:TrvBatiments')->findOneBy(array('id'=>$_POST["interventionbundle_recherche"]["lieu"]));
                            $mag=$this->getDoctrine()->getRepository('AdministrationSocieteBundle:AdmMagasin')->findOneBy(array('magId'=>$_POST["interventionbundle_recherche"]["lieu"]));
                            if ($bat != null) {
                                $qb->add('select', 'interv')
                                    ->from('InterventionBundle\Entity\TrvIntervention', 'interv')
                                    ->leftjoin('InterventionBundle\Entity\TrvCout', 'c', 'with', 'interv.id = c.idIntervention')
                                    ->leftjoin('InterventionBundle\Entity\TrvAgents', 'a', 'with', 'interv.id = a.idIntervention')
                                    ->join('interv.idStatus', 'stm')
                                    ->join('interv.assistante', 'assist')
                                    ->join('a.idUser', 'u')
                                    ->join('interv.idBatiment', 'batiment')
                                    ->join('interv.idRaison', 'raison')
                                    ->join('c.idType', 'type')
                                    ->join('type.idNature', 'nature')
                                    ->where('interv.idStatus = stm.id')
                                    ->andWhere('stm.id in (:st)')
                                    ->andWhere("assist.displayname LIKE :usr")
                                    ->andWhere("u.id in (:ag)")
                                    ->andWhere("interv.dateDemande BETWEEN :date1 AND :date2")
                                    ->andWhere("batiment.id = :bat")
                                    ->andWhere("raison.id = :r")
                                    ->andWhere("interv.dateDebut BETWEEN :date_deb1 AND :date_deb2")
                                    ->andWhere("nature.id = :nat")
                                    ->orderBy('interv.id', 'DESC')
                                    ->setParameter('st', $stat)
                                    ->setParameter('usr', '%'.$_POST["interventionbundle_recherche"]["assistante"].'%')
                                    ->setParameter('ag',$ag)
                                    ->setParameter('date1', $date1->format('Y-m-d'))
                                    ->setParameter('date2', $date2->format('Y-m-d'))
                                    ->setParameter('r', $_POST["interventionbundle_recherche"]["raison"])
                                    ->setParameter('bat', $_POST["interventionbundle_recherche"]["lieu"])
                                    ->setParameter('nat', $_POST["interventionbundle_recherche"]["nature"])
                                    ->setParameter('date_deb1', $date_deb1->format('Y-m-d'))
                                    ->setParameter('date_deb2', $date_deb2->format('Y-m-d'));
                            } elseif ($mag != null) {
                                $qb->add('select', 'interv')
                                    ->from('InterventionBundle\Entity\TrvIntervention', 'interv')
                                    ->leftjoin('InterventionBundle\Entity\TrvCout', 'c', 'with', 'interv.id = c.idIntervention')
                                    ->leftjoin('InterventionBundle\Entity\TrvAgents', 'a', 'with', 'interv.id = a.idIntervention')
                                    ->join('interv.idStatus', 'stm')
                                    ->join('interv.assistante', 'assist')
                                    ->join('a.idUser', 'u')
                                    ->join('interv.idMagasin', 'mag')
                                    ->join('interv.idRaison', 'raison')
                                    ->join('c.idType', 'type')
                                    ->join('type.idNature', 'nature')
                                    ->where('interv.idStatus = stm.id')
                                    ->andWhere('stm.id in (:st)')
                                    ->andWhere("assist.displayname LIKE :usr")
                                    ->andWhere("u.id in (:ag)")
                                    ->andWhere("interv.dateDemande BETWEEN :date1 AND :date2")
                                    ->andWhere("mag.magId = :mag")
                                    ->andWhere("raison.id = :r")
                                    ->andWhere("interv.dateDebut BETWEEN :date_deb1 AND :date_deb2")
                                    ->andWhere("nature.id = :nat")
                                    ->orderBy('interv.id', 'DESC')
                                    ->setParameter('st', $stat)
                                    ->setParameter('usr', '%'.$_POST["interventionbundle_recherche"]["assistante"].'%')
                                    ->setParameter('ag',$ag)
                                    ->setParameter('date1', $date1->format('Y-m-d'))
                                    ->setParameter('date2', $date2->format('Y-m-d'))
                                    ->setParameter('r', $_POST["interventionbundle_recherche"]["raison"])
                                    ->setParameter('mag', $_POST["interventionbundle_recherche"]["lieu"])
                                    ->setParameter('nat', $_POST["interventionbundle_recherche"]["nature"])
                                    ->setParameter('date_deb1', $date_deb1->format('Y-m-d'))
                                    ->setParameter('date_deb2', $date_deb2->format('Y-m-d'));
                            }
                        } else {
                            $qb->add('select', 'interv')
                                ->from('InterventionBundle\Entity\TrvIntervention', 'interv')
                                ->leftjoin('InterventionBundle\Entity\TrvCout', 'c', 'with', 'interv.id = c.idIntervention')
                                ->leftjoin('InterventionBundle\Entity\TrvAgents', 'a', 'with', 'interv.id = a.idIntervention')
                                ->join('interv.idStatus', 'stm')
                                ->join('interv.assistante', 'assist')
                                ->join('a.idUser', 'u')
                                ->join('interv.idRaison', 'raison')
                                ->join('c.idType', 'type')
                                ->join('type.idNature', 'nature')
                                ->where('interv.idStatus = stm.id')
                                ->andWhere('stm.id in (:st)')
                                ->andWhere("assist.displayname LIKE :usr")
                                ->andWhere("u.id in (:ag)")
                                ->andWhere("interv.dateDemande BETWEEN :date1 AND :date2")
                                ->andWhere("raison.id = :r")
                                ->andWhere("interv.dateDebut BETWEEN :date_deb1 AND :date_deb2")
                                ->andWhere("nature.id = :nat")
                                ->orderBy('interv.id', 'DESC')
                                ->setParameter('st', $stat)
                                ->setParameter('usr', '%'.$_POST["interventionbundle_recherche"]["assistante"].'%')
                                ->setParameter('ag',$ag)
                                ->setParameter('date1', $date1->format('Y-m-d'))
                                ->setParameter('date2', $date2->format('Y-m-d'))
                                ->setParameter('r', $_POST["interventionbundle_recherche"]["raison"])
                                ->setParameter('nat', $_POST["interventionbundle_recherche"]["nature"])
                                ->setParameter('date_deb1', $date_deb1->format('Y-m-d'))
                                ->setParameter('date_deb2', $date_deb2->format('Y-m-d'));
                        }
                    }
                } else {
                    if (isset($_POST["interventionbundle_recherche"]["agent"])==false) {
                        if ($_POST["interventionbundle_recherche"]["lieu"]!=null) {
                            $bat= $this->getDoctrine()->getRepository('InterventionBundle:TrvBatiments')->findOneBy(array('id'=>$_POST["interventionbundle_recherche"]["lieu"]));
                            $mag=$this->getDoctrine()->getRepository('AdministrationSocieteBundle:AdmMagasin')->findOneBy(array('magId'=>$_POST["interventionbundle_recherche"]["lieu"]));
                            if ($bat != null) {
                                $qb->add('select', 'interv')
                                    ->from('InterventionBundle\Entity\TrvIntervention', 'interv')
                                    ->leftjoin('InterventionBundle\Entity\TrvCout', 'c', 'with', 'interv.id = c.idIntervention')
                                    ->join('interv.idStatus', 'stm')
                                    ->join('interv.assistante', 'assist')
                                    ->join('interv.idBatiment', 'batiment')
                                    ->join('c.idType', 'type')
                                    ->join('type.idNature', 'nature')
                                    ->where('interv.idStatus = stm.id')
                                    ->andWhere('stm.id in (:st)')
                                    ->andWhere("assist.displayname LIKE :usr")
                                    ->andWhere("interv.dateDemande BETWEEN :date1 AND :date2")
                                    ->andWhere("batiment.id = :bat")
                                    ->andWhere("interv.dateDebut BETWEEN :date_deb1 AND :date_deb2")
                                    ->andWhere("nature.id = :nat")
                                    ->orderBy('interv.id', 'DESC')
                                    ->setParameter('st', $stat)
                                    ->setParameter('usr', '%'.$_POST["interventionbundle_recherche"]["assistante"].'%')
                                    ->setParameter('date1', $date1->format('Y-m-d'))
                                    ->setParameter('date2', $date2->format('Y-m-d'))
                                    ->setParameter('bat', $_POST["interventionbundle_recherche"]["lieu"])
                                    ->setParameter('nat', $_POST["interventionbundle_recherche"]["nature"])
                                    ->setParameter('date_deb1', $date_deb1->format('Y-m-d'))
                                    ->setParameter('date_deb2', $date_deb2->format('Y-m-d'));
                            } elseif ($mag != null) {
                                $qb->add('select', 'interv')
                                    ->from('InterventionBundle\Entity\TrvIntervention', 'interv')
                                    ->leftjoin('InterventionBundle\Entity\TrvCout', 'c', 'with', 'interv.id = c.idIntervention')
                                    ->join('interv.idStatus', 'stm')
                                    ->join('interv.assistante', 'assist')
                                    ->join('interv.idMagasin', 'mag')
                                    ->join('c.idType', 'type')
                                    ->join('type.idNature', 'nature')
                                    ->where('interv.idStatus = stm.id')
                                    ->andWhere('stm.id in (:st)')
                                    ->andWhere("assist.displayname LIKE :usr")
                                    ->andWhere("interv.dateDemande BETWEEN :date1 AND :date2")
                                    ->andWhere("mag.magId = :mag")
                                    ->andWhere("interv.dateDebut BETWEEN :date_deb1 AND :date_deb2")
                                    ->andWhere("nature.id = :nat")
                                    ->orderBy('interv.id', 'DESC')
                                    ->setParameter('st', $stat)
                                    ->setParameter('usr', '%'.$_POST["interventionbundle_recherche"]["assistante"].'%')
                                    ->setParameter('date1', $date1->format('Y-m-d'))
                                    ->setParameter('date2', $date2->format('Y-m-d'))
                                    ->setParameter('mag', $_POST["interventionbundle_recherche"]["lieu"])
                                    ->setParameter('nat', $_POST["interventionbundle_recherche"]["nature"])
                                    ->setParameter('date_deb1', $date_deb1->format('Y-m-d'))
                                    ->setParameter('date_deb2', $date_deb2->format('Y-m-d'));
                            }
                        } else {
                            $qb->add('select', 'interv')
                                ->from('InterventionBundle\Entity\TrvIntervention', 'interv')
                                ->leftjoin('InterventionBundle\Entity\TrvCout', 'c', 'with', 'interv.id = c.idIntervention')
                                ->join('interv.idStatus', 'stm')
                                ->join('interv.assistante', 'assist')
                                ->join('c.idType', 'type')
                                ->join('type.idNature', 'nature')
                                ->where('interv.idStatus = stm.id')
                                ->andWhere('stm.id in (:st)')
                                ->andWhere("assist.displayname LIKE :usr")
                                ->andWhere("interv.dateDemande BETWEEN :date1 AND :date2")
                                ->andWhere("interv.dateDebut BETWEEN :date_deb1 AND :date_deb2")
                                ->andWhere("nature.id = :nat")
                                ->orderBy('interv.id', 'DESC')
                                ->setParameter('st', $stat)
                                ->setParameter('usr', '%'.$_POST["interventionbundle_recherche"]["assistante"].'%')
                                ->setParameter('date1', $date1->format('Y-m-d'))
                                ->setParameter('date2', $date2->format('Y-m-d'))
                                ->setParameter('nat', $_POST["interventionbundle_recherche"]["nature"])
                                ->setParameter('date_deb1', $date_deb1->format('Y-m-d'))
                                ->setParameter('date_deb2', $date_deb2->format('Y-m-d'));
                        }
                    } else {
                        if ($_POST["interventionbundle_recherche"]["lieu"]!=null) {
                            $bat= $this->getDoctrine()->getRepository('InterventionBundle:TrvBatiments')->findOneBy(array('id'=>$_POST["interventionbundle_recherche"]["lieu"]));
                            $mag=$this->getDoctrine()->getRepository('AdministrationSocieteBundle:AdmMagasin')->findOneBy(array('magId'=>$_POST["interventionbundle_recherche"]["lieu"]));
                            if ($bat != null) {
                                $qb->add('select', 'interv')
                                    ->from('InterventionBundle\Entity\TrvIntervention', 'interv')
                                    ->leftjoin('InterventionBundle\Entity\TrvCout', 'c', 'with', 'interv.id = c.idIntervention')
                                    ->leftjoin('InterventionBundle\Entity\TrvAgents', 'a', 'with', 'interv.id = a.idIntervention')
                                    ->join('interv.idStatus', 'stm')
                                    ->join('interv.assistante', 'assist')
                                    ->join('a.idUser', 'u')
                                    ->join('interv.idBatiment', 'batiment')
                                    ->join('c.idType', 'type')
                                    ->join('type.idNature', 'nature')
                                    ->where('interv.idStatus = stm.id')
                                    ->andWhere('stm.id in (:st)')
                                    ->andWhere("assist.displayname LIKE :usr")
                                    ->andWhere("u.id in (:ag)")
                                    ->andWhere("interv.dateDemande BETWEEN :date1 AND :date2")
                                    ->andWhere("batiment.id = :bat")
                                    ->andWhere("interv.dateDebut BETWEEN :date_deb1 AND :date_deb2")
                                    ->andWhere("nature.id = :nat")
                                    ->orderBy('interv.id', 'DESC')
                                    ->setParameter('st', $stat)
                                    ->setParameter('usr', '%'.$_POST["interventionbundle_recherche"]["assistante"].'%')
                                    ->setParameter('ag',$ag)
                                    ->setParameter('date1', $date1->format('Y-m-d'))
                                    ->setParameter('date2', $date2->format('Y-m-d'))
                                    ->setParameter('bat', $_POST["interventionbundle_recherche"]["lieu"])
                                    ->setParameter('nat', $_POST["interventionbundle_recherche"]["nature"])
                                    ->setParameter('date_deb1', $date_deb1->format('Y-m-d'))
                                    ->setParameter('date_deb2', $date_deb2->format('Y-m-d'));
                            } elseif ($mag != null) {
                                $qb->add('select', 'interv')
                                    ->from('InterventionBundle\Entity\TrvIntervention', 'interv')
                                    ->leftjoin('InterventionBundle\Entity\TrvCout', 'c', 'with', 'interv.id = c.idIntervention')
                                    ->leftjoin('InterventionBundle\Entity\TrvAgents', 'a', 'with', 'interv.id = a.idIntervention')
                                    ->join('interv.idStatus', 'stm')
                                    ->join('interv.assistante', 'assist')
                                    ->join('a.idUser', 'u')
                                    ->join('interv.idMagasin', 'mag')
                                    ->join('c.idType', 'type')
                                    ->join('type.idNature', 'nature')
                                    ->where('interv.idStatus = stm.id')
                                    ->andWhere('stm.id in (:st)')
                                    ->andWhere("assist.displayname LIKE :usr")
                                    ->andWhere("u.id in (:ag)")
                                    ->andWhere("interv.dateDemande BETWEEN :date1 AND :date2")
                                    ->andWhere("mag.magId = :mag")
                                    ->andWhere("interv.dateDebut BETWEEN :date_deb1 AND :date_deb2")
                                    ->andWhere("nature.id = :nat")
                                    ->orderBy('interv.id', 'DESC')
                                    ->setParameter('st', $stat)
                                    ->setParameter('usr', '%'.$_POST["interventionbundle_recherche"]["assistante"].'%')
                                    ->setParameter('ag',$ag)
                                    ->setParameter('date1', $date1->format('Y-m-d'))
                                    ->setParameter('date2', $date2->format('Y-m-d'))
                                    ->setParameter('mag', $_POST["interventionbundle_recherche"]["lieu"])
                                    ->setParameter('nat', $_POST["interventionbundle_recherche"]["nature"])
                                    ->setParameter('date_deb1', $date_deb1->format('Y-m-d'))
                                    ->setParameter('date_deb2', $date_deb2->format('Y-m-d'));
                            }
                        } else {
                            $qb->add('select', 'interv')
                                ->from('InterventionBundle\Entity\TrvIntervention', 'interv')
                                ->leftjoin('InterventionBundle\Entity\TrvCout', 'c', 'with', 'interv.id = c.idIntervention')
                                ->leftjoin('InterventionBundle\Entity\TrvAgents', 'a', 'with', 'interv.id = a.idIntervention')
                                ->join('interv.idStatus', 'stm')
                                ->join('interv.assistante', 'assist')
                                ->join('a.idUser', 'u')
                                ->join('c.idType', 'type')
                                ->join('type.idNature', 'nature')
                                ->where('interv.idStatus = stm.id')
                                ->andWhere('stm.id in (:st)')
                                ->andWhere("assist.displayname LIKE :usr")
                                ->andWhere("u.id in (:ag)")
                                ->andWhere("interv.dateDemande BETWEEN :date1 AND :date2")
                                ->andWhere("interv.dateDebut BETWEEN :date_deb1 AND :date_deb2")
                                ->andWhere("nature.id = :nat")
                                ->orderBy('interv.id', 'DESC')
                                ->setParameter('st', $stat)
                                ->setParameter('usr', '%'.$_POST["interventionbundle_recherche"]["assistante"].'%')
                                ->setParameter('ag',$ag)
                                ->setParameter('date1', $date1->format('Y-m-d'))
                                ->setParameter('date2', $date2->format('Y-m-d'))
                                ->setParameter('nat', $_POST["interventionbundle_recherche"]["nature"])
                                ->setParameter('date_deb1', $date_deb1->format('Y-m-d'))
                                ->setParameter('date_deb2', $date_deb2->format('Y-m-d'));
                        }
                    }
                }
            }
            else{
                if ($_POST["interventionbundle_recherche"]["raison"]!=null) {
                    if (isset($_POST["interventionbundle_recherche"]["agent"])==false) {
                        if ($_POST["interventionbundle_recherche"]["lieu"]!=null) {
                            $bat= $this->getDoctrine()->getRepository('InterventionBundle:TrvBatiments')->findOneBy(array('id'=>$_POST["interventionbundle_recherche"]["lieu"]));
                            $mag=$this->getDoctrine()->getRepository('AdministrationSocieteBundle:AdmMagasin')->findOneBy(array('magId'=>$_POST["interventionbundle_recherche"]["lieu"]));
                            if ($bat != null) {
                                $qb->add('select', 'interv')
                                    ->from('InterventionBundle\Entity\TrvIntervention', 'interv')
                                    ->join('interv.idStatus', 'stm')
                                    ->join('interv.assistante', 'assist')
                                    ->join('interv.idBatiment', 'batiment')
                                    ->join('interv.idRaison', 'raison')
                                    ->where('interv.idStatus = stm.id')
                                    ->andWhere('stm.id in (:st)')
                                    ->andWhere("assist.displayname LIKE :usr")
                                    ->andWhere("interv.dateDemande BETWEEN :date1 AND :date2")
                                    ->andWhere("batiment.id = :bat")
                                    ->andWhere("raison.id = :r")
                                    ->andWhere("interv.dateDebut BETWEEN :date_deb1 AND :date_deb2")
                                    ->orderBy('interv.id', 'DESC')
                                    ->setParameter('st', $stat)
                                    ->setParameter('usr', '%'.$_POST["interventionbundle_recherche"]["assistante"].'%')
                                    ->setParameter('date1', $date1->format('Y-m-d'))
                                    ->setParameter('date2', $date2->format('Y-m-d'))
                                    ->setParameter('r', $_POST["interventionbundle_recherche"]["raison"])
                                    ->setParameter('bat', $_POST["interventionbundle_recherche"]["lieu"])
                                    ->setParameter('date_deb1', $date_deb1->format('Y-m-d'))
                                    ->setParameter('date_deb2', $date_deb2->format('Y-m-d'));
                            } elseif ($mag != null) {
                                $qb->add('select', 'interv')
                                    ->from('InterventionBundle\Entity\TrvIntervention', 'interv')
                                    ->join('interv.idStatus', 'stm')
                                    ->join('interv.assistante', 'assist')
                                    ->join('interv.idMagasin', 'mag')
                                    ->join('interv.idRaison', 'raison')
                                    ->where('interv.idStatus = stm.id')
                                    ->andWhere('stm.id in (:st)')
                                    ->andWhere("assist.displayname LIKE :usr")
                                    ->andWhere("interv.dateDemande BETWEEN :date1 AND :date2")
                                    ->andWhere("mag.magId = :mag")
                                    ->andWhere("raison.id = :r")
                                    ->andWhere("interv.dateDebut BETWEEN :date_deb1 AND :date_deb2")
                                    ->orderBy('interv.id', 'DESC')
                                    ->setParameter('st', $stat)
                                    ->setParameter('usr', '%'.$_POST["interventionbundle_recherche"]["assistante"].'%')
                                    ->setParameter('date1', $date1->format('Y-m-d'))
                                    ->setParameter('date2', $date2->format('Y-m-d'))
                                    ->setParameter('r', $_POST["interventionbundle_recherche"]["raison"])
                                    ->setParameter('mag', $_POST["interventionbundle_recherche"]["lieu"])
                                    ->setParameter('date_deb1', $date_deb1->format('Y-m-d'))
                                    ->setParameter('date_deb2', $date_deb2->format('Y-m-d'));
                            }
                        } else {
                            $qb->add('select', 'interv')
                                ->from('InterventionBundle\Entity\TrvIntervention', 'interv')
                                ->join('interv.idStatus', 'stm')
                                ->join('interv.assistante', 'assist')
                                ->join('interv.idRaison', 'raison')
                                ->where('interv.idStatus = stm.id')
                                ->andWhere('stm.id in (:st)')
                                ->andWhere("assist.displayname LIKE :usr")
                                ->andWhere("interv.dateDemande BETWEEN :date1 AND :date2")
                                ->andWhere("raison.id = :r")
                                ->andWhere("interv.dateDebut BETWEEN :date_deb1 AND :date_deb2")
                                ->orderBy('interv.id', 'DESC')
                                ->setParameter('st', $stat)
                                ->setParameter('usr', '%'.$_POST["interventionbundle_recherche"]["assistante"].'%')
                                ->setParameter('date1', $date1->format('Y-m-d'))
                                ->setParameter('date2', $date2->format('Y-m-d'))
                                ->setParameter('r', $_POST["interventionbundle_recherche"]["raison"])
                                ->setParameter('date_deb1', $date_deb1->format('Y-m-d'))
                                ->setParameter('date_deb2', $date_deb2->format('Y-m-d'));
                        }
                    } else {
                        if ($_POST["interventionbundle_recherche"]["lieu"]!=null) {
                            $bat= $this->getDoctrine()->getRepository('InterventionBundle:TrvBatiments')->findOneBy(array('id'=>$_POST["interventionbundle_recherche"]["lieu"]));
                            $mag=$this->getDoctrine()->getRepository('AdministrationSocieteBundle:AdmMagasin')->findOneBy(array('magId'=>$_POST["interventionbundle_recherche"]["lieu"]));
                            if ($bat != null) {
                                $qb->add('select', 'interv')
                                    ->from('InterventionBundle\Entity\TrvIntervention', 'interv')
                                    ->leftjoin('InterventionBundle\Entity\TrvAgents', 'a', 'with', 'interv.id = a.idIntervention')
                                    ->join('interv.idStatus', 'stm')
                                    ->join('interv.assistante', 'assist')
                                    ->join('a.idUser', 'u')
                                    ->join('interv.idBatiment', 'batiment')
                                    ->join('interv.idRaison', 'raison')
                                    ->where('interv.idStatus = stm.id')
                                    ->andWhere('stm.id in (:st)')
                                    ->andWhere("assist.displayname LIKE :usr")
                                    ->andWhere("u.id in (:ag)")
                                    ->andWhere("interv.dateDemande BETWEEN :date1 AND :date2")
                                    ->andWhere("batiment.id = :bat")
                                    ->andWhere("raison.id = :r")
                                    ->andWhere("interv.dateDebut BETWEEN :date_deb1 AND :date_deb2")
                                    ->orderBy('interv.id', 'DESC')
                                    ->setParameter('st', $stat)
                                    ->setParameter('usr', '%'.$_POST["interventionbundle_recherche"]["assistante"].'%')
                                    ->setParameter('ag',$ag)
                                    ->setParameter('date1', $date1->format('Y-m-d'))
                                    ->setParameter('date2', $date2->format('Y-m-d'))
                                    ->setParameter('r', $_POST["interventionbundle_recherche"]["raison"])
                                    ->setParameter('bat', $_POST["interventionbundle_recherche"]["lieu"])
                                    ->setParameter('date_deb1', $date_deb1->format('Y-m-d'))
                                    ->setParameter('date_deb2', $date_deb2->format('Y-m-d'));
                            } elseif ($mag != null) {
                                $qb->add('select', 'interv')
                                    ->from('InterventionBundle\Entity\TrvIntervention', 'interv')
                                    ->leftjoin('InterventionBundle\Entity\TrvAgents', 'a', 'with', 'interv.id = a.idIntervention')
                                    ->join('interv.idStatus', 'stm')
                                    ->join('interv.assistante', 'assist')
                                    ->join('a.idUser', 'u')
                                    ->join('interv.idMagasin', 'mag')
                                    ->join('interv.idRaison', 'raison')
                                    ->where('interv.idStatus = stm.id')
                                    ->andWhere('stm.id in (:st)')
                                    ->andWhere("assist.displayname LIKE :usr")
                                    ->andWhere("u.id in (:ag)")
                                    ->andWhere("interv.dateDemande BETWEEN :date1 AND :date2")
                                    ->andWhere("mag.magId = :mag")
                                    ->andWhere("raison.id = :r")
                                    ->andWhere("interv.dateDebut BETWEEN :date_deb1 AND :date_deb2")
                                    ->orderBy('interv.id', 'DESC')
                                    ->setParameter('st', $stat)
                                    ->setParameter('usr', '%'.$_POST["interventionbundle_recherche"]["assistante"].'%')
                                    ->setParameter('ag',$ag)
                                    ->setParameter('date1', $date1->format('Y-m-d'))
                                    ->setParameter('date2', $date2->format('Y-m-d'))
                                    ->setParameter('r', $_POST["interventionbundle_recherche"]["raison"])
                                    ->setParameter('mag', $_POST["interventionbundle_recherche"]["lieu"])
                                    ->setParameter('date_deb1', $date_deb1->format('Y-m-d'))
                                    ->setParameter('date_deb2', $date_deb2->format('Y-m-d'));
                            }
                        } else {
                            $qb->add('select', 'interv')
                                ->from('InterventionBundle\Entity\TrvIntervention', 'interv')
                                ->leftjoin('InterventionBundle\Entity\TrvAgents', 'a', 'with', 'interv.id = a.idIntervention')
                                ->join('interv.idStatus', 'stm')
                                ->join('interv.assistante', 'assist')
                                ->join('a.idUser', 'u')
                                ->join('interv.idRaison', 'raison')
                                ->where('interv.idStatus = stm.id')
                                ->andWhere('stm.id in (:st)')
                                ->andWhere("assist.displayname LIKE :usr")
                                ->andWhere("u.id in (:ag)")
                                ->andWhere("interv.dateDemande BETWEEN :date1 AND :date2")
                                ->andWhere("raison.id = :r")
                                ->andWhere("interv.dateDebut BETWEEN :date_deb1 AND :date_deb2")
                                ->orderBy('interv.id', 'DESC')
                                ->setParameter('st', $stat)
                                ->setParameter('usr', '%'.$_POST["interventionbundle_recherche"]["assistante"].'%')
                                ->setParameter('ag',$ag)
                                ->setParameter('date1', $date1->format('Y-m-d'))
                                ->setParameter('date2', $date2->format('Y-m-d'))
                                ->setParameter('r', $_POST["interventionbundle_recherche"]["raison"])
                                ->setParameter('date_deb1', $date_deb1->format('Y-m-d'))
                                ->setParameter('date_deb2', $date_deb2->format('Y-m-d'));
                        }
                    }
                } else {
                    if (isset($_POST["interventionbundle_recherche"]["agent"])==false) {
                        if ($_POST["interventionbundle_recherche"]["lieu"]!=null) {
                            $bat= $this->getDoctrine()->getRepository('InterventionBundle:TrvBatiments')->findOneBy(array('id'=>$_POST["interventionbundle_recherche"]["lieu"]));
                            $mag=$this->getDoctrine()->getRepository('AdministrationSocieteBundle:AdmMagasin')->findOneBy(array('magId'=>$_POST["interventionbundle_recherche"]["lieu"]));
                            if ($bat != null) {
                                $qb->add('select', 'interv')
                                    ->from('InterventionBundle\Entity\TrvIntervention', 'interv')
                                    ->join('interv.idStatus', 'stm')
                                    ->join('interv.assistante', 'assist')
                                    ->join('interv.idBatiment', 'batiment')
                                    ->where('interv.idStatus = stm.id')
                                    ->andWhere('stm.id in (:st)')
                                    ->andWhere("assist.displayname LIKE :usr")
                                    ->andWhere("interv.dateDemande BETWEEN :date1 AND :date2")
                                    ->andWhere("batiment.id = :bat")
                                    ->andWhere("interv.dateDebut BETWEEN :date_deb1 AND :date_deb2")
                                    ->orderBy('interv.id', 'DESC')
                                    ->setParameter('st', $stat)
                                    ->setParameter('usr', '%'.$_POST["interventionbundle_recherche"]["assistante"].'%')
                                    ->setParameter('date1', $date1->format('Y-m-d'))
                                    ->setParameter('date2', $date2->format('Y-m-d'))
                                    ->setParameter('bat', $_POST["interventionbundle_recherche"]["lieu"])
                                    ->setParameter('date_deb1', $date_deb1->format('Y-m-d'))
                                    ->setParameter('date_deb2', $date_deb2->format('Y-m-d'));
                            } elseif ($mag != null) {
                                $qb->add('select', 'interv')
                                    ->from('InterventionBundle\Entity\TrvIntervention', 'interv')
                                    ->join('interv.idStatus', 'stm')
                                    ->join('interv.assistante', 'assist')
                                    ->join('interv.idMagasin', 'mag')
                                    ->where('interv.idStatus = stm.id')
                                    ->andWhere('stm.id in (:st)')
                                    ->andWhere("assist.displayname LIKE :usr")
                                    ->andWhere("interv.dateDemande BETWEEN :date1 AND :date2")
                                    ->andWhere("mag.magId = :mag")
                                    ->andWhere("interv.dateDebut BETWEEN :date_deb1 AND :date_deb2")
                                    ->orderBy('interv.id', 'DESC')
                                    ->setParameter('st', $stat)
                                    ->setParameter('usr', '%'.$_POST["interventionbundle_recherche"]["assistante"].'%')
                                    ->setParameter('date1', $date1->format('Y-m-d'))
                                    ->setParameter('date2', $date2->format('Y-m-d'))
                                    ->setParameter('mag', $_POST["interventionbundle_recherche"]["lieu"])
                                    ->setParameter('date_deb1', $date_deb1->format('Y-m-d'))
                                    ->setParameter('date_deb2', $date_deb2->format('Y-m-d'));
                            }
                        } else {
                            $qb->add('select', 'interv')
                                ->from('InterventionBundle\Entity\TrvIntervention', 'interv')
                                ->join('interv.idStatus', 'stm')
                                ->join('interv.assistante', 'assist')
                                ->where('interv.idStatus = stm.id')
                                ->andWhere('stm.id in (:st)')
                                ->andWhere("assist.displayname LIKE :usr")
                                ->andWhere("interv.dateDemande BETWEEN :date1 AND :date2")
                                ->andWhere("interv.dateDebut BETWEEN :date_deb1 AND :date_deb2")
                                ->orderBy('interv.id', 'DESC')
                                ->setParameter('st', $stat)
                                ->setParameter('usr', '%'.$_POST["interventionbundle_recherche"]["assistante"].'%')
                                ->setParameter('date1', $date1->format('Y-m-d'))
                                ->setParameter('date2', $date2->format('Y-m-d'))
                                ->setParameter('date_deb1', $date_deb1->format('Y-m-d'))
                                ->setParameter('date_deb2', $date_deb2->format('Y-m-d'));

                        }
                    } else {
                        if ($_POST["interventionbundle_recherche"]["lieu"]!=null) {
                            $bat= $this->getDoctrine()->getRepository('InterventionBundle:TrvBatiments')->findOneBy(array('id'=>$_POST["interventionbundle_recherche"]["lieu"]));
                            $mag=$this->getDoctrine()->getRepository('AdministrationSocieteBundle:AdmMagasin')->findOneBy(array('magId'=>$_POST["interventionbundle_recherche"]["lieu"]));
                            if ($bat != null) {
                                $qb->add('select', 'interv')
                                    ->from('InterventionBundle\Entity\TrvIntervention', 'interv')
                                    ->leftjoin('InterventionBundle\Entity\TrvAgents', 'a', 'with', 'interv.id = a.idIntervention')
                                    ->join('interv.idStatus', 'stm')
                                    ->join('interv.assistante', 'assist')
                                    ->join('a.idUser', 'u')
                                    ->join('interv.idBatiment', 'batiment')
                                    ->where('interv.idStatus = stm.id')
                                    ->andWhere('stm.id in (:st)')
                                    ->andWhere("assist.displayname LIKE :usr")
                                    ->andWhere("u.id in (:ag)")
                                    ->andWhere("interv.dateDemande BETWEEN :date1 AND :date2")
                                    ->andWhere("batiment.id = :bat")
                                    ->andWhere("interv.dateDebut BETWEEN :date_deb1 AND :date_deb2")
                                    ->orderBy('interv.id', 'DESC')
                                    ->setParameter('st', $stat)
                                    ->setParameter('usr', '%'.$_POST["interventionbundle_recherche"]["assistante"].'%')
                                    ->setParameter('ag',$ag)
                                    ->setParameter('date1', $date1->format('Y-m-d'))
                                    ->setParameter('date2', $date2->format('Y-m-d'))
                                    ->setParameter('bat', $_POST["interventionbundle_recherche"]["lieu"])
                                    ->setParameter('date_deb1', $date_deb1->format('Y-m-d'))
                                    ->setParameter('date_deb2', $date_deb2->format('Y-m-d'));

                            } elseif ($mag != null) {
                                $qb->add('select', 'interv')
                                    ->from('InterventionBundle\Entity\TrvIntervention', 'interv')
                                    ->leftjoin('InterventionBundle\Entity\TrvAgents', 'a', 'with', 'interv.id = a.idIntervention')
                                    ->join('interv.idStatus', 'stm')
                                    ->join('interv.assistante', 'assist')
                                    ->join('a.idUser', 'u')
                                    ->join('interv.idMagasin', 'mag')
                                    ->where('interv.idStatus = stm.id')
                                    ->andWhere('stm.id in (:st)')
                                    ->andWhere("assist.displayname LIKE :usr")
                                    ->andWhere("u.id in (:ag)")
                                    ->andWhere("interv.dateDemande BETWEEN :date1 AND :date2")
                                    ->andWhere("mag.magId = :mag")
                                    ->andWhere("interv.dateDebut BETWEEN :date_deb1 AND :date_deb2")
                                    ->orderBy('interv.id', 'DESC')
                                    ->setParameter('st', $stat)
                                    ->setParameter('usr', '%'.$_POST["interventionbundle_recherche"]["assistante"].'%')
                                    ->setParameter('ag',$ag)
                                    ->setParameter('date1', $date1->format('Y-m-d'))
                                    ->setParameter('date2', $date2->format('Y-m-d'))
                                    ->setParameter('mag', $_POST["interventionbundle_recherche"]["lieu"])
                                    ->setParameter('date_deb1', $date_deb1->format('Y-m-d'))
                                    ->setParameter('date_deb2', $date_deb2->format('Y-m-d'));

                            }
                        } else {
                            $qb->add('select', 'interv')
                                ->from('InterventionBundle\Entity\TrvIntervention', 'interv')
                                ->leftjoin('InterventionBundle\Entity\TrvAgents', 'a', 'with', 'interv.id = a.idIntervention')
                                ->join('interv.idStatus', 'stm')
                                ->join('interv.assistante', 'assist')
                                ->join('a.idUser', 'u')
                                ->where('interv.idStatus = stm.id')
                                ->andWhere('stm.id in (:st)')
                                ->andWhere("assist.displayname LIKE :usr")
                                ->andWhere("u.id in (:ag)")
                                ->andWhere("interv.dateDemande BETWEEN :date1 AND :date2")
                                ->andWhere("interv.dateDebut BETWEEN :date_deb1 AND :date_deb2")
                                ->orderBy('interv.id', 'DESC')
                                ->setParameter('st', $stat)
                                ->setParameter('usr', '%'.$_POST["interventionbundle_recherche"]["assistante"].'%')
                                ->setParameter('ag',$ag)
                                ->setParameter('date1', $date1->format('Y-m-d'))
                                ->setParameter('date2', $date2->format('Y-m-d'))
                                ->setParameter('date_deb1', $date_deb1->format('Y-m-d'))
                                ->setParameter('date_deb2', $date_deb2->format('Y-m-d'));
                        }
                    }
                }
            }

            $interventions =$qb->getQuery()
                ->setFetchMode("InterventionBundle\Entity\TrvIntervention", "elements", "EAGER")
                ->getResult();
            $nature='';
            $agentMaintenance='';
            $interv=array();
            $i=0;

            foreach ($interventions as $intervention){
                $nature='';
                $agentMaintenance='';
                $couts=$this->getDoctrine()->getRepository('InterventionBundle:TrvCout')->findBy(array('idIntervention'=>$intervention->getId()));
                foreach ($couts as $cout){
                    $nature.= $cout->getIdType()->getIdNature()->getLibelle().' - '.$cout->getIdType()->getLibelle().' - ';
                }
                $nature=rtrim($nature,' - ');
                $lieu='';
                if($intervention->getIdMagasin()!=null){
                    $mag=$this->getDoctrine()->getRepository('AdministrationSocieteBundle:AdmMagasin')->findOneBy(array('magId'=>$intervention->getIdMagasin()));
                    $lieu=$mag->getMagId().' '.$mag->getMagLibelle();
                }
                elseif ($intervention->getIdBatiment()!=null){
                    $bat= $this->getDoctrine()->getRepository('InterventionBundle:TrvBatiments')->findOneBy(array('id'=>$intervention->getIdBatiment()));
                    $lieu=$bat->getNom();

                }
                $agents_interv=$this->getDoctrine()->getRepository('InterventionBundle:TrvAgents')->findBy(array('idIntervention'=>$intervention->getId()));
                foreach ($agents_interv as $agent_interv){
                    $agentMaintenance.=  $agent_interv->getIdUser()->getDisplayName().' - ';
                }
                $agentMaintenance=rtrim($agentMaintenance,' - ');
                $interv[$i]=array('id'=>$intervention->getId(),'assistante'=>($intervention->getAssistante()!=null) ? $interv[$i]["assistante"]=$intervention->getAssistante()->getDisplayName() : '',
                    'statut'=>$intervention->getIdStatus()->getLibelle(),'date'=> $intervention->getDateDemande()->format('Y-m-d'),
                    'lieu'=> $lieu, 'nature'=> $nature, 'raison'=> ($intervention->getIdRaison()!=null)? $interv[$i]["raison"]= $intervention->getIdRaison()->getLibelle(): '','agent'=>$agentMaintenance,
                    'date_intervention'=>$intervention->getDateDebut());
                $i++;
            }
            $lieux = array();
            $mags = $em->getRepository('AdministrationSocieteBundle:AdmMagasin')->findAll();
            $batis = $em->getRepository('InterventionBundle:TrvBatiments')->findAll();
            $i = 0;
            foreach ($mags as $mag) {
                $lieux[$i]['id'] = $mag->getMagId();
                $lieux[$i]['name'] = $mag->getMagLibelle();
                $i++;
            }
            foreach ($batis as $bat) {
                $lieux[$i]['id'] = $bat->getId();
                $lieux[$i]['name'] = $bat->getNom();
                $i++;
            }
            $raisons=$em->getRepository('InterventionBundle:TrvRaison')->findAll();
            $natures= $em->getRepository('InterventionBundle:TrvNature')->findAll();
            $statuts=$em->getRepository('InterventionBundle:TrvStatus')->findAll();
            $form = $this->createForm(new RechercheType());
            return $this->render('InterventionBundle:Default:liste_interventions.html.twig',
                array('form' => $form->createView(), 'interventions'=>$interv, 'agents'=>$agents,'lieux'=>$lieux,
                    'raisons'=>$raisons, 'natures'=>$natures, 'statuts'=>$statuts));

        }
        else{
            return $this->redirect($this->generateUrl('tab_interventions'));
        }

    }

}
