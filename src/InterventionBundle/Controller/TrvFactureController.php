<?php

namespace InterventionBundle\Controller;

use InterventionBundle\Entity\TrvFacture;
use InterventionBundle\Entity\TrvFournisseur;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use InterventionBundle\Form\TrvFactureType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use InterventionBundle\Entity\TrvPieceJointe;
use InterventionBundle\Form\TrvTourneeType;
/**
 * TrvIntervention controller.
 *
 * @Route("/facture")
 */
class TrvFactureController extends Controller
{
    /**
     * @Route("/materiel/{id}", name="materiel")
     */
    public function materielAction($id){
        $form = $this->createForm(new TrvFactureType());
        $interv= $this->getDoctrine()->getRepository("InterventionBundle:TrvIntervention")-> findOneBy(array('id'=>$id));
        $lieuId='';
        $lieu='';
        if($interv->getIdMagasin()!=null){
            $mag=$this->getDoctrine()->getRepository('AdministrationSocieteBundle:AdmMagasin')->findOneBy(array('magId'=>$interv->getIdMagasin()));
            $lieuId=$mag->getMagId();
            $lieu=$mag->getMagLibelle();
        }
        elseif ($interv->getIdBatiment()!=null){
            $bat= $this->getDoctrine()->getRepository('InterventionBundle:TrvBatiments')->findOneBy(array('id'=>$interv->getIdBatiment()));
            $lieuId=$bat->getId();
            $lieu=$bat->getNom();
        }
        $fournisseurs=$this->getDoctrine()->getRepository('InterventionBundle:TrvFournisseur')->findAll();
        $agent= $this->getDoctrine()->getRepository("InterventionBundle:TrvAgents")-> findBy(array('idIntervention'=>$interv->getId(),'idUser'=>$this->container->get('security.context')->getToken()->getUser() ));
        if($agent[0]->getIdTournee()!=null){
			$interv->setIdTournee($agent[0]->getIdTournee());
            $em = $this->getDoctrine()->getManager();
			$em->persist($interv);
            $em->flush();
			$agents= $this->getDoctrine()->getRepository("InterventionBundle:TrvAgents")-> findBy(array('idIntervention'=>$interv->getId()));
            $factures= $this->getDoctrine()->getRepository("InterventionBundle:TrvFacture")-> findBy(array('idIntervention'=>$interv->getId()));
            $form = $this->createForm(new TrvTourneeType());
            return $this->render('InterventionBundle:Default:saisie_frais.html.twig',
                array('form' => $form->createView(), 'interv'=> $interv, 'lieuId'=>$lieuId, 'lieu'=>$lieu ,'agents'=>$agents,
                    'factures'=>$factures));
        }
        if ($this->getRequest()->isMethod('POST')) {
            $em = $this->getDoctrine()->getManager();
            $form->bind($this->getRequest());
            $dir = $this->get('kernel')->getRootDir().'/../web/uploads/attachments/intervention/'.$interv->getId().'/';
            for($i=1; $i<=$_POST["interventionbundle_trvfacture"]["countFacture"];$i++){
                $fact= new TrvFacture();
                if(isset($_POST["interventionbundle_trvfacture"]["facture".$i])){
                    $fact->setBonLivraison($_POST["interventionbundle_trvfacture"]["facture".$i]);
                }
                if(isset($_POST["interventionbundle_trvfacture"]["reference".$i])){
                    $fact->setReference($_POST["interventionbundle_trvfacture"]["reference".$i]);
                }
                if(isset($_POST["interventionbundle_trvfacture"]["fournisseur".$i])){
                    $f=$em->getRepository('InterventionBundle:TrvFournisseur')->findOneBy(array('id'=>$_POST["interventionbundle_trvfacture"]["fournisseur".$i]));
                    $fact->setFournisseur($f);
                }
                if(isset($_POST["interventionbundle_trvfacture"]["montant".$i])){
                    $fact->setMontant($_POST["interventionbundle_trvfacture"]["montant".$i]);
                }
                $fact->setIdIntervention($interv);
                $em->persist($fact);
                $em->flush();
                if (isset($form['file'.$i])) {
                    $fichier= $form['file'.$i]->getData();
                    if (!empty($fichier)){
                        $piece = new TrvPieceJointe();
                        $origLongName = $fichier->getClientOriginalName();
                        $origExtension = $fichier->getClientOriginalExtension();
                        $origShortName = substr($origLongName, 0, strlen($origLongName) - strlen($origExtension) - 1);
                        $finalFileName = $origShortName . rand(1, 99999) . '.' . $origExtension;
                        $fichier->move($dir, $finalFileName);
                        $piece->setAttachement($finalFileName);
                        $piece->setIdIntervention($interv);
                        $piece->setIdFacture($fact);
                        $em->persist($piece);
                        $em->flush();
                    }
                }
            }
            $statut=$em->getRepository('InterventionBundle:TrvStatus')->findOneBy(array('id'=>2));
            $interv->setIdStatus($statut);
            $interv->setCommentaireAgent($_POST["interventionbundle_trvfacture"]["commentaire"]);
            $em->persist($interv);
            $em->flush();
            return $this->redirect($this->generateUrl('fiche_intervention', array('id'=>$interv->getId())));
        }
        return $this->render('InterventionBundle:Default:saisie_materiel.html.twig',
            array('form' => $form->createView(), 'interv'=> $interv, 'lieuId'=>$lieuId, 'lieu'=>$lieu, 'fournisseur'=>$fournisseurs ));
    }
    /**
     * @Route("/saisie_frais/{id}", name="saisie_frais")
     */

    public function saisieFraisAction($id)
    {
        if ($this->getRequest()->isMethod('POST')) {
            $em = $this->getDoctrine()->getManager();
            $intervention=$em->getRepository('InterventionBundle:TrvIntervention')->findOneBy(array('id'=>$id));
            $tournee= $intervention->getIdTournee();
            $intervention->setDepart($_POST["interventionbundle_trvtournee"]["villeDep"]);
            $intervention->setArrivee($_POST["interventionbundle_trvtournee"]["villeArrivee"]);
            //$intervention->setCommentaireAgent($_POST["interventionbundle_trvtournee"]["commentaireAgent"]);
            $em->persist($intervention);
            $em->flush();
            $tournee->setNbvehicule($_POST["interventionbundle_trvtournee"]["nbrVehicules"]);
            $tournee->setNbkm($_POST["interventionbundle_trvtournee"]["nbrKm"]);
            $tournee->setHebergement($_POST["interventionbundle_trvtournee"]["hebergement"]);
            $tournee->setRepas($_POST["interventionbundle_trvtournee"]["repas"]);
            $tournee->setCommentaire($_POST["interventionbundle_trvtournee"]["commentaireAgent"]);
            $em->persist($tournee);
            $em->flush();
            $form = $this->get('form.factory')->create(new TrvTourneeType());
            $form->bind($this->getRequest());
            $dir = $this->get('kernel')->getRootDir().'/../web/uploads/attachments/intervention/'.$intervention->getId().'/';
                if (isset($form['file'])) {
                    $fichier= $form['file']->getData();
                    if (!empty($fichier)){
                        $piece = new TrvPieceJointe();
                        $origLongName = $fichier->getClientOriginalName();
                        $origExtension = $fichier->getClientOriginalExtension();
                        $origShortName = substr($origLongName, 0, strlen($origLongName) - strlen($origExtension) - 1);
                        $finalFileName = $origShortName . rand(1, 99999) . '.' . $origExtension;
                        $fichier->move($dir, $finalFileName);
                        $piece->setAttachement($finalFileName);
                        $piece->setIdIntervention($intervention);
                        $piece->setIdTournee($intervention->getIdTournee());
                        $em->persist($piece);
                        $em->flush();
                    }
                }

        }


        return $this->redirect($this->generateUrl('ajout_tournee'));
    }

    /**
     * @Route("/nv_fournisseur", name="nv_fournisseur")
     */

    public function nvFournisseurAction()
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $nv_fournisseur = $request->get('nv_fournisseur');
        $fournisseur= new TrvFournisseur();
        $fournisseur->setnom($nv_fournisseur);
        $em->persist($fournisseur);
        $em->flush();
        $res=$fournisseur->getId();
        $return = $res;
        $response = new Response();
        $data = json_encode($return);
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent($data);
        return $response;
    }

    /**
 * @Route("/fournisseur", name="fournisseur")
 */

    public function fournisseurAction()
    {
        $em = $this->getDoctrine()->getManager();
        $result = array();
        $fournisseurs = $em->getRepository('InterventionBundle:TrvFournisseur')->findAll();
        $i = 0;
        foreach ($fournisseurs as $fournisseur) {
            $result[$i]['id'] = $fournisseur->getId();
            $result[$i]['nom'] = $fournisseur->getNom();
            $i++;
        }

        $output = '<option  value></option>';
        foreach ($result as $row) {
            $output .= '<option value="' . $row['id'] . '">' . $row['nom'] . '</option>';
        }

        $return = $output;
        $response = new Response();
        $data = json_encode($return);
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent($data);
        return $response;
    }

}
