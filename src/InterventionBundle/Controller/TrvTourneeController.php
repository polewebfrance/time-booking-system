<?php

namespace InterventionBundle\Controller;

use InterventionBundle\Entity\TrvTournee;
use InterventionBundle\Form\TrvFactureType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use InterventionBundle\Form\AddTrvTourneeType;
use InterventionBundle\Form\TrvTourneeType;
use InterventionBundle\Form\TrvEditTourneeType;
use InterventionBundle\Entity\TrvIntervention;
use InterventionBundle\Entity\TrvAgents;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\ORM\EntityRepository;

/**
 * TrvTournee controller.
 *
 * @Route("/tournee")
 */
class TrvTourneeController extends Controller
{

    /**
     * @Route("/", name="ajout_tournee")
     */
    public function ajoutTourneeAction(Request $request){
    	$em = $this->getDoctrine()->getManager();
    	$tour = new TrvTournee();
    	$form = $this->createForm(new AddTrvTourneeType($em), $tour);
    	$form=$form->handleRequest($request);
        if ($form->isValid()) {
        	$em->persist($tour);
        	$em->flush();
        	foreach ($tour->getAgents() as $agent){
        		$agent->setIdTournee($tour);
        		$em->persist($agent);
        	}
        	$em->flush();
        }
        $tournees= $this->getDoctrine()->getRepository("InterventionBundle:TrvTournee")-> findAll();
        $i=0;
		$tab1=array();
        foreach ($tournees as $tournee){
            $tab1[$i]=array("tournee"=>$tournee);
            $formEdit= $this->createFormBuilder($tournee)
            	->setAction($this->generateUrl('edit_tournee'))
            	->add('id','hidden')
            	->add('agents','entity', array('mapped'=> true,'class' => 'InterventionBundle:TrvAgents',
                'property' => 'idIntervention',
            	'query_builder'=> function (EntityRepository $er)  use ( $tournee )  {
        			return $er->createQueryBuilder('a')
            			->where('a.idUser = ?1')
        				->setParameter(1, $tournee->getIdUser());
    					},
            	'multiple'=>'true',
                'required' => true,
				'attr'=>array('class'=>'chosen form-control input-sm chosen-select chosen-container chosen-container-single chosen-with-drop chosen-container-active chosen-search chosen-drop chosen-single'),
            ))
            	->add('sauvegarder', 'submit', array('attr' => array('class' => 'btn btn-default')))
            
            	->getForm();
            $tab1[$i]['editForm'] = $formEdit->createView();
            $i++;
        }
        return $this->render('InterventionBundle:Default:ajout_tournee.html.twig',
            array('form' => $form->createView(), 'tournee'=>$tab1 ));
    }

    /**
     * @Route("/edit_tournee", name="edit_tournee")
     */

    public function editTourneeIdAction(Request $request)
    {
    	$data = $request->request->get('form');
    	$em = $this->getDoctrine()->getManager();
    	$tournee = $em->getRepository('InterventionBundle:TrvTournee')->find($data['id']);
    	foreach ($tournee->getAgents() as $oldAgent){
    		$oldAgent->setIdTournee(NULL);
    		$em->persist($oldAgent);
    		$em->flush();
    	}
    	foreach ($data['agents'] as $idAgent){
    		$newAgent = $em->getRepository('InterventionBundle:TrvAgents')->find($idAgent);
    		$newAgent->setIdTournee($tournee);
    		$em->persist($newAgent);
    		$em->flush();    		
    	}
        return $this->redirect($this->generateUrl('ajout_tournee'));
    }

    
    /**
     * @Route("/get_agent_by_user/{idUser}", name="get_agent_by_user")
     */
    
    public function getAgentByUSer($idUser)
    {
    
    	$em = $this->getDoctrine()->getManager();
    	$agents = $this->getDoctrine()
    		->getRepository('InterventionBundle:TrvAgents')
    		->findBy(array('idUser'=>$idUser));
    	$data= array();
    	foreach ($agents as $agent){
    		$data[]=array("id" => $agent->getId(),"intervention" => $agent->getIdIntervention()->getId());
    	}
    	return new JsonResponse($data);
    	
    }
    
    
    
}
