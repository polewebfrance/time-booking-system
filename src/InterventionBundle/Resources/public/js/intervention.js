/**
 * Created by ons.allaya on 17/07/2017.
 */

function lieu() {
    $.ajax({
        type: "POST",
        url: "../ajout/lieu",
        dataType: "json",
        success: function (html) {

            $('#interventionbundle_trvintervention_lieux').html(html);
        },
        error: function (xhr, textStatus, errorThrown) {

        }
    });
}
function displayType1() {
    var nat = $('select#interventionbundle_trvintervention_nature1 option:selected').val();
    if (nat != '0') {
        $.ajax({
            async: false,
            dataType: "json",
            type: "POST",
            url: "../ajout/types",
            data: {nat: nat},
            cache: false,
            error: function (jqXHR, textStatus, errorThrown) {
                alert(errorThrown)
            },
            success: function (html) {
                $("select#interventionbundle_trvintervention_type1").html(html);
            }
        });
    }

}
function displayAgent1() {
    $.ajax({
        type: "POST",
        url: "../ajout/agent_maint_tr",
        dataType: "json",
        success: function (html) {

            $('#interventionbundle_trvintervention_agent1').html(html);
        },
        error: function (xhr, textStatus, errorThrown) {

        }
    });
}




$(document).ready(function () {
    jQuery(".chosen").chosen();
    displayAgent1();
    lieu(-1);
    $("#interventionbundle_trvintervention_dateDemande, #interventionbundle_trvintervention_dateDebut, #interventionbundle_trvintervention_dateFin").css({"cursor":"pointer"});
$("#interventionbundle_trvintervention_dateDemande").datepicker({
    dateFormat: "yy-mm-dd",
    minDate:  0,
    language: "fr",
    beforeShow: function(input, inst) {
        $(input).attr('readonly', true);
    },
    onClose: function(dateText, inst) {
        $(this).attr('readonly', false);
    },
});
    $("#interventionbundle_trvintervention_dateDebut").datepicker({
        dateFormat: "yy-mm-dd",
        minDate:  0,
        language: "fr",
        beforeShow: function(input, inst) {
            $(input).attr('readonly', true);
        },
        onClose: function(dateText, inst) {
            $(this).attr('readonly', false);
        },
        onSelect: function(date){
            var date1 = $('#interventionbundle_trvintervention_dateDebut').datepicker('getDate');
            var date = new Date( Date.parse( date1 ) );
            date.setDate( date.getDate() + 1 );
            var newDate = date.toDateString();
            newDate = new Date( Date.parse( newDate ) );
            $('#interventionbundle_trvintervention_dateFin').datepicker("option","minDate",newDate);

        }
    });
$("#interventionbundle_trvintervention_dateFin").datepicker({
    dateFormat: "yy-mm-dd",
    minDate:  0,
    language: "fr",
    beforeShow: function(input, inst) {
        $(input).attr('readonly', true);
    },
    onClose: function(dateText, inst) {
        $(this).attr('readonly', false);
    },
});
    $('#interventionbundle_trvintervention_nature1').on("change", function () {
        displayType1();
    });

    $('#interventionbundle_trvintervention_file1').on("change", function () {
        var str = $('#interventionbundle_trvintervention_file1').val();
        var pieces = str.split("\\").pop(-1);
        document.getElementById("interventionbundle_trvintervention_piece1").value =pieces;
    });

    $('#ajout_inter').bootstrapValidator({
        err: {
            container: '#messages'
        },
        fields: {
            "interventionbundle_trvintervention[lieux]": {
                validators: {
                    notEmpty: {
                        message: 'Veuillez renseigner ce champ!'
                    }
                }
            },
            "interventionbundle_trvintervention[nature1]": {
                validators: {
                    notEmpty: {
                        message: 'Veuillez renseigner ce champ!'
                    }
                }
            },
            "interventionbundle_trvintervention[type1]": {
                validators: {
                    notEmpty: {
                        message: 'Veuillez renseigner ce champ!'
                    }
                }
            },
            "interventionbundle_trvintervention[agent1]": {
                validators: {
                    notEmpty: {
                        message: 'Veuillez renseigner ce champ!'
                    }
                }
            },
            "interventionbundle_trvintervention[idRaison]": {
                validators: {
                    notEmpty: {
                        message: 'Veuillez renseigner ce champ!'
                    }
                }
            },
            "interventionbundle_trvintervention[nbHeures1]": {
                validators: {
                    notEmpty: {
                        message: 'Veuillez renseigner ce champ!'
                    }
                }
            }
        }
    });








})