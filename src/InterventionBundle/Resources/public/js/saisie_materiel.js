/**
 * Created by ons.allaya on 26/07/2017.
 */
function ajouter_fournisseur(i) {
    var nv_fournisseur = $('#interventionbundle_trvfacture_nvFournisseur'+i).val();
    if(nv_fournisseur!=''){
        $.ajax({
            async: false,
            dataType: "json",
            type: "POST",
            url: "../nv_fournisseur",
            data: {nv_fournisseur:nv_fournisseur},
            cache: false,
            error: function (jqXHR, textStatus, errorThrown) {
            },
            success: function (res) {
                $('#interventionbundle_trvfacture_fournisseur'+i +' option:last').before("<option value='"+res+"'>"+ nv_fournisseur +"</option>");
                $('#interventionbundle_trvfacture_nvFournisseur'+i).val('');

            }
        });

    }
}

$(document).ready(function () {

})