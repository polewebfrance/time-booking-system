/**
 * Created by ons.allaya on 02/08/2017.
 */

function lieux(id) {
    $.ajax({
        type: "POST",
        url: "../lieux/"+id,
        dataType: "json",
        success: function (html) {

            $('#interventionbundle_trvintervention_lieux').html(html);
        },
        error: function (xhr, textStatus, errorThrown) {

        }
    });
}

$(document).ready(function () {
    lieux(-1);

    var table=$('#tabRecherche').dataTable({
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": false,
        "bInfo": false,
        "bAutoWidth": true });
    jQuery(".chosen").chosen();
    $('#interventionbundle_recherche_date[name="interventionbundle_recherche[date]"]').daterangepicker(
        {
            locale: {
                format: 'YYYY-MM-DD'
            }
        }
    );
    $('#interventionbundle_recherche_dateIntervention[name="interventionbundle_recherche[dateIntervention]"]').daterangepicker({
            locale: {
                format: 'YYYY-MM-DD'
            }
        }
    );

    $("#interventionbundle_recherche_date, #interventionbundle_recherche_dateIntervention").css({"cursor":"pointer"});


});

