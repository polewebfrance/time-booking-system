

function ajouter_fournisseur(i) {
    var nv_fournisseur = $('#interventionbundle_trvfacture_nvFournisseur'+i).val();
    if(nv_fournisseur!=''){
        $.ajax({
            async: false,
            dataType: "json",
            type: "POST",
            url: "../nv_fournisseur",
            data: {nv_fournisseur:nv_fournisseur},
            cache: false,
            error: function (jqXHR, textStatus, errorThrown) {
            },
            success: function (res) {
                $('#interventionbundle_trvfacture_fournisseur'+i +' option:last').before("<option value='"+res+"'>"+ nv_fournisseur +"</option>");
                $('#interventionbundle_trvfacture_nvFournisseur'+i).val('');

            }
        });

    }
}
function lieux(id) {
    $.ajax({
        type: "POST",
        url: "../lieux/"+id,
        dataType: "json",
        success: function (html) {

            $('#interventionbundle_trvintervention_lieux').html(html);
        },
        error: function (xhr, textStatus, errorThrown) {

        }
    });
}
function type_select(id) {
    var nat = $("select#interventionbundle_trvintervention_nature"+ id +" option:selected").val();
    if (nat != '0') {
        $.ajax({
            async: false,
            dataType: "json",
            type: "POST",
            url: "../type",
            data: {nat: nat},
            cache: false,
            error: function (jqXHR, textStatus, errorThrown) {
                alert(errorThrown)
            },
            success: function (html) {
                $("select#interventionbundle_trvintervention_type"+id).html(html);
            }
        });
    }

}
function file_name(i) {
    var str = $('#interventionbundle_trvintervention_file'+i).val();
    var pieces = str.split("\\").pop(-1);
    document.getElementById("interventionbundle_trvintervention_piece"+i).value =pieces;
}
function file_name_fact(i) {
    var str = $('#interventionbundle_trvintervention_filefact'+i).val();
    var pieces = str.split("\\").pop(-1);
    document.getElementById("interventionbundle_trvintervention_piecefact"+i).value =pieces;
}
function file_name_tr() {
    var str = $('#interventionbundle_trvintervention_fileTournee').val();
    var pieces = str.split("\\").pop(-1);
    document.getElementById("interventionbundle_trvintervention_piecetournee").value =pieces;
}
function ajouter_fournisseur(i) {
    var nv_fournisseur = $('#interventionbundle_trvintervention_nvFournisseur'+i).val();
    if(nv_fournisseur!=''){
        $.ajax({
            async: false,
            dataType: "json",
            type: "POST",
            url: "../nv_fournisseurs",
            data: {nv_fournisseur:nv_fournisseur},
            cache: false,
            error: function (jqXHR, textStatus, errorThrown) {
            },
            success: function (res) {
                $('#interventionbundle_trvintervention_fournisseur'+i +' option:last').before("<option value='"+res+"'>"+ nv_fournisseur +"</option>");
                $('#interventionbundle_trvintervention_nvFournisseur'+i).val('');

            }
        });

    }
}

$(document).ready(function () {
    jQuery(".chosen").chosen();
    var url = window.location.pathname;
    var id = url.substring(url.lastIndexOf('/') + 1);
    $("#interventionbundle_trvintervention_dateDemande, #interventionbundle_trvintervention_dateDebut, #interventionbundle_trvintervention_dateFin").css({"cursor":"pointer"});
    $("#interventionbundle_trvintervention_dateDemande").datepicker({
        dateFormat: "yy-mm-dd",
        minDate:  0,
        language: "fr",
        beforeShow: function(input, inst) {
            $(input).attr('readonly', true);
        },
        onClose: function(dateText, inst) {
            $(this).attr('readonly', false);
        },
    });
    $("#interventionbundle_trvintervention_dateDebut").datepicker({
        dateFormat: "yy-mm-dd",
        minDate:  0,
        language: "fr",
        beforeShow: function(input, inst) {
            $(input).attr('readonly', true);
        },
        onClose: function(dateText, inst) {
            $(this).attr('readonly', false);
        },
        onSelect: function(date){
            var date1 = $('#interventionbundle_trvintervention_dateDebut').datepicker('getDate');
            var date = new Date( Date.parse( date1 ) );
            date.setDate( date.getDate() + 1 );
            var newDate = date.toDateString();
            newDate = new Date( Date.parse( newDate ) );
            $('#interventionbundle_trvintervention_dateFin').datepicker("option","minDate",newDate);

        }
    });
    $("#interventionbundle_trvintervention_dateFin").datepicker({
        dateFormat: "yy-mm-dd",
        minDate:  0,
        language: "fr",
        beforeShow: function(input, inst) {
            $(input).attr('readonly', true);
        },
        onClose: function(dateText, inst) {
            $(this).attr('readonly', false);
        },
    });

});