
$(document).ready(function () {
    jQuery(".chosen").chosen();
	displayAgents();

	$( "#interventionbundle_trvtournee_idUser" ).change(function() {
		displayAgents();
	});
	
	function displayAgents() {
		var user = $("select#interventionbundle_trvtournee_idUser option:selected").val();
		if (user != '0') {
			$.ajax({
				async: false,
				dataType: "json",
				type: "POST",
				url: "./get_agent_by_user/"+user,
				cache: false,
				error: function (jqXHR, textStatus, errorThrown) {
					alert(errorThrown)
				},
				success: function (agents) {
					$("select#interventionbundle_trvtournee_agents").empty();
					if (agents !== null && agents.length !== 0) {
						for(var i=0;i<agents.length;i++) {
							$("#interventionbundle_trvtournee_agents").append('<option value='+agents[i].id+'>'+agents[i].intervention+'</option');
						}
					$('#interventionbundle_trvtournee_agents').trigger("chosen:updated");
					}
				}
			});
		}		
	}
	
	
})