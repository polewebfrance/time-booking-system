<?php

namespace InterventionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class TrvEditTourneeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            	->add('id','hidden')
            	->add('agents','entity', array('mapped'=> true,'class' => 'InterventionBundle:TrvAgents',
                'property' => 'idIntervention',
            	'multiple'=>'true',
                'required' => true,
				'attr'=>array('class'=>'chosen form-control input-sm chosen-select chosen-container chosen-container-single chosen-with-drop chosen-container-active chosen-search chosen-drop chosen-single'),
            ))
            	->add('sauvegarder', 'submit', array('attr' => array('class' => 'btn btn-default')))

        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'InterventionBundle\Entity\TrvTournee'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'interventionbundle_trvtournee';
    }
}
