<?php

namespace InterventionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\File;

class TrvFactureType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('reference1', 'text', array('mapped' => false,'required'=> false,'attr'=>array('class'=>'form-control input-sm')))
            ->add('nvFournisseur1', 'text', array('mapped' => false,'required'=> false,'attr'=>array('class'=>'form-control input-sm', 'style'=>'display: none')))
            ->add('fournisseur1', 'entity', array('mapped' => false,'label'=> 'Fournisseur',
                'class' => 'InterventionBundle:TrvFournisseur',
                'property' => 'nom',
                'required' => false,
                'data'=> '1', 'attr'=>array('class'=>'form-control input-sm'),
            ))
            ->add('montant1', 'text', array('mapped' => false,'required'=> false,'attr'=>array('class'=>'form-control input-sm')))
            ->add('facture1','choice', array('mapped' => false,
                'choices' => array('0' => 'Facture', '1' => 'Bon de livraison'),
                'expanded' => true,
                'multiple' => false,
                'required' => true,
                'data' => '0'
            ))
            ->add('commentaire','textarea', array('mapped' => false,'required'=> false,'attr'=>array('class'=>'form-control')))
            ->add('countFacture','hidden', array('mapped' => false))
            ->add('ajouterUneFacture','button', array('attr' => array('class' => 'btn btn-default', 'value'=>'Ajouter une facture')))
            ->add('sauvegarder','submit', array('attr' => array('class' => 'btn btn-danger')))
            ->add('file1','file', array('mapped' => false,'required'=> false))
            ->add('file2','file', array('mapped' => false,'required'=> false))
            ->add('file3','file', array('mapped' => false,'required'=> false))
            ->add('file4','file', array('mapped' => false,'required'=> false))
            ->add('file5','file', array('mapped' => false,'required'=> false))
            ->add('file6','file', array('mapped' => false,'required'=> false))
            ->add('file7','file', array('mapped' => false,'required'=> false))
            ->add('file8','file', array('mapped' => false,'required'=> false))
            ->add('file9','file', array('mapped' => false,'required'=> false))
            ->add('file10','file', array('mapped' => false,'required'=> false))


        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'InterventionBundle\Entity\TrvFacture'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'interventionbundle_trvfacture';
    }
}
