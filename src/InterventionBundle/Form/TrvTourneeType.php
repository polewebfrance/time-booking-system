<?php

namespace InterventionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class TrvTourneeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('idIntervention','entity', array('mapped'=> false,'class' => 'InterventionBundle:TrvIntervention',
                'property' => 'id', 'multiple'=>'true',
                'required' => false,'attr'=>array('class'=>'chosen form-control input-sm chosen-select chosen-container chosen-container-single chosen-with-drop chosen-container-active chosen-search chosen-drop chosen-single'),
            ))
            ->add('villeDep','text', array('mapped' => false,'required'=> false,'attr'=>array('class'=>'form-control input-sm')))
            ->add('villeArrivee','text', array('mapped' => false,'required'=> false,'attr'=>array('class'=>'form-control input-sm')))
            ->add('nbrVehicules','integer', array('mapped' => false,'required'=> false,'attr'=>array('class'=>'form-control input-sm','min' => 0)))
            ->add('nbrKm','integer', array('mapped' => false,'required'=> false,'attr'=>array('class'=>'form-control input-sm','min' => 0)))
            ->add('repas','integer', array('mapped' => false,'required'=> false,'attr'=>array('class'=>'form-control input-sm','min' => 0, 'step'=> 0.01)))
            ->add('hebergement','integer', array('mapped' => false,'required'=> false,'attr'=>array('class'=>'form-control input-sm','min' => 0, 'step'=> 0.01)))
            ->add('file','file',array('mapped'=> false,'required'=> false))
            ->add('technicien','choice', array('mapped'=> false, 'multiple'=>'true',
                'required' => false,'attr'=>array('class'=>'chosen form-control input-sm chosen-select chosen-container chosen-container-single chosen-with-drop chosen-container-active chosen-search chosen-drop chosen-single'),
            ))
            ->add('commentaireAgent','textarea', array('mapped' => false,'required'=> false,'attr'=>array('class'=>'form-control')))
            ->add('sauvegarder', 'submit', array('attr' => array('class' => 'btn btn-danger')))

        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'InterventionBundle\Entity\TrvTournee'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'interventionbundle_trvtournee';
    }
}
