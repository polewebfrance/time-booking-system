<?php

namespace InterventionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\File;
//use Symfony\Component\HttpFoundation\File\File;

class TrvInterventionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateDemande','text', array('mapped' => false,'required'=> false,'attr'=>array('class'=>'form-control ')))
            ->add('dateDebut','text', array('mapped' => false,'required'=> false,'attr'=>array('class'=>'form-control ')))
            ->add('dateFin','text', array('mapped' => false,'required'=> false,'attr'=>array('class'=>'form-control')))
            ->add('travaux','textarea', array('required'=> false,'attr'=>array('class'=>'form-control')))
            ->add('commentaireAgent','textarea', array('mapped' => false,'required'=> false,'attr'=>array('class'=>'form-control')))
            ->add('commentaire','textarea', array('mapped' => false,'required'=> false,'attr'=>array('class'=>'form-control')))
            ->add('nature1', 'entity', array('mapped' => false,'class' => 'InterventionBundle:TrvNature',
                'property' => 'libelle',
                'required' => false,'attr'=>array('class'=>'form-control'),
            ))
            ->add('type1','choice', array('mapped' => false,'required'=> false,'attr'=>array('class'=>'form-control ')))
            ->add('villeDep','text', array('mapped' => false,'required'=> false,'attr'=>array('class'=>'form-control input-sm')))
            ->add('villeArrivee','text', array('mapped' => false,'required'=> false,'attr'=>array('class'=>'form-control input-sm')))
            ->add('nbrVehicules','integer', array('mapped' => false,'required'=> false,'attr'=>array('class'=>'form-control input-sm','min' => 0)))
            ->add('nbrKm','integer', array('mapped' => false,'required'=> false,'attr'=>array('class'=>'form-control input-sm','min' => 0)))
            ->add('repas','integer', array('mapped' => false,'required'=> false,'attr'=>array('class'=>'form-control input-sm','min' => 0, 'step'=> 0.01)))
            ->add('hebergement','integer', array('mapped' => false,'required'=> false,'attr'=>array('class'=>'form-control input-sm','min' => 0, 'step'=> 0.01)))
            ->add('fileTournee','file',array('mapped'=> false,'required'=> false))
            ->add('idRaison', 'entity', array('class' => 'InterventionBundle:TrvRaison',
                'property' => 'libelle',
                'required' => false,'attr'=>array('class'=>'form-control'),
            ))
            ->add('lieux', 'choice', array('mapped' => false,'required'=> false,'attr'=>array('class'=>'form-control')))
            ->add('agent1','choice', array('mapped' => false,'required'=> false,'attr'=>array('class'=>'form-control ')))
            ->add('nbHeures1','integer', array('mapped' => false,'required'=> false,'attr'=>array('class'=>'form-control')))
            ->add('piece1','text', array('mapped' => false,'required'=> false,'attr'=>array('class'=>'form-control','style' => 'width: 200px')))
            ->add('file1','file', array(
                'data_class' => '\Symfony\Component\HttpFoundation\File\UploadedFile',
                'property_path' => 'file1',
                'required'  => false,
                'mapped'=>false,
            ))
            ->add('observateur', 'choice', array('mapped'=> false, 'multiple'=>'true',
                'required' => false,'attr'=>array('class'=>'chosen form-control input-sm chosen-select chosen-container chosen-container-single chosen-with-drop chosen-container-active chosen-search chosen-drop chosen-single'),
            ))
            ->add('file2','file',array('mapped'=> false))
            ->add('file3','file',array('mapped'=> false))
            ->add('file4','file',array('mapped'=> false))
            ->add('file5','file',array('mapped'=> false))
            ->add('file6','file',array('mapped'=> false))
            ->add('file7','file',array('mapped'=> false))
            ->add('file8','file',array('mapped'=> false))
            ->add('file9','file',array('mapped'=> false))
            ->add('file10','file',array('mapped'=> false))
            ->add('countNat','hidden', array('mapped' => false))
            ->add('countAgent','hidden', array('mapped' => false))
            ->add('countFile','hidden', array('mapped' => false))
            ->add('valider', 'submit', array('attr' => array('class' => 'btn btn-danger')))
            ->add('imprimer', 'button', array('attr' => array('class' => 'btn btn-default')))
            ->add('filefact1','file',array('mapped'=> false))
            ->add('filefact2','file',array('mapped'=> false))
            ->add('filefact3','file',array('mapped'=> false))
            ->add('filefact4','file',array('mapped'=> false))
            ->add('filefact5','file',array('mapped'=> false))
            ->add('filefact6','file',array('mapped'=> false))
            ->add('filefact7','file',array('mapped'=> false))
            ->add('filefact8','file',array('mapped'=> false))
            ->add('filefact9','file',array('mapped'=> false))
            ->add('filefact10','file',array('mapped'=> false))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'InterventionBundle\Entity\TrvIntervention'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'interventionbundle_trvintervention';
    }
}
