<?php

namespace InterventionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RechercheType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('raison', 'entity', array('required' => false,'class' => 'InterventionBundle:TrvRaison',
                'property' => 'libelle',
                'attr'=>array('class'=>'form-control'),
            ))
            ->add('assistante','text', array('required' => false,'attr'=>array('class'=>'form-control')))
            ->add('statut', 'entity', array('required' => false,'label'=> 'Statut',
                'class' => 'InterventionBundle:TrvStatus',
                'property' => 'libelle','multiple'=>'true',
                'attr'=>array('class'=>'chosen form-control input-sm chosen-select chosen-container chosen-container-single chosen-with-drop chosen-container-active chosen-search chosen-drop chosen-single')
            ))
            ->add('date', 'text', array('required' => false,'attr'=>array('class'=>'form-control')))
            ->add('lieu','choice', array('required' => false,'attr'=>array('class'=>'form-control')))
            ->add('nature', 'entity', array('required' => false,'mapped' => false,'class' => 'InterventionBundle:TrvNature',
                'property' => 'libelle',
                'attr'=>array('class'=>'form-control')
            ))
            ->add('type', 'entity', array('required' => false,'mapped' => false,'class' => 'InterventionBundle:TrvType',
                'property' => 'libelle',
                'attr'=>array('class'=>'form-control')
            ))
            ->add('agent','choice',array('required' => false,'multiple'=>'true','attr'=>array('class'=>'chosen form-control input-sm chosen-select chosen-container chosen-container-single chosen-with-drop chosen-container-active chosen-search chosen-drop chosen-single')))
            ->add('dateIntervention','text', array('required' => false,'attr'=>array('class'=>'form-control')))
            ->add('chercher','submit', array('attr' => array('class' => 'btn btn-danger')))
            ->add('afficherTout','submit', array('attr' => array('class' => 'btn btn-danger')))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'InterventionBundle\Entity\TrvIntervention'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'interventionbundle_recherche';
    }
}
