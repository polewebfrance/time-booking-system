<?php

namespace InterventionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\validator\Constraints as Assert;

/**
 * TrvPieceJointe
 *
 * @ORM\Table(name="trv_piece_jointe", indexes={@ORM\Index(name="id_facture", columns={"id_facture"}), @ORM\Index(name="id_intervention", columns={"id_intervention"}), @ORM\Index(name="id_tournee", columns={"id_tournee"})})
 * @ORM\Entity
 */
class TrvPieceJointe
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="attachement", type="string", length=1000, nullable=false)
     */
    private $attachement;

    /**
     * @var \InterventionBundle\Entity\TrvFacture
     *
     * @ORM\ManyToOne(targetEntity="InterventionBundle\Entity\TrvFacture")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_facture", referencedColumnName="id", nullable=true)
     * })
     */
    private $idFacture;

    /**
     * @var \InterventionBundle\Entity\TrvIntervention
     *
     * @ORM\ManyToOne(targetEntity="InterventionBundle\Entity\TrvIntervention")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_intervention", referencedColumnName="id", nullable=true)
     * })
     */
    private $idIntervention;

    /**
     * @var \InterventionBundle\Entity\TrvTournee
     *
     * @ORM\ManyToOne(targetEntity="InterventionBundle\Entity\TrvTournee")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tournee", referencedColumnName="id", nullable=true)
     * })
     */
    private $idTournee;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getAttachement()
    {
        return $this->attachement;
    }

    /**
     * @param string $attachement
     */
    public function setAttachement($attachement)
    {
        $this->attachement = $attachement;
    }

    /**
     * @return TrvFacture
     */
    public function getIdFacture()
    {
        return $this->idFacture;
    }

    /**
     * @param TrvFacture $idFacture
     */
    public function setIdFacture($idFacture)
    {
        $this->idFacture = $idFacture;
    }

    /**
     * @return TrvIntervention
     */
    public function getIdIntervention()
    {
        return $this->idIntervention;
    }

    /**
     * @param TrvIntervention $idIntervention
     */
    public function setIdIntervention($idIntervention)
    {
        $this->idIntervention = $idIntervention;
    }

    /**
     * @return TrvTournee
     */
    public function getIdTournee()
    {
        return $this->idTournee;
    }

    /**
     * @param TrvTournee $idTournee
     */
    public function setIdTournee($idTournee)
    {
        $this->idTournee = $idTournee;
    }






}

