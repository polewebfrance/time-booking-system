<?php

namespace InterventionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TrvCout
 *
 * @ORM\Table(name="trv_cout", indexes={@ORM\Index(name="id_type", columns={"id_type"}),@ORM\Index(name="id_intervention", columns={"id_intervention"})})
 * @ORM\Entity
 */
class TrvCout
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="montant", type="float", precision=10, scale=0, nullable=false)
     */
    private $montant;

    /**
     * @var \InterventionBundle\Entity\TrvType
     *
     * @ORM\ManyToOne(targetEntity="InterventionBundle\Entity\TrvType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_type", referencedColumnName="id", nullable=true)
     * })
     */
    private $idType;

    /**
     * @var \InterventionBundle\Entity\TrvIntervention
     *
     * @ORM\ManyToOne(targetEntity="InterventionBundle\Entity\TrvIntervention")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_intervention", referencedColumnName="id", nullable=true)
     * })
     */
    private $idIntervention;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return float
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * @param float $montant
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;
    }

    /**
     * @return TrvType
     */
    public function getIdType()
    {
        return $this->idType;
    }

    /**
     * @param TrvType $idType
     */
    public function setIdType($idType)
    {
        $this->idType = $idType;
    }

    /**
     * @return TrvIntervention
     */
    public function getIdIntervention()
    {
        return $this->idIntervention;
    }

    /**
     * @param TrvIntervention $idIntervention
     */
    public function setIdIntervention($idIntervention)
    {
        $this->idIntervention = $idIntervention;
    }





}

