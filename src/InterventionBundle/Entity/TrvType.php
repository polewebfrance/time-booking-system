<?php

namespace InterventionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TrvType
 *
 * @ORM\Table(name="trv_type", indexes={@ORM\Index(name="id_nature", columns={"id_nature"})})
 * @ORM\Entity
 */
class TrvType
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=50, nullable=false)
     */
    private $libelle;

    /**
     * @var integer
     *
     * @ORM\Column(name="actif", type="integer", nullable=false)
     */
    private $actif;

    /**
     * @var \InterventionBundle\Entity\TrvNature
     *
     * @ORM\ManyToOne(targetEntity="InterventionBundle\Entity\TrvNature")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_nature", referencedColumnName="id", nullable=true)
     * })
     */
    private $idNature;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @param string $libelle
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
    }

    /**
     * @return int
     */
    public function getActif()
    {
        return $this->actif;
    }

    /**
     * @param int $actif
     */
    public function setActif($actif)
    {
        $this->actif = $actif;
    }

    /**
     * @return TrvNature
     */
    public function getIdNature()
    {
        return $this->idNature;
    }

    /**
     * @param TrvNature $idNature
     */
    public function setIdNature($idNature)
    {
        $this->idNature = $idNature;
    }






}

