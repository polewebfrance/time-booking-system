<?php

namespace InterventionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * TrvTournee
 *
 * @ORM\Table(name="trv_tournee", indexes={@ORM\Index(name="id_user", columns={"id_user"}), @ORM\Index(name="id_intervention", columns={"id_intervention"})})
 * @ORM\Entity
 */
class TrvTournee
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbvehicule", type="integer", nullable=true)
     */
    private $nbvehicule;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbkm", type="integer", nullable=true)
     */
    private $nbkm;

    /**
     * @var string
     *
     * @ORM\Column(name="hebergement", type="string", length=200, nullable=true)
     */
    private $hebergement;

    /**
     * @var \InterventionBundle\Entity\TrvIntervention
     *
     * @ORM\ManyToOne(targetEntity="InterventionBundle\Entity\TrvIntervention")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_intervention", referencedColumnName="id", nullable=true)
     * })
     */
    private $idIntervention;

    /**
     * @var string
     *
     * @ORM\Column(name="repas", type="string", length=50, nullable=true)
     */
    private $repas;

    /**
     * @var string
     *
     * @ORM\Column(name="commentaire", type="string", length=700, nullable=true)
     */
    private $commentaire;

    /**    
    * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User")
    * @ORM\JoinColumns({
   	*   @ORM\JoinColumn(name="id_user", referencedColumnName="id", nullable=true)
    * })
    **/
    private $idUser;
    
    
    /**
     * @var InterventionBundle\Entity\TrvAgents
     *
     * @ORM\OneToMany(targetEntity="InterventionBundle\Entity\TrvAgents", mappedBy="idTournee")
     */
    private $agents;

    public function __construct() {
    	$this->agents = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getNbvehicule()
    {
        return $this->nbvehicule;
    }

    /**
     * @param int $nbvehicule
     */
    public function setNbvehicule($nbvehicule)
    {
        $this->nbvehicule = $nbvehicule;
    }

    /**
     * @return int
     */
    public function getNbkm()
    {
        return $this->nbkm;
    }

    /**
     * @param int $nbkm
     */
    public function setNbkm($nbkm)
    {
        $this->nbkm = $nbkm;
    }

    /**
     * @return string
     */
    public function getHebergement()
    {
        return $this->hebergement;
    }

    /**
     * @param string $hebergement
     */
    public function setHebergement($hebergement)
    {
        $this->hebergement = $hebergement;
    }

    /**
     * @return string
     */
    public function getRepas()
    {
        return $this->repas;
    }

    /**
     * @param string $repas
     */
    public function setRepas($repas)
    {
        $this->repas = $repas;
    }

    /**
     * @return string
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }

    /**
     * @param string $commentaire
     */
    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;
    }

    /**
     * @return \User\UserBundle\Entity\User
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * @param \User\UserBundle\Entity\User $idUser
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;
    }

    /**
     * @return ArrayCollection $agents
     */
    public function getAgents() {
    	return $this->agents;
    }
    

    /**
     * @param ArrayCollection $agent
     */
    public function setAgents($agents) {
    	$this->agents=$agents;
    }

    /**
     * @return TrvIntervention
     */
    public function getIdIntervention()
    {
        return $this->idIntervention;
    }

    /**
     * @param TrvIntervention $idIntervention
     */
    public function setIdIntervention($idIntervention)
    {
        $this->idIntervention = $idIntervention;
    }


}

