<?php

namespace InterventionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TrvFacture
 *
 * @ORM\Table(name="trv_facture", indexes={@ORM\Index(name="fournisseur", columns={"fournisseur"}), @ORM\Index(name="id_intervention", columns={"id_intervention"})})
 * @ORM\Entity
 */
class TrvFacture
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=50, nullable=false)
     */
    private $reference;

    /**
     * @var \InterventionBundle\Entity\TrvFournisseur
     *
     * @ORM\ManyToOne(targetEntity="InterventionBundle\Entity\TrvFournisseur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fournisseur", referencedColumnName="id", nullable=true)
     * })
     */
    private $fournisseur;

    /**
     * @var float
     *
     * @ORM\Column(name="montant", type="float", precision=10, scale=0, nullable=false)
     */
    private $montant;

    /**
     * @var string
     *
     * @ORM\Column(name="bon_livraison", type="string", length=50, nullable=false)
     */
    private $bonLivraison;

    /**
     * @var \InterventionBundle\Entity\TrvIntervention
     *
     * @ORM\ManyToOne(targetEntity="InterventionBundle\Entity\TrvIntervention")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_intervention", referencedColumnName="id", nullable=true)
     * })
     */
    private $idIntervention;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
    }

    /**
     * @return TrvFournisseur
     */
    public function getFournisseur()
    {
        return $this->fournisseur;
    }

    /**
     * @param TrvFournisseur $fournisseur
     */
    public function setFournisseur($fournisseur)
    {
        $this->fournisseur = $fournisseur;
    }

    /**
     * @return float
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * @param float $montant
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;
    }

    /**
     * @return string
     */
    public function getBonLivraison()
    {
        return $this->bonLivraison;
    }

    /**
     * @param string $bonLivraison
     */
    public function setBonLivraison($bonLivraison)
    {
        $this->bonLivraison = $bonLivraison;
    }

    /**
     * @return TrvIntervention
     */
    public function getIdIntervention()
    {
        return $this->idIntervention;
    }

    /**
     * @param TrvIntervention $idIntervention
     */
    public function setIdIntervention($idIntervention)
    {
        $this->idIntervention = $idIntervention;
    }



}

