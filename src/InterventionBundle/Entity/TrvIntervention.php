<?php

namespace InterventionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * TrvIntervention
 *
 * @ORM\Table(name="trv_intervention", indexes={@ORM\Index(name="id_raison", columns={"id_raison"}), @ORM\Index(name="id_batiment", columns={"id_batiment"}), @ORM\Index(name="id_magasin", columns={"id_magasin"}), @ORM\Index(name="assistante", columns={"assistante"}), @ORM\Index(name="id_status", columns={"id_status"})})
 * @ORM\Entity
 */
class TrvIntervention
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_demande", type="date", nullable=true)
     */
    private $dateDemande;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_debut", type="date", nullable=true)
     */
    private $dateDebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_fin", type="date", nullable=true)
     */
    private $dateFin;

    /**
     * @var string
     *
     * @ORM\Column(name="travaux", type="string", length=1000, nullable=true)
     */
    private $travaux;

    /**
     * @var string
     *
     * @ORM\Column(name="commentaire_agent", type="string", length=700, nullable=true)
     */
    private $commentaireAgent;

    /**
     * @var string
     *
     * @ORM\Column(name="nature", type="string", length=50, nullable=true)
     */
    private $nature;

    /**
     * @var string
     *
     * @ORM\Column(name="depart", type="string", length=50, nullable=true)
     */
    private $depart;

    /**
     * @var string
     *
     * @ORM\Column(name="arrivee", type="string", length=50, nullable=true)
     */
    private $arrivee;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbvehicule", type="integer", nullable=true)
     */
    private $nbvehicule;

    /**
     * @var \InterventionBundle\Entity\TrvRaison
     *
     * @ORM\ManyToOne(targetEntity="InterventionBundle\Entity\TrvRaison")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_raison", referencedColumnName="id", nullable=true)
     * })
     */
    private $idRaison;


    /**
     * @var \InterventionBundle\Entity\TrvBatiments
     *
     * @ORM\ManyToOne(targetEntity="InterventionBundle\Entity\TrvBatiments")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_batiment", referencedColumnName="id", nullable=true)
     * })
     */
    private $idBatiment;

    /**
     * @var \Administration\SocieteBundle\Entity\AdmMagasin
     *
     * @ORM\ManyToOne(targetEntity="Administration\SocieteBundle\Entity\AdmMagasin")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_magasin", referencedColumnName="mag_id", nullable=true)
     * })
     */
    private $idMagasin;

    /**
     * @var \User\UserBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="assistante", referencedColumnName="id", nullable=true)
     * })
     */
    private $assistante;

    /**
     * @var \InterventionBundle\Entity\TrvStatus
     *
     * @ORM\ManyToOne(targetEntity="\InterventionBundle\Entity\TrvStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_status", referencedColumnName="id", nullable=true)
     * })
     */
    private $idStatus;

    /**
     * @var \InterventionBundle\Entity\TrvTournee
     *
     * @ORM\ManyToOne(targetEntity="InterventionBundle\Entity\TrvTournee")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tournee", referencedColumnName="id", nullable=true)
     * })
     */
    private $idTournee;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \DateTime
     */
    public function getDateDemande()
    {
        return $this->dateDemande;
    }

    /**
     * @param \DateTime $dateDemande
     */
    public function setDateDemande($dateDemande)
    {
        $this->dateDemande = $dateDemande;
    }

    /**
     * @return \DateTime
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * @param \DateTime $dateDebut
     */
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;
    }

    /**
     * @return \DateTime
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }

    /**
     * @param \DateTime $dateFin
     */
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;
    }



    /**
     * @return string
     */
    public function getTravaux()
    {
        return $this->travaux;
    }

    /**
     * @param string $travaux
     */
    public function setTravaux($travaux)
    {
        $this->travaux = $travaux;
    }

    /**
     * @return string
     */
    public function getCommentaireAgent()
    {
        return $this->commentaireAgent;
    }

    /**
     * @param string $commentaireAgent
     */
    public function setCommentaireAgent($commentaireAgent)
    {
        $this->commentaireAgent = $commentaireAgent;
    }

    /**
     * @return string
     */
    public function getNature()
    {
        return $this->nature;
    }

    /**
     * @param string $nature
     */
    public function setNature($nature)
    {
        $this->nature = $nature;
    }

    /**
     * @return string
     */
    public function getDepart()
    {
        return $this->depart;
    }

    /**
     * @param string $depart
     */
    public function setDepart($depart)
    {
        $this->depart = $depart;
    }

    /**
     * @return string
     */
    public function getArrivee()
    {
        return $this->arrivee;
    }

    /**
     * @param string $arrivee
     */
    public function setArrivee($arrivee)
    {
        $this->arrivee = $arrivee;
    }


    /**
     * @return int
     */
    public function getNbvehicule()
    {
        return $this->nbvehicule;
    }

    /**
     * @param int $nbvehicule
     */
    public function setNbvehicule($nbvehicule)
    {
        $this->nbvehicule = $nbvehicule;
    }

    /**
     * @return TrvRaison
     */
    public function getIdRaison()
    {
        return $this->idRaison;
    }

    /**
     * @param TrvRaison $idRaison
     */
    public function setIdRaison($idRaison)
    {
        $this->idRaison = $idRaison;
    }

    /**
     * @return TrvBatiments
     */
    public function getIdBatiment()
    {
        return $this->idBatiment;
    }

    /**
     * @param TrvBatiments $idBatiment
     */
    public function setIdBatiment($idBatiment)
    {
        $this->idBatiment = $idBatiment;
    }

    /**
     * @return \Administration\SocieteBundle\Entity\AdmMagasin
     */
    public function getIdMagasin()
    {
        return $this->idMagasin;
    }

    /**
     * @param \Administration\SocieteBundle\Entity\AdmMagasin $idMagasin
     */
    public function setIdMagasin($idMagasin)
    {
        $this->idMagasin = $idMagasin;
    }

    /**
     * @return \User\UserBundle\Entity\User
     */
    public function getAssistante()
    {
        return $this->assistante;
    }

    /**
     * @param \User\UserBundle\Entity\User $assistante
     */
    public function setAssistante($assistante)
    {
        $this->assistante = $assistante;
    }

    /**
     * @return TrvStatus
     */
    public function getIdStatus()
    {
        return $this->idStatus;
    }

    /**
     * @param TrvStatus $idStatus
     */
    public function setIdStatus($idStatus)
    {
        $this->idStatus = $idStatus;
    }

    /**
     * @return TrvTournee
     */
    public function getIdTournee()
    {
        return $this->idTournee;
    }

    /**
     * @param TrvTournee $idTournee
     */
    public function setIdTournee($idTournee)
    {
        $this->idTournee = $idTournee;
    }
    
    public function __toString() {
    	return  (string) $this->id;
    }
    

}

