<?php

namespace InterventionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TrvAgents
 *
 * @ORM\Table(name="trv_agents", indexes={@ORM\Index(name="id_intervention", columns={"id_intervention"}), @ORM\Index(name="id_user", columns={"id_user"})})
 * @ORM\Entity
 */
class TrvAgents
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \User\UserBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id", nullable=true)
     * })
     */
    private $idUser;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbheures", type="integer", nullable=false)
     */
    private $nbheures;

    /**
     * @var \InterventionBundle\Entity\TrvIntervention
     *
     * @ORM\ManyToOne(targetEntity="InterventionBundle\Entity\TrvIntervention")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_intervention", referencedColumnName="id", nullable=true)
     * })
     */
    private $idIntervention;

    /**
     * @var \InterventionBundle\Entity\TrvTournee
     *
     * @ORM\ManyToOne(targetEntity="InterventionBundle\Entity\TrvTournee")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tournee", referencedColumnName="id", nullable=true)
     * })
     */
    private $idTournee;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \User\UserBundle\Entity\User
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * @param \User\UserBundle\Entity\User $idUser
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;
    }

    /**
     * @return int
     */
    public function getNbheures()
    {
        return $this->nbheures;
    }

    /**
     * @param int $nbheures
     */
    public function setNbheures($nbheures)
    {
        $this->nbheures = $nbheures;
    }

    /**
     * @return TrvIntervention
     */
    public function getIdIntervention()
    {
        return $this->idIntervention;
    }

    /**
     * @param TrvIntervention $idIntervention
     */
    public function setIdIntervention($idIntervention)
    {
        $this->idIntervention = $idIntervention;
    }

    /**
     * @return TrvTournee
     */
    public function getIdTournee()
    {
        return $this->idTournee;
    }

    /**
     * @param TrvTournee $idTournee
     */
    public function setIdTournee($idTournee)
    {
        $this->idTournee = $idTournee;
    }


}

