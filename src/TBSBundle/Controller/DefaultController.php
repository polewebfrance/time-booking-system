<?php

namespace Time\TBSBundle\Controller;

use DateTime;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Time\TBSBundle\Entity\PlanificationSuiviEtudes;
use Time\TBSBundle\Repository\PlanificationSuiviEtudesRepository;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Time\TBSBundle\Services\WSDataManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class DefaultController
 * @package Time\TBSBundle\Controller
 */
class DefaultController extends Controller
{

    public function indexAction()
    {
        if ($this->get('security.context')->isGranted('ROLE_TBS_ETUDES_USER')) {
            return new RedirectResponse($this->generateUrl('time_tbs_homepage'));
        }
        if ($this->get('security.context')->isGranted('ROLE_TBS_ETUDES_MANAGER')) {
            return new RedirectResponse($this->generateUrl('time_tbs_manager'));
        }
        if ($this->get('security.context')->isGranted('ROLE_TBS_ETUDES_ADMIN')) {
            return new RedirectResponse($this->generateUrl('time_tbs_admin_service'));
        }
        return new RedirectResponse($this->generateUrl('suivi_etudes_accueil'));
    }
}
