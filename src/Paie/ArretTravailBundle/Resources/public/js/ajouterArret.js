function ouvrirCalendar(){
	$(".date" ).removeClass('hasDatepicker').datepicker({
		altField: "#datepicker",
		minDate:-365,
		showAnim:'slideDown',
		showWeek:true,
		numberOfMonths: 2,
		closeText: 'Fermer',
		prevText: 'Précédent',
		nextText: 'Suivant',
		currentText: 'Aujourd\'hui',
		monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
		monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
		dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
		dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
		dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
		weekHeader: '',
		dateFormat: 'dd/mm/yy',
		firstDay: 1
	});
}
$(function(){
	ouvrirCalendar();
	$('.chosen').removeClass('chosen').chosen();
	$('#saveAjouter').click(function(){
		$("#loader").show();
		$.ajax({
			dataType: "html",
			type: "POST",
			url: url_add_arret_valid,
			data: $('form[name="add_arret_travail"]').serialize(),
			success: function(arret){
				$('#detailArret').html(arret);
				$('#ajouterArret').modal('hide');
				$("#loader").hide();
			},
		});
	});
});			