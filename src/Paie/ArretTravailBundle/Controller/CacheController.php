<?php

namespace Paie\ArretTravailBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Paie\ArretTravailBundle\Entity\ArretTravail;
use Paie\ArretTravailBundle\Form\AddArretTravailType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;

class CacheController extends Controller {
	
	/**
	 * @Route("/cache", name="paie_cache")
	 */
	public function addArretAction() {
		$request = $this->container->get('request');
		$arret = new ArretTravail();
		$repository = $this->getDoctrine()->getRepository('AdministrationSocieteBundle:PandoreContrat');
		$form = $this->createForm(new AddArretTravailType(), $arret);	
		$errors = 0;			
		$form->handleRequest($request);		
		//On récupère le cache avant tout 
        $cacheDriver = new \Doctrine\Common\Cache\ApcCache(); 
		$response = $this->render('PaieArretTravailBundle:Default:ajouterArret.html.twig', array(
			'form' => $form->createView(),
			'arret' => $arret,
			'errors' => $errors,
			'arretId' => $arret->getId(),
		));
		$cacheDriver->save('paie_add_arret', $response, "86400");
		return $response;
	}
}	

