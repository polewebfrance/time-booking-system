<?php

namespace Paie\ArretTravailBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ModificationArret
 * 
 * @ORM\Table(name="paie_modification_arret")
 * @ORM\Entity(repositoryClass="Paie\ArretTravailBundle\Entity\ModificationArretRepository")
 */
class ModificationArret
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="Paie\ArretTravailBundle\Entity\ArretTravail")
     * @ORM\JoinColumn(nullable=true)
     */
    private $arret;

    /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User")
     * @ORM\JoinColumn(nullable=true)
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="ancienneValeur", type="text", nullable=true)
     */
    private $ancienneValeur;

    /**
     * @var string
     *
     * @ORM\Column(name="nouvelleValeur", type="text", nullable=true)
     */
    private $nouvelleValeur;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Modification
     */
    public function setDate(\DateTime $date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set arret
     *
     * @param \stdClass $arret
     * @return Modification
     */
    public function setArretTravail($arret)
    {
        $this->arret = $arret;
    
        return $this;
    }

    /**
     * Get arret
     *
     * @return \stdClass 
     */
    public function getArretTravail()
    {
        return $this->arret;
    }

    /**
     * Set user
     *
     * @param \stdClass $user
     * @return Modification
     */
    public function setUser($user)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \stdClass 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Modification
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set ancienneValeur
     *
     * @param string $ancienneValeur
     * @return Modification
     */
    public function setAncienneValeur($ancienneValeur)
    {
        $this->ancienneValeur = $ancienneValeur;
    
        return $this;
    }

    /**
     * Get ancienneValeur
     *
     * @return string 
     */
    public function getAncienneValeur()
    {
        return $this->ancienneValeur;
    }

    /**
     * Set nouvelleValeur
     *
     * @param string $nouvelleValeur
     * @return Modification
     */
    public function setNouvelleValeur($nouvelleValeur)
    {
        $this->nouvelleValeur = $nouvelleValeur;
    
        return $this;
    }

    /**
     * Get nouvelleValeur
     *
     * @return string 
     */
    public function getNouvelleValeur()
    {
        return $this->nouvelleValeur;
    }
    public function __construct()
    {
    	$this->date         = new \DateTime();
    }
    


    /**
     * Get display Modification
     *
     * @return tab
     */
    public function getDisplayModification()
    {
    	$convertibles = array(
    			'typeReception' => array(
    					'1' => 'mail',
    					'2' => 'papier',
    			),
    			'statut' => array(
    					'1' => 'NON_CONCERNE',
    					'2' => 'EN_ATTENTE',
    					'3' => 'SOLDE',
    			)
    	);
    	$result=array();
   		if ($this->getType() === 'dateTraitement' ||
    		$this->getType() === 'datePriseEnCompte' ||
    		$this->getType() === 'dateDebut' ||
    		$this->getType() === 'dateFin' ||
    		$this->getType() === 'dateReception') {
    		$results['modificationAncienneValeur'] = substr($this->getAncienneValeur(), 0, 10) === '1970-01-01' ?
    			'' : substr($this->getAncienneValeur(), 0, 10);
    			$results['modificationNouvelleValeur'] = substr($this->getNouvelleValeur(), 0, 10);
    		} else {
    			$results['modificationAncienneValeur'] = $this->getAncienneValeur();
    			$results['modificationNouvelleValeur'] = $this->getNouvelleValeur();
    		}
    		if (array_key_exists($this->getType(), $convertibles)) {
    			$results['modificationAncienneValeur'] = $convertibles[$this->getType()][$results['modificationAncienneValeur']];
    			$results['modificationNouvelleValeur'] = $convertibles[$this->getType()][$results['modificationNouvelleValeur']];
    		}    	 
    	return $results;
    }
    
}
