<?php

namespace Paie\ArretTravailBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormInterface;
use Doctrine\ORM\EntityRepository;

use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;
use \Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\NotEqualTo;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\NotIdenticalTo;
use Symfony\Component\Validator\Constraints\Required;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Regex;
use \DateTime;
use Paie\ArretTravailBundle\Form\PiecejointeType;


class AddArretTravailType extends AbstractType
{

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		/*	->add('contrat', 'choice', array(
				'choices'   => $this->utilisateurs,
				'mapped' => false,
				'label'     => 'Salarié',
				'attr' => array('class' => 'chosen', 'data-placeholder' => 'choisir utilisateur'),
                'empty_value' => '',
				'required'  => false,
				'constraints' => array(new NotBlank(array('message' => 'Le utilisateur ne peut pas être vide!'))),
				'multiple' => false,
				'expanded' => false,
			)) */
			->add('contrat', 'entity', array(
				'class' => 'AdministrationSocieteBundle:PandoreContrat',
				'mapped' => false,
				'query_builder' => function(EntityRepository $er) {
					return $er->queryActifs();
				},
				'label'     => 'Salarié',
				'attr' => array('class' => 'chosen', 'data-placeholder' => 'choisir utilisateur'),
                'empty_value' => '',
				'required'  => false,
				'constraints' => array(new NotBlank(array('message' => 'Le utilisateur ne peut pas être vide!'))),
				'multiple' => false,
				'expanded' => false,
			))
			->add('nature', 'entity', array(
					'class' => 'PaieArretTravailBundle:ArretNature',
					'property' => 'libelle',
					'empty_value' => '',
					'label'     => 'Nature',
					'constraints' => array(new NotBlank(array('message' => 'La nature ne peut pas être vide!'))),
						
			))
			->add('dateDebut', 'date', array(
				'widget' => 'single_text',
				'format' => 'dd/MM/yyyy',
				'label' => 'Debut',
				'invalid_message' => 'la date debut n\'est pas valide',
				'constraints' => array(new NotBlank(array('message' => 'Date obligatoire!')))
			))
			->add('dateFin', 'date', array(
				'widget' => 'single_text',
				'format' => 'dd/MM/yyyy',
				'label' => 'Fin',
				'invalid_message' => 'la date fin n\'est pas valide'
			))
			->add('dateReception', 'date', array(
				'widget' => 'single_text',
				'format' => 'dd/MM/yyyy',
				'label' => 'Reception',
				'invalid_message' => 'la date reception n\'est pas valide',
				'constraints' => array(new NotBlank(array('message' => 'Date obligatoire!')))
			))
			->add('typeReception', 'choice', array(
				'choices'   => array(
					'1' => 'mail',
					'2' => 'papier',
				),
				'label'     => 'Type Reception',
				'required'  => true,
				'constraints' => array(new NotBlank(array('message' => 'Le type reception ne peut pas être vide!'))),
				'multiple' => false,
				'expanded' => false,
			))
			->add('grossesse', 'checkbox', array(
					'label'     => 'En rapport avec un état pathologique résultant de la grossesse',
			))
			->add('commentaire', 'textarea', array(
				'required'  => false,
			));
	}

	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'Paie\ArretTravailBundle\Entity\ArretTravail',
			'em' => 'default',
			'csrf_protection' => false,
            'validation_groups' => function(FormInterface $form) {
				$data = $form->getData();
				$result = array('Default');
				return $result;
			}
		));
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'add_arret_travail';
	}
}