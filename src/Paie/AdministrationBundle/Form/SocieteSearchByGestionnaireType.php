<?php

namespace Paie\AdministrationBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\File;

class SocieteSearchByGestionnaireType extends AbstractType
{
	private $em;

	public function __construct($em){
		$this->em = $em;
	}

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('direction', 'entity', array(
				'class' => 'AdministrationDirectionBundle:Direction',
				'property' => 'libelle',
				'multiple' => false,
				'expanded' => false,
				'required'  => false,
				'label' => "Direction"
		))
		->add('gestionnairePaie', 'entity', array(
				'class' => 'UserUserBundle:User',
				'property' => 'displayname',
				'multiple' => false,
				'expanded' => false,
				'empty_value' => '',
				'required' => false,
				'label' => 'Gestionnaire Paie',
				'query_builder'=> $this->em->getRepository('UserUserBundle:User')
				->getByRole('ROLE_PAIE_GESTIONNAIRE'),
		))
		->add('gestionnaireContrat', 'entity', array(
				'class' => 'UserUserBundle:User',
				'property' => 'displayname',
				'multiple' => false,
				'expanded' => false,
				'empty_value' => '',
				'required' => false,
				'label' => 'Gestionnaire Contrat',
				'query_builder'=> $this->em->getRepository('UserUserBundle:User')
				->getByRole('ROLE_PAIE_CONTRAT'),
		))
		->add('gestionnaireRH', 'entity', array(
				'class' => 'UserUserBundle:User',
				'property' => 'displayname',
				'multiple' => false,
				'expanded' => false,
				'empty_value' => '',
				'required' => false,
				'label' => 'Gestionnaire RH',
				'query_builder'=> $this->em->getRepository('UserUserBundle:User')
				->getByRole('ROLE_PAIE_RH'),
		))
		;
	}

	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
				'data_class' => 'Paie\AdministrationBundle\Entity\PaieSociete'
		));
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'societe_search';
	}
}
