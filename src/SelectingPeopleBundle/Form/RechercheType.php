<?php

namespace SelectingPeopleBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RechercheType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder


            ->add('statut', 'entity', array('label'=> 'Statut',
                'class' => 'SelectingPeopleBundle:SpStatutMission',
                'property' => 'statut','multiple'=>'true',
                'required' => false,'attr'=>array('class'=>'chosen form-control input-sm chosen-select chosen-container chosen-container-single chosen-with-drop chosen-container-active chosen-search chosen-drop chosen-single'),
            ))
            ->add('direction', 'entity', array('label'=> 'Direction',
                'class' => 'AdministrationDirectionBundle:Direction',
                'property' => 'libelle','multiple'=>'true',
                'required' => false,'attr'=>array('class'=>'chosen form-control input-sm chosen-select chosen-container chosen-container-single chosen-with-drop chosen-container-active chosen-search chosen-drop chosen-single'),
            ))
            ->add('ch_rec', 'entity', array('label'=> 'chargé de recrutement',
                'class' => 'SelectingPeopleBundle:SpChargeRecrutement',
                'property' => 'userId',
                'required' => false,'attr'=>array('class'=>'form-control input-sm chosen-select chosen-container chosen-container-single chosen-with-drop chosen-container-active chosen-search chosen-drop chosen-single'),
            ))
            ->add('cabinet_rec', 'entity', array('class' => 'SelectingPeopleBundle:SpCabinetRecrutement',
                'property' => 'nomCabinet',
                'required' => false,'attr'=>array('class'=>'form-control input-sm chosen-select chosen-container chosen-container-single chosen-with-drop chosen-container-active chosen-search chosen-drop chosen-single'),
            ))
            ->add('sous_trait', 'choice', array('required'=> false,'label'=> 'Sous traité?','attr'=>array('class'=>'form-control input-sm'),
                'choices' => array(
                    '' => '',
                    'Oui' => 'Oui',
                    'Non' => 'Non'
                ),
                'multiple'  => false,
            ))
            ->add('poste','text', array('required'=> false,'attr'=>array('class'=>'form-control input-sm'),'label'=> 'Poste'))
            ->add('reference','text', array('required'=> false,'attr'=>array('class'=>'form-control input-sm'),'label'=> 'Reference'))
            ->add('sec_geo','text', array('required'=> false,'attr'=>array('class'=>'form-control input-sm'),'label'=> 'Secteur géographique'))
            ->add('date_rec','text', array('required'=> false,'attr'=>array('class'=>'form-control input-sm'),'label'=> 'Date Recrutement entre'))
            ->add('demandeur','text', array('required'=> false,'attr'=>array('class'=>'form-control input-sm'),'label'=> 'Demandeur'))
            ->add('nom_cand','text', array('required'=> false,'attr'=>array('class'=>'form-control input-sm'),'label'=> 'Nom du candidat'))
            ->add('date_dmd','text', array('required'=> false,'attr'=>array('class'=>'form-control input-sm'),'label'=> 'Date de demande entre'))
            ->add('modifier', 'button', array('attr' => array('class' => 'btn btn-primary', 'value'=>'Modifier')))
            ->add('enregistrer', 'submit', array('attr' => array('class' => 'btn btn-primary', 'value'=>'Enregistrer')))



        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SelectingPeopleBundle\Entity\SpDemandeRecrutement','SelectingPeopleBundle\Entity\SpDemandeRecrutement'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'selectingpeoplebundle_recherche';
    }
}
