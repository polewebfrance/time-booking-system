<?php

namespace SelectingPeopleBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SpAffectationDemandeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('aff_sous_traitance','choice', array('label' => 'Mode de recrutement','choices'=> array('1' => 'Selecting People', '0' => 'Sous traitance'),'attr'=>array('class'=>'form-control input-sm')))
            ->add('idCabinet', 'entity', array('label'=> 'Cabinet extérieur',
                'class' => 'SelectingPeopleBundle:SpCabinetRecrutement',
                'property' => 'nomCabinet',
                'empty_value' => '',
                'required' => false,'attr'=>array('class'=>'form-control input-sm'),
            ))
            ->add('idRecruteur', 'choice', array('mapped' => false,'required'=> false,'attr'=>array('class'=>'form-control input-sm'),'label'=> 'Chargé de recrutement'))
            ->add('charge_recherche', 'choice', array('mapped' => false,'required'=> false,'attr'=>array('class'=>'form-control input-sm'),'label'=> 'Chargé de recherche'))
            ->add('affecter', 'submit', array('attr' => array('class' => 'btn btn-primary', 'value'=>'Affecter')))
            ->add('sal_hidden','hidden',array('mapped' => false))
            ->add('accompte','hidden', array('mapped' => false,'required'=> false));
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SelectingPeopleBundle\Entity\SpAffectationDemande'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'selectingpeoplebundle_spaffectationdemande';
    }
}
