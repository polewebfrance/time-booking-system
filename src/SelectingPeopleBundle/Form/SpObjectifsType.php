<?php

namespace SelectingPeopleBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SpObjectifsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('userId')
            ->add('objectif','text', array('required'=> false,'attr'=>array('class'=>'form-control')))
            ->add('actif','checkbox')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SelectingPeopleBundle\Entity\SpChargeRecrutement'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'selectingpeoplebundle_spobjectifs';
    }
}
