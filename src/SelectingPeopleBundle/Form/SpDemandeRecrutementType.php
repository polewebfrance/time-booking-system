<?php

namespace SelectingPeopleBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class SpDemandeRecrutementType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder


            ->add('steId', 'entity', array('label'=> 'Société',
                'class' => 'AdministrationSocieteBundle:Societe',
                'property' => 'libelle',
                'empty_value' => '',
                'required' => false,'attr'=>array('class'=>'form-control input-sm chosen-select chosen-container chosen-container-single chosen-with-drop chosen-container-active chosen-search chosen-drop chosen-single'),
            ))
            ->add('produit','text', array('required'=> false,'attr'=>array('class'=>'form-control input-sm'),'label'=> 'Famille de produit'))
            ->add('contratId', 'entity', array(
                'class' => 'SelectingPeopleBundle:SpTypeContrat',
                'property' => 'type',
                'empty_value' => '',
                'required' => false,'attr'=>array('class'=>'form-control input-sm'),
            ))
            ->add('statutCollaborateurId', 'entity', array('label'=> 'Statut',
                'class' => 'SelectingPeopleBundle:SpStatutCollaborateur',
                'property' => 'statut',
                'required' => false,'attr'=>array('class'=>'form-control input-sm'),
            ))
            ->add('paysId', 'entity', array('label'=> 'Pays',
                'class' => 'SelectingPeopleBundle:SpPays',
                'property' => 'nomPays',
                'empty_value' => '',
                'required' => false,'attr'=>array('class'=>'form-control input-sm'),
            ))
            ->add('ville','text', array('label'=> 'Lieu de travail','attr'=>array('class'=>'form-control input-sm')))
            ->add('confidentiel','choice', array('label' => 'Confidentiel',
        'choices' => array('Oui' => 'Oui', 'Non' => 'Non'),
        'expanded' => true,
        'multiple' => false,
        'required' => true,
        'data' => 'Non'
    ))
            ->add('prenomDemandeur','text', array('required'=> true,'label'=> 'Prénom','attr'=>array('class'=>'form-control input-sm'),
                'constraints' => array(new NotBlank(array('message' => 'Prénom obligatoire!')))
            ))
            ->add('nomDemandeur','text', array('required'=> true,'label'=> 'Nom', 'attr'=>array('class'=>'form-control input-sm'),
                'constraints' => array(new NotBlank(array('message' => 'Nom obligatoire!')))
            ))
            ->add('mailDemandeur','text', array('required'=> true,'attr'=>array('class'=>'form-control input-sm'),'label'=> 'Email',
                'constraints' => array(new NotBlank(array('message' => 'Email obligatoire!')))
            ))
            ->add('dir', 'entity', array('label'=> 'Direction',
                'class' => 'AdministrationDirectionBundle:Direction',
                'property' => 'libelle',
                'empty_value' => '',
                'required' => false,'attr'=>array('class'=>'form-control input-sm'),
            ))
            ->add('srv', 'entity', array('label'=> 'Service',
                'class' => 'AdministrationDirectionBundle:Service',
                'property' => 'libelle',
                'empty_value' => '',
                'required' => false,'attr'=>array('class'=>'form-control input-sm'),
            ))
            ->add('pol', 'entity', array('label'=> 'pole',
                'class' => 'SelectingPeopleBundle:SpPole',
                'property' => 'polLibelle',
                'empty_value' => '',
                'required' => false,'attr'=>array('class'=>'form-control input-sm'),
            ))

            ->add('pst', 'entity', array('label'=> 'poste',
                'class' => 'SelectingPeopleBundle:SpPoste',
                'property' => 'pstLibelle',
                'empty_value' => '',
                'required' => false,'attr'=>array('class'=>'form-control input-sm'),
            ))
            ->add('regionId', 'entity', array('label'=> 'Region',
                'class' => 'SelectingPeopleBundle:SpRegion',
                'property' => 'libelle',
                'empty_value' => '',
                'required' => false,'attr'=>array('class'=>'form-control input-sm'),
            ))
            ->add('commentaire', 'textarea',array('label'=>'Commentaire éventuel','attr'=>array('class'=>'form-control'), 'required'=>false))
            ->add('reference','text', array('required'=> false,'attr'=>array('class'=>'form-control input-sm'),'label'=> 'Reference'))
            ->add('periodeEssai','text', array('required'=> false,'attr'=>array('class'=>'form-control input-sm'),'label'=> 'Durée période essai'))
            ->add('commentaireSp', 'textarea',array('label'=>'Commentaire','attr'=>array('class'=>'form-control'), 'required'=>false))
            ->add('preference','choice', array('required'=> false,'attr'=>array('class'=>'form-control input-sm'),'label'=> 'Avec qui souhaitez vous travailler idéalement ?'
            ))
            ->add('exclusion','choice', array('required'=> false,'attr'=>array('class'=>'form-control input-sm'),'label'=> 'Avec qui ne souhaitez vous pas travailler idéalement ?'
            ))
            ->add('save', 'submit', array('attr' => array('class' => 'btn btn-primary', 'value'=>'Envoyer demande')))
            ->add('salaire','text', array('mapped' => false,'required'=> false,'attr'=>array('class'=>'form-control input-sm'),'label'=> 'Salaire souhaité'))

            ->add('modifier', 'button', array('attr' => array('class' => 'btn btn-primary', 'value'=>'Modifier')))
            ->add('enregistrer', 'submit', array('attr' => array('class' => 'btn btn-primary', 'value'=>'Enregistrer')))



        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SelectingPeopleBundle\Entity\SpDemandeRecrutement'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'selectingpeoplebundle_spdemanderecrutement';
    }
}
