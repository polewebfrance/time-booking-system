<?php

namespace SelectingPeopleBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SpRecrutementType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('dateRecrutement','text', array('mapped' => false,'required'=> false,'attr'=>array('class'=>'form-control input-sm'),'label'=> 'Date du recrutement'))
            ->add('dateIntegration','text', array('mapped' => false,'required'=> false,'attr'=>array('class'=>'form-control input-sm'),'label'=> "Date d'intégration"))
            ->add('dateRecrutement2','text', array('mapped' => false,'required'=> false,'attr'=>array('class'=>'form-control input-sm'),'label'=> 'Date du recrutement'))
            ->add('dateIntegration2','text', array('mapped' => false,'required'=> false,'attr'=>array('class'=>'form-control input-sm'),'label'=> "Date d'intégration"))
            ->add('nom_candidat','text', array('mapped' => false,'required'=> false,'attr'=>array('class'=>'form-control input-sm'),'label'=> 'Nom'))
            ->add('prenom_candidat','text', array('mapped' => false,'required'=> false,'attr'=>array('class'=>'form-control input-sm'),'label'=> 'Prénom'))
            ->add('email_candidat','text', array('mapped' => false,'required'=> false,'attr'=>array('class'=>'form-control input-sm'),'label'=> 'Email'))
            ->add('enPoste','choice', array('label' => 'En poste?','choices'=> array('Oui' => 'Oui', 'Non' => 'Non'),'attr'=>array('class'=>'form-control input-sm')))
            ->add('nom_candidat2','text', array('mapped' => false,'required'=> false,'attr'=>array('class'=>'form-control input-sm'),'label'=> 'Nom'))
            ->add('prenom_candidat2','text', array('mapped' => false,'required'=> false,'attr'=>array('class'=>'form-control input-sm'),'label'=> 'Prénom'))
            ->add('email_candidat2','text', array('mapped' => false,'required'=> false,'attr'=>array('class'=>'form-control input-sm'),'label'=> 'Email'))
            ->add('en_poste2','choice', array('mapped' => false,'label' => 'En poste?','attr'=>array('class'=>'form-control input-sm')))
            ->add('periodeConfirmee','choice', array('label' => "Période d'essai confirmée",'choices'=> array('Oui' => 'Oui', 'Non' => 'Non'),'attr'=>array('class'=>'form-control input-sm')))
            ->add('dateRelance','text', array('mapped' => false,'required'=> false,'attr'=>array('class'=>'form-control input-sm'),'label'=> 'Date relance'))
            ->add('suivi', 'submit', array('attr' => array('class' => 'btn btn-primary', 'value'=>'Enregistrer')))
            ->add('enCours', 'button', array('attr' => array('class' => 'btn btn-default', 'value'=>'En cours')))
            ->add('enAttente', 'button', array('attr' => array('class' => 'btn btn-default', 'value'=>'En attente')))
            ->add('annulee', 'button', array('attr' => array('class' => 'btn btn-default', 'value'=>'Annulée')))
            ->add('recrutee', 'button', array('attr' => array('class' => 'btn btn-default', 'value'=>'Recruté')))
            ->add('garantieCours', 'button', array('attr' => array('class' => 'btn btn-default', 'value'=>'Garantie en cours')))
            ->add('garantieTerminee', 'button', array('attr' => array('class' => 'btn btn-default', 'value'=>'Garantie terminée')))
            ->add('doublon', 'button', array('attr' => array('class' => 'btn btn-default', 'value'=>'Doublon')))
            ->add('vivier', 'button', array('attr' => array('class' => 'btn btn-default', 'value'=>'Vivier')))
            ->add('sourcing', 'button', array('attr' => array('class' => 'btn btn-default', 'value'=>'Sourcing')))
            ->add('garantieReattribuee', 'button', array('attr' => array('class' => 'btn btn-default', 'value'=>'Garantie réattribuée')))
            ->add('garantieTermineeReattribuee', 'button', array('attr' => array('class' => 'btn btn-default', 'value'=>'Garantie terminée réattribuée')))
            ->add('blanc', 'button', array('attr' => array('class' => 'btn btn-default', 'value'=>'Blanc')))
            ->add('reference','text', array('mapped'=>false,'required'=> false,'attr'=>array('class'=>'form-control input-sm'),'label'=> 'Reference'))
            ->add('salaire','text', array('mapped' => false,'required'=> false,'attr'=>array('class'=>'form-control input-sm'),'label'=> 'Salaire souhaité'))
            ->add('statutt','hidden',array('mapped' => false))
            ->add('garantie', 'choice', array('mapped' => true,'attr'=>array('class'=>'form-control input-sm'),
                'choices' => array(
                    'Oui' => 'Oui',
                    'Non' => 'Non'
                ),
                'multiple'  => false,
            ))


        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SelectingPeopleBundle\Entity\SpRecrutement'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'selectingpeoplebundle_sprecrutement';
    }
}
