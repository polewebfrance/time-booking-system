<?php

namespace SelectingPeopleBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SpPosteType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id','hidden')
            ->add('pstLibelle','text', array('required'=> true,'attr'=>array('class'=>'form-control input-sm'),'label'=> 'Nom poste'))
            ->add('pstSalaire','text', array('required'=> true,'attr'=>array('class'=>'form-control input-sm'),'label'=> 'Salaire'))
            ->add('ajouterPoste', 'button', array('attr' => array('class' => 'btn btn-default', 'value'=>'Ajouter poste')))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SelectingPeopleBundle\Entity\SpPoste'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'selectingpeoplebundle_spposte';
    }
}
