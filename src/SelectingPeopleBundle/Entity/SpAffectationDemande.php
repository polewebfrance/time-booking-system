<?php

namespace SelectingPeopleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use SelectingPeopleBundle\SelectingPeopleBundle;

/**
 * SpAffectationDemande
 *
 * @ORM\Table(name="sp_affectation_demande", indexes={@ORM\Index(name="dmd_id", columns={"dmd_id"}), @ORM\Index(name="id_cabinet", columns={"id_cabinet"}), @ORM\Index(name="fk_id_recruteur", columns={"id_recruteur"})})
 * @ORM\Entity
 */
class SpAffectationDemande
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    /**
     * @var \SelectingPeopleBundle\Entity\SpDemandeRecrutement
     *
     * @ORM\OneToOne(targetEntity="SelectingPeopleBundle\Entity\SpDemandeRecrutement", fetch="EAGER")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="dmd_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $dmdId;

    /**
     * @var integer
     *
     * @ORM\Column(name="aff_sous_traitance", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $affSousTraitance;
    /**
     * @var \SelectingPeopleBundle\Entity\SpChargeRecrutement
     *
     * @ORM\OneToOne(targetEntity="SelectingPeopleBundle\Entity\SpChargeRecrutement", fetch="EAGER")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_recruteur", referencedColumnName="id", nullable=true)
     *
     * })
     */

    private $idRecruteur;
    /**
     * @var \SelectingPeopleBundle\Entity\SpCabinetRecrutement
     *
     * @ORM\OneToOne(targetEntity="SelectingPeopleBundle\Entity\SpCabinetRecrutement", fetch="EAGER")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cabinet", referencedColumnName="id", nullable=true)
     *
     * })
     */

    private $idCabinet;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_affectation", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $dateAffectation;

    /**
     * @var integer
     *
     * @ORM\Column(name="premier_recruteur", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $premierRecruteur;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dmdId
     *
     * @param \SelectingPeopleBundle\Entity\SpDemandeRecrutement $dmdId
     *
     * @return SpAffectationDemande
     */
    public function setDmdId(\SelectingPeopleBundle\Entity\SpDemandeRecrutement $dmdId = null)
    {
        $this->dmdId = $dmdId;

        return $this;
    }

    /**
     * Get dmdId
     *
     * @return \SelectingPeopleBundle\Entity\SpDemandeRecrutement
     */
    public function getDmdId()
    {
        return $this->dmdId;
    }

    /**
     * Set affSousTraitance
     *
     * @param integer $affSousTraitance
     *
     * @return SpAffectationDemande
     */
    public function setAffSousTraitance($affSousTraitance)
    {
        $this->affSousTraitance = $affSousTraitance;

        return $this;
    }

    /**
     * Get affSousTraitance
     *
     * @return integer
     */
    public function getAffSousTraitance()
    {
        return $this->affSousTraitance;
    }

    /**
     * Set idRecruteur
     *
     * @param \SelectingPeopleBundle\Entity\SpChargeRecrutement $idRecruteur
     *
     * @return SpAffectationDemande
     */
    public function setIdRecruteur(\SelectingPeopleBundle\Entity\SpChargeRecrutement $idRecruteur = null)
    {
        $this->idRecruteur = $idRecruteur;

        return $this;
    }

    /**
     * Get idRecruteur
     *
     * @return \SelectingPeopleBundle\Entity\SpChargeRecrutement
     */
    public function getIdRecruteur()
    {
        return $this->idRecruteur;
    }


    /**
     * Set idCabinet
     *
     * @param \SelectingPeopleBundle\Entity\SpCabinetRecrutement $idCabinet
     *
     * @return SpAffectationDemande
     */
    public function setIdCabinet(\SelectingPeopleBundle\Entity\SpCabinetRecrutement $idCabinet = null)
    {
        $this->idCabinet = $idCabinet;

        return $this;
    }

    /**
     * Get idCabinet
     *
     * @return \SelectingPeopleBundle\Entity\SpCabinetRecrutement
     */
    public function getIdCabinet()
    {
        return $this->idCabinet;
    }

    /**
     * Set dateAffectation
     *
     * @param \DateTime $dateAffectation
     *
     * @return SpAffectationDemande
     */
    public function setDateAffectation($dateAffectation)
    {
        $this->dateAffectation = $dateAffectation;

        return $this;
    }

    /**
     * Get dateAffectation
     *
     * @return \DateTime
     */
    public function getDateAffectation()
    {
        return $this->dateAffectation;
    }

    /**
     * Set premierRecruteur
     *
     * @param integer $premierRecruteur
     *
     * @return SpAffectationDemande
     */
    public function setPremierRecruteur($premierRecruteur)
    {
        $this->premierRecruteur = $premierRecruteur;

        return $this;
    }

    /**
     * Get premierRecruteur
     *
     * @return integer
     */
    public function getPremierRecruteur()
    {
        return $this->premierRecruteur;
    }

}

