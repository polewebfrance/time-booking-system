<?php

namespace SelectingPeopleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PosteBridge
 *
 * @ORM\Table(name="sp_poste_bridge", indexes={@ORM\Index(name="fk_poste_bridge_direction_dir1_idx", columns={"dir_id"}), @ORM\Index(name="fk_poste_bridge_service_srv1_idx", columns={"srv_id"}), @ORM\Index(name="fk_poste_bridge_pole_pol1_idx", columns={"pol_id"}), @ORM\Index(name="fk_poste_bridge_poste_pst1_idx", columns={"pst_id"})})
 * @ORM\Entity
 */
class SpPosteBridge
{
    /**
     * @var integer
     *
     * @ORM\Column(name="bdg_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $bdgId;

    /**
     * @var \Administration\DirectionBundle\Entity\Direction
     *
     * @ORM\ManyToOne(targetEntity="Administration\DirectionBundle\Entity\Direction")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="dir_id", referencedColumnName="id")
     * })
     */
    private $dir;

    /**
     * @var \SelectingPeopleBundle\Entity\SpPoste
     *
     * @ORM\ManyToOne(targetEntity="SelectingPeopleBundle\Entity\SpPoste")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pst_id", referencedColumnName="pst_id")
     * })
     */
    private $pst;

    /**
     * @var \Administration\DirectionBundle\Entity\Service
     *
     * @ORM\ManyToOne(targetEntity="Administration\DirectionBundle\Entity\Service")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="srv_id", referencedColumnName="id")
     * })
     */
    private $srv;

    /**
     * @var \SelectingPeopleBundle\Entity\SpPole
     *
     * @ORM\ManyToOne(targetEntity="SelectingPeopleBundle\Entity\SpPole")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pol_id", referencedColumnName="pol_id")
     * })
     */
    private $pol;


    /**
     * Get bdgId
     *
     * @return integer
     */
    public function getBdgId()
    {
        return $this->bdgId;
    }

    /**
     * Set dir
     *
     * @param \Administration\DirectionBundle\Entity\Direction $dir
     *
     * @return PosteBridge
     */
    public function setDir(\Administration\DirectionBundle\Entity\Direction $dir = null)
    {
        $this->dir = $dir;
    
        return $this;
    }

    /**
     * Get dir
     *
     * @return \Administration\DirectionBundle\Entity\Direction
     */
    public function getDir()
    {
        return $this->dir;
    }

    /**
     * Set pst
     *
     * @param \SelectingPeopleBundle\Entity\SpPoste $pst
     *
     * @return PosteBridge
     */
    public function setPst(\SelectingPeopleBundle\Entity\SpPoste $pst = null)
    {
        $this->pst = $pst;
    
        return $this;
    }

    /**
     * Get pst
     *
     * @return \SelectingPeopleBundle\Entity\SpPoste
     */
    public function getPst()
    {
        return $this->pst;
    }

    /**
     * Set srv
     *
     * @param \Administration\DirectionBundle\Entity\Service $srv
     *
     * @return PosteBridge
     */
    public function setSrv(\Administration\DirectionBundle\Entity\Service $srv = null)
    {
        $this->srv = $srv;
    
        return $this;
    }

    /**
     * Get srv
     *
     * @return \Administration\DirectionBundle\Entity\Service
     */
    public function getSrv()
    {
        return $this->srv;
    }

    /**
     * Set pol
     *
     * @param \SelectingPeopleBundle\Entity\SpPole $pol
     *
     * @return PosteBridge
     */
    public function setPol(\SelectingPeopleBundle\Entity\SpPole $pol = null)
    {
        $this->pol = $pol;

        return $this;
    }

    /**
     * Get pol
     *
     * @return \SelectingPeopleBundle\Entity\SpPole
     */
    public function getPol()
    {
        return $this->pol;
    }

}

