<?php

namespace SelectingPeopleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SpCabinetRecrutement
 *
 * @ORM\Table(name="sp_cabinet_recrutement")
 * @ORM\Entity
 */
class SpCabinetRecrutement
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_cabinet", type="string", length=500, precision=0, scale=0, nullable=false, unique=false)
     */
    private $nomCabinet;

    /**
     * @var string
     *
     * @ORM\Column(name="pays_cabinet", type="string", length=500, precision=0, scale=0, nullable=false, unique=false)
     *
     */

    private $paysCabinet;

    /**
     * @var string
     *
     * @ORM\Column(name="mail_cabinet", type="string", length=500, precision=0, scale=0, nullable=false, unique=false)
     */
    private $mailCabinet;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomCabinet
     *
     * @param string $nomCabinet
     *
     * @return SpCabinetRecrutement
     */
    public function setNomCabinet($nomCabinet)
    {
        $this->nomCabinet = $nomCabinet;

        return $this;
    }

    /**
     * Get nomCabinet
     *
     * @return string
     */
    public function getNomCabinet()
    {
        return $this->nomCabinet;
    }

    /**
     * Set paysCabinet
     *
     * @param string $paysCabinet
     *
     * @return SpCabinetRecrutement
     */
    public function setPaysCabinet($paysCabinet)
    {
        $this->paysCabinet = $paysCabinet;

        return $this;
    }

    /**
     * Get paysCabinet
     *
     * @return string
     */
    public function getPaysCabinet()
    {
        return $this->paysCabinet;
    }

    /**
     * Set mailCabinet
     *
     * @param string $mailCabinet
     *
     * @return SpCabinetRecrutement
     */
    public function setMailCabinet($mailCabinet)
    {
        $this->mailCabinet = $mailCabinet;

        return $this;
    }

    /**
     * Get mailCabinet
     *
     * @return string
     */
    public function getMailCabinet()
    {
        return $this->mailCabinet;
    }
}

