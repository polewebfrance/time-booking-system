<?php

namespace SelectingPeopleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SpStatutCollaborateur
 *
 * @ORM\Table(name="sp_statut_collaborateur")
 * @ORM\Entity
 */
class SpStatutCollaborateur
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="statut", type="string", length=500, precision=0, scale=0, nullable=false, unique=false)
     */
    private $statut;

    /**
     * @var integer
     *
     * @ORM\Column(name="periode", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $periode;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set statut
     *
     * @param string $statut
     *
     * @return SpStatutCollaborateur
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * Get statut
     *
     * @return string
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * Set periode
     *
     * @param integer $periode
     *
     * @return SpStatutCollaborateur
     */
    public function setPeriode($periode)
    {
        $this->periode = $periode;

        return $this;
    }

    /**
     * Get periode
     *
     * @return integer
     */
    public function getPeriode()
    {
        return $this->periode;
    }
}

