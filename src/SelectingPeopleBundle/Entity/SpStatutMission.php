<?php

namespace SelectingPeopleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SpStatutMission
 *
 * @ORM\Table(name="sp_statut_mission")
 * @ORM\Entity
 */
class SpStatutMission
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="statut", type="string", length=500, precision=0, scale=0, nullable=false, unique=false)
     */
    private $statut;

    /**
     * @var string
     *
     * @ORM\Column(name="statut_origine", type="string", length=500, precision=0, scale=0, nullable=false, unique=false)
     */
    private $statutOrigine;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set statut
     *
     * @param string $statut
     *
     * @return SpStatutMission
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * Get statut
     *
     * @return string
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * Set statutOrigine
     *
     * @param string $statutOrigine
     *
     * @return SpStatutMission
     */
    public function setStatutOrigine($statutOrigine)
    {
        $this->statutOrigine = $statutOrigine;

        return $this;
    }

    /**
     * Get statutOrigine
     *
     * @return string
     */
    public function getStatutOrigine()
    {
        return $this->statutOrigine;
    }

    function __toString(){
        return $this->getStatut();
    }
}

