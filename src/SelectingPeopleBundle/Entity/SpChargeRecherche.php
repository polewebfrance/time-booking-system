<?php

namespace SelectingPeopleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SpChargeRecherche
 *
 * @ORM\Table(name="sp_charge_recherche")
 * @ORM\Entity
 */
class SpChargeRecherche
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \User\UserBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $userId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Set userId
     *
     * @param \User\UserBundle\Entity\User $userId
     *
     * @return SpDemandeRecrutement
     */
    public function setUserId(\User\UserBundle\Entity\User $userId = null)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return \User\UserBundle\Entity\User
     */
    public function getUserId()
    {
        return $this->userId;
    }

}

