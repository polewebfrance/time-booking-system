<?php

namespace SelectingPeopleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PostePst
 *
 * @ORM\Table(name="sp_poste")
 * @ORM\Entity
 */
class SpPoste
{
    /**
     * @var string
     *
     * @ORM\Column(name="pst_libelle", type="string", length=70, nullable=false)
     */
    private $pstLibelle;

    /**
     * @var integer
     *
     * @ORM\Column(name="pst_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $pstId;
    /**
     * @var integer
     *
     * @ORM\Column(name="pst_salaire", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $pstSalaire;


    /**
     * Set pstLibelle
     *
     * @param string $pstLibelle
     *
     * @return SpPoste
     */
    public function setPstLibelle($pstLibelle)
    {
        $this->pstLibelle = $pstLibelle;
    
        return $this;
    }

    /**
     * Get pstLibelle
     *
     * @return string
     */
    public function getPstLibelle()
    {
        return $this->pstLibelle;
    }

    /**
     * Get pstId
     *
     * @return integer
     */
    public function getPstId()
    {
        return $this->pstId;
    }

    /**
     * Get pstSalaire
     *
     * @return integer
     */
    public function getPstSalaire()
    {
        return $this->pstSalaire;
    }

    /**
     * Set pstSalaire
     *
     * @param integer $pstSalaire
     *
     * @return SpPoste
     */
    public function setPstSalaire($pstSalaire)
    {
        $this->pstSalaire = $pstSalaire;
    }

}

