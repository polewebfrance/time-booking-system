<?php

namespace SelectingPeopleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SpChargeRecrutement
 *
 * @ORM\Table(name="sp_charge_recrutement", indexes={@ORM\Index(name="user_id", columns={"user_id"})})
 * @ORM\Entity
 */
class SpChargeRecrutement
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="objectif", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $objectif;

    /**
     * @var integer
     *
     * @ORM\Column(name="actif", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $actif;

    /**
     * @var \User\UserBundle\Entity\User
     *
     * @ORM\OneToOne(targetEntity="User\UserBundle\Entity\User", fetch="EAGER")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
     *
     * })
     */
    private $userId;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set objectif
     *
     * @param integer $objectif
     *
     * @return SpChargeRecrutement
     */
    public function setObjectif($objectif)
    {
        $this->objectif = $objectif;

        return $this;
    }

    /**
     * Get objectif
     *
     * @return integer
     */
    public function getObjectif()
    {
        return $this->objectif;
    }

    /**
     * Set actif
     *
     * @param integer $actif
     *
     * @return SpChargeRecrutement
     */
    public function setActif($actif)
    {
        $this->actif = $actif;

        return $this;
    }

    /**
     * Get actif
     *
     * @return integer
     */
    public function getActif()
    {
        return $this->actif;
    }

    /**
     * Set userId
     *
     * @param \User\UserBundle\Entity\User $userId
     *
     * @return SpChargeRecrutement
     */
    public function setUserId(\User\UserBundle\Entity\User $userId = null)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return \User\UserBundle\Entity\User
     */
    public function getUserId()
    {
        return $this->userId;
    }
}

