<?php

namespace SelectingPeopleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SpPays
 *
 * @ORM\Table(name="sp_pays")
 * @ORM\Entity
 */
class SpPays
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_pays", type="string", length=500, precision=0, scale=0, nullable=false, unique=false)
     */
    private $nomPays;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomPays
     *
     * @param string $nomPays
     *
     * @return SpPays
     */
    public function setNomPays($nomPays)
    {
        $this->nomPays = $nomPays;

        return $this;
    }

    /**
     * Get nomPays
     *
     * @return string
     */
    public function getNomPays()
    {
        return $this->nomPays;
    }
}

