<?php

namespace SelectingPeopleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SpRecrutement
 *
 * @ORM\Table(name="sp_recrutement", indexes={@ORM\Index(name="dmd_id", columns={"dmd_id"})})
 * @ORM\Entity
 */
class SpRecrutement
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_candidat", type="string", length=500, precision=0, scale=0, nullable=false, unique=false)
     */
    private $nomCandidat;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom_candidat", type="string", length=500, precision=0, scale=0, nullable=false, unique=false)
     */
    private $prenomCandidat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_recrutement", type="date", precision=0, scale=0, nullable=false, unique=false)
     */
    private $dateRecrutement;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_integration", type="date", precision=0, scale=0, nullable=false, unique=false)
     */
    private $dateIntegration;
    /**
     * @var string
     *
     * @ORM\Column(name="periode_confirmee", type="string", length=11, precision=0, scale=0, nullable=false, unique=false)
     */

    private $periodeConfirmee;
    /**
     * @var string
     *
     * @ORM\Column(name="en_poste", type="string", length=11, precision=0, scale=0, nullable=true, unique=false)
     */
    private $enPoste;

    /**
     * @var integer
     *
     * @ORM\Column(name="garantie", type="string", length=10,precision=0, scale=0, nullable=true, unique=false)
     */
    private $garantie;

    /**
     * @var \SelectingPeopleBundle\Entity\SpDemandeRecrutement
     *
     * @ORM\ManyToOne(targetEntity="SelectingPeopleBundle\Entity\SpDemandeRecrutement")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="dmd_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $dmdId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_relance", type="date", precision=0, scale=0, nullable=false, unique=false)
     */
    private $dateRelance;

    /**
     * @var integer
     *
     * @ORM\Column(name="numero", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $numero;

    /**
     * @var string
     *
     * @ORM\Column(name="mail", type="string", length=500, precision=0, scale=0, nullable=false, unique=false)
     */
    private $mail;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomCandidat
     *
     * @param string $nomCandidat
     *
     * @return SpRecrutement
     */
    public function setNomCandidat($nomCandidat)
    {
        $this->nomCandidat = $nomCandidat;

        return $this;
    }

    /**
     * Get nomCandidat
     *
     * @return string
     */
    public function getNomCandidat()
    {
        return $this->nomCandidat;
    }

    /**
     * Set prenomCandidat
     *
     * @param string $prenomCandidat
     *
     * @return SpRecrutement
     */
    public function setPrenomCandidat($prenomCandidat)
    {
        $this->prenomCandidat = $prenomCandidat;

        return $this;
    }

    /**
     * Get prenomCandidat
     *
     * @return string
     */
    public function getPrenomCandidat()
    {
        return $this->prenomCandidat;
    }

    /**
     * Set dateRecrutement
     *
     * @param \DateTime $dateRecrutement
     *
     * @return SpRecrutement
     */
    public function setDateRecrutement($dateRecrutement)
    {
        $this->dateRecrutement = $dateRecrutement;

        return $this;
    }

    /**
     * Get dateRecrutement
     *
     * @return \DateTime
     */
    public function getDateRecrutement()
    {
        return $this->dateRecrutement;
    }

    /**
     * Set dateIntegration
     *
     * @param \DateTime $dateIntegration
     *
     * @return SpRecrutement
     */
    public function setDateIntegration($dateIntegration)
    {
        $this->dateIntegration = $dateIntegration;

        return $this;
    }

    /**
     * Get dateIntegration
     *
     * @return \DateTime
     */
    public function getDateIntegration()
    {
        return $this->dateIntegration;
    }

    /**
     * @return string
     */
    public function getPeriodeConfirmee()
    {
        return $this->periodeConfirmee;
    }

    /**
     * @param string $periodeConfirmee
     */
    public function setPeriodeConfirmee($periodeConfirmee)
    {
        $this->periodeConfirmee = $periodeConfirmee;
    }

    /**
     * @return string
     */
    public function getEnPoste()
    {
        return $this->enPoste;
    }

    /**
     * @param string $enPoste
     */
    public function setEnPoste($enPoste)
    {
        $this->enPoste = $enPoste;
    }


    /**
     * @param string $garantie
     */
    public function setGarantie($garantie)
    {
        $this->garantie = $garantie;

        return $this;
    }

    /**
     * @return string
     */
    public function getGarantie()
    {
        return $this->garantie;
    }

    /**
     * Set dmdId
     *
     * @param \SelectingPeopleBundle\Entity\SpDemandeRecrutement $dmdId
     *
     * @return SpRecrutement
     */
    public function setDmdId(\SelectingPeopleBundle\Entity\SpDemandeRecrutement $dmdId = null)
    {
        $this->dmdId = $dmdId;

        return $this;
    }

    /**
     * Get dmdId
     *
     * @return \SelectingPeopleBundle\Entity\SpDemandeRecrutement
     */
    public function getDmdId()
    {
        return $this->dmdId;
    }

    /**
     * Set dateRelance
     *
     * @param \DateTime $dateRelance
     *
     * @return SpRecrutement
     */
    public function setDateRelance($dateRelance)
    {
        $this->dateRelance = $dateRelance;

        return $this;
    }

    /**
     * Get dateRelance
     *
     * @return \DateTime
     */
    public function getDateRelance()
    {
        return $this->dateRelance;
    }

    /**
     * Set numero
     *
     * @param integer $numero
     *
     * @return SpRecrutement
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return integer
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set mail
     *
     * @param string $mail
     *
     * @return SpRecrutement
     */
    public function setMail($mail)
    {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get mail
     *
     * @return string
     */
    public function getMail()
    {
        return $this->mail;
    }
}

