<?php

namespace SelectingPeopleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * SpDemandeRecrutement
 *
 * @ORM\Table(name="sp_demande_recrutement", indexes={ @ORM\Index(name="fk_ctr", columns={"contrat_id"}), @ORM\Index(name="fk_st_coll", columns={"statut_collaborateur_id"}), @ORM\Index(name="fk_pys", columns={"pays_id"}), @ORM\Index(name="fk_st_mission", columns={"statut_mission_id"}), @ORM\Index(name="region_id", columns={"region_id"}), @ORM\Index(name="ste_id", columns={"ste_id"}), @ORM\Index(name="poste_bridge_id", columns={"poste_bridge_id"}), @ORM\Index(name="id_doublon", columns={"id_doublon"}),@ORM\Index(name="fk_dir1_idx", columns={"dir_id"}), @ORM\Index(name="fk_srv1_idx", columns={"srv_id"}), @ORM\Index(name="fk_pol1_idx", columns={"pol_id"}), @ORM\Index(name="fk_pst1_idx", columns={"pst_id"})})
 * @ORM\Entity
 */
class SpDemandeRecrutement
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", precision=0, scale=0, nullable=false, unique=false)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=500, precision=0, scale=0, nullable=true, unique=false)
     */
    private $ville;

    /**
     * @var string
     *
     * @ORM\Column(name="confidentiel", type="string", length=10, precision=0, scale=0, nullable=true, unique=false)
     */
    private $confidentiel;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom_demandeur", type="string", length=500, precision=0, scale=0, nullable=true, unique=false)
     */
    private $prenomDemandeur;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_demandeur", type="string", length=500, precision=0, scale=0, nullable=true, unique=false)
     */
    private $nomDemandeur;

    /**
     * @var string
     *
     * @ORM\Column(name="mail_demandeur", type="string", length=500, precision=0, scale=0, nullable=true, unique=false)
     */
    private $mailDemandeur;

    /**
     * @var string
     *
     * @ORM\Column(name="commentaire", type="string", length=500, precision=0, scale=0, nullable=true, unique=false)
     */
    private $commentaire;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=12, precision=0, scale=0, nullable=true, unique=false)
     */
    private $reference;

    /**
     * @var float
     *
     * @ORM\Column(name="accompte", type="float", precision=10, scale=0, nullable=true, unique=false)
     */
    private $accompte;

    /**
     * @var float
     *
     * @ORM\Column(name="solde", type="float", precision=10, scale=0, nullable=true, unique=false)
     */
    private $solde;

    /**
     * @var integer
     *
     * @ORM\Column(name="salaire", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $salaire;

    /**
     * @var integer
     *
     * @ORM\Column(name="periode_essai", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $periodeEssai;
    /**
     * @var \SelectingPeopleBundle\Entity\SpDemandeRecrutement
     *
     * @ORM\ManyToOne(targetEntity="SelectingPeopleBundle\Entity\SpDemandeRecrutement")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_doublon", referencedColumnName="id", nullable=true)
     * })
     */

    private $idDoublon;

    /**
     * @var string
     *
     * @ORM\Column(name="commentaire_sp", type="string", length=500, precision=0, scale=0, nullable=true, unique=false)
     */
    private $commentaireSp;

    /**
     * @var string
     *
     * @ORM\Column(name="preference", type="string", length=500, precision=0, scale=0, nullable=true, unique=false)
     */
    private $preference;

    /**
     * @var string
     *
     * @ORM\Column(name="exclusion", type="string", length=500, precision=0, scale=0, nullable=true, unique=false)
     */
    private $exclusion;

    /**
     * @var \SelectingPeopleBundle\Entity\SpChargeRecherche
     *
     * @ORM\ManyToOne(targetEntity="SelectingPeopleBundle\Entity\SpChargeRecherche")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="charge_recherche_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $chargeRechercheId;



    /**
     * @var \Administration\SocieteBundle\Entity\Societe
     *
     * @ORM\ManyToOne(targetEntity="Administration\SocieteBundle\Entity\Societe")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ste_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $steId;

    /**
     * @var \SelectingPeopleBundle\Entity\SpPosteBridge
     *
     * @ORM\ManyToOne(targetEntity="SelectingPeopleBundle\Entity\SpPosteBridge")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="poste_bridge_id", referencedColumnName="bdg_id", nullable=true)
     * })
     */
    private $posteBridgeId;

    /**
     * @var string
     *
     * @ORM\Column(name="produit", type="string", length=500, precision=0, scale=0, nullable=true, unique=false)
     */
    private $produit;

    /**
     * @var \SelectingPeopleBundle\Entity\SpTypeContrat
     *
     * @ORM\ManyToOne(targetEntity="SelectingPeopleBundle\Entity\SpTypeContrat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="contrat_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $contratId;

    /**
     * @var \SelectingPeopleBundle\Entity\SpStatutCollaborateur
     *
     * @ORM\ManyToOne(targetEntity="SelectingPeopleBundle\Entity\SpStatutCollaborateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="statut_collaborateur_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $statutCollaborateurId;

    /**
     * @var \SelectingPeopleBundle\Entity\SpPays
     *
     * @ORM\ManyToOne(targetEntity="SelectingPeopleBundle\Entity\SpPays")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pays_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $paysId;

    /**
     * @var \SelectingPeopleBundle\Entity\SpStatutMission
     *
     * @ORM\ManyToOne(targetEntity="SelectingPeopleBundle\Entity\SpStatutMission", fetch="EAGER")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="statut_mission_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $statutMissionId;

    /**
     * @var \SelectingPeopleBundle\Entity\SpRegion
     *
     * @ORM\ManyToOne(targetEntity="SelectingPeopleBundle\Entity\SpRegion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="region_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $regionId;
	/**
     * @var \Administration\DirectionBundle\Entity\Direction
     *
     * @ORM\ManyToOne(targetEntity="Administration\DirectionBundle\Entity\Direction")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="dir_id", referencedColumnName="id")
     * })
     */
    private $dir;

    /**
     * @var \SelectingPeopleBundle\Entity\SpPoste
     *
     * @ORM\ManyToOne(targetEntity="SelectingPeopleBundle\Entity\SpPoste")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pst_id", referencedColumnName="pst_id")
     * })
     */
    private $pst;

    /**
     * @var \Administration\DirectionBundle\Entity\Service
     *
     * @ORM\ManyToOne(targetEntity="Administration\DirectionBundle\Entity\Service")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="srv_id", referencedColumnName="id")
     * })
     */
    private $srv;

    /**
     * @var \SelectingPeopleBundle\Entity\SpPole
     *
     * @ORM\ManyToOne(targetEntity="SelectingPeopleBundle\Entity\SpPole")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pol_id", referencedColumnName="pol_id")
     * })
     */
    private $pol;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return SpDemandeRecrutement
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set ville
     *
     * @param string $ville
     *
     * @return SpDemandeRecrutement
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Get confidentiel
     *
     * @return string
     */
    public function getConfidentiel()
    {
        return $this->confidentiel;
    }
    /**
     * Set confidentiel
     *
     * @param string $confidentiel
     *
     * @return SpDemandeRecrutement
     */
    public function setConfidentiel($confidentiel)
    {
        $this->confidentiel = $confidentiel;
    }



    /**
     * Set prenomDemandeur
     *
     * @param string $prenomDemandeur
     *
     * @return SpDemandeRecrutement
     */
    public function setPrenomDemandeur($prenomDemandeur)
    {
        $this->prenomDemandeur = $prenomDemandeur;

        return $this;
    }

    /**
     * Get prenomDemandeur
     *
     * @return string
     */
    public function getPrenomDemandeur()
    {
        return $this->prenomDemandeur;
    }

    /**
     * Set nomDemandeur
     *
     * @param string $nomDemandeur
     *
     * @return SpDemandeRecrutement
     */
    public function setNomDemandeur($nomDemandeur)
    {
        $this->nomDemandeur = $nomDemandeur;

        return $this;
    }

    /**
     * Get nomDemandeur
     *
     * @return string
     */
    public function getNomDemandeur()
    {
        return $this->nomDemandeur;
    }

    /**
     * Set mailDemandeur
     *
     * @param string $mailDemandeur
     *
     * @return SpDemandeRecrutement
     */
    public function setMailDemandeur($mailDemandeur)
    {
        $this->mailDemandeur = $mailDemandeur;

        return $this;
    }

    /**
     * Get mailDemandeur
     *
     * @return string
     */
    public function getMailDemandeur()
    {
        return $this->mailDemandeur;
    }

    /**
     * Set commentaire
     *
     * @param string $commentaire
     *
     * @return SpDemandeRecrutement
     */
    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    /**
     * Get commentaire
     *
     * @return string
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }

    /**
     * Set reference
     *
     * @param string $reference
     *
     * @return SpDemandeRecrutement
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set accompte
     *
     * @param float $accompte
     *
     * @return SpDemandeRecrutement
     */
    public function setAccompte($accompte)
    {
        $this->accompte = $accompte;

        return $this;
    }

    /**
     * Get accompte
     *
     * @return float
     */
    public function getAccompte()
    {
        return $this->accompte;
    }

    /**
     * Set solde
     *
     * @param float $solde
     *
     * @return SpDemandeRecrutement
     */
    public function setSolde($solde)
    {
        $this->solde = $solde;

        return $this;
    }

    /**
     * Get solde
     *
     * @return float
     */
    public function getSolde()
    {
        return $this->solde;
    }

    /**
     * Set salaire
     *
     * @param integer $salaire
     *
     * @return SpDemandeRecrutement
     */
    public function setSalaire($salaire)
    {
        $this->salaire = $salaire;

        return $this;
    }

    /**
     * Get salaire
     *
     * @return integer
     */
    public function getSalaire()
    {
        return $this->salaire;
    }

    /**
     * Set periodeEssai
     *
     * @param integer $periodeEssai
     *
     * @return SpDemandeRecrutement
     */
    public function setPeriodeEssai($periodeEssai)
    {
        $this->periodeEssai = $periodeEssai;

        return $this;
    }

    /**
     * Get periodeEssai
     *
     * @return integer
     */
    public function getPeriodeEssai()
    {
        return $this->periodeEssai;
    }

    /**
     * Set idDoublon
     *
     * @param \SelectingPeopleBundle\Entity\SpDemandeRecrutement $idDoublon
     *
     * @return SpDemandeRecrutement
     */
    public function setIdDoublon(\SelectingPeopleBundle\Entity\SpDemandeRecrutement $idDoublon=null)
    {
        $this->idDoublon = $idDoublon;

        return $this;
    }

    /**
     * Get idDoublon
     *
     * @return \SelectingPeopleBundle\Entity\SpDemandeRecrutement
     */
    public function getIdDoublon()
    {
        return $this->idDoublon;
    }

    /**
     * Set commentaireSp
     *
     * @param string $commentaireSp
     *
     * @return SpDemandeRecrutement
     */
    public function setCommentaireSp($commentaireSp)
    {
        $this->commentaireSp = $commentaireSp;

        return $this;
    }

    /**
     * Get commentaireSp
     *
     * @return string
     */
    public function getCommentaireSp()
    {
        return $this->commentaireSp;
    }

    /**
     * Set preference
     *
     * @param string $preference
     *
     * @return SpDemandeRecrutement
     */
    public function setPreference($preference)
    {
        $this->preference = $preference;

        return $this;
    }

    /**
     * Get preference
     *
     * @return string
     */
    public function getPreference()
    {
        return $this->preference;
    }

    /**
     * Set exclusion
     *
     * @param string $exclusion
     *
     * @return SpDemandeRecrutement
     */
    public function setExclusion($exclusion)
    {
        $this->exclusion = $exclusion;

        return $this;
    }

    /**
     * Get exclusion
     *
     * @return string
     */
    public function getExclusion()
    {
        return $this->exclusion;
    }
    /**
     * Set chargeRechercheId
     *
     * @param \SelectingPeopleBundle\Entity\SpChargeRecherche $chargeRechercheId
     *
     * @return SpDemandeRecrutement
     */

    public function setChargeRechercheId(\SelectingPeopleBundle\Entity\SpChargeRecherche $chargeRechercheId=null)
    {
        $this->chargeRechercheId = $chargeRechercheId;

        return $this;
    }

    /**
     * Get chargeRechercheId
     *
     * @return \SelectingPeopleBundle\Entity\SpChargeRecherche
     */
    public function getChargeRechercheId()
    {
        return $this->chargeRechercheId;
    }



    /**
     * Set steId
     *
     * @param \Administration\SocieteBundle\Entity\Societe $steId
     *
     * @return SpDemandeRecrutement
     */
    public function setSteId(\Administration\SocieteBundle\Entity\Societe $steId = null)
    {
        $this->steId = $steId;

        return $this;
    }

    /**
     * Get steId
     *
     * @return \Administration\SocieteBundle\Entity\Societe
     */
    public function getSteId()
    {
        return $this->steId;
    }

    /**
     * Set posteBridgeId
     *
     * @param \SelectingPeopleBundle\Entity\SpPosteBridge $posteBridgeId
     *
     * @return SpDemandeRecrutement
     */
    public function setPosteBridgeId(\SelectingPeopleBundle\Entity\SpPosteBridge $posteBridgeId = null)
    {
        $this->posteBridgeId = $posteBridgeId;

        return $this;
    }

    /**
     * Get posteBridgeId
     *
     * @return \SelectingPeopleBundle\Entity\SpPosteBridge
     */
    public function getPosteBridgeId()
    {
        return $this->posteBridgeId;
    }

    /**
     * @return string
     */
    public function getProduit()
    {
        return $this->produit;
    }

    /**
     * @param string $produit
     */
    public function setProduit($produit)
    {
        $this->produit = $produit;
    }



    /**
     * Set contratId
     *
     * @param \SelectingPeopleBundle\Entity\SpTypeContrat $contratId
     *
     * @return SpDemandeRecrutement
     */
    public function setContratId(\SelectingPeopleBundle\Entity\SpTypeContrat $contratId = null)
    {
        $this->contratId = $contratId;

        return $this;
    }

    /**
     * Get contratId
     *
     * @return \SelectingPeopleBundle\Entity\SpTypeContrat
     */
    public function getContratId()
    {
        return $this->contratId;
    }

    /**
     * Set statutCollaborateurId
     *
     * @param \SelectingPeopleBundle\Entity\SpStatutCollaborateur $statutCollaborateurId
     *
     * @return SpDemandeRecrutement
     */
    public function setStatutCollaborateurId(\SelectingPeopleBundle\Entity\SpStatutCollaborateur $statutCollaborateurId = null)
    {
        $this->statutCollaborateurId = $statutCollaborateurId;

        return $this;
    }

    /**
     * Get statutCollaborateurId
     *
     * @return \SelectingPeopleBundle\Entity\SpStatutCollaborateur
     */
    public function getStatutCollaborateurId()
    {
        return $this->statutCollaborateurId;
    }

    /**
     * Set paysId
     *
     * @param \SelectingPeopleBundle\Entity\SpPays $paysId
     *
     * @return SpDemandeRecrutement
     */
    public function setPaysId(\SelectingPeopleBundle\Entity\SpPays $paysId = null)
    {
        $this->paysId = $paysId;

        return $this;
    }

    /**
     * Get paysId
     *
     * @return \SelectingPeopleBundle\Entity\SpPays
     */
    public function getPaysId()
    {
        return $this->paysId;
    }

    /**
     * Set statutMissionId
     *
     * @param \SelectingPeopleBundle\Entity\SpStatutMission $statutMissionId
     *
     * @return SpDemandeRecrutement
     */
    public function setStatutMissionId(\SelectingPeopleBundle\Entity\SpStatutMission $statutMissionId = null)
    {
        $this->statutMissionId = $statutMissionId;

        return $this;
    }

    /**
     * Get statutMissionId
     *
     * @return \SelectingPeopleBundle\Entity\SpStatutMission
     */
    public function getStatutMissionId()
    {
        return $this->statutMissionId;
    }

    /**
     * Set regionId
     *
     * @param \SelectingPeopleBundle\Entity\SpRegion $regionId
     *
     * @return SpDemandeRecrutement
     */
    public function setRegionId(\SelectingPeopleBundle\Entity\SpRegion $regionId = null)
    {
        $this->regionId = $regionId;

        return $this;
    }

    /**
     * Get regionId
     *
     * @return \SelectingPeopleBundle\Entity\SpRegion
     */
    public function getRegionId()
    {
        return $this->regionId;
    }

    /**
     * Set dir
     *
     * @param \Administration\DirectionBundle\Entity\Direction $dir
     *
     * @return PosteBridge
     */
    public function setDir(\Administration\DirectionBundle\Entity\Direction $dir = null)
    {
        $this->dir = $dir;

        return $this;
    }

    /**
     * Get dir
     *
     * @return \Administration\DirectionBundle\Entity\Direction
     */
    public function getDir()
    {
        return $this->dir;
    }

    /**
     * Set pst
     *
     * @param \SelectingPeopleBundle\Entity\SpPoste $pst
     *
     * @return PosteBridge
     */
    public function setPst(\SelectingPeopleBundle\Entity\SpPoste $pst = null)
    {
        $this->pst = $pst;

        return $this;
    }

    /**
     * Get pst
     *
     * @return \SelectingPeopleBundle\Entity\SpPoste
     */
    public function getPst()
    {
        return $this->pst;
    }

    /**
     * Set srv
     *
     * @param \Administration\DirectionBundle\Entity\Service $srv
     *
     * @return PosteBridge
     */
    public function setSrv(\Administration\DirectionBundle\Entity\Service $srv = null)
    {
        $this->srv = $srv;

        return $this;
    }

    /**
     * Get srv
     *
     * @return \Administration\DirectionBundle\Entity\Service
     */
    public function getSrv()
    {
        return $this->srv;
    }

    /**
     * Set pol
     *
     * @param \SelectingPeopleBundle\Entity\SpPole $pol
     *
     * @return PosteBridge
     */
    public function setPol(\SelectingPeopleBundle\Entity\SpPole $pol = null)
    {
        $this->pol = $pol;

        return $this;
    }

    /**
     * Get pol
     *
     * @return \SelectingPeopleBundle\Entity\SpPole
     */
    public function getPol()
    {
        return $this->pol;
    }

}

