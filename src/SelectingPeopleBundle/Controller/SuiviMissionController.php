<?php

namespace SelectingPeopleBundle\Controller;


use SelectingPeopleBundle\Entity\SpAffectationDemande;
use SelectingPeopleBundle\Entity\SpRecrutement;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use SelectingPeopleBundle\Form\SpDemandeRecrutementType;
use SelectingPeopleBundle\Form\SpAffectationDemandeType;
use SelectingPeopleBundle\Form\SpRecrutementType;
use SelectingPeopleBundle\Entity\SpDemandeRecrutement;
use Symfony\Component\HttpFoundation\Response;
use User\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\Session\Session;
use SelectingPeopleBundle\Entity\SpCabinetRecrutement;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;
use Swift_Validate;

/**
 * SuiviMission controller.
 *
 * @Route("/spsuivimission")
 */

class SuiviMissionController extends Controller
{

    /**
     * @Route("/suivie/{id}", name="suivie")
     */
    public function suivieAction ($id)
    {

        $session = $this->getRequest()->getSession();
        $dmd= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpDemandeRecrutement")-> findBy(array('id'=>$id));
        $affectation= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpAffectationDemande")-> findOneBy(array('dmdId'=>$id),
            array('id' => 'DESC'));
        $rec_g= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpRecrutement")-> findOneBy(array('dmdId'=>$id, 'numero'=>'1'),
            array('id' => 'DESC'));

            if ($this->getRequest()->isMethod('POST')) {
                $donnee=$_POST;
                $modifSal=$this->changeSalaire($id, $donnee['selectingpeoplebundle_sprecrutement']['salaire']);
                if ($modifSal=="soldemodifie"){
                    if(!$session->has('info')) $session->set('info','Un mail a été envoyé à la compta');
                }
                $statut_origine=$dmd[0]->getStatutMissionId();
                $statut = $this->getDoctrine()->getRepository('SelectingPeopleBundle:SpStatutMission')->findBy(array('id' => $donnee['selectingpeoplebundle_sprecrutement']['statutt']));
                $dmd[0]->setStatutMissionId($statut[0]);
                $dmd[0]->setReference($donnee['selectingpeoplebundle_sprecrutement']['reference']);
                $em = $this->getDoctrine()->getManager();
                $em->persist($dmd[0]);
                $em->flush();

                if (($dmd[0]->getStatutMissionId()->getId()==7)||($dmd[0]->getStatutMissionId()->getId()==9)){
                    if($dmd[0]->getIdDoublon()==null){
                        $dmd2 = new SpDemandeRecrutement();
                        $time = new \DateTime();
                        $dmd2->setDate($time);
                        $dmd2->setNomDemandeur($dmd[0]->getNomDemandeur());
                        $dmd2->setPrenomDemandeur($dmd[0]->getPrenomDemandeur());
                        $dmd2->setMailDemandeur($dmd[0]->getMailDemandeur());
                        $dmd2->setPreference($dmd[0]->getPreference());
                        $dmd2->setExclusion($dmd[0]->getExclusion());
                        $dmd2->setPosteBridgeId($dmd[0]->getPosteBridgeId());
                        $dmd2->setConfidentiel($dmd[0]->getConfidentiel());
                        $dmd2->setSteId($dmd[0]->getSteId());
                        $dmd2->setProduit($dmd[0]->getProduit());
                        $dmd2->setContratId($dmd[0]->getContratId());
                        $dmd2->setStatutCollaborateurId($dmd[0]->getStatutCollaborateurId());
                        $dmd2->setPeriodeEssai($dmd[0]->getPeriodeEssai());
                        $dmd2->setPaysId($dmd[0]->getPaysId());
                        if ($dmd[0]->getPaysId()->getNomPays()=='France') {
                            $dmd2->setRegionId($dmd[0]->getRegionId());
                        }
                        if($dmd[0]->getStatutMissionId()->getId()==9){
                            $dmd2->setRegionId($dmd[0]->getReference());
                        }
                        $dmd2->setVille($dmd[0]->getVille());
                        $dmd2->setCommentaire($dmd[0]->getCommentaire());
                        $st = $this->getDoctrine()->getRepository('SelectingPeopleBundle:SpStatutMission')->findBy(array('id' => $donnee['selectingpeoplebundle_sprecrutement']['statutt']));
                        $dmd2->setStatutMissionId($st[0]);
                        $dmd[0]->setStatutMissionId($statut_origine);
                        $dmd2->setIdDoublon($dmd[0]);
                        $dmd2->setSalaire($dmd[0]->getSalaire());
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($dmd2);
                        $em->flush();
                        if(!$session->has('success')) $session->set('success','La mission a bien été dupliquée');
                        if(!$session->has('info')) $session->set('info','vous êtes sur la fiche de la copie dupliquée que vous venez de créer');
                    }
                    if(($dmd[0]->getIdDoublon()!=null)&&($dmd[0]->getStatutMissionId()->getId()==7)){
                        $rec1= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpRecrutement")-> findOneBy(array('dmdId'=>$id, 'numero'=>'1'));
                        if(($donnee['selectingpeoplebundle_sprecrutement']['nom_candidat']!='')&&($donnee['selectingpeoplebundle_sprecrutement']['prenom_candidat']!='')){
                            if ($rec1==null){
                                $rec1 = new SpRecrutement();
                                $rec1->setDmdId($dmd[0]);
                            }
                            $rec1->setDateRecrutement(new \DateTime($donnee['selectingpeoplebundle_sprecrutement']['dateRecrutement']));
                            $rec1->setDateIntegration(new \DateTime($donnee['selectingpeoplebundle_sprecrutement']['dateIntegration']));
                            $rec1->setNomCandidat($donnee['selectingpeoplebundle_sprecrutement']['nom_candidat']);
                            $rec1->setPrenomCandidat($donnee['selectingpeoplebundle_sprecrutement']['prenom_candidat']);
                            $rec1->setMail($donnee['selectingpeoplebundle_sprecrutement']['email_candidat']);
                            $rec1->setEnPoste($donnee['selectingpeoplebundle_sprecrutement']['enPoste']);
                            $rec1->setNumero('1');
                            $affectation->setPremierRecruteur(1);
                            $em->persist($affectation);
                            $em->flush();
                            $statut = $this->getDoctrine()->getRepository('SelectingPeopleBundle:SpStatutMission')->findBy(array('id' => '4'));
                            $dmd[0]->setStatutMissionId($statut[0]);
                            $em = $this->getDoctrine()->getManager();
                            $em->persist($rec1);
                            $em->flush();
                            if(!$session->has('success')) $session->set('success','Le recrutement a été modifié avec succes');
                            $lastAffectation= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpAffectationDemande")-> findOneBy(array('dmdId'=>$id),
                                array('id' => 'DESC'));
                            $sql = "SELECT aff.date_affectation FROM sp_affectation_demande aff, sp_charge_recrutement ch
                WHERE aff.aff_sous_traitance = 1 
				AND aff.id_recruteur=ch.id
				AND dmd_id= ".$id."
				and ch.user_id NOT IN (SELECT u.id FROM user u WHERE u.roles LIKE '%EUROPEEN%')
				LIMIT 0,1 ;";//Europeen

                            $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
                            $query->execute();
                            $result = $query->fetchAll();
                            if ($result==null){
                                $europeen=1;
                            }
                            else{
                                $europeen=0;
                            }
                            if(($europeen==0)&&($lastAffectation->getAffSousTraitance()==1)){
                                //on enregistre le montant de la facture
                                $this->changeSolde($id);
                            }
                            if(!$session->has('success')) $session->set('success','La modification du recrutement a été modifié avec succes.');
                        }
                        else{
                            if(!$session->has('error')) $session->set('error','La modification du recrutement a échoué.');
                        }
                    }

                    $recruit=$this->getDoctrine()->getRepository("SelectingPeopleBundle:SpRecrutement")-> findBy(array('dmdId'=>$dmd2->getId()));
                    $affect= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpAffectationDemande")-> findOneBy(array('dmdId'=>$dmd2->getId()),
                        array('id' => 'DESC'));

                    return $this->redirect($this->generateUrl('suivie', array('id'=>$dmd2->getId())));
                }
/******************/ if ($dmd[0]->getStatutMissionId()->getId()==10){
                    $rec2= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpRecrutement")-> findOneBy(array('dmdId'=>$id, 'numero'=>'2'));
                    if(($donnee['selectingpeoplebundle_sprecrutement']['nom_candidat2']!='')&&($donnee['selectingpeoplebundle_sprecrutement']['prenom_candidat2']!='')){
                        if($rec2==null){
                            $rec2 = new SpRecrutement();
                            $rec2->setDmdId($dmd[0]);
                        }
                        $rec2->setDateRecrutement(new \DateTime($donnee['selectingpeoplebundle_sprecrutement']['dateRecrutement2']));
                        $rec2->setDateIntegration(new \DateTime($donnee['selectingpeoplebundle_sprecrutement']['dateIntegration2']));
                        $rec2->setNomCandidat($donnee['selectingpeoplebundle_sprecrutement']['nom_candidat2']);
                        $rec2->setPrenomCandidat($donnee['selectingpeoplebundle_sprecrutement']['prenom_candidat2']);
                        $rec2->setMail($donnee['selectingpeoplebundle_sprecrutement']['email_candidat2']);
                        $rec2->setEnPoste($donnee['selectingpeoplebundle_sprecrutement']['en_poste2']);
                        $rec2->setNumero('2');
                        $statut = $this->getDoctrine()->getRepository('SelectingPeopleBundle:SpStatutMission')->findBy(array('id' => '6'));
                        $dmd[0]->setStatutMissionId($statut[0]);
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($rec2);
                        $em->flush();
                        if(!$session->has('success')) $session->set('success','Le recrutement a été modifié avec succes.');
                    }
                    else{
                        if(!$session->has('error')) $session->set('error','la modification du recrutement a échoué.');

                    }

                }
/******************/  if (($dmd[0]->getStatutMissionId()->getId()==5)&&($rec_g->getGarantie()=='Oui')&&($rec_g->getPeriodeConfirmee()=='Non')){

                    $rec2= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpRecrutement")-> findOneBy(array('dmdId'=>$id, 'numero'=>'2'));
                    if(($donnee['selectingpeoplebundle_sprecrutement']['nom_candidat2']!='')&&($donnee['selectingpeoplebundle_sprecrutement']['prenom_candidat2']!='')){
                        if($rec2==null){
                            $rec2 = new SpRecrutement();
                            $rec2->setDmdId($dmd[0]);
                        }
                        $rec2->setDateRecrutement(new \DateTime($donnee['selectingpeoplebundle_sprecrutement']['dateRecrutement2']));
                        $rec2->setDateIntegration(new \DateTime($donnee['selectingpeoplebundle_sprecrutement']['dateIntegration2']));
                        $rec2->setNomCandidat($donnee['selectingpeoplebundle_sprecrutement']['nom_candidat2']);
                        $rec2->setPrenomCandidat($donnee['selectingpeoplebundle_sprecrutement']['prenom_candidat2']);
                        $rec2->setMail($donnee['selectingpeoplebundle_sprecrutement']['email_candidat2']);
                        $rec2->setEnPoste($donnee['selectingpeoplebundle_sprecrutement']['en_poste2']);
                        $rec2->setNumero('2');
                        $statut = $this->getDoctrine()->getRepository('SelectingPeopleBundle:SpStatutMission')->findBy(array('id' => '6'));
                        $dmd[0]->setStatutMissionId($statut[0]);
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($rec2);
                        $em->flush();
                        if(!$session->has('success')) $session->set('success','Le recrutement a été modifié avec succes.');
                    }
                    else{
                        if(!$session->has('error')) $session->set('error','la modification du recrutement a échoué.');
                    }

                }
                if(($dmd[0]->getStatutMissionId()->getId()!=$statut_origine->getId())&&($dmd[0]->getStatutMissionId()->getId()!=7)&&($dmd[0]->getStatutMissionId()->getId()!=9)){
                    //Si le statut passe de recruté à en cours
                    if(($dmd[0]->getStatutMissionId()->getId()==1)&&($statut_origine->getId()==4)){
                        //on supprime le premier recrutement
                        $rec1= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpRecrutement")-> findOneBy(array('dmdId'=>$id, 'numero'=>'1'));
                        $em = $this->getDoctrine()->getManager();
                        $em->remove($rec1);
                        $em->flush($rec1);
                        $time = new \DateTime();
                        $dateCurrent=$time->format('Y-m-d');
                        $moisCurrent=substr($dateCurrent,5,2);
                        $yearCurrent=substr($dateCurrent,0,4);
                        if ( ((substr($_POST ["selectingpeoplebundle_sprecrutement"]['dateIntegration'],0,4) < $yearCurrent) or ((substr($_POST ["selectingpeoplebundle_sprecrutement"]['dateIntegration'],0,4) == $yearCurrent) && substr($_POST ["selectingpeoplebundle_sprecrutement"]['dateIntegration'],5,2) < $moisCurrent)) && ($dmd[0]->getSolde()>0) ){
                            $this->mailannulsolde($id);
                            if(!$session->has('info')) $session->set('info','Un mail a été envoyé à la compta.');
                        }
                        $dmd[0]->setSolde(0); //remettre le solde à zéro
                    }
                    // quand l'utilisateur passe de statut garantie terminée au statut garantie en cours
                    if (($dmd[0]->getStatutMissionId()->getId()==5)&&($statut_origine->getId()==6)){
                        //on supprime le deuxième recrutement
                        $rec2= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpRecrutement")-> findOneBy(array('dmdId'=>$id, 'numero'=>'2'));
                        $em = $this->getDoctrine()->getManager();
                        $em->remove($rec2);
                        $em->flush($rec2);
                    }
                    if (($donnee['selectingpeoplebundle_sprecrutement']['statutt'])>0){
                        if(!$session->has('success')) $session->set('success','Le statut de la mission a été modifié avec succès.');
                    } else {
                        if(!$session->has('error')) $session->set('error','la modification du statut échoué. Veuillez recommencer, ou contacter l\'informatique si le problème persiste.');
                    }

                        }
                if (($dmd[0]->getStatutMissionId()->getId()==4)&&($statut_origine->getId()==4)){
                    $rec1= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpRecrutement")-> findOneBy(array('dmdId'=>$id, 'numero'=>'1'));
                    if(($donnee['selectingpeoplebundle_sprecrutement']['nom_candidat']!='')&&($donnee['selectingpeoplebundle_sprecrutement']['prenom_candidat']!='')){
                        if ($rec1==null){
                            $rec1 = new SpRecrutement();
                            $rec1->setDmdId($dmd[0]);
                            if(!$session->has('success')) $session->set('success','Le recrutement a été enregistré avec succès.');
                        }
                        else {
                            if(!$session->has('success')) $session->set('success','Le recrutement a été mis à jour.');
                        }
                        $rec1->setDateRecrutement(new \DateTime($donnee['selectingpeoplebundle_sprecrutement']['dateRecrutement']));
                        $rec1->setDateIntegration(new \DateTime($donnee['selectingpeoplebundle_sprecrutement']['dateIntegration']));
                        $rec1->setNomCandidat($donnee['selectingpeoplebundle_sprecrutement']['nom_candidat']);
                        $rec1->setPrenomCandidat($donnee['selectingpeoplebundle_sprecrutement']['prenom_candidat']);
                        $rec1->setMail($donnee['selectingpeoplebundle_sprecrutement']['email_candidat']);
                        $rec1->setEnPoste($donnee['selectingpeoplebundle_sprecrutement']['enPoste']);
                        $rec1->setPeriodeConfirmee($donnee['selectingpeoplebundle_sprecrutement']['periodeConfirmee']);
                        $rec1->setGarantie($donnee['selectingpeoplebundle_sprecrutement']['garantie']);
                        $rec1->setDateRelance(new \DateTime($donnee['selectingpeoplebundle_sprecrutement']['dateRelance']));
                        if(($donnee['selectingpeoplebundle_sprecrutement']['periodeConfirmee']=='Non')&&($donnee['selectingpeoplebundle_sprecrutement']['garantie']=='Oui')){
                            $statut = $this->getDoctrine()->getRepository('SelectingPeopleBundle:SpStatutMission')->findBy(array('id' => '5'));
                            $dmd[0]->setStatutMissionId($statut[0]);
                            $ref=$dmd[0]->getReference();
                            $str = substr_replace($ref,'6',11,1); // Si le statut passe à garantie en cours on remplace le dernier numéro de la référence par un 6
                            $dmd[0]->setReference($str);
                            $this->garantieDemandeur($id);
                            if(!$session->has('success')) $session->set('success','Le demandeur a été informé de cette affectation.');
                             // Envoi du mail destiné à la Roumanie, pour une demande de référence SAP
                            $this->garantieRoumanie($id);
                            if(!$session->has('success')) $session->set('success','Un mail a été envoyé pour une modification de référence dans SAP.');



                        }
                        $rec1->setNumero('1');
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($rec1);
                        $em->flush();
                    }
                    else{
                        if(!$session->has('error')) $session->set('error','L\'enregistrement du recrutement a échoué.');
                    }

                }


                if(($dmd[0]->getStatutMissionId()->getId()==1)&&($statut_origine->getId()==1)){ //premier recrutement
                    if(($donnee['selectingpeoplebundle_sprecrutement']['nom_candidat']!='')&&($donnee['selectingpeoplebundle_sprecrutement']['prenom_candidat']!='')){
                        $rec = new SpRecrutement();
                        $rec->setDmdId($dmd[0]);
                        $rec->setDateRecrutement(new \DateTime($donnee['selectingpeoplebundle_sprecrutement']['dateRecrutement']));
                        $rec->setDateIntegration(new \DateTime($donnee['selectingpeoplebundle_sprecrutement']['dateIntegration']));
                        $rec->setNomCandidat($donnee['selectingpeoplebundle_sprecrutement']['nom_candidat']);
                        $rec->setPrenomCandidat($donnee['selectingpeoplebundle_sprecrutement']['prenom_candidat']);
                        $rec->setMail($donnee['selectingpeoplebundle_sprecrutement']['email_candidat']);
                        $rec->setEnPoste($donnee['selectingpeoplebundle_sprecrutement']['enPoste']);
                        $rec->setNumero('1');
                        $affectation->setPremierRecruteur(1);
                        $salaire=$dmd[0]->getSalaire();
                        $solde=(($salaire*11)/100)*0.5;
                        $dmd[0]->setSolde($solde);
                        $em->persist($affectation);
                        $em->flush();
                        $statut = $this->getDoctrine()->getRepository('SelectingPeopleBundle:SpStatutMission')->findBy(array('id' => '4'));
                        $dmd[0]->setStatutMissionId($statut[0]);
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($rec);
                        $em->flush();
                        if(!$session->has('success')) $session->set('success','Le recrutement a été enregistré avec succès.');
                    } else{
                        if(!$session->has('error')) $session->set('error','L\'enregistrement du recrutement a échoué.');
                    }


                }
                if(($dmd[0]->getIdDoublon()!=null)&&($dmd[0]->getStatutMissionId()->getId()==4)) { // si un doublon est recruté on annule la demande originale et ses autres doublons
                    $em = $this->getDoctrine()->getManager();
                    $dmd_org = $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpDemandeRecrutement")->findOneBy(array('id' => $dmd[0]->getIdDoublon()->getId()));
                    $statut = $this->getDoctrine()->getRepository('SelectingPeopleBundle:SpStatutMission')->findBy(array('id' => '3'));
                    $dmd_org->setStatutMissionId($statut[0]);// annulation de la demande origine
                    $em->persist($dmd_org);
                    $em->flush();
                    $sql = "SELECT id, id_doublon, statut_mission_id
				FROM sp_demande_recrutement
				WHERE id_doublon =" . $dmd[0]->getIdDoublon()->getId() . "
				AND statut_mission_id <> 4;";
                    //annulation des autres doublons
                    $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
                    $query->execute();
                    $result = $query->fetchAll();
                    $statut = $this->getDoctrine()->getRepository('SelectingPeopleBundle:SpStatutMission')->findBy(array('id' => '3'));
                    //$t = array();
                    //$i = 0;
                    foreach ($result as $q) {
                        $t = $q['id'];
                        $dmd_filles = $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpDemandeRecrutement")->findOneBy(array('id' => $t));

                        $dmd_filles->setStatutMissionId($statut[0]);
                        $em->persist($dmd_filles);
                        $em->flush();
                    }
                }
                    //si la demande d'origine est recruté ou bien annulée on annule tous les demandes filles
                    if(($dmd[0]->getStatutMissionId()->getId()==4)&&($dmd[0]->getIdDoublon()==null)){
                        $statut = $this->getDoctrine()->getRepository('SelectingPeopleBundle:SpStatutMission')->findBy(array('id' => '3'));
                        $dmd_filles= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpDemandeRecrutement")-> findBy(array('idDoublon'=>$dmd[0]->getId()));
                        $i=0;
                        if($dmd_filles!=null){
                            foreach ($dmd_filles as $fille){
                                $fille->setStatutMissionId($statut[0]);
                                $em->persist($fille);
                                $em->flush();
                            }
                        }
                    }
                    if(($dmd[0]->getStatutMissionId()->getId()==3)&&($dmd[0]->getIdDoublon()==null)){
                        $statut = $this->getDoctrine()->getRepository('SelectingPeopleBundle:SpStatutMission')->findBy(array('id' => '3'));
                        $dmd_filles= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpDemandeRecrutement")-> findBy(array('idDoublon'=>$dmd[0]->getId()));
                        if($dmd_filles!=null){
                            foreach ($dmd_filles as $fille){
                                $fille->setStatutMissionId($statut[0]);
                                $em->persist($fille);
                                $em->flush();
                            }
                        }
                    }
                $em->persist($dmd[0]);
                $em->flush();

            }


        $recruitement1=$this->getDoctrine()->getRepository("SelectingPeopleBundle:SpRecrutement")-> findOneBy(array('dmdId'=>$id, 'numero'=>'1'));
        $recruitement2=$this->getDoctrine()->getRepository("SelectingPeopleBundle:SpRecrutement")-> findOneBy(array('dmdId'=>$id, 'numero'=>'2'));
        $affectation= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpAffectationDemande")-> findOneBy(array('dmdId'=>$id),
            array('id' => 'DESC'));
        $form = $this->createForm(new SpDemandeRecrutementType());
        $form1 = $this->createForm(new SpAffectationDemandeType(),$affectation);
        $form2 = $this->createForm(new SpRecrutementType(),$recruitement1);
        return $this->render ('SelectingPeopleBundle:Default:suivi_mission.html.twig',
            array('dmd'=> $dmd[0],'aff'=>$affectation,'rec1'=>$recruitement1, 'rec2'=>$recruitement2,'form' => $form->createView(),
                'form1' => $form1->createView(),'form2' => $form2->createView()));
    }

    /**
     * @Route("/changesolde/{id}", name="changesolde")
     */

    public function changeSolde($id){
        $dmd= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpDemandeRecrutement")-> findBy(array('id'=>$id));
        $montant = (11*$dmd[0]->getSalaire())/100;
        $accompte=$dmd[0]->getAccompte();
        if($accompte==null){
            if($dmd[0]->getIdDoublon()!=null){
                $dmd_org= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpDemandeRecrutement")-> findBy(array('id'=>$dmd[0]->getIdDoublon()));
                if($dmd_org[0]->getAccompte()!=0){
                    $accompte=$dmd_org[0]->getAccompte();
                }
                else{
                    $dmd_filles= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpDemandeRecrutement")-> findBy(array('idDoublon'=>$dmd_org[0]->getId()));
                    foreach ($dmd_filles as $dmd_fille){
                        if($dmd_fille->getAccompte()!=0){
                            $accompte=$dmd_fille->getAccompte();
                        }
                    }
                }
            }
            $dmd_filles= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpDemandeRecrutement")-> findBy(array('idDoublon'=>$id));
            foreach ($dmd_filles as $dmd_fille){
                if($dmd_fille->getAccompte()!=0){
                    $accompte=$dmd_fille->getAccompte();
                }
            }

        }
        $dmd[0]->setSolde($montant - $accompte);
    }
    /**
     * @Route("/changesalaire/{id}", name="changesalaire")
     */

    public function changeSalaire($id, $nouveauSal){
        $dmd= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpDemandeRecrutement")-> findBy(array('id'=>$id));
        $dmd[0]->setSalaire($nouveauSal);
        // on cherche sur quelle offre a été calculé le montant de l'accompte
        $dmd_a_facturer=$dmd[0];


        if($dmd[0]->getIdDoublon()!=null){
            $dmd_org= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpDemandeRecrutement")-> findBy(array('id'=>$dmd[0]->getIdDoublon()));
            if($dmd_org[0]->getAccompte()!=0){
                $dmd_a_facturer=$dmd_org[0];
            }
            $dmd_filles= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpDemandeRecrutement")-> findBy(array('idDoublon'=>$dmd_org[0]->getId()));
            foreach ($dmd_filles as $dmd_fille){
                if ($dmd_fille->getAccompte()!=0){
                    $dmd_a_facturer=$dmd_fille;
                }
            }
        }
        // On cherche dans les demandes filles
        $dmd_filles= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpDemandeRecrutement")-> findBy(array('idDoublon'=>$id));
        foreach ($dmd_filles as $dmd_fille){
            if($dmd_fille->getAccompte()!=0){
                $dmd_a_facturer=$dmd_fille;
            }
        }

        $dateAccompte=$this->getDateAccompte($dmd_a_facturer->getId());
        $moisAccompte=substr($dateAccompte,0,7);
        $time = new \DateTime();
        $dateCurrent=$time->format('Y-m-d');
        $moisCurrent=substr($dateCurrent,0,7);
        // si l'accompte a été calculé un autre mois que le mois courant (ou s'il n'a jamais été calculé)
        // on ne le recalcule pas
        if(($moisAccompte== $moisCurrent)&&($dmd[0]->getSolde()!=0)){
			var_dump();die();
            $sal=0.5*(($nouveauSal*11)/100);
            $dmd_a_facturer->setAccompte($sal);
        }
        //on vérifie si le solde doit être ajusté
        $rec1=$this->getDoctrine()->getRepository("SelectingPeopleBundle:SpRecrutement")-> findOneBy(array('dmdId'=>$id, 'numero'=>1));
        if($rec1!=null){
            $dateFacturation=$rec1->getDateIntegration()->format('Y-m-d'); // c'est la date solde
        }
        else{
            $dateFacturation='';
        }
        /* pour que le solde soit ajusté il faut :
		 qu'il ne soit pas égal à 0
		 qu'on soit AVANT le mois suivant la date d'intégration (date de facturation)
		 */
        $moisFacturation=substr($dateFacturation,0,7);
        if(($moisFacturation==$moisCurrent)&&($dmd[0]->getSolde()!=0)){
            $ancienSolde=$dmd[0]->getSolde();
            $this->changeSolde($dmd[0]->getId());
            $nouvSolde=$dmd[0]->getSolde();
            $this->mailmodifsolde($id, $ancienSolde, $nouvSolde);
            return "soldemodifie";
        }
        else{
            return "soldenonmodifie";
        }
    }

    /**
     * @Route("/dateaccompte/{id}", name="dateaccompte")
     */

    public function getDateAccompte($id){
        $sql = "SELECT aff.date_affectation FROM sp_affectation_demande aff, sp_charge_recrutement ch
                WHERE aff.aff_sous_traitance = 1 
				AND aff.id_recruteur=ch.id
				AND dmd_id= ".$id."
				and ch.user_id NOT IN (SELECT u.id FROM user u WHERE u.roles LIKE '%EUROPEEN%')
				LIMIT 0,1 ;";//Europeen

        $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
        $query->execute();
        $result = $query->fetchAll();
        if($result!=null){
            return($result[0] ["date_affectation"]);
        }else{
            return(null);
        }
    }


    /**
     * @Route("/mailmodifsolde/{id}", name="mailmodifsolde")
     */

    public function mailmodifsolde($id, $ancien_solde, $nouveau_solde){
        $dmd= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpDemandeRecrutement")-> findBy(array('id'=>$id));
        $msg  = "Bonjour,\n ";
        $msg .= "Le salaire de la personne recrutée pour l'annonce numero ".$dmd[0]->getId()." (REF:".$dmd[0]->getReference().") a été modifié.\n";
        $msg .= "Le solde est passé de ".$ancien_solde." euros à ".$nouveau_solde." euros.\n";
        $msg .= "merci de faire un avoir à la société ".$dmd[0]->getSteId()->getLibelle().".\n";
        $msg .= "Merci d'avance pour votre action.";
        if($this->container->get('kernel')->getEnvironment()!='prod'){
            $message = \Swift_Message::newInstance()
                ->setSubject('TEST: Modification solde')
                ->setFrom('selectingpeoplerecruitement@gmail.com')
                ->addTo('poleweb@noz.fr') // mail compta
                ->addBcc('poleweb@noz.fr')
                ->setBody($msg."\nCECI EST UN MAIL DE TEST, MERCI DE NE PAS EN TENIR COMPTE");
        }else{
            $message = \Swift_Message::newInstance()
                ->setSubject('Modification solde')
                ->setFrom('selectingpeoplerecruitement@gmail.com')
                ->addTo('poleweb@noz.fr') // mail compta
                ->addBcc('poleweb@noz.fr')
                ->setBody($msg);
        }
        $mailer = $this->get('mailer');
        $mailer->send($message);
        $spool = $mailer->getTransport()->getSpool();
        $transport = $this->get('swiftmailer.transport.real');
        $spool->flushQueue($transport);

    }
    /**
     * @Route("/mailannulsolde/{id}", name="mailannulsolde")
     */

    public function mailannulsolde($id){
        $dmd= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpDemandeRecrutement")-> findBy(array('id'=>$id));
        $msg  = "Bonjour,\n";
        $msg .= "Le recrutement de l'annonce numero ".$dmd[0]->getId()." (REF:".$dmd[0]->getReference().") a été annulé.\n";
        $msg .= "merci de faire un avoir de ".$dmd[0]->getSolde()." à la société ".$dmd[0]->getSteId()->getLibelle().".\n";
        $msg .= "Merci d'avance pour votre action.";
        if($this->container->get('kernel')->getEnvironment()!='prod'){
            $message = \Swift_Message::newInstance()
                ->setSubject('TEST: Compta annulation solde')
                ->setFrom('selectingpeoplerecruitement@gmail.com')
                ->addTo('poleweb@noz.fr') // mail compta
                ->addBcc('poleweb@noz.fr')
                ->setBody($msg."\nCECI EST UN MAIL DE TEST, MERCI DE NE PAS EN TENIR COMPTE");
        }else{
            $message = \Swift_Message::newInstance()
                ->setSubject('Compta annulation solde')
                ->setFrom('selectingpeoplerecruitement@gmail.com')
                ->addTo('poleweb@noz.fr') // mail compta
                ->addBcc('poleweb@noz.fr')
                ->setBody($msg);
        }

        $mailer = $this->get('mailer');
        $mailer->send($message);
        $spool = $mailer->getTransport()->getSpool();
        $transport = $this->get('swiftmailer.transport.real');
        $spool->flushQueue($transport);
    }
    /**
     * @Route("/mailannulaccompte/{id}", name="mailannulaccompte")
     */

    public function mailannulaccompte($dmd){
        $msg  = "Bonjour,\n";
        $msg .= "L'acompte pour l'annonce  numero ".$dmd->getId()." (REF:".$dmd->getReference().") a été annulé.\n";
        $msg .= "merci de faire un avoir de ".$dmd->getAccompte()." € à la société ".$dmd->getSteId()->getLibelle().".\n";
        $msg .= "Merci d'avance pour votre action.";
        if($this->container->get('kernel')->getEnvironment()!='prod'){
            $message = \Swift_Message::newInstance()
                ->setSubject('TEST: annulation accompte')
                ->setFrom('selectingpeoplerecruitement@gmail.com')
                ->addTo('poleweb@noz.fr') // mail compta
                ->addBcc('poleweb@noz.fr')
                ->setBody($msg."\nCECI EST UN MAIL DE TEST, MERCI DE NE PAS EN TENIR COMPTE");
        }else{
            $message = \Swift_Message::newInstance()
                ->setSubject('annulation accompte')
                ->setFrom('selectingpeoplerecruitement@gmail.com')
                ->addTo('poleweb@noz.fr') // mail compta
                ->addBcc('poleweb@noz.fr')
                ->setBody($msg);
        }
        $mailer = $this->get('mailer');
        $mailer->send($message);
        $spool = $mailer->getTransport()->getSpool();
        $transport = $this->get('swiftmailer.transport.real');
        $spool->flushQueue($transport);
    }

    /**
     * @Route("/mail_garantie_demandeur/{id}", name="mail_garantie_demandeur")
     */

    public function garantieDemandeur($id){
        $dmd= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpDemandeRecrutement")-> findBy(array('id'=>$id));
        $aff= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpAffectationDemande")-> findOneBy(array('dmdId'=>$id, 'premierRecruteur'=>'1'));

        if($aff->getAffSousTraitance()==1){
            $recruteur=$aff->getIdRecruteur()->getUserId()->getEmail();
            $recruteur_name=$aff->getIdRecruteur()->getUserId()->getDisplayName();
        }
        else{
            $recruteur=$aff->getIdCabinet()->getMailCabinet();
            $recruteur_name=$aff->getIdCabinet()->getNomCabinet();
        }
        $msg  = "Bonjour,\n";
        $msg .= "Votre demande de recrutement pour un ".$dmd[0]->getPosteBridgeId()->getPst()->getPstLibelle()." à ".$dmd[0]->getVille()." vient de passer EN GARANTIE.\n";
        $msg .= "Cette mission est confiée à ".$recruteur_name;
        $msg .= " qui vous contactera dans les meilleurs délais pour en établir les paramètres.\n";
        $msg .= "Nous sommes assurés que le processus de recrutement se déroulera à votre entière satisfaction.\n";
        $msg .= "L’équipe Selecting People.";
        if(Swift_Validate::email($recruteur)){
            if($this->container->get('kernel')->getEnvironment()!='prod'){
                $message = \Swift_Message::newInstance()
                    ->setSubject('TEST: Suivi de votre demande '.$dmd[0]->getPosteBridgeId()->getPst()->getPstLibelle())
                    ->setFrom('selectingpeoplerecruitement@gmail.com')
                    ->addTo($dmd[0]->getMailDemandeur())
                    ->addCc($recruteur)
                    ->addBcc('coslobanu@veo-finances.com')//Email RH
                    ->addBcc('poleweb@noz.fr')
                    ->setBody($msg."\nCECI EST UN MAIL DE TEST, MERCI DE NE PAS EN TENIR COMPTE");
            }else{
                $message = \Swift_Message::newInstance()
                    ->setSubject('Suivi de votre demande '.$dmd[0]->getPosteBridgeId()->getPst()->getPstLibelle())
                    ->setFrom('selectingpeoplerecruitement@gmail.com')
                    ->addTo($dmd[0]->getMailDemandeur())
                    ->addCc($recruteur)
                    ->addBcc('coslobanu@veo-finances.com')//Email RH
                    ->addBcc('poleweb@noz.fr')
                    ->setBody($msg);
            }

            $mailer = $this->get('mailer');
            $mailer->send($message);
            $spool = $mailer->getTransport()->getSpool();
            $transport = $this->get('swiftmailer.transport.real');
            $spool->flushQueue($transport);
        }
        else{
            if($this->container->get('kernel')->getEnvironment()!='prod'){
                $message = \Swift_Message::newInstance()
                    ->setSubject('TEST: Suivi de votre demande '.$dmd[0]->getPosteBridgeId()->getPst()->getPstLibelle())
                    ->setFrom('selectingpeoplerecruitement@gmail.com')
                    ->addTo($dmd[0]->getMailDemandeur())
                    ->addBcc('coslobanu@veo-finances.com')//Email RH
                    ->addBcc('poleweb@noz.fr')
                    ->setBody($msg."\nCECI EST UN MAIL DE TEST, MERCI DE NE PAS EN TENIR COMPTE");
            }else{
                $message = \Swift_Message::newInstance()
                    ->setSubject('Suivi de votre demande '.$dmd[0]->getPosteBridgeId()->getPst()->getPstLibelle())
                    ->setFrom('selectingpeoplerecruitement@gmail.com')
                    ->addTo($dmd[0]->getMailDemandeur())
                    ->addBcc('coslobanu@veo-finances.com')//Email RH
                    ->addBcc('poleweb@noz.fr')
                    ->setBody($msg);
            }
            $mailer = $this->get('mailer');
            $mailer->send($message);
            $spool = $mailer->getTransport()->getSpool();
            $transport = $this->get('swiftmailer.transport.real');
            $spool->flushQueue($transport);
        }

    }
    /**
     * @Route("/mail_garantie_roumanie/{id}", name="mail_garantie_roumanie")
     */

    public function garantieRoumanie($id){
        $dmd= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpDemandeRecrutement")-> findBy(array('id'=>$id));
        $aff= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpAffectationDemande")-> findOneBy(array('dmdId'=>$id),
            array('id' => 'DESC'));

        if($aff->getAffSousTraitance()==1){
            $recruteur_name=$aff->getIdRecruteur()->getUserId()->getDisplayName();
        }
        else{
            $recruteur_name=$aff->getIdCabinet()->getNomCabinet();
        }
        $msg  = "Bonjour,\n";
        $msg .= "Merci de nous modifier le numéro de référence dans SAP : ".$dmd[0]->getReference().".\n";
        $msg .= "Cette mission de recherche d'un ".$dmd[0]->getPosteBridgeId()->getPst()->getPstLibelle()." et attribuée à :".$recruteur_name;
        $msg .= " est passée en garantie.\n";
        $msg .= "Merci d'avance pour votre action.";
        if($this->container->get('kernel')->getEnvironment()!='prod'){
            $message = \Swift_Message::newInstance()
                ->setSubject('TEST: Modification du numéro de référence ')
                ->setFrom('selectingpeoplerecruitement@gmail.com')
                ->addTo('coslobanu@veo-finances.com')
                ->addTo('srusu@veo-finances.com')
                ->addTo('lsoula@veo-finances.com')
                ->addTo('cpadurariu@veo-finances.com')
                ->addTo('atudosa@veo-finances.com')
                ->addTo('mhutuliac@veo-finances.com')
                ->addTo('rnichifor@veo-finances.com')
                ->addBcc('poleweb@noz.fr')
                ->setBody($msg."\nCECI EST UN MAIL DE TEST, MERCI DE NE PAS EN TENIR COMPTE");
        }else{
            $message = \Swift_Message::newInstance()
                ->setSubject('Modification du numéro de référence ')
                ->setFrom('selectingpeoplerecruitement@gmail.com')
                ->addTo('coslobanu@veo-finances.com')
                ->addTo('srusu@veo-finances.com')
                ->addTo('lsoula@veo-finances.com')
                ->addTo('cpadurariu@veo-finances.com')
                ->addTo('atudosa@veo-finances.com')
                ->addTo('mhutuliac@veo-finances.com')
                ->addTo('rnichifor@veo-finances.com')
                ->addBcc('poleweb@noz.fr')
                ->setBody($msg);
        }
            $mailer = $this->get('mailer');
            $mailer->send($message);
            $spool = $mailer->getTransport()->getSpool();
            $transport = $this->get('swiftmailer.transport.real');
            $spool->flushQueue($transport);

    }
    /**
     * @Route("/mail_charge_recherche/{id}", name="mail_charge_recherche")
     */

    public function mailChargeRecherche($dmd, $mail_charge){
        $msg  = "Bonjour,\n";
        $msg .= "La mission pour le poste de ".$dmd->getPosteBridgeId()->getPst()->getPstLibelle()." à ".$dmd->getVille()." est confiée à ".$dmd->getChargeRechercheId()->getUserId()->getDisplayName();
        $msg .= ", qui prendra son brief dans les meilleurs délais.\n";
        $msg .= "Bonnes recherches !";
        if(Swift_Validate::email($mail_charge)){
            if($this->container->get('kernel')->getEnvironment()!='prod'){
                $message = \Swift_Message::newInstance()
                    ->setSubject('TEST: Affectation à un chargé de recherche')
                    ->setFrom('selectingpeoplerecruitement@gmail.com')
                    ->addTo($mail_charge)
                    ->addBcc('poleweb@noz.fr')
                    ->addCc($dmd->getChargeRechercheId()->getUserId()->getEmail())
                    ->setBody($msg."\nCECI EST UN MAIL DE TEST, MERCI DE NE PAS EN TENIR COMPTE");
                $mailer = $this->get('mailer');
                $mailer->send($message);
                $spool = $mailer->getTransport()->getSpool();
                $transport = $this->get('swiftmailer.transport.real');
                $spool->flushQueue($transport);
            }else{
                $message = \Swift_Message::newInstance()
                    ->setSubject('Affectation à un chargé de recherche')
                    ->setFrom('selectingpeoplerecruitement@gmail.com')
                    ->addTo($mail_charge)
                    ->addBcc('poleweb@noz.fr')
                    ->addCc($dmd->getChargeRechercheId()->getUserId()->getEmail())
                    ->setBody($msg);
                $mailer = $this->get('mailer');
                $mailer->send($message);
                $spool = $mailer->getTransport()->getSpool();
                $transport = $this->get('swiftmailer.transport.real');
                $spool->flushQueue($transport);
            }
        }
        else{
            if($this->container->get('kernel')->getEnvironment()!='prod'){
                $message = \Swift_Message::newInstance()
                    ->setSubject('TEST: Affectation à un chargé de recherche')
                    ->setFrom('selectingpeoplerecruitement@gmail.com')
                    ->addTo($dmd->getChargeRechercheId()->getUserId()->getEmail())
                    ->addBcc('poleweb@noz.fr')
                    ->setBody($msg."\nCECI EST UN MAIL DE TEST, MERCI DE NE PAS EN TENIR COMPTE");
            }else{
                $message = \Swift_Message::newInstance()
                    ->setSubject('Affectation à un chargé de recherche')
                    ->setFrom('selectingpeoplerecruitement@gmail.com')
                    ->addTo($dmd->getChargeRechercheId()->getUserId()->getEmail())
                    ->addBcc('poleweb@noz.fr')
                    ->setBody($msg);
            }
            $mailer = $this->get('mailer');
            $mailer->send($message);
            $spool = $mailer->getTransport()->getSpool();
            $transport = $this->get('swiftmailer.transport.real');
            $spool->flushQueue($transport);
        }

    }
    /**
     * @Route("/mail_affectation_roumanie/{id}", name="mail_affectation_roumanie")
     */

    public function mailAffectationRoumanie($dmd, $recruteur_name){
        $msg  = "Bonjour,\n";
        $msg .= "Merci de nous faire parvenir un numéro de référence pour la mission suivante :\n";
        $msg .= "Poste : ".$dmd->getPosteBridgeId()->getPst()->getPstLibelle().".\n";
        $msg .= "Demandeur : ".$dmd->getPrenomDemandeur()." ".$dmd->getNomDemandeur().".\n";
        $msg .= "Secteur Géographique : ".$dmd->getPaysId()->getNomPays();
        if ($dmd->getRegionId()!= null) {
            $msg .= " - ".$dmd->getRegionId()->getLibelle();
        }
        $msg .= " - ".$dmd->getVille().".\n";
        $msg .= "Date de la demande : ".$dmd->getDate()->format('Y-m-d').".\n";
        $msg .= "Confidentiel : ".$dmd->getConfidentiel().".\n";
        $msg .= "Cette mission est confiée à ".$recruteur_name."</b>.";
        $msg .= "Merci d'avance pour votre action.";
        if($this->container->get('kernel')->getEnvironment()!='prod'){
            $message = \Swift_Message::newInstance()
                ->setSubject('TEST: Demande du numéro de référence')
                ->setFrom('selectingpeoplerecruitement@gmail.com')
                ->addTo('coslobanu@veo-finances.com')
                ->addTo('srusu@veo-finances.com')
                ->addTo('lsoula@veo-finances.com')
                ->addTo('cpadurariu@veo-finances.com')
                ->addTo('atudosa@veo-finances.com')
                ->addTo('mhutuliac@veo-finances.com')
                ->addTo('rnichifor@veo-finances.com')
                ->addBcc('poleweb@noz.fr')
                ->setBody($msg."\nCECI EST UN MAIL DE TEST, MERCI DE NE PAS EN TENIR COMPTE");
        }else{
            $message = \Swift_Message::newInstance()
                ->setSubject('Demande du numéro de référence')
                ->setFrom('selectingpeoplerecruitement@gmail.com')
                ->addTo('coslobanu@veo-finances.com')
                ->addTo('srusu@veo-finances.com')
                ->addTo('lsoula@veo-finances.com')
                ->addTo('cpadurariu@veo-finances.com')
                ->addTo('atudosa@veo-finances.com')
                ->addTo('mhutuliac@veo-finances.com')
                ->addTo('rnichifor@veo-finances.com')
                ->addBcc('poleweb@noz.fr')
                ->setBody($msg);
        }
        $mailer = $this->get('mailer');
        $mailer->send($message);
        $spool = $mailer->getTransport()->getSpool();
        $transport = $this->get('swiftmailer.transport.real');
        $spool->flushQueue($transport);
    }
    /**
     * @Route("/affectation_demandeur/{id}", name="affectation_demandeur")
     */

    public function affectationDemandeur($dmd, $recruteur_mail, $recruteur_name){
        $msg  = "Bonjour,\n";
        $msg .= "Votre demande de recrutement pour le poste de ".$dmd->getPosteBridgeId()->getPst()->getPstLibelle()." à ".$dmd->getVille()." ";
        $msg .= "a été confiée à ".$recruteur_name;
        $msg .= ", qui vous contactera dans les meilleurs délais pour en établir les paramètres.\n";
        $msg .= "Merci de votre confiance,\n";
        $msg .= "L'équipe Selecting People.";
        if(Swift_Validate::email($recruteur_mail)){
            $message = \Swift_Message::newInstance()
                ->setSubject('Suivi de votre demande - '.$dmd->getPosteBridgeId()->getPst()->getPstLibelle())
                ->setFrom('selectingpeoplerecruitement@gmail.com')
                ->addTo($dmd->getMailDemandeur())
                ->addCc($recruteur_mail)
                ->setBody($msg);
            $mailer = $this->get('mailer');
            $mailer->send($message);
            $spool = $mailer->getTransport()->getSpool();
            $transport = $this->get('swiftmailer.transport.real');
            $spool->flushQueue($transport);
        }
        else{
            $message = \Swift_Message::newInstance()
                ->setSubject('Suivi de votre demande - '.$dmd->getPosteBridgeId()->getPst()->getPstLibelle())
                ->setFrom('selectingpeoplerecruitement@gmail.com')
                ->setTo($dmd->getMailDemandeur())
                ->setBody($msg);
            $mailer = $this->get('mailer');
            $mailer->send($message);
            $spool = $mailer->getTransport()->getSpool();
            $transport = $this->get('swiftmailer.transport.real');
            $spool->flushQueue($transport);
        }

    }




    /**
     * @Route("/affect/{id}", name="affect")
     */
    public function affecterMissionAction ($id)
    {
        $session = $this->getRequest()->getSession();
        $dmd= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpDemandeRecrutement")-> findBy(array('id'=>$id));
        if ($this->getRequest()->isMethod('POST')) {
            $donnee = $_POST;
            //var_dump($donnee);die;
            $aff = new SpAffectationDemande();
            $aff->setDmdId($dmd[0]);
            $dmd[0]->setSalaire($donnee['selectingpeoplebundle_spaffectationdemande']['sal_hidden']);
            $time = new \DateTime();
            $aff->setDateAffectation($time);
            $aff->setPremierRecruteur(0);
            if ($donnee['selectingpeoplebundle_spaffectationdemande']['aff_sous_traitance']==0){
                $cabinet = $this->getDoctrine()->getRepository('SelectingPeopleBundle:SpCabinetRecrutement')->findBy(array('id' => $donnee['selectingpeoplebundle_spaffectationdemande']['idCabinet']));
                $aff->setIdCabinet($cabinet[0]);
                $aff->setAffSousTraitance('0');
                if ($donnee['selectingpeoplebundle_spaffectationdemande']['charge_recherche'] != -1) {
                    $charge_rech = $this->getDoctrine()->getRepository('SelectingPeopleBundle:SpChargeRecherche')->findBy(array('id' => $donnee['selectingpeoplebundle_spaffectationdemande']['charge_recherche']));
                    $dmd[0]->setChargeRechercheId($charge_rech[0]);
                    $this->mailChargeRecherche($dmd[0], $cabinet[0]->getMailCabinet());
                    if (!$session->has('info')) $session->set('info', 'Un mail a été envoyé aux chargés de recrutement et de recherche.');
                }
            } elseif ($donnee['selectingpeoplebundle_spaffectationdemande']['aff_sous_traitance']==1) {
                $charge_rec = $this->getDoctrine()->getRepository('SelectingPeopleBundle:SpChargeRecrutement')->findBy(array('id' => $donnee['selectingpeoplebundle_spaffectationdemande']['idRecruteur']));
                $aff->setIdRecruteur($charge_rec[0]);
                $aff->setAffSousTraitance('1');
                $time = new \DateTime();
                $aff->setDateAffectation($time);
                if ($donnee['selectingpeoplebundle_spaffectationdemande']['charge_recherche'] != -1) {
                    $charge_rech = $this->getDoctrine()->getRepository('SelectingPeopleBundle:SpChargeRecherche')->findBy(array('id' => $donnee['selectingpeoplebundle_spaffectationdemande']['charge_recherche']));
                    $dmd[0]->setChargeRechercheId($charge_rech[0]);
                    $this->mailChargeRecherche($dmd[0], $charge_rec[0]->getUserId()->getEmail());
                    if (!$session->has('info')) $session->set('info', 'Un mail a été envoyé aux chargés de recrutement et de recherche.');
                }
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($aff);
            $em->flush();
            if (($dmd[0]->getStatutMissionId()->getId() == 5) || ($dmd[0]->getStatutMissionId()->getId() == 10)) {
                $statut = $this->getDoctrine()->getRepository('SelectingPeopleBundle:SpStatutMission')->findBy(array('id' => '10'));
                $dmd[0]->setStatutMissionId($statut[0]);
            }
            // si c'est la première affectation et que la demande n'est pas en sourcing,
            // on envoie un mail à la roumanie pour demander la référence
            if ($aff->getAffSousTraitance() == 1) {
                $recruteur_name = $aff->getIdRecruteur()->getUserId()->getDisplayname();
                $recruteur_mail = $aff->getIdRecruteur()->getUserId()->getEmail();
            } else {
                $recruteur_name = $aff->getIdCabinet()->getNomCabinet();
                $recruteur_mail = $aff->getIdCabinet()->getMailCabinet();
            }
            $affectation_nbr = $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpAffectationDemande")->findBy(array('dmdId' => $id));
            if ((count($affectation_nbr) == 1) && ($dmd[0]->getStatutMissionId()->getId() != 9)) {
                $this->mailAffectationRoumanie($dmd[0], $recruteur_name);
                if (!$session->has('success')) $session->set('success', 'Un mail a été envoyé pour une demande de référence SAP.');
            }
            if ((count($affectation_nbr) == 1) && ($dmd[0]->getStatutMissionId()->getId() != 7) && ($dmd[0]->getStatutMissionId()->getId() != 9)) {
                $statut = $this->getDoctrine()->getRepository('SelectingPeopleBundle:SpStatutMission')->findBy(array('id' => '1'));
                $dmd[0]->setStatutMissionId($statut[0]);
                $this->affectationDemandeur($dmd[0], $recruteur_mail, $recruteur_name);
                if (!$session->has('success')) $session->set('success', 'Le demandeur a été informé de cette affectation.');
            }
            // Si la demande l'affectation est sous traitée ou bien le recruteur est europeen on ne calcule pas le solde
            $sql = "SELECT aff.date_affectation FROM sp_affectation_demande aff, sp_charge_recrutement ch
                WHERE aff.aff_sous_traitance = 1 
				AND aff.id_recruteur=ch.id
				AND dmd_id= ".$id."
				and ch.user_id NOT IN (SELECT u.id FROM user u WHERE u.roles LIKE '%EUROPEEN%')
				LIMIT 0,1 ;";//Europeen

            $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll();
            if ($result==null){
                $europeen=1;
            }
            else{
                $europeen=0;
            }
            if(($europeen==1)||($aff->getAffSousTraitance()==0)){
                $dmd[0]->setSolde(0);
            }
            /* on calcul l'acompte si :
            l'affectation a bien été enregistrée
            la mission n'a pas été sous traité
            la mission n'a pas été confiée à un chargé de recrutement européen
            la mission n'est pas en garantie
            */
            if(($aff->getAffSousTraitance()==1)&&($europeen==0)&&(($dmd[0]->getStatutMissionId()->getId()<5)||($dmd[0]->getStatutMissionId()->getId()==7))){
                $accompte_calcule=false;
                //si la mission est un doublon, on récupère la demande "mère"
                if($dmd[0]->getIdDoublon()!=null){
                    $dmd_org=$this->getDoctrine()->getRepository("SelectingPeopleBundle:SpDemandeRecrutement")->findOneBy(array('id' => $dmd[0]->getIdDoublon()->getId()));
                    if($dmd_org->getAccompte()!=0){
                        $accompte_calcule=true;
                    }
                    else{
                        //si l'accompte n'a pas été calculé pour la demande originale, on regarde s'il a été calaculé pour l'un des autres doublons de la demande originale.
                        $dmd_filles= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpDemandeRecrutement")-> findBy(array('idDoublon'=>$dmd_org->getId()));
                        foreach ($dmd_filles as $dmd_fille){
                            if($dmd_fille->getAccompte()!=0){
                                $accompte_calcule=true;
                            }
                        }
                    }
                }
                if($accompte_calcule==false){
                    // si on a pas trouvé d'accompte dans la demande mère, ni dans les demandes filles de cette dernière
                    //On regarde si la demande n'a pas de demande fille et si l'accompte a été calculé dans l'une d'elle
                    $dmd_filles= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpDemandeRecrutement")-> findBy(array('idDoublon'=>$dmd[0]->getId()));
                    foreach ($dmd_filles as $dmd_fille){
                        if ($dmd_fille->getAccompte()!=0){
                            $accompte_calcule=true;
                        }
                    }
                }
                if($accompte_calcule==false){
                    $sal=$dmd[0]->getSalaire();
                    $accompte=0.5*((11*$sal)/100);
                    $dmd[0]->setAccompte($accompte);
                }
            }
            if( $donnee["selectingpeoplebundle_spaffectationdemande"]["accompte"]=="zero"){
                $dateAccompte=$this->getDateAccompte($dmd[0]->getId());
                $moisAccompte=substr($dateAccompte,0,7);
                $time = new \DateTime();
                $dateCurrent=$time->format('Y-m-d');
                $moisCurrent=substr($dateCurrent,0,7);
                if($moisAccompte!= $moisCurrent){
                    $this->mailannulaccompte($dmd[0]);
                    if (!$session->has('info')) $session->set('info', 'Un mail a été envoyé à la compta.');
                }
                $dmd[0]->setAccompte(0);
                if (!$session->has('success')) $session->set('success', 'l\'acompte a été mis à zéro.');
                $sql = "DELETE aff FROM sp_affectation_demande aff, sp_charge_recrutement ch
                WHERE `dmd_id` =".$id." 
                AND aff.aff_sous_traitance = 1 
                AND aff.id_recruteur=ch.id
                AND ch.user_id NOT IN (SELECT u.id FROM user u WHERE u.roles LIKE '%EUROPEEN%');";
                $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
                $query->execute();

            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($dmd[0]);
            $em->flush();
        }
        $recruitement1=$this->getDoctrine()->getRepository("SelectingPeopleBundle:SpRecrutement")-> findOneBy(array('dmdId'=>$id, 'numero'=>'1'));
        $recruitement2=$this->getDoctrine()->getRepository("SelectingPeopleBundle:SpRecrutement")-> findOneBy(array('dmdId'=>$id, 'numero'=>'2'));
        $affectation= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpAffectationDemande")-> findOneBy(array('dmdId'=>$id),
            array('id' => 'DESC'));
        $form = $this->createForm(new SpDemandeRecrutementType());
        $form1 = $this->createForm(new SpAffectationDemandeType(), $affectation);
        $form2 = $this->createForm(new SpRecrutementType(),$recruitement1);
        return $this->render ('SelectingPeopleBundle:Default:suivi_mission.html.twig',
            array('dmd'=> $dmd[0],'aff'=> $affectation,'rec1'=>$recruitement1, 'rec2'=>$recruitement2,'form' => $form->createView(),
                'form1' => $form1->createView(),'form2' => $form2->createView()));
    }


    /**
     * @Route("/chargerec/{id}", name="chargerec")
     */

    public function chargeRecAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $result = array();
        $charges = $em->getRepository('SelectingPeopleBundle:SpChargeRecrutement')->findAll();
        $affectation= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpAffectationDemande")-> findOneBy(array('dmdId'=>$id),
            array('id' => 'DESC'));
        $dmd= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpDemandeRecrutement")-> findOneBy(array('id'=>$id));
        $pst=$dmd->getPosteBridgeId()->getPst()->getPstLibelle();
        if ($affectation!=null){
        if(($affectation->getAffSousTraitance()!= null)&&($affectation->getAffSousTraitance()== 1)){
            $r=$affectation->getIdRecruteur()->getId();
        }else {$r=0;}
        }
        else{
            $r=0;
        }
        $i = 0;
        foreach ($charges as $charge) {
            $result[$i]['id'] = $charge->getId();
            $result[$i]['nom_charge'] = $charge->getUserId()->getDisplayName();
            $i++;
        }
        $output = '<option value="-1" ></option>' ;


        $j=0;
        foreach ($result as $row) {
            if($result[$j]['id']== $r){
                $output.= '<option value="'.$row['id'].'" selected>'.$row['nom_charge'].'</option>';
                $j++;
            }
            else{
                $output.= '<option value="'.$row['id'].'">'.$row['nom_charge'].'</option>';
                $j++;
            }
        }
        $return = $output;
        $response = new Response();
        $data = json_encode($return);
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent($data);
        return $response;
    }
    /**
     * @Route("/chargerech/{id}", name="chargerech")
     */

    public function chargeRechAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();
        $result = array();
        $charges = $em->getRepository('SelectingPeopleBundle:SpChargeRecherche')->findAll();
        $dmd= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpDemandeRecrutement")-> findBy(array('id'=>$id));
        if ($dmd[0]->getChargeRechercheId()!= null){
            $rech= $dmd[0]->getChargeRechercheId()->getId();
        }
        else{$rech=0;}

        $i = 0;
        foreach ($charges as $charge) {
            $result[$i]['id'] = $charge->getId();
            $result[$i]['nom_charge'] = $charge->getUserId()->getDisplayName();
            $i++;
        }
        $output = '<option value="-1"> </option>';
        $j=0;
        foreach ($result as $row) {
            if($result[$j]['id']== $rech){
                $output.= '<option value="'.$row['id'].'" selected>'.$row['nom_charge'].'</option>';
                $j++;
            }
            else {
                $output .= '<option value="' . $row['id'] . '">' . $row['nom_charge'] . '</option>';
                $j++;
            }
        }
        $return = $output;
        $response = new Response();
        $data = json_encode($return);
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent($data);
        return $response;
    }


    /**
     * @Route("/directions/{id}", name="dir")
     */

    public function directionAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();
        $result = array();
        $directions = $em->getRepository('AdministrationDirectionBundle:Direction')->findAll();
        $dmd= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpDemandeRecrutement")-> findBy(array('id'=>$id));
        $d= $dmd[0]->getPosteBridgeId()->getDir()->getId();
        $i = 0;
        foreach ($directions as $direction) {
            $result[$i]['id'] = $direction->getId();
            $result[$i]['libelle'] = $direction->getLibelle();
            $i++;
        }
        $output = '<option value="" >------------------ Directions ----------------</option>' ;
        $j=0;
        foreach ($result as $row) {
            if($result[$j]['id']== $d){
                $output.= '<option value="'.$row['id'].'" selected>'.$row['libelle'].'</option>';
                $j++;
            }
            else{
                $output.= '<option value="'.$row['id'].'">'.$row['libelle'].'</option>';
                $j++;
            }
        }
        $return = $output;
        $response = new Response();
        $data = json_encode($return);
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent($data);
        return $response;
    }

    /**
     * @Route("/ser/{id}", name="ser")
     */

    public function serviceAction($id)
    {
        $request = $this->getRequest();

        $em = $this->getDoctrine()->getManager();
        if ($request->isXmlHttpRequest()) {
            $direction = $request->get('direction');
            $result = array();
            $services = $em->getRepository('AdministrationDirectionBundle:Service')->findBy(array('direction' => $direction));
            $dmd= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpDemandeRecrutement")-> findBy(array('id'=>$id));
            $s=$dmd[0]->getPosteBridgeId()->getSrv()->getId();
            $i = 0;
            foreach ($services as $service) {
                $result[$i]['id'] = $service->getId();
                $result[$i]['libelle'] = $service->getLibelle();
                $i++;
            }
            $j=0; $output='';
            if (count($result) >= 1) {
                $output = '<option  value=""> --------------------- Services ---------------------- </option>';
                foreach ($result as $row) {
                if ($result[$j]['id'] == $s) {
                    $output .= '<option value="' . $row['id'] . '" selected>' . $row['libelle'] . '</option>';
                    $j++;
                } else {
                    $output .= '<option value="' . $row['id'] . '">' . $row['libelle'] . '</option>';
                    $j++;
                }}}
                $return = $output;
                $response = new Response();
                $data = json_encode($return);
                $response->headers->set('Content-Type', 'application/json');
                $response->setContent($data);
                return $response;
        }
        return new Response('Erreur');
    }
    /**
     * @Route("/polelist/{id}", name="polelist")
     */
    public function poleAction($id)
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
            $serv = $request->get('service');
            $result = array();
            $poles = $em->getRepository('SelectingPeopleBundle:SpPole')->findBy(array('srv' => $serv));
            $dmd= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpDemandeRecrutement")-> findBy(array('id'=>$id));
            if($p=$dmd[0]->getPosteBridgeId()->getPol()!= null){
                $p=$dmd[0]->getPosteBridgeId()->getPol()->getPolId();
            }
            else{
                $p=0;
            }
            $i = 0;
            foreach ($poles as $pole) {
                $result[$i]['id'] = $pole->getPolId();
                $result[$i]['libelle'] = $pole->getPolLibelle();
                $i++;
            }
            $j=0;
                $output = '<option  value=""> --------------------- Pôles ---------------------- </option>';
        if (count($result) > 0) {
            foreach ($result as $row) {
                if ($result[$j]['id'] == $p) {
                    $output .= '<option value="' . $row['id'] . '" selected>' . $row['libelle'] . '</option>';
                    $j++;
                } else {
                    $output .= '<option value="' . $row['id'] . '">' . $row['libelle'] . '</option>';
                    $j++;
                }}
            $return = $output;
            $response = new Response();
            $data = json_encode($return);
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent($data);
            return $response;
        }

        return new Response('Erreur');}
    /**
     * @Route("/poste/{id}", name="poste")
     */

    public function posteAction($id)
    {
        $request = $this->getRequest();
        $dmd= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpDemandeRecrutement")-> findBy(array('id'=>$id));
        $p= $dmd[0]->getPosteBridgeId()->getPst()->getPstId();
        $output = '<option  value=""> ---------------------- Postes ---------------------- </option>';
            $idBridge = $request->get('idBridge');
            if ($idBridge[1] != '') {
                if ($idBridge[2] == 'NULL') {
                    $lineSql = " AND pol_id IS NULL ";
                } else {
                    $lineSql = " AND pol_id = " . $idBridge[2];
                }

                $sql = "SELECT pst_id, pst_libelle
				FROM sp_poste
				WHERE pst_id IN (
					SELECT pst_id
					FROM sp_poste_bridge
					WHERE dir_id =" . $idBridge[0] . "
					AND srv_id =" . $idBridge[1] .
                    $lineSql . "
				)
				ORDER BY pst_libelle;
			";
                $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
                $query->execute();
                $result = $query->fetchAll();
                $j=0;
                if (count($result) >= 0) {
                    foreach ($result as $row) {
                        if ($result[$j]['pst_id'] == $p) {
                            $output .= '<option value="' . $row['pst_id'] . '" selected>' . $row['pst_libelle'] . '</option>';
                            $j++;
                        } else {
                            $output .= '<option value="' . $row['pst_id'] . '">' . $row['pst_libelle'] . '</option>';
                            $j++;
                        }}}
                }
        $return = $output;
        $response = new Response();
        $data = json_encode($return);
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent($data);
        return $response;
    }

    /**
     * @Route("/en_poste2/{id}", name="en_poste2")
     */

    public function enPoste2Action($id)
    {
        $request = $this->getRequest();
        $recrutement= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpRecrutement")->findOneBy(array('dmdId'=>$id, 'numero'=>'2'),
            array('id' => 'DESC'));
        if ($recrutement!= null){
            $r= $recrutement->getEnPoste();
        }
        else{
            $r='Non';
        }
        $output = '';
        if($r=='Oui'){
            $output.= '<option value="Oui" selected>Oui</option><option value="Non">Non</option>';
        }
        else {
            $output.= '<option value="Oui">Oui</option><option value="Non" selected>Non</option>';
        }
        $return = $output;
        $response = new Response();
        $data = json_encode($return);
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent($data);
        return $response;
    }

    /**
     * @Route("/modification_dmd/{id}", name="modification_dmd")
     */

    public function modificationDmdAction($id, Request $request)
    {
        $session = $this->getRequest()->getSession();
        $dmd=$this->getDoctrine()->getRepository('SelectingPeopleBundle:SpDemandeRecrutement')->findBy(array('id'=>$id));
        $recruitement1=$this->getDoctrine()->getRepository("SelectingPeopleBundle:SpRecrutement")-> findOneBy(array('dmdId'=>$id, 'numero'=>'1'));
        $recruitement2=$this->getDoctrine()->getRepository("SelectingPeopleBundle:SpRecrutement")-> findOneBy(array('dmdId'=>$id, 'numero'=>'2'));
        $affectation= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpAffectationDemande")-> findOneBy(array('dmdId'=>$id),
            array('id' => 'DESC'));
        $form = $this->createForm(new SpDemandeRecrutementType());
        $form->handleRequest($request);
        $form1 = $this->createForm(new SpAffectationDemandeType(), $affectation);
        $form2 = $this->createForm(new SpRecrutementType(), $recruitement1);
        if ($this->getRequest()->isMethod('POST')) {
            $donnee=$_POST;
            $dir=$this->getDoctrine()->getRepository('AdministrationDirectionBundle:Direction')->findBy(array('id'=>$donnee['selectingpeoplebundle_spdemanderecrutement']['dir']));
            $ser=$this->getDoctrine()->getRepository('AdministrationDirectionBundle:Service')->findBy(array('id'=>$donnee['selectingpeoplebundle_spdemanderecrutement']['srv']));
            $pst=$this->getDoctrine()->getRepository('SelectingPeopleBundle:SpPoste')->findBy(array('pstId'=>$donnee['selectingpeoplebundle_spdemanderecrutement']['pst']));
            $dmd[0]->setDir($dir[0]);
            $dmd[0]->setSrv($ser[0]);
            $dmd[0]->setPst($pst[0]);
            if (isset($donnee['selectingpeoplebundle_spdemanderecrutement']['pol'])){
                $pol=$this->getDoctrine()->getRepository('SelectingPeopleBundle:SpPole')->findBy(array('polId'=>$donnee['selectingpeoplebundle_spdemanderecrutement']['pol']));
                $bdg=$this->getDoctrine()->getRepository('SelectingPeopleBundle:SpPosteBridge')->findBy(array('dir'=>$dir[0], 'pst'=>$pst, 'srv'=>$ser[0], 'pol'=>$pol[0]));
                $dmd[0]->setPol($pol[0]);
            }
            else{
                $bdg=$this->getDoctrine()->getRepository('SelectingPeopleBundle:SpPosteBridge')->findBy(array('dir'=>$dir[0], 'pst'=>$pst, 'srv'=>$ser[0], 'pol'=>null));
                $dmd[0]->setPol(null);

            }
            $dmd[0]->setPosteBridgeId($bdg[0]);
            $dmd[0]->setConfidentiel($donnee['selectingpeoplebundle_spdemanderecrutement']['confidentiel']);
            $ste=$this->getDoctrine()->getRepository('AdministrationSocieteBundle:Societe')->findBy(array('id'=>$donnee['selectingpeoplebundle_spdemanderecrutement']['steId']));
            $dmd[0]->setSteId($ste[0]);
            $dmd[0]->setReference($donnee['selectingpeoplebundle_spdemanderecrutement']['reference']);
            $ctr=$this->getDoctrine()->getRepository('SelectingPeopleBundle:SpTypeContrat')->findBy(array('id'=>$donnee['selectingpeoplebundle_spdemanderecrutement']['contratId']));
            $dmd[0]->setContratId($ctr[0]);
            $stt=$this->getDoctrine()->getRepository('SelectingPeopleBundle:SpStatutCollaborateur')->findBy(array('id'=>$donnee['selectingpeoplebundle_spdemanderecrutement']['statutCollaborateurId']));
            $dmd[0]->setStatutCollaborateurId($stt[0]);
            if ($donnee['selectingpeoplebundle_spdemanderecrutement']['salaire']!= $dmd[0]->getSalaire()){
                $modifSal=$this->changeSalaire($id, $donnee['selectingpeoplebundle_spdemanderecrutement']['salaire']);
                if ($modifSal=="soldenonmodifie"){
                    if(!$session->has('warning')) $session->set('warning','La date de modification du solde à expiré. Penser à appeller la compta pour un avoir');
                }
            }

            $dmd[0]->setPeriodeEssai($donnee['selectingpeoplebundle_spdemanderecrutement']['periodeEssai']);
            $dmd[0]->setCommentaireSp($donnee['selectingpeoplebundle_spdemanderecrutement']['commentaireSp']);
            $pys=$this->getDoctrine()->getRepository('SelectingPeopleBundle:SpPays')->findBy(array('id'=>$donnee['selectingpeoplebundle_spdemanderecrutement']['paysId']));
            $dmd[0]->setPaysId($pys[0]);
            $dmd[0]->setVille($donnee['selectingpeoplebundle_spdemanderecrutement']['ville']);
            if($donnee['selectingpeoplebundle_spdemanderecrutement']['produit']!=null){
                $dmd[0]->setProduit($donnee['selectingpeoplebundle_spdemanderecrutement']['produit']);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($dmd[0]);
            $em->flush();
            return $this->render ('SelectingPeopleBundle:Default:suivi_mission.html.twig',
                array('dmd'=> $dmd[0],'aff'=>$affectation,'rec1'=>$recruitement1,'rec2'=>$recruitement2,'form' => $form->createView(),
                    'form1' => $form1->createView(),'form2' => $form2->createView()));
        }
        return $this->render ('SelectingPeopleBundle:Default:suivi_mission.html.twig',
            array('dmd'=> $dmd[0],'aff'=>$affectation,'rec1'=>$recruitement1, 'rec2'=>$recruitement2,'form' => $form->createView(),
                'form1' => $form1->createView(),'form2' => $form2->createView()));
    }
}
