<?php

namespace SelectingPeopleBundle\Controller;

use SelectingPeopleBundle\Entity\SpPoste;
use SelectingPeopleBundle\Entity\SpPosteBridge;
use SelectingPeopleBundle\Form\SpPosteType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use SelectingPeopleBundle\Entity\SpDemandeRecrutement;
use Administration\DirectionBundle\Entity\Direction;
use SelectingPeopleBundle\Form\SpDemandeRecrutementType;
use Symfony\Component\HttpFoundation\Response;
use User\UserBundle\Entity\User;
use Administration\DirectionBundle\Entity\Service;
use Swift_Validate;



/**
 * SpDemandeRecrutement controller.
 *
 * @Route("/spdemanderecrutement")
 */
class SpDemandeRecrutementController extends Controller
{
    /**
     * @Route("/", name="dmd")
     */

    public function successAction(Request $request){
        $dmd = new SpDemandeRecrutement();
        $form = $this->createForm(new SpDemandeRecrutementType());
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
            $dmd = $form->getData();
            $bdg=$this->getDoctrine()->getRepository('SelectingPeopleBundle:SpPosteBridge')->findBy(array('dir'=>$dmd->getDir(), 'pst'=>$dmd->getPst(), 'srv'=>$dmd->getSrv(), 'pol'=>$dmd->getPol()));
            $dmd->setPosteBridgeId($bdg[0]);
            $statut=$this->getDoctrine()->getRepository('SelectingPeopleBundle:SpStatutMission')->findBy(array('id'=>'12'));
            $dmd->setStatutMissionId($statut[0]);
            $dmd->setPeriodeEssai($dmd->getStatutCollaborateurId()->getPeriode());
            $dmd->setPreference($_POST['selectingpeoplebundle_spdemanderecrutement']['preference']);
            $dmd->setExclusion($_POST['selectingpeoplebundle_spdemanderecrutement']['exclusion']);
            $dmd->setSalaire($dmd->getPst()->getPstSalaire());
            $time = new \DateTime();
            $dmd->setDate($time);
            $em->persist($dmd);
            $em->flush();
            if($dmd->getConfidentiel()=='Oui'){
                $c='Confidentiel';
            }
            else{
                $c= 'Non confidentiel';
            }
            $msg  = "Bonjour,\n";
            $msg .= "Nous avons bien reçu votre demande de recrutement pour un ".$dmd->getPosteBridgeId()->getPst()->getPstLibelle()." ".$dmd->getProduit()." à ".$dmd->getVille()." ".$c." et nous vous remercions.\n";
            $msg .= "Nous sommes assurés que le processus de recrutement se déroulera à votre entière satisfaction.\n";
            $msg .= "L’équipe Selecting People.";
            if(Swift_Validate::email($dmd->getMailDemandeur())){
                if($this->container->get('kernel')->getEnvironment()!='prod'){
                    $message = \Swift_Message::newInstance()
                        ->setSubject('TEST: Confirmation demande recrutement ')
                        ->setFrom('selectingpeoplerecruitement@gmail.com')
                        ->addTo($dmd->getMailDemandeur())
                        ->addBcc('poleweb@noz.fr')
                        ->setBody($msg." \nCECI EST UN MAIL DE TEST, MERCI DE NE PAS EN TENIR COMPTE ");
                } else{
                    $message = \Swift_Message::newInstance()
                        ->setSubject('Confirmation demande recrutement ')
                        ->setFrom('selectingpeoplerecruitement@gmail.com')
                        ->addTo($dmd->getMailDemandeur())
                        ->addBcc('poleweb@noz.fr')
                        ->setBody($msg);
                }
                $mailer = $this->get('mailer');
                $mailer->send($message);
                $spool = $mailer->getTransport()->getSpool();
                $transport = $this->get('swiftmailer.transport.real');
                $spool->flushQueue($transport);
// deuxieme mail au RH
                $template = 'SelectingPeopleBundle:Mail:mail_nv_recrutement.html.twig';
                $body = $this->render($template, array('dmd' => $dmd));
                if($this->container->get('kernel')->getEnvironment()!='prod'){
                    $message = \Swift_Message::newInstance()
                        ->setContentType("text/html")
                        ->setSubject('TEST: Nouvelle demande de recrutement')
                        ->setFrom('selectingpeoplerecruitement@gmail.com')
                        ->addTo('coslobanu@veo-finances.com')
                        ->addBcc('poleweb@noz.fr')
                        ->setBody($body."\nCECI EST UN MAIL DE TEST, MERCI DE NE PAS EN TENIR COMPTE");
                }else{
                    $message = \Swift_Message::newInstance()
                        ->setContentType("text/html")
                        ->setSubject('Nouvelle demande de recrutement')
                        ->setFrom('selectingpeoplerecruitement@gmail.com')
                        ->addTo('coslobanu@veo-finances.com')
                        ->addBcc('poleweb@noz.fr')
                        ->setBody($body);
                }
                $mailer = $this->get('mailer');
                $mailer->send($message);
                $spool = $mailer->getTransport()->getSpool();
                $transport = $this->get('swiftmailer.transport.real');
                $spool->flushQueue($transport);
            }

            return $this->render('SelectingPeopleBundle:Default:validation.html.twig');
        }
        $form1= $this->createForm(new SpPosteType());
        $usr= $this->get('security.context')->getToken()->getUser();
        return $this->render('SelectingPeopleBundle:Default:demande_recrutement.html.twig',
            array('usr'=>$usr, 'form' => $form->createView(), 'form1'=> $form1->createView()));
    }

    /**
     * @Route("/preference", name="pref")
     */

    public function preferenceAction(){

        $request = $this->getRequest();

        $result = array();
        $em = $this->getDoctrine()->getManager();
        $charges = $em->getRepository('SelectingPeopleBundle:SpChargeRecrutement')->findAll();

        $i = 0;

        foreach ($charges as $charge) {
            $result[$i]['id'] = $charge->getId();
            $result[$i]['name'] = $charge->getUserId()->getDisplayName();
            $i++;
        }
        $output = '<option value="Indifférent"> -------------------- Indifferent -------------------- </option>';
        $output .='<option value="Cabinet extérieur"> Cabinet extérieur  </option>';
        foreach ($result as $row) {
            $output .= '<option value="' . $row['name'] . '">' . $row['name'] . '</option>';
        }
            $return = $output;
            $response = new Response();
            $data = json_encode($return);
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent($data);
            return $response;
        }
    /**
     * @Route("/direction", name="direction")
     */

    public function directionsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();
        //if ($request->isXmlHttpRequest()) {
        $result = array();
        $directions = $em->getRepository('AdministrationDirectionBundle:Direction')->findAll();
        $i = 0;
        foreach ($directions as $direction) {
            $result[$i]['id'] = $direction->getId();
            $result[$i]['libelle'] = $direction->getLibelle();
            $i++;
        }
        $output = '<option value=""> ---------------------- Directions ---------------------- </option>';
        foreach ($result as $row) {
            $output .= '<option value="' . $row['id'] . '">' . $row['libelle'] . '</option>';
        }

        $return = $output;
        $response = new Response();
        $data = json_encode($return);
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent($data);
        return $response;
    }

    /**
     * @Route("/serv", name="serv")
     */

    public function servicesAction()
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $direction = $request->get('direction');
        $result = array();
        $services = $em->getRepository('AdministrationDirectionBundle:Service')->findBy(array('direction' => $direction));
        $i = 0;
        foreach ($services as $service) {
            $result[$i]['id'] = $service->getId();
            $result[$i]['libelle'] = $service->getLibelle();
            $i++;
        }
            $output = '<option  value=""> ---------------------- Services ---------------------- </option>';
            foreach ($result as $row) {
                $output .= '<option value="' . $row['id'] . '">' . $row['libelle'] . '</option>';
            }
            $return = $output;
            $response = new Response();
            $data = json_encode($return);
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent($data);
            return $response;
    }
    /**
     * @Route("/poles", name="poles")
     */
    public function polesAction()
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $serv = $request->get('service');
        $result = array();
        $poles = $em->getRepository('SelectingPeopleBundle:SpPole')->findBy(array('srv' => $serv));
        $i = 0;
        foreach ($poles as $pole) {
            $result[$i]['id'] = $pole->getPolId();
            $result[$i]['libelle'] = $pole->getPolLibelle();
            $i++;
        }
        $output = '<option value=""> ---------------------- Pôles ---------------------- </option>';
        if (count($result) > 0) {
            foreach ($result as $row) {
                $output .= '<option value="' . $row['id'] . '">' . $row['libelle'] . '</option>';
            }
            $return = $output;
            $response = new Response();
            $data = json_encode($return);
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent($data);
            return $response;
        }
        return new Response('Erreur');}
    /**
     * @Route("/nv_pst", name="nv_pst")
     */

    public function nvPosteAction()
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $nv_pst = $request->get('nv_pst');
        $poste= new SpPoste();
        $poste->setPstLibelle($nv_pst[3]);
        $sal=$nv_pst[4];
        if($sal==''){
            $poste->setPstSalaire(0);
        }else{
            $poste->setPstSalaire($sal);
        }
        $em->persist($poste);
        $em->flush();
        $bdg= new SpPosteBridge();
        $dir=$this->getDoctrine()->getRepository('AdministrationDirectionBundle:Direction')->findOneBy(array('id'=>$nv_pst[0]));
        $ser=$this->getDoctrine()->getRepository('AdministrationDirectionBundle:Service')->findOneBy(array('id'=>$nv_pst[1]));
        $bdg->setDir($dir);
        $bdg->setSrv($ser);
        $bdg->setPst($poste);
        if ($nv_pst[2]!=0){
            $pol=$this->getDoctrine()->getRepository('SelectingPeopleBundle:SpPole')->findOneBy(array('polId'=>$nv_pst[2]));
            $bdg->setPol($pol);
        }
        $em->persist($bdg);
        $em->flush();
        $res=$poste->getPstId();
        $return = $res;
        $response = new Response();
        $data = json_encode($return);
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent($data);
        return $response;
    }

    /**
     * @Route("/postes", name="postes")
     */

    public function postesAction()
    {
        $request = $this->getRequest();
        $output = '<option value=""> ----------------------- Postes ------------------------- </option>';
        $idBridge = $request->get('idBridge');
            if ($idBridge[1] != '') {
                if ($idBridge[2] == 'NULL') {
                    $lineSql = " AND pol_id IS NULL ";
                } else {
                    $lineSql = " AND pol_id = " . $idBridge[2];
                }

                $sql = "	SELECT pst_id, pst_libelle
				FROM sp_poste
				WHERE pst_id IN (
					SELECT pst_id
					FROM sp_poste_bridge
					WHERE dir_id =" . $idBridge[0] . "
					AND srv_id =" . $idBridge[1] .
                    $lineSql . "
				)
				ORDER BY pst_libelle;
			";
                $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
                $query->execute();
                $result = $query->fetchAll();


                    foreach ($result as $row) {
                        $output .= '<option value="' . $row['pst_id'] . '">' . $row['pst_libelle'] . '</option>';
                    }
                $role=$this->get('security.context')->getToken()->getUser()->isGranted('ROLE_ADMIN');
                if($role){
                    $output .= '<option value="-1">Autre</option>';
                }
                    $return = $output;
                    $response = new Response();
                    $data = json_encode($return);
                    $response->headers->set('Content-Type', 'application/json');
                    $response->setContent($data);
                    return $response;

            } else {
                $return = $output;
                $response = new Response();
                $data = json_encode($return);
                $response->headers->set('Content-Type', 'application/json');
                $response->setContent($data);
                return $response;
            }
    }

}
