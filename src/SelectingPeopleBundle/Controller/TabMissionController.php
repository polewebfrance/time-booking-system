<?php

namespace SelectingPeopleBundle\Controller;

use SelectingPeopleBundle\Form\RechercheType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\DBAL\Query\QueryBuilder;
use SelectingPeopleBundle\Entity\SpPosteBridge;
use Symfony\Component\Validator\Constraints\DateTime;


/**
 * TabMission controller.
 *
 * @Route("/sptabmission")
 */
class TabMissionController extends Controller
{
    /**
     * @Route("/", name="tab")
     */

    public function tabAction(){
        $form = $this->createForm(new RechercheType());
        $em = $this->getDoctrine()->getManager();
        $statuts = $em->getRepository('SelectingPeopleBundle:SpStatutMission')->findAll();
        $directions = $em->getRepository('AdministrationDirectionBundle:Direction')->findAll();
        $ch_rec = $em->getRepository('SelectingPeopleBundle:SpChargeRecrutement')->findAll();
        $cabinet = $em->getRepository('SelectingPeopleBundle:SpCabinetRecrutement')->findAll();
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery('SELECT dmd FROM SelectingPeopleBundle\Entity\SpDemandeRecrutement dmd ORDER BY dmd.id DESC');
        $query->setMaxResults(100);
        $demandes = $query->getResult();
        $session = $this->getRequest()->getSession();
        $session->remove('statut');
        $session->remove('direction');
        $result=array();
        $res=array();
        $affectation=array();
        $aff_dmd_array=array();
        $rec1_array=array();
        $rec2_array=array();
        $i=0;
        foreach ($demandes as $dmd){
            $aff= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpAffectationDemande")-> findOneBy(array('dmdId'=>$dmd->getId(),'premierRecruteur'=>'1'));
            $aff_dmd= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpAffectationDemande")-> findOneBy(array('dmdId'=>$dmd->getId()),
                array('id' => 'DESC'));
            $rec1= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpRecrutement")-> findOneBy(array('dmdId'=>$dmd->getId(),'numero'=>'1'));
            $rec2= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpRecrutement")-> findOneBy(array('dmdId'=>$dmd->getId(),'numero'=>'2'));
            $d1=$dmd->getDate();
            if($rec1!=null){
                $d2=$rec1->getDateRelance();
                $d_recrutement=$rec1->getDateRecrutement();
                $duree=$d1->diff($d_recrutement)->format('%m');
            }
            else{
                $d2=null;
                $duree='';
            }
            $time = new \DateTime();
            if ((($dmd->getStatutMissionId()->getId()==1) and ($d1->diff($time)->format('%m')>3)) or (($dmd->getStatutMissionId()->getId()==5) and ($d2!=null) and ($d2->diff($time)->format('%m')>3 ))){
                $mission_difficile= 'Oui';
            } else {
                $mission_difficile= '';
            }


            $res[]=array("id_dmd"=>$dmd->getId(),"mission_difficile"=>$mission_difficile,"poste"=>$dmd->getPosteBridgeId()->getPst()->getPstLibelle(),
                "pays"=> ($dmd->getPaysId()!=null) ? $res["pays"]=$dmd->getPaysId()->getNomPays() : '',"demandeur"=>$dmd->getNomDemandeur()." ".$dmd->getPrenomDemandeur(),
                "direction"=>$dmd->getPosteBridgeId()->getDir()->getLibelle(),"date_dmd"=>$dmd->getDate()->format('Y-m-d'),
                "charge_rech"=>($dmd->getChargeRechercheId()!=null) ? $res["charge_rech"]=$dmd->getChargeRechercheId()->getUserId()->getDisplayName() : '',
                "reference"=>$dmd->getReference(),"statut"=>$dmd->getStatutMissionId()->getStatut(),"confidentiel"=>$dmd->getConfidentiel(),
                "service"=>$dmd->getPosteBridgeId()->getSrv()->getLibelle(),
                "pole" => ($dmd->getPosteBridgeId()->getPol()!=null) ? $res["pole"]=$dmd->getPosteBridgeId()->getPol()->getPolLibelle() : '',
                "societe"=>$dmd->getSteId()->getLibelle(),"Nsociete"=>$dmd->getSteId()->getReference(),"salaire"=>$dmd->getSalaire(),
                "accompte"=>$dmd->getAccompte(),"solde"=>$dmd->getSolde(),"duree_rec"=>$duree,
                "comment_dem"=>$dmd->getCommentaire(),"comment_dir"=>$dmd->getCommentaireSp(),
                "statut_id"=>$dmd->getStatutMissionId()->getId(),"ville"=>$dmd->getVille());

            if($aff!=null){
                $affectation[]=array("id_dmd"=>$aff->getDmdId()->getId(),"r1_recruteur"=>($aff->getIdRecruteur()!=null) ? $affectation["r1_recruteur"]=$aff->getIdRecruteur()->getUserId()->getDisplayName() : $aff->getIdCabinet()->getNomCabinet());
                if($aff_dmd!=null){
                    $aff_dmd_array[]=array("id_dmd"=>$dmd->getId(),
                        "sous_traitance" => ($aff_dmd->getAffSousTraitance()== 1) ? $aff_dmd_array["sous_traitance"]='Non' : 'Oui',
                        "charge_rec"=>($aff_dmd->getIdRecruteur()!=null) ? $aff_dmd_array["charge_rec"]=$aff_dmd->getIdRecruteur()->getUserId()->getDisplayName() : $aff_dmd->getIdCabinet()->getNomCabinet() );
                    if ($rec2!=null){
                        $rec2_array[]=array("id_dmd"=>$dmd->getId(),"r2_nom_cand"=>$rec2->getNomCandidat()." ".$rec2->getPrenomCandidat(),
                            "r2_date_rec"=>$rec2->getDateRecrutement(),"r2_date_integration"=>$rec2->getDateIntegration(),"r2_enposte"=>$rec2->getEnPoste());
                        $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>$rec1->getNomCandidat()." ".$rec1->getPrenomCandidat(),
                            "r1_date_rec"=>$rec1->getDateRecrutement(),"r1_date_integration"=>$rec1->getDateIntegration(),
                            'r1_enposte'=>$rec1->getEnPoste(),"r1_peride_conf"=>$rec1->getPeriodeConfirmee(),
                            "r1_garantie"=>$rec1->getGarantie(),"r1_date_relance"=>$rec1->getDateRelance());
                    }
                    else{
                        $rec2_array[]=array("id_dmd"=>$dmd->getId(),"r2_nom_cand"=>'',
                            "r2_date_rec"=>'',"r2_date_integration"=>'',"r2_enposte"=>'');
                        if ($rec1!=null){
                            $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>$rec1->getNomCandidat()." ".$rec1->getPrenomCandidat(),
                                "r1_date_rec"=>$rec1->getDateRecrutement(),"r1_date_integration"=>$rec1->getDateIntegration(),
                                'r1_enposte'=>$rec1->getEnPoste(),"r1_peride_conf"=>$rec1->getPeriodeConfirmee(),
                                "r1_garantie"=>$rec1->getGarantie(),"r1_date_relance"=>$rec1->getDateRelance());
                        }
                        else{
                            $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>'',
                                "r1_date_rec"=>'',"r1_date_integration"=>'',
                                'r1_enposte'=>'',"r1_peride_conf"=>'',
                                "r1_garantie"=>'',"r1_date_relance"=>'');
                        }
                    }

                }
                else{
                    $aff_dmd_array[]=array("id_dmd"=>$aff->getDmdId()->getId(),"sous_traitance"=> '', "charge_rec"=> '');
                    if ($rec2!=null){
                        $rec2_array[]=array("id_dmd"=>$dmd->getId(),"r2_nom_cand"=>$rec2->getNomCandidat()." ".$rec2->getPrenomCandidat(),
                            "r2_date_rec"=>$rec2->getDateRecrutement(),"r2_date_integration"=>$rec2->getDateIntegration(),"r2_enposte"=>$rec2->getEnPoste());
                        if ($rec1!=null){
                            $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>$rec1->getNomCandidat()." ".$rec1->getPrenomCandidat(),
                                "r1_date_rec"=>$rec1->getDateRecrutement(),"r1_date_integration"=>$rec1->getDateIntegration(),
                                'r1_enposte'=>$rec1->getEnPoste(),"r1_peride_conf"=>$rec1->getPeriodeConfirmee(),
                                "r1_garantie"=>$rec1->getGarantie(),"r1_date_relance"=>$rec1->getDateRelance());
                        }
                        else{
                            $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>'',
                                "r1_date_rec"=>'',"r1_date_integration"=>'',
                                'r1_enposte'=>'',"r1_peride_conf"=>'',
                                "r1_garantie"=>'',"r1_date_relance"=>'');
                        }
                    }
                    else{
                        $rec2_array[]=array("id_dmd"=>$dmd->getId(),"r2_nom_cand"=>'',
                            "r2_date_rec"=>'',"r2_date_integration"=>'',"r2_enposte"=>'');
                        if ($rec1!=null){
                            $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>$rec1->getNomCandidat()." ".$rec1->getPrenomCandidat(),
                                "r1_date_rec"=>$rec1->getDateRecrutement(),"r1_date_integration"=>$rec1->getDateIntegration(),
                                'r1_enposte'=>$rec1->getEnPoste(),"r1_peride_conf"=>$rec1->getPeriodeConfirmee(),
                                "r1_garantie"=>$rec1->getGarantie(),"r1_date_relance"=>$rec1->getDateRelance());
                        }
                        else{
                            $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>'',
                                "r1_date_rec"=>'',"r1_date_integration"=>'',
                                'r1_enposte'=>'',"r1_peride_conf"=>'',
                                "r1_garantie"=>'',"r1_date_relance"=>'');
                        }
                    }
                }
                $result[] = array_merge($rec1_array[$i], $rec2_array[$i], $aff_dmd_array[$i], $res[$i], $affectation[$i]);
            } else {
                $affectation[] = array("id_dmd" => $dmd->getId(), "r1_recruteur" => '');
                if ($aff_dmd != null) {
                    $aff_dmd_array[] = array("id_dmd" => $dmd->getId(),
                        "sous_traitance" => ($aff_dmd->getAffSousTraitance() == 1) ? $aff_dmd_array["sous_traitance"] = 'Non' : 'Oui',
                        "charge_rec" => ($aff_dmd->getIdRecruteur() != null) ? $aff_dmd_array["charge_rec"] = $aff_dmd->getIdRecruteur()->getUserId()->getDisplayName() : $aff_dmd->getIdCabinet()->getNomCabinet());
                    if ($rec2!=null){
                        $rec2_array[]=array("id_dmd"=>$dmd->getId(),"r2_nom_cand"=>$rec2->getNomCandidat()." ".$rec2->getPrenomCandidat(),
                            "r2_date_rec"=>$rec2->getDateRecrutement(),"r2_date_integration"=>$rec2->getDateIntegration(),"r2_enposte"=>$rec2->getEnPoste());
                        if ($rec1!=null){
                            $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>$rec1->getNomCandidat()." ".$rec1->getPrenomCandidat(),
                                "r1_date_rec"=>$rec1->getDateRecrutement(),"r1_date_integration"=>$rec1->getDateIntegration(),
                                'r1_enposte'=>$rec1->getEnPoste(),"r1_peride_conf"=>$rec1->getPeriodeConfirmee(),
                                "r1_garantie"=>$rec1->getGarantie(),"r1_date_relance"=>$rec1->getDateRelance());
                        }
                        else{
                            $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>'',
                                "r1_date_rec"=>'',"r1_date_integration"=>'',
                                'r1_enposte'=>'',"r1_peride_conf"=>'',
                                "r1_garantie"=>'',"r1_date_relance"=>'');
                        }
                    }
                    else{
                        $rec2_array[]=array("id_dmd"=>$dmd->getId(),"r2_nom_cand"=>'',
                            "r2_date_rec"=>'',"r2_date_integration"=>'',"r2_enposte"=>'');
                        if ($rec1!=null){
                            $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>$rec1->getNomCandidat()." ".$rec1->getPrenomCandidat(),
                                "r1_date_rec"=>$rec1->getDateRecrutement(),"r1_date_integration"=>$rec1->getDateIntegration(),
                                'r1_enposte'=>$rec1->getEnPoste(),"r1_peride_conf"=>$rec1->getPeriodeConfirmee(),
                                "r1_garantie"=>$rec1->getGarantie(),"r1_date_relance"=>$rec1->getDateRelance());
                        }
                        else{
                            $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>'',
                                "r1_date_rec"=>'',"r1_date_integration"=>'',
                                'r1_enposte'=>'',"r1_peride_conf"=>'',
                                "r1_garantie"=>'',"r1_date_relance"=>'');
                        }
                    }
                } else {
                    $aff_dmd_array[] = array("id_dmd" => $dmd->getId(), "sous_traitance" => '', "charge_rec" => '');
                    if ($rec2!=null){
                        $rec2_array[]=array("id_dmd"=>$dmd->getId(),"r2_nom_cand"=>$rec2->getNomCandidat()." ".$rec2->getPrenomCandidat(),
                            "r2_date_rec"=>$rec2->getDateRecrutement(),"r2_date_integration"=>$rec2->getDateIntegration(),"r2_enposte"=>$rec2->getEnPoste());
                        if ($rec1!=null){
                            $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>$rec1->getNomCandidat()." ".$rec1->getPrenomCandidat(),
                                "r1_date_rec"=>$rec1->getDateRecrutement(),"r1_date_integration"=>$rec1->getDateIntegration(),
                                'r1_enposte'=>$rec1->getEnPoste(),"r1_peride_conf"=>$rec1->getPeriodeConfirmee(),
                                "r1_garantie"=>$rec1->getGarantie(),"r1_date_relance"=>$rec1->getDateRelance());
                        }
                        else{
                            $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>'',
                                "r1_date_rec"=>'',"r1_date_integration"=>'',
                                'r1_enposte'=>'',"r1_peride_conf"=>'',
                                "r1_garantie"=>'',"r1_date_relance"=>'');
                        }
                    }
                    else{
                        $rec2_array[]=array("id_dmd"=>$dmd->getId(),"r2_nom_cand"=>'',
                            "r2_date_rec"=>'',"r2_date_integration"=>'',"r2_enposte"=>'');
                        if ($rec1!=null){
                            $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>$rec1->getNomCandidat()." ".$rec1->getPrenomCandidat(),
                                "r1_date_rec"=>$rec1->getDateRecrutement(),"r1_date_integration"=>$rec1->getDateIntegration(),
                                'r1_enposte'=>$rec1->getEnPoste(),"r1_peride_conf"=>$rec1->getPeriodeConfirmee(),
                                "r1_garantie"=>$rec1->getGarantie(),"r1_date_relance"=>$rec1->getDateRelance());
                        }
                        else{
                            $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>'',
                                "r1_date_rec"=>'',"r1_date_integration"=>'',
                                'r1_enposte'=>'',"r1_peride_conf"=>'',
                                "r1_garantie"=>'',"r1_date_relance"=>'');
                        }
                    }
                }
                $result[] = array_merge($rec1_array[$i], $rec2_array[$i] ,$aff_dmd_array[$i], $affectation[$i], $res[$i]);
            }
            $i++;
        }

        return $this->render('SelectingPeopleBundle:Default:tab_mission.html.twig',
            array('form' => $form->createView(), 'result'=>$result, 'statut'=>$statuts, 'direction'=>$directions, 'ch_rec'=>$ch_rec, 'cabinets'=>$cabinet));
    }
    /**
     * @Route("/tout", name="aff_tout")
     */

    public function affToutAction(){
        $form = $this->createForm(new RechercheType());
        $em = $this->getDoctrine()->getManager();
        $statuts = $em->getRepository('SelectingPeopleBundle:SpStatutMission')->findAll();
        $directions = $em->getRepository('AdministrationDirectionBundle:Direction')->findAll();
        $ch_rec = $em->getRepository('SelectingPeopleBundle:SpChargeRecrutement')->findAll();
        $cabinet = $em->getRepository('SelectingPeopleBundle:SpCabinetRecrutement')->findAll();
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery('SELECT dmd FROM SelectingPeopleBundle\Entity\SpDemandeRecrutement dmd ORDER BY dmd.id DESC');
        $demandes = $query->getResult();

        $result=array();
        $res=array();
        $affectation=array();
        $aff_dmd_array=array();
        $rec1_array=array();
        $rec2_array=array();
        $i=0;
        foreach ($demandes as $dmd){
            $aff= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpAffectationDemande")-> findOneBy(array('dmdId'=>$dmd->getId(),'premierRecruteur'=>'1'));
            $aff_dmd= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpAffectationDemande")-> findOneBy(array('dmdId'=>$dmd->getId()),
                array('id' => 'DESC'));
            $rec1= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpRecrutement")-> findOneBy(array('dmdId'=>$dmd->getId(),'numero'=>'1'));
            $rec2= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpRecrutement")-> findOneBy(array('dmdId'=>$dmd->getId(),'numero'=>'2'));
            $d1=$dmd->getDate();
            if($rec1!=null){
                $d2=$rec1->getDateRelance();
                $d_recrutement=$rec1->getDateRecrutement();
                $duree=$d1->diff($d_recrutement)->format('%m');
            }
            else{
                $d2=null;
                $duree='';
            }
            $time = new \DateTime();
            if ((($dmd->getStatutMissionId()->getId()==1) and ($d1->diff($time)->format('%m')>3)) or (($dmd->getStatutMissionId()->getId()==5) and ($d2!=null) and ($d2->diff($time)->format('%m')>3 ))){
                $mission_difficile= 'Oui';
            } else {
                $mission_difficile= '';
            }


            $res[]=array("id_dmd"=>$dmd->getId(),"mission_difficile"=>$mission_difficile,"poste"=>$dmd->getPosteBridgeId()->getPst()->getPstLibelle(),
                "pays"=> ($dmd->getPaysId()!=null) ? $res["pays"]=$dmd->getPaysId()->getNomPays() : '',"demandeur"=>$dmd->getNomDemandeur()." ".$dmd->getPrenomDemandeur(),
                "direction"=>$dmd->getPosteBridgeId()->getDir()->getLibelle(),"date_dmd"=>$dmd->getDate()->format('Y-m-d'),
                "charge_rech"=>($dmd->getChargeRechercheId()!=null) ? $res["charge_rech"]=$dmd->getChargeRechercheId()->getUserId()->getDisplayName() : '',
                "reference"=>$dmd->getReference(),"statut"=>$dmd->getStatutMissionId()->getStatut(),"confidentiel"=>$dmd->getConfidentiel(),
                "service"=>$dmd->getPosteBridgeId()->getSrv()->getLibelle(),
                "pole" => ($dmd->getPosteBridgeId()->getPol()!=null) ? $res["pole"]=$dmd->getPosteBridgeId()->getPol()->getPolLibelle() : '',
                "societe"=>$dmd->getSteId()->getLibelle(),"Nsociete"=>$dmd->getSteId()->getReference(),"salaire"=>$dmd->getSalaire(),
                "accompte"=>$dmd->getAccompte(),"solde"=>$dmd->getSolde(),"duree_rec"=>$duree,
                "comment_dem"=>$dmd->getCommentaire(),"comment_dir"=>$dmd->getCommentaireSp(),
                "statut_id"=>$dmd->getStatutMissionId()->getId(),"ville"=>$dmd->getVille());

            if($aff!=null){
                $affectation[]=array("id_dmd"=>$aff->getDmdId()->getId(),"r1_recruteur"=>($aff->getIdRecruteur()!=null) ? $affectation["r1_recruteur"]=$aff->getIdRecruteur()->getUserId()->getDisplayName() : $aff->getIdCabinet()->getNomCabinet());
                if($aff_dmd!=null){
                    $aff_dmd_array[]=array("id_dmd"=>$dmd->getId(),
                        "sous_traitance" => ($aff_dmd->getAffSousTraitance()== 1) ? $aff_dmd_array["sous_traitance"]='Non' : 'Oui',
                        "charge_rec"=>($aff_dmd->getIdRecruteur()!=null) ? $aff_dmd_array["charge_rec"]=$aff_dmd->getIdRecruteur()->getUserId()->getDisplayName() : $aff_dmd->getIdCabinet()->getNomCabinet() );
                    if ($rec2!=null){
                        $rec2_array[]=array("id_dmd"=>$dmd->getId(),"r2_nom_cand"=>$rec2->getNomCandidat()." ".$rec2->getPrenomCandidat(),
                            "r2_date_rec"=>$rec2->getDateRecrutement(),"r2_date_integration"=>$rec2->getDateIntegration(),"r2_enposte"=>$rec2->getEnPoste());
                        $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>$rec1->getNomCandidat()." ".$rec1->getPrenomCandidat(),
                            "r1_date_rec"=>$rec1->getDateRecrutement(),"r1_date_integration"=>$rec1->getDateIntegration(),
                            'r1_enposte'=>$rec1->getEnPoste(),"r1_peride_conf"=>$rec1->getPeriodeConfirmee(),
                            "r1_garantie"=>$rec1->getGarantie(),"r1_date_relance"=>$rec1->getDateRelance());
                    }
                    else{
                        $rec2_array[]=array("id_dmd"=>$dmd->getId(),"r2_nom_cand"=>'',
                            "r2_date_rec"=>'',"r2_date_integration"=>'',"r2_enposte"=>'');
                        if ($rec1!=null){
                            $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>$rec1->getNomCandidat()." ".$rec1->getPrenomCandidat(),
                                "r1_date_rec"=>$rec1->getDateRecrutement(),"r1_date_integration"=>$rec1->getDateIntegration(),
                                'r1_enposte'=>$rec1->getEnPoste(),"r1_peride_conf"=>$rec1->getPeriodeConfirmee(),
                                "r1_garantie"=>$rec1->getGarantie(),"r1_date_relance"=>$rec1->getDateRelance());
                        }
                        else{
                            $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>'',
                                "r1_date_rec"=>'',"r1_date_integration"=>'',
                                'r1_enposte'=>'',"r1_peride_conf"=>'',
                                "r1_garantie"=>'',"r1_date_relance"=>'');
                        }
                    }

                }
                else{
                    $aff_dmd_array[]=array("id_dmd"=>$aff->getDmdId()->getId(),"sous_traitance"=> '', "charge_rec"=> '');
                    if ($rec2!=null){
                        $rec2_array[]=array("id_dmd"=>$dmd->getId(),"r2_nom_cand"=>$rec2->getNomCandidat()." ".$rec2->getPrenomCandidat(),
                            "r2_date_rec"=>$rec2->getDateRecrutement(),"r2_date_integration"=>$rec2->getDateIntegration(),"r2_enposte"=>$rec2->getEnPoste());
                        if ($rec1!=null){
                            $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>$rec1->getNomCandidat()." ".$rec1->getPrenomCandidat(),
                                "r1_date_rec"=>$rec1->getDateRecrutement(),"r1_date_integration"=>$rec1->getDateIntegration(),
                                'r1_enposte'=>$rec1->getEnPoste(),"r1_peride_conf"=>$rec1->getPeriodeConfirmee(),
                                "r1_garantie"=>$rec1->getGarantie(),"r1_date_relance"=>$rec1->getDateRelance());
                        }
                        else{
                            $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>'',
                                "r1_date_rec"=>'',"r1_date_integration"=>'',
                                'r1_enposte'=>'',"r1_peride_conf"=>'',
                                "r1_garantie"=>'',"r1_date_relance"=>'');
                        }
                    }
                    else{
                        $rec2_array[]=array("id_dmd"=>$dmd->getId(),"r2_nom_cand"=>'',
                            "r2_date_rec"=>'',"r2_date_integration"=>'',"r2_enposte"=>'');
                        if ($rec1!=null){
                            $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>$rec1->getNomCandidat()." ".$rec1->getPrenomCandidat(),
                                "r1_date_rec"=>$rec1->getDateRecrutement(),"r1_date_integration"=>$rec1->getDateIntegration(),
                                'r1_enposte'=>$rec1->getEnPoste(),"r1_peride_conf"=>$rec1->getPeriodeConfirmee(),
                                "r1_garantie"=>$rec1->getGarantie(),"r1_date_relance"=>$rec1->getDateRelance());
                        }
                        else{
                            $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>'',
                                "r1_date_rec"=>'',"r1_date_integration"=>'',
                                'r1_enposte'=>'',"r1_peride_conf"=>'',
                                "r1_garantie"=>'',"r1_date_relance"=>'');
                        }
                    }
                }
                $result[] = array_merge($rec1_array[$i], $rec2_array[$i], $aff_dmd_array[$i], $res[$i], $affectation[$i]);
                //var_dump($result);die;
            } else {
                $affectation[] = array("id_dmd" => $dmd->getId(), "r1_recruteur" => '');
                if ($aff_dmd != null) {
                    $aff_dmd_array[] = array("id_dmd" => $dmd->getId(),
                        "sous_traitance" => ($aff_dmd->getAffSousTraitance() == 1) ? $aff_dmd_array["sous_traitance"] = 'Non' : 'Oui',
                        "charge_rec" => ($aff_dmd->getIdRecruteur() != null) ? $aff_dmd_array["charge_rec"] = $aff_dmd->getIdRecruteur()->getUserId()->getDisplayName() : $aff_dmd->getIdCabinet()->getNomCabinet());
                    if ($rec2!=null){
                        $rec2_array[]=array("id_dmd"=>$dmd->getId(),"r2_nom_cand"=>$rec2->getNomCandidat()." ".$rec2->getPrenomCandidat(),
                            "r2_date_rec"=>$rec2->getDateRecrutement(),"r2_date_integration"=>$rec2->getDateIntegration(),"r2_enposte"=>$rec2->getEnPoste());
                        if ($rec1!=null){
                            $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>$rec1->getNomCandidat()." ".$rec1->getPrenomCandidat(),
                                "r1_date_rec"=>$rec1->getDateRecrutement(),"r1_date_integration"=>$rec1->getDateIntegration(),
                                'r1_enposte'=>$rec1->getEnPoste(),"r1_peride_conf"=>$rec1->getPeriodeConfirmee(),
                                "r1_garantie"=>$rec1->getGarantie(),"r1_date_relance"=>$rec1->getDateRelance());
                        }
                        else{
                            $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>'',
                                "r1_date_rec"=>'',"r1_date_integration"=>'',
                                'r1_enposte'=>'',"r1_peride_conf"=>'',
                                "r1_garantie"=>'',"r1_date_relance"=>'');
                        }
                    }
                    else{
                        $rec2_array[]=array("id_dmd"=>$dmd->getId(),"r2_nom_cand"=>'',
                            "r2_date_rec"=>'',"r2_date_integration"=>'',"r2_enposte"=>'');
                        if ($rec1!=null){
                            $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>$rec1->getNomCandidat()." ".$rec1->getPrenomCandidat(),
                                "r1_date_rec"=>$rec1->getDateRecrutement(),"r1_date_integration"=>$rec1->getDateIntegration(),
                                'r1_enposte'=>$rec1->getEnPoste(),"r1_peride_conf"=>$rec1->getPeriodeConfirmee(),
                                "r1_garantie"=>$rec1->getGarantie(),"r1_date_relance"=>$rec1->getDateRelance());
                        }
                        else{
                            $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>'',
                                "r1_date_rec"=>'',"r1_date_integration"=>'',
                                'r1_enposte'=>'',"r1_peride_conf"=>'',
                                "r1_garantie"=>'',"r1_date_relance"=>'');
                        }
                    }
                } else {
                    $aff_dmd_array[] = array("id_dmd" => $dmd->getId(), "sous_traitance" => '', "charge_rec" => '');
                    if ($rec2!=null){
                        $rec2_array[]=array("id_dmd"=>$dmd->getId(),"r2_nom_cand"=>$rec2->getNomCandidat()." ".$rec2->getPrenomCandidat(),
                            "r2_date_rec"=>$rec2->getDateRecrutement(),"r2_date_integration"=>$rec2->getDateIntegration(),"r2_enposte"=>$rec2->getEnPoste());
                        if ($rec1!=null){
                            $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>$rec1->getNomCandidat()." ".$rec1->getPrenomCandidat(),
                                "r1_date_rec"=>$rec1->getDateRecrutement(),"r1_date_integration"=>$rec1->getDateIntegration(),
                                'r1_enposte'=>$rec1->getEnPoste(),"r1_peride_conf"=>$rec1->getPeriodeConfirmee(),
                                "r1_garantie"=>$rec1->getGarantie(),"r1_date_relance"=>$rec1->getDateRelance());
                        }
                        else{
                            $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>'',
                                "r1_date_rec"=>'',"r1_date_integration"=>'',
                                'r1_enposte'=>'',"r1_peride_conf"=>'',
                                "r1_garantie"=>'',"r1_date_relance"=>'');
                        }
                    }
                    else{
                        $rec2_array[]=array("id_dmd"=>$dmd->getId(),"r2_nom_cand"=>'',
                            "r2_date_rec"=>'',"r2_date_integration"=>'',"r2_enposte"=>'');
                        if ($rec1!=null){
                            $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>$rec1->getNomCandidat()." ".$rec1->getPrenomCandidat(),
                                "r1_date_rec"=>$rec1->getDateRecrutement(),"r1_date_integration"=>$rec1->getDateIntegration(),
                                'r1_enposte'=>$rec1->getEnPoste(),"r1_peride_conf"=>$rec1->getPeriodeConfirmee(),
                                "r1_garantie"=>$rec1->getGarantie(),"r1_date_relance"=>$rec1->getDateRelance());
                        }
                        else{
                            $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>'',
                                "r1_date_rec"=>'',"r1_date_integration"=>'',
                                'r1_enposte'=>'',"r1_peride_conf"=>'',
                                "r1_garantie"=>'',"r1_date_relance"=>'');
                        }
                    }
                }
                $result[] = array_merge($rec1_array[$i], $rec2_array[$i] ,$aff_dmd_array[$i], $affectation[$i], $res[$i]);
            }
            $i++;
        }

        return $this->render('SelectingPeopleBundle:Default:tab_mission.html.twig',
            array('form' => $form->createView(), 'result'=>$result, 'statut'=>$statuts, 'direction'=>$directions, 'ch_rec'=>$ch_rec, 'cabinets'=>$cabinet));
    }


    /**
     * @Route("/filtre", name="filtre")
     */

    public function filtreAction(){

        $form = $this->createForm(new RechercheType());
        $em = $this->getDoctrine()->getManager();
        $statuts = $em->getRepository('SelectingPeopleBundle:SpStatutMission')->findAll();
        $directions = $em->getRepository('AdministrationDirectionBundle:Direction')->findAll();
        $ch_rec = $em->getRepository('SelectingPeopleBundle:SpChargeRecrutement')->findAll();
        $cabinet = $em->getRepository('SelectingPeopleBundle:SpCabinetRecrutement')->findAll();
        $em = $this->getDoctrine()->getManager();
        $qb = $this->getDoctrine()->getManager()->createQueryBuilder();
        //$dates= $this->getDoctrine()->getManager()->createQueryBuilder();
        if ($this->getRequest()->isMethod('POST')) {
            $session = $this->getRequest()->getSession();
            $session->set('poste',$_POST["selectingpeoplebundle_recherche"]["poste"]);
            $session->set('demandeur',$_POST["selectingpeoplebundle_recherche"]["demandeur"]);
            $session->set('reference',$_POST["selectingpeoplebundle_recherche"]["reference"]);
            $session->set('nom_cand',$_POST["selectingpeoplebundle_recherche"]["nom_cand"]);
            $session->set('sec_geo',$_POST["selectingpeoplebundle_recherche"]["sec_geo"]);
            $session->set('date_dmd',$_POST["selectingpeoplebundle_recherche"]["date_dmd"]);
            $session->set('date_rec',$_POST["selectingpeoplebundle_recherche"]["date_rec"]);
            $session->set('tr',$_POST["selectingpeoplebundle_recherche"]["sous_trait"]);
            $session->set('ch_rec',$_POST["selectingpeoplebundle_recherche"]["ch_rec"]);
            $session->set('cabinet_rec',$_POST["selectingpeoplebundle_recherche"]["cabinet_rec"]);

            if(isset($_POST["selectingpeoplebundle_recherche"]["statut"])==false){
                //$session->set('statut',$stat);
                $stat=array(1,2,3,4,5,6,7,8,9,10,11,12);
                $t=array();
                $session->set('statut',$t);

            }
            else{
                $stat=$_POST["selectingpeoplebundle_recherche"]["statut"];
                $session->set('statut',$_POST["selectingpeoplebundle_recherche"]["statut"]);
            }
            if(isset($_POST["selectingpeoplebundle_recherche"]["direction"])==false){
                $dir=array();
                foreach ($directions as $direction){
                    array_push($dir, $direction->getId());
                }
                $t=array();
                $session->set('direction',$t);
            }
            else{
                $dir=$_POST["selectingpeoplebundle_recherche"]["direction"];
                $session->set('direction',$_POST["selectingpeoplebundle_recherche"]["direction"]);
            }
            $trait='';
            if($_POST["selectingpeoplebundle_recherche"]["sous_trait"]=='Oui'){
                $tait='0';
            }
            if ($_POST["selectingpeoplebundle_recherche"]["sous_trait"]=='Non'){
                $trait='1';
            }

            if($_POST["selectingpeoplebundle_recherche"]["date_rec"]!=null){
                $date1=substr($_POST["selectingpeoplebundle_recherche"]["date_rec"],0,10);
                $date2=substr($_POST["selectingpeoplebundle_recherche"]["date_rec"],13,10);
                $date1=new \DateTime($date1);
                $date2=new \DateTime($date2);
            }
            else{
                $dates = $em ->createQuery("SELECT MIN (rec.dateRecrutement), MAX(rec.dateRecrutement) FROM SelectingPeopleBundle:SpRecrutement rec")
                    ->getScalarResult();
                $date1=new\DateTime($dates[0][1]);
                $date2=new\DateTime($dates[0][2]);
            }
            if($_POST["selectingpeoplebundle_recherche"]["date_dmd"]!=null){
                $date_dmd1=substr($_POST["selectingpeoplebundle_recherche"]["date_dmd"],0,10);
                $date_dmd2=substr($_POST["selectingpeoplebundle_recherche"]["date_dmd"],13,10);
                $date_dmd1=new \DateTime($date_dmd1);
                $date_dmd2=new \DateTime($date_dmd2);
            }
            else{
                $dates_dmd = $em ->createQuery("SELECT MIN (dmd.date), MAX(dmd.date) FROM SelectingPeopleBundle:SpDemandeRecrutement dmd")
                    ->getScalarResult();
                $date_dmd1=new\DateTime($dates_dmd[0][1]);
                $date_dmd2=new\DateTime($dates_dmd[0][2]);
            }
            if($_POST["selectingpeoplebundle_recherche"]["reference"]!= null){
                if(($_POST["selectingpeoplebundle_recherche"]["nom_cand"]!=null)||($_POST["selectingpeoplebundle_recherche"]["date_rec"]!=null)){
                    if($_POST["selectingpeoplebundle_recherche"]["sous_trait"]!=null){
                        if($_POST["selectingpeoplebundle_recherche"]["cabinet_rec"]!=null){
                            $qb->add('select', 'dmd')
                                ->from('SelectingPeopleBundle\Entity\SpDemandeRecrutement', 'dmd')
                                ->leftjoin('SelectingPeopleBundle\Entity\SpRecrutement', 'rec', 'with', 'dmd.id = rec.dmdId')
                                ->leftjoin('SelectingPeopleBundle\Entity\SpAffectationDemande', 'aff', 'with', 'dmd.id = aff.dmdId')
                                ->join('dmd.posteBridgeId', 'bdg')
                                ->join('bdg.pst', 'post')
                                ->join('dmd.statutMissionId', 'stm')
                                ->join('bdg.dir', 'direction')
                                ->join('dmd.paysId', 'pys')
                                ->join('aff.idCabinet', 'cab')
                                ->where('dmd.posteBridgeId = bdg.bdgId')
                                ->andWhere('bdg.pst = post.pstId')
                                ->andWhere(' post.pstLibelle LIKE :postt')
                                ->andwhere('dmd.statutMissionId = stm.id')
                                ->andWhere('stm.id in (:st)')
                                ->andWhere("CONCAT(CONCAT(dmd.prenomDemandeur,' '), dmd.nomDemandeur) LIKE :demandeur")
                                ->andWhere("dmd.reference LIKE :ref")
                                ->andWhere('bdg.dir = direction.id')
                                ->andWhere('direction.id in (:d)')
                                ->andWhere('dmd.paysId = pys.id')
                                ->andWhere("CONCAT(CONCAT(pys.nomPays,' - '), dmd.ville) LIKE :py")
                                ->andWhere("CONCAT(CONCAT(rec.prenomCandidat,' '), rec.nomCandidat) LIKE :cand")
                                ->andWhere('aff.affSousTraitance = :trait')
                                ->andWhere('rec.dateRecrutement BETWEEN :date2 AND :date1')
                                ->andWhere('aff.idCabinet = cab.id')
                                ->andWhere('cab.id = :cabinet')
                                ->andWhere('dmd.date BETWEEN :date_dmd1 AND :date_dmd2')
                                ->orderBy('dmd.id', 'DESC')
                                ->setParameter('postt', '%'.$_POST["selectingpeoplebundle_recherche"]["poste"].'%')
                                ->setParameter('st', $stat)
                                ->setParameter('demandeur', '%'.$_POST["selectingpeoplebundle_recherche"]["demandeur"].'%')
                                ->setParameter('ref', '%'.$_POST["selectingpeoplebundle_recherche"]["reference"].'%')
                                ->setParameter('d', $dir)
                                ->setParameter('py', '%'.$_POST["selectingpeoplebundle_recherche"]["sec_geo"].'%')
                                ->setParameter('cand', '%'.$_POST["selectingpeoplebundle_recherche"]["nom_cand"].'%')
                                ->setParameter('trait', $trait)
                                ->setParameter('date1', $date1->format('Y-m-d'))
                                ->setParameter('date2', $date2->format('Y-m-d'))
                                ->setParameter('cabinet', $_POST["selectingpeoplebundle_recherche"]["cabinet_rec"])
                                ->setParameter('date_dmd1', $date_dmd1->format('Y-m-d'))
                                ->setParameter('date_dmd2', $date_dmd2->format('Y-m-d'));
                        }
                        elseif ($_POST["selectingpeoplebundle_recherche"]["ch_rec"]!=null){
                            $qb->add('select', 'dmd')
                                ->from('SelectingPeopleBundle\Entity\SpDemandeRecrutement', 'dmd')
                                ->leftjoin('SelectingPeopleBundle\Entity\SpRecrutement', 'rec', 'with', 'dmd.id = rec.dmdId')
                                ->leftjoin('SelectingPeopleBundle\Entity\SpAffectationDemande', 'aff', 'with', 'dmd.id = aff.dmdId')
                                ->join('dmd.posteBridgeId', 'bdg')
                                ->join('bdg.pst', 'post')
                                ->join('dmd.statutMissionId', 'stm')
                                ->join('bdg.dir', 'direction')
                                ->join('dmd.paysId', 'pys')
                                ->join('aff.idRecruteur', 'recruteur')
                                ->join('recruteur.userId', 'u')
                                ->where('dmd.posteBridgeId = bdg.bdgId')
                                ->andWhere('bdg.pst = post.pstId')
                                ->andWhere(' post.pstLibelle LIKE :postt')
                                ->andwhere('dmd.statutMissionId = stm.id')
                                ->andWhere('stm.id in (:st)')
                                ->andWhere("CONCAT(CONCAT(dmd.prenomDemandeur,' '), dmd.nomDemandeur) LIKE :demandeur")
                                ->andWhere("dmd.reference LIKE :ref")
                                ->andWhere('bdg.dir = direction.id')
                                ->andWhere('direction.id in (:d)')
                                ->andWhere('dmd.paysId = pys.id')
                                ->andWhere("CONCAT(CONCAT(pys.nomPays,' - '), dmd.ville) LIKE :py")
                                ->andWhere("CONCAT(CONCAT(rec.prenomCandidat,' '), rec.nomCandidat) LIKE :cand")
                                ->andWhere('aff.affSousTraitance = :trait')
                                ->andWhere('rec.dateRecrutement BETWEEN :date2 AND :date1')
                                ->andWhere('aff.idRecruteur = recruteur.id')
                                ->andWhere('recruteur.userId = :recruteur')
                                ->andWhere('dmd.date BETWEEN :date_dmd1 AND :date_dmd2')
                                ->orderBy('dmd.id', 'DESC')
                                ->setParameter('postt', '%'.$_POST["selectingpeoplebundle_recherche"]["poste"].'%')
                                ->setParameter('st', $stat)
                                ->setParameter('demandeur', '%'.$_POST["selectingpeoplebundle_recherche"]["demandeur"].'%')
                                ->setParameter('ref', '%'.$_POST["selectingpeoplebundle_recherche"]["reference"].'%')
                                ->setParameter('d', $dir)
                                ->setParameter('py', '%'.$_POST["selectingpeoplebundle_recherche"]["sec_geo"].'%')
                                ->setParameter('cand', '%'.$_POST["selectingpeoplebundle_recherche"]["nom_cand"].'%')
                                ->setParameter('trait', $trait)
                                ->setParameter('date1', $date1->format('Y-m-d'))
                                ->setParameter('date2', $date2->format('Y-m-d'))
                                ->setParameter('recruteur', $_POST["selectingpeoplebundle_recherche"]["ch_rec"])
                                ->setParameter('date_dmd1', $date_dmd1->format('Y-m-d'))
                                ->setParameter('date_dmd2', $date_dmd2->format('Y-m-d'));
                        }
                        else{
                            $qb->add('select', 'dmd')
                                ->from('SelectingPeopleBundle\Entity\SpDemandeRecrutement', 'dmd')
                                ->leftjoin('SelectingPeopleBundle\Entity\SpRecrutement', 'rec', 'with', 'dmd.id = rec.dmdId')
                                ->leftjoin('SelectingPeopleBundle\Entity\SpAffectationDemande', 'aff', 'with', 'dmd.id = aff.dmdId')
                                ->join('dmd.posteBridgeId', 'bdg')
                                ->join('bdg.pst', 'post')
                                ->join('dmd.statutMissionId', 'stm')
                                ->join('bdg.dir', 'direction')
                                ->join('dmd.paysId', 'pys')
                                ->where('dmd.posteBridgeId = bdg.bdgId')
                                ->andWhere('bdg.pst = post.pstId')
                                ->andWhere(' post.pstLibelle LIKE :postt')
                                ->andwhere('dmd.statutMissionId = stm.id')
                                ->andWhere('stm.id in (:st)')
                                ->andWhere("CONCAT(CONCAT(dmd.prenomDemandeur,' '), dmd.nomDemandeur) LIKE :demandeur")
                                ->andWhere("dmd.reference LIKE :ref")
                                ->andWhere('bdg.dir = direction.id')
                                ->andWhere('direction.id in (:d)')
                                ->andWhere('dmd.paysId = pys.id')
                                ->andWhere("CONCAT(CONCAT(pys.nomPays,' - '), dmd.ville) LIKE :py")
                                ->andWhere("CONCAT(CONCAT(rec.prenomCandidat,' '), rec.nomCandidat) LIKE :cand")
                                ->andWhere('aff.affSousTraitance = :trait')
                                ->andWhere('rec.dateRecrutement BETWEEN :date2 AND :date1')
                                ->andWhere('dmd.date BETWEEN :date_dmd1 AND :date_dmd2')
                                ->orderBy('dmd.id', 'DESC')
                                ->setParameter('postt', '%'.$_POST["selectingpeoplebundle_recherche"]["poste"].'%')
                                ->setParameter('st', $stat)
                                ->setParameter('demandeur', '%'.$_POST["selectingpeoplebundle_recherche"]["demandeur"].'%')
                                ->setParameter('ref', '%'.$_POST["selectingpeoplebundle_recherche"]["reference"].'%')
                                ->setParameter('d', $dir)
                                ->setParameter('py', '%'.$_POST["selectingpeoplebundle_recherche"]["sec_geo"].'%')
                                ->setParameter('cand', '%'.$_POST["selectingpeoplebundle_recherche"]["nom_cand"].'%')
                                ->setParameter('trait', $trait)
                                ->setParameter('date1', $date1->format('Y-m-d'))
                                ->setParameter('date2', $date2->format('Y-m-d'))
                                ->setParameter('date_dmd1', $date_dmd1->format('Y-m-d'))
                                ->setParameter('date_dmd2', $date_dmd2->format('Y-m-d'));
                        }
                    }
                    else{
                        $qb->add('select', 'dmd')
                            ->from('SelectingPeopleBundle\Entity\SpDemandeRecrutement', 'dmd')
                            ->leftjoin('SelectingPeopleBundle\Entity\SpRecrutement', 'rec', 'with', 'dmd.id = rec.dmdId')
                            ->join('dmd.posteBridgeId', 'bdg')
                            ->join('bdg.pst', 'post')
                            ->join('dmd.statutMissionId', 'stm')
                            ->join('bdg.dir', 'direction')
                            ->join('dmd.paysId', 'pys')
                            ->where('dmd.posteBridgeId = bdg.bdgId')
                            ->andWhere('bdg.pst = post.pstId')
                            ->andWhere(' post.pstLibelle LIKE :postt')
                            ->andwhere('dmd.statutMissionId = stm.id')
                            ->andWhere('stm.id in (:st)')
                            ->andWhere("CONCAT(CONCAT(dmd.prenomDemandeur,' '), dmd.nomDemandeur) LIKE :demandeur")
                            ->andWhere("dmd.reference LIKE :ref")
                            ->andWhere('bdg.dir = direction.id')
                            ->andWhere('direction.id in (:d)')
                            ->andWhere('dmd.paysId = pys.id')
                            ->andWhere("CONCAT(CONCAT(pys.nomPays,' - '), dmd.ville) LIKE :py")
                            ->andWhere("CONCAT(CONCAT(rec.prenomCandidat,' '), rec.nomCandidat) LIKE :cand")
                            ->andWhere('rec.dateRecrutement BETWEEN :date1 AND :date2')
                            ->andWhere('dmd.date BETWEEN :date_dmd1 AND :date_dmd2')
                            ->orderBy('dmd.id', 'DESC')
                            ->setParameter('postt', '%'.$_POST["selectingpeoplebundle_recherche"]["poste"].'%')
                            ->setParameter('st', $stat)
                            ->setParameter('demandeur', '%'.$_POST["selectingpeoplebundle_recherche"]["demandeur"].'%')
                            ->setParameter('ref', '%'.$_POST["selectingpeoplebundle_recherche"]["reference"].'%')
                            ->setParameter('d', $dir)
                            ->setParameter('py', '%'.$_POST["selectingpeoplebundle_recherche"]["sec_geo"].'%')
                            ->setParameter('cand', '%'.$_POST["selectingpeoplebundle_recherche"]["nom_cand"].'%')
                            ->setParameter('date1', $date1->format('Y-m-d'))
                            ->setParameter('date2', $date2->format('Y-m-d'))
                            ->setParameter('date_dmd1', $date_dmd1->format('Y-m-d'))
                            ->setParameter('date_dmd2', $date_dmd2->format('Y-m-d'));
                    }
                }

                if (($_POST["selectingpeoplebundle_recherche"]["sous_trait"]!=null)&&($_POST["selectingpeoplebundle_recherche"]["nom_cand"]=='')&&($_POST["selectingpeoplebundle_recherche"]["date_rec"]==null)){
                    if($_POST["selectingpeoplebundle_recherche"]["cabinet_rec"]!=null){
                        $qb->add('select', 'dmd')
                            ->from('SelectingPeopleBundle\Entity\SpDemandeRecrutement', 'dmd')
                            ->leftjoin('SelectingPeopleBundle\Entity\SpAffectationDemande', 'aff', 'with', 'dmd.id = aff.dmdId')
                            ->join('dmd.posteBridgeId', 'bdg')
                            ->join('bdg.pst', 'post')
                            ->join('dmd.statutMissionId', 'stm')
                            ->join('bdg.dir', 'direction')
                            ->join('dmd.paysId', 'pys')
                            ->join('aff.idCabinet', 'cab')
                            ->where('dmd.posteBridgeId = bdg.bdgId')
                            ->andWhere('bdg.pst = post.pstId')
                            ->andWhere(' post.pstLibelle LIKE :postt')
                            ->andwhere('dmd.statutMissionId = stm.id')
                            ->andWhere('stm.id in (:st)')
                            ->andWhere("CONCAT(CONCAT(dmd.prenomDemandeur,' '), dmd.nomDemandeur) LIKE :demandeur")
                            ->andWhere("dmd.reference LIKE :ref")
                            ->andWhere('bdg.dir = direction.id')
                            ->andWhere('direction.id in (:d)')
                            ->andWhere('dmd.paysId = pys.id')
                            ->andWhere("CONCAT(CONCAT(pys.nomPays,' - '), dmd.ville) LIKE :py")
                            ->andWhere('aff.affSousTraitance = :trait')
                            ->andWhere('aff.idCabinet = cab.id')
                            ->andWhere('cab.id = :cabinet')
                            ->andWhere('dmd.date BETWEEN :date_dmd1 AND :date_dmd2')
                            ->orderBy('dmd.id', 'DESC')
                            ->setParameter('postt', '%'.$_POST["selectingpeoplebundle_recherche"]["poste"].'%')
                            ->setParameter('st', $stat)
                            ->setParameter('demandeur', '%'.$_POST["selectingpeoplebundle_recherche"]["demandeur"].'%')
                            ->setParameter('ref', '%'.$_POST["selectingpeoplebundle_recherche"]["reference"].'%')
                            ->setParameter('d', $dir)
                            ->setParameter('py', '%'.$_POST["selectingpeoplebundle_recherche"]["sec_geo"].'%')
                            ->setParameter('trait', $trait)
                            ->setParameter('cabinet', $_POST["selectingpeoplebundle_recherche"]["cabinet_rec"])
                            ->setParameter('date_dmd1', $date_dmd1->format('Y-m-d'))
                            ->setParameter('date_dmd2', $date_dmd2->format('Y-m-d'));
                    }
                    elseif ($_POST["selectingpeoplebundle_recherche"]["ch_rec"]!=null){
                        $qb->add('select', 'dmd')
                            ->from('SelectingPeopleBundle\Entity\SpDemandeRecrutement', 'dmd')
                            ->leftjoin('SelectingPeopleBundle\Entity\SpAffectationDemande', 'aff', 'with', 'dmd.id = aff.dmdId')
                            ->join('dmd.posteBridgeId', 'bdg')
                            ->join('bdg.pst', 'post')
                            ->join('dmd.statutMissionId', 'stm')
                            ->join('bdg.dir', 'direction')
                            ->join('dmd.paysId', 'pys')
                            ->join('aff.idRecruteur', 'recruteur')
                            ->join('recruteur.userId', 'u')
                            ->where('dmd.posteBridgeId = bdg.bdgId')
                            ->andWhere('bdg.pst = post.pstId')
                            ->andWhere(' post.pstLibelle LIKE :postt')
                            ->andwhere('dmd.statutMissionId = stm.id')
                            ->andWhere('stm.id in (:st)')
                            ->andWhere("CONCAT(CONCAT(dmd.prenomDemandeur,' '), dmd.nomDemandeur) LIKE :demandeur")
                            ->andWhere("dmd.reference LIKE :ref")
                            ->andWhere('bdg.dir = direction.id')
                            ->andWhere('direction.id in (:d)')
                            ->andWhere('dmd.paysId = pys.id')
                            ->andWhere("CONCAT(CONCAT(pys.nomPays,' - '), dmd.ville) LIKE :py")
                            ->andWhere('aff.affSousTraitance = :trait')
                            ->andWhere('aff.idRecruteur = recruteur.id')
                            ->andWhere('recruteur.userId = :recruteur')
                            ->andWhere('dmd.date BETWEEN :date_dmd1 AND :date_dmd2')
                            ->orderBy('dmd.id', 'DESC')
                            ->setParameter('postt', '%'.$_POST["selectingpeoplebundle_recherche"]["poste"].'%')
                            ->setParameter('st', $stat)
                            ->setParameter('demandeur', '%'.$_POST["selectingpeoplebundle_recherche"]["demandeur"].'%')
                            ->setParameter('ref', '%'.$_POST["selectingpeoplebundle_recherche"]["reference"].'%')
                            ->setParameter('d', $dir)
                            ->setParameter('py', '%'.$_POST["selectingpeoplebundle_recherche"]["sec_geo"].'%')
                            ->setParameter('trait', $trait)
                            ->setParameter('recruteur', $_POST["selectingpeoplebundle_recherche"]["ch_rec"])
                            ->setParameter('date_dmd1', $date_dmd1->format('Y-m-d'))
                            ->setParameter('date_dmd2', $date_dmd2->format('Y-m-d'));
                    }
                    else{
                        $qb->add('select', 'dmd')
                            ->from('SelectingPeopleBundle\Entity\SpDemandeRecrutement', 'dmd')
                            ->leftjoin('SelectingPeopleBundle\Entity\SpAffectationDemande', 'aff', 'with', 'dmd.id = aff.dmdId')
                            ->join('dmd.posteBridgeId', 'bdg')
                            ->join('bdg.pst', 'post')
                            ->join('dmd.statutMissionId', 'stm')
                            ->join('bdg.dir', 'direction')
                            ->join('dmd.paysId', 'pys')
                            ->where('dmd.posteBridgeId = bdg.bdgId')
                            ->andWhere('bdg.pst = post.pstId')
                            ->andWhere(' post.pstLibelle LIKE :postt')
                            ->andwhere('dmd.statutMissionId = stm.id')
                            ->andWhere('stm.id in (:st)')
                            ->andWhere("CONCAT(CONCAT(dmd.prenomDemandeur,' '), dmd.nomDemandeur) LIKE :demandeur")
                            ->andWhere("dmd.reference LIKE :ref")
                            ->andWhere('bdg.dir = direction.id')
                            ->andWhere('direction.id in (:d)')
                            ->andWhere('dmd.paysId = pys.id')
                            ->andWhere("CONCAT(CONCAT(pys.nomPays,' - '), dmd.ville) LIKE :py")
                            ->andWhere('aff.affSousTraitance = :trait')
                            ->andWhere('dmd.date BETWEEN :date_dmd1 AND :date_dmd2')
                            ->orderBy('dmd.id', 'DESC')
                            ->setParameter('postt', '%'.$_POST["selectingpeoplebundle_recherche"]["poste"].'%')
                            ->setParameter('st', $stat)
                            ->setParameter('demandeur', '%'.$_POST["selectingpeoplebundle_recherche"]["demandeur"].'%')
                            ->setParameter('ref', '%'.$_POST["selectingpeoplebundle_recherche"]["reference"].'%')
                            ->setParameter('d', $dir)
                            ->setParameter('py', '%'.$_POST["selectingpeoplebundle_recherche"]["sec_geo"].'%')
                            ->setParameter('trait', $trait)
                            ->setParameter('date_dmd1', $date_dmd1->format('Y-m-d'))
                            ->setParameter('date_dmd2', $date_dmd2->format('Y-m-d'));
                    }
                }
                if(($_POST["selectingpeoplebundle_recherche"]["sous_trait"]==null)&&($_POST["selectingpeoplebundle_recherche"]["nom_cand"]==null)&&($_POST["selectingpeoplebundle_recherche"]["date_rec"]==null)){
                    $qb->add('select', 'dmd')
                        ->from('SelectingPeopleBundle\Entity\SpDemandeRecrutement', 'dmd')
                        ->join('dmd.posteBridgeId', 'bdg')
                        ->join('bdg.pst', 'post')
                        ->join('dmd.statutMissionId', 'stm')
                        ->join('bdg.dir', 'direction')
                        ->join('dmd.paysId', 'pys')
                        ->where('dmd.posteBridgeId = bdg.bdgId')
                        ->andWhere('bdg.pst = post.pstId')
                        ->andWhere(' post.pstLibelle LIKE :postt')
                        ->andwhere('dmd.statutMissionId = stm.id')
                        ->andWhere('stm.id in (:st)')
                        ->andWhere("CONCAT(CONCAT(dmd.prenomDemandeur,' '), dmd.nomDemandeur) LIKE :demandeur")
                        ->andWhere("dmd.reference LIKE :ref")
                        ->andWhere('bdg.dir = direction.id')
                        ->andWhere('direction.id in (:d)')
                        ->andWhere('dmd.paysId = pys.id')
                        ->andWhere("CONCAT(CONCAT(pys.nomPays,' - '), dmd.ville) LIKE :py")
                        ->andWhere('dmd.date BETWEEN :date_dmd1 AND :date_dmd2')
                        ->orderBy('dmd.id', 'DESC')
                        ->setParameter('postt', '%'.$_POST["selectingpeoplebundle_recherche"]["poste"].'%')
                        ->setParameter('st', $stat)
                        ->setParameter('demandeur', '%'.$_POST["selectingpeoplebundle_recherche"]["demandeur"].'%')
                        ->setParameter('ref', '%'.$_POST["selectingpeoplebundle_recherche"]["reference"].'%')
                        ->setParameter('d', $dir)
                        ->setParameter('py', '%'.$_POST["selectingpeoplebundle_recherche"]["sec_geo"].'%')
                        ->setParameter('date_dmd1', $date_dmd1->format('Y-m-d'))
                        ->setParameter('date_dmd2', $date_dmd2->format('Y-m-d'));
                }
            }

            // Sans reference
            else{
                if(($_POST["selectingpeoplebundle_recherche"]["nom_cand"]!=null)||($_POST["selectingpeoplebundle_recherche"]["date_rec"]!=null)){
                    if($_POST["selectingpeoplebundle_recherche"]["sous_trait"]!=null){
                        if($_POST["selectingpeoplebundle_recherche"]["cabinet_rec"]!=null){
                            $qb->add('select', 'dmd')
                                ->from('SelectingPeopleBundle\Entity\SpDemandeRecrutement', 'dmd')
                                ->leftjoin('SelectingPeopleBundle\Entity\SpRecrutement', 'rec', 'with', 'dmd.id = rec.dmdId')
                                ->leftjoin('SelectingPeopleBundle\Entity\SpAffectationDemande', 'aff', 'with', 'dmd.id = aff.dmdId')
                                ->join('dmd.posteBridgeId', 'bdg')
                                ->join('bdg.pst', 'post')
                                ->join('dmd.statutMissionId', 'stm')
                                ->join('bdg.dir', 'direction')
                                ->join('dmd.paysId', 'pys')
                                ->join('aff.idCabinet', 'cab')
                                ->where('dmd.posteBridgeId = bdg.bdgId')
                                ->andWhere('bdg.pst = post.pstId')
                                ->andWhere(' post.pstLibelle LIKE :postt')
                                ->andwhere('dmd.statutMissionId = stm.id')
                                ->andWhere('stm.id in (:st)')
                                ->andWhere("CONCAT(CONCAT(dmd.prenomDemandeur,' '), dmd.nomDemandeur) LIKE :demandeur")
                                ->andWhere('bdg.dir = direction.id')
                                ->andWhere('direction.id in (:d)')
                                ->andWhere('dmd.paysId = pys.id')
                                ->andWhere("CONCAT(CONCAT(pys.nomPays,' - '), dmd.ville) LIKE :py")
                                ->andWhere("CONCAT(CONCAT(rec.prenomCandidat,' '), rec.nomCandidat) LIKE :cand")
                                ->andWhere('aff.affSousTraitance = :trait')
                                ->andWhere('rec.dateRecrutement BETWEEN :date1 AND :date2')
                                ->andWhere('aff.idCabinet = cab.id')
                                ->andWhere('cab.id = :cabinet')
                                ->andWhere('dmd.date BETWEEN :date_dmd1 AND :date_dmd2')
                                ->orderBy('dmd.id', 'DESC')
                                ->setParameter('postt', '%'.$_POST["selectingpeoplebundle_recherche"]["poste"].'%')
                                ->setParameter('st', $stat)
                                ->setParameter('demandeur', '%'.$_POST["selectingpeoplebundle_recherche"]["demandeur"].'%')
                                ->setParameter('d', $dir)
                                ->setParameter('py', '%'.$_POST["selectingpeoplebundle_recherche"]["sec_geo"].'%')
                                ->setParameter('cand', '%'.$_POST["selectingpeoplebundle_recherche"]["nom_cand"].'%')
                                ->setParameter('trait', $trait)
                                ->setParameter('date1', $date1->format('Y-m-d'))
                                ->setParameter('date2', $date2->format('Y-m-d'))
                                ->setParameter('cabinet', $_POST["selectingpeoplebundle_recherche"]["cabinet_rec"])
                                ->setParameter('date_dmd1', $date_dmd1->format('Y-m-d'))
                                ->setParameter('date_dmd2', $date_dmd2->format('Y-m-d'));
                        }
                        elseif ($_POST["selectingpeoplebundle_recherche"]["ch_rec"]!=null){
                            $qb->add('select', 'dmd')
                                ->from('SelectingPeopleBundle\Entity\SpDemandeRecrutement', 'dmd')
                                ->leftjoin('SelectingPeopleBundle\Entity\SpRecrutement', 'rec', 'with', 'dmd.id = rec.dmdId')
                                ->leftjoin('SelectingPeopleBundle\Entity\SpAffectationDemande', 'aff', 'with', 'dmd.id = aff.dmdId')
                                ->join('dmd.posteBridgeId', 'bdg')
                                ->join('bdg.pst', 'post')
                                ->join('dmd.statutMissionId', 'stm')
                                ->join('bdg.dir', 'direction')
                                ->join('dmd.paysId', 'pys')
                                ->join('aff.idRecruteur', 'recruteur')
                                ->join('recruteur.userId', 'u')
                                ->where('dmd.posteBridgeId = bdg.bdgId')
                                ->andWhere('bdg.pst = post.pstId')
                                ->andWhere(' post.pstLibelle LIKE :postt')
                                ->andwhere('dmd.statutMissionId = stm.id')
                                ->andWhere('stm.id in (:st)')
                                ->andWhere("CONCAT(CONCAT(dmd.prenomDemandeur,' '), dmd.nomDemandeur) LIKE :demandeur")
                                ->andWhere('bdg.dir = direction.id')
                                ->andWhere('direction.id in (:d)')
                                ->andWhere('dmd.paysId = pys.id')
                                ->andWhere("CONCAT(CONCAT(pys.nomPays,' - '), dmd.ville) LIKE :py")
                                ->andWhere("CONCAT(CONCAT(rec.prenomCandidat,' '), rec.nomCandidat) LIKE :cand")
                                ->andWhere('aff.affSousTraitance = :trait')
                                ->andWhere('rec.dateRecrutement BETWEEN :date1 AND :date2')
                                ->andWhere('aff.idRecruteur = recruteur.id')
                                ->andWhere('recruteur.userId = :recruteur')
                                ->andWhere('dmd.date BETWEEN :date_dmd1 AND :date_dmd2')
                                ->orderBy('dmd.id', 'DESC')
                                ->setParameter('postt', '%'.$_POST["selectingpeoplebundle_recherche"]["poste"].'%')
                                ->setParameter('st', $stat)
                                ->setParameter('demandeur', '%'.$_POST["selectingpeoplebundle_recherche"]["demandeur"].'%')
                                ->setParameter('d', $dir)
                                ->setParameter('py', '%'.$_POST["selectingpeoplebundle_recherche"]["sec_geo"].'%')
                                ->setParameter('cand', '%'.$_POST["selectingpeoplebundle_recherche"]["nom_cand"].'%')
                                ->setParameter('trait', $trait)
                                ->setParameter('date1', $date1->format('Y-m-d'))
                                ->setParameter('date2', $date2->format('Y-m-d'))
                                ->setParameter('recruteur', $_POST["selectingpeoplebundle_recherche"]["ch_rec"])
                                ->setParameter('date_dmd1', $date_dmd1->format('Y-m-d'))
                                ->setParameter('date_dmd2', $date_dmd2->format('Y-m-d'));
                        }
                        else{
                            $qb->add('select', 'dmd')
                                ->from('SelectingPeopleBundle\Entity\SpDemandeRecrutement', 'dmd')
                                ->leftjoin('SelectingPeopleBundle\Entity\SpRecrutement', 'rec', 'with', 'dmd.id = rec.dmdId')
                                ->leftjoin('SelectingPeopleBundle\Entity\SpAffectationDemande', 'aff', 'with', 'dmd.id = aff.dmdId')
                                ->join('dmd.posteBridgeId', 'bdg')
                                ->join('bdg.pst', 'post')
                                ->join('dmd.statutMissionId', 'stm')
                                ->join('bdg.dir', 'direction')
                                ->join('dmd.paysId', 'pys')
                                ->where('dmd.posteBridgeId = bdg.bdgId')
                                ->andWhere('bdg.pst = post.pstId')
                                ->andWhere(' post.pstLibelle LIKE :postt')
                                ->andwhere('dmd.statutMissionId = stm.id')
                                ->andWhere('stm.id in (:st)')
                                ->andWhere("CONCAT(CONCAT(dmd.prenomDemandeur,' '), dmd.nomDemandeur) LIKE :demandeur")
                                ->andWhere('bdg.dir = direction.id')
                                ->andWhere('direction.id in (:d)')
                                ->andWhere('dmd.paysId = pys.id')
                                ->andWhere("CONCAT(CONCAT(pys.nomPays,' - '), dmd.ville) LIKE :py")
                                ->andWhere("CONCAT(CONCAT(rec.prenomCandidat,' '), rec.nomCandidat) LIKE :cand")
                                ->andWhere('aff.affSousTraitance = :trait')
                                ->andWhere('rec.dateRecrutement BETWEEN :date1 AND :date2')
                                ->andWhere('dmd.date BETWEEN :date_dmd1 AND :date_dmd2')
                                ->orderBy('dmd.id', 'DESC')
                                ->setParameter('postt', '%'.$_POST["selectingpeoplebundle_recherche"]["poste"].'%')
                                ->setParameter('st', $stat)
                                ->setParameter('demandeur', '%'.$_POST["selectingpeoplebundle_recherche"]["demandeur"].'%')
                                ->setParameter('d', $dir)
                                ->setParameter('py', '%'.$_POST["selectingpeoplebundle_recherche"]["sec_geo"].'%')
                                ->setParameter('cand', '%'.$_POST["selectingpeoplebundle_recherche"]["nom_cand"].'%')
                                ->setParameter('trait', $trait)
                                ->setParameter('date1', $date1->format('Y-m-d'))
                                ->setParameter('date2', $date2->format('Y-m-d'))
                                ->setParameter('date_dmd1', $date_dmd1->format('Y-m-d'))
                                ->setParameter('date_dmd2', $date_dmd2->format('Y-m-d'));
                        }
                    }
                    else{
                        $qb->add('select', 'dmd')
                            ->from('SelectingPeopleBundle\Entity\SpDemandeRecrutement', 'dmd')
                            ->leftjoin('SelectingPeopleBundle\Entity\SpRecrutement', 'rec', 'with', 'dmd.id = rec.dmdId')
                            ->join('dmd.posteBridgeId', 'bdg')
                            ->join('bdg.pst', 'post')
                            ->join('dmd.statutMissionId', 'stm')
                            ->join('bdg.dir', 'direction')
                            ->join('dmd.paysId', 'pys')
                            ->where('dmd.posteBridgeId = bdg.bdgId')
                            ->andWhere('bdg.pst = post.pstId')
                            ->andWhere(' post.pstLibelle LIKE :postt')
                            ->andwhere('dmd.statutMissionId = stm.id')
                            ->andWhere('stm.id in (:st)')
                            ->andWhere("CONCAT(CONCAT(dmd.prenomDemandeur,' '), dmd.nomDemandeur) LIKE :demandeur")
                            ->andWhere('bdg.dir = direction.id')
                            ->andWhere('direction.id in (:d)')
                            ->andWhere('dmd.paysId = pys.id')
                            ->andWhere("CONCAT(CONCAT(pys.nomPays,' - '), dmd.ville) LIKE :py")
                            ->andWhere("CONCAT(CONCAT(rec.prenomCandidat,' '), rec.nomCandidat) LIKE :cand")
                            ->andWhere('rec.dateRecrutement BETWEEN :date1 AND :date2')
                            ->andWhere('dmd.date BETWEEN :date_dmd1 AND :date_dmd2')
                            ->orderBy('dmd.id', 'DESC')
                            ->setParameter('postt', '%'.$_POST["selectingpeoplebundle_recherche"]["poste"].'%')
                            ->setParameter('st', $stat)
                            ->setParameter('demandeur', '%'.$_POST["selectingpeoplebundle_recherche"]["demandeur"].'%')
                            ->setParameter('d', $dir)
                            ->setParameter('py', '%'.$_POST["selectingpeoplebundle_recherche"]["sec_geo"].'%')
                            ->setParameter('cand', '%'.$_POST["selectingpeoplebundle_recherche"]["nom_cand"].'%')
                            ->setParameter('date1', $date1->format('Y-m-d'))
                            ->setParameter('date2', $date2->format('Y-m-d'))
                            ->setParameter('date_dmd1', $date_dmd1->format('Y-m-d'))
                            ->setParameter('date_dmd2', $date_dmd2->format('Y-m-d'));
                    }

                }
                if (($_POST["selectingpeoplebundle_recherche"]["sous_trait"]!=null)&&($_POST["selectingpeoplebundle_recherche"]["nom_cand"]==null)&&($_POST["selectingpeoplebundle_recherche"]["date_rec"]==null)){
                    if($_POST["selectingpeoplebundle_recherche"]["cabinet_rec"]!=null){
                        $qb->add('select', 'dmd')
                            ->from('SelectingPeopleBundle\Entity\SpDemandeRecrutement', 'dmd')
                            ->leftjoin('SelectingPeopleBundle\Entity\SpAffectationDemande', 'aff', 'with', 'dmd.id = aff.dmdId')
                            ->join('dmd.posteBridgeId', 'bdg')
                            ->join('bdg.pst', 'post')
                            ->join('dmd.statutMissionId', 'stm')
                            ->join('bdg.dir', 'direction')
                            ->join('dmd.paysId', 'pys')
                            ->join('aff.idCabinet', 'cab')
                            ->where('dmd.posteBridgeId = bdg.bdgId')
                            ->andWhere('bdg.pst = post.pstId')
                            ->andWhere(' post.pstLibelle LIKE :postt')
                            ->andwhere('dmd.statutMissionId = stm.id')
                            ->andWhere('stm.id in (:st)')
                            ->andWhere("CONCAT(CONCAT(dmd.prenomDemandeur,' '), dmd.nomDemandeur) LIKE :demandeur")
                            ->andWhere('bdg.dir = direction.id')
                            ->andWhere('direction.id in (:d)')
                            ->andWhere('dmd.paysId = pys.id')
                            ->andWhere("CONCAT(CONCAT(pys.nomPays,' - '), dmd.ville) LIKE :py")
                            ->andWhere('aff.affSousTraitance = :trait')
                            ->andWhere('aff.idCabinet = cab.id')
                            ->andWhere('cab.id = :cabinet')
                            ->andWhere('dmd.date BETWEEN :date_dmd1 AND :date_dmd2')
                            ->orderBy('dmd.id', 'DESC')
                            ->setParameter('postt', '%'.$_POST["selectingpeoplebundle_recherche"]["poste"].'%')
                            ->setParameter('st', $stat)
                            ->setParameter('demandeur', '%'.$_POST["selectingpeoplebundle_recherche"]["demandeur"].'%')
                            ->setParameter('d', $dir)
                            ->setParameter('py', '%'.$_POST["selectingpeoplebundle_recherche"]["sec_geo"].'%')
                            ->setParameter('trait', $trait)
                            ->setParameter('cabinet', $_POST["selectingpeoplebundle_recherche"]["cabinet_rec"])
                            ->setParameter('date_dmd1', $date_dmd1->format('Y-m-d'))
                            ->setParameter('date_dmd2', $date_dmd2->format('Y-m-d'));
                    }
                    elseif ($_POST["selectingpeoplebundle_recherche"]["ch_rec"]!=null){
                        $qb->add('select', 'dmd')
                            ->from('SelectingPeopleBundle\Entity\SpDemandeRecrutement', 'dmd')
                            ->leftjoin('SelectingPeopleBundle\Entity\SpAffectationDemande', 'aff', 'with', 'dmd.id = aff.dmdId')
                            ->join('dmd.posteBridgeId', 'bdg')
                            ->join('bdg.pst', 'post')
                            ->join('dmd.statutMissionId', 'stm')
                            ->join('bdg.dir', 'direction')
                            ->join('dmd.paysId', 'pys')
                            ->join('aff.idRecruteur', 'recruteur')
                            ->join('recruteur.userId', 'u')
                            ->where('dmd.posteBridgeId = bdg.bdgId')
                            ->andWhere('bdg.pst = post.pstId')
                            ->andWhere(' post.pstLibelle LIKE :postt')
                            ->andwhere('dmd.statutMissionId = stm.id')
                            ->andWhere('stm.id in (:st)')
                            ->andWhere("CONCAT(CONCAT(dmd.prenomDemandeur,' '), dmd.nomDemandeur) LIKE :demandeur")
                            ->andWhere('bdg.dir = direction.id')
                            ->andWhere('direction.id in (:d)')
                            ->andWhere('dmd.paysId = pys.id')
                            ->andWhere("CONCAT(CONCAT(pys.nomPays,' - '), dmd.ville) LIKE :py")
                            ->andWhere('aff.affSousTraitance = :trait')
                            ->andWhere('aff.idRecruteur = recruteur.id')
                            ->andWhere('recruteur.userId = :recruteur')
                            ->andWhere('dmd.date BETWEEN :date_dmd1 AND :date_dmd2')
                            ->orderBy('dmd.id', 'DESC')
                            ->setParameter('postt', '%'.$_POST["selectingpeoplebundle_recherche"]["poste"].'%')
                            ->setParameter('st', $stat)
                            ->setParameter('demandeur', '%'.$_POST["selectingpeoplebundle_recherche"]["demandeur"].'%')
                            ->setParameter('d', $dir)
                            ->setParameter('py', '%'.$_POST["selectingpeoplebundle_recherche"]["sec_geo"].'%')
                            ->setParameter('trait', $trait)
                            ->setParameter('recruteur', $_POST["selectingpeoplebundle_recherche"]["ch_rec"])
                            ->setParameter('date_dmd1', $date_dmd1->format('Y-m-d'))
                            ->setParameter('date_dmd2', $date_dmd2->format('Y-m-d'));
                    }
                    else{

                        $qb->add('select', 'dmd')
                            ->from('SelectingPeopleBundle\Entity\SpDemandeRecrutement', 'dmd')
                            ->leftjoin('SelectingPeopleBundle\Entity\SpAffectationDemande', 'aff', 'with', 'dmd.id = aff.dmdId')
                            ->join('dmd.posteBridgeId', 'bdg')
                            ->join('bdg.pst', 'post')
                            ->join('dmd.statutMissionId', 'stm')
                            ->join('bdg.dir', 'direction')
                            ->join('dmd.paysId', 'pys')
                            ->where('aff.affSousTraitance = :trait')
                            ->andwhere('dmd.posteBridgeId = bdg.bdgId')
                            ->andWhere('bdg.pst = post.pstId')
                            ->andWhere(' post.pstLibelle LIKE :postt')
                            ->andwhere('dmd.statutMissionId = stm.id')
                            ->andWhere('stm.id in (:st)')
                            ->andWhere("CONCAT(CONCAT(dmd.prenomDemandeur,' '), dmd.nomDemandeur) LIKE :demandeur")
                            ->andWhere('bdg.dir = direction.id')
                            ->andWhere('direction.id in (:d)')
                            ->andWhere('dmd.paysId = pys.id')
                            ->andWhere("CONCAT(CONCAT(pys.nomPays,' - '), dmd.ville) LIKE :py")
                            ->andWhere('dmd.date BETWEEN :date_dmd1 AND :date_dmd2')
                            ->orderBy('dmd.id', 'DESC')
                            ->setParameter('postt', '%'.$_POST["selectingpeoplebundle_recherche"]["poste"].'%')
                            ->setParameter('st', $stat)
                            ->setParameter('demandeur', '%'.$_POST["selectingpeoplebundle_recherche"]["demandeur"].'%')
                            ->setParameter('d', $dir)
                            ->setParameter('py', '%'.$_POST["selectingpeoplebundle_recherche"]["sec_geo"].'%')
                            ->setParameter('trait', $trait)
                            ->setParameter('date_dmd1', $date_dmd1->format('Y-m-d'))
                            ->setParameter('date_dmd2', $date_dmd2->format('Y-m-d'));
                    }

                }
                if(($_POST["selectingpeoplebundle_recherche"]["sous_trait"]==null)&&($_POST["selectingpeoplebundle_recherche"]["nom_cand"]==null)&&($_POST["selectingpeoplebundle_recherche"]["date_rec"]==null)) {
                    $qb->add('select', 'dmd')
                        ->from('SelectingPeopleBundle\Entity\SpDemandeRecrutement', 'dmd')
                        ->join('dmd.posteBridgeId', 'bdg')
                        ->join('bdg.pst', 'post')
                        ->join('dmd.statutMissionId', 'stm')
                        ->join('bdg.dir', 'direction')
                        ->join('dmd.paysId', 'pys')
                        ->where('dmd.posteBridgeId = bdg.bdgId')
                        ->andWhere('bdg.pst = post.pstId')
                        ->andWhere(' post.pstLibelle LIKE :postt')
                        ->andwhere('dmd.statutMissionId = stm.id')
                        ->andWhere('stm.id in (:st)')
                        ->andWhere("CONCAT(CONCAT(dmd.prenomDemandeur,' '), dmd.nomDemandeur) LIKE :demandeur")
                        ->andWhere('bdg.dir = direction.id')
                        ->andWhere('direction.id in (:d)')
                        ->andWhere('dmd.paysId = pys.id')
                        ->andWhere("CONCAT(CONCAT(pys.nomPays,' - '), dmd.ville) LIKE :py")
                        ->andWhere('dmd.date BETWEEN :date_dmd1 AND :date_dmd2')
                        ->orderBy('dmd.id', 'DESC')
                        ->setParameter('postt', '%'.$_POST["selectingpeoplebundle_recherche"]["poste"].'%')
                        ->setParameter('st', $stat)
                        ->setParameter('demandeur', '%'.$_POST["selectingpeoplebundle_recherche"]["demandeur"].'%')
                        ->setParameter('d', $dir)
                        ->setParameter('py', '%'.$_POST["selectingpeoplebundle_recherche"]["sec_geo"].'%')
                        ->setParameter('date_dmd1', $date_dmd1->format('Y-m-d'))
                        ->setParameter('date_dmd2', $date_dmd2->format('Y-m-d'));

                }

            }

            $demandes =$qb->getQuery()
                ->setFetchMode("SelectingPeopleBundle\Entity\SpDemandeRecrutement", "elements", "EAGER")
                ->getResult();


            $result=array();
            $res=array();
            $affectation=array();
            $aff_dmd_array=array();
            $rec1_array=array();
            $rec2_array=array();
            $i=0;
            foreach ($demandes as $dmd){
                $aff= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpAffectationDemande")-> findOneBy(array('dmdId'=>$dmd->getId(),'premierRecruteur'=>'1'));
                $aff_dmd= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpAffectationDemande")-> findOneBy(array('dmdId'=>$dmd->getId()),
                    array('id' => 'DESC'));
                $rec1= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpRecrutement")-> findOneBy(array('dmdId'=>$dmd->getId(),'numero'=>'1'));
                $rec2= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpRecrutement")-> findOneBy(array('dmdId'=>$dmd->getId(),'numero'=>'2'));
                $d1=$dmd->getDate();
                if($rec1!=null){
                    $d2=$rec1->getDateRelance();
                    $d_recrutement=$rec1->getDateRecrutement();
                    $duree=$d1->diff($d_recrutement)->format('%m');
                }
                else{
                    $d2=null;
                    $duree='';
                }
                $time = new \DateTime();
                if ((($dmd->getStatutMissionId()->getId()==1) and ($d1->diff($time)->format('%m')>3)) or (($dmd->getStatutMissionId()->getId()==5) and ($d2!=null) and ($d2->diff($time)->format('%m')>3 ))){
                    $mission_difficile= 'Oui';
                } else {
                    $mission_difficile= '';
                }


                $res[]=array("id_dmd"=>$dmd->getId(),"mission_difficile"=>$mission_difficile,"poste"=>$dmd->getPosteBridgeId()->getPst()->getPstLibelle(),
                    "pays"=> ($dmd->getPaysId()!=null) ? $res["pays"]=$dmd->getPaysId()->getNomPays() : '',"demandeur"=>$dmd->getNomDemandeur()." ".$dmd->getPrenomDemandeur(),
                    "direction"=>$dmd->getPosteBridgeId()->getDir()->getLibelle(),"date_dmd"=>$dmd->getDate()->format('Y-m-d'),
                    "charge_rech"=>($dmd->getChargeRechercheId()!=null) ? $res["charge_rech"]=$dmd->getChargeRechercheId()->getUserId()->getDisplayName() : '',
                    "reference"=>$dmd->getReference(),"statut"=>$dmd->getStatutMissionId()->getStatut(),"confidentiel"=>$dmd->getConfidentiel(),
                    "service"=>$dmd->getPosteBridgeId()->getSrv()->getLibelle(),
                    "pole" => ($dmd->getPosteBridgeId()->getPol()!=null) ? $res["pole"]=$dmd->getPosteBridgeId()->getPol()->getPolLibelle() : '',
                    "societe"=>$dmd->getSteId()->getLibelle(),"Nsociete"=>$dmd->getSteId()->getReference(),"salaire"=>$dmd->getSalaire(),
                    "accompte"=>$dmd->getAccompte(),"solde"=>$dmd->getSolde(),"duree_rec"=>$duree,
                    "comment_dem"=>$dmd->getCommentaire(),"comment_dir"=>$dmd->getCommentaireSp(),
                    "statut_id"=>$dmd->getStatutMissionId()->getId(),"ville"=>$dmd->getVille());

                if($aff!=null){
                    $affectation[]=array("id_dmd"=>$aff->getDmdId()->getId(),"r1_recruteur"=>($aff->getIdRecruteur()!=null) ? $affectation["r1_recruteur"]=$aff->getIdRecruteur()->getUserId()->getDisplayName() : $aff->getIdCabinet()->getNomCabinet());
                    if($aff_dmd!=null){
                        $aff_dmd_array[]=array("id_dmd"=>$dmd->getId(),
                            "sous_traitance" => ($aff_dmd->getAffSousTraitance()== 1) ? $aff_dmd_array["sous_traitance"]='Non' : 'Oui',
                            "charge_rec"=>($aff_dmd->getIdRecruteur()!=null) ? $aff_dmd_array["charge_rec"]=$aff_dmd->getIdRecruteur()->getUserId()->getDisplayName() : $aff_dmd->getIdCabinet()->getNomCabinet() );
                        if ($rec2!=null){
                            $rec2_array[]=array("id_dmd"=>$dmd->getId(),"r2_nom_cand"=>$rec2->getNomCandidat()." ".$rec2->getPrenomCandidat(),
                                "r2_date_rec"=>$rec2->getDateRecrutement(),"r2_date_integration"=>$rec2->getDateIntegration(),"r2_enposte"=>$rec2->getEnPoste());
                            $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>$rec1->getNomCandidat()." ".$rec1->getPrenomCandidat(),
                                "r1_date_rec"=>$rec1->getDateRecrutement(),"r1_date_integration"=>$rec1->getDateIntegration(),
                                'r1_enposte'=>$rec1->getEnPoste(),"r1_peride_conf"=>$rec1->getPeriodeConfirmee(),
                                "r1_garantie"=>$rec1->getGarantie(),"r1_date_relance"=>$rec1->getDateRelance());
                        }
                        else{
                            $rec2_array[]=array("id_dmd"=>$dmd->getId(),"r2_nom_cand"=>'',
                                "r2_date_rec"=>'',"r2_date_integration"=>'',"r2_enposte"=>'');
                            if ($rec1!=null){
                                $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>$rec1->getNomCandidat()." ".$rec1->getPrenomCandidat(),
                                    "r1_date_rec"=>$rec1->getDateRecrutement(),"r1_date_integration"=>$rec1->getDateIntegration(),
                                    'r1_enposte'=>$rec1->getEnPoste(),"r1_peride_conf"=>$rec1->getPeriodeConfirmee(),
                                    "r1_garantie"=>$rec1->getGarantie(),"r1_date_relance"=>$rec1->getDateRelance());
                            }
                            else{
                                $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>'',
                                    "r1_date_rec"=>'',"r1_date_integration"=>'',
                                    'r1_enposte'=>'',"r1_peride_conf"=>'',
                                    "r1_garantie"=>'',"r1_date_relance"=>'');
                            }
                        }

                    }
                    else{
                        $aff_dmd_array[]=array("id_dmd"=>$aff->getDmdId()->getId(),"sous_traitance"=> '', "charge_rec"=> '');
                        if ($rec2!=null){
                            $rec2_array[]=array("id_dmd"=>$dmd->getId(),"r2_nom_cand"=>$rec2->getNomCandidat()." ".$rec2->getPrenomCandidat(),
                                "r2_date_rec"=>$rec2->getDateRecrutement(),"r2_date_integration"=>$rec2->getDateIntegration(),"r2_enposte"=>$rec2->getEnPoste());
                            if ($rec1!=null){
                                $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>$rec1->getNomCandidat()." ".$rec1->getPrenomCandidat(),
                                    "r1_date_rec"=>$rec1->getDateRecrutement(),"r1_date_integration"=>$rec1->getDateIntegration(),
                                    'r1_enposte'=>$rec1->getEnPoste(),"r1_peride_conf"=>$rec1->getPeriodeConfirmee(),
                                    "r1_garantie"=>$rec1->getGarantie(),"r1_date_relance"=>$rec1->getDateRelance());
                            }
                            else{
                                $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>'',
                                    "r1_date_rec"=>'',"r1_date_integration"=>'',
                                    'r1_enposte'=>'',"r1_peride_conf"=>'',
                                    "r1_garantie"=>'',"r1_date_relance"=>'');
                            }
                        }
                        else{
                            $rec2_array[]=array("id_dmd"=>$dmd->getId(),"r2_nom_cand"=>'',
                                "r2_date_rec"=>'',"r2_date_integration"=>'',"r2_enposte"=>'');
                            if ($rec1!=null){
                                $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>$rec1->getNomCandidat()." ".$rec1->getPrenomCandidat(),
                                    "r1_date_rec"=>$rec1->getDateRecrutement(),"r1_date_integration"=>$rec1->getDateIntegration(),
                                    'r1_enposte'=>$rec1->getEnPoste(),"r1_peride_conf"=>$rec1->getPeriodeConfirmee(),
                                    "r1_garantie"=>$rec1->getGarantie(),"r1_date_relance"=>$rec1->getDateRelance());
                            }
                            else{
                                $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>'',
                                    "r1_date_rec"=>'',"r1_date_integration"=>'',
                                    'r1_enposte'=>'',"r1_peride_conf"=>'',
                                    "r1_garantie"=>'',"r1_date_relance"=>'');
                            }
                        }
                    }
                    $result[] = array_merge($rec1_array[$i], $rec2_array[$i], $aff_dmd_array[$i], $res[$i], $affectation[$i]);
                    //var_dump($result);die;
                } else {
                    $affectation[] = array("id_dmd" => $dmd->getId(), "r1_recruteur" => '');
                    if ($aff_dmd != null) {
                        $aff_dmd_array[] = array("id_dmd" => $dmd->getId(),
                            "sous_traitance" => ($aff_dmd->getAffSousTraitance() == 1) ? $aff_dmd_array["sous_traitance"] = 'Non' : 'Oui',
                            "charge_rec" => ($aff_dmd->getIdRecruteur() != null) ? $aff_dmd_array["charge_rec"] = $aff_dmd->getIdRecruteur()->getUserId()->getDisplayName() : $aff_dmd->getIdCabinet()->getNomCabinet());
                        if ($rec2!=null){
                            $rec2_array[]=array("id_dmd"=>$dmd->getId(),"r2_nom_cand"=>$rec2->getNomCandidat()." ".$rec2->getPrenomCandidat(),
                                "r2_date_rec"=>$rec2->getDateRecrutement(),"r2_date_integration"=>$rec2->getDateIntegration(),"r2_enposte"=>$rec2->getEnPoste());
                            if ($rec1!=null){
                                $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>$rec1->getNomCandidat()." ".$rec1->getPrenomCandidat(),
                                    "r1_date_rec"=>$rec1->getDateRecrutement(),"r1_date_integration"=>$rec1->getDateIntegration(),
                                    'r1_enposte'=>$rec1->getEnPoste(),"r1_peride_conf"=>$rec1->getPeriodeConfirmee(),
                                    "r1_garantie"=>$rec1->getGarantie(),"r1_date_relance"=>$rec1->getDateRelance());
                            }
                            else{
                                $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>'',
                                    "r1_date_rec"=>'',"r1_date_integration"=>'',
                                    'r1_enposte'=>'',"r1_peride_conf"=>'',
                                    "r1_garantie"=>'',"r1_date_relance"=>'');
                            }
                        }
                        else{
                            $rec2_array[]=array("id_dmd"=>$dmd->getId(),"r2_nom_cand"=>'',
                                "r2_date_rec"=>'',"r2_date_integration"=>'',"r2_enposte"=>'');
                            if ($rec1!=null){
                                $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>$rec1->getNomCandidat()." ".$rec1->getPrenomCandidat(),
                                    "r1_date_rec"=>$rec1->getDateRecrutement(),"r1_date_integration"=>$rec1->getDateIntegration(),
                                    'r1_enposte'=>$rec1->getEnPoste(),"r1_peride_conf"=>$rec1->getPeriodeConfirmee(),
                                    "r1_garantie"=>$rec1->getGarantie(),"r1_date_relance"=>$rec1->getDateRelance());
                            }
                            else{
                                $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>'',
                                    "r1_date_rec"=>'',"r1_date_integration"=>'',
                                    'r1_enposte'=>'',"r1_peride_conf"=>'',
                                    "r1_garantie"=>'',"r1_date_relance"=>'');
                            }
                        }
                    } else {
                        $aff_dmd_array[] = array("id_dmd" => $dmd->getId(), "sous_traitance" => '', "charge_rec" => '');
                        if ($rec2!=null){
                            $rec2_array[]=array("id_dmd"=>$dmd->getId(),"r2_nom_cand"=>$rec2->getNomCandidat()." ".$rec2->getPrenomCandidat(),
                                "r2_date_rec"=>$rec2->getDateRecrutement(),"r2_date_integration"=>$rec2->getDateIntegration(),"r2_enposte"=>$rec2->getEnPoste());
                            if ($rec1!=null){
                                $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>$rec1->getNomCandidat()." ".$rec1->getPrenomCandidat(),
                                    "r1_date_rec"=>$rec1->getDateRecrutement(),"r1_date_integration"=>$rec1->getDateIntegration(),
                                    'r1_enposte'=>$rec1->getEnPoste(),"r1_peride_conf"=>$rec1->getPeriodeConfirmee(),
                                    "r1_garantie"=>$rec1->getGarantie(),"r1_date_relance"=>$rec1->getDateRelance());
                            }
                            else{
                                $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>'',
                                    "r1_date_rec"=>'',"r1_date_integration"=>'',
                                    'r1_enposte'=>'',"r1_peride_conf"=>'',
                                    "r1_garantie"=>'',"r1_date_relance"=>'');
                            }
                        }
                        else{
                            $rec2_array[]=array("id_dmd"=>$dmd->getId(),"r2_nom_cand"=>'',
                                "r2_date_rec"=>'',"r2_date_integration"=>'',"r2_enposte"=>'');
                            if ($rec1!=null){
                                $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>$rec1->getNomCandidat()." ".$rec1->getPrenomCandidat(),
                                    "r1_date_rec"=>$rec1->getDateRecrutement(),"r1_date_integration"=>$rec1->getDateIntegration(),
                                    'r1_enposte'=>$rec1->getEnPoste(),"r1_peride_conf"=>$rec1->getPeriodeConfirmee(),
                                    "r1_garantie"=>$rec1->getGarantie(),"r1_date_relance"=>$rec1->getDateRelance());
                            }
                            else{
                                $rec1_array[]=array("id_dmd"=>$dmd->getId(),"r1_nom_cand"=>'',
                                    "r1_date_rec"=>'',"r1_date_integration"=>'',
                                    'r1_enposte'=>'',"r1_peride_conf"=>'',
                                    "r1_garantie"=>'',"r1_date_relance"=>'');
                            }
                        }
                    }
                    $result[] = array_merge($rec1_array[$i], $rec2_array[$i] ,$aff_dmd_array[$i], $affectation[$i], $res[$i]);
                }
                $i++;
            }

            return $this->render('SelectingPeopleBundle:Default:tab_mission.html.twig',
                array('form' => $form->createView(), 'result'=>$result, 'statut'=>$statuts, 'direction'=>$directions, 'ch_rec'=>$ch_rec, 'cabinets'=>$cabinet));
        }
        else{
            return $this->redirect($this->generateUrl('tab'));
        }
    }

    /**
     * @Route("/delete/{id}", name="delete")
     */
    public function deleteAction ($id)
    {
        $dmd= $this->getDoctrine()->getRepository("SelectingPeopleBundle:SpDemandeRecrutement")-> findBy(array('id'=>$id));
        $em = $this->getDoctrine()->getManager();
        $em->remove($dmd[0]);
        $em->flush($dmd[0]);
        return $this->redirect($this->generateUrl('tab'));
    }

}