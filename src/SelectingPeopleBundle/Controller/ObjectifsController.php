<?php

namespace SelectingPeopleBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use SelectingPeopleBundle\Form\SpObjectifsType;
use SelectingPeopleBundle\Entity\SpChargeRecrutement;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * Objectifs controller.
 *
 * @Route("/spobjectif")
 */
class ObjectifsController extends Controller
{

    /**
     * @Route("/", name="obj")
     */

    public function changeObjectifAction(Request $request)
    {
        $form = $this->createForm(new SpObjectifsType());
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery("SELECT c FROM SelectingPeopleBundle\Entity\SpChargeRecrutement c WHERE c.userId IN (SELECT u FROM User\UserBundle\Entity\User u WHERE u.roles LIKE :role)");
        $query->setParameter('role', '%ROLE_CHARGE_RECRUTEMENT%');
        $objectifs = $query->getResult();
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $donnee = $_POST;
            $em = $this->getDoctrine()->getManager();
            foreach ($objectifs as $objectif){
                $i=$objectif->getId();
                $objectif->setObjectif($donnee ["selectingpeoplebundle_spobjectifs"]['objectif'.$i]);
                if(isset($donnee[$i])){
                    $user=$this->getDoctrine()->getRepository('UserUserBundle:User')->findOneBy(array('id'=>$objectif->getUserId()->getId()));
                    $user->removeRole('ROLE_CHARGE_RECRUTEMENT');
                }
                $em->persist($objectif);
                $em->flush();
            }
            $query = $em->createQuery("SELECT c FROM SelectingPeopleBundle\Entity\SpChargeRecrutement c WHERE c.userId IN (SELECT u FROM User\UserBundle\Entity\User u WHERE u.roles LIKE :role)");
            $query->setParameter('role', '%ROLE_CHARGE_RECRUTEMENT%');
            $objectifs = $query->getResult();
        }
        return $this->render ('SelectingPeopleBundle:Default:objectifs.html.twig',
            array('obj'=>$objectifs,'form'=>$form->createView()));
    }
}
