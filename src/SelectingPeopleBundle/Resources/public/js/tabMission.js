/**
 * Created by ons.allaya on 02/03/2017.
 */

function searchTrait() {
    var input, filter, table, tr, td, i;
    input = document.getElementById("selectingpeoplebundle_recherche_sous_trait");
    filter = input.value.toUpperCase();
    table = document.getElementById("myTable");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[13];
        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}



$(document).ready(function () {

    var table=$('#myTable').dataTable({
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": false,
        "bInfo": false,
        "bAutoWidth": true });
    jQuery(".chosen").chosen();
    $('#rec1').hide();
    $('#rec2').hide();
    $('#cab1').hide();
    $('#cab2').hide();
    searchTrait();

    var r=$('#selectingpeoplebundle_recherche_sous_trait').val();
    if(r=='Oui'){
        $('#rec1').hide();
        $('#rec2').hide();
        $('#cab1').show();
        $('#cab2').show();
    }
    if (r=='Non'){
        $('#rec1').show();
        $('#rec2').show();
        $('#cab1').hide();
        $('#cab2').hide();
    }
    if(r=='') {
        $('#rec1').hide();
        $('#rec2').hide();
        $('#cab1').hide();
        $('#cab2').hide();
    }


    $('#selectingpeoplebundle_recherche_date_dmd[name="selectingpeoplebundle_recherche[date_dmd]"]').daterangepicker(
        {
            locale: {
                format: 'YYYY-MM-DD'
            }
        }
    );
    $('#selectingpeoplebundle_recherche_date_rec[name="selectingpeoplebundle_recherche[date_rec]"]').daterangepicker({
            locale: {
                format: 'YYYY-MM-DD'
            }
        }
        );

    $('#selectingpeoplebundle_recherche_sous_trait').on("change", function () {
        if($('#selectingpeoplebundle_recherche_sous_trait').val()==''){
            $("#selectingpeoplebundle_recherche_ch_rec").val('');
            $("#selectingpeoplebundle_recherche_cabinet_rec").val('');

        }
        if($('#selectingpeoplebundle_recherche_sous_trait').val()=='Oui'){
            $("#selectingpeoplebundle_recherche_ch_rec").val('');
        }
        if($('#selectingpeoplebundle_recherche_sous_trait').val()=='Non'){
            $("#selectingpeoplebundle_recherche_cabinet_rec").val('');
        }
    });



    $('#selectingpeoplebundle_recherche_sous_trait').on("change", function () {
        var r=$('#selectingpeoplebundle_recherche_sous_trait').val();
        if(r=='Oui'){
            $('#rec1').hide();
            $('#rec2').hide();
            $('#cab1').show();
            $('#cab2').show();
        }
        if (r=='Non'){
            $('#rec1').show();
            $('#rec2').show();
            $('#cab1').hide();
            $('#cab2').hide();
        }
        if(r=='') {
            $('#rec1').hide();
            $('#rec2').hide();
            $('#cab1').hide();
            $('#cab2').hide();
        }

    })
})
