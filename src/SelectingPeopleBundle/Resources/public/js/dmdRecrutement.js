/**
 * Created by ons.allaya on 02/03/2017.
 */
function initDisplay1() {

    displayPreference();
    displayExclusion();
    upListDirection();
    displaySer();
    //displayPole();
    displayPost(2);
    $('#divregion').hide();
    $('#divprod').hide();



}

function displayPreference() {
    $.ajax({
        type: "POST",
        url: "../spdemanderecrutement/preference",
        dataType: "json",
        success: function (html) {

            $('#selectingpeoplebundle_spdemanderecrutement_preference').html(html);

        },
        error: function (xhr, textStatus, errorThrown) {


        }
    });
}
function displayExclusion() {
    $.ajax({
        async: false,
        type: "POST",
        url: "../spdemanderecrutement/preference",
        dataType: "json",
        cache: false,
        success: function (html) {
            $('#selectingpeoplebundle_spdemanderecrutement_exclusion').html(html);
        },
        error: function (xhr, textStatus, errorThrown) {
        }
    });
}
function upListDirection() {
    //$("select#selectingpeoplebundle_spdemanderecrutement_direction option").remove();
    $.ajax({
        async: false,
        dataType: "json",
        type: "POST",
        url: "../spdemanderecrutement/direction",
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
            $output = '<option value="-1"></option>';
        },
        success: function (html) {
            $("select#selectingpeoplebundle_spdemanderecrutement_dir").html(html);
        }
    });
}


function displaySer() {

        upListSer();

}
function upListSer() {
    var direction = $('select#selectingpeoplebundle_spdemanderecrutement_dir option:selected').val();
    // $("select#selectingpeoplebundle_spdemanderecrutement_service option").remove();
    if (direction != "0") {
        $.ajax({
            async: false,
            dataType: "json",
            type: "POST",
            url: "../spdemanderecrutement/serv",
            data: {direction: direction},
            cache: false,
            error: function (jqXHR, textStatus, errorThrown) {

            },
            success: function (html) {
                $("select#selectingpeoplebundle_spdemanderecrutement_srv").html(html);
                //displayPost(0);

            }
        });
    }
}
function displayPole() {

        upListPole();

}
function upListPole() {
    var service = $('select#selectingpeoplebundle_spdemanderecrutement_srv option:selected').val();
    if (service != "0") {
        $.ajax({
            async: false,
            dataType: "json",
            type: "POST",
            url: "../spdemanderecrutement/poles",
            data: {service: service},
            cache: false,
            error: function (jqXHR, textStatus, errorThrown) {
                displayPost(2);
                $("select#selectingpeoplebundle_spdemanderecrutement_pol option").remove();
                $('#selectingpeoplebundle_spdemanderecrutement_pol').append($('<option >', {
                    value: 0,
                    text: '---------------------- Pôles ----------------------',
                    selected: true
                }));
                document.getElementById("selectingpeoplebundle_spdemanderecrutement_pol").disabled = true;
            },
            success: function (html) {
                document.getElementById("selectingpeoplebundle_spdemanderecrutement_pol").disabled = false;
                $("select#selectingpeoplebundle_spdemanderecrutement_pol").html(html);
            }
        });
    }
}
function displayPost(hierarchyLevel) {
    upListPost(hierarchyLevel);

}
function upListPost($hierarchyLevel) {
    var idBridge = [];
    if ($hierarchyLevel == "2") {
        idBridge.push($('select#selectingpeoplebundle_spdemanderecrutement_dir option:selected').val()); // direction
        idBridge.push($('select#selectingpeoplebundle_spdemanderecrutement_srv option:selected').val()); // service
        idBridge.push('NULL'); // pole

    }
    else if ($hierarchyLevel == "3") {
        idBridge.push($('select#selectingpeoplebundle_spdemanderecrutement_dir option:selected').val()); // direction
        idBridge.push($('select#selectingpeoplebundle_spdemanderecrutement_srv option:selected').val()); // service
        idBridge.push($('select#selectingpeoplebundle_spdemanderecrutement_pol option:selected').val()); // pole
    }

    $.ajax({
        async: false,
        dataType: "json",
        type: "POST",
        url: "../spdemanderecrutement/postes",
        data: {idBridge: idBridge},
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {

        },
        success: function (html) {
            $("select#selectingpeoplebundle_spdemanderecrutement_pst").html(html);
        }
    });
}
function addPost() {
    var nv_pst = [];
    var pst = $('#selectingpeoplebundle_spposte_pstLibelle').val();
    var sal = $('#selectingpeoplebundle_spposte_pstSalaire').val();
    var dir = $('select#selectingpeoplebundle_spdemanderecrutement_dir option:selected').val();
    var srv = $('select#selectingpeoplebundle_spdemanderecrutement_srv option:selected').val();
    var pol = $('select#selectingpeoplebundle_spdemanderecrutement_pol option:selected').val();
    nv_pst.push(dir);
    nv_pst.push(srv);
    nv_pst.push(pol);
    nv_pst.push(pst);
    nv_pst.push(sal);
        $.ajax({
            async: false,
            dataType: "json",
            type: "POST",
            url: "../spdemanderecrutement/nv_pst",
            data: {nv_pst:nv_pst},
            cache: false,
            error: function (jqXHR, textStatus, errorThrown) {
            },
            success: function (res) {
                $('#selectingpeoplebundle_spdemanderecrutement_pst option:last').before($('<option >', {
                    value: res,
                    text: pst,
                }));
            }
        });
}
function regions(){
    var pys = $('#selectingpeoplebundle_spdemanderecrutement_paysId option:selected').val();
    if (pys == '3') {
        $('#divregion').show();
    }
    else{
        $('#divregion').hide();
    }
}
function calc() {
    if ($('#conf_aff').bootstrapSwitch('state') == true) {
        //oui
        document.getElementById("selectingpeoplebundle_spdemanderecrutement_confidentiel_0").checked=true;
    }
    else {
        //non
        document.getElementById("selectingpeoplebundle_spdemanderecrutement_confidentiel_1").checked=true;
    }
}

function validateForm(){
    var x = document.forms["form_dmd"]["selectingpeoplebundle_spdemanderecrutement[steId]"].value;

    if(x==''){
        //.chosen-container-single .chosen-single
        document.getElementById("selectingpeoplebundle_spdemanderecrutement_steId").style.borderColor = "#dd4b39";
        //$('#selectingpeoplebundle_spdemanderecrutement_steId').
        $('#ste_error').show();
        return false;
    }
    document.getElementById("selectingpeoplebundle_spdemanderecrutement_save").disabled = false;
    return true;
}


$(document).ready(function () {
    $('#selectingpeoplebundle_spdemanderecrutement_confidentiel').hide();
    $("select#selectingpeoplebundle_spdemanderecrutement_pol option").remove();
    $('#selectingpeoplebundle_spdemanderecrutement_pol').append($('<option >', {
            value: 0,
            text: '---------------------- Pôles ----------------------',
            selected: true
         }));
    initDisplay1();
    $("#selectingpeoplebundle_spdemanderecrutement_steId").chosen();
    $(".BSswitch").bootstrapSwitch('state', false);
    $('#ajout_pst').bootstrapValidator({
        err: {
            container: '#messages'
        },
        fields: {
            "selectingpeoplebundle_spposte[posteLibelle]": {
                validators: {
                    notEmpty: {
                        message: 'Veuillez renseigner ce champ!'
                    }
                }
            },
            "selectingpeoplebundle_spposte[pstSalaire]": {
                validators: {
                    number: {
                        message: 'Entrez un salaire valide!'
                    }
                }
            }
        }
    });
    $('#selectingpeoplebundle_spposte_ajouterPoste').click(function(){
        addPost();
        $('#modal_add_poste').modal('hide');
    });
    $("#form_dmd").submit(function(){
        return validateForm();
    });
    $('#form_dmd').bootstrapValidator({
        err: {
            container: '#messages'
        },
            fields: {
                "selectingpeoplebundle_spdemanderecrutement[nomDemandeur]": {
                    validators: {
                        notEmpty: {
                            message: 'Veuillez renseigner ce champ!'
                        }
                    }
                },
                "selectingpeoplebundle_spdemanderecrutement[prenomDemandeur]": {
                    validators: {
                        notEmpty: {
                            message: 'Veuillez renseigner ce champ!'
                        }
                    }
                },
                "selectingpeoplebundle_spdemanderecrutement[mailDemandeur]":{
                    validators: {
                        notEmpty: {
                            message: 'Veuillez renseigner ce champ!'
                        },
                        emailAddress: {
                            message: 'Veuillez entrer un email valide!'
                        }
                    }
                },
                "selectingpeoplebundle_spdemanderecrutement[ville]": {
                    validators: {
                        notEmpty: {
                            message: 'Veuillez renseigner ce champ!'
                        }
                    }
                },
                "selectingpeoplebundle_spdemanderecrutement[dir]": {
                    validators: {
                        notEmpty: {
                            message: 'Veuillez renseigner ce champ!'
                        }
                    }
                },
                "selectingpeoplebundle_spdemanderecrutement[srv]": {
                    validators: {
                        notEmpty: {
                            message: 'Veuillez renseigner ce champ!'
                        }
                    }
                },
                "selectingpeoplebundle_spdemanderecrutement[pst]": {
                    validators: {
                        notEmpty: {
                            message: 'Veuillez renseigner ce champ!'
                        }
                    }
                },
                "selectingpeoplebundle_spdemanderecrutement[steId]": {
                    validators: {
                        notEmpty: {
                            message: 'Veuillez renseigner ce champ!'
                        }
                    }
                },
                "selectingpeoplebundle_spdemanderecrutement[contratId]": {
                    validators: {
                        notEmpty: {
                            message: 'Veuillez renseigner ce champ!'
                        }
                    }
                },
                "selectingpeoplebundle_spdemanderecrutement[statutCollaborateurId]": {
                    validators: {
                        notEmpty: {
                            message: 'Veuillez renseigner ce champ!'
                        }
                    }
                },
                "selectingpeoplebundle_spdemanderecrutement[paysId]": {
                    validators: {
                        notEmpty: {
                            message: 'Veuillez renseigner ce champ!'
                        }
                    }
                },

            }
        });

    $('#selectingpeoplebundle_spdemanderecrutement_steId').on("change", function () {
        $('#ste_error').hide();
        validateForm();
    });
    $('#selectingpeoplebundle_spdemanderecrutement_dir').on("change", function () {
        var d = $('#selectingpeoplebundle_spdemanderecrutement_dir option:selected').val();
        if (d == '1') {
            $('#divprod').show();
        }
        else {
            $('#divprod').hide();
        }
        document.getElementById("selectingpeoplebundle_spdemanderecrutement_pol").disabled = false;
        $("select#selectingpeoplebundle_spdemanderecrutement_pol option").remove();
        $('#selectingpeoplebundle_spdemanderecrutement_pol').append($('<option >', {
            value: 0,
            text: '---------------------- Pôles ----------------------',
            selected: true
        }));
        displaySer();
        displayPost(0);
    });
    $('#selectingpeoplebundle_spdemanderecrutement_srv').on("change", function () {
        displayPole();
       // displayPost(2);
    });
    $('#selectingpeoplebundle_spdemanderecrutement_pol').on("change", function () {
        displayPost(3);
    });
    $('#selectingpeoplebundle_spdemanderecrutement_pst').on("change", function () {
        var val = $('#selectingpeoplebundle_spdemanderecrutement_pst option:selected').val();
        if(val==-1){
            $('#modal_add_poste').modal('show');
        }
    });
    $('#selectingpeoplebundle_spdemanderecrutement_paysId').on("change", function () {
        regions();
    });
    $('#veo').on("change", function () {
        if($('#veo').val()=='veo'){
            window.location = "http://localhost/Applications/web/app_dev.php/veo";
        }
    });
    document.getElementById("selectingpeoplebundle_spposte_ajouterPoste").disabled = true;
    $('#selectingpeoplebundle_spposte_pstLibelle').on("change", function () {
        var libelle=$('#selectingpeoplebundle_spposte_pstLibelle').val();
        if(libelle!=null){
            document.getElementById("selectingpeoplebundle_spposte_ajouterPoste").disabled = false;
        }
        else {
            document.getElementById("selectingpeoplebundle_spposte_ajouterPoste").disabled = true;
        }
    });
})
