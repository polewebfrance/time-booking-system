/**
 * Created by ons.allaya on 02/03/2017.
 */
function initDisplay1() {


    $('#divregion').hide();
    $('#divprod').hide();
    $('#save').hide();
    $('#pst_edit').hide();
    $('#dir_edit').hide();
    $('#srv_edit').hide();
    $('#pol_edit').hide();
    $('#conf_edit').hide();
    $('#ste_edit').hide();
    $('#ref_edit').hide();
    $('#ctr_edit').hide();
    $('#statut_edit').hide();
    $('#sal_edit').hide();
    $('#per_edit').hide();
    $('#pys_edit').hide();
    $('#reg_edit').hide();
    $('#vill_edit').hide();
    $('#cmt_sp_edit').hide();
    $('#prod_edit').hide();


}
function listDirection(id) {
    //$("select#selectingpeoplebundle_spdemanderecrutement_direction option").remove();
    $.ajax({
        async: false,
        dataType: "json",
        type: "POST",
        url: "../directions/"+id,
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
            $output = '<option value="-1"></option>';
        },
        success: function (html) {
            $("select#selectingpeoplebundle_spdemanderecrutement_dir").html(html);
        }
    });
}
function displaySer(id) {
    var direction = $('select#selectingpeoplebundle_spdemanderecrutement_dir option:selected').val();

    $.ajax({
        async: false,
        dataType: "json",
        type: "POST",
        url: "../ser/"+id,
        data: {direction: direction},
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (html) {
            $("select#selectingpeoplebundle_spdemanderecrutement_srv").html(html);
        }
    });
}

function displayPole(id) {
    var service = $('select#selectingpeoplebundle_spdemanderecrutement_srv option:selected').val();

    $.ajax({
        async: false,
        dataType: "json",
        type: "POST",
        url: "../polelist/"+id,
        data: {service: service},
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
            upListPost(id);
            $("select#selectingpeoplebundle_spdemanderecrutement_pol option").remove();
            $('#selectingpeoplebundle_spdemanderecrutement_pol').append($('<option >', {
                value: 0,
                text: '---------------------- Pôles ----------------------',
                selected: true
            }));
            document.getElementById("selectingpeoplebundle_spdemanderecrutement_pol").disabled = true;
        },

        success: function (html) {
            document.getElementById("selectingpeoplebundle_spdemanderecrutement_pol").disabled = false;
            $("select#selectingpeoplebundle_spdemanderecrutement_pol").html(html);
            upListPost(id);
        }
    });

}
function upListPost(id) {
    var idBridge = [];
        idBridge.push($('select#selectingpeoplebundle_spdemanderecrutement_dir option:selected').val()); // direction
        idBridge.push($('select#selectingpeoplebundle_spdemanderecrutement_srv option:selected').val()); // service
        if ($('select#selectingpeoplebundle_spdemanderecrutement_pol option:selected').val()!=0)
        {
            idBridge.push($('select#selectingpeoplebundle_spdemanderecrutement_pol option:selected').val()); // pole
        }
        else{
            idBridge.push('NULL');
        }

    $.ajax({
        async: false,
        dataType: "json",
        type: "POST",
        url: "../poste/"+id,
        data: {idBridge: idBridge},
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {

        },
        success: function (html) {
            $("select#selectingpeoplebundle_spdemanderecrutement_pst").html(html);
        }
    });
}



function displayChargeRec(dmdid) {
    $.ajax({
        type: "POST",
        url: "../chargerec/"+dmdid,
        success: function (html) {

            $('#selectingpeoplebundle_spaffectationdemande_idRecruteur').html(html);
        },
        error: function (xhr, textStatus, errorThrown) {
        }
    });
}
// function garantie(dmdid) {
//     $.ajax({
//         type: "POST",
//         url: "../garantie/"+dmdid,
//         dataType: "json",
//         success: function (html) {
//
//             $('#selectingpeoplebundle_sprecrutement_garantie').html(html);
//         },
//         error: function (xhr, textStatus, errorThrown) {
//         }
//     });
// }


function displayRegion(id) {
    $.ajax({
        type: "POST",
        url: "../region/"+id,
        dataType: "json",
        success: function (html) {
            $('#selectingpeoplebundle_spdemanderecrutement_regionId').html(html);
        },
        error: function (xhr, textStatus, errorThrown) {
        }
    });
}

function modifier() {
    $('#modif').hide();
    $('#pst_show').hide();
    $('#dir_show').hide();
    $('#srv_show').hide();
    $('#pol_show').hide();
    $('#conf_show').hide();
    $('#ste_show').hide();
    $('#ref_show').hide();
    $('#ctr_show').hide();
    $('#staut_show').hide();
    $('#sal_show').hide();
    $('#per_show').hide();
    $('#pys_show').hide();
    $('#reg_show').hide();
    $('#vill_show').hide();
    $('#cmt_sp_show').hide();
    $('#prod_show').hide();
    $('#save').show();
    $('#pst_edit').show();
    $('#dir_edit').show();
    $('#srv_edit').show();
    $('#pol_edit').show();
    $('#conf_edit').show();
    $('#ste_edit').show();
    $('#ref_edit').show();
    $('#ctr_edit').show();
    $('#statut_edit').show();
    $('#sal_edit').show();
    $('#per_edit').show();
    $('#pys_edit').show();
    $('#reg_edit').show();
    $('#vill_edit').show();
    $('#cmt_sp_edit').show();
    $('#prod_edit').show();

}
function validateForm() {
    var x = document.forms["suiviForm"]["selectingpeoplebundle_sprecrutement[salaire]"].value;
    var ch= document.forms["affectForm"]["selectingpeoplebundle_spaffectationdemande[idRecruteur]"].value;
    var ch1= document.forms["affectForm"]["selectingpeoplebundle_spaffectationdemande[idCabinet]"].value;
    if (x <= 0) {

        $('#myModal').modal('show');
        return false;
    }
    if ((ch== -1)&&(ch1==-1)){
        $('#erreur_rec').show();
        return false;
    }
    var z= $('#selectingpeoplebundle_spaffectationdemande_aff_sous_traitance').val();
    var y= $('#selectingpeoplebundle_spaffectationdemande_accompte').val();
    if((z!=y)&&(y==1)){
        $('#modal_accompte').modal('show');
        return false;
    }
}
function displayChargeRech(id) {
    $.ajax({
        type: "POST",
        url: "../chargerech/"+id,
        dataType: "json",
        success: function (html) {

            $('#selectingpeoplebundle_spaffectationdemande_charge_recherche').html(html);
        },
        error: function (xhr, textStatus, errorThrown) {
        }
    });
}

function affecter() {
    var sp = $('select#selectingpeoplebundle_spaffectationdemande_aff_sous_traitance option:selected').val();
    if (sp == "0") {
        $('#chrg_rec').hide();
        $('#cabnt_ext').show();
    }
    else{
        $('#chrg_rec').show();
        $('#cabnt_ext').hide();
    }
}

function enPoste2(dmdid) {
    $.ajax({
        type: "POST",
        url: "../en_poste2/"+dmdid,
        dataType: "json",
        success: function (html) {
            $('#selectingpeoplebundle_sprecrutement_en_poste2').html(html);
        },
        error: function (xhr, textStatus, errorThrown) {
        }
    });
}
// function pEssai(dmdid) {
//     $.ajax({
//         type: "POST",
//         url: "../p_essai/"+dmdid,
//         dataType: "json",
//         success: function (html) {
//             $('#selectingpeoplebundle_sprecrutement_p_essai_conf').html(html);
//         },
//         error: function (xhr, textStatus, errorThrown) {
//         }
//     });
// }


$(document).ready(function () {
    $('#selectingpeoplebundle_spaffectationdemande_accompte').hide()
    var url = window.location.pathname;
    var dmdid = url.substring(url.lastIndexOf('/') + 1);
    initDisplay1();
    displayChargeRech(dmdid);
    listDirection(dmdid);
    displaySer(dmdid);
    displayPole(dmdid);
    displayChargeRec(dmdid);
    //displayCabinets(dmdid);
    displayRegion(dmdid);
    //enPoste(dmdid);
    //pEssai(dmdid);
    enPoste2(dmdid);
    //garantie(dmdid);
    $('#selectingpeoplebundle_sprecrutement_statutt').attr("type", "hidden");
    document.getElementById("selectingpeoplebundle_spaffectationdemande_sal_hidden").value = $('#selectingpeoplebundle_sprecrutement_salaire').val();
    $('#selectingpeoplebundle_spaffectationdemande_sal_hidden').hide();
    $("#selectingpeoplebundle_spdemanderecrutement_steId").chosen();
    $('#selectingpeoplebundle_sprecrutement_salaire').on("change", function () {
        var s=$('#selectingpeoplebundle_sprecrutement_salaire').val();
        document.getElementById("selectingpeoplebundle_spaffectationdemande_sal_hidden").value = s;
    })

    $('#selectingpeoplebundle_spdemanderecrutement_modifier').click(function(){
        modifier();
    });
    $('#selectingpeoplebundle_spaffectationdemande_affecter').click(function() {
        validateForm();
    });

    $('#selectingpeoplebundle_spaffectationdemande_aff_sous_traitance').on("change", function () {
        affecter();
    })
    $('#acc_zero').click(function() {
        document.getElementById("selectingpeoplebundle_spaffectationdemande_accompte").value = 'zero';
        affectForm.submit();
    });
    $('#laisser_acc').click(function() {
        affectForm.submit();
    });
    $('#selectingpeoplebundle_spdemanderecrutement_dir').on("change", function () {
        document.getElementById("selectingpeoplebundle_spdemanderecrutement_pol").disabled = false;
        var d = $('#selectingpeoplebundle_spdemanderecrutement_dir option:selected').val();
        if (d == '1') {
            $('#div_prod').show();
        }
        else {
            $('#div_prod').hide();
        }
        displaySer(dmdid);
        //displayPole(dmdid);
        upListPost(dmdid);
        $("select#selectingpeoplebundle_spdemanderecrutement_pol option").remove();
        $('#selectingpeoplebundle_spdemanderecrutement_pol').append($('<option >', {
            value: 0,
            text: '---------------------- Pôles ----------------------',
            selected: true
        }));
    });
    $('#selectingpeoplebundle_spdemanderecrutement_srv').on("change", function () {
        displayPole(dmdid);
        upListPost(dmdid);
    });
    $('#selectingpeoplebundle_spdemanderecrutement_pol').on("change", function () {
        upListPost(dmdid);
    });
    $("#selectingpeoplebundle_sprecrutement_dateRecrutement, #selectingpeoplebundle_sprecrutement_dateIntegration, #selectingpeoplebundle_sprecrutement_dateRelance,#selectingpeoplebundle_sprecrutement_dateRecrutement2, #selectingpeoplebundle_sprecrutement_dateIntegration2").css({"cursor":"pointer"});

    $("#selectingpeoplebundle_sprecrutement_dateRecrutement").datepicker({
        dateFormat: "yy-mm-dd",
        minDate:  0,
        language: "fr",
        beforeShow: function(input, inst) {
            $(input).attr('readonly', true);
        },
        onClose: function(dateText, inst) {
            $(this).attr('readonly', false);
        },
        onSelect: function(date){
            var date1 = $('#selectingpeoplebundle_sprecrutement_dateRecrutement').datepicker('getDate');
            var date = new Date( Date.parse( date1 ) );
            date.setDate( date.getDate() + 1 );
            var newDate = date.toDateString();
            newDate = new Date( Date.parse( newDate ) );
            $('#selectingpeoplebundle_sprecrutement_dateIntegration').datepicker("option","minDate",newDate);

        }
    });

    $('#selectingpeoplebundle_sprecrutement_dateIntegration').datepicker({
        dateFormat: "yy-mm-dd",
        language: "fr",
        minDate:  0,
        beforeShow: function(input, inst) {
            $(input).attr('readonly', true);
        },
        onClose: function(dateText, inst) {
            $(this).attr('readonly', false);
        },
        onSelect: function(date){
            var date1 = $('#selectingpeoplebundle_sprecrutement_dateIntegration').datepicker('getDate');
            var date = new Date( Date.parse( date1 ) );
            date.setDate( date.getDate() + 1 );
            var newDate = date.toDateString();
            newDate = new Date( Date.parse( newDate ) );
            $('#selectingpeoplebundle_sprecrutement_dateRelance').datepicker("option","minDate",newDate);

        }
    });
    $('#selectingpeoplebundle_sprecrutement_dateRelance').datepicker({
        dateFormat: "yy-mm-dd",
        language: "fr",
        beforeShow: function(input, inst) {
            $(input).attr('readonly', true);
            var date1 = $('#selectingpeoplebundle_sprecrutement_dateIntegration').datepicker('getDate');
            var date = new Date( Date.parse( date1 ) );
            date.setDate( date.getDate() + 1 );
            var newDate = date.toDateString();
            newDate = new Date( Date.parse( newDate ) );
            $('#selectingpeoplebundle_sprecrutement_dateRelance').datepicker("option","minDate",newDate);


        },
        onClose: function(dateText, inst) {
            $(this).attr('readonly', false);
        },
        onSelect: function(date){
            var date1 = $('#selectingpeoplebundle_sprecrutement_dateRelance').datepicker('getDate');
            var date = new Date( Date.parse( date1 ) );
            date.setDate( date.getDate() + 1 );
            var newDate = date.toDateString();
            newDate = new Date( Date.parse( newDate ) );
            $('#selectingpeoplebundle_sprecrutement_dateRecrutement2').datepicker("option","minDate",newDate);

        }
    });
    $("#selectingpeoplebundle_sprecrutement_dateRecrutement2").datepicker({
        dateFormat: "yy-mm-dd",
        minDate:  0,
        language: "fr",
        beforeShow: function(input, inst) {
            $(input).attr('readonly', true);
            var date1 = $('#selectingpeoplebundle_sprecrutement_dateRelance').datepicker('getDate');
            var date = new Date( Date.parse( date1 ) );
            date.setDate( date.getDate() + 1 );
            var newDate = date.toDateString();
            newDate = new Date( Date.parse( newDate ) );
            $('#selectingpeoplebundle_sprecrutement_dateRecrutement2').datepicker("option","minDate",newDate);
        },
        onClose: function(dateText, inst) {
            $(this).attr('readonly', false);
        },
        onSelect: function(date){
            var date1 = $('#selectingpeoplebundle_sprecrutement_dateRecrutement2').datepicker('getDate');
            var date = new Date( Date.parse( date1 ) );
            date.setDate( date.getDate() + 1 );
            var newDate = date.toDateString();
            newDate = new Date( Date.parse( newDate ) );
            $('#selectingpeoplebundle_sprecrutement_dateIntegration2').datepicker("option","minDate",newDate);

        }
    });
    $('#selectingpeoplebundle_sprecrutement_dateIntegration2').datepicker({
        dateFormat: "yy-mm-dd",
        language: "fr",
        minDate:  0,
        beforeShow: function(input, inst) {
            $(input).attr('readonly', true);
        },
        onClose: function(dateText, inst) {
            $(this).attr('readonly', false);
        }
    });
    $('#suiviForm').bootstrapValidator({
        err: {
            container: '#messages'
        },
        fields: {
            "selectingpeoplebundle_sprecrutement[salaire]": {
                validators: {
                    notEmpty: {
                        message: 'Veuillez renseigner ce champ!'
                    },
                    numeric: {
                        message: 'Entrez un salaire valide!'
                    }
                }
            },
            "selectingpeoplebundle_sprecrutement[reference]": {
                validators: {
                    stringLength: {
                        min: 12,
                        max: 12,
                        message: 'Veuillez fournir au moins 12 caractères'
                    }
                }
            },
            "selectingpeoplebundle_sprecrutement[dateRecrutement2]": {
                validators: {
                    notEmpty: {
                        message: 'Veuillez renseigner ce champ!'
                    }
                }
            },
            "selectingpeoplebundle_sprecrutement[dateIntegration2]": {
                validators: {
                    notEmpty: {
                        message: 'Veuillez renseigner ce champ!'
                    }
                }
            },
            "selectingpeoplebundle_sprecrutement[nom_candidat2]": {
                validators: {
                    notEmpty: {
                        message: 'Veuillez renseigner ce champ!'
                    }
                }
            },
            "selectingpeoplebundle_sprecrutement[prenom_candidat2]": {
                validators: {
                    notEmpty: {
                        message: 'Veuillez renseigner ce champ!'
                    }
                }
            },
        }

    });

    $('#modifier_dmd').bootstrapValidator({
        err: {
            container: '#messages'
        },
        fields: {
            "selectingpeoplebundle_spdemanderecrutement[nomDemandeur]": {
                validators: {
                    notEmpty: {
                        message: 'Veuillez renseigner ce champ!'
                    }
                }
            },
            "selectingpeoplebundle_spdemanderecrutement[prenomDemandeur]": {
                validators: {
                    notEmpty: {
                        message: 'Veuillez renseigner ce champ!'
                    }
                }
            },
            "selectingpeoplebundle_spdemanderecrutement[mailDemandeur]":{
                validators: {
                    notEmpty: {
                        message: 'Veuillez renseigner ce champ!'
                    },
                    emailAddress: {
                        message: 'Veuillez entrer un email valide!'
                    }
                }
            },
            "selectingpeoplebundle_spdemanderecrutement[ville]": {
                validators: {
                    notEmpty: {
                        message: 'Veuillez renseigner ce champ!'
                    }
                }
            },
            "selectingpeoplebundle_spdemanderecrutement[dir]": {
                validators: {
                    notEmpty: {
                        message: 'Veuillez renseigner ce champ!'
                    }
                }
            },
            "selectingpeoplebundle_spdemanderecrutement[srv]": {
                validators: {
                    notEmpty: {
                        message: 'Veuillez renseigner ce champ!'
                    }
                }
            },
            "selectingpeoplebundle_spdemanderecrutement[pst]": {
                validators: {
                    notEmpty: {
                        message: 'Veuillez renseigner ce champ!'
                    }
                }
            },
            "selectingpeoplebundle_spdemanderecrutement[steId]": {
                validators: {
                    notEmpty: {
                        message: 'Veuillez renseigner ce champ!'
                    }
                }
            },
            "selectingpeoplebundle_spdemanderecrutement[contratId]": {
                validators: {
                    notEmpty: {
                        message: 'Veuillez renseigner ce champ!'
                    }
                }
            },
            "selectingpeoplebundle_spdemanderecrutement[statutCollaborateurId]": {
                validators: {
                    notEmpty: {
                        message: 'Veuillez renseigner ce champ!'
                    }
                }
            },
            "selectingpeoplebundle_spdemanderecrutement[paysId]": {
                validators: {
                    notEmpty: {
                        message: 'Veuillez renseigner ce champ!'
                    }
                }
            },
            "selectingpeoplebundle_spdemanderecrutement[salaire]": {
                validators: {
                    notEmpty: {
                        message: 'Veuillez renseigner ce champ!'
                    },
                    numeric: {
                        message: 'Entrez un salaire valide!'
                    }
                }
            },
            "selectingpeoplebundle_spdemanderecrutement[periodeEssai]": {
                validators: {
                    numeric: {
                        message: 'Entrez une période valide!'
                    }
                }
            },
            "selectingpeoplebundle_spdemanderecrutement[reference]": {
                validators: {
                    stringLength: {
                        min: 12,
                        max: 12,
                        message: 'Veuillez fournir au moins 12 caractères'
                    }
                }
            },

        }

    });

})
