<?php

namespace Suivi\Twig;

use Symfony\Component\Security\Core\SecurityContext;
use Doctrine\ORM\EntityManager;

class Extensions extends \Twig_Extension {

    protected $container;
    protected $em;

    function __construct(SecurityContext $container, EntityManager $em) {
        $this->container = $container;
        $this->em = $em;
    }

    public function getUserId() {
        // only if user is logged in
        if (null !== $this->container->getToken() && $this->container->getToken()->getUser() != "anon.") {
            return $this->container->getToken()->getUser()->getId();
        }
        return null;
    }

    public function getPoleId() {
        $userId = $this->getUserId();
        if(null !== $userId) {
            $departament = $this->em->getRepository('SuiviEtudesBundle:ProjectTech')->find($userId);
            if ($departament) {
                return $departament->getPole()->getId();
            }
        }
        return null;
    }

    public function getGlobals() {
        return array(
            'absenceDepId' => $this->getPoleId()
        );
    }

    public function getName() {
        return 'twig_extension';
    }

}
