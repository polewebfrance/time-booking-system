<?php

namespace Suivi\ReportingBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Ob\HighchartsBundle\Highcharts\Highchart;
use Suivi\ReportingBundle\Entity\Reporting;
use Symfony\Component\HttpFoundation\Request;
use Suivi\EtudesBundle\Entity\Demande;
use Suivi\EtudesBundle\Entity\Statut;

class ReportingController extends Controller
{
    public function statsAction() {


        /*color: ['#18B738','#2799e9','#2780e3','#2139AF','#FE8A06','#ff7518','#BA62D5','#9954bb','#777777','#222222','#ff0039'];*/

        $em = $this->getDoctrine()->getManager();
        //Récupération des sommes des demandes, regroupées par directions
        $demandesByDirections = $em->getRepository('SuiviEtudesBundle:Demande')->getDemandesByDirections();


        //Restructuration du tableau
        $data = array();
        $drilldown = array();
       $i=0;

        foreach($demandesByDirections as $direction) {
            $compteurService=0;
            //Gestion du cas des directions non défini
            if ($direction['libelle'] == NULL) {
                $data[] = array(
                    'name' => "Non défini",
                    'y' => intval($direction['nb']),
                    'drilldown' => 'dir_non_def',
                );
            }
            else {
                $data[] = array(
                    'name' => $direction['libelle'],
                    'y' => intval($direction['nb']),
                    'drilldown' => 'dir_'.$direction['libelle'],
                );
            }
            //Alimentation de l'array "dcata", pour le premier niveau du graphique
            //Pour cette direction, on réupère le détail de chaque service
            $demandesByServices = $em->getRepository('SuiviEtudesBundle:Demande')->getDemandesByServices($direction['id']);

            $dataN2 = array();
            $servicesDirection = $em->getRepository('AdministrationDirectionBundle:Direction')->find($direction['id'])->getServices();
            foreach($demandesByServices as $service) {
               if(in_array($em->getRepository('AdministrationDirectionBundle:Service')->find($service['id']),$servicesDirection->getValues())) {
                   //Gestion du cas des services non définis
                   if ($service['libelle'] == NULL) {
                       $dataN2[] = array(
                           'name' => 'Non défini',
                           'y' => intval($service['nb']),
                           'drilldown' => 'srv_non_defini',
                       );
                   } else {
                       $dataN2[] = array(
                           'name' => $service['libelle'],
                           'y' => intval($service['nb']),
                           'drilldown' => 'srv_' . $service['libelle'],
                       );
                   }

                   $compteurService += intval($service['nb']);


                   $demandesByStatuts = $em->getRepository('SuiviEtudesBundle:Demande')->getDemandesByStatutsService($service['id']);

                   $dataN3 = array();
                   foreach ($demandesByStatuts as $statut) {
                       //Gestion du cas des services non définis
                       //Alimentation de l'array pour le deuxième niveau
                       $dataN3[] = array(
                           'name' => $statut['name'],
                           'y' => intval($statut['nb']),
                           'url' => $this->generateUrl('suivi_etudes_liste', array('demande_search' => array('direction' => $direction['id'], 'service' => $service['id'], 'statut' => array($statut['id']))))
                       );
                   }


                   $drilldown[] = array(
                       'id' => 'srv_' . $service['libelle'],
                       'name' => 'Statut',
                       'type' => 'pie',
                       'data' => $dataN3,
                   );
               }
            }

            $i++;


            if($compteurService < intval($direction['nb'])){

                $dataN2[] = array(
                    'name' => 'Service Non défini',
                    'y' => intval($direction['nb'])-$compteurService,
                    'drilldown' => 'srv_non_defini',
                );
            }


            $drilldown[] = array(
                'id'   => 'dir_'.$direction['libelle'],
                'name' => 'Services',
                'type' => 'pie',
                'data' => $dataN2
            );


        }




        //Déclaration de l'objet
        $pieDirection = new Highchart();
        $pieDirection->chart->renderTo('pie');
        $pieDirection->chart->type('pie');
        $pieDirection->title->text("Répartition des demandes par directions");
        //$pieDirection->tooltip->formater($pieDirection->series->name.': <b>'.$ob->point->percentage.'%</b>');
        //Option
        $pieDirection->plotOptions->series(
            array(
                'cursor'    => 'pointer',
                'dataLabels'    => array(
                    'enabled' => true,
                    //'format' => 'nb : '.$ob->point->name,
                ),
                'showInLegend' => true,
            )
        );

        //legende niveau 1
        $pieDirection->series(
            array(array(
                'type' => 'pie', //type graphique du niveau 1
                'name' => 'Directions',
                'colorByPoint' => true,
                'data' => $data,
                'credit' => false
            ))
        );

        //definition du diagramme a afficher
        $pieDirection->drilldown->series($drilldown);

        return $this->render('SuiviReportingBundle:Reporting:statistique.html.twig', array(
            'pieDirection' => $pieDirection,
            'chart2' => json_encode($pieDirection),
            'data_series' => json_encode($data),
            'data_drilldown' => json_encode($drilldown)
        ));


    }

    public function etatAction() {
        $em = $this->getDoctrine()->getManager();
        //Récupération des sommes des demandes, regroupées par directions

        //$ApplicationsByStatut = $em->getRepository('SuiviEtudesBundle:Demande')->getDemandesByApplications();
        $listeApplication = $em->getRepository('SuiviEtudesBundle:Application')->findAll();
        foreach($listeApplication as $application) {
            $listeNameApplication[] = $application->getName();
        }

        //Récupération de la liste des statuts
        $listeStatut = $em->getRepository('SuiviEtudesBundle:Statut')->findAll();

        //Construction du tableau des données
        $demandesByApplications = array();
        //Pour chaque statut
        foreach($listeStatut as $statut) {
            //Récupération du nom
            $demandesByApplications[$statut->getId()]['name'] = $statut->getName();
            //Récupération du nombre de demandes, pour chaque application
            foreach($listeApplication as $application) {
                $demandesByApplications[$statut->getId()]['data'][] = intval($em->getRepository('SuiviEtudesBundle:Demande')->getDemandesByApplicationStatut($application->getId(), $statut->getId())[0][1]);
            }
            //Ajout un style
            switch($statut->getId()) {
                case Statut::STAND_BY:
                    $demandesByApplications[$statut->getId()]['stack'] = "Stand by";
                    $demandesByApplications[$statut->getId()]['color'] = '#FFFF75';
                    break;
                case Statut::NOUVEAU:
                    $demandesByApplications[$statut->getId()]['stack'] = "Nouveau";
                    $demandesByApplications[$statut->getId()]['color'] = '#18B738';
                    break;
                case Statut::PRIS_EN_COMPTE:
                    $demandesByApplications[$statut->getId()]['stack'] = "Nouveau";
                    $demandesByApplications[$statut->getId()]['color'] = '#A35200';
                    break;
                case Statut::ETUDES:
                    $demandesByApplications[$statut->getId()]['stack'] = "Nouveau";
                    $demandesByApplications[$statut->getId()]['color'] = '#2799e9';
                    break;
                case Statut::EN_ATTENTE:
                    $demandesByApplications[$statut->getId()]['stack'] = "Nouveau";
                    $demandesByApplications[$statut->getId()]['color'] = '#2780e3';
                    break;
                case Statut::VALIDE:
                    $demandesByApplications[$statut->getId()]['stack'] = "En cours";
                    $demandesByApplications[$statut->getId()]['color'] = '#1435BC';
                    $demandesByApplications[$statut->getId()]['color'] = '#2139AF';
                    break;
                case Statut::EN_COURS_CP:
                    $demandesByApplications[$statut->getId()]['stack'] = "En cours";
                    $demandesByApplications[$statut->getId()]['color'] = '#ff7518';
                    $demandesByApplications[$statut->getId()]['color'] = '#FE8A06';
                    break;
                case Statut::EN_COURS_DEV:
                    $demandesByApplications[$statut->getId()]['stack'] = "En cours";
                    $demandesByApplications[$statut->getId()]['color'] = '#CB6002';
                    $demandesByApplications[$statut->getId()]['color'] = '#ff7518';
                    break;
                case Statut::RECETTE_INFO:
                    $demandesByApplications[$statut->getId()]['stack'] = "En cours";
                    $demandesByApplications[$statut->getId()]['color'] = '#BA62D5';
                    break;
                case Statut::RECETTE_USER:
                    $demandesByApplications[$statut->getId()]['stack'] = "En cours";
                    $demandesByApplications[$statut->getId()]['color'] = '#9954bb';
                    break;
                case Statut::MISE_EN_PROD:
                    $demandesByApplications[$statut->getId()]['stack'] = "Traité";
                    $demandesByApplications[$statut->getId()]['color'] = '#777777';
                    break;
                case Statut::CLOS:
                    $demandesByApplications[$statut->getId()]['stack'] = "Traité";
                    $demandesByApplications[$statut->getId()]['color'] = '#222222';
                    break;
                case Statut::ANNULE:
                    $demandesByApplications[$statut->getId()]['stack'] = "Traité";
                    $demandesByApplications[$statut->getId()]['color'] = '#ff0039';
                    break;
            }
            //Ajout de l'url
            $demandesByApplications[$statut->getId()]['url'] = $this->generateUrl('suivi_etudes_liste', array('demande_search' => array('application' => $application->getId(),'statut' => $statut->getId())));

        }
//        var_dump($demandesByApplications);die;
        //Création du graphique
        $column = new Highchart();
        //Identification du conteneur HTML
        $column->chart->renderTo('barchart');
        //Titre du graphique
        $column->title->text("Répartition de l'activité du service par application");
        //Type de graphique
        $column->chart->type('column');
        //Options 3D
        $column->chart->options3d(
            array(
                'enabled'      => true,
                'alpha'        => 10,
                'beta'         => 10,
                'viewDistance' => 100,
                'depth'        => 50
            )
        );
        //Marges
        $column->chart->marginTop(60);
        $column->chart->marginRight(30);

        //Définition des abscisses et ordonnées
        $column->yAxis->title(array('text' => "Nombre de demandes"));
        $column->yAxis->allowDecimals(false);
        $column->xAxis->title(array('text' => "Applications"));
        $column->xAxis->categories($listeNameApplication);

        //Options globales au grpahique
        $column->plotOptions->column(
            array(
                'stacking' => 'normal',
                'depth'    => 50,
            )
        );
        //Ajout des données
        $column->series($demandesByApplications);

        return $this->render('SuiviReportingBundle:Reporting:etatservice.html.twig', array(
            'barchart' => $column,
            'data' => json_encode($demandesByApplications),
            'categories' => json_encode($listeNameApplication)
        ));

    }

    public function chargesAction() {
        $em = $this->getDoctrine()->getManager();
        $libelle = array("Chef de Projet", "Développement", "Recette");
        $couleur = array("#FE8A06", "#C299EB", "#BA62D5");

        $structureData = array();
        for ($i=0;$i<count($libelle);$i++) {
            $structureData[$i]['name'] = $libelle[$i];
            $structureData[$i]['color'] = $couleur[$i];
        }

        /**
         * REPARTITION DES CHARGES PAR APPLICATION
         */

        $chargesByApplications = $em->getRepository('SuiviEtudesBundle:Demande')->getChargesByApplication();
        $i = 0;
        $chargesByApps = array();
        $chargesToutesApplications = $structureData;
        foreach($em->getRepository('SuiviEtudesBundle:Application')->findAll() as $application) {

            $chargesByApp = $structureData;
            $chargesByApp[0]['data'][] = intval($chargesByApplications[$i]['chargesCPRest']);
            $chargesByApp[1]['data'][] = intval($chargesByApplications[$i]['chargesDevRest']);


            $chargesToutesApplications[0]['data'][] = intval($chargesByApplications[$i]['chargesCPRest']);
            $chargesToutesApplications[1]['data'][] = intval($chargesByApplications[$i]['chargesDevRest']);

            $categories[] = $application->getName();
            //$chargesByApps[$i]['name'] = $application->getName();

            $chargesByApps[$i] = $chargesByApp;
            $i++;
        }

        //var_dump($chargesByApplications, $chargesByApps[0], $data, $categories);die();



        /**
         * REPARTITION DES CHARGES PAR STATUT
         */
        $chargesByStatut = $em->getRepository('SuiviEtudesBundle:Demande')->getChargesByStatut();

        $demandesByStatut = $structureData;
        $demandesByStatutPie = $structureData;

        foreach($chargesByStatut as $statut) {
            $demandesByStatut[0]['data'] = array(intval($statut['chargesCPRest']));
            $demandesByStatutPie[0]['y'] = intval($statut['chargesCPRest']);
            $demandesByStatut[1]['data'] = array(intval($statut['chargesDevRest']));
            $demandesByStatutPie[1]['y'] = intval($statut['chargesDevRest']);
            //A CHANGER AVEC LES VRAIES CHARGES !!
            $demandesByStatut[2]['data'] = 0;
            $demandesByStatutPie[2]['y'] = 0;
        }


        return $this->render('SuiviReportingBundle:Reporting:chargesetudes.html.twig', array(
            //tableau global des 4 applications
            'series_app' => json_encode($chargesToutesApplications),
            //charge par application
            'series_sap' => json_encode($chargesByApps[0]),
            'series_crm' => json_encode($chargesByApps[1]),
            'series_cognos' => json_encode($chargesByApps[2]),
            'series_web' => json_encode($chargesByApps[3]),
            //charge global des 4 applications
            'chargesByApplications' => $chargesByApplications,
            //charge par statut
            'series_statut' => json_encode($demandesByStatut),
            'series_statut_pie' => json_encode($demandesByStatutPie),

            'categories' => json_encode($categories)

        ));

    }
}
