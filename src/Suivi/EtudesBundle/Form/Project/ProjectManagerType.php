<?php

namespace Suivi\EtudesBundle\Form\Project;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProjectManagerType extends AbstractType {

    private $em;

    public function __construct($em) {
        $this->em = $em;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('manager', 'entity', array(
                    'class' => 'UserUserBundle:User',
                    'property' => 'displayname',
                    'query_builder' => $this->em->getRepository('UserUserBundle:User')
                    ->getByRole('ROLE_SET_MANAGER_TECH'),
                    'required' => true,
                    'multiple' => false,
                    'expanded' => false,
                    'mapped' => true,
                    'empty_value' => ''
                ))
                ->add('pole', 'entity', array(
                    'class' => 'SuiviEtudesBundle:ProjectPole',
                    'property' => 'name',
                    'required' => true,
                    'multiple' => false,
                    'expanded' => false,
                    'mapped' => true,
                    'empty_value' => ''
                ))
                ->add('save', 'submit')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Suivi\EtudesBundle\Entity\ProjectManager'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return '';
    }

}
