<?php

namespace Suivi\EtudesBundle\Form\Project;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProjectTechType extends AbstractType {

    private $em;

    public function __construct($em) {
        $this->em = $em;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('tech', 'entity', array(
                    'class' => 'UserUserBundle:User',
                    'property' => 'displayname',
                    'query_builder' => $this->em->getRepository('UserUserBundle:User')
                    ->getByRole('ROLE_SET_TECH'),
                    'required' => true,
                    'multiple' => false,
                    'expanded' => false,
                    'mapped' => true,
                    'empty_value' => ''
                ))
                ->add('pole', 'entity', array(
                    'class' => 'SuiviEtudesBundle:ProjectPole',
                    'property' => 'name',
                    'required' => true,
                    'multiple' => false,
                    'expanded' => false,
                    'mapped' => true,
                    'empty_value' => ''
                ))
                ->add('poste', 'entity', array(
                    'class' => 'SuiviEtudesBundle:ProjectJob',
                    'property' => 'name',
                    'required' => true,
                    'multiple' => false,
                    'expanded' => false,
                    'mapped' => true,
                    'empty_value' => ''
                ))
                ->add('techInfo', 'collection', array(
                    'type' => new \Suivi\EtudesBundle\Form\Absence\AbsenceTechInfoType(),
                    'cascade_validation' => true,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'label' => false,
                    'by_reference' => false,
                ))
                ->add('tauxHoraire')
                ->add('capacite')
                ->add('country', 'entity', array(
                    'class' => 'SuiviEtudesBundle:ProjectCountry',
                    'property' => 'name',
                    'required' => true,
                    'multiple' => false,
                    'expanded' => false,
                    'mapped' => true,
                ))
                ->add('save', 'submit')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Suivi\EtudesBundle\Entity\ProjectTech',
            'cascade_validation' => true,
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return '';
    }

}
