<?php

namespace Suivi\EtudesBundle\Form\Absence;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AbsenceTypeType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('name')
                ->add('abbrev')
                ->add('substract', 'choice', array(
                    'choices' => array(
                        '0' => 'NON',
                        '1' => 'OUI',
                        '2' => 'Recuperation',
                    ),
                    'required' => true,
                    'empty_data' => 0
                ))
                ->add('techVisible', 'choice', array(
                    'choices' => array(
                        '0' => 'NON',
                        '1' => 'OUI'
                    ),
                    'required' => true,
                    'empty_data' => 0
                ))
                ->add('save', 'submit')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Suivi\EtudesBundle\Entity\AbsenceType'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return '';
    }

}
