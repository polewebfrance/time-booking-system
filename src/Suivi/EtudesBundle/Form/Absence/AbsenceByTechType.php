<?php

namespace Suivi\EtudesBundle\Form\Absence;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class AbsenceByTechType extends AbstractType {

    private $isTechManager;

    public function __construct($isTechManager) {
        $this->isTechManager = $isTechManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('type', 'entity', array(
                    'class' => 'SuiviEtudesBundle:AbsenceType',
                    'property' => 'name',
                    'empty_value' => "Choisissez un type",
                    'query_builder' => function(EntityRepository $r) {
                        $q = $r->createQueryBuilder('a')
                                ->where("a.substract < '2'")
                                ->orderBy('a.name', 'ASC');
                        #if user is not manager then is tech so hide other absence type
                        if ($this->isTechManager == false) {
                            $q->where("a.techVisible = '1'");
                        }
                        return $q;
                    },
                ))
                ->add('startDate', 'date', array(
                    'label' => 'Date de début:',
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd',
                ))
                ->add('endDate', 'date', array(
                    'label' => 'Date de fin:',
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd'
                ))
                ->add('days', 'text', array(
                    'label' => 'Jours'
                ))
                ->add('save', 'submit')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Suivi\EtudesBundle\Entity\AbsenceList',
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return '';
    }

}
