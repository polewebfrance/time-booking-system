<?php

namespace Suivi\EtudesBundle\Form\Absence;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class TimekeepingType extends AbstractType {

    private $managerId;

    public function __construct($managerId) {
        $this->managerId = $managerId;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('month', 'text', array(
                    'label' => 'Période:'
                ))
                ->add('country', 'entity', array(
                    'class' => 'SuiviEtudesBundle:ProjectCountry',
                    'property' => 'name',
                    'empty_value' => "Tous",
                    'label' => 'Pays:',
                    'query_builder' => function(EntityRepository $r) {
                        $q = $r->createQueryBuilder('country')
                                ->innerJoin('SuiviEtudesBundle:ProjectTech', 'tech', 'WITH', 'IDENTITY(tech.country)=country.id')
                                ->innerJoin('SuiviEtudesBundle:ProjectManager', 'manager', 'WITH', 'IDENTITY(manager.pole)=IDENTITY(tech.pole)')
                                ->groupBy('country.id')
                                ->orderBy('country.name', 'ASC');
                        return $q;
                    },
                ))
                ->add('departament', 'entity', array(
                    'class' => 'SuiviEtudesBundle:ProjectPole',
                    'property' => 'name',
                    'empty_value' => "Tous",
                    'label' => 'Pôle:',
                    'query_builder' => function(EntityRepository $r) {
                        $q = $r->createQueryBuilder('pole')
                                ->innerJoin('SuiviEtudesBundle:ProjectManager', 'manager', 'WITH', 'IDENTITY(manager.pole)=pole.id')
                                ->where('IDENTITY(manager.manager) = :managerId')
                                ->setParameter('managerId', $this->managerId)
                                ->orderBy('pole.name', 'ASC');
                        return $q;
                    }
        ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => null,
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return '';
    }

}
