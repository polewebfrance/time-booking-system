<?php

namespace Suivi\EtudesBundle\Form\Absence;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class AbsenceTechInfoType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('year', 'text',array(
                    'label' => 'Annee:'
                ))
                ->add('vacationDays', 'integer',array(
                    'label' => 'Jours de vacances'
                ))
                ->add('recHours', 'integer',array(
                    'label' => 'Heures de récupération:',
                    'required' => false
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Suivi\EtudesBundle\Entity\AbsenceTechInfo'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return '';
    }

}
