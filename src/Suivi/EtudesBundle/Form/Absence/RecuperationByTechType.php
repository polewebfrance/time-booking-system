<?php

namespace Suivi\EtudesBundle\Form\Absence;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class RecuperationByTechType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('type', 'entity', array(
                    'class' => 'SuiviEtudesBundle:AbsenceType',
                    'property' => 'name',
                    'empty_value' => "Choisissez un type",
                    'query_builder' => function(EntityRepository $r) {
                        $q = $r->createQueryBuilder('a')
                                ->where("a.substract = '2'")
                                ->orderBy('a.name', 'ASC');
                        return $q;
                    },
                ))
                ->add('startDate', 'date', array(
                    'label' => 'Date:',
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd',
                ))
                ->add('hours', 'text', array(
                    'label' => 'Heures'
                ))
                ->add('save', 'submit')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Suivi\EtudesBundle\Entity\RecuperationList',
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return '';
    }

}
