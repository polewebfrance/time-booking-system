<?php

namespace Suivi\EtudesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DemandeEstimationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('charges', 'text',array(
                'required' => false
            ))
            ->add('chargesCP', 'text',array(
                'required' => false
            ))
            ->add('chargesDev', 'text',array(
                'required' => false
            ))
            ->add('date_recette', 'date', array(
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',

                'invalid_message' => 'la date de recette n\'est pas valide'
            ))
            ->add('devis', 'file', array(
                'mapped'=>false,
                'required' => false,
             ))
            ->add('date_production', 'date', array(
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',

                'invalid_message' => 'la date de mise en production n\'est pas valide'
            ))
        ;
    }


    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'attr' => ['id' => 'formEstimationId']
        ));
    }


    public function getName()
    {
      return 'suivi_etudes_demande_estimation';
    }
}