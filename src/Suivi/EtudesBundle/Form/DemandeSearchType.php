<?php

namespace Suivi\EtudesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Suivi\EtudesBundle\Entity\DemandeUser;
use Doctrine\ORM\EntityRepository;

class DemandeSearchType extends AbstractType {

    private  $directions ;

    public function buildForm(FormBuilderInterface $builder, array $options) {


        $this->directions = $options['data']->getDirection()->getValues();

        $builder
                ->add('demande_id', 'text', array(
                    'required' => false,
                ))
            ->add('direction', 'entity', array(
                    'class' => 'AdministrationDirectionBundle:Direction',
                    'query_builder' => function (EntityRepository $er) {
                          return $er->createQueryBuilder('d')->where('d.actif = 1');},
                    'property' => 'libelle',
                'empty_value' => 'Selectionner une direction',
                    'required' => false,
                    'multiple' => true,

                ))

                ->add('service', 'entity', array(
                    'class' => 'AdministrationDirectionBundle:Service',
                    'property' => 'libelle',
                    'empty_value' => '',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('service')->where('service.actif = 1')
                            ->andWhere('service.direction in (:listeDirection)')
                            ->setParameter('listeDirection',$this->directions )
                            ->orderBy('service.libelle', 'ASC');
                    },
                    'required' => false,
                    'multiple' => true,
                ))
                ->add('application', 'entity', array(
                    'class' => 'SuiviEtudesBundle:Application',
                    'property' => 'name',
                    'empty_value' => 'Selectionner une application',
                    'required' => false,
                ))
                ->add('type', 'entity', array(
                    'class' => 'SuiviEtudesBundle:Type',
                    'property' => 'name',
                    'empty_value' => 'Selectionner un type',
                    'required' => false,
                ))
                ->add('urgence', 'entity', array(
                    'class' => 'SuiviEtudesBundle:Urgence',
                    'property' => 'name',
                    'empty_value' => '',
                    'required' => false,
                ))
                ->add('statut', 'entity', array(
                    'class' => 'SuiviEtudesBundle:Statut',
                    'query_builder' => function(\Suivi\EtudesBundle\Entity\StatutRepository $r) {
                        $q = $r->createQueryBuilder('s')
                                ->addSelect('CASE WHEN (s.id > 0) THEN 1 ELSE 0 END HIDDEN orderParam')
                                ->orderBy('orderParam', 'DESC')
                                ->addOrderBy('s.id', 'ASC');
                        return $q;
                    },
                    'property' => 'name',
                    'empty_value' => 'Selectionner une statut',
                    'required' => false,
                    'multiple' => true,
                ))
                ->add('expression', 'text', array(
                    'required' => false,
                ))
                ->add('booDescription', 'choice', array(
                    'choices' => array('false' => 'dans le titre seulement.', 'true' => 'dans le titre et la description.'),
                    'expanded' => true,
                    'multiple' => false,
                    'required' => true,
                ))
                ->add('technicien', 'entity', array(
                    'class' => 'UserUserBundle:User',
                    'property' => 'displayname',
                    'empty_value' => 'Selectionner un technicien',
                    'required' => false,
                    'query_builder' => function(\Doctrine\ORM\EntityRepository $er) {
                        return $er->createQueryBuilder('u')
                                ->where("u.roles LIKE '%ROLE_SET_TECH%'")
                               ;
                    },
                ))
                ->add('demandeur', 'entity', array(
                    'class' => 'UserUserBundle:User',
                    'property' => 'displayname',
                    'empty_value' => 'selectionner un demandeur',
                    'required' => false,
                    'query_builder' => function(\Doctrine\ORM\EntityRepository $er) {
                        //Evolution  n'afficher que les demandeurs ! (demandeUser : type=demandeur)
                        return $er->createQueryBuilder('u')
                                ->innerJoin('Suivi\EtudesBundle\Entity\DemandeUser', 'du', 'WITH', 'du.user = u.id')
                                ->andWhere('du.type = :typeLiaison')
                                ->setParameter('typeLiaison', DemandeUser::DEMANDEUR)
                                ->orWhere('du.type = :typeLiaison2')
                                ->setParameter('typeLiaison2', DemandeUser::OBSERVATEUR)
                                ->orderBy("u.displayname", "ASC");
                    },
                ))
                ->add('nbResulParPage', 'choice', array(
                    'choices' => array('20' => '20', '50' => '50', '100' => '100', 'all' => 'tout les résultats'),
                    'mapped' => false,
                ))
                ->add('Rechercher', 'submit')

        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Suivi\EtudesBundle\Entity\DemandeSearch',
        ));
    }

    public function getName() {
        return 'demande_search';
    }

}
