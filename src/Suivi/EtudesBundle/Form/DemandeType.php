<?php

namespace Suivi\EtudesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Suivi\EtudesBundle\Form\suiviMailType;
use Suivi\EtudesBundle\Form\PieceJointeType;
use User\UserBundle\Form\UserType;
use Suivi\EtudesBundle\Entity\Mail;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Validator\Constraints as Assert;

class DemandeType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('direction', 'entity', array(
                    'mapped' => false,
                    'class' => 'AdministrationDirectionBundle:Direction',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('table')->where('table.actif = 1')
                                ->orderBy('table.libelle', 'ASC');
                    },
                    'empty_value' => 'Veuillez selctionner une ou plusieurs direction',
                    'required' => true,
                    'multiple' => true
                ))
                ->add('service', 'entity', array(
                    'mapped' => false,
                    'class' => 'AdministrationDirectionBundle:Service',
                    'required' => false,
                    'empty_value' => '',
                    'multiple' => true
                ))
                ->add('type', 'entity', array(
                    'class' => 'SuiviEtudesBundle:Type',
                    'property' => 'name',
                    'empty_value' => '',
                ))
                ->add('application', 'entity', array(
                    'mapped' => false,
                    'class' => 'SuiviEtudesBundle:Application',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('table')
                                ->orderBy('table.name', 'ASC');
                    },
                    'empty_value' => 'Veuillez selctionner une ou plusieurs application',
                    'multiple' => true
                ))
                ->add('titre', 'text')
                ->add('description', 'textarea')
                ->add('criterionFrequence', 'entity', array(
                    'class' => 'SuiviEtudesBundle:DemandeCriterionFrequence',
                    'property' => 'name',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('table')
                                ->orderBy('table.name', 'ASC');
                    },
                    'constraints' => array(
                        new Assert\NotNull(array('message' => 'La frequence d\'utilisation ne peut pas être vide'))
                    ),
                ))
                ->add('criterionPopulation', 'entity', array(
                    'class' => 'SuiviEtudesBundle:DemandeCriterionPopulation',
                    'property' => 'name',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('table')
                                ->orderBy('table.name', 'ASC');
                    },
                    'constraints' => array(
                        new Assert\NotNull(array('message' => 'La population concernee ne peut pas être vide'))
                    ),
                ))
                ->add('criterionOrigin', 'entity', array(
                    'class' => 'SuiviEtudesBundle:DemandeCriterionOrigin',
                    'property' => 'name',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('table')
                                ->orderBy('table.name', 'ASC');
                    },
                    'constraints' => array(
                        new Assert\NotNull(array('message' => 'La origine de la demande ne peut pas être vide'))
                    ),
                ))
                ->add('mails', 'entity', array(
                    'class' => 'SuiviEtudesBundle:Mail',
                    'property' => 'libelle',
                    'mapped' => false,
                    'multiple' => true,
                    'expanded' => true
                ))
                ->add('suivi', 'entity', array(
                    'class' => 'UserUserBundle:User',
                    'property' => 'displayname',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('table')
                            ->where('table.expired = 0')
                            ->andWhere('table.enabled = 1')
                            ;},
                    'multiple' => true,
                    'empty_value' => '',
                    'mapped' => false,
                    'required' => false,
                ))
                ->add('mailing', 'checkbox', array(
                    'mapped' => false,
                    'required' => false,
                ))
                ->add('piecejointes', 'collection', array(
                    'type' => new PieceJointeType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                ))
            ->add('parent', 'entity', array(
                'class' => 'SuiviEtudesBundle:Demande',
                'empty_value' => '',
                'required' => false,
            ))
                ->add('Ajouter', 'submit')
                ->add('Ajouter', 'submit', array(
                    'label' => 'Envoyer votre demande',
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Suivi\EtudesBundle\Entity\Demande'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'suivi_etudesbundle_demande';
    }

}
