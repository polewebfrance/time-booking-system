<?php

namespace Suivi\EtudesBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\Common\Persistence\Doctrine\Common\Persistence\ObjectManager;

class DemandeEditType extends AbstractType {

    private $demande;
    private $listeObservateurs;
    private $listeChefDeProjets;
    private $listeTechs;
    private $em;
    private $directions = array();
    public $listeDirection = array();

    public function __construct($demande, $em) {
        $this->demande = $demande;
        $this->em = $em;
        $this->listeObservateurs = $em->getRepository('SuiviEtudesBundle:DemandeUser')->getObservateursbyDemande($demande);
        $this->listeChefDeProjets = $em->getRepository('SuiviEtudesBundle:DemandeUser')->getChefDeProjetbyDemande($demande);
        $this->listeTechs = $em->getRepository('SuiviEtudesBundle:DemandeUser')->getTechniciensbyDemande($demande);


    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $this->directions = implode(',', $this->directions);
        $this->listeDirection=$options['data']->getDirections();
        $this->listeServices=$options['data']->getServices();

        $builder
                ->add('direction', 'entity', array(
                    'mapped' => false,
                    'class' => 'AdministrationDirectionBundle:Direction',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('d')->where('d.actif = 1')
                            ->orderBy('d.libelle', 'ASC');
                    },
                    'property' => 'libelle',
                    'required' => true,
                    'empty_value' => '',
                    'multiple' => true,
                    'data' => $this->listeDirection
                ))
            ->add('parent', 'entity', array(
                'class' => 'SuiviEtudesBundle:Demande',
                'required' => false,
                'empty_value' => '',

            ))
                ->add('service', 'entity', array(
                    'mapped' => false,
                    'class' => 'AdministrationDirectionBundle:Service',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('service')->where('service.actif = 1')
                            ->andWhere('service.direction in (:listeDirection)')
                            ->setParameter('listeDirection',$this->listeDirection )
                            ->orderBy('service.libelle', 'ASC');
                    },
                    'empty_value' => '',
                    'required' => false,
                    'multiple' => true,
                    'data' => $this->listeServices,
                ))
                //Pour l'instant, le statut ne sera pas modifiable, c'est pourquoi le champ est désactivé.
                ->add('statut', 'entity', array(
                    'class' => 'SuiviEtudesBundle:Statut',
                    'property' => 'name',
                    'required' => false,
                ))
                ->add('suiviMail', 'entity', array(
                    'class' => 'UserUserBundle:User',
                    'property' => 'displayname',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('table')
                            ->where('table.expired = 0')
                            ->andWhere('table.enabled = 1')
                            ;},
                    'required' => false,
                    'multiple' => true,
                    'expanded' => false,
                    'mapped' => false,
                    'data' => $this->listeObservateurs,
                ))
                ->add('chefdeProjet', 'entity', array(
                    'class' => 'UserUserBundle:User',
                    'property' => 'displayname',
                    'query_builder' => $this->em->getRepository('UserUserBundle:User')
                    ->getByRole('ROLE_SET_TECH'),
                    'required' => false,
                    'multiple' => true,
                    'expanded' => false,
                    'mapped' => false,
                    'data' => $this->listeChefDeProjets,
                ))
                ->add('techniciens', 'entity', array(
                    'class' => 'UserUserBundle:User',
                    'property' => 'displayname',
                    'query_builder' => $this->em->getRepository('UserUserBundle:User')
                    ->getByRole('ROLE_SET_TECH'),
                    'required' => false,
                    'multiple' => true,
                    'expanded' => false,
                    'mapped' => false,
                    'data' => $this->listeTechs,
                ))
                ->add('piecejointes', 'collection', array(
                    'type' => new PieceJointeType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'mapped' => false,
                ))
                ->add('Modifier', 'submit', array(
                    'label' => 'Enregistrer les modifications',
                ))
        ;
    }

    public function getName() {
        return 'suivi_etudes_demande_edit';
    }

}
