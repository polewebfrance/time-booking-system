<?php

namespace Suivi\EtudesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DemandeRecetteUserOkType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('date_production', 'date', array(
        		'widget' => 'single_text',
        		'format' => 'dd/MM/yyyy',))
            //->add('Modifier', 'submit')
        
        ->add('commentaire','textarea', array(
        		'mapped'=>false,
        ))

        ->add('cahier', 'file', array(
        		'mapped'=>false,
        		'required' => false,
        ))

            ->add('input1', 'text', array( 'mapped'=>false, 'attr'=>array(
                'class'=>'rating rating-loading' ,'data-min'=>'0' ,'data-max'=>'5', 'data-step'=>'1', 'data-size'=>'xs' ,'data-show-clear'=>'true',
        'data-show-caption'=>'false'
            )))

            ->add('input2', 'text', array( 'mapped'=>false, 'attr'=>array(
                'class'=>'rating rating-loading' ,'data-min'=>'0' ,'data-max'=>'5', 'data-step'=>'1', 'data-size'=>'xs' ,'data-show-clear'=>'true',
                'data-show-caption'=>'false'
            )))

            ->add('input3', 'text', array( 'mapped'=>false, 'attr'=>array(
                'class'=>'rating rating-loading' ,'data-min'=>'0' ,'data-max'=>'5', 'data-step'=>'1', 'data-size'=>'xs' ,'data-show-clear'=>'true',
                'data-show-caption'=>'false'
            )))

        ;
    }

    public function getName()
    {
      return 'suivi_etudes_demande_recetteuserok';
    }
}
