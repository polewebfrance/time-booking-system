<?php
namespace Suivi\EtudesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DemandeModifierDateType extends AbstractType
{
    /**
     * Valérian GUEMENE <vguemene@noz.fr>
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('date_recette', 'date', array(
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy'))

            ->add('date_production', 'date', array(
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy'))
        ;
    }

    public function getName()
    {
      return 'suivi_etudes_modifier_date';
    }
}