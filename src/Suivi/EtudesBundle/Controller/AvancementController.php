<?php

namespace Suivi\EtudesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Suivi\EtudesBundle\Form\EstimationEtudesType;
use Suivi\EtudesBundle\Form\AvancementType;


class AvancementController extends Controller
{

    /**
     * EstimationEtudesAction : Renseignement des estimations de la demande, lors de la phase Etudes
     * @author allard_pi
     * @param $id id d'une demande 
     * @return redirection
     */
    public function EstimationEtudesAction(Request $request, $id) {	
        //init des entity manager
        $em = $this->getDoctrine()->getManager();
        //récupération de la demande
        $demande = $em->getRepository('SuiviEtudesBundle:Demande')->find($id);
        //Création du formulaire
        $form = $this->createForm(new EstimationEtudesType, $demande);
        
        $form->handleRequest($request);
        
        if ($form->isValid()) {
            $em->flush();
            //message de confirm
            $this->get('session')->getFlashBag()->add('success', 'Ces estimations ont été ajoutées à cette demande !');

        } else {
            $this->get('session')->getFlashBag()->add('erreur', 'Erreur ! Veuillez recommencer, ou contacter le pôle web');
            foreach ($form->getErrors() as $key => $error) {
                $this->get('session')->getFlashBag()->add('erreur', $error->getMessage());
            }
            foreach ($form->all() as $child) {
                if (!$child->isValid()) {
                    foreach ($child->getErrors() as $key => $error) {
                        $this->get('session')->getFlashBag()->add('erreur', $error->getMessage());
                    }
                }
            }
        }
        //retour vers la vue
    	return $this->redirect( $this->generateUrl('suivi_etudes_voir', array('id' =>  $demande->getId())) );
    }//Fin methode EstimationEtudesAction
    	
    
    public function RealiseAction(Request $request, $id) {	
        //init des entity manager
        $em = $this->getDoctrine()->getManager();
        //récupération de la demande
        $demande = $em->getRepository('SuiviEtudesBundle:Demande')->find($id);
        //Création du formulaire
        $form = $this->createForm(new AvancementType, $demande);
        
        $form->handleRequest($request);
        
        if ($form->isValid()) {
            $em->flush();
            //message de confirm
            $this->get('session')->getFlashBag()->add('success', 'L\'avancement de la demande a bien été renseigné!');

        } else {
            $this->get('session')->getFlashBag()->add('erreur', 'Erreur ! Veuillez recommencer, ou contacter le pôle web');
            foreach ($form->getErrors() as $key => $error) {
                $this->get('session')->getFlashBag()->add('erreur', $error->getMessage());
            }
            foreach ($form->all() as $child) {
                if (!$child->isValid()) {
                    foreach ($child->getErrors() as $key => $error) {
                        $this->get('session')->getFlashBag()->add('erreur', $error->getMessage());
                    }
                }
            }
        }
        //retour vers la vue
    	return $this->redirect( $this->generateUrl('suivi_etudes_voir', array('id' =>  $demande->getId())) );
    }//Fin methode RealiseAction
}