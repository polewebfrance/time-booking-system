<?php

namespace Suivi\EtudesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Suivi\EtudesBundle\Entity\Statut;
use Suivi\EtudesBundle\Entity\DemandeUser;
use Suivi\EtudesBundle\Form\DemandeEstimationType;
use Suivi\EtudesBundle\Form\DemandeRecetteUserOkType;
use Suivi\EtudesBundle\Form\DemandeRecetteUserNotOkType;
use Suivi\EtudesBundle\Form\DemandeAffectationType;
use Suivi\EtudesBundle\Form\DemandeChangerProdType;
use Suivi\EtudesBundle\Form\DemandeSpecType;
use Suivi\EtudesBundle\Form\DemandeModifierDateType;
use Suivi\EtudesBundle\Entity\PieceJointe;
use Suivi\EtudesBundle\Entity\Note;
use Symfony\Component\HttpFoundation\Response;

class StatutController extends Controller {

    /**
     * TEST MAIL
     * @author allard_pi
     * @param $id id d'une demande
     * @return vue d'un mail
     */
    public function testmailAction($id, Request $request) {
        //init des entity manager
        $em = $this->getDoctrine()->getManager();
        //récupération de la demande
        $demande = $em->getRepository('SuiviEtudesBundle:Demande')->find($id);
        //Récupération du motif et de la description du refus lors d'un devis refusé
        $motifRefus = $request->query->get('suivi_etudesbundle_demande')['motif'];

        //Récupération du demandeur
        $userDmd = $demande->getDemandeUser()->getdisplayname();
        //Récupération du motif lors d'un devis refusé
        if (Statut::EN_ATTENTE) {
            $motifRefus = $request->request->get('suivi_etudesbundle_demande')['motif'];
        }

        $url = $request->getScheme() . '://' . $request->getHttpHost();

        return $this->render('SuiviEtudesBundle:Mail:etudes.html.twig', array(
                    'demande' => $demande,
                    'url' => $url,
                    'userDmd' => $userDmd
        ));
    }

    /**
     * EtudesAction : Passe la demande au statut "PRIS_EN_COMPTE"
     * @author manea_ti
     * @param $id id d'une demande
     * @return redirection
     */
    public function PrisEnCompteAction($id) {
        //init des entity manager
        $em = $this->getDoctrine()->getManager();
        //récupération de la demande
        $demande = $em->getRepository('SuiviEtudesBundle:Demande')->find($id);

        if ($demande->getStatut()->getId() == Statut::NOUVEAU) {
            //récupération du statut "Etudes"
            $statut = $em->getRepository('SuiviEtudesBundle:Statut')->find(Statut::PRIS_EN_COMPTE);
            //passage au statut "Etudes"
            $demande->setStatut($statut);
            //flush doctrine
            $em->flush();
            //message de confirm
            $this->get('session')->getFlashBag()->add('success', 'Votre statut a bien été modifié !');

            $this->envoyerMail(null, $demande);
        } else {
            $this->get('session')->getFlashBag()->add('info', 'Cette demande a déjà été prise en compte.');
        }

        //retour vers la vue
        return $this->redirect($this->generateUrl('suivi_etudes_voir', array('id' => $demande->getId())));
    }

    /**
     * EtudesAction : Passe la demande au statut "Etudes"
     * @author allard_pi
     * @param $id id d'une demande
     * @return redirection
     */
    public function EtudesAction($id) {
        //init des entity manager
        $em = $this->getDoctrine()->getManager();
        //récupération de la demande
        $demande = $em->getRepository('SuiviEtudesBundle:Demande')->find($id);

        if ($demande->getStatut()->getId() == Statut::PRIS_EN_COMPTE) {
            //récupération du statut "Etudes"
            $statut = $em->getRepository('SuiviEtudesBundle:Statut')->find(Statut::ETUDES);
            //passage au statut "Etudes"
            $demande->setStatut($statut);
            //flush doctrine
            $em->flush();
            //message de confirm
            $this->get('session')->getFlashBag()->add('success', 'Votre statut a bien été modifié !');

            $this->envoyerMail(null, $demande);
            //instanciation de l objet mail
            //$mail = $this->container->get('suivi_etudes.mail');
            //$mail->envoyer(null, $demande);
        } else {
            $this->get('session')->getFlashBag()->add('info', 'Cette demande a déjà été prise en compte.');
        }

        //retour vers la vue
        return $this->redirect($this->generateUrl('suivi_etudes_voir', array('id' => $demande->getId())));
    }

    /**
     * AnnuleAction : Annule la demande
     * @author guemene_va
     * @param $id id d'une demande
     * @return redirection
     */
    public function AnnuleAction(Request $request, $id) {

        //init des entity manager
        $em = $this->getDoctrine()->getManager();
        //récupération de la demande
        $demande = $em->getRepository('SuiviEtudesBundle:Demande')->find($id);

        //On garde le statut, pour savoir d'où la demande vient
        $old_statut = $demande->getStatut();

        //Récupération du statut "Annulé"
        $statut = $em->getRepository('SuiviEtudesBundle:Statut')->find(Statut::ANNULE);

        //Récupération de la description de l'annulation
        $descAnnulation = $request->query->get('descAnnulation');

        if (isset($descAnnulation)) {
            //Définition du statut annulé pour la demande
            $demande->setStatut($statut);

            //Enregistrement du statut annulé pour la demande
            $em->flush();

            //message de confirmation de l'annulation
            $this->get('session')->getFlashBag()->add('success', "L'annulation de la demande à bien été prise en compte.");

            //Envoi du ou des mail(s)
            $this->envoyerMail($old_statut, $demande, $descAnnulation);
        } else {
            $this->get('session')->getFlashBag()->add('erreur', 'Erreur ! Veuillez recommencer, ou contacter le pôle web');
        }
        //Retour à la vue
        return $this->redirect($this->generateUrl('suivi_etudes_voir', array('id' => $demande->getId())));
    }

    /**
     * EnAttenteAction : Passe la demande au statut "En attente"
     * @author allard_pi/guemene_va
     * @param $id id d'une demande
     * @return redirection
     */
    public function EnAttenteAction(Request $request, $id) {
        //init des entity manager
        $em = $this->getDoctrine()->getManager();
        //récupération de la demande
        $demande = $em->getRepository('SuiviEtudesBundle:Demande')->find($id);

        //On garde le statut, pour savoir d'où la demande vient
        $old_statut = $demande->getStatut();

        //Création du formulaire
        $form = $this->createForm(new DemandeEstimationType, $demande);
        // get old value from demande entity to be compared with values from the form
        $charges = $demande->getCharges();
        $chargesCp = $demande->getChargesCp();
        $chargesDev = $demande->getChargesDev();
        $dateRecette = $demande->getDateRecette();
        $dateProd = $demande->getDateProduction();
        $oldValues = array(
            'charges' => !empty($charges) ? $charges : null,
            'chargesCp' => !empty($chargesCp) ? $chargesCp : null,
            'chargesDev' => !empty($chargesDev) ? $chargesDev : null,
            'chargesDev' => !empty($dateRecette) ? $dateRecette->format('Y-m-d') : null,
            'dateProd' => !empty($dateProd) ? $dateProd->format('Y-m-d') : null,
        );
        $form->handleRequest($request);

        if ($form->isValid()) {
            // get form new values
            $newValues = array(
                'charges' => $form->get('charges')->getData(),
                'chargesCp' => $form->get('chargesCP')->getData(),
                'chargesDev' => $form->get('chargesDev')->getData(),
                'dateRecette' => $form->get('date_recette')->getData()->format('Y-m-d'),
                'dateProd' => $form->get('date_production')->getData()->format('Y-m-d')
            );
            if ($demande->getCharges() and is_numeric($demande->getChargesCP()) and is_numeric($demande->getChargesDev())) {
                if ($demande->getStatut()->getId() == Statut::ETUDES) {
                    $statut_en_attente = $em->getRepository('SuiviEtudesBundle:Statut')->find(Statut::EN_ATTENTE);
                    $demande->setStatut($statut_en_attente);
                }
                // compare DB values with form values and if there are differences set chargeModify to null
                $compare = $em->getRepository('SuiviEtudesBundle:Demande')->compareOldWithNewValues($oldValues, $newValues);
                if ($demande->getStatut()->getId() == Statut::EN_ATTENTE && !empty($compare)) {
                    $demande->setChargeModif(null);
                }
                if ($form->get('devis')->getData()) {
                    $devis = new PieceJointe();
                    $devis->setFile($form->get('devis')->getData());
                    $devis->setDemande($demande);

                    $listePJ = $em->getRepository('SuiviEtudesBundle:PieceJointe')->getPJparDemande($demande->getId());
                    $i = 0;
                    foreach ($listePJ as $pj) {
                        if ($pj->getName() == $devis->getName()) {
                            $pj->setFile($form->get('devis')->getData());
                            $pj->upload();
                            $demande->setDevis($pj);
                            $i++;
                        }
                    }
                    if ($i == 0) {
                        $em->persist($devis);
                        $demande->setDevis($devis);
                    }
                }

                $em->flush();
                //message de confirm
                $this->get('session')->getFlashBag()->add('notice', 'Vos estimations ont été prises en compte !');
                $this->envoyerMail($old_statut, $demande);
            } else {
                //s'il manque l'une des dates ou la charge
                $this->get('session')->getFlashBag()->add('erreur', 'Vous devez renseigner les charges et les dates de recette et de mise en production !');
            }
        } else {
            $this->get('session')->getFlashBag()->add('erreur', 'Erreur ! Veuillez recommencer, ou contacter le pôle web');
            foreach ($form->getErrors() as $key => $error) {
                $this->get('session')->getFlashBag()->add('erreur', $error->getMessage());
            }
            foreach ($form->all() as $child) {
                if (!$child->isValid()) {
                    foreach ($child->getErrors() as $key => $error) {
                        $this->get('session')->getFlashBag()->add('erreur', $error->getMessage());
                    }
                }
            }
        }
        //retour vers la vue
        return $this->redirect($this->generateUrl('suivi_etudes_voir', array('id' => $demande->getId())));
    }

    public function valideAction($id) {
        $em = $this->getDoctrine()->getManager();
        $demande = $em->getRepository('SuiviEtudesBundle:Demande')->find($id);
        if ($demande->getStatut()->getId() == Statut::VALIDE) {
            $statut_en_cour_cp = $em->getRepository('SuiviEtudesBundle:Statut')->find(Statut::EN_COURS_CP);
            $demande->setStatut($statut_en_cour_cp);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'Votre statut a bien été modifié !');
            return new Response('Ok');
        } else {
            return new Response('Not ok');
        }
    }

    /**
     * valideDevisAction : traite la validation des estimations
     * Valerian Guemene <vguemene@noz.fr>
     * @param unknown $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function valideDevisAction(Request $request, $id) {

        //init des entity manager
        $em = $this->getDoctrine()->getManager();
        //récupération de la demande
        $demande = $em->getRepository('SuiviEtudesBundle:Demande')->find($id);

        //On garde le statut, pour savoir d'où la demande vient
        $old_statut = $demande->getStatut();

        if ($demande->getStatut()->getId() < Statut::VALIDE) {
            //récupération du statut "Etudes"
            $statut = $em->getRepository('SuiviEtudesBundle:Statut')->find(Statut::VALIDE);
            //passage au statut "Etudes"
            $demande->setStatut($statut);

            $demande->setChargesCPRestante($demande->getChargesCP());
            $demande->setChargesDevRestante($demande->getChargesDev());
            $demande->setChargeModif(null);
            //flush doctrine
            $em->flush();
            //message de confirm
            $this->get('session')->getFlashBag()->add('success', 'Votre validation a été confirmée !');

            //Envoi du ou des mail(s)
            $this->envoyerMail($old_statut, $demande);
        } else {
            $this->get('session')->getFlashBag()->add('info', 'Les estimations de cette demande ont déjà été validées.');
        }

        //retour vers la vue
        return $this->redirect($this->generateUrl('suivi_etudes_voir', array('id' => $demande->getId())));
    }

    /**
     * refusDevisAction : traite le refus des estimations
     * @author Valerian Guemene <vguemene@noz.fr>
     * (non-PHPdoc)
     * @see \Symfony\Bundle\FrameworkBundle\Controller\Controller::createAccessDeniedException()
     */
    public function refusDevisAction(Request $request, $id) {

        $em = $this->getDoctrine()->getManager();
        $demande = $em->getRepository('SuiviEtudesBundle:Demande')->find($id);
        $demande->setChargeModif('1');

        //On garde le statut, pour savoir d'où la demande vient
        $old_statut = $demande->getStatut();

        //récupération des variables du formulaire
        $motif = $request->query->get('motif');
        $description = $request->query->get('description');

        if ($description) {
            //message de confirm
            $this->get('session')->getFlashBag()->add('info', "Votre refus (" . $motif . ") a bien été pris en comte");

            $demande->setMotifRefus($motif);
            $demande->setDescriptionRefus($description);
            $em->flush();
            //Envoi du ou des mail(s)
            $this->envoyerMail($old_statut, $demande, array($motif, $description)); //?
        } else {
            $this->get('session')->getFlashBag()->add('erreur', 'Erreur ! Vous devez renseigner la description de votre refus');
        }
        //retour vers la vue
        return $this->redirect($this->generateUrl('suivi_etudes_voir', array('id' => $demande->getId())));
    }

    public function standByAction(Request $request, $id) {
        // create form to validate with data from SuiviEtudes->Demande->voir action
        $form = $this->createForm(new \Suivi\EtudesBundle\Form\StandByForm);
        $form->handleRequest($request);
        $reason = $form->getData()['raison'];
        $description = $form->getData()['description'];
        if (null !== $reason && !empty($description)) {
            // add into modification table
            $em = $this->getDoctrine()->getManager();
            $demande = $em->getRepository('SuiviEtudesBundle:Demande')->find($id);
            $old_statut = $demande->getStatut()->getName();

            $modifs = new \Suivi\EtudesBundle\Entity\Modification();
            $modifs->setDate(new \DateTime('now'));
            $modifs->setUser($this->getUser());
            $modifs->setDemande($demande);
            $modifs->setType('statut');
            $modifs->setAncienneValeur($old_statut);
            $modifs->setNouvelleValeur('En attente de validation du passage en Stand-by pour la raison suivante - ' . $reason
                    . ' et commentaire - ' . $description);

            $demandeStandBy = new \Suivi\EtudesBundle\Entity\DemandeStandBy();
            $demandeStandBy->setDemande($demande);
            $demandeStandBy->setReason($reason);
            $demandeStandBy->setDescription($description);
            $demandeStandBy->setCreatedAt(new \Datetime());
            $demandeStandBy->setValidated('0');

            $em->persist($modifs);
            $em->persist($demandeStandBy);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'La demande de mis en Stand-by a bien été prise en compte.');
            // send mail to Gilles Danjou
            $sujet = "Une proposition de mise en \"Stand-by\" a été faite";
            if ($this->container->get('kernel')->getEnvironment() != 'prod') {
                $sujet = '[TEST] ' . $sujet;
            }
            $message = \Swift_Message::newInstance()
                    ->setSubject($sujet)
                    ->setFrom('suivietudes-standby@noz.fr', 'Suivi des Etudes')
                    ->setTo('GDANJOU@noz.fr')
                    ->setContentType('text/html')
                    ->setBody($this->renderView('SuiviEtudesBundle:Mail:standBy.html.twig', array(
                        'demande' => $demande,
                        'reason' => $reason,
                        'description' => $description,
                        'url' => $request->getScheme() . '://' . $request->getHttpHost(),
                        'environnement' => $this->container->get('kernel')->getEnvironment(),
                    )))
            ;
            $this->get('mailer')->send($message);

            return $this->redirect($this->generateUrl('suivi_etudes_voir', array('id' => $id)));
        } else {
            $this->get('session')->getFlashBag()->add('erreur', 'Impossible d\'aller sur le stand by statut.');
            return $this->redirect($this->generateUrl('suivi_etudes_voir', array('id' => $id)));
        }
    }

    public function cancelStandByAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $demande = $em->getRepository('SuiviEtudesBundle:Demande')->find($id);
        // find last status in modification table
        $modifs = $em->getRepository('SuiviEtudesBundle:Modification')->findOneBy(array('demande' => $demande, 'type' => 'statut'), array('date' => 'desc'));
        $lastStatue = null;
        // create new row in modification table
        if ($modifs) {
            $lastStatue = $modifs->getNouvelleValeur();
            $newStatus = $demande->getStatut()->getName();
            foreach ($demande->getStandBy() as $row) {
                if ($row->getValidated() == 0) {
                    $modifs = new \Suivi\EtudesBundle\Entity\Modification();
                    $modifs->setDate(new \DateTime('now'));
                    $modifs->setUser($this->getUser());
                    $modifs->setDemande($demande);
                    $modifs->setType('statut');
                    $modifs->setAncienneValeur($lastStatue);
                    $modifs->setNouvelleValeur($newStatus);
                    $em->persist($modifs);
                    // remove stand by row
                    $em->remove($row);
                }
            }
            $em->persist($demande);
            $em->flush();
        }
        return new Response("Fini");
    }

    public function validateStandByAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $demande = $em->getRepository('SuiviEtudesBundle:Demande')->find($id);
        $stand_by = $em->getRepository('SuiviEtudesBundle:Statut')->find(Statut::STAND_BY);
        $reason = $em->getRepository('SuiviEtudesBundle:DemandeStandBy')->findOneBy(array('demande' => $demande, 'validated' => 0));
        // check if demandeStatus is not standBy => on multiple request run only once code bellow
        if ($demande && $demande->getStatut()->getId() != $stand_by->getId() && $this->getUser()->hasRole('ROLE_SET_VALIDATE_STAND_BY')) {
            // disable event listener for inserting new row in modification table
            $eventManager = $em->getEventManager();
            $eventManager->removeEventListener(
                    array('prePersist', 'preUpdate', 'postPersist', 'postUpdate'), $this->container->get('suivi_etudes.doctrine.motification')
            );
            // end disable event manager

            $old_statut = $demande->getStatut()->getId();
            $modifs = new \Suivi\EtudesBundle\Entity\Modification();
            $modifs->setDate(new \DateTime('now'));
            $modifs->setUser($this->getUser());
            $modifs->setDemande($demande);
            $modifs->setType('statut');
            $modifs->setAncienneValeur($stand_by);
            $modifs->setNouvelleValeur($stand_by);
            $demande->setStatut($stand_by);
            $demande->setDernierStatut($old_statut);
            $em->persist($modifs);

            $standBy = $em->getRepository('SuiviEtudesBundle:DemandeStandBy')->findBy(array('demande' => $demande));
            foreach ($standBy as $row) {
                $row->setValidated('1');
                $em->persist($row);
            }
            $em->flush();
            // send mail
            $sujet = "Votre demande n°" . $demande->getId() . " vient d'être modifiée.";
            if ($this->container->get('kernel')->getEnvironment() != 'prod') {
                $sujet = '[TEST] ' . $sujet;
            }
            foreach ($demande->getDemandeUser() as $user) {
                $users[$user->getUser()->getId()] = $user->getUser()->getEmail();
            }
            $role = "ROLE_SET_MANAGER_TECH";
            $query = $em->createQuery('SELECT u FROM UserUserBundle:User u WHERE u.roles LIKE :role')
                    ->setParameter('role', '%"' . $role . '"%');
            $listeManagersTech = $query->getResult();

            //récupération des adresses mails des managers tech.
            foreach ($listeManagersTech as $man_tech) {
                $manager_tech[] = $man_tech->getEmail();
            }
            $message = \Swift_Message::newInstance()
                    ->setSubject($sujet)
                    ->setFrom('suivietudes@noz.fr', 'Suivi des Etudes')
                    ->setTo($users)
                    ->setCc($manager_tech)
                    ->setContentType('text/html')
                    ->setBody($this->renderView('SuiviEtudesBundle:Mail:valide_standBy.html.twig', array(
                        'demande' => $demande,
                        'reason' => $reason,
                        'url' => $request->getScheme() . '://' . $request->getHttpHost(),
                        'environnement' => $this->container->get('kernel')->getEnvironment(),
                    )))
            ;
            $this->get('mailer')->send($message);
            return new Response("ok");
        }
        return new Response("Not allowed.");
    }

    public function removeStandByAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $demande = $em->getRepository('SuiviEtudesBundle:Demande')->find($id);
        if ($demande) {
            // disable event listener for inserting new row in modification table
            $eventManager = $em->getEventManager();
            $eventManager->removeEventListener(
                    array('prePersist', 'preUpdate', 'postPersist', 'postUpdate'), $this->container->get('suivi_etudes.doctrine.motification')
            );
            // end disable event manager
            $oldStatus = $demande->getDernierStatut();
            $status = $em->getRepository('SuiviEtudesBundle:Statut')->find($oldStatus);
            $demande->setStatut($status);
            $demande->setDernierStatut(null);
            $modifs = new \Suivi\EtudesBundle\Entity\Modification();
            $modifs->setDate(new \DateTime('now'));
            $modifs->setUser($this->getUser());
            $modifs->setDemande($demande);
            $modifs->setType('statut');
            $modifs->setAncienneValeur("Stand by");
            $modifs->setNouvelleValeur($status);
            $em->persist($modifs);
            $em->flush();
            return new Response("ok");
        }
        return new Response("");
    }

    /**
     * @author Julien Bureau <jbureau@noz.fr>
     * @param unknown $id
     */
    public function AffectationAction($id, Request $request) {
        //init des entity manager
        $em = $this->getDoctrine()->getManager();
        //récupération de la demande
        $demande = $em->getRepository('SuiviEtudesBundle:Demande')->find($id);
        $old_statut = $demande->getStatut();

        //Création du formulaire
        $form = $this->createForm(new DemandeAffectationType($demande, $em), $demande);

        //array qui contiendra les adresses mails des nouvelles personnes affectées à la demande.
        $listeMailAffectation = array();

        $form->bind($request);
        if ($form->isValid()) {
            //Techniciens
            // on récupere la liste des techniciens déja enregistrés
            $listeTechPAncienne = $em->getRepository('SuiviEtudesBundle:DemandeUser')->getTechniciensbyDemande($demande);
            // et les techniciens sélectionnés par l'utilisateurs
            $listeTechNouvelle = $form->get('techniciens')->getData();
            foreach ($listeTechNouvelle as $nouveauTech) {
                if ($listeTechPAncienne->contains($nouveauTech)) {
                    //si le nouveau technicien est déja enregistré dans la base, on le retire de la liste des techniciens déja
                    // enregistrés (ainsi, il ne restera dans listeTechPAncienne QUE les techniciens à retirer.
                    $listeTechPAncienne->removeElement($nouveauTech);
                } else {
                    // sinon on l'ajoute dans la base
                    $liaisonUser = new DemandeUser();
                    $liaisonUser->setDemande($demande);
                    $liaisonUser->setUser($nouveauTech);
                    $liaisonUser->setType(DemandeUser::TECHNICIEN);
                    $em->persist($liaisonUser);

                    $listeMailAffectation[] = $nouveauTech->getEmail();
                }
            }
            /* START se_projet_charge table */
            $chargeUsers = $em->createQueryBuilder('query')
                            ->select('ch.userId,SUM(ch.hours) alocatedHours, SUM(ch.hoursWorked) hoursWorked')
                            ->from('SuiviEtudesBundle:ProjetCharge', 'ch')
                            ->where('ch.demandeId = :id')
                            ->setParameter('id', $id)
                            ->groupBy('ch.userId')
                            ->getQuery()->getResult();
            // get form techs and chefsdeProjet
            $formTechs = isset($request->query->get('suivi_etudes_demande_affectation')['techniciens']) ? $request->query->get('suivi_etudes_demande_affectation')['techniciens'] : array();
            $formCP = isset($request->query->get('suivi_etudes_demande_affectation')['chefdeProjet']) ? $request->query->get('suivi_etudes_demande_affectation')['chefdeProjet'] : array();
            // create new array with unique user ids
            $formUsers = array_unique(array_merge($formTechs, $formCP));
            foreach ($chargeUsers as $user) {
                // set update condition
                if (!in_array($user['userId'], $formUsers) && $user['hoursWorked'] <= 0) {
                    $updateUser = $em->getRepository('SuiviEtudesBundle:ProjetCharge')->findBy(array('demandeId' => $id, 'userId' => $user['userId']));
                    if (null != $updateUser) {
                        foreach ($updateUser as $row) {
                            $em->remove($row);
                            try {
                                $em->flush();
                            } catch (\Doctrine\DBAL\DBALException $e) {
                                return new Response('Erreur.');
                            }
                        }
                    }
                }
            }
            /* END se_projet_charge table */


            // on retire ensuite tout les anciens observateurs qui n'ont pas été sélectionné à nouveau par l'utilisateur
            foreach ($listeTechPAncienne as $user) {
                //"Réinitialisation" de l'entité DemandeUser
                $listeTechPAncienne = $em->getRepository('SuiviEtudesBundle:DemandeUser')->deleteTechniciens($demande, $user);
            }

            //Chef de Projet
            $listeCDPAncienne = $em->getRepository('SuiviEtudesBundle:DemandeUser')->getChefDeProjetbyDemande($demande);
            // et les chefs de projets sélectionnés par l'utilisateurs
            $listCDPNouvelle = $form->get('chefdeProjet')->getData();
            foreach ($listCDPNouvelle as $nouveauCDP) {
                if ($listeCDPAncienne->contains($nouveauCDP)) {
                    //si le nouveau chef de projet est déja enregistré dans la base, on le retire de la liste des chef de projet déja
                    // enregistrés (ainsi, il ne restera dans listeCDPAncienne QUE les chefs de projet à retirer.
                    $listeCDPAncienne->removeElement($nouveauCDP);
                } else {
                    // sinon on l'zjoute dans la base
                    $liaisonUser = new DemandeUser();
                    $liaisonUser->setDemande($demande);
                    $liaisonUser->setUser($nouveauCDP);
                    $liaisonUser->setType(DemandeUser::CHEF_DE_PROJET);
                    $em->persist($liaisonUser);

                    $listeMailAffectation[] = $nouveauCDP->getEmail();
                }
            }
            // on retire ensuite tout les anciens chefs de projet qui n'ont pas été sélectionnés à nouveau par l'utilisateur
            foreach ($listeCDPAncienne as $user) {
                //"Réinitialisation" de l'entité DemandeUser
                $listeCDPAncienne = $em->getRepository('SuiviEtudesBundle:DemandeUser')->deleteChefDeProjet($demande, $user);
            }
            $em->flush();
            //message de confirm
            $this->get('session')->getFlashBag()->add('notice', 'Les affectations ont été prises en compte !');

            if (!empty($formTechs) || !empty($formCP)) {
                $this->envoyerMail($old_statut, $demande, null, $listeMailAffectation);
            }
        } else {
            $this->get('session')->getFlashBag()->add('erreur', '<strong>Erreur !</strong> Veuillez recommencer, ou contacter le pôle web');
            foreach ($form->getErrors() as $key => $error) {
                $this->get('session')->getFlashBag()->add('erreur', $error->getMessage());
            }
            foreach ($form->all() as $child) {
                if (!$child->isValid()) {
                    foreach ($child->getErrors() as $key => $error) {
                        $this->get('session')->getFlashBag()->add('erreur', $error->getMessage());
                    }
                }
            }
        }

        //retour vers la vue
        return $this->redirect($this->generateUrl('suivi_etudes_voir', array('id' => $demande->getId())));
    }

    /**
     * Passage à "En cours CP"
     * @author : allard_pi
     * @param int $id id d'une demande
     * @return redirect : redirection vers la vue de la demande
     */
    public function EnCoursCPAction($id) {
        //init des entity manager
        $em = $this->getDoctrine()->getManager();
        //récupération de la demande
        $demande = $em->getRepository('SuiviEtudesBundle:Demande')->find($id);

        //récupération du statut "En cours dev"
        $statut = $em->getRepository('SuiviEtudesBundle:Statut')->find(Statut::EN_COURS_CP);
        $old_statut = $demande->getStatut();
        //$listeTechCP = $demande->getDemandeUser();

        if ($demande->getStatut()->getId() == Statut::RECETTE_USER) {
            //# ENVOI MAIL #\\
            //Envoi d'un mail au CP + au manager tech
            //Réinit du booléen "Recette" pour redemander confirmation du demandeur
            $demande->setRecette(null);

            //passage au statut "En cours - CP"
            $demande->setStatut($statut);
        }
        if ($demande->getStatut()->getId() == Statut::EN_COURS_DEV) {
            $demande->setStatut($statut);
        }
        //flush doctrine
        $em->flush();
        //message de confirm
        $this->get('session')->getFlashBag()->add('notice', 'Votre statut a bien été modifié !');

        $this->envoyerMail($old_statut, $demande);

        //retour vers la vue
        return $this->redirect($this->generateUrl('suivi_etudes_voir', array('id' => $demande->getId())));
    }

    public function EnCoursDevAction($id, Request $request) {
        //init des entity manager
        $em = $this->getDoctrine()->getManager();
        //récupération de la demande
        $demande = $em->getRepository('SuiviEtudesBundle:Demande')->find($id);
        $old_statut = $demande->getStatut();
        if ($old_statut->getId() == Statut::EN_COURS_CP) {
            $form = $this->createForm(new DemandeSpecType, $demande);
            $form->handleRequest($request);
            if ($form->isValid()) {
                if ($form->get('choix')->getData() == 'upload' && $form->get('fichier')->getData()) {
                    $spec = new PieceJointe();
                    $spec->setFile($form->get('fichier')->getData());
                    $spec->setDemande($demande);
                    $em->persist($spec);
                    $demande->setSpec('uploads/attachments/' . $demande->getId() . '/' . $spec->getName());
                } else {
                    $demande->setSpec($form->get('chemin')->getData());
                }
            }
        }
        if (isset($request)) {
            $data = $request->get('description');
            if ($data === null) {
                $data = $demande->getDescriptionRefus();
            }
            if ($data === '') {
                $this->get('session')->getFlashBag()->add('erreur', 'Veuillez renseigner la description du refus');
                return $this->redirect($this->generateUrl('suivi_etudes_voir', array('id' => $demande->getId())));
            }
        } else {
            $data = $demande->getDescriptionRefus();
        }

        //message de confirm
        $this->get('session')->getFlashBag()->add('success', 'Votre statut a bien été modifié !');


        //récupération du statut "En cours dev"

        $statut = $em->getRepository('SuiviEtudesBundle:Statut')->find(Statut::EN_COURS_DEV);

        //passage au statut "En cours dev"
        $demande->setStatut($statut);
        //$demande->setChargesCPRestante(0);
        //flush doctrine
        $em->flush();
        $this->envoyerMail($old_statut, $demande, $data);

        //retour vers la vue
        return $this->redirect($this->generateUrl('suivi_etudes_voir', array('id' => $demande->getId())));
    }

    public function RecetteInfoAction($id) {
        //init des entity manager
        $em = $this->getDoctrine()->getManager();
        //récupération de la demande
        $demande = $em->getRepository('SuiviEtudesBundle:Demande')->find($id);

        $old_statut = $demande->getStatut();
        //récupération du statut "Recette Info"
        $statut = $em->getRepository('SuiviEtudesBundle:Statut')->find(Statut::RECETTE_INFO);

        //passage au statut "Recette Info"
        $demande->setStatut($statut);

        //flush doctrine
        $em->flush();

        //message de confirm
        $this->get('session')->getFlashBag()->add('success', 'Votre statut a bien été modifié !');

        //Envoi du ou des mails
        $this->envoyerMail($old_statut, $demande);
        //retour vers la vue
        return $this->redirect($this->generateUrl('suivi_etudes_voir', array('id' => $demande->getId())));
    }

    /**
     * Validation des recettes utilisateur
     * @author : guemene_va
     * @param int $id id d'une demande
     * @return redirect : redirection vers la vue de la demande
     */
    public function recetteUserOkAction($id, Request $request) {

        $em = $this->getDoctrine()->getManager();
        $demande = $em->getRepository('SuiviEtudesBundle:Demande')->find($id);

        //Création du formulaire
        $form = $this->createForm(new DemandeRecetteUserOkType, $demande);


        //$form->handleRequest($request);
        $form->bind($request);

        if ($form->get('commentaire')->getData() and $form->get('date_production')->getData()) {
            //La recette a été validée !

          $demande->setRecette(true);
            $this->get('session')->getFlashBag()->add('notice', 'Votre date a été proposée !');
            $dateProd = $demande->getDateProduction();
            $demande->setDateProduction($dateProd);
            //Récupération du commentaire
            if ($form->get('cahier')->getData()) {
                $cahier = new PieceJointe();
                $cahier->setFile($form->get('cahier')->getData());
                $cahier->setDemande($demande);
                $em->persist($cahier);
                $em->flush();
                $demande->setCahierRecette($cahier);
            }
            $commentaire = $form->get('commentaire')->getData();
            $demande->setCommentaireRecette($commentaire);
            $this->envoyerMail(1, $demande, $commentaire);
            $this->get('session')->getFlashBag()->add('info', 'Votre retour a été pris en compte !');
           // flush doctrine
            $em->flush();


            $client=  $this->get('security.context')->getToken()->getUser();
            if($form->get('input1')->getData() || $form->get('input2')->getData() || $form->get('input3')->getData()) {
                $note = new Note();
                //Recuperation id_demande
                $note->setIdDemande($demande);
                //Récuperation de client
                $note->setIdClient($client);
                //Récupération de la délais
                $delay = $form->get('input1')->getData();
                $note->setNoteDelay($delay);
                //Récupération de qualité
                $quality = $form->get('input2')->getData();
                $note->setNoteQuality($quality);
                //Récuperation de production
                $production = $form->get('input3')->getData();
                $note->setNoteProduction($production);
                //date
                $note->setDateCreation(new \DateTime('NOW'));
                $em->persist($note);
                $em->flush();
            }

        } else {
            $this->get('session')->getFlashBag()->add('erreur', 'Erreur ! Vous devez renseigner la date de mise en production et la description de la recette !');
        }

        //retour vers la vue
        return $this->redirect($this->generateUrl('suivi_etudes_voir', array('id' => $demande->getId())));
    }

    public function recetteUserNoOkAction($id, Request $request) {

        $em = $this->getDoctrine()->getManager();
        $demande = $em->getRepository('SuiviEtudesBundle:Demande')->find($id);
        $form = $this->createForm(new DemandeRecetteUserNotOkType, $demande);
        $form->bind($request);
        if (!($form->get('description')->getData())) {
            $this->get('session')->getFlashBag()->add('erreur', 'Vous devez renseigner la description de votre refus');
            return $this->redirect($this->generateUrl('suivi_etudes_voir', array('id' => $demande->getId())));
        }

        //récupération de la description du refus
        $descriptionTests = $form->get('description')->getData();
        if ($form->get('cahier')->getData()) {
            $cahier = new PieceJointe();
            $cahier->setFile($form->get('cahier')->getData());
            $cahier->setDemande($demande);
            $em->persist($cahier);
            $em->flush();
            $demande->setCahierRecette($cahier);
        }

        //On passe le booléen Test de la demande à FALSE (qui permet le retour aux statuts précédent)
        $demande->setRecette(false);
        $demande->setCommentaireRecette($descriptionTests);

        //flush doctrine
        $em->flush();

        //# ENVOI MAIL MANAGERTECH, CP, HUGUES et TECH POUR DIRE QUE C'EST PAS BON #\\
        //message de confirm
        $this->get('session')->getFlashBag()->add('info', 'Votre retour a été pris en compte !');

        //Envoi du ou des mails
        $this->envoyerMail(2, $demande, $descriptionTests);

        $client=  $this->get('security.context')->getToken()->getUser();
if($form->get('input1')->getData() || $form->get('input2')->getData() || $form->get('input3')->getData()) {
    $note = new Note();
    //Recuperation id_demande
    $note->setIdDemande($demande);
    //Récuperation de client
    $note->setIdClient($client);
    //Récupération de la délais
    $delay = $form->get('input1')->getData();
    $note->setNoteDelay($delay);
    //Récupération de qualité
    $quality = $form->get('input2')->getData();
    $note->setNoteQuality($quality);
    //Récuperation de production
    $production = $form->get('input3')->getData();
    $note->setNoteProduction($production);
    //date
    $note->setDateCreation(new \DateTime('NOW'));
    $em->persist($note);
    $em->flush();
}
        //retour vers la vue
        return $this->redirect($this->generateUrl('suivi_etudes_voir', array('id' => $demande->getId())));
    }

    public function PassageRecetteUserAction($id) {
        //init des entity manager
        $em = $this->getDoctrine()->getManager();
        //récupération de la demande
        $demande = $em->getRepository('SuiviEtudesBundle:Demande')->find($id);

        $demande->setRecette(null);

        if ($demande->getStatut()->getId() == Statut::RECETTE_INFO) {
            //récupération du statut "Etudes"
            $statut = $em->getRepository('SuiviEtudesBundle:Statut')->find(Statut::RECETTE_USER);

            //passage au statut "Recette User"
            $demande->setStatut($statut);
            //R.A.Z des charges CP et Dev restantes
            if ($demande->getChargesCPRestante() != 0) {
                $demande->setChargesCPRestante(0);
            }
            if ($demande->getChargesDevRestante() != 0) {
                $demande->setChargesDevRestante(0);
            }
            //flush doctrine
            $em->flush();
            //message de confirm
            $this->get('session')->getFlashBag()->add('success', 'Votre statut a bien été modifié !');
            //Envoi du ou des mails
            $this->envoyerMail(0, $demande);
        } else {
            $this->get('session')->getFlashBag()->add('info', 'Cette demande doit passer par la recette "informatique".');
        }

        //# ENVOI MAIL #\\
        //Envoi un mail au demandeur + observateur : "c'est à vous de tester maintenant"
        //retour vers la vue
        return $this->redirect($this->generateUrl('suivi_etudes_voir', array('id' => $demande->getId())));
    }

    /**
     * @author Mikael LOLIVE <mlolive@noz.fr>
     */
    public function ChangerDateProductionAction($id, Request $request) {
        //init des entity manager
        $em = $this->getDoctrine()->getManager();
        //récupération de la demande
        $demande = $em->getRepository('SuiviEtudesBundle:Demande')->find($id);

        //Passage de "modification_date_prod" à true
        $demande->setModifProd(true);

        $form = $this->createForm(new DemandeChangerProdType, $demande);
        // $form->bind($request);
        $form->handleRequest($request);

        //Récupération de la nouvelle dateProd et de la description du refus
        $description = $request->request->get('suivi_etudes_modification_date_prod')['description'];
        //var_dump($description);die();
        //Si les champs sont valides et non-nulls
        if ($form->isValid()) {
            if (isset($description)) {
                //message de confirm
                $this->get('session')->getFlashBag()->add('info', 'Votre retour a été pris en compte !');
                //Envoi du ou des mail(s)
                $this->envoyerMail(3, $demande, $description); //?
                $demande->setCommentaireProd($description);
                //Flushing des infos
                $em->flush();
            } else {
                $this->get('session')->getFlashBag()->add('erreur', 'Erreur ! Veuillez recommencer, ou contacter le pôle web');
            }
        }

        return $this->redirect($this->generateUrl('suivi_etudes_voir', array('id' => $demande->getId())));
    }

    /**
     * @author Julien Bureau <jbureau@noz.fr>
     * (non-PHPdoc)
     * @see \Symfony\Bundle\FrameworkBundle\Controller\Controller::createAccessDeniedException()
     */
    public function MiseEnProdAction($id) {

        //init des entity manager
        $em = $this->getDoctrine()->getManager();
        //récupération de la demande
        $demande = $em->getRepository('SuiviEtudesBundle:Demande')->find($id);

        if ($demande->getStatut()->getId() == Statut::RECETTE_USER) {
            # prevent going on prod if no technical documentation
            $technicalDoc = $em->getRepository('SuiviEtudesBundle:DemandeDocs')->findBy(array('demande' => $demande));
            if (empty($technicalDoc)) {
                $this->get('session')->getFlashBag()->add('erreur', "Aucune documentation technique trouvée. <br> S'il vous plaît ajouter un à être en mesure de mettre la demande en production.");
                return $this->redirect($this->generateUrl('suivi_etudes_voir', array('id' => $demande->getId())));
            }
            # END
            //Si les tests ont été validés
            if ($demande->getRecette() == TRUE) {
                //On passe la demande à "Mise en production"
                $old_statut = $demande->getStatut();
                $demande->setStatut($em->getRepository('SuiviEtudesBundle:Statut')->find(Statut::MISE_EN_PROD));

                $this->get('session')->getFlashBag()->add('success', 'La demande vient de passé au statut Mise en production');
                //$demande->setDateProduction($dateProd);
                //# ENVOI MAIL A L'USER POUR RETOUR DATE DE PRODUCTION #\\
            //
            }
            //sinon
            else {
                $this->get('session')->getFlashBag()->add('erreur', "Erreur ! Les tests n'ont pas été validés par le demandeur");
            }



            //flush doctrine
            $em->flush();

            //Envoi du ou des mails
            $this->envoyerMail($old_statut, $demande);
        } else {
            $this->get('session')->getFlashBag()->add('erreur', 'Erreur ! La demande doit être passée par "Recette Utilisateur" pour être mise en production.');
        }
        //retour vers la vue
        return $this->redirect($this->generateUrl('suivi_etudes_voir', array('id' => $demande->getId())));
    }

    /**
     * @author allard_pi
     */
    public function CloreAction($id) {
        //init des entity manager
        $em = $this->getDoctrine()->getManager();
        //récupération de la demande
        $demande = $em->getRepository('SuiviEtudesBundle:Demande')->find($id);

        if ($demande->getStatut()->getId() == Statut::MISE_EN_PROD) {
            //passage au statut "Clos"
            $demande->setStatut($em->getRepository('SuiviEtudesBundle:Statut')->find(Statut::CLOS));
            //flush doctrine
            $em->flush();
            //message de confirm
            $this->get('session')->getFlashBag()->add('success', 'Cette demande vient de passer au statut Clos !');

            //Envoi du ou des mails
            $this->envoyerMail(null, $demande);
        } else {
            $this->get('session')->getFlashBag()->add('info', 'Impossible de clore la demande.');
        }

        //retour vers la vue
        return $this->redirect($this->generateUrl('suivi_etudes_voir', array('id' => $demande->getId())));
    }

    /**
     * @author Valérian GUEMENE <vguemene@noz.fr>
     */
    public function modifierDateLivraisonAction($id, Request $request) {
        //init des entity manager
        $em = $this->getDoctrine()->getManager();
        //récupération de la demande
        $demande = $em->getRepository('SuiviEtudesBundle:Demande')->find($id);

        $url = $request->getScheme() . '://' . $request->getHttpHost();

        $form = $this->createForm(new DemandeModifierDateType, $demande);
        // $form->bind($request);
        $form->handleRequest($request);
        //Récupération de l'email des destinataires
        /* $demandeUser = new DemandeUser();
          $demandeur = $this->getUser($demande)->getEmail();
          $observateur = $demande->getDemandeUser();
          foreach($observateur as $obs){
          $users = $obs->getUser()->getEmail();
          $users;
          } */
        foreach ($demande->getDemandeUser() as $user) {
            $users[] = $user->getUser()->getEmail();
        }
        $role = "ROLE_SET_MANAGER_TECH";
        $query = $em->createQuery('SELECT u FROM UserUserBundle:User u WHERE u.roles LIKE :role')
                ->setParameter('role', '%"' . $role . '"%');
        $listeManagersTech = $query->getResult();

        //récupération des adresses mails des managers tech.
        foreach ($listeManagersTech as $man_tech) {
            $manager_tech[] = $man_tech->getEmail();
        }
        //Si les champs sont valides et non-nulls
        if ($form->isValid()) {
            // get optional comment that is not bounded by demande entity. Used only in email
            $comment = trim($request->request->get('suivi_etudes_modifier_date_comment'));
            //message de confirm
            $this->get('session')->getFlashBag()->add('info', 'les modifications ont bien été prises en compte !');
            //Envoi du ou des mails
            if ($this->container->get('kernel')->getEnvironment() != 'prod') {
                $sujet = '[TEST] ';
            }
            $sujet .= 'Demande n°' . $demande->getId() . ' : Modification des dates de livraison';
            $message = \Swift_Message::newInstance()
                    ->setSubject($sujet)
                    ->setFrom('suivietudes@noz.fr', 'Suivi des Etudes')
                    ->setTo($users)
                    ->setCc($manager_tech)
                    ->setBody($this->renderView('SuiviEtudesBundle:Mail:modification_date_livraison.html.twig', array(
                                'demande' => $demande,
                                'dateRecette' => $demande->getDateRecette(),
                                'comment' => $comment,
                                'dateProduction' => $demande->getDateProduction(),
                                'environnement' => $this->container->get('kernel')->getEnvironment(),
                                'url' => $url,
                    )))
                    ->setContentType('text/html');
            $this->get('mailer')->send($message);
            // delete from charge table 
            $projectCharge = $em->getRepository('SuiviEtudesBundle:ProjetCharge')->findChargesByIdAndPeriod($id, $request->request->get('suivi_etudes_modifier_date')['date_production']);
            foreach ($projectCharge as $row) {
                $em->remove($row);
            }
            //Flushing des infos
            $em->flush();
        } else {
            $this->get('session')->getFlashBag()->add('erreur', 'Erreur ! Veuillez recommencer, ou contacter le pôle web');
        }
        return $this->redirect($this->generateUrl('suivi_etudes_voir', array('id' => $demande->getId())));
    }

    /**
     * @author allard_pi
     * @author Julien BUREAU <julien.bureau02@gmail.com>
     * @param \Suivi\EtudesBundle\Controller\statut $demande
     */
    public function envoyerMail($old_statut, $demande, $data = NULL, $listeMail = null) {

        //Entité Manager
        $em = $this->getDoctrine()->getManager();

        //Récupération du demandeur
        $demandeUser = new DemandeUser();
        $userDmd = $this->getUser($demande)->getDisplayname();
        //Récupération de l'e-mail du demandeur
        $emailDmd = $this->getUser($demande)->getEmail();

        //$role = "ROLE_SET_TECH";
        $role = "ROLE_SET_MANAGER_TECH";
        $query = $em->createQuery('SELECT u FROM UserUserBundle:User u WHERE u.roles LIKE :role')
                ->setParameter('role', '%"' . $role . '"%');
        $listeManagersTech = $query->getResult();
        //récupération des adresses mails des managers tech.
        foreach ($listeManagersTech as $man_tech) {
            $manager_tech[] = $man_tech->getEmail();
        }

        // Récupération des managers tech
        // $manager_tech = $em->getRepository('UserUserBundle:User')->findByRoles('ROLE_SET_SUPER_ADMIN');
        if ($this->container->get('kernel')->getEnvironment() != 'prod') {
            $sujet = '[TEST] ';
        } else {
            $sujet = '';
        }

        //Construction du premier message
        $message_user = \Swift_Message::newInstance()->setContentType('text/html')
                ->setFrom('suivietudes@noz.fr', 'Suivi des Etudes')
                ->setReplyTo('poleweb@noz.fr')
                ->setBcc('poleweb@noz.fr')

        ;
        $envoi_user = FALSE;

        //Construction du deuxième message
        $message_info = \Swift_Message::newInstance()->setContentType('text/html')
                ->setFrom('suivietudes@noz.fr', 'Suivi des Etudes')
                ->setReplyTo('poleweb@noz.fr')
                ->setBcc('poleweb@noz.fr')
        ;
        $envoi_info = FALSE;

        //Construction du message au(x) responsbles techniques
        $message_resp_tech = \Swift_Message::newInstance()->setContentType('text/html')
                ->setFrom('suivietudes@noz.fr', 'Suivi des Etudes')
                ->setReplyTo('poleweb@noz.fr')
                ->setBcc('poleweb@noz.fr')
        ;
        $envoi_resp_tech = FALSE;

        //Construction du message d'affectation
        $message_affectation = \Swift_Message::newInstance()->setContentType('text/html')
                ->setFrom('suivietudes@noz.fr', 'Suivi des Etudes')
                ->setReplyTo('poleweb@noz.fr')
                ->setBcc('poleweb@noz.fr')
        ;
        $envoi_affectation = FALSE;

        //A CHANGER POUR TEST/DEV/PROD ==> parameters.yml
        $request = $this->getRequest();
        $url = $request->getScheme() . '://' . $request->getHttpHost();

        //Peu importe le statut, on récupère le demandeur, les observateurs, et les personnes affectées.
        $observateur = array();
        $technicien = array();
        $chef_de_projet = array();
        foreach ($demande->getDemandeUser() as $user) { // recuperation de la liste des utilisateurs
            if ($user->getType() == DemandeUser::DEMANDEUR) {
                $demandeur = $user;
                $mailDemandeur = $user->getUser()->getEmail();
            } elseif ($user->getType() == DemandeUser::OBSERVATEUR) {
                $observateur[] = $user->getUser()->getEmail();
            } elseif ($user->getType() == DemandeUser::TECHNICIEN) {
                $technicien[] = $user->getUser()->getEmail();
            } elseif ($user->getType() == DemandeUser::CHEF_DE_PROJET) {
                $chef_de_projet[] = $user->getUser()->getEmail();
            }
        }
        //Traitement des différents cas, pour l'envoi de mail
//         var_dump($demande->getRecette());
        switch ($demande->getStatut()->getId()) {
            case Statut::NOUVEAU :
                //Mail déjà présent dans SuiviEtudesBundle:ajouterAction
                break;

            case Statut::PRIS_EN_COMPTE :
                if ($listeMail != null) {
                    //Envoi d'un mail au(x) technicien(s) pour l'informer de son affectation
                    $message_affectation
                            ->setSubject($sujet . "La demande n°" . $demande->getId() . " : vous a été affectée.")
                            ->setTo($listeMail)
                            ->setBody($this->renderView('SuiviEtudesBundle:Mail:encours_cp.html.twig', array(
                                        'demande' => $demande,
                                        'environnement' => $this->container->get('kernel')->getEnvironment(),
                                        'url' => $url
                            )))
                    ;
                    $envoi_affectation = TRUE;
                } else {
                    //Envoi d'un mail au demandeur pour confirmer la prise en compte de la demande
                    $message_user
                            ->setSubject($sujet . "Votre demande n°" . $demande->getId() . " vient d'être prise en compte.")
                            ->setTo($mailDemandeur)
                            ->setCc($observateur)
                            ->setBody($this->renderView('SuiviEtudesBundle:Mail:etudes.html.twig', array(
                                        'demande' => $demande,
                                        'url' => $url,
                                        'environnement' => $this->container->get('kernel')->getEnvironment(),
                            )))
                    ;
                    $envoi_user = TRUE;
                }
                break;


            case Statut::ETUDES :
                if ($listeMail != null) {
                    //Envoi d'un mail au(x) technicien(s) pour l'informer de son affectation
                    $message_affectation
                            ->setSubject($sujet . "La demande n°" . $demande->getId() . " : vous a été affectée.")
                            ->setTo($listeMail)
                            ->setBody($this->renderView('SuiviEtudesBundle:Mail:encours_cp.html.twig', array(
                                        'demande' => $demande,
                                        'environnement' => $this->container->get('kernel')->getEnvironment(),
                                        'url' => $url
                            )))
                    ;
                    $envoi_affectation = TRUE;
                }
                break;

            case Statut::EN_ATTENTE :
                switch ($old_statut->getId()) {
                    // Cas classique : le manager tech renseigne les estimations
                    case Statut::ETUDES :
                        //Envoi d'un mail au demandeur pour l'information que les estimations ont été ajoutées à la demande

                        $message_user
                                ->setSubject($sujet . "Votre demande n°" . $demande->getId() . " vient d'être modifiée.")
                                ->setTo($mailDemandeur)
                                ->setBody($this->renderView('SuiviEtudesBundle:Mail:en_attente.html.twig', array(
                                            'demande' => $demande,
                                            'url' => $url,
                                            'environnement' => $this->container->get('kernel')->getEnvironment(),
                                                //'dateProd'	=> $dateProd
                                )))
                        ;
                        if ($demande->getDevis() != '') {
                            $message_user->attach(
                                    \Swift_Attachment::fromPath('uploads/attachments/' . $demande->getId() . '/' . $demande->getDevis()->getName())->setFilename($demande->getDevis()->getName())
                            );
                        }
                        $envoi_user = in_array($this->getDoctrine()->getRepository('SuiviEtudesBundle:Mail')->find(3), $demandeur->getMail()->getValues());
                        break;

                    // Cas de non validation des estimations, on reste au même statut
                    case Statut::EN_ATTENTE:
                        if ($data != NULL) {
                            //Envoi d'un mail au demandeur pour confirmer sa "NON Validation" des estimations
                            $message_user
                                    ->setSubject($sujet . "Demande n°" . $demande->getId() . " : estimations non validées.")
                                    ->setTo($mailDemandeur)
                                    ->setBody($this->renderView('SuiviEtudesBundle:Mail:non_valide_user.html.twig', array(
                                                'demande' => $demande,
                                                'url' => $url,
                                                'environnement' => $this->container->get('kernel')->getEnvironment(),
                                    )))
                            ;
                            $envoi_user = in_array($this->getDoctrine()->getRepository('SuiviEtudesBundle:Mail')->find(3), $demandeur->getMail()->getValues());

                            //var_dump($data);die();
                            //Envoi d'un mail au Manager Tech pour confirmer la "NON Validation" du demandeur
                            $message_info
                                    ->setSubject($sujet . "Demande n°" . $demande->getId() . " : estimations non validées.")
                                    ->setTo($manager_tech)
                                    //->setCc($observateur)
                                    ->setBody($this->renderView('SuiviEtudesBundle:Mail:non_valide_info.html.twig', array(
                                                'demande' => $demande,
                                                'url' => $url,
                                                'environnement' => $this->container->get('kernel')->getEnvironment(),
                                                'userDmd' => $userDmd,
                                                'emailDmd' => $emailDmd,
                                                'motifRefus' => $data[0],
                                                'descriptionRefus' => $data[1]
                                    )))
                            ;
                            $envoi_info = TRUE;
                        } else if (null == $listeMail) {
                            $message_user
                                    ->setSubject($sujet . "Votre demande n°" . $demande->getId() . " vient d'être modifiée.")
                                    ->setTo($mailDemandeur)
                                    ->setBody($this->renderView('SuiviEtudesBundle:Mail:en_attente.html.twig', array(
                                                'demande' => $demande,
                                                'url' => $url,
                                                'environnement' => $this->container->get('kernel')->getEnvironment(),
                                                    //'dateProd'	=> $dateProd
                            )));
                            if ($demande->getDevis() != '') {
                                $message_user->attach(
                                        \Swift_Attachment::fromPath('uploads/attachments/' . $demande->getId() . '/' . $demande->getDevis()->getName())->setFilename($demande->getDevis()->getName())
                                );
                            }
                            $envoi_user = in_array($this->getDoctrine()->getRepository('SuiviEtudesBundle:Mail')->find(3), $demandeur->getMail()->getValues());
                        } else if ($listeMail != null) {
                            //Envoi d'un mail au(x) technicien(s) pour l'informer de son affectation
                            $message_affectation
                                    ->setSubject($sujet . "La demande n°" . $demande->getId() . " : vous a été affectée.")
                                    ->setTo($listeMail)
                                    ->setBody($this->renderView('SuiviEtudesBundle:Mail:encours_cp.html.twig', array(
                                                'demande' => $demande,
                                                'environnement' => $this->container->get('kernel')->getEnvironment(),
                                                'url' => $url
                                    )))
                            ;
                            $envoi_affectation = TRUE;
                        }
                        break;
                }//fin switch
                break;

            case Statut::VALIDE :
                if (!empty($listeMail)) {
                    $message_affectation
                            ->setSubject($sujet . "La demande n°" . $demande->getId() . " : vous a été affectée.")
                            ->setTo($listeMail)
                            ->setBody($this->renderView('SuiviEtudesBundle:Mail:encours_cp.html.twig', array(
                                        'demande' => $demande,
                                        'environnement' => $this->container->get('kernel')->getEnvironment(),
                                        'url' => $url
                            )))
                    ;
                    $envoi_affectation = TRUE;
                } else {
                    //Envoi d'un mail au demandeur pour confirmer sa validation
                    $message_user
                            ->setSubject($sujet . "Demande n°" . $demande->getId() . " : Confirmation de votre validation.")
                            ->setTo($mailDemandeur)
                            ->setBody($this->renderView('SuiviEtudesBundle:Mail:valide_user.html.twig', array(
                                        'demande' => $demande,
                                        'environnement' => $this->container->get('kernel')->getEnvironment(),
                                        'url' => $url
                            )))
                    ;
                    $envoi_user = in_array($this->getDoctrine()->getRepository('SuiviEtudesBundle:Mail')->find(4), $demandeur->getMail()->getValues());
                    //Envoi d'un mail aux manager tech pour l'informer de la réponse du demandeur
                    $message_info
                            ->setSubject($sujet . "Demande n°" . $demande->getId() . " : Estimations validées.")
                            ->setTo($manager_tech)
                            //->setCc($observateur)
                            ->setBody($this->renderView('SuiviEtudesBundle:Mail:valide_info.html.twig', array(
                                        'demande' => $demande,
                                        'environnement' => $this->container->get('kernel')->getEnvironment(),
                                        'url' => $url
                            )))
                    ;
                    $envoi_info = TRUE;
                }
                break;

            case Statut::EN_COURS_CP :
                switch ($old_statut->getId()) {
                    //Cas classique : première affectation
                    case Statut::VALIDE :

                        //Envoi d'un mail au(x) technicien(s) pour l'informer de son affectation
                        $message_info
                                ->setSubject($sujet . "La demande n°" . $demande->getId() . " : vous a été affectée.")
                                ->setTo($chef_de_projet)
                                ->setCc($technicien)
                                //->setCc($observateur)
                                ->setBody($this->renderView('SuiviEtudesBundle:Mail:encours_cp.html.twig', array(
                                            'demande' => $demande,
                                            'environnement' => $this->container->get('kernel')->getEnvironment(),
                                            'url' => $url
                                )))
                        ;
                        $envoi_info = TRUE;
                        break;

                    //Cas d'une nouvelle affectation
                    case Statut::EN_COURS_CP :
                        if ($listeMail != null) {
                            //Envoi d'un mail au(x) technicien(s) pour l'informer de son affectation
                            $message_affectation
                                    ->setSubject($sujet . "La demande n°" . $demande->getId() . " : vous a été affectée.")
                                    ->setTo($listeMail)
                                    //->setCc($observateur)
                                    ->setBody($this->renderView('SuiviEtudesBundle:Mail:encours_cp.html.twig', array(
                                                'demande' => $demande,
                                                'environnement' => $this->container->get('kernel')->getEnvironment(),
                                                'url' => $url
                                    )))
                            ;
                            $envoi_affectation = TRUE;
                        }
                        break;

                    //Cas retour après une recette info
                    case Statut::RECETTE_INFO :
                        //MAIL ??
                        break;
                    //Cas retour après recette utilisateur
                    case Statut::RECETTE_USER :

                        //MAIL UTILISATEUR ???
                        //Envoi d'un mail au(x) technicien(s) pour l'informer de sa réaffectation
                        $message_info
                                ->setSubject($sujet . "La demande n°" . $demande->getId() . " : nécessite un retour en arrière.")
                                ->setTo($chef_de_projet)
                                ->setCc($technicien)
                                ->setBody($this->renderView('SuiviEtudesBundle:Mail:retour_encours_cp.html.twig', array(
                                            'demande' => $demande,
                                            'environnement' => $this->container->get('kernel')->getEnvironment(),
                                            'url' => $url
                                )))
                        ;
                        $envoi_info = TRUE;
                }
                break;

            case Statut::EN_COURS_DEV :
                switch ($old_statut->getId()) {
                    //Cas classique : fin de l'étape d'analyse
                    case Statut::EN_COURS_CP :
                        //Envoi d'un mail au responsable technique pour l'informer de son affectation
                        /* $message_info
                          ->setSubject("La demande n°".$demande->getId()." : vous a été affectée.")
                          ->setTo($technicien)
                          //->setCc($observateur)
                          ->setBody($this->renderView('SuiviEtudesBundle:Mail:encours_dev.html.twig', array(
                          'demande' => $demande,
                          'url'     => $url
                          )))
                          ;
                          $envoi_info = TRUE; */

                        foreach ($demande->getApplication() as $app) {
                            if ($app->getName() == "SAP") {
                                $sap_manager_username = $this->container->getParameter('sap_manager_username');
                                $resTechSAP = $em->getRepository('UserUserBundle:User')->findOneByusername($sap_manager_username);
                                $mailResTechSAP = $resTechSAP->getEmail();
                                $message_resp_tech
                                        ->setSubject($sujet . "La demande SAP n°" . $demande->getId() . " vient de passer à l'étape de développement.")
                                        ->setTo($mailResTechSAP)
                                        ->setBody($this->renderView('SuiviEtudesBundle:Mail:encours_dev.html.twig', array(
                                                    'demande' => $demande,
                                                    'environnement' => $this->container->get('kernel')->getEnvironment(),
                                                    'url' => $url
                                        )))
                                ;
                                $envoi_resp_tech = TRUE;
                            }
                        }

                        break;
                    //Nouvelle affectation
                    case Statut::EN_COURS_DEV :
                        if ($listeMail != null) {
                            //Envoi d'un mail au(x) technicien(s) pour l'informer de son affectation
                            $message_affectation
                                    ->setSubject($sujet . "La demande n°" . $demande->getId() . " : vous a été affectée.")
                                    ->setTo($listeMail)
                                    //->setCc($observateur)
                                    ->setBody($this->renderView('SuiviEtudesBundle:Mail:encours_cp.html.twig', array(
                                                'demande' => $demande,
                                                'environnement' => $this->container->get('kernel')->getEnvironment(),
                                                'url' => $url
                                    )))
                            ;
                            $envoi_affectation = TRUE;
                        }
                        break;
                    //Cas retour après une recette informatique non concluante
                    case Statut::RECETTE_INFO :
                        //Envoi d'un mail au(x) technicien(s) pour les informer du retour à l'étape de dév.
                        $message_info
                                ->setSubject($sujet . "La demande n°" . $demande->getId() . " nécessite un retour à l'étape de développement.")
                                ->setTo($technicien)
                                ->setCc($manager_tech)
                                ->setBody($this->renderView('SuiviEtudesBundle:Mail:retour_encours_dev.html.twig', array(
                                            'demande' => $demande,
                                            'url' => $url,
                                            'environnement' => $this->container->get('kernel')->getEnvironment(),
                                            'commentaire' => $data
                                )))
                        ;
                        $envoi_info = TRUE;
                        break;
                    //Cas retour après une recette utilisateur non concluante
                    case Statut::RECETTE_USER :
                        //MAIL AU DEMANDEUR ???
                        //Envoi d'un mail au(x) technicien(s) pour les informer du retour à l'étape de dév.
                        $message_info
                                ->setSubject($sujet . "La demande n°" . $demande->getId() . " nécessite un retour à l'étape de développement.")
                                ->setTo($technicien)
                                ->setCc($chef_de_projet)
                                ->setBody($this->renderView('SuiviEtudesBundle:Mail:retour_encours_dev.html.twig', array(
                                            'demande' => $demande,
                                            'url' => $url,
                                            'environnement' => $this->container->get('kernel')->getEnvironment(),
                                            'commentaire' => $data
                                )))
                        ;
                        $envoi_info = TRUE;
                        break;
                }
                break;

            case Statut::RECETTE_INFO :

                if ($old_statut->getId() == Statut::EN_COURS_DEV) {
                    //Envoi d'un mail au chef de proejt technique pour l'informer du passage en recette
                    $message_info
                            ->setSubject($sujet . "Demande n°" . $demande->getId() . " : fin du développement.")
                            ->setTo($chef_de_projet)
                            //->setCc($observateur)
                            ->setBody($this->renderView('SuiviEtudesBundle:Mail:recette_info.html.twig', array(
                                        'demande' => $demande,
                                        'environnement' => $this->container->get('kernel')->getEnvironment(),
                                        'url' => $url
                            )))
                    ;
                    $envoi_info = TRUE;
                }

                if ($listeMail != null) {
                    //Envoi d'un mail au(x) technicien(s) pour l'informer de son affectation
                    $message_affectation
                            ->setSubject($sujet . "La demande n°" . $demande->getId() . " : vous a été affectée.")
                            ->setTo($listeMail)
                            //->setCc($observateur)
                            ->setBody($this->renderView('SuiviEtudesBundle:Mail:encours_cp.html.twig', array(
                                        'demande' => $demande,
                                        'environnement' => $this->container->get('kernel')->getEnvironment(),
                                        'url' => $url
                            )))
                    ;
                    $envoi_affectation = TRUE;
                }
                break;

            case Statut::RECETTE_USER :
                $recette = $old_statut;
                if (gettype($recette) == "integer") {
                    switch ($recette) {
                        //Cas classique : la demande passe en recette utilisateur
                        case 0 :
                            //Mail au demandeur pour préciser que sa demande vient de passer en recette utilisateur
                            $message_user
                                    ->setSubject($sujet . "Demande n°" . $demande->getId() . " : passage en recette.")
                                    ->setTo($mailDemandeur)
                                    ->setBody($this->renderView('SuiviEtudesBundle:Mail:recette_user.html.twig', array(
                                                'demande' => $demande,
                                                'environnement' => $this->container->get('kernel')->getEnvironment(),
                                                'url' => $url
                                    )))
                            ;
                            $envoi_user = in_array($this->getDoctrine()->getRepository('SuiviEtudesBundle:Mail')->find(5), $demandeur->getMail()->getValues());
                            break;
                        //Cas test OK
                        case 1 :

                            // MAIL AU DEMANDEUR ???
                            //Mail aux chefs de projet + manager tech pour préciser que le demandeur a validé la recette
                            $message_info
                                    ->setSubject($sujet . "Demande n°" . $demande->getId() . " : Recette validée par le demandeur.")
                                    ->setTo(array_merge($chef_de_projet, $technicien))
                                    ->setCc($manager_tech)
                                    ->setBody($this->renderView('SuiviEtudesBundle:Mail:recette_ok.html.twig', array(
                                                'demande' => $demande,
                                                'environnement' => $this->container->get('kernel')->getEnvironment(),
                                                'url' => $url,
                                                'userDmd' => $userDmd,
                                                'commentaire' => $data
                                    )))
                            ;
                            $envoi_info = TRUE;
                            break;
                        //Cas test NOK
                        case 2 :

                            // MAIL AU DEMANDEUR ???
                            if ($data != NULL) {
                                //Mail aux chefs de projet + manager tech pour préciser que le demandeur n'a pas validé la recette
                                $message_info
                                        ->setSubject($sujet . "Demande n°" . $demande->getId() . " : Recette refusée par le demandeur.")
                                        ->setTo($chef_de_projet)
                                        ->setCc($manager_tech)
                                        ->setBody($this->renderView('SuiviEtudesBundle:Mail:recette_nok.html.twig', array(
                                                    'demande' => $demande,
                                                    'environnement' => $this->container->get('kernel')->getEnvironment(),
                                                    'url' => $url,
                                                    'userDmd' => $userDmd,
                                                    'description' => $data
                                        )))
                                ;
                                $envoi_info = TRUE;
                            }
                            break;
                        // case modification de la mise en prod
                        case 3 :
                            // MAIL AU DEMANDEUR ???
                            $message_user
                                    ->setSubject($sujet . "Demande n°" . $demande->getId() . " : Date de mise en production modifiée par le service informatique.")
                                    ->setTo($mailDemandeur)
                                    ->setBody($this->renderView('SuiviEtudesBundle:Mail:modification_date_prod.html.twig', array(
                                                'demande' => $demande,
                                                'environnement' => $this->container->get('kernel')->getEnvironment(),
                                                'url' => $url,
                                                'userDmd' => $userDmd,
                                                'description' => $data
                                    )))
                            ;
                            $envoi_user = in_array($this->getDoctrine()->getRepository('SuiviEtudesBundle:Mail')->find(6), $demandeur->getMail()->getValues());
                            break;
                    }//fin switch
                } else {
                    if ($listeMail != null) {
                        //Envoi d'un mail au(x) technicien(s) pour l'informer de son affectation
                        $message_affectation
                                ->setSubject($sujet . "La demande n°" . $demande->getId() . " : vous a été affectée.")
                                ->setTo($listeMail)
                                //->setCc($observateur)
                                ->setBody($this->renderView('SuiviEtudesBundle:Mail:encours_cp.html.twig', array(
                                            'demande' => $demande,
                                            'environnement' => $this->container->get('kernel')->getEnvironment(),
                                            'url' => $url
                                )))
                        ;
                        $envoi_affectation = TRUE;
                    }
                }//Fin if recette integer
                break;

            case Statut::MISE_EN_PROD :
                //Envoi d'un mail au demandeur pour l'informer que sa demande a été mise en production
                if ($old_statut->getId() == Statut::RECETTE_USER) {
                    $message_user
                            ->setSubject($sujet . "Demande n°" . $demande->getId() . " : mise en production effectuée.")
                            ->setTo($mailDemandeur)
                            ->setCc($observateur)
                            ->setBody($this->renderView('SuiviEtudesBundle:Mail:mise_en_prod.html.twig', array(
                                        'demande' => $demande,
                                        'environnement' => $this->container->get('kernel')->getEnvironment(),
                                        'url' => $url
                            )))
                    ;
                    $envoi_user = TRUE;
                }

                if ($listeMail != null) {
                    //Envoi d'un mail au(x) technicien(s) pour l'informer de son affectation
                    $message_affectation
                            ->setSubject($sujet . "La demande n°" . $demande->getId() . " : vous a été affectée.")
                            ->setTo($listeMail)
                            //->setCc($observateur)
                            ->setBody($this->renderView('SuiviEtudesBundle:Mail:encours_cp.html.twig', array(
                                        'demande' => $demande,
                                        'environnement' => $this->container->get('kernel')->getEnvironment(),
                                        'url' => $url
                            )))
                    ;
                    $envoi_affectation = TRUE;
                }
                break;

            case Statut::CLOS :
                //Envoi d'un mail au demandeur et aux observateurs pour confirmer la clôture de la demande
                $message_user
                        ->setSubject($sujet . "Demande n°" . $demande->getId() . " : Close !")
                        ->setTo($mailDemandeur)
                        ->setBody($this->renderView('SuiviEtudesBundle:Mail:clore.html.twig', array(
                                    'demande' => $demande,
                                    'environnement' => $this->container->get('kernel')->getEnvironment(),
                                    'url' => $url
                        )))
                ;
                $envoi_user = in_array($this->getDoctrine()->getRepository('SuiviEtudesBundle:Mail')->find(8), $demandeur->getMail()->getValues());
                //Envoi d'un mail au manager tech, au(x) technicien(s) et au chef de projet aussi
                $message_info
                        ->setSubject($sujet . "Demande n°" . $demande->getId() . " : Close !")
                        ->setTo($manager_tech)
                        ->setBody($this->renderView('SuiviEtudesBundle:Mail:clore.html.twig', array(
                                    'demande' => $demande,
                                    'environnement' => $this->container->get('kernel')->getEnvironment(),
                                    'url' => $url
                        )))
                ;
                $envoi_info = TRUE;
                break;

            case Statut::ANNULE :
                //Envoi d'un mail aux demandeur et aux observateurs pour confirmer l'annulation de la demande
                $message_user
                        ->setSubject($sujet . "Demande n°" . $demande->getId() . " : Annulée !")
                        ->setTo($mailDemandeur)
                        ->setCc($observateur) // + $manager_tech + $technicien
                        ->setBody($this->renderView('SuiviEtudesBundle:Mail:annule.html.twig', array(
                                    'demande' => $demande,
                                    'environnement' => $this->container->get('kernel')->getEnvironment(),
                                    'url' => $url,
                                    'descAnnulation' => $data
                        )))
                ;
                $envoi_user = in_array($this->getDoctrine()->getRepository('SuiviEtudesBundle:Mail')->find(9), $demandeur->getMail()->getValues());
                //Envoi d'un mail au manager tech, au(x) technicien(s) et au(x) chef(s) de projet aussi
                $message_info
                        ->setSubject($sujet . "Demande n°" . $demande->getId() . " : Annulée !")
                        ->setTo($manager_tech)
                        ->setCc($chef_de_projet) // + $manager_tech + $technicien
                        ->setBody($this->renderView('SuiviEtudesBundle:Mail:annule.html.twig', array(
                                    'demande' => $demande,
                                    'environnement' => $this->container->get('kernel')->getEnvironment(),
                                    'url' => $url,
                                    'descAnnulation' => $data
                        )))
                ;
                $envoi_info = TRUE;
                break;
        }//fin switch
        //Envoi de l'e-mail pour les utilisateurs
        if ($envoi_user) {
            if ($demande->getMailing()) {
                $message_user->setCc($observateur);
            }
            if ($this->get('mailer')->send($message_user)) {
                $this->get('session')->getFlashBag()->add('info', "Un mail a été envoyé au demandeur pour les prévenir.");
            } else {
                $this->get('session')->getFlashBag()->add('warning', "L'envoi de mail n'a pas marché. Veuillez prévenir le demandeur manuellement.");
            }
        }
        //Envoi de l'e-mail pour le service info
        if ($envoi_info) {
            if ($this->get('mailer')->send($message_info)) {
                $this->get('session')->getFlashBag()->add('info', "Un mail a été envoyé au service informatique pour les prévenir.");
            } else {
                $this->get('session')->getFlashBag()->add('warning', "L'envoi de mail n'a pas marché.");
            }
        }
        //Envoi de l'e-mail pour le service info
        if ($envoi_resp_tech) {
            if ($this->get('mailer')->send($message_resp_tech)) {
                $this->get('session')->getFlashBag()->add('info', "Un mail a été envoyé à " . $resTechSAP->getDisplayname() . ".");
            } else {
                $this->get('session')->getFlashBag()->add('warning', "L'envoi de mail n'a pas marché. Veuillez prévenir " . $resTechSAP->getDisplayname() . " manuellement.");
            }
        }
        //Envoi de l'e-mail pour le service info
        if ($envoi_affectation) {
            if ($this->get('mailer')->send($message_affectation)) {
                $this->get('session')->getFlashBag()->add('info', "Un mail a été envoyé aux personnes affectées.");
            } else {
                $this->get('session')->getFlashBag()->add('warning', "L'envoi de mail n'a pas marché. Veuillez prévenir les personnes affectées manuellement.");
            }
        }
    }

//Fin fonction envoyerMail
}
