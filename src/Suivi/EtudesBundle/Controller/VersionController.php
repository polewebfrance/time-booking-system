<?php

namespace Suivi\EtudesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class VersionController extends Controller {

    public function modificationsAction() {
        if (!$this->get('security.context')->isGranted('ROLE_SET_TECH')) {
            return $this->redirect($this->generateUrl('suivi_etudes_accueil'));
        }
        return $this->render('SuiviEtudesBundle:Version:index.html.twig', array(
        ));
    }

}
