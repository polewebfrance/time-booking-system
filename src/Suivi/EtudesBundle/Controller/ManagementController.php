<?php

namespace Suivi\EtudesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Suivi\EtudesBundle\Entity\ProjectPole;
use Suivi\EtudesBundle\Form\Project\ProjectPoleType;
use Suivi\EtudesBundle\Entity\ProjectJob;
use Suivi\EtudesBundle\Form\Project\ProjectJobType;
use Suivi\EtudesBundle\Entity\ProjectManager;
use Suivi\EtudesBundle\Form\Project\ProjectManagerType;
use Suivi\EtudesBundle\Entity\ProjectTech;
use Suivi\EtudesBundle\Form\Project\ProjectTechType;
use Doctrine\Common\Collections\ArrayCollection;

class ManagementController extends Controller {

//    /**
//     * @Route("/pole/{id}", name="suivi_etudes_admin_management_pole")
//     * @Method({"GET","POST"})
//     */
//    public function pole(Request $request, $id = 0) {
//        $userId = $this->getUser()->getId();
//        $em = $this->getDoctrine()->getManager();
//        $pole = $em->getRepository('SuiviEtudesBundle:ProjectPole')->find($id);
//        $allPols = $em->getRepository('SuiviEtudesBundle:ProjectPole')->findBy(
//                array(), array('name' => 'ASC'), 10
//        );
//        if (null === $pole) {
//            $pole = new ProjectPole();
//            $pole->setCreatedBy($userId);
//        } else {
//            $pole->setUpdatedBy($userId);
//        }
//        $form = $this->createForm(new ProjectPoleType(), $pole);
//        $form->handleRequest($request);
//        if ($request->isMethod("POST")) {
//            if ($form->isValid()) {
//                $em->persist($pole);
//                try {
//                    $this->get('session')->getFlashBag()->add('success', 'Le pôle a été enregistré avec succès.');
//                    $em->flush();
//                } catch (\Doctrine\DBAL\DBALException $e) {
//                    $this->get('session')->getFlashBag()->add('myError', 'Oups ... Il y avait une erreur.');
//                }
//            }
//            return $this->redirect($this->generateUrl('suivi_etudes_admin_management_pole_list'));
//        }
//        return $this->render('SuiviEtudesBundle:Projet:Admin/pole.html.twig', array(
//                    'form' => $form->createView(),
//                    'allPols' => $allPols,
//        ));
//    }

    /**
     * @Route("delete-pole/{id}", name="suivi_etudes_admin_management_pole_delete")
     * @Method({"POST"})
     * 
     * Description:
     * delete pole
     */
    public function deletePole(Request $request, $id) {
        if (!$request->isXmlHttpRequest()) {
            return new Response('');
        }
        $em = $this->getDoctrine()->getManager();
        $row = $em->getRepository('SuiviEtudesBundle:ProjectPole')->find($id);
        $row->setActive('0');
        try {
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'Désactiver avec succès.');
        } catch (\Doctrine\DBAL\DBALException $e) {
            $this->get('session')->getFlashBag()->add('myError', 'Oups ... Il y avait une erreur.');
        }
        return new Response('Done');
    }

    /**
     * @Route("activate-pole/{id}", name="suivi_etudes_admin_management_pole_activate")
     * @Method({"POST"})
     */
    public function activatePole(Request $request, $id) {
        if (!$request->isXmlHttpRequest()) {
            return new Response('');
        }
        $em = $this->getDoctrine()->getManager();
        $row = $em->getRepository('SuiviEtudesBundle:ProjectPole')->find($id);
        $row->setActive('1');
        try {
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'Activer avec succès.');
        } catch (\Doctrine\DBAL\DBALException $e) {
            $this->get('session')->getFlashBag()->add('myError', 'Oups ... Il y avait une erreur.');
        }
        return new Response('Done');
    }

//    /**
//     * @Route("/list/pole", name="suivi_etudes_admin_management_pole_list")
//     * @Method({"GET"})
//     */
//    public function listPole() {
//        return $this->render('SuiviEtudesBundle:Projet:Admin/listpole.html.twig');
//    }

    /**
     * @Route("/list/pole-ajax", name="suivi_etudes_admin_management_pole_list_ajax")
     * @Method({"GET"})
     */
    public function listPoleXHR(Request $request) {
        if (!$request->isXmlHttpRequest()) {
            return new Response('');
        }
        $em = $this->getDoctrine()->getManager();
        $list = $em->getRepository('SuiviEtudesBundle:ProjectPole')->getAll();
        return new JsonResponse($list);
    }

//    /**
//     * @Route("/metier/{id}", name="suivi_etudes_admin_management_job")
//     * @Method({"GET","POST"})
//     */
//    public function job(Request $request, $id = 0) {
//        $userId = $this->getUser()->getId();
//        $em = $this->getDoctrine()->getManager();
//        $job = $em->getRepository('SuiviEtudesBundle:ProjectJob')->find($id);
//        $lastJobs = $em->getRepository('SuiviEtudesBundle:ProjectJob')->findBy(
//                array('active' => 1), array('name' => 'ASC'), 10
//        );
//        if (null === $job) {
//            $job = new ProjectJob();
//            $job->setCreatedBy($userId);
//        } else {
//            $job->setUpdatedBy($userId);
//        }
//        $form = $this->createForm(new ProjectJobType(), $job);
//        $form->handleRequest($request);
//        if ($request->isMethod("POST")) {
//            if ($form->isValid()) {
//                $em->persist($job);
//                try {
//                    $this->get('session')->getFlashBag()->add('success', 'Le métier a été enregistré avec succès.');
//                    $em->flush();
//                } catch (\Doctrine\DBAL\DBALException $e) {
//                    $this->get('session')->getFlashBag()->add('myError', 'Oups ... Il y avait une erreur.');
//                }
//            }
//            return $this->redirect($this->generateUrl('suivi_etudes_admin_management_job_list'));
//        }
//        return $this->render('SuiviEtudesBundle:Projet:Admin/job.html.twig', array(
//                    'form' => $form->createView(),
//                    'lastJobs' => $lastJobs,
//        ));
//    }

    /**
     * @Route("delete-metier/{id}", name="suivi_etudes_admin_management_job_delete")
     * @Method({"POST"})
     * 
     * Description:
     * delete job
     */
    public function deleteJob(Request $request, $id) {
        if (!$request->isXmlHttpRequest()) {
            return new Response('');
        }
        $em = $this->getDoctrine()->getManager();
        $row = $em->getRepository('SuiviEtudesBundle:ProjectJob')->find($id);
        $row->setActive('0');
        try {
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'Désactiver avec succès.');
        } catch (\Doctrine\DBAL\DBALException $e) {
            $this->get('session')->getFlashBag()->add('error', 'Ups...There was an error.');
        }
        return new Response('Done');
    }

    /**
     * @Route("activate-job/{id}", name="suivi_etudes_admin_management_job_activate")
     * @Method({"POST"})
     */
    public function activateJob(Request $request, $id) {
        if (!$request->isXmlHttpRequest()) {
            return new Response('');
        }
        $em = $this->getDoctrine()->getManager();
        $row = $em->getRepository('SuiviEtudesBundle:ProjectJob')->find($id);
        $row->setActive('1');
        try {
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'Activer avec succès.');
        } catch (\Doctrine\DBAL\DBALException $e) {
            $this->get('session')->getFlashBag()->add('error', 'Ups...There was an error.');
        }
        return new Response('Done');
    }

//    /**
//     * @Route("/list/metier", name="suivi_etudes_admin_management_job_list")
//     * @Method({"GET"})
//     */
//    public function listJob() {
//        return $this->render('SuiviEtudesBundle:Projet:Admin/listjob.html.twig');
//    }
//
//    /**
//     * @Route("/list/metier-ajax", name="suivi_etudes_admin_management_job_list_ajax")
//     * @Method({"GET"})
//     */
//    public function listJobXHR(Request $request) {
//        if (!$request->isXmlHttpRequest()) {
//            return new Response('');
//        }
//        $em = $this->getDoctrine()->getManager();
//        $list = $em->getRepository('SuiviEtudesBundle:ProjectJob')->getAll();
//        return new JsonResponse($list);
//    }

    /**
     * @Route("/manager/{id}", name="suivi_etudes_admin_management_manager")
     * @Method({"GET","POST"})
     */
//    public function manager(Request $request, $id = 0) {
//        $userId = $this->getUser()->getId();
//        $em = $this->getDoctrine()->getManager();
//        $manager = $em->getRepository('SuiviEtudesBundle:ProjectManager')->find($id);
//        $lastManagers = $em->getRepository('SuiviEtudesBundle:ProjectManager')->findBy(
//                array(), array('pole' => 'ASC', 'manager' => 'ASC'), 10
//        );
//        if (null === $manager) {
//            $manager = new ProjectManager();
//            $manager->setCreatedBy($userId);
//        } else {
//            $manager->setUpdatedBy($userId);
//        }
//        $form = $this->createForm(new ProjectManagerType($em), $manager);
//        $form->handleRequest($request);
//        if ($request->isMethod("POST")) {
//            if ($form->isValid()) {
//                $em->persist($manager);
//                try {
//                    $this->get('session')->getFlashBag()->add('success', 'Les informations ont été enregistrées.');
//                    $em->flush();
//                } catch (\Doctrine\DBAL\DBALException $e) {
//                    $this->get('session')->getFlashBag()->add('myError', 'Oups ... Il y avait une erreur.');
//                }
//                return $this->redirect($this->generateUrl('suivi_etudes_admin_management_manager_list'));
//            }
//        }
//        return $this->render('SuiviEtudesBundle:Projet:Admin/manager.html.twig', array(
//                    'form' => $form->createView(),
//                    'lastManagers' => $lastManagers
//        ));
//    }

    /**
     * @Route("delete-manager/{id}", name="suivi_etudes_admin_management_manager_delete")
     * @Method({"POST"})
     * 
     * Description:
     * delete job
     */
    public function deleteManager(Request $request, $id) {
        if (!$request->isXmlHttpRequest()) {
            return new Response('');
        }
        $em = $this->getDoctrine()->getManager();
        $row = $em->getRepository('SuiviEtudesBundle:ProjectManager')->find($id);
        $row->setActive('0');
        try {
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'Désactiver avec succès.');
        } catch (\Doctrine\DBAL\DBALException $e) {
            $this->get('session')->getFlashBag()->add('error', 'Ups...There was an error.');
        }
        return new Response('Done');
    }

    /**
     * @Route("activate-manager/{id}", name="suivi_etudes_admin_management_manager_activate")
     * @Method({"POST"})
     */
    public function activateManager(Request $request, $id) {
        if (!$request->isXmlHttpRequest()) {
            return new Response('');
        }
        $em = $this->getDoctrine()->getManager();
        $row = $em->getRepository('SuiviEtudesBundle:ProjectManager')->find($id);
        $row->setActive('1');
        try {
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'Activer avec succès.');
        } catch (\Doctrine\DBAL\DBALException $e) {
            $this->get('session')->getFlashBag()->add('error', 'Ups...There was an error.');
        }
        return new Response('Done');
    }

    /**
     * @Route("/list/manager", name="suivi_etudes_admin_management_manager_list")
     * @Method({"GET"})
     */
    public function listManager() {
        return $this->render('SuiviEtudesBundle:Projet:Admin/listmanager.html.twig');
    }

    /**
     * @Route("/list/manager-ajax", name="suivi_etudes_admin_management_manager_list_ajax")
     * @Method({"GET"})
     */
    public function listManagerXHR(Request $request) {
        if (!$request->isXmlHttpRequest()) {
            return new Response('');
        }
        $em = $this->getDoctrine()->getManager();
        $list = $em->getRepository('SuiviEtudesBundle:ProjectManager')->getAll();
        return new JsonResponse($list);
    }

//    /**
//     * @Route("/tech/{id}", name="suivi_etudes_admin_management_tech")
//     * @Method({"GET","POST"})
//     */
//    public function tech(Request $request, $id = 0) {
//        $userId = $this->getUser()->getId();
//        $em = $this->getDoctrine()->getManager();
//        $tech = $em->getRepository('SuiviEtudesBundle:ProjectTech')->find($id);
//
//        if (null === $tech) {
//            $tech = new ProjectTech();
//            $tech->setCreatedBy($userId);
//        } else {
//            $tech->setUpdatedBy($userId);
//        }
//
//        // get tech info to array
//        $originalTechInfo = new ArrayCollection();
//        foreach ($tech->getTechInfo() as $info) {
//            $originalTechInfo->add($info);
//        }
//        $form = $this->createForm(new ProjectTechType($em), $tech);
//        $form->handleRequest($request);
//        if ($request->isMethod("POST")) {
//            if ($form->isValid()) {
//                foreach ($tech->getTechInfo() as $info) {
//                    $info->setTech($tech);
//                }
//                $em->persist($tech);
//                try {
//                    $this->get('session')->getFlashBag()->add('success', 'Les informations ont été enregistrées.');
//                    $em->flush();
//                } catch (\Doctrine\DBAL\DBALException $e) {
//                    $this->get('session')->getFlashBag()->add('myError', 'Oups ... Il y avait une erreur.');
//                }
//                return $this->redirect($this->generateUrl('suivi_etudes_admin_management_tech_list'));
//            }
//        }
//        $lastTechs = $em->getRepository('SuiviEtudesBundle:ProjectTech')->findBy(
//                array('active' => 1), array('pole' => 'ASC', 'tech' => 'ASC'), 10
//        );
//        return $this->render('SuiviEtudesBundle:Projet:Admin/tech.html.twig', array(
//                    'form' => $form->createView(),
//                    'lastTechs' => $lastTechs,
//                    'techId' => $id
//        ));
//    }

    /**
     * @Route("delete-tech/{id}", name="suivi_etudes_admin_management_tech_delete")
     * @Method({"POST"})
     * 
     * Description:
     * delete tech
     */
    public function deleteTech(Request $request, $id) {
        if (!$request->isXmlHttpRequest()) {
            return new Response('');
        }
        $em = $this->getDoctrine()->getManager();
        $row = $em->getRepository('SuiviEtudesBundle:ProjectTech')->find($id);
        $row->setActive('0');
        try {
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'Supprimer avec succès.');
        } catch (\Doctrine\DBAL\DBALException $e) {
            $this->get('session')->getFlashBag()->add('myError', 'Oups ... Il y avait une erreur.');
        }
        return new Response('Done');
    }

    /**
     * @Route("activate-tech/{id}", name="suivi_etudes_admin_management_tech_activate")
     * @Method({"POST"})
     */
    public function activateTech(Request $request, $id) {
        if (!$request->isXmlHttpRequest()) {
            return new Response('');
        }
        $em = $this->getDoctrine()->getManager();
        $row = $em->getRepository('SuiviEtudesBundle:ProjectTech')->find($id);
        $row->setActive('1');
        try {
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'Activer avec succès.');
        } catch (\Doctrine\DBAL\DBALException $e) {
            $this->get('session')->getFlashBag()->add('myError', 'Oups ... Il y avait une erreur.');
        }
        return new Response('Done');
    }

//    /**
//     * @Route("/list/tech", name="suivi_etudes_admin_management_tech_list")
//     * @Method({"GET"})
//     */
//    public function listTech() {
//        return $this->render('SuiviEtudesBundle:Projet:Admin/listtech.html.twig');
//    }

    /**
     * @Route("/list/tech-ajax", name="suivi_etudes_admin_management_tech_list_ajax")
     * @Method({"GET"})
     */
    public function listTechXHR(Request $request) {
        if (!$request->isXmlHttpRequest()) {
            return new Response('');
        }
        $em = $this->getDoctrine()->getManager();
        $list = $em->getRepository('SuiviEtudesBundle:ProjectTech')->getAll();
        return new JsonResponse($list);
    }

//    /**
//     * @Route("/absence/type/{typeId}", name="suivi_etudes_admin_management_absence_type")
//     * @Method({"GET","POST"})
//     */
//    public function absenceType(Request $request, $typeId = 0) {
//        $em = $this->getDoctrine()->getManager();
//        $row = $em->getRepository('SuiviEtudesBundle:AbsenceType')->find($typeId);
//        if (empty($row)) {
//            $row = new \Suivi\EtudesBundle\Entity\AbsenceType();
//        }
//        $form = $this->createForm(new \Suivi\EtudesBundle\Form\Absence\AbsenceTypeType(), $row);
//
//        $form->handleRequest($request);
//        if ($request->isMethod('POST')) {
//            if ($form->isValid()) {
//                $em->persist($row);
//                try {
//                    $em->flush();
//                    $this->get('session')->getFlashBag()->add('success', 'The change was made successfully.');
//                } catch (\Doctrine\DBAL\DBALException $e) {
//                    $this->get('session')->getFlashBag()->add('myError', 'Oups ... Il y avait une erreur.');
//                }
//                return $this->redirect($this->generateUrl('suivi_etudes_admin_management_absence_type_list'));
//            }
//        }
//        return $this->render('SuiviEtudesBundle:Projet:Admin/Absence/type.html.twig', array(
//                    'form' => $form->createView()
//        ));
//    }

    /**
     * @Route("/absence/type/delete/{typeId}", name="suivi_etudes_admin_management_absence_type_delete")
     * @Method({"POST"})
     */
    public function absenceDeleteType(Request $request, $typeId) {
        if (!$request->isXmlHttpRequest()) {
            return new Response('');
        }
        $em = $this->getDoctrine()->getManager();
        $row = $em->getRepository('SuiviEtudesBundle:AbsenceType')->find($typeId);
        $em->remove($row);
        try {
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'Supprimer avec succès.');
        } catch (\Doctrine\DBAL\DBALException $e) {
            $this->get('session')->getFlashBag()->add('myError', 'Oups ... Il y avait une erreur.');
        }
        return new Response('Done');
    }

//    /**
//     * @Route("/absence/list-type", name="suivi_etudes_admin_management_absence_type_list")
//     * @Method({"GET"})
//     */
//    public function absenceTypeList() {
//        return $this->render('SuiviEtudesBundle:Projet:Admin/Absence/typelist.html.twig');
//    }

    /**
     * @Route("/absence/list-type-ajax", name="suivi_etudes_admin_management_absence_type_list_ajax")
     * @Method({"GET"})
     */
    public function listAbsenceTypeXHR(Request $request) {
        if (!$request->isXmlHttpRequest()) {
            return new Response('');
        }
        $em = $this->getDoctrine()->getManager();
        $list = $em->getRepository('SuiviEtudesBundle:AbsenceType')->getAll();
        return new JsonResponse($list);
    }

}
