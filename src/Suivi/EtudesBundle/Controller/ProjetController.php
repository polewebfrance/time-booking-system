<?php

namespace Suivi\EtudesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Suivi\EtudesBundle\Entity\Statut;

class ProjetController extends Controller {

//    /**
//     * @Route("/projet/capacite/{periode}", name="suivi_etudes_admin_project_person",
//     * defaults={"periode":0},requirements={"periode": "[0-9]{4}[-]{1}[0-9]{2}[-]{1}[0-9]{2}"})
//     * @Method({"GET"})
//     */
//    public function viewCapacity($periode) {
//        if (empty($periode) || !strtotime($periode)) {
//            return $this->redirect($this->generateUrl('suivi_etudes_admin_project_person', array('periode' => date('Y-m-01'))));
//        }
//        return $this->render('SuiviEtudesBundle:Projet:utilisateur_capacite.html.twig', array(
//                    'periode' => $periode,
//                    'prevPeriode' => $date = date('Y-m-d', strtotime($periode . '-1 month')),
//                    'nextPeriode' => $date = date('Y-m-d', strtotime($periode . '+1 month')),
//        ));
//    }

    /**
     * @Route("/projet/capacite-xhr/{periode}", name="suivi_etudes_admin_project_person_xhr",
     * defaults={"periode":0},requirements={"periode": "[0-9]{4}[-]{1}[0-9]{2}[-]{1}[0-9]{2}"})
     * @Method({"GET"})
     */
    public function viewCapacityXHR(Request $request, $periode) {
        if (!$request->isXmlHttpRequest()) {
            return new Response('');
        }
        $managerId = $this->getUser()->getId();
        $em = $this->getDoctrine()->getManager();
        $list = $em->getRepository('SuiviEtudesBundle:ProjectTech')->getTechCapacityByManagerId($managerId, $periode);
        return new JsonResponse($list);
    }

    /**
     * @Route("/projet/ajouter-utilisateur-capacite", name="suivi_etudes_admin_project_add_user_capacity")
     * @Method({"POST"})
     */
    public function addUserCapacityXHR() {
        $request = $this->container->get('request');
        $userId = $request->request->get('pk');
        $value = $request->request->get('value'); # capacity (hours) or percent
        $name = $request->request->get('name');
        $period = $request->request->get('period');
        $hours = $request->request->get('hours');
        $percent = $request->request->get('percent');
        $em = $this->getDoctrine()->getManager();

        // set a response
        $response = 'Success';
        // get informations about worked hours for this user on this period
        $capacityRow = $em->createQueryBuilder('query')
                        ->select('cap.remainingHours', 'sum(ch.hoursWorked) hoursWorked', 'sum(ch.hours) hoursPrev', 'cap.capacity', 'cap.hours', 'ch.demandeId')
                        ->from('SuiviEtudesBundle:ProjetUserCapacite', 'cap')
                        ->leftJoin('SuiviEtudesBundle:ProjetCharge', 'ch', 'WITH', 'ch.userId = cap.userId AND ch.period = cap.period')
                        ->where('cap.userId = :userId')
                        ->andWhere('cap.period = :period')
                        ->setParameter('userId', $userId)
                        ->setParameter('period', $period)
                        ->getQuery()->getSingleResult();
        $userCapacite = $this->getDoctrine()->getManager()->getRepository('SuiviEtudesBundle:ProjetUserCapacite')->findOneBy(
                array(
                    'userId' => $userId,
                    'period' => new \DateTime($period)
                )
        );
        if (null === $userCapacite) {
            $userCapacite = new \Suivi\EtudesBundle\Entity\ProjetUserCapacite();
        } else {
            $userCapacite->setUpdatedBy($this->getUser()->getId());
            $userCapacite->setUpdatedAt(new \DateTime('now'));
        }
        /* projet_user_capacite table
         * 
         * Atention
         * table projet_taux_horaire is populated using a trigger on this DB 
         */
        $userCapacite->setUserId($userId);
        $userCapacite->setPeriod(new \DateTime($period));
        if ($name == 'hours') {
            // validate insert based on user informations
            if ($value * $capacityRow['capacity'] / 100 < $capacityRow['hoursPrev']) {
                $response = 'Modifier les heures travaillées pour cet utilisateur dans ce mois.';
            } else {
                $userCapacite->setHours($value);
                $userCapacite->setRemainingHours("0");
                $userCapacite->setCapacity($percent);
            }
        }
        if ($name == 'percent') {
            // validate insert based on user informations
            if ($capacityRow['hours'] * $value / 100 < $capacityRow['hoursPrev']) {
                $response = 'Modifier les heures travaillées pour cet utilisateur dans ce mois.';
            } else {
                $userCapacite->setHours($userCapacite->getHours());
                $userCapacite->setRemainingHours("0");
                $userCapacite->setCapacity($value);
            }
        }
        $userCapacite->setCreatedBy($this->getUser()->getId());
        $userCapacite->setCreatedAt(new \DateTime('now'));
        $userCapacite->setFirstname($request->request->get('usrfirstname'));
        $userCapacite->setLastname($request->request->get('usrlastname'));

        if ($response == "Success") {
            $em->persist($userCapacite);
            try {
                $em->flush();
            } catch (\Doctrine\DBAL\DBALException $e) {
                return new Response('Erreur sur insert.');
            }
        }

        return new Response($response);
    }

//    /**
//     * @Route("/projet/taux-horaire/{periode}", name="suivi_etudes_admin_project_taux_horaire",
//     * defaults={"periode":0},requirements={"periode": "[0-9]{4}[-]{1}[0-9]{2}[-]{1}[0-9]{2}"})
//     * @Method({"GET"})
//     */
//    public function hourlyRate($periode) {
//        /* ACCESS Rule */
//        $user = $this->getUser();
//        if (!$user->isGranted('ROLE_SET_TAX')) {
//            return $this->redirect($this->generateUrl('suivi_etudes_accueil'));
//        }
//        /* END ACCESS RULE */
//        $em = $this->getDoctrine()->getManager();
//        if (empty($periode) || !strtotime($periode)) {
//            return $this->redirect($this->generateUrl('suivi_etudes_admin_project_taux_horaire', array('periode' => date('Y-m-01'))));
//        }
//        return $this->render('SuiviEtudesBundle:Projet:taux_horaire.html.twig', array(
//                    'periode' => $periode, 'prevPeriode' => $date = date('Y-m-d', strtotime($periode . '-1 month')),
//                    'nextPeriode' => $date = date('Y-m-d', strtotime($periode . '+1 month')),
//        ));
//    }

    /**
     * @Route("/projet/taux-horaire-ajax/{periode}", name="suivi_etudes_admin_project_taux_horaire_xhr",
     * defaults={"periode":0},requirements={"periode": "[0-9]{4}[-]{1}[0-9]{2}[-]{1}[0-9]{2}"})
     * @Method({"GET"})
     */
    public function hourlyRateXHR(Request $request, $periode) {
        if (!$request->isXmlHttpRequest()) {
            return new Response('');
        }
        $em = $this->getDoctrine()->getManager();
        $list = $em->getRepository('SuiviEtudesBundle:ProjectTech')->getHourlyTax($periode);
        return new JsonResponse($list);
    }

    /**
     * @Route("/projet/ajouter-temps-de-travail", name="suivi_etudes_admin_project_add_user_working_hours")
     * @Method({"POST"})
     */
    public function addWorkingHoursXHR() {
        $request = $this->container->get('request');
        $hours = $request->request->get('value');
        $period = $request->request->get('period');

        $em = $this->getDoctrine()->getManager();
        $workingHours = $this->getDoctrine()->getManager()->getRepository('SuiviEtudesBundle:ProjetWorkingHours')->findOneBy(
                array(
                    'period' => new \DateTime($period . ' 00:00:00')
                )
        );

        if ($workingHours) { #update
            $workingHours->setHours($hours);
        } else { #insert
            $workingHours = new \Suivi\EtudesBundle\Entity\ProjetWorkingHours();
            $workingHours->setHours($hours);
            $workingHours->setPeriod(new \DateTime($period . ' 00:00:00'));
        }

        try {
            $em->persist($workingHours);
            $em->flush();
        } catch (\Doctrine\DBAL\DBALException $e) {
            return new Response('Erreur.');
        }
        return new Response('Terminer');
    }

    /**
     * @Route("/projet/modifier-taux-horaire", name="suivi_etudes_admin_project_modify_hourly_tax")
     * @Method({"POST"})
     */
    public function modifyHourlyTaxXHR() {
        $request = $this->container->get('request');
        $tax = $request->request->get('value');
        $period = $request->request->get('period');
        $userId = $request->request->get('pk');
        $em = $this->getDoctrine()->getManager();
        $taxRow = $this->getDoctrine()->getManager()->getRepository('SuiviEtudesBundle:ProjetTauxHoraire')->findOneBy(
                array(
                    'period' => $period,
                    'userId' => $userId
                )
        );

        #tax is already in table so just do an update
        if ($taxRow) {
            $taxRow->setTauxHoraire($tax);
        } else { #insert
            $taxRow = new \Suivi\EtudesBundle\Entity\ProjetTauxHoraire();
            $taxRow->setUserId($userId);
            $taxRow->setPeriod($period);
            $taxRow->setTauxHoraire($tax);
        }

        try {
            $em->persist($taxRow);
            $em->flush();
        } catch (\Doctrine\DBAL\DBALException $e) {
            return new Response('Erreur.');
        }
        return new Response('Terminer');
    }

    /**
     * @Route("/projet/planification/{id}", name="suivi_etudes_admin_project_plan",
     * defaults={"id":0},requirements={"id": "\d+"})
     * @Method({"GET"})
     */
    public function planProjectAction($id) {
        ini_set('xdebug.max_nesting_level', 110);
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder('query')
                ->select('CONCAT(user.surname,\' \',user.givenname) userName', 'd.id', 'd.titre', 'du.id demandeUserId', 'du.type demandeUserType', 'du.userId'
                        , 'pc.period chargePeriode', 'pc.hours chargeHours', 'pc.hoursWorked', 'cap.capacity userCapacity', 'cap.hours capHours'
                        , 'cap.remainingHours capRemainingHours', 'tax.tauxHoraire', 'tax.jobTitle', 'j.name actualJobTitle', 'd.statutId'
                        , 'd.date projectDate', 'd.dateProduction', 'pc.id chargeId', 'pc.demandeId as chargeDemandeId', 'cap.period capacityPeriode')
                ->from('SuiviEtudesBundle:Demande', 'd')
                ->leftJoin('SuiviEtudesBundle:DemandeUser', 'du', 'WITH', 'd.id=du.demande')
                ->leftJoin('SuiviEtudesBundle:ProjectTech', 'prjUser', 'WITH', 'Identity(prjUser.tech)=du.userId')
                ->leftJoin('SuiviEtudesBundle:ProjectJob', 'j', 'WITH', 'Identity(prjUser.poste)=j.id')
                ->leftJoin('UserUserBundle:User', 'user', 'WITH', 'user.id=du.userId')
                ->leftJoin('SuiviEtudesBundle:ProjetUserCapacite', 'cap', 'WITH', 'cap.userId = user.id')
                ->leftJoin('SuiviEtudesBundle:ProjetTauxHoraire', 'tax', 'WITH', 'tax.userId = user.id AND tax.period = cap.period')
                ->leftJoin('SuiviEtudesBundle:ProjetCharge', 'pc', 'WITH', 'pc.demandeUserId=du.id and pc.period = cap.period')
                ->where('d.id = :id')
                ->setParameter('id', $id)
                ->orderBy('du.type', 'ASC')
                ->addOrderBy('user.surname', 'ASC')
                ->addOrderBy('tax.period', 'ASC');
        $project = $qb->getQuery()->getResult();

        // calculate number of months
        $demande = $em->getRepository('SuiviEtudesBundle:Demande')->find($id);
        if (empty($demande)) {
            return new Response('Vide');
        }
        $techOnDemand = $em->createQueryBuilder('query')
                        ->select('min(m.date) modifAdded, min(pc.period) charge')
                        ->from('SuiviEtudesBundle:Modification', 'm')
                        ->leftJoin('SuiviEtudesBundle:ProjetCharge', 'pc', 'WITH', 'pc.demande=m.demande')
                        ->where('m.demande = :id')
                        ->andWhere("m.type = 'CHEF_DE_PROJET' OR m.type = 'TECHNICIEN'")
                        ->setParameter('id', $demande)
                        ->getQuery()->getSingleResult();
        $workingTechs = empty($techOnDemand['modifAdded']) ? 0 : 1;
        $demandeTech = $em->createQueryBuilder('query')
                        ->select('du.userId, sum(pc.hours) hours, sum(pc.hoursWorked) hoursWorked')
                        ->from('SuiviEtudesBundle:DemandeUser', 'du')
                        ->innerJoin('SuiviEtudesBundle:ProjetCharge', 'pc', 'WITH', 'pc.demandeUserId=du.id')
                        ->where('du.demande = :id')
                        ->andWhere('du.type=\'TECHNICIEN\'')
                        ->groupBy('du.userId')
                        ->setParameter('id', $id)
                        ->getQuery()->getResult();
        $demandeCP = $em->createQueryBuilder('query')
                        ->select('du.userId, sum(pc.hours) hours, sum(pc.hoursWorked) hoursWorked')
                        ->from('SuiviEtudesBundle:DemandeUser', 'du')
                        ->innerJoin('SuiviEtudesBundle:ProjetCharge', 'pc', 'WITH', 'pc.demandeUserId=du.id')
                        ->where('du.demande = :id')
                        ->andWhere('du.type=\'CHEF_DE_PROJET\'')
                        ->groupBy('du.userId')
                        ->setParameter('id', $id)
                        ->getQuery()->getResult();
        $projectDate = $demande->getDate()->format('Y-m-d');
        if (!empty($techOnDemand['modifAdded'])) {
            $techDate = substr($techOnDemand['modifAdded'], 0, 8) . "01";
            if (!empty($techOnDemand['charge'])) {
                $techDate = $techOnDemand['charge'];
            }
            $datetime1 = new \DateTime($techDate);
            $datetime2 = new \DateTime(date('Y-m-01'));
            $interval = $datetime1->diff($datetime2);
            $validationDate = round($interval->y * 12 + $interval->m + $interval->d / 30);

            // set no of months based on prod date or if status is bigger than CLOS, then stop increasing month no 
            if (!empty($project[0]['dateProduction']) && ($project[0]['dateProduction']->format('Y-m-01') >= date('Y-m-01') || $demande->getStatut()->getId() >= Statut::CLOS)) {
                $dateProduction = $demande->getDateProduction()->format('Y-m-01');
                $datetime1 = new \DateTime($dateProduction);
                $datetime2 = new \DateTime($techDate);
                $interval = $datetime1->diff($datetime2);
                $validationDate = round($interval->y * 12 + $interval->m + $interval->d / 30);
            }

            // add filter for demande user type
            $qb->andWhere('du.type=\'TECHNICIEN\' or du.type=\'CHEF_DE_PROJET\'');
            $project = $qb->getQuery()->getResult();

            $data = array();
            // create array for every user to render in view and add for every project month a row
            foreach ($project as $row) {
                $data[$row['userId']]['userName'] = $row['userName'];
                $data[$row['userId']]['demandeUserType'] = $row['demandeUserType'];
                $data[$row['userId']]['jobTitle'] = $row['jobTitle'];
                $data[$row['userId']]['actualJobTitle'] = $row['actualJobTitle'];
                $data[$row['userId']]['demandeUserId'] = $row['demandeUserId'];
                $period = $row['capacityPeriode'];
                if (null !== $period) {
                    $period = $period->format('Y-m');
                }
                $month = 0;
                while ($month <= $validationDate) {
                    if ($period == date('Y-m', strtotime($techDate . '+' . $month . ' month'))) {
                        $data[$row['userId']][$month] = $row;
                        $dateVar = date('Y-m-01', strtotime($techDate . '+' . $month . ' month'));
                        $totalCharge = $em->createQueryBuilder('query')
                                        ->select('SUM(ch.hours) alocatedHours, SUM(ch.hoursWorked) totalHoursWorked')
                                        ->from('SuiviEtudesBundle:ProjetCharge', 'ch')
                                        ->where('ch.period = :period')
                                        ->andWhere('ch.userId = :userId')
                                        ->andWhere('ch.demandeId != :demandeId')
                                        ->setParameter('period', $dateVar)
                                        ->setParameter('userId', $row['userId'])
                                        ->setParameter('demandeId', $row['id'])
                                        ->getQuery()->getSingleResult();
                        $data[$row['userId']][$month]['alocatedHours'] = $totalCharge['alocatedHours'];
                        $data[$row['userId']][$month]['totalHoursWorked'] = (int) $totalCharge['totalHoursWorked'];
                    }
                    $month++;
                }
            }

            return $this->render('SuiviEtudesBundle:Projet:planification.html.twig', array(
                        'project' => $data,
                        'rows' => count($data),
                        'demande' => $demande,
                        'validationDate' => (int) $validationDate,
                        'demandeTech' => $demandeTech,
                        'demandeCP' => $demandeCP,
                        'techDate' => $techDate,
                        'workingTechs' => $workingTechs
            ));
        } else {
            return $this->render('SuiviEtudesBundle:Projet:planification.html.twig', array(
                        'demande' => $demande,
                        'demandeTech' => $demandeTech,
                        'demandeCP' => $demandeCP,
                        'workingTechs' => $workingTechs
            ));
        }
    }

    /**
     * @Route("/projet/ajouter-charge", name="suivi_etudes_admin_project_add_charge")
     * @Method({"POST"})
     */
    public function addPredictedChargeXHR() {
        $request = $this->container->get('request');
        $projectId = $request->request->get('pk');
        $period = substr($request->request->get('period'), 0, 8) . "01";
        $demandeUserId = $request->request->get('demandeuserid');
        $userId = $request->request->get('userid');
        $value = $request->request->get('value');
        $oldVal = $request->request->get('oldVal');

        $em = $this->getDoctrine()->getManager();
        $demandeUser = $em->getRepository('SuiviEtudesBundle:DemandeUser')->find($demandeUserId);
        $demande = $em->getRepository('SuiviEtudesBundle:Demande')->find($projectId);
        $tax = $em->getRepository('SuiviEtudesBundle:ProjetTauxHoraire')->findOneBy(array('userId' => $userId, 'period' => $period));
        $charge = $em->getRepository('SuiviEtudesBundle:ProjetCharge')->findOneBy(array('demandeId' => $projectId, 'period' => $period, 'userId' => $userId));

        $capacityRow = $em->createQuery('SELECT ch.demandeId,ch.hours chargeHours,cap.hours totalHours,cap.capacity percent'
                        . ',(SELECT sum(ch2.hours) FROM SuiviEtudesBundle:ProjetCharge ch2 WHERE ch2.period = :period AND ch2.userId = :userId AND ch2.demandeId != :projectId) allocatedHours'
                        . ' FROM SuiviEtudesBundle:ProjetUserCapacite cap'
                        . ' LEFT JOIN SuiviEtudesBundle:ProjetCharge ch WITH ch.userId = cap.userId AND ch.period = cap.period'
                        . ' WHERE cap.userId = :userId'
                        . ' AND cap.period = :period')
                ->setParameter('userId', $userId)
                ->setParameter('period', $period)
                ->setParameter('projectId', $projectId)
                ->getResult();
        // no row in charge table
        if (round($capacityRow[0]['totalHours'] / 100 * $capacityRow[0]['percent']) < $value + $capacityRow[0]['allocatedHours']) {
            return new Response(0);
        }
        if (null == $charge) {
            $charge = new \Suivi\EtudesBundle\Entity\ProjetCharge();
        }

        $charge->setDemandeUserId($demandeUserId);
        $charge->setDemandeUser($demandeUser);
        $charge->setDemandeId($projectId);
        $charge->setPeriod($period);
        $charge->setTax($tax);
        $charge->setHours($value);
        $charge->setDemande($demande);
        $em->persist($charge);
        try {
            $em->flush();
        } catch (\Doctrine\DBAL\DBALException $e) {
            return new Response('Erreur.');
        }
        return new Response('Terminer');
    }

    /**
     * @Route("/projet/ajouter-charge-realisee", name="suivi_etudes_admin_project_add_realised_charge")
     * @Method({"POST"})
     */
    public function addRealisedChargeXHR() {
        // parameters from request
        $request = $this->container->get('request');
        $chargeId = $request->request->get('chargeid');
        $value = (int) $request->request->get('value');
        $userId = $request->request->get('userid');
        $period = $request->request->get('period');
        $demandeId = $request->request->get('pk');
        // db operations
        $em = $this->getDoctrine()->getManager();
        $charge = $em->getRepository('SuiviEtudesBundle:ProjetCharge')->find($chargeId);
        $capacity = $em->getRepository('SuiviEtudesBundle:ProjetUserCapacite')->findOneBy(
                array(
                    'userId' => $userId,
                    'period' => new \DateTime($period)
                )
        );
        $capacityRow = $em->createQuery('SELECT cap.hours capHours,ch.demandeId,ch.hoursWorked chargeHours'
                        . ',(SELECT sum(ch2.hoursWorked) FROM SuiviEtudesBundle:ProjetCharge ch2 WHERE ch2.period = :period '
                        . 'AND ch2.userId = :userId AND ch2.demandeId != :projectId) allocatedHours'
                        . ' FROM SuiviEtudesBundle:ProjetUserCapacite cap'
                        . ' LEFT JOIN SuiviEtudesBundle:ProjetCharge ch WITH ch.userId = cap.userId AND ch.period = cap.period and ch.demande = :projectId'
                        . ' WHERE cap.userId = :userId'
                        . ' AND cap.period = :period')
                ->setParameter('userId', $userId)
                ->setParameter('period', $period)
                ->setParameter('projectId', $demandeId)
                ->getSingleResult();

        if (null == $charge) {
            $demandeUser = $em->createQueryBuilder('query')
                            ->select('du')
                            ->from('SuiviEtudesBundle:DemandeUser', 'du')
                            ->where('du.demandeId= :demandeId')
                            ->andWhere('du.userId= :userId')
                            ->andWhere("du.type != 'OBSERVATEUR'")
                            ->andWhere("du.type != 'DEMANDEUR'")
                            ->setParameter('demandeId', $demandeId)
                            ->setParameter('userId', $userId)
                            ->getQuery()->getSingleResult();

            $tax = $em->getRepository('SuiviEtudesBundle:ProjetTauxHoraire')->findOneBy(array('userId' => $userId, 'period' => $period));
            $demande = $em->getRepository('SuiviEtudesBundle:Demande')->find($demandeId);

            $charge = new \Suivi\EtudesBundle\Entity\ProjetCharge();
            $charge->setDemandeUserId($demandeUser->getId());
            $charge->setDemandeUser($demandeUser);
            $charge->setDemandeId($demandeId);
            $charge->setPeriod(new \DateTime($period));
            $charge->setTax($tax);
            $charge->setDemande($demande);
            if (null == $charge->getHours()) {
                $charge->setHours('0');
            }
        }
        if ($value <= $capacityRow['capHours'] - $capacityRow['allocatedHours']) {
            $charge->setHoursWorked($value);
        }

        if ($capacityRow['allocatedHours'] + $value > $capacityRow['capHours']) {
            return new Response(0);
        }

        $em->persist($charge);
        try {
            $em->flush();
        } catch (\Doctrine\DBAL\DBALException $e) {
            return new Response('Erreur.');
        }
        return new Response('Terminer');
    }

    /**
     * @Route("/demande/pdf/{id}", name="suivi_etudes_admin_demande_pdf")
     * @Method({"GET"})
     */
    public function voir($id) {
        $em = $this->getDoctrine()->getManager();
        $demande = $em->getRepository('SuiviEtudesBundle:Demande')->find($id);
        //pdf generation
        $facade = $this->get('ps_pdf.facade');
        $response = new Response();
        $this->render('SuiviEtudesBundle:Demande:voir.pdf.twig', array(
            "demande" => $demande,
                ), $response);
        $page = $response->getContent();
        $content = $facade->render($page);

        return new Response($content, 200, array('content-type' => 'application/pdf'));
        //end generation
    }

    /**
     * @Route("/demande/supprimer-pice-jointe/{id}/{demandeId}", name="suivi_etudes_remove_attachament" ,requirements={"id": "\d+","demandeId": "\d+"})
     * @Method({"POST"})
     */
    public function removeAtachamentXHR($id, $demandeId) {
        $em = $this->getDoctrine()->getManager();
        $attachament = $em->getRepository('SuiviEtudesBundle:PieceJointe')->find($id);
        // get attachement path
        $file = $this->getParameter('assetic.write_to'). DIRECTORY_SEPARATOR .'uploads'
                .DIRECTORY_SEPARATOR.'attachments'.DIRECTORY_SEPARATOR.$attachament->getDemande()->getId().DIRECTORY_SEPARATOR.$attachament->getName();
        $em->remove($attachament);
        // remove devis
        if ($attachament->getDemande()->getDevis()->getId() == $attachament->getId()) {
            $demande = $em->getRepository('SuiviEtudesBundle:Demande')->find($attachament->getDemande()->getId());
            $demande->removeDevis();
            // remove session file from HDD
            $em->persist($demande);
        }
        try {
            @unlink($file);
            $em->flush();
        } catch (\Doctrine\DBAL\DBALException $e) {
            return new Response('Erreur.');
        }
        return new Response('Success.');
    }

//    /**
//     * @Route("/projet/charge-de-travail/{period}/", name="suivi_etudes_admin_project_work_load", requirements={"period": "[0-9]{4}[-]{1}[0-9]{2}"})
//     * @Method({"GET"})
//     */
//    public function workLoadAction($period) {
//        $managerId = $this->getUser()->getId();
//        $em = $this->getDoctrine()->getManager();
//        $data = $em->getRepository('SuiviEtudesBundle:ProjectTech')->getChargeDeTravail($period, $managerId);
//        return $this->render('SuiviEtudesBundle:Projet:charge_de_travail.html.twig', array(
//                    'charge' => $data,
//                    'period' => $period,
//        ));
//    }

    /**
     * @Route("/projet/graphique/demande/{id}/", name="suivi_etudes_admin_project_mois_graphique")
     * @Method({"GET"})
     */
    public function chartProject($id) {
        $em = $this->getDoctrine()->getManager();
        $projectCharge = $em->createQueryBuilder('query')
                        ->select('c.userId', 'CONCAT(user.surname,\' \',user.givenname) userName', 'COALESCE(SUM(c.hoursWorked),0) hoursReal', 'COALESCE(SUM(c.hours),0) hoursPrev', 'c.period')
                        ->from('SuiviEtudesBundle:ProjetCharge', 'c')
                        ->leftJoin('UserUserBundle:User', 'user', 'WITH', 'user.id=c.userId')
                        ->leftJoin('SuiviEtudesBundle:ProjectTech', 'pu', 'WITH', 'c.userId=IDENTITY(pu.tech)')
                        ->where('c.demandeId = :id')
                        ->setParameter('id', $id)
                        ->groupBy('c.userId')
                        ->orderBy('user.surname')
                        ->getQuery()->getResult();

        //balanceByProfesion
        $data = $em->createQueryBuilder('query')
                        ->select('d.titre', 'CONCAT(user.surname,\' \',user.givenname) name', 'c.period', 'c.hours prevHours', 'c.hoursWorked'
                                , 't.jobTitle', 't.tauxHoraire')
                        ->from('SuiviEtudesBundle:ProjetCharge', 'c')
                        ->leftJoin('UserUserBundle:User', 'user', 'WITH', 'user.id=c.userId')
                        ->leftJoin('SuiviEtudesBundle:ProjectTech', 'pu', 'WITH', 'c.userId=IDENTITY(pu.tech)')
                        ->leftJoin('SuiviEtudesBundle:Demande', 'd', 'WITH', 'd.id=c.demandeId')
                        ->leftJoin('SuiviEtudesBundle:ProjetTauxHoraire', 't', 'WITH', 't.userId=c.userId and t.period=c.period')
                        ->where('c.demandeId = :id')
                        ->setParameter('id', $id)
                        ->orderBy('user.surname')
                        ->addOrderBy('c.period', 'ASC')
                        ->getQuery()->getResult();
        $balanceByProfesion = array();
        if (!empty($data)) {
            foreach ($data as $row) {
                $monthName = date('M', strtotime($row['period'])); # set month name
                $balanceByProfesion[$row['name']][$monthName]['ecartEuro'] = ($row['prevHours'] * $row['tauxHoraire']) - ($row['hoursWorked'] * $row['tauxHoraire']);
                $balanceByProfesion[$row['name']][$monthName]['ecartH'] = null !== $row['prevHours'] ? $row['prevHours'] : 0;
                $balanceByProfesion[$row['name']][$monthName]['cumulEuro'] = $row['hoursWorked'] * $row['tauxHoraire'];
            }
        }
        return $this->render('SuiviEtudesBundle:Projet:chart_projet.html.twig', array(
                    'projectCharge' => $projectCharge,
                    'id' => $id,
                    'balanceByProfesion' => $balanceByProfesion
        ));
    }

    /**
     * @Route("/projet/techProjects/{month}", name="suivi_etudes_admin_tech_projects")
     * @Method({"GET"})
     */
    public function projectsForTechAction(Request $request, $month = 1) {
        $user = $this->getUser();
        if ($user->isGranted('ROLE_SET_TECH')) {
            $em = $this->getDoctrine()->getManager();
            $viewVariables = array();
            $techDemandes = $em->getRepository('SuiviEtudesBundle:Demande')->getDemandesForTechnician($user->getId());
            $viewVariables['techProjectsLastMonth'] = $techDemandes['techProjectsLastMonth'];
            $viewVariables['techProjectsCurrentMonth'] = $techDemandes['techProjectsCurrentMonth'];
            $viewVariables['month'] = $month;
            $viewVariables['techProjectsLastMonthData'] = $techDemandes['techProjectsLastMonthData'];
            $viewVariables['techProjectsCurrentMonthData'] = $techDemandes['techProjectsCurrentMonthData'];
            return $this->render('SuiviEtudesBundle:Projet:tech_projects.html.twig', $viewVariables);
        }
        return $this->redirect($this->generateUrl('suivi_etudes_accueil'));
    }

}
