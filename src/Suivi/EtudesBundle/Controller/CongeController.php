<?php

namespace Suivi\EtudesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class CongeController extends Controller {

    public function sendEmail($to, $subject, $params, $layout) {
        $from = 'poleweb@noz.fr';
        if ($this->container->get('kernel')->getEnvironment() != 'prod') {
            $subject = "TEST " . $subject;
            $params['testEnv'] = true;
        }
        $message = \Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom($from)
                ->setTo($to)
                ->setBody($this->renderView(
                        $layout, $params
                ), 'text/html')
        ;
        $this->get('mailer')->send($message);
    }

//    /**
//     * @Route("/tech/{id}/{absenceId}", name="suivietudes_absence_by_id", defaults={"id":0,"absenceId":0})
//     * @Method({"GET", "POST"})
//     * @Template()
//     */
//    public function absenceAddAction(Request $request, $id = 0, $absenceId) {
//        # variables
//        $userId = $this->getUser()->getId();
//        # check if user is manager tech
//        $isTechManager = false;
//        foreach ($this->getUser()->getRoles() as $role) {
//            if ($role == 'ROLE_SET_MANAGER_TECH') {
//                $isTechManager = true;
//            }
//        }
//        $hasInfo = false;
//        $em = $this->getDoctrine()->getManager();
//        $tech = $em->getRepository('SuiviEtudesBundle:ProjectTech')->findOneBy(array('tech' => $userId));
//        # edit absence
//        $row = $em->getRepository('SuiviEtudesBundle:AbsenceList')->find($absenceId);
//        # restrict access to an absence that is not created by the current technician
//        if (!empty($row)) {
//            $techId = $row->getTech()->getTech()->getId();
//            if ($techId != $userId) {
//                return $this->render('ExceptionBundle:Exception:404.html.twig', array(
//                            'homeUrl' => $this->generateUrl('suivi_etudes_accueil')
//                ));
//            }
//        }
//
//        # create absence if no id is provided in the url
//        if (empty($row)) {
//            $row = new \Suivi\EtudesBundle\Entity\AbsenceList();
//            $row->setTech($tech);
//        }
//
//        # create form and add $roleManager variable to form
//        $form = $this->createForm(new \Suivi\EtudesBundle\Form\Absence\AbsenceByTechType($isTechManager), $row);
//        $form->handleRequest($request);
//
//        # if request is POST and form is valid
//        if ($request->isMethod('POST') && $form->isValid()) {
//            # if years are different then ask the user to create separate absence requests
//            $startYear = $row->getStartDate()->format('Y');
//            $endYear = $row->getEndDate()->format('Y');
//            if (empty($startYear) || empty($endYear) || $endYear != $startYear) {
//                $this->get('session')->getFlashBag()->add('warning', 'Votre demande d\'absence est dans des années différentes. '
//                        . 'Si tel est le cas s\'il vous plaît faire deux demandes distinctes, une pour chaque année.');
//                return $this->redirect($this->generateUrl('suivietudes_absence_by_id', array('id' => $id)));
//            }
//
//            $techInfo = $tech->getTechInfo();
//            $startDate = new \Datetime($request->request->get('startDate'));
//            foreach ($techInfo as $info) {
//                if ($info->getYear() == $startDate->format('Y')) {
//                    $hasInfo = true;
//                }
//            }
//            # if the technician doesn't have informations for current year then redirect with message
//            if ($hasInfo == false) {
//                $this->get('session')->getFlashBag()->add('info', 'Vous n\'avez pas d\'info pour année ' . $startDate->format('Y') . '.Contactez votre manager de mise à jour votre informations.');
//                return $this->redirect($this->generateUrl('suivietudes_absence_list_by_tech', array('techId' => $userId)));
//            }
//
//            # if the absence type is substract
//            if ($row->getType()->getSubstract() == 1) {
//                $days = $request->request->get('days');
//                $daysLeft = $info->getDaysLeft();
//                if ($days > $daysLeft) {
//                    $this->get('session')->getFlashBag()->add('warning', 'Le nombre de jours est plus grand que'
//                            . ' le nombre de jours disponibles pur vous. Vous avez ' . $daysLeft . ' jours à gauche.');
//                    return $this->redirect($this->generateUrl('suivietudes_absence_by_id', array('id' => $id)));
//                }
//            }
//
//            # get managers emails
//            $managersMails = array();
//            foreach ($tech->getPole()->getManager() as $manager) {
//                array_push($managersMails, $manager->getManager()->getEmail());
//            }
//            # save new absence
//            $em->persist($row);
//            try {
//                $em->flush();
//                $this->sendEmail($managersMails, 'Nouvelle requête de congés', array('type' => 'new', 'tech' => $tech), 'SuiviEtudesBundle:Conge:email.html.twig');
//                $this->get('session')->getFlashBag()->add('success', 'Le changement a été effectué avec succès.');
//            } catch (\Doctrine\DBAL\DBALException $e) {
//                $this->get('session')->getFlashBag()->add('error', 'Ups...There was an error.');
//            }
//
//            return $this->redirect($this->generateUrl('suivietudes_absence_by_id', array('id' => $id)));
//        }
//        return array(
//            'form' => $form->createView(),
//            'tech' => $tech
//        );
//    }

//    /**
//     * @Route("/recuperation/{id}/{absenceId}", name="suivietudes_absence_recuperation_by_id", defaults={"id":0,"absenceId":0})
//     * @Method({"GET", "POST"})
//     * @Template()
//     */
//    public function recuperationHoursAction(Request $request, $id = 0, $absenceId) {
//        # variables
//        $userId = $this->getUser()->getId();
//        $hasInfo = false;
//        $em = $this->getDoctrine()->getManager();
//        $tech = $em->getRepository('SuiviEtudesBundle:ProjectTech')->findOneBy(array('tech' => $userId));
//        # edit absence
//        $row = $em->getRepository('SuiviEtudesBundle:RecuperationList')->find($absenceId);
//        # restrict access to an absence that is not created by the current technician
//        if (!empty($row)) {
//            $techId = $row->getTech()->getTech()->getId();
//            if ($techId != $userId) {
//                return $this->render('ExceptionBundle:Exception:404.html.twig', array(
//                            'homeUrl' => $this->generateUrl('suivi_etudes_accueil')
//                ));
//            }
//        }
//
//        # create absence if no id is provided in the url
//        if (empty($row)) {
//            $row = new \Suivi\EtudesBundle\Entity\RecuperationList();
//            $row->setTech($tech);
//        }
//
//        # create form and add $roleManager variable to form
//        $form = $this->createForm(new \Suivi\EtudesBundle\Form\Absence\RecuperationByTechType(), $row);
//        $form->handleRequest($request);
//
//        # if request is POST and form is valid
//        if ($request->isMethod('POST') && $form->isValid()) {
//            $techInfo = $tech->getTechInfo();
//
//            $startDate = new \Datetime($request->request->get('startDate'));
//            foreach ($techInfo as $info) {
//                if ($info->getYear() == $startDate->format('Y')) {
//                    $hasInfo = true;
//                }
//            }
//
//            # if the technician doesn't have informations for current year then redirect with message
//            if ($hasInfo == false) {
//                $this->get('session')->getFlashBag()->add('info', 'Vous n\'avez pas d\'info pour année ' . $startDate->format('Y') . '.Contactez votre manager de mise à jour votre informations.');
//                return $this->redirect($this->generateUrl('suivietudes_absence_list_by_tech', array('techId' => $userId)));
//            }
//
//            # if the absence type is substract
//            if ($row->getType()->getSubstract() == 2) {
//                $hours = $request->request->get('hours');
//                $hoursLeft = $info->getRecHours();
//                if ($hours > $hoursLeft) {
//                    $this->get('session')->getFlashBag()->add('warning', 'Le nombre de heures est plus grand que'
//                            . ' le nombre de heures disponibles pour vous. Vous avez ' . $hoursLeft . ' heures à gauche.');
//                    return $this->redirect($this->generateUrl('suivietudes_absence_recuperation_by_id', array('id' => $id)));
//                }
//            }
//
//            # get managers emails
//            $managersMails = array();
//            foreach ($tech->getPole()->getManager() as $manager) {
//                array_push($managersMails, $manager->getManager()->getEmail());
//            }
//
//
//            # save new absence
//            $em->persist($row);
//            try {
//                $em->flush();
//                // schimba mail
//                $this->sendEmail($managersMails, 'Nouvelle requête de congés', array('type' => 'new', 'tech' => $tech), 'SuiviEtudesBundle:Conge:email.html.twig');
//                $this->get('session')->getFlashBag()->add('success', 'Le changement a été effectué avec succès.');
//            } catch (\Doctrine\DBAL\DBALException $e) {
//                $this->get('session')->getFlashBag()->add('error', 'Ups...There was an error.');
//            }
//
//            return $this->redirect($this->generateUrl('suivietudes_absence_recuperation_by_id', array('id' => $id)));
//        }
//        return array(
//            'form' => $form->createView(),
//            'tech' => $tech
//        );
//    }

//    /**
//     * @Route("/pour-tech", name="suivietudes_absence_for_tech")
//     * @Method({"GET", "POST"})
//     * @Template()
//     */
//    public function absenceAddForTechAction(Request $request) {
//        if (false === $this->get('security.context')->isGranted('ROLE_SET_MANAGER_TECH')) {
//            return $this->render('ExceptionBundle:Exception:404.html.twig', array(
//                        'homeUrl' => $this->generateUrl('suivi_etudes_accueil')
//            ));
//        }
//        $userId = $this->getUser()->getId();
//        $em = $this->getDoctrine()->getManager();
//        $row = new \Suivi\EtudesBundle\Entity\AbsenceList();
//        $form = $this->createForm(new \Suivi\EtudesBundle\Form\Absence\AbsenceForTechType($em, $userId), $row);
//        $form->handleRequest($request);
//        # if request is POST and form is valid
//        if ($request->isMethod('POST') && $form->isValid()) {
//            $techId = $form['tech']->getData()->getTech()->getId();
//            $tech = $em->getRepository('SuiviEtudesBundle:ProjectTech')->findOneBy(array('tech' => $techId));
//            $hasInfo = false;
//            $startDate = new \Datetime($request->request->get('startDate'));
//            $techInfo = $tech->getTechInfo();
//            foreach ($techInfo as $info) {
//                if ($info->getYear() == $startDate->format('Y')) {
//                    $hasInfo = true;
//                }
//            }
//            # if the technician doesn't have informations for current year then redirect with message
//            if ($hasInfo == false) {
//                $this->get('session')->getFlashBag()->add('info', 'No info pour année ' . $startDate->format('Y') . ' pour cet technicien.');
//                return $this->redirect($this->generateUrl('suivietudes_absence_for_tech'));
//            }
//            # if years are different then ask the user to create separate absence requests
//            $startYear = $row->getStartDate()->format('Y');
//            $endYear = $row->getEndDate()->format('Y');
//            if (empty($startYear) || empty($endYear) || $endYear != $startYear) {
//                $this->get('session')->getFlashBag()->add('warning', 'Le demande d\'absence est dans des années différentes. '
//                        . 'Si tel est le cas s\'il vous plaît faire deux demandes distinctes, une pour chaque année.');
//                return $this->redirect($this->generateUrl('suivietudes_absence_for_tech'));
//            }
//            # if the absence type is substract
//            if ($row->getType()->getSubstract() == 1) {
//                $days = $request->request->get('days');
//                $daysLeft = $info->getDaysLeft();
//                if ($days > $daysLeft) {
//                    $this->get('session')->getFlashBag()->add('warning', 'Le nombre de jours est plus grand que'
//                            . ' le nombre de jours disponibles pur le technicien. ' . $daysLeft . ' jours à gauche.');
//                    return $this->redirect($this->generateUrl('suivietudes_absence_for_tech'));
//                }
//                // update tech info
//                $techInfo = $em->getRepository('SuiviEtudesBundle:AbsenceTechInfo')->findBy(array('tech' => $row->getTech()));
//                $startYear = $row->getStartDate()->format('Y');
//                foreach ($techInfo as $info) {
//                    $year = $info->getYear();
//                    if ($startYear == $year) {
//                        $remainingDays = $info->getVacationDays() - $row->getDays();
//                        $info->setDaysLeft($remainingDays);
//                        $em->persist($info);
//                    }
//                }
//            }
//            #validate request
//            $row->setValidatedAt(new \DateTime('now'));
//            $row->setValidatedBy($userId);
//            $manager = $em->getRepository('UserUserBundle:User')->find($userId);
//            $row->setManager($manager);
//            $em->persist($row);
//            try {
//                $em->flush();
//                $this->get('session')->getFlashBag()->add('success', 'Le changement a été effectué avec succès.');
//            } catch (\Doctrine\DBAL\DBALException $e) {
//                $this->get('session')->getFlashBag()->add('error', 'Ups...There was an error.');
//            }
//            return $this->redirect($this->generateUrl('suivietudes_absence_for_tech'));
//        }
//        return array(
//            'form' => $form->createView(),
//        );
//    }

    /**
     * @Route("/tech-country/{techId}", name="suivietudes_absence_tech_country")
     * @Method({"GET"})
     */
    public function absenceTechCountry($techId) {
        $em = $this->getDoctrine()->getManager();
        $tech = $em->getRepository('SuiviEtudesBundle:ProjectTech')->find($techId);
        $techId = null;
        if (null !== $tech->getCountry()->getShort()) {
            $techId = $tech->getCountry()->getShort();
        }
        return new Response($techId);
    }

    /**
     * @Route("/list/{techId}/{year}", name="suivietudes_absence_list_by_tech")
     * @Method({"GET"})
     * @Template()
     */
    public function absenceListByTechAction($techId = null, $year = null) {
        $userId = $this->getUser()->getId();
        if (!$year) {
            $year = date('Y');
        }
        /* techs see only theirs vacation days */
        if (false === $this->get('security.context')->isGranted('ROLE_SET_TECH') || ($techId != $userId)) {
            return $this->render('ExceptionBundle:Exception:404.html.twig', array(
                        'homeUrl' => $this->generateUrl('suivi_etudes_accueil')
            ));
        }
        $em = $this->getDoctrine()->getManager();
        $techInfo = $em->getRepository('SuiviEtudesBundle:AbsenceTechInfo')->findBy(array('tech' => $techId, 'year' => $year));
        $totalDays = !empty($techInfo) ? $techInfo[0]->getVacationDays() : 0;
        $recHours = !empty($techInfo) ? $techInfo[0]->getRecHours() : 0;
        $ramainingDays = !empty($techInfo) ? $techInfo[0]->getDaysLeft() : 0;
        return array(
            'techId' => $techId,
            'year' => $year,
            'totalDays' => $totalDays,
            'ramainingDays' => $ramainingDays,
            'recHours' => $recHours,
            'year' => $year,
        );
    }

    /**
     * @Route("/list-ajax/{techId}/{year}", name="suivietudes_absence_list_by_tech_ajax")
     * @Method({"GET"})
     */
    public function absenceListByTechActionXHR(Request $request, $techId = null, $year = null) {
        if (!$request->isXmlHttpRequest()) {
            return $this->render('ExceptionBundle:Exception:404.html.twig', array(
                        'homeUrl' => $this->generateUrl('suivi_etudes_accueil')
            ));
        }
        $em = $this->getDoctrine()->getManager();
        $list = $em->getRepository('SuiviEtudesBundle:AbsenceList')->findAllByTechAndYear($techId, $year);
        return new JsonResponse($list);
    }

//    /**
//     * @Route("/recuperation-list/{techId}/{year}", name="suivietudes_absence_recuperation_list_by_tech")
//     * @Method({"GET"})
//     * @Template()
//     */
//    public function recuperationListByTechAction($techId = null, $year = null) {
//        $userId = $this->getUser()->getId();
//        if (!$year) {
//            $year = date('Y');
//
//        }
//        /* techs see only theirs vacation days */
//        if (false === $this->get('security.context')->isGranted('ROLE_SET_TECH') || ($techId != $userId)) {
//            return $this->render('ExceptionBundle:Exception:404.html.twig', array(
//                        'homeUrl' => $this->generateUrl('suivi_etudes_accueil')
//            ));
//        }
//        $em = $this->getDoctrine()->getManager();
//        $techInfo = $em->getRepository('SuiviEtudesBundle:AbsenceTechInfo')->findBy(array('tech' => $techId, 'year' => $year));
//
//        $totalDays = !empty($techInfo) ? $techInfo[0]->getVacationDays() : 0;
//        $recHours = !empty($techInfo) ? $techInfo[0]->getRecHours() : 0;
//        $ramainingDays = !empty($techInfo) ? $techInfo[0]->getDaysLeft() : 0;
//        return array(
//            'techId' => $techId,
//            'year' => $year,
//            'totalDays' => $totalDays,
//            'ramainingDays' => $ramainingDays,
//            'recHours' => $recHours,
//            'year' => $year,
//        );
//    }

    /**
     * @Route("/list-recuperation-ajax/{techId}/{year}", name="suivietudes_absence_recuperation_list_by_tech_ajax")
     * @Method({"GET"})
     */
    public function recuperationListByTechActionXHR(Request $request, $techId = null, $year = null) {
        if (!$request->isXmlHttpRequest()) {
            return $this->render('ExceptionBundle:Exception:404.html.twig', array(
                        'homeUrl' => $this->generateUrl('suivi_etudes_accueil')
            ));
        }
        $em = $this->getDoctrine()->getManager();

        $list = $em->getRepository('SuiviEtudesBundle:RecuperationList')->findAllByTechAndYear($techId, $year);


        return new JsonResponse($list);
    }

    /**
     * @Route("/tech-absence/cancel/{id}", name="suivietudes_absence_cancel")
     * @Method("POST")
     */
    public function absenceCancelAction(Request $request, $id = 0) {
        $techId = $this->getUser()->getId();
        $em = $this->getDoctrine()->getManager();
        $row = $em->getRepository('SuiviEtudesBundle:AbsenceList')->find($id);
        if ($row && $row->getCanceled() == 0) {
            $userId = $row->getTech()->getTech()->getId();
            // only the tech that created the absence can cancel it
            if ($techId != $userId) {
                return $this->render('ExceptionBundle:Exception:404.html.twig', array(
                            'homeUrl' => $this->generateUrl('suivi_etudes_accueil')
                ));
            }
            $row->setCanceled(1);
            $em->persist($row);
            try {
                $em->flush();
                $this->get('session')->getFlashBag()->add('success', 'Le changement a été effectué avec succès.');
            } catch (\Doctrine\DBAL\DBALException $e) {
                $this->get('session')->getFlashBag()->add('error', 'Ups...There was an error.');
            }
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Ups...There was an error.');
        }
        return new Response('Done');
    }

    /**
     * @Route("/tech-recuperation/cancel/{id}", name="suivietudes_absence_recuperation_cancel")
     * @Method("POST")
     */
    public function recuperationCancelAction(Request $request, $id = 0) {
        $techId = $this->getUser()->getId();
        $em = $this->getDoctrine()->getManager();
        $row = $em->getRepository('SuiviEtudesBundle:RecuperationList')->find($id);
        if ($row && $row->getCanceled() == 0) {
            $userId = $row->getTech()->getTech()->getId();
            // only the tech that created the absence can cancel it
            if ($techId != $userId) {
                return $this->render('ExceptionBundle:Exception:404.html.twig', array(
                            'homeUrl' => $this->generateUrl('suivi_etudes_accueil')
                ));
            }
            $row->setCanceled(1);
            $em->persist($row);
            try {
                $em->flush();
                $this->get('session')->getFlashBag()->add('success', 'Le changement a été effectué avec succès.');
            } catch (\Doctrine\DBAL\DBALException $e) {
                $this->get('session')->getFlashBag()->add('error', 'Ups...There was an error.');
            }
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Ups...There was an error.');
        }
        return new Response('Done');
    }

//    /**
//     * @Route("/liste-par-departament/{year}", name="suivietudes_absence_list_by_departament")
//     * @Method({"GET"})
//     * @Template()
//     */
//    public function absenceListByDepartamentAction(Request $request, $year = null) {
//        /* Security for it managers */
//        if (false === $this->get('security.context')->isGranted('ROLE_SET_MANAGER_TECH')) {
//            return $this->render('ExceptionBundle:Exception:404.html.twig', array(
//                        'homeUrl' => $this->generateUrl('suivi_etudes_accueil')
//            ));
//        }
//        $managerId = $this->getUser()->getId();
//        if (!$year) {
//            $year = date('Y');
//        }
//        $em = $this->getDoctrine()->getManager();
//        $managerInfo = $em->getRepository('SuiviEtudesBundle:ProjectManager')->getTechsAbsencesByManager($managerId);
//        $form = $this->createForm(new \Suivi\EtudesBundle\Form\Absence\TimekeepingType($managerId));
//        return array(
//            'year' => $year,
//            'managerInfo' => $managerInfo,
//            'managerId' => $managerId,
//            'form' => $form->createView(),
//        );
//    }

    /**
     * @Route("/liste-par-departament-xhr/{year}", name="suivietudes_absence_list_by_departament_ajax")
     * @Method({"GET"})
     */
    public function absenceListByDepartamentActionXHR(Request $request, $year = null) {
        if (!$request->isXmlHttpRequest()) {
            return $this->render('ExceptionBundle:Exception:404.html.twig', array(
                        'homeUrl' => $this->generateUrl('suivi_etudes_accueil')
            ));
        }
        $managerId = $this->getUser()->getId();
        $em = $this->getDoctrine()->getManager();
        $poleIds = $em->getRepository('SuiviEtudesBundle:ProjectManager')->getPolIdsByManager($managerId);
        $list = $em->getRepository('SuiviEtudesBundle:AbsenceList')->findAllTechByPoleAndYear(implode(',', $poleIds), $year);
        return new JsonResponse($list);
    }

//    /**
//     * @Route("/liste-par-departament-et-mois", name="suivietudes_absence_list_by_departament_et_mois")
//     * @Method({"GET"})
//     * @Template()
//     */
//    public function absenceListByDepartamentAndMonthAction() {
//        /* Security for it managers */
//        if (false === $this->get('security.context')->isGranted('ROLE_SET_MANAGER_TECH')) {
//            return $this->render('ExceptionBundle:Exception:404.html.twig', array(
//                        'homeUrl' => $this->generateUrl('suivi_etudes_accueil')
//            ));
//        }
//        $managerId = $this->getUser()->getId();
//        $em = $this->getDoctrine()->getManager();
//        $managerInfo = $em->getRepository('SuiviEtudesBundle:ProjectManager')->getTechsAbsencesByManager($managerId);
//        return array('managerInfo' => $managerInfo);
//    }

    /**
     * @Route("/liste-par-departament-et-mois-xhr/{poleIds}", requirements={"poleIds":".+"}, name="suivietudes_absence_list_by_departament_et_mois_ajax")
     * @Method({"GET"})
     */
    public function absenceListByDepartamentAndMonthXHR(Request $request, $poleIds = null) {
        if (!$request->isXmlHttpRequest()) {
            return $this->render('ExceptionBundle:Exception:404.html.twig', array(
                        'homeUrl' => $this->generateUrl('suivi_etudes_accueil')
            ));
        }
        $em = $this->getDoctrine()->getManager();
        $list = $em->getRepository('SuiviEtudesBundle:AbsenceList')->findAllTechByPole($poleIds);
        return new JsonResponse($list);
    }

    /**
     * @Route("/tech-departament-xhr/{managerId}/{year}", name="suivietudes_absence_list_tech_by_departament_ajax")
     * @Method({"GET"})
     */
    public function listTechByDepartamentActionXHR(Request $request, $managerId = null, $year = null) {
        if (!$request->isXmlHttpRequest()) {
            return $this->render('ExceptionBundle:Exception:404.html.twig', array(
                        'homeUrl' => $this->generateUrl('suivi_etudes_accueil')
            ));
        }
        $em = $this->getDoctrine()->getManager();
        $managerInfo = $em->getRepository('SuiviEtudesBundle:ProjectManager')->getTechsByManager($managerId, $year);
        return new JsonResponse($managerInfo);
    }

    /**
     * @Route("/aprove/{id}", name="suivietudes_absence_aprove")
     * @Method("POST")
     */
    public function absenceAproveAction(Request $request, $id = 0) {
        $userId = $this->getUser()->getId();
        $em = $this->getDoctrine()->getManager();
        $row = $em->getRepository('SuiviEtudesBundle:AbsenceList')->find($id);
        $validatedBy = $row->getValidatedBy();
        if ($row && empty($validatedBy)) {
            $row->setValidatedBy($userId);
            $row->setValidatedAt(new \DateTime('now'));
            if ($row->getType()->getSubstract() == 1) {
                // Validation rules
                $techInfo = $em->getRepository('SuiviEtudesBundle:AbsenceTechInfo')->findBy(array('tech' => $row->getTech()));
                $startYear = $row->getStartDate()->format('Y');
                foreach ($techInfo as $info) {
                    $year = $info->getYear();
                    if ($startYear == $year) {
                        $remainingDays = $info->getVacationDays() - $row->getDays();
                        $info->setDaysLeft($remainingDays);
                        $em->persist($info);
                    }
                }
            }
            $em->persist($row);
            try {
                $this->sendEmail($row->getTech()->getTech()->getEmail(), 'Suivi requête de vacances', array(
                    'type' => 'approved', 'data' => $row
                        ), 'SuiviEtudesBundle:Conge:email.html.twig');
                $em->flush();
                $this->get('session')->getFlashBag()->add('success', 'Le changement a été effectué avec succès.');
            } catch (\Doctrine\DBAL\DBALException $e) {
                $this->get('session')->getFlashBag()->add('error', 'Ups...There was an error.');
            }
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Ups...There was an error.');
        }
        return new Response('Done');
    }

    /**
     * @Route("/recuperation-aprove/{id}", name="suivietudes_absence_recuperation_aprove")
     * @Method("POST")
     */
    public function recuperationAproveAction(Request $request, $id = 0) {
        $userId = $this->getUser()->getId();
        $em = $this->getDoctrine()->getManager();
        $row = $em->getRepository('SuiviEtudesBundle:RecuperationList')->find($id);
        $validatedBy = $row->getValidatedBy();
        if ($row && empty($validatedBy)) {
            $row->setValidatedBy($userId);
            $row->setValidatedAt(new \DateTime('now'));
            if ($row->getType()->getSubstract() == 2) {
                // Validation rules
                $techInfo = $em->getRepository('SuiviEtudesBundle:AbsenceTechInfo')->findBy(array('tech' => $row->getTech()));
                $startYear = $row->getStartDate()->format('Y');
                foreach ($techInfo as $info) {
                    $year = $info->getYear();
                    $remainingHors = $info->getRecHours() - $row->getHours();
                    $info->setRecHours($remainingHors);
                    $em->persist($info);
                }
            }
            $em->persist($row);
            try {
                $this->sendEmail($row->getTech()->getTech()->getEmail(), 'Suivi requête de vacances', array(
                    'type' => 'approved', 'data' => $row
                        ), 'SuiviEtudesBundle:Conge:email.html.twig');
                $em->flush();
                $this->get('session')->getFlashBag()->add('success', 'Le changement a été effectué avec succès.');
            } catch (\Doctrine\DBAL\DBALException $e) {
                $this->get('session')->getFlashBag()->add('error', 'Ups...There was an error.');
            }
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Ups...There was an error.');
        }
        return new Response('Done');
    }

    /**
     * @Route("/disaprove/{id}", name="suivietudes_absence_disaprove")
     * @Method("POST")
     */
    public function absenceDisaproveAction(Request $request, $id = 0) {
        $managerId = $this->getUser()->getId();
        $em = $this->getDoctrine()->getManager();
        $row = $em->getRepository('SuiviEtudesBundle:AbsenceList')->find($id);
        $reason = $request->request->get('reason');
        $validatedBy = $row->getValidatedBy();
        if ($row && empty($validatedBy)) {
            $row->setValidatedBy($managerId);
            $row->setValidatedAt(new \DateTime('now'));
            $row->setRefusReason($reason);
            $em->persist($row);
            try {
                $this->sendEmail($row->getTech()->getTech()->getEmail(), 'Suivi requête de vacances', array(
                    'type' => 'cancel', 'data' => $row
                        ), 'SuiviEtudesBundle:Conge:email.html.twig');
                $em->flush();
                $this->get('session')->getFlashBag()->add('success', 'Le changement a été effectué avec succès.');
            } catch (\Doctrine\DBAL\DBALException $e) {
                $this->get('session')->getFlashBag()->add('error', 'Ups...There was an error.');
            }
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Ups...There was an error.');
        }
        return new Response('Done');
    }

    /**
     * @Route("/recuperation-disaprove/{id}", name="suivietudes_absence_recuperation_disaprove")
     * @Method("POST")
     */
    public function recuperationDisaproveAction(Request $request, $id = 0) {
        $managerId = $this->getUser()->getId();
        $em = $this->getDoctrine()->getManager();
        $row = $em->getRepository('SuiviEtudesBundle:RecuperationList')->find($id);
        $reason = $request->request->get('reason');
        $validatedBy = $row->getValidatedBy();
        if ($row && empty($validatedBy)) {
            $row->setValidatedBy($managerId);
            $row->setValidatedAt(new \DateTime('now'));
            $row->setRefusReason($reason);
            $em->persist($row);
            try {
                $this->sendEmail($row->getTech()->getTech()->getEmail(), 'Suivi requête de vacances', array(
                    'type' => 'cancel', 'data' => $row
                        ), 'SuiviEtudesBundle:Conge:email.html.twig');
                $em->flush();
                $this->get('session')->getFlashBag()->add('success', 'Le changement a été effectué avec succès.');
            } catch (\Doctrine\DBAL\DBALException $e) {
                $this->get('session')->getFlashBag()->add('error', 'Ups...There was an error.');
            }
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Ups...There was an error.');
        }
        return new Response('Done');
    }

    /**
     * @Route("/gestion-en-heure-xhr/{year}", name="suivietudes_absence_gestion_en_heure_ajax")
     * @Method({"GET"})
     */
    public function gestionEnHeureActionXHR(Request $request, $year = null) {
        $managerId = $this->getUser()->getId();
        if (!$year) {
            $year = date('Y');
        }
        $em = $this->getDoctrine()->getManager();
        $poleIds = $em->getRepository('SuiviEtudesBundle:ProjectManager')->getPolIdsByManager($managerId);
        $recHours = $em->getRepository('SuiviEtudesBundle:RecuperationList')->findAllTechByPoleAndYear(implode(',', $poleIds), $year);
        return new JsonResponse($recHours);
    }

    /**
     * @Route("/export/{startDate}/{country}/{departament}"
     * ,defaults={"startDate":0,"country":0,"departament":0}
     * ,name="suivietudes_absence_export_timekeeping")
     * @Method({"GET"})
     */
    public function exportExcel($startDate, $country, $departament) {
        setlocale(LC_TIME, 'fr_FR.utf8', 'fra');
        $month = new \Datetime(date($startDate));
        /* Security for it managers */
        if (false === $this->get('security.context')->isGranted('ROLE_SET_MANAGER_TECH')) {
            return $this->render('ExceptionBundle:Exception:404.html.twig', array(
                        'homeUrl' => $this->generateUrl('suivi_etudes_accueil')
            ));
        }
        $managerId = $this->getUser()->getId();
        $em = $this->getDoctrine()->getManager();
        $poleIds = $em->getRepository('SuiviEtudesBundle:ProjectManager')->getPolIdsByManager($managerId);
        // remove departament if timesheet is just for certain departaments
        foreach ($poleIds as $k => $id) {
            if ($departament > 0 && $departament != $id) {
                unset($poleIds[$k]);
            }
        }
        $poles = $em->getRepository('SuiviEtudesBundle:ProjectPole')->getPoleById($poleIds);
        $techsInfo = $em->getRepository('SuiviEtudesBundle:ProjectTech')->timekeeping($startDate, $poleIds, $country, $departament);
        if (null !== $techsInfo) {
            $absTypeList = $em->getRepository('SuiviEtudesBundle:AbsenceType')->findAll(array(), array('name' => 'ASC')); #get all absence types with order
            $fillColor = 'b8b894';
            $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
            $sheet = $phpExcelObject->getActiveSheet();
            $borderStyle = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            );

            $sheet->setCellValue('A2', 'NOM Prénom Salarié');
            $sheet->mergeCells('A2:A4');
            $sheet->getStyle('A2:A4')->applyFromArray($borderStyle); #set borders
            $sheet->getColumnDimension('A')->setAutoSize(true); #set width for column A

            /* CREATE DAYS AND DAY NAME STARTING FROM FOR TABLE HEADER */
            #correction when number > 26 (letters of alphabet)
            $j = 0;
            $lastCell = 'Z';
            for ($i = 2; $i <= count($techsInfo['daysInfo']) + 1; $i++) { #start from B1
                $cell = chr(64 + $i);
                if ($i > 26) {
                    $cell = chr(64 + $i - 26 - $j) . chr(64 + $i - 26);
                    $j++;
                }
                $sheet->setCellValue($cell . 3, $techsInfo['daysInfo'][$i - 1])
                        ->setCellValue($cell . 4, $i - 1);
                #if weekend set color
                $maxRow = count($techsInfo['techsInfo']) + 4; #fix max no of rows 
                if ($techsInfo['daysInfo'][$i - 1] == 'S' || $techsInfo['daysInfo'][$i - 1] == 'D') {
                    $sheet->getStyle($cell . 3 . ":" . $cell . $maxRow)->getFill()
                            ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                            ->getStartColor()
                            ->setRGB($fillColor);
                    $totalRows = count($techsInfo['techsInfo']);
                }
                $sheet->getStyle("A2:" . $cell . $maxRow)->applyFromArray($borderStyle); #set borders for tech table
                $sheet->getColumnDimension($cell)->setWidth(3); #set width for days columns
                $lastCell = $cell;
            }

            // CALCULATE STARTING COLUMN FOR ABSENCE TYPE TABLE
            $splitLastCol = str_split($lastCell);
            $lastCol = $colAfterLast = null;
            // get letter if multiple , excepting last one
            for ($i = 0; $i < count($splitLastCol) - 1; $i++) {
                $lastCol = chr(ord($splitLastCol[$i]));
                $colAfterLast = chr(ord($splitLastCol[$i]));
            }
            $lastCol .= chr(ord(end($splitLastCol)) + 2); #this is the starting column for the absence type table
            $colAfterLast .= chr(ord(end($splitLastCol)) + 3); #this is the starting column for the absence type table
            // END CALCULATION FOR STARTING COLUMN OF ABSENCE TYPE TABLE
            // START CREATING ABSENCE TYPE TABLE
            $sheet->setCellValue($lastCol . 4, 'Légende');
            $sheet->getStyle($lastCol . 4)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getColumnDimension($lastCol)->setAutoSize(true); #set width for column with absence types
            $sheet->getStyle($lastCol . 4)->getFill() #set color for absence type table header
                    ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB('FF11AABB');
            $sheet->getStyle($colAfterLast . 4)->getFill() #set color for absence type table header
                    ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB('FF11AABB');
            for ($i = 0; $i < count($absTypeList); $i++) {
                $absTypeName = $absTypeList[$i]->getName();
                $absAbrev = $absTypeList[$i]->getAbbrev();
                $sheet->setCellValue($lastCol . (5 + $i), $absTypeName);
                $sheet->setCellValue($colAfterLast . (5 + $i), $absAbrev);
                $sheet->getStyle($colAfterLast . (5 + $i))->getFill()#set color for absence type abrev column
                        ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('FF11AABB');
            }
            setlocale(LC_TIME, "fr_FR");
            $sheet->setCellValue('B2', 'Departament(s): ' . $poles . ' - ' . strftime(" %B %Y", strtotime($month->format('Y-m-d'))));
            $sheet->mergeCells('B2:' . $lastCell . '2');

            #set tech infos by day
            $startRow = 5;
            foreach ($techsInfo['techsInfo'] as $tech => $daysInfo) {
                $sheet->setCellValue('A' . $startRow, $tech); #set tech name
                $j = 0;
                for ($i = 1; $i <= count($daysInfo['days']); $i++) {
                    $cell = chr(64 + $i + 1) . $startRow;
                    if ($i > 25) {
                        $cell = chr(64 + $i - 25 - $j) . chr(64 + $i - 25) . $startRow;
                        $j++;
                    }
                    #return element from array
                    $elem = array_slice($daysInfo['days'], ($i - 1), 1);
                    $type = array_values($elem);
                    $type = $type[0] == false ? '' : $type[0];
                    $getCollor = $sheet->getStyle($cell)->getFill()->getStartColor()->getARGB();
                    if ($getCollor == 'FF' . $fillColor) {
                        $type = '/'; # weekend cellc value
                    }
                    if ($type == 'JF') {
                        $sheet->getStyle($cell)->getFill()#set color for holliday (JF)
                                ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                                ->getStartColor()
                                ->setRGB($fillColor);
                    }
                    $sheet->setCellValue($cell, $type);
                }
                $startRow++;
            }
            //wrap text for this cells
            $sheet->getStyle('A1:A100')->getAlignment()->setWrapText(true);
            //Horizontal allign for this cells
            $sheet->getStyle('B1:AF100')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            //set title
            $title = new \DateTime($startDate);
            $phpExcelObject->getProperties()
                    ->setTitle($title->format('m.Y'));
            // set worksheet title
            $sheet->setTitle($title->format('m.Y'));
            // set page orientation
            $sheet->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
            // fit content to printable area
            $sheet->getPageSetup()->setFitToWidth(1);
            // set paper size
            $sheet->getPageSetup()->setPaperSize(\PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
            // create the writer
            $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
            // create the response
            $response = $this->get('phpexcel')->createStreamedResponse($writer);
            // adding headers
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=' . $title->format('m.Y') . '.xls');
            $response->headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        }
        return new Response(null);
    }

}
