/**
 * Created by hachmi.halfaoui on 18/09/2017.
 */
$(function () {
    $('.chosen-select').chosen();
    $('.chosen-select-deselect').chosen({allow_single_deselect: true});
});

$(document).ready(function () {
    $(document).on('change', '#suivi_etudesbundle_demande_direction', function (e) {
        var idDirection = $('#suivi_etudesbundle_demande_direction').val();
        console.log(idDirection);
        var i = 0;
        var text = "";
        $.ajax({
            type: "POST",
            url: 'get_service',
            data: {id: idDirection},
            beforeSend: function () {
                $("body").append("<div id='loadingBlock'>");
                $("#submit").prop('disabled', true);
            },
            success: function (data) {
                console.log(data);

                if(data.length >0) {
                    $("#application_id-error").css('display','none');

                    var list = [];
                    var j = 0;
                    $( "#service_id .chosen-choices li span" ).each(function() {
                        list[j]=$( this ).text();
                        j++;
                    });

                    console.log(list);
                    for (i ; i < data.length; i++) {

                        if(list.indexOf(data[i]['libelle'])>=0){

                            text += '<option selected value="' + data[i]['id'] + '">' + data[i]['libelle'] + '</option>'
                        }else{
                            text += '<option value="' + data[i]['id'] + '">' + data[i]['libelle'] + '</option>'
                        }
                    }



                    $("#service_id").html("");
                    $("#service_id").append('<label class="col-sm-4 control-label">Services <span class="symbol required"></span> </label>' +
                        '<div class="col-sm-4">' +
                        ' <select class="chosen-select" id="suivi_etudesbundle_demande_service" name="suivi_etudesbundle_demande[service][]"  multiple="multiple" data-placeholder="Sélectionnez un ou plusieurs service(s)"> ' +
                        text +
                        '</select>' +
                        '</div>')

                }else{
                    $("#service_id").html("");
                }
            },
            complete: function () {
                $("#loadingBlock").remove();
                $('.chosen-select').chosen();
                $('.chosen-select-deselect').chosen({allow_single_deselect: true});
            }
        });





    });

    $(document).on('change', '#demande_search_direction', function (e) {
        var idDirection = $('#demande_search_direction').val();
        console.log(idDirection);
        var i = 0;
        var text = "";
        $.ajax({
            type: "POST",
            url: 'get_service',
            data: {id: idDirection},
            beforeSend: function () {
                $("body").append("<div id='loadingBlock'>");
                $("#submit").prop('disabled', true);
            },
            success: function (data) {
                console.log(data);

                if(data.length >0) {
                    $("#application_id-error").css('display','none');


                    var list = [];
                    var j = 0;
                    $( "#service_id .chosen-choices li span" ).each(function() {
                        list[j]=$( this ).text();
                        j++;
                    });

                    for (i ; i < data.length; i++) {

                        if(list.indexOf(data[i]['libelle'])>=0){

                            text += '<option selected value="' + data[i]['id'] + '">' + data[i]['libelle'] + '</option>'
                        }else{
                            text += '<option value="' + data[i]['id'] + '">' + data[i]['libelle'] + '</option>'
                        }
                    }



                    // for (i ; i < data.length; i++) {
                    //     console.log(data[i]['id']);
                    //     text += '<option value="' + data[i]['id'] + '">' + data[i]['libelle'] + '</option>'
                    // }
                    $("#service_id").html("");
                    $("#service_id").append('<label class="col-md-5 col-xs-4  control-label">Services <span class="symbol required"></span> </label>' +
                        '<div class="col-md-7 col-xs-6">' +
                        ' <select class="chosen-select" id="demande_search_service" name="demande_search[service][]" multiple="multiple" data-placeholder=" Choisir une service"> ' +
                        text +
                        '</select>' +
                        '</div>')

                }else{
                    $("#service_id").html("");
                }
            },
            complete: function () {
                $("#loadingBlock").remove();
                $('.chosen-select').chosen();
                $('.chosen-select-deselect').chosen({allow_single_deselect: true});
            }
        });

    });

    $(document).on('change', '#suivi_etudes_demande_edit_direction', function (e) {
        var idDirection = $('#suivi_etudes_demande_edit_direction').val();
        console.log(idDirection);

        var i = 0;
        var text = "";
        var l = window.location;
        var base_url = l.protocol + "//" + l.host + "/" + l.pathname.split('/')[1];
console.log(base_url);
        $.ajax({
            type: "POST",
            url: base_url+'/web/app_dev.php/suivi-etudes/get_service',
            data: {id: idDirection},
            beforeSend: function () {
                $("body").append("<div id='loadingBlock'>");
                $("#submit").prop('disabled', true);
            },
            success: function (data) {
                console.log(data);

                if(data.length >0) {
                    $("#application_id-error").css('display','none');


                    var list = [];
                    var j = 0;
                    $( "#service_id .chosen-choices li span" ).each(function() {
                        list[j]=$( this ).text();
                        j++;
                    });

                    for (i ; i < data.length; i++) {

                        if(list.indexOf(data[i]['libelle'])>=0){

                            text += '<option selected value="' + data[i]['id'] + '">' + data[i]['libelle'] + '</option>'
                        }else{
                            text += '<option value="' + data[i]['id'] + '">' + data[i]['libelle'] + '</option>'
                        }
                    }

                    // for (i ; i < data.length; i++) {
                    //     console.log(data[i]['id']);
                    //     text += '<option value="' + data[i]['id'] + '">' + data[i]['libelle'] + '</option>'
                    // }
                     $("#service_id").html("");
                    $("#service_id").append('<label class="col-md-4  control-label">Services <span class="symbol required"></span> </label>' +
                        '<div class="col-md-7 col-xs-6">' +
                        ' <select class="chosen-select" id="suivi_etudes_demande_edit_service" name="suivi_etudes_demande_edit[service][]" multiple="multiple" required="false" data-placeholder=" Choisir une service"> ' +
                        text +
                        '</select>' +
                        '</div>')

                }else{
                    $("#service_id").html("");
                }
            },
            complete: function () {
                $("#loadingBlock").remove();

                $('.chosen-select').chosen();
                $('.chosen-select-deselect').chosen({allow_single_deselect: true});
            }
        });

    });
});

$(function () {


    $("#suivi_etudes_demande_estimation_date_recette").datetimepicker({
        locale: 'fr',
        format: 'DD/MM/YYYY',
        minDate: new Date()

    });

    $("#suivi_etudes_demande_estimation_date_production").datetimepicker({
        locale: 'fr',
        format: 'DD/MM/YYYY',
        minDate: new Date()
    });
});

$(document).on('change', '#suivi_etudes_demande_estimation_chargesDev, #suivi_etudes_demande_estimation_chargesCP', function () {

    var cp = parseInt($('#suivi_etudes_demande_estimation_chargesCP').val());
    var dev = parseInt($('#suivi_etudes_demande_estimation_chargesDev').val());

    if(isNaN(cp)){
        $('#suivi_etudes_demande_estimation_charges').val(dev);
    }else if(isNaN(dev)){
        $('#suivi_etudes_demande_estimation_charges').val(cp);
    }else{
        $('#suivi_etudes_demande_estimation_charges').val(cp +dev);
    }



});

$(document).ready(function () {
    $(function () {

        $("#suivi_etudes_demande_estimation_date_recette").on("dp.change", function (e) {

            var date_rec = $('#suivi_etudes_demande_estimation_date_recette').val();
            $('#suivi_etudes_demande_estimation_date_production').data("DateTimePicker").minDate(date_rec);

        });

        $("#suivi_etudes_demande_estimation_date_production").on("dp.change", function (e) {

            var date_prod = $('#suivi_etudes_demande_estimation_date_production').val();
            $('#suivi_etudes_demande_estimation_date_recette').data("DateTimePicker").maxDate(date_prod);

        });

    });
});

$(document).on('change', '#suivi_etudesbundle_demande_direction', function (e) {
    $('#suivi_etudesbundle_demande_direction-error').css('display','none');
});
$(document).on('change', '#suivi_etudesbundle_demande_application', function (e) {
    $('#suivi_etudesbundle_demande_application-error').css('display','none');
});

$(function () {
    var validator = $("#addForm").submit(function () {
    }).validate({
        ignore: "",
        rules: {
            "suivi_etudesbundle_demande_direction": "required",
            "suivi_etudesbundle_demande[type]": "required",
            "suivi_etudesbundle_demande[application][]":"required",
            "suivi_etudesbundle_demande[titre]":"required"

        },
        messages: {
            "suivi_etudesbundle_demande[direction][]": "Veuillez selctionner une ou plusieurs direction",
            "suivi_etudesbundle_demande[type]": "Veuillez selctionner le type de la demande",
            "suivi_etudesbundle_demande[application][]":"Veuillez selctionner une ou plusieurs application",
            "suivi_etudesbundle_demande[titre]":"Veuillez saisir un titre"

        }
    });
})

