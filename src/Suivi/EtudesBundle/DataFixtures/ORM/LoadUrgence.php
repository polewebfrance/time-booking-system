<?php

namespace Suivi\EtudesBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Suivi\EtudesBundle\Entity\Urgence;

class LoadUrgence extends AbstractFixture implements OrderedFixtureInterface
{

  /**
   * ORDRE DE PASSAGE DU FIXTURES:LOAD
   */
  public function getOrder()
  {
    return 2;
  }


  public function load(ObjectManager $manager)
  {
    // Liste des noms de compétences à ajouter
    $names = array('Très basse','Basse', 'Moyenne', 'Haute', 'Très Haute');

    foreach ($names as $name) {
      // On crée la compétence
      $type = new Urgence();
      $type->setName($name);

      // On la persiste
      $manager->persist($type);
    }

    // On déclenche l'enregistrement de toutes les catégories
    $manager->flush();
  }
}