<?php

namespace Suivi\EtudesBundle\DoctrineListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Suivi\EtudesBundle\Entity\Modification;
use Suivi\EtudesBundle\Entity\DemandeUser;


class AjoutDemandeUser
{
	protected $container;
	private $modifs;

	public function __construct($container)
	{
		$this->container = $container;
	}
	
	/**
	 *
	 * @param LifecycleEventArgs $args
	 */
	public function postPersist(LifecycleEventArgs $args){
		$entity = $args->getEntity();
		$em = $args->getEntityManager();
		
		if ($entity instanceof DemandeUser){
			if ($entity->getType() != 'DEMANDEUR'){
		        $modification = new Modification();
		        $modification->setDate(new \Datetime());
		        $modification->setDemande($entity->getDemande());
		        $modification->setType($entity->getType());
		        $modification->setNouvelleValeur($entity->getUser()->getDisplayname());
				$modification->setUser($this->container->get('security.context')->getToken()->getUser());
				$em->persist($modification);
		        $em->flush();
			}
		}
	}
	
}