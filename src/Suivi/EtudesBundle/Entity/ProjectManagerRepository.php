<?php

namespace Suivi\EtudesBundle\Entity;

use Doctrine\ORM\EntityRepository;

class ProjectManagerRepository extends EntityRepository {

    public function getAll() {
        $data = $this->createQueryBuilder('m')
                        ->select('m.id,u.displayname,p.name poleName,m.active')
                        ->leftJoin('SuiviEtudesBundle:ProjectPole', 'p', 'WITH', 'p.id=IDENTITY(m.pole)')
                        ->leftJoin('UserUserBundle:User', 'u', 'WITH', 'u.id=IDENTITY(m.manager)')
                        ->orderBy('m.id', 'ASC')
                        ->getQuery()->getResult();
        return array('data' => $data);
    }

    public function getTechsAbsencesByManager($managerId) {
        $data = $this->createQueryBuilder('m')
                        ->where('IDENTITY(m.manager)= :managerId')
                        ->setParameter('managerId', $managerId)
                        ->getQuery()->getResult();
        $poleIds = $pole = array();
        foreach ($data as $managerRow) {
            if ($managerRow && $managerRow->getActive() == 1) {
                array_push($poleIds, $managerRow->getPole()->getId());
                $pole[] = $managerRow->getPole()->getName();
            }
        }
        return array(
            'pole' => implode(",", $pole),
            'poleIds' => implode(",", $poleIds),
        );
    }

    public function getTechsByManager($managerId, $year) {
        $data = $this->createQueryBuilder('m')
                        ->where('IDENTITY(m.manager)= :managerId')
                        ->setParameter('managerId', $managerId)
                        ->getQuery()->getResult();
        $techs = array();
        foreach ($data as $managerRow) {
            if ($managerRow && $managerRow->getActive() == 1) {
                foreach ($managerRow->getPole()->getTech() as $tech) {
                    $vacationDays = $daysLeft = $recHours = "N/A";
                    foreach ($tech->getTechInfo() as $techInfo) {
                        if ($year == $techInfo->getYear()) {
                            $vacationDays = null !== $techInfo->getVacationDays() ? $techInfo->getVacationDays() : $vacationDays;
                            $daysLeft = null !== $techInfo->getDaysLeft() ? $techInfo->getDaysLeft() : $daysLeft;
                            $recHours = null !== $techInfo->getRecHOurs() ? $techInfo->getRecHOurs() : $recHours;
                        }
                    }
                    $techs[] = array(
                        'displayname' => $tech->getTech()->getDisplayname(),
                        'pole' => $managerRow->getPole()->getName(),
                        'vacationDays' => $vacationDays,
                        'daysLeft' => $daysLeft,
                        'recHours' => $recHours,
                    );
                }
            }
        }
        array_multisort($techs, SORT_ASC);
        return array(
            'data' => $techs,
        );
    }

    public function getPolIdsByManager($managerId) {
        $data = $this->createQueryBuilder('m')
                        ->select('GROUP_CONCAT(IDENTITY(m.pole),\'\') poleIds')
                        ->where('IDENTITY(m.manager)= :managerId')
                        ->setParameter('managerId', $managerId)
                        ->getQuery()
						->getSingleResult();
        return array_map('intval', explode(',', $data['poleIds'])); #return array of integers
    }

}
