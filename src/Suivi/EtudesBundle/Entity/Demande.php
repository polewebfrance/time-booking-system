<?php

namespace Suivi\EtudesBundle\Entity;

use Administration\DirectionBundle\Entity\Direction;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Suivi\EtudesBundle\Entity\Statut;
use Suivi\EtudesBundle\Entity\PieceJointe;
use Suivi\EtudesBundle\Entity\suiviMail;
use Suivi\EtudesBundle\Entity\Modification;
use User\UserBundle\Entity\User;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * Demande
 *
 * @ORM\Table(name="se_demande")
 * @ORM\Entity(repositoryClass="Suivi\EtudesBundle\Entity\DemandeRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @Assert\Callback(methods={"validateDates"})
 */
class Demande {


    public function __construct() {
        $this->date = new \Datetime();
        $this->demandeApplication = new ArrayCollection();
        $this->demandeDirection = new ArrayCollection();
        $this->demandeService = new ArrayCollection();
        $this->piecejointes = new ArrayCollection();
        $this->demandeUser = new ArrayCollection();
        $this->standBy = new ArrayCollection();
        $this->demandeDocs = new ArrayCollection();
        $this->directions = new ArrayCollection();
        $this->services = new ArrayCollection();
        // On récupère l'EntityManager
    }


    
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="statut_id", type="integer")
     */
    private $statutId;

    /**
     * @ORM\Column(name="dernier_statut", type="integer")
     */
    private $dernierStatut;

    /**
     * @ORM\Column(name="titre", type="string", length=255)
     */
    private $titre;

    /**
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(name="date_recette", type="datetime", nullable=true)
     * @Assert\DateTime(message="La date de recette n'est pas valide.")
     */
    private $dateRecette;

    /**
     * @ORM\Column(name="charges", type="string", nullable=true)
     * @Assert\Type(type="digit", message="La charge doit être un nombre entier.")
     */
    private $charges;

    /**
     * @ORM\Column(name="charges_cp", type="string", nullable=true)
     * @Assert\Type(type="digit", message="La charge du chef de projet doit être un nombre entier.")
     */
    private $chargesCP;

    /**
     * @ORM\Column(name="charges_dev", type="string", nullable=true)
     * @Assert\Type(type="digit", message="La charge du développeur doit être un nombre entier.")
     */
    private $chargesDev;

    /**
     * @ORM\Column(name="charges_cp_restante", type="string", nullable=true)
     * @Assert\Type(type="digit", message="La charge restante du chef de projet doit être un nombre entier.")
     */
    private $chargesCPRestante;

    /**
     * @ORM\Column(name="charges_dev_restante", type="string", nullable=true)
     * @Assert\Type(type="digit", message="La charge restante du développeur doit être un nombre entier.")
     */
    private $chargesDevRestante;

    /**
     * @ORM\Column(name="date_production", type="datetime", nullable=true)
     * @Assert\DateTime(message="La date de mise en production n'est pas valide.")
     */
    private $dateProduction;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)

     */
    private $updatedAt;

    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @ORM\Column(name="date_cloture", type="date", nullable=true)
     */
    private $dateCloture;

    /**
     * @ORM\ManyToOne(targetEntity="Suivi\EtudesBundle\Entity\Statut")
     * @ORM\JoinColumn(nullable=false)
     */
    private $statut;

    /**
     * @ORM\ManyToOne(targetEntity="Suivi\EtudesBundle\Entity\Urgence")
     * @ORM\JoinColumn(nullable=false)
     */
    private $urgence;

    /**
     * @ORM\ManyToOne(targetEntity="Suivi\EtudesBundle\Entity\Type")
     * @ORM\JoinColumn(nullable=false)
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity="Suivi\EtudesBundle\Entity\DemandeApplication",mappedBy="demande")
     */
    private $demandeApplication;

//    /**
//     * @ORM\OneToMany(targetEntity="Suivi\EtudesBundle\Entity\DemandeDirection",mappedBy="demande")
//     */
//    private $demandeDirection;
//
//    /**
//     * @ORM\OneToMany(targetEntity="Suivi\EtudesBundle\Entity\DemandeService",mappedBy="demande")
//     */
//    private $demandeService;

    /**
     * @ORM\OneToMany(targetEntity="Suivi\EtudesBundle\Entity\PieceJointe", mappedBy="demande", cascade={"persist", "remove"})
     */
    private $piecejointes; //Notez le « s », une demande est liée à plusieurs pièces jointes

    /**
     * @ORM\Column(name="mailing", type="boolean", nullable=true)
     */
    private $mailing;

    /**
     * @ORM\OneToMany(targetEntity="Suivi\EtudesBundle\Entity\DemandeUser", mappedBy="demande", cascade={"persist", "remove"})
     */
    private $demandeUser;

    /**
     * @ORM\Column(name="recette", type="boolean", nullable=true)
     */
    private $recette;

    /**
     * @ORM\OneToOne(targetEntity="Suivi\EtudesBundle\Entity\PieceJointe", mappedBy="demande", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $devis;

    /**
     * @ORM\Column(name="demandeur_id", type="integer")
     */
    private $demandeur;

    /**
     * @ORM\Column(name="modification_date_prod", type="boolean", nullable=true)
     */
    private $modifProd;

    /**
     * @ORM\Column(name="motifRefus", type="string", length=255, nullable=true)
     */
    private $motifRefus;

    /**
     * @ORM\Column(name="descriptionRefus", type="text", nullable=true)
     */
    private $descriptionRefus;

    /**
     * @ORM\Column(name="commentaireRecette", type="text", nullable=true)
     */
    private $commentaireRecette;

    /**
     * @ORM\Column(name="commentaireProd", type="text", nullable=true)
     */
    private $commentaireProd;

    /**
     * @ORM\Column(name="commentaireInfo", type="text", nullable=true)
     */
    private $commentaireInfo;

    /**
     * @ORM\Column(name="spec", type="text", nullable=true)
     */
    private $spec;

    /**
     * @ORM\OneToOne(targetEntity="Suivi\EtudesBundle\Entity\PieceJointe", mappedBy="demande", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $cahierRecette;

    /**
     * @ORM\Column(name="charge_modif", type="integer")
     */
    private $chargeModif;

    /**
     * @ORM\ManyToOne(targetEntity="Suivi\EtudesBundle\Entity\DemandeCriterionFrequence")
     * @ORM\JoinColumn(nullable=true, name="criterion_frequence", referencedColumnName="id")
     */
    private $criterionFrequence;

    /**
     * @ORM\ManyToOne(targetEntity="Suivi\EtudesBundle\Entity\DemandeCriterionOrigin")
     * @ORM\JoinColumn(nullable=true, name="criterion_origin", referencedColumnName="id")
     */
    private $criterionOrigin;

    /**
     * @ORM\ManyToOne(targetEntity="Suivi\EtudesBundle\Entity\DemandeCriterionPopulation")
     * @ORM\JoinColumn(nullable=true, name="criterion_population", referencedColumnName="id")
     */
    private $criterionPopulation;

    /**
     * @ORM\OneToMany(targetEntity="Suivi\EtudesBundle\Entity\DemandeStandBy", mappedBy="demande")
     */
    private $standBy;

    /**
     * @ORM\Column(name="criterion_origin", type="integer")
     */
    private $origin;

    /**
     * @ORM\Column(name="criterion_population", type="integer")
     */
    private $population;

    /**
     * @ORM\Column(name="criterion_frequence", type="integer")
     */
    private $frequence;

    /**
     * @ORM\OneToMany(targetEntity="Suivi\EtudesBundle\Entity\DemandeDocs", mappedBy="demande", cascade={"persist", "remove"})
     */
    private $demandeDocs;


    /**
     * @ORM\ManyToOne(targetEntity="Administration\DirectionBundle\Entity\Direction", inversedBy="demandes")
     * @ORM\JoinColumn(name="direction_id", referencedColumnName="id")
     */
    public $directions;
    /**
     * Add Direction
     *
     * @param \Administration\DirectionBundle\Entity\Direction $direction
     *
     * @return Direction
     */
    public function addDirection(\Administration\DirectionBundle\Entity\Direction $direction)
    {
        $this->directions[] = $direction;

        return $this;
    }
    /**
     * Remove direction
     *
     * @param \Administration\DirectionBundle\Entity\Direction $direction
     */
    public function removeDirection(\Administration\DirectionBundle\Entity\Direction $direction)
    {
        $this->directions->removeElement($direction);
    }
    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDirections()
    {
        return $this->directions;
    }
    /**
     * @ORM\ManyToOne(targetEntity="Administration\DirectionBundle\Entity\Service", inversedBy="demandes")
     * @ORM\JoinColumn(name="service_id", referencedColumnName="id")
     */
    public $services;
    /**
     * Add Service
     *
     * @param \Administration\DirectionBundle\Entity\Service $service
     *
     * @return Service
     */
    public function addService(\Administration\DirectionBundle\Entity\Service $service)
    {
        $this->services[] = $service;

        return $this;
    }
    /**
     * Remove direction
     *
     * @param \Administration\DirectionBundle\Entity\Direction $direction
     */
    public function removeService(\Administration\DirectionBundle\Entity\Service $service)
    {
        $this->services->removeElement($service);
    }
    /**
     * @return mixed
     */
    public function getServices()
    {
        return $this->services;
    }















    /**
     * @var \Suivi\EtudesBundle\Entity\Demande
     *
     * @ORM\ManyToOne(targetEntity="Suivi\EtudesBundle\Entity\Demande")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(nullable=true, name="parent_id", referencedColumnName="id")
     * })
     */
    private $parent;



    /**
     * Set parent
     *
     * @param \Suivi\EtudesBundle\Entity\Demande $parent
     *
     * @return Demande
     */
    public function setParent(\Suivi\EtudesBundle\Entity\Demande $parent = null)
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * Get parent
     *
     * @return \Suivi\EtudesBundle\Entity\Demande
     */
    public function getParent()
    {
        return $this->parent;
    }



    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Get statutId
     *
     * @return integer
     */
    public function getstatutId() {
        return $this->statutId;
    }

    /**
     * Set statutId
     *
     * @param string $statutId
     * @return Demande
     */
    public function setStatutId($statutId) {
        $this->statutId = $statutId;

        return $this;
    }

    /**
     * Get dernierStatut
     *
     * @return integer
     */
    public function getDernierStatut() {
        return $this->dernierStatut;
    }

    /**
     * Set dernierStatut
     *
     * @param string dernierStatut
     * @return Demande
     */
    public function setDernierStatut($dernierStatut) {
        $this->dernierStatut = $dernierStatut;

        return $this;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return Demande
     */
    public function setTitre($titre) {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre() {
        return $this->titre;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Demande
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Demande
     */
    public function setDate($date) {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate() {
        return $this->date;
    }

    /**
     * @ORM\PreUpdate
     */
    public function updateDate() {
        $this->setUpdatedAt(new \Datetime());
    }

    public function setUpdatedAt(\Datetime $updatedAt) {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    /**
     * Set dateCloture
     *
     * @param \DateTime $dateCloture
     * @return Demande
     */
    public function setDateCloture($dateCloture) {
        $this->dateCloture = $dateCloture;

        return $this;
    }

    /**
     * Get dateCloture
     *
     * @return \DateTime
     */
    public function getDateCloture() {
        return $this->dateCloture;
    }

    /**
     * Set statut
     *
     * @param \Suivi\EtudesBundle\Entity\Statut $statut
     * @return Demande
     */
    public function setStatut(\Suivi\EtudesBundle\Entity\Statut $statut) {
        $this->statut = $statut;

        return $this;
    }

    /**
     * Get statut
     *
     * @return \Suivi\EtudesBundle\Entity\Statut
     */
    public function getStatut() {
        return $this->statut;
    }

    /**
     * Set urgence
     *
     * @param \Suivi\EtudesBundle\Entity\Urgence $urgence
     * @return Demande
     */
    public function setUrgence(\Suivi\EtudesBundle\Entity\Urgence $urgence) {
        $this->urgence = $urgence;

        return $this;
    }

    /**
     * Get urgence
     *
     * @return \Suivi\EtudesBundle\Entity\Urgence
     */
    public function getUrgence() {
        return $this->urgence;
    }

    /**
     * Set type
     *
     * @param \Suivi\EtudesBundle\Entity\Type $type
     * @return Demande
     */
    public function setType(\Suivi\EtudesBundle\Entity\Type $type) {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \Suivi\EtudesBundle\Entity\Type
     */
    public function getType() {
        return $this->type;
    }

    public function getDeletedAt() {
        return $this->deletedAt;
    }

    public function setDeletedAt($deletedAt) {
        $this->deletedAt = $deletedAt;
    }

    /**
     * add application
     *
     * @param \Suivi\EtudesBundle\Entity\Application $application
     * @return Demande
     */
    public function getApplication() {
        $application = array();
        foreach ($this->getDemandeApplication() as $row) {
            $application[] = $row->getApplication();
        }
        return $application;
    }

    /**
     * Set demandeApplication
     *
     * @param \Suivi\EtudesBundle\Entity\DemandeApplication $demandeApplication
     * @return DemandeApplication
     */
    public function setDemandeApplication(\Suivi\EtudesBundle\Entity\DemandeApplication $demandeApplication) {
        $this->demandeApplication = $demandeApplication;

        return $this;
    }

    /**
     * Get demandeApplication
     *
     * @return \Suivi\EtudesBundle\Entity\DemandeApplication
     */
    public function getDemandeApplication() {
        return $this->demandeApplication;
    }

//    public function getDirection() {
//        $direction = array();
//        foreach ($this->getDemandeDirection() as $row) {
//            $direction[] = $row->getDirection();
//        }
//        return $direction;
//    }

//    public function setDemandeDirection(\Suivi\EtudesBundle\Entity\DemandeDirection $demandeDirection) {
//        $this->demandeDirection = $demandeDirection;
//
//        return $this;
//    }
//
//    public function getDemandeDirection() {
//        return $this->demandeDirection;
//    }

//    public function getService() {
//        $service = array();
//        foreach ($this->getDemandeService() as $row) {
//            $service[] = $row->getService();
//        }
//        return $service;
//    }
//
//    public function setDemandeService(\Suivi\EtudesBundle\Entity\DemandeService $demandeService) {
//        $this->demandeService = $demandeService;
//
//        return $this;
//    }
//
//    public function getDemandeService() {
//        return $this->demandeService;
//    }

    /**
     * Add piecejointes
     *
     * @param \Suivi\EtudesBundle\Entity\PieceJointe $piecejointe
     * @return Demande
     */
    public function addPieceJointe(\Suivi\EtudesBundle\Entity\PieceJointe $piecejointe) {
        $this->piecejointes[] = $piecejointe;

        $piecejointe->setDemande($this);

        return $this;
    }

    /**
     * Get piecejointes`
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPieceJointes() {
        return $this->piecejointes->toArray();
    }

    /**
     * Remove piecejointe
     *
     * @param \Suivi\EtudesBundle\Entity\PieceJointe $piecejointe
     */
    public function removePieceJointe(\Suivi\EtudesBundle\Entity\PieceJointe $piecejointe) {
        $this->piecejointes->removeElement($piecejointe);
    }

    /**
     * Set devis
     *
     * @param \Suivi\EtudesBundle\Entity\PieceJointe $piecejointe
     */
    public function setDevis(\Suivi\EtudesBundle\Entity\PieceJointe $devis) {
        if ($this->devis != null) {
            $this->addPieceJointe($this->devis);
        }
        $this->devis = $devis;
    }
    
    /**
     * Remove devis
     *
     */
    public function removeDevis() {
        $this->devis = null;
    }

    /**
     * Get devis
     *
     * @return \Suivi\EtudesBundle\Entity\PieceJointe
     */
    public function getDevis() {
        return $this->devis;
    }

    /**
     * Set mailing
     *
     * @param boolean $mailing
     * @return Demande
     */
    public function setMailing($mailing) {
        $this->mailing = $mailing;

        return $this;
    }

    /**
     * Get mailing
     *
     * @return boolean
     */
    public function getMailing() {
        return $this->mailing;
    }

    /**
     * Add demandeUser
     *
     * @param \Suivi\EtudesBundle\Entity\DemandeUser $demandeUser
     * @return Demande
     */
    public function addDemandeUser(\Suivi\EtudesBundle\Entity\DemandeUser $demandeUser, User $userConnecte = null) {
        $this->demandeUser[] = $demandeUser;

        return $this;
    }

    /**
     * Remove demandeUser
     *
     * @param \Suivi\EtudesBundle\Entity\DemandeUser $demandeUser
     */
    public function removeDemandeUser(\Suivi\EtudesBundle\Entity\DemandeUser $demandeUser) {
        $this->demandeUser->removeElement($demandeUser);
    }

    /**
     * Get demandeUser
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDemandeUser() {
        return $this->demandeUser;
    }

    /**
     * Set dateRecette
     *
     * @param \DateTime £dateRecette
     * @return Demande
     */
    public function setDateRecette($dateRecette) {
        $this->dateRecette = $dateRecette;

        return $this;
    }

    /**
     * Get dateRecette
     *
     * @return \DateTime
     */
    public function getDateRecette() {
        return $this->dateRecette;
    }

    /**
     * Set charges
     *
     * @param string $charges
     * @return Demande
     */
    public function setCharges($charges) {
        $this->charges = $charges;

        return $this;
    }

    /**
     * Get charges
     *
     * @return string
     */
    public function getCharges() {
        return $this->charges;
    }

    /**
     * Set chargesCP
     *
     * @param string $chargesCP
     * @return Demande
     */
    public function setChargesCP($chargesCP) {
        $this->chargesCP = $chargesCP;

        return $this;
    }

    /**
     * Get chargesCP
     *
     * @return string
     */
    public function getChargesCP() {
        return $this->chargesCP;
    }

    /**
     * Set chargesDev
     *
     * @param string $chargesDev
     * @return Demande
     */
    public function setChargesDev($chargesDev) {
        $this->chargesDev = $chargesDev;

        return $this;
    }

    /**
     * Get chargesDev
     *
     * @return string
     */
    public function getChargesDev() {
        return $this->chargesDev;
    }

    /**
     * Set dateProduction
     *
     * @param \DateTime $dateProduction
     * @return Demande
     */
    public function setDateProduction($dateProduction) {
        $this->dateProduction = $dateProduction;

        return $this;
    }

    /**
     * Get dateProduction
     *
     * @return \DateTime
     */
    public function getDateProduction() {
        return $this->dateProduction;
    }

    /**
     * Set recette
     *
     * @param boolean $recette
     * @return Demande
     */
    public function setRecette($recette) {
        $this->recette = $recette;

        return $this;
    }

    /**
     * Get recette
     *
     * @return boolean
     */
    public function getRecette() {
        return $this->recette;
    }

    public function __toString() {
        return $this->id . ' - ' . $this->titre;
    }

//    /**
//     * Set demandeur
//     *
//     * @param integer $demandeur
//     * @return Demande
//     */
//    public function setDemandeur($demandeur) {
//        $this->demandeur = $demandeur;
//
//        return $this;
//    }

//    /**
//     * Get demandeur
//     *
//     * @return integer
//     */
//    public function getDemandeur() {
//        return $this->demandeur;
//    }

    /**
     * Set modifProd
     *
     * @param boolean $modifProd
     * @return Demande
     */
    public function setModifProd($modifProd) {
        $this->modifProd = $modifProd;

        return $this;
    }

    /**
     * Get modifProd
     *
     * @return boolean
     */
    public function getModifProd() {
        return $this->modifProd;
    }

    /**
     * Set motifRefus
     *
     * @param boolean $motifRefus
     * @return Demande
     */
    public function setMotifRefus($motifRefus) {
        $this->motifRefus = $motifRefus;

        return $this;
    }

    /**
     * Get motifRefus
     *
     * @return string
     */
    public function getMotifRefus() {
        return $this->motifRefus;
    }

    /**
     * Set motifcommentaireProd
     *
     * @param boolean $motifRefus
     * @return Demande
     */
    public function setCommentaireProd($commentaireProd) {
        $this->commentaireProd = $commentaireProd;

        return $this;
    }

    /**
     * Get motifCommentaireProd
     *
     * @return string
     */
    public function getCommentaireProd() {
        return $this->commentaireProd;
    }

    /**
     * Set descriptionRefus
     *
     * @param string $descriptionRefus
     * @return Demande
     */
    public function setDescriptionRefus($descriptionRefus) {
        $this->descriptionRefus = $descriptionRefus;

        return $this;
    }

    /**
     * Get descriptionRefus
     *
     * @return string
     */
    public function getDescriptionRefus() {
        return $this->descriptionRefus;
    }

    /**
     * Set descriptioncommentaireRecette
     *
     * @param string $commentaireRecette
     * @return Demande
     */
    public function setCommentaireRecette($commentaireRecette) {
        $this->commentaireRecette = $commentaireRecette;

        return $this;
    }

    /**
     * Get commentaireRecette
     *
     * @return string
     */
    public function getCommentaireRecette() {
        return $this->commentaireRecette;
    }

    /**
     * Set spec
     *
     * @param string $spec
     * @return Demande
     */
    public function setSpec($spec) {
        $this->spec = $spec;

        return $this;
    }

    /**
     * Get spec
     *
     * @return string
     */
    public function getSpec() {
        return $this->spec;
    }

    /**
     * Set CommentaireInfo
     *
     * @param string $commentaireInfo
     * @return Demande
     */
    public function setCommentaireInfo($commentaireInfo) {
        $this->commentaireInfo = $commentaireInfo;

        return $this;
    }

    /**
     * Get CommentaireInfo
     *
     * @return string
     */
    public function getCommentaireInfo() {
        return $this->commentaireInfo;
    }

    /**
     * Set devis
     *
     * @param \Suivi\EtudesBundle\Entity\PieceJointe $piecejointe
     */
    public function setCahierRecette(\Suivi\EtudesBundle\Entity\PieceJointe $cahierRecette) {
        if ($this->cahierRecette != null) {
            $this->addPieceJointe($this->cahierRecette);
        }
        $this->cahierRecette = $cahierRecette;
    }

    /**
     * Get devis
     *
     * @return \Suivi\EtudesBundle\Entity\PieceJointe
     */
    public function getCahierRecette() {
        return $this->cahierRecette;
    }

    /**
     * @Assert\Callback
     */
    public function validateDates(ExecutionContextInterface $context) {
        if ($this->getDateProduction() < $this->getDateRecette()) {
            $context->addViolationAt(
                    'DateRecette', 'La date de la recette doit être antérieure à la date de mise en production', array(), null
            );
        }
    }

    /**
     * Set chargesCPRestante
     *
     * @param string $chargesCPRestante
     * @return Demande
     */
    public function setChargesCPRestante($chargesCPRestante) {
        $this->chargesCPRestante = $chargesCPRestante;

        return $this;
    }

    /**
     * Get chargesCPRestante
     *
     * @return string
     */
    public function getChargesCPRestante() {
        return $this->chargesCPRestante;
    }

    /**
     * Set chargesDevRestante
     *
     * @param string $chargesDevRestante
     * @return Demande
     */
    public function setChargesDevRestante($chargesDevRestante) {
        $this->chargesDevRestante = $chargesDevRestante;

        return $this;
    }

    /**
     * Get chargesDevRestante
     *
     * @return string
     */
    public function getChargesDevRestante() {
        return $this->chargesDevRestante;
    }

    /**
     * Get $chargeModif
     *
     * @return integer
     */
    public function getChargeModif() {
        return $this->chargeModif;
    }

    /**
     * Set chargeModif
     *
     * @param string $chargeModif
     * @return Demande
     */
    public function setChargeModif($chargeModif) {
        $this->chargeModif = $chargeModif;

        return $this;
    }

    /**
     * Get $criterionFrequence
     *
     * @return integer
     */
    public function getCriterionFrequence() {
        return $this->criterionFrequence;
    }

    /**
     * Set criterionFrequence
     *
     * @param \Suivi\EtudesBundle\Entity\DemandeCriterionFrequence $criterionFrequence
     * @return Demande
     */
    public function setCriterionFrequence(\Suivi\EtudesBundle\Entity\DemandeCriterionFrequence $criterionFrequence) {
        $this->criterionFrequence = $criterionFrequence;

        return $this;
    }

    /**
     * Get $criterionPopulation
     *
     * @return integer
     */
    public function getCriterionPopulation() {
        return $this->criterionPopulation;
    }

    /**
     * Set criterionPopulation
     *
     * @param string $criterionPopulation
     * @return Demande
     */
    public function setCriterionPopulation($criterionPopulation) {
        $this->criterionPopulation = $criterionPopulation;

        return $this;
    }

    /**
     * Get $criterionOrigin
     *
     * @return integer
     */
    public function getCriterionOrigin() {
        return $this->criterionOrigin;
    }

    /**
     * Set criterionOrigin
     *
     * @param string $criterionOrigin
     * @return Demande
     */
    public function setCriterionOrigin($criterionOrigin) {
        $this->criterionOrigin = $criterionOrigin;

        return $this;
    }

    public function getOrigin() {
        return $this->origin;
    }

    public function getPopulation() {
        return $this->population;
    }

    public function getFrequence() {
        return $this->frequence;
    }

    public function getStandBy() {
        return $this->standBy;
    }

    public function setStandBy($standBy) {
        $this->standBy = $standBy;

        return $this;
    }

    public function getDemandeDocs() {
        return $this->demandeDocs;
    }

    public function setDemandeDocs(\Suivi\EtudesBundle\Entity\DemandeDocs $demandeDocs) {
        $this->demandeDocs = $demandeDocs;

        return $this;
    }

}
