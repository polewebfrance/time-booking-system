<?php

namespace Suivi\EtudesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * ProjectPole
 *
 * @ORM\Table(name="se_project_pole", uniqueConstraints={@ORM\UniqueConstraint(name="name_UNIQUE", columns={"name"})})
 * @ORM\Entity(repositoryClass="Suivi\EtudesBundle\Entity\ProjectPoleRepository")
 * @ORM\HasLifecycleCallbacks
 * @UniqueEntity(
 *      fields={"name"},
 *      message="Ce nom est déjà utilisé."
 * )
 */
class ProjectPole {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="created_by", type="integer", nullable=false)
     */
    private $createdBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="updated_by", type="integer", nullable=true)
     */
    private $updatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="active", type="string", nullable=true)
     */
    private $active = 1;

    /**
     * @var \ProjectManager
     *
     * @ORM\OneToMany(targetEntity="ProjectManager", mappedBy="pole")
     */
    private $manager;
    
    /**
     * @var \ProjectTech
     *
     * @ORM\OneToMany(targetEntity="ProjectTech", mappedBy="pole")
     */
    private $tech;
    
    public function __construct() {
        $this->manager = new ArrayCollection();
        $this->tech = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ProjectPole
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set createdBy
     *
     * @param integer $createdBy
     *
     * @return ProjectPole
     */
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return integer
     */
    public function getCreatedBy() {
        return $this->createdBy;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime
     * @ORM\PrePersist
     *
     * @return ProjectPole
     */
    public function setCreatedAt() {
        if (!$this->createdAt) {
            $this->createdAt = new \DateTime();
        }
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * Set updatedBy
     *
     * @param integer $updatedBy
     *
     * @return ProjectPole
     */
    public function setUpdatedBy($updatedBy) {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return integer
     */
    public function getUpdatedBy() {
        return $this->updatedBy;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return ProjectPole
     */
    public function setUpdatedAt($updatedAt) {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    /**
     * Set active
     *
     * @param string $active
     *
     * @return ProjectPole
     */
    public function setActive($active) {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return string
     */
    public function getActive() {
        return $this->active;
    }
    
    /**
     * Set manager
     *
     * @param \Suivi\EtudesBundle\Entity\ProjectManager $manager
     *
     * @return ProjectManager
     */
    public function setManager(\Suivi\EtudesBundle\Entity\ProjectManager $manager = null) {
        $this->manager = $manager;

        return $this;
    }

    /**
     * Get manager
     *
     * \Suivi\EtudesBundle\Entity\ProjectManager $manager
     */
    public function getManager() {
        return $this->manager;
    }
    
    /**
     * Set tech
     *
     * @param \Suivi\EtudesBundle\Entity\ProjectTech $tech
     *
     * @return ProjectTech
     */
    public function setTech(\Suivi\EtudesBundle\Entity\ProjectTech $tech = null) {
        $this->tech = $tech;

        return $this;
    }

    /**
     * Get tech
     *
     * \Suivi\EtudesBundle\Entity\ProjectTech $tech
     */
    public function getTech() {
        return $this->tech;
    }

}
