<?php

namespace Suivi\EtudesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProjectTech
 *
 * @ORM\Table(name="se_project_user", indexes={@ORM\Index(name="project_pole_idx", columns={"pole_id"}), @ORM\Index(name="project_poste_idx", columns={"poste_id"}), @ORM\Index(name="project_user_country_idx", columns={"country_id"})})
 * @ORM\Entity(repositoryClass="Suivi\EtudesBundle\Entity\ProjectTechRepository")
 */
class ProjectTech {

    /**
     * @var float
     *
     * @ORM\Column(name="taux_horaire", type="float", precision=10, scale=0, nullable=false, unique=false)
     */
    private $tauxHoraire;

    /**
     * @var integer
     *
     * @ORM\Column(name="capacite", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $capacite;

    /**
     * @var integer
     *
     * @ORM\Column(name="created_by", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $createdBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="updated_by", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $updatedBy;

    /**
     * @var string
     *
     * @ORM\Column(name="active", type="string", precision=0, scale=0, nullable=true, unique=false)
     */
    private $active;

    /**
     * @var \Suivi\EtudesBundle\Entity\ProjectCountry
     *
     * @ORM\ManyToOne(targetEntity="Suivi\EtudesBundle\Entity\ProjectCountry")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $country;

    /**
     * @var \User\UserBundle\Entity\User
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $tech;

    /**
     * @var \Suivi\EtudesBundle\Entity\ProjectPole
     *
     * @ORM\ManyToOne(targetEntity="Suivi\EtudesBundle\Entity\ProjectPole", inversedBy="tech")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pole_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $pole;

    /**
     * @var \Suivi\EtudesBundle\Entity\ProjectJob
     *
     * @ORM\ManyToOne(targetEntity="Suivi\EtudesBundle\Entity\ProjectJob")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="poste_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $poste;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="Suivi\EtudesBundle\Entity\AbsenceTechInfo", mappedBy="tech", cascade={"persist"})
     */
    private $techInfo;
    /**
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;
    /**
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * ProjectJob constructor.
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        $this->techInfo = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set tauxHoraire
     *
     * @param float $tauxHoraire
     *
     * @return ProjectTech
     */
    public function setTauxHoraire($tauxHoraire) {
        $this->tauxHoraire = $tauxHoraire;

        return $this;
    }

    /**
     * Get tauxHoraire
     *
     * @return float
     */
    public function getTauxHoraire() {
        return $this->tauxHoraire;
    }

    /**
     * Set capacite
     *
     * @param integer $capacite
     *
     * @return ProjectTech
     */
    public function setCapacite($capacite) {
        $this->capacite = $capacite;

        return $this;
    }

    /**
     * Get capacite
     *
     * @return integer
     */
    public function getCapacite() {
        return $this->capacite;
    }

    /**
     * Set createdBy
     *
     * @param integer $createdBy
     *
     * @return ProjectTech
     */
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return integer
     */
    public function getCreatedBy() {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param integer $updatedBy
     *
     * @return ProjectTech
     */
    public function setUpdatedBy($updatedBy) {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return integer
     */
    public function getUpdatedBy() {
        return $this->updatedBy;
    }

    /**
     * Set active
     *
     * @param string $active
     *
     * @return ProjectTech
     */
    public function setActive($active) {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return string
     */
    public function getActive() {
        return $this->active;
    }

    /**
     * Set tech
     *
     * @param \User\UserBundle\Entity\User $tech
     *
     * @return ProjectTech
     */
    public function setTech(\User\UserBundle\Entity\User $tech) {
        $this->tech = $tech;

        return $this;
    }

    /**
     * Get tech
     *
     * @return \User\UserBundle\Entity\User
     */
    public function getTech() {
        return $this->tech;
    }

    /**
     * Set country
     *
     * @param \Suivi\EtudesBundle\Entity\ProjectCountry $country
     *
     * @return SeProjectUser
     */
    public function setCountry(\Suivi\EtudesBundle\Entity\ProjectCountry $country = null) {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \Suivi\EtudesBundle\Entity\ProjectCountry
     */
    public function getCountry() {
        return $this->country;
    }

    /**
     * Set pole
     *
     * @param \Suivi\EtudesBundle\Entity\ProjectPole $pole
     *
     * @return ProjectTech
     */
    public function setPole(\Suivi\EtudesBundle\Entity\ProjectPole $pole = null) {
        $this->pole = $pole;

        return $this;
    }

    /**
     * Get pole
     *
     * @return \Suivi\EtudesBundle\Entity\ProjectPole
     */
    public function getPole() {
        return $this->pole;
    }

    /**
     * Set poste
     *
     * @param \Suivi\EtudesBundle\Entity\ProjectJob $poste
     *
     * @return ProjectTech
     */
    public function setPoste(\Suivi\EtudesBundle\Entity\ProjectJob $poste = null) {
        $this->poste = $poste;

        return $this;
    }

    /**
     * Get poste
     *
     * @return \Suivi\EtudesBundle\Entity\ProjectJob
     */
    public function getPoste() {
        return $this->poste;
    }

    /**
     * Add techInfo
     *
     * @param \Suivi\EtudesBundle\Entity\AbsenceTechInfo $techInfo
     *
     * @return ProjectTech
     */
    public function addTechInfo(\Suivi\EtudesBundle\Entity\AbsenceTechInfo $techInfo) {
        $this->techInfo[] = $techInfo;

        return $this;
    }

    /**
     * Remove techInfo
     *
     * @param \Suivi\EtudesBundle\Entity\AbsenceTechInfo $techInfo
     */
    public function removeTechInfo(\Suivi\EtudesBundle\Entity\AbsenceTechInfo $techInfo) {
        $this->techInfo->removeElement($techInfo);
    }

    /**
     * Get techInfo
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTechInfo() {
        return $this->techInfo;
    }

}
