<?php

namespace Suivi\EtudesBundle\Entity;

use Doctrine\ORM\EntityRepository;

class ProjectPoleRepository extends EntityRepository {

    public function getAll() {
        $data = $this->createQueryBuilder('pole')
                        ->select('pole.id,pole.name,pole.active')
                        ->orderBy('pole.name', 'ASC')
                        ->getQuery()->getResult();
        return array('data' => $data);
    }

    public function getPoleById($ids) {
        $poles = $this->createQueryBuilder('pole')
                        ->select('pole.name')
                        ->where('pole.id IN(:ids)')
                        ->setParameter('ids', $ids)
                        ->orderBy('pole.name', 'ASC')
                        ->getQuery()->getResult();
        $poleNames = array();
        foreach ($poles as $pole) {
            $poleNames[] = $pole['name'];
        }
        return implode(',',$poleNames);
    }

}
