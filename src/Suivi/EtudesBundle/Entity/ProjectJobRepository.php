<?php

namespace Suivi\EtudesBundle\Entity;

use Doctrine\ORM\EntityRepository;

class ProjectJobRepository extends EntityRepository {

    public function getAll() {
        $data = $this->createQueryBuilder('job')
                        ->select('job.id,job.name,job.active')
                        ->orderBy('job.name', 'ASC')
                        ->getQuery()->getResult();
        return array('data' => $data);
    }

}
