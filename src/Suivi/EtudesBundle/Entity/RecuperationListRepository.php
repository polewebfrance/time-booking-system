<?php

namespace Suivi\EtudesBundle\Entity;

use Doctrine\ORM\EntityRepository;

class RecuperationListRepository extends EntityRepository {

    public function findAllByTechAndYear($tech = null, $year = null) {
        $data = $this->createQueryBuilder('list')
                ->select('list.id,u.displayname,abs.name absenceType,DATE_FORMAT(list.startDate,\'%Y-%m-%d\') startDate'
                        . ', list.hours,list.refusReason,list.canceled'
                        . ', CASE WHEN list.validatedBy IS NULL THEN 0 ELSE u1.displayname END validatedBy')
                ->leftJoin('SuiviEtudesBundle:AbsenceTechInfo', 'techInfo', 'WITH', 'list.tech=techInfo.tech')
                ->leftJoin('UserUserBundle:User', 'u', 'WITH', 'u.id=IDENTITY(list.tech)')
                ->leftJoin('SuiviEtudesBundle:AbsenceType', 'abs', 'WITH', 'abs.id=IDENTITY(list.type)')
                ->leftJoin('UserUserBundle:User', 'u1', 'WITH', 'u1.id=list.validatedBy')
                ->where('list.tech = :tech')
                ->andWhere('techInfo.year = :year')
                ->setParameter('tech', $tech)
                ->setParameter('year', $year)
                ->orderBy('list.startDate', 'DESC')
                ->getQuery()
                ->getResult();
        return array('data' => $data);
    }

    public function findAllTechByPoleAndYear($depIds, $year) {
        $firstDayOfYear = new \DateTime(date("$year-01-01"));
        $lastDayOfYear = new \DateTime(date("$year-12-t"));
        $depIds = explode(',', $depIds);
        $data = $this->createQueryBuilder('list')
                ->select('list.id,u.displayname,DATE_FORMAT(list.startDate,\'%Y-%m-%d\') startDate'
                        . ',list.hours, CASE WHEN list.validatedBy IS NULL THEN 0 ELSE u1.displayname END validatedBy'
                        . ',list.refusReason,list.canceled')
                ->leftJoin('UserUserBundle:User', 'u', 'WITH', 'u.id=IDENTITY(list.tech)')
                ->leftJoin('SuiviEtudesBundle:AbsenceTechInfo', 'techInfo', 'WITH', 'list.tech=techInfo.tech')
                ->leftJoin('SuiviEtudesBundle:ProjectTech', 'tech', 'WITH', 'tech.tech=techInfo.tech')
                ->leftJoin('SuiviEtudesBundle:ProjectPole', 'pole', 'WITH', 'tech.pole=pole.id')
                ->leftJoin('UserUserBundle:User', 'u1', 'WITH', 'u1.id=list.validatedBy')
                ->where('pole.id IN(:depIds)')
                ->andWhere("list.startDate >= :firstDayOfYear AND list.startDate <= :lastDayOfYear")
                ->setParameter('depIds', $depIds)
                ->setParameter('firstDayOfYear', $firstDayOfYear->format('Y-m-d'))
                ->setParameter('lastDayOfYear', $lastDayOfYear->format('Y-m-d'))
                ->groupBy('list.id')
                ->orderBy('list.startDate', 'ASC')
                ->getQuery()
                ->getResult();
        return array('data' => $data);
    }

}
