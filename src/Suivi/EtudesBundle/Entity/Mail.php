<?php
namespace Suivi\EtudesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="se_mail")
 * @ORM\Entity(repositoryClass="Suivi\EtudesBundle\Entity\MailRepository")
 */
class Mail
{
	/**
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;
	
    /**
     * @ORM\Column(name="libelle", type="string", length=255)
     */
    private $libelle;
    

    /**
     * @ORM\Column(name="obligatoire", type="boolean")
     */
    private $obligatoire;
    
        
    
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return Demande
     */
    public function setLibelle($libelle)
    {
    	$this->libelle = $libelle;
    
    	return $this;
    }
    
    /**
     * Get titre
     *
     * @return string
     */
    public function getLibelle()
    {
    	return $this->libelle;
    }    
}