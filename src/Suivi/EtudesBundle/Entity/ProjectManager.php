<?php

namespace Suivi\EtudesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * ProjectManager
 *
 * @ORM\Table(name="se_project_manager")
 * @ORM\Entity(repositoryClass="Suivi\EtudesBundle\Entity\ProjectManagerRepository")
 * @UniqueEntity(
 *      fields={"manager", "pole"},
 *      message="Ce manager est déjà sur ce département."
 * )
 */
class ProjectManager {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="manager_id", referencedColumnName="id")
     * })
     * @Assert\NotBlank(
     *      message = "Cette valeur ne doit pas être vide."
     * )
     */
    private $manager;

    /**
     * @ORM\ManyToOne(targetEntity="ProjectPole", inversedBy="manager")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pole_id", referencedColumnName="id")
     * })
     * @Assert\NotBlank(
     *      message = "Cette valeur ne doit pas être vide."
     * )
     */
    private $pole;

    /**
     * @var integer
     *
     * @ORM\Column(name="created_by", type="integer", nullable=false)
     */
    private $createdBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="updated_by", type="integer", nullable=true)
     */
    private $updatedBy;

    /**
     * @var string
     *
     * @ORM\Column(name="active", type="string", nullable=true)
     */
    private $active = 1;

    /**
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;
    /**
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * ProjectJob constructor.
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set manager
     *
     * @param \User\UserBundle\Entity\User $manager
     *
     * @return ProjectManager
     */
    public function setManager(\User\UserBundle\Entity\User $manager = null) {
        $this->manager = $manager;

        return $this;
    }

    /**
     * Get manager
     *
     * \User\UserBundle\Entity\User $manager
     */
    public function getManager() {
        return $this->manager;
    }

    /**
     * Set pole
     *
     * @param ProjectPole $pole
     *
     * @return ProjectManager
     */
    public function setPole(ProjectPole $pole = null) {
        $this->pole = $pole;

        return $this;
    }

    /**
     * Get pole
     *
     * @return ProjectPole
     */
    public function getPole() {
        return $this->pole;
    }

    /**
     * Set createdBy
     *
     * @param integer $createdBy
     *
     * @return ProjectManager
     */
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return integer
     */
    public function getCreatedBy() {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param integer $updatedBy
     *
     * @return ProjectManager
     */
    public function setUpdatedBy($updatedBy) {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return integer
     */
    public function getUpdatedBy() {
        return $this->updatedBy;
    }

    /**
     * Set active
     *
     * @param string $active
     *
     * @return ProjectPole
     */
    public function setActive($active) {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return string
     */
    public function getActive() {
        return $this->active;
    }

}
