<?php

namespace Suivi\EtudesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DemandeUserColumns
 *
 * @ORM\Table(name="se_demande_user_columns", uniqueConstraints={@ORM\UniqueConstraint(name="user_id_UNIQUE", columns={"user_id"})})
 * @ORM\Entity
 */
class DemandeUserColumns
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="title", type="boolean", nullable=true)
     */
    private $title;

    /**
     * @var boolean
     *
     * @ORM\Column(name="date", type="boolean", nullable=true)
     */
    private $date;

    /**
     * @var boolean
     *
     * @ORM\Column(name="type", type="boolean", nullable=true)
     */
    private $type;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", nullable=true)
     */
    private $status;

    /**
     * @var boolean
     *
     * @ORM\Column(name="urgence", type="boolean", nullable=true)
     */
    private $urgence;

    /**
     * @var boolean
     *
     * @ORM\Column(name="application", type="boolean", nullable=true)
     */
    private $application;

    /**
     * @var boolean
     *
     * @ORM\Column(name="applicant", type="boolean", nullable=true)
     */
    private $applicant;

    /**
     * @var boolean
     *
     * @ORM\Column(name="departament", type="boolean", nullable=true)
     */
    private $departament;

    /**
     * @var boolean
     *
     * @ORM\Column(name="service", type="boolean", nullable=true)
     */
    private $service;

    /**
     * @var boolean
     *
     * @ORM\Column(name="observer", type="boolean", nullable=true)
     */
    private $observer;

    /**
     * @var boolean
     *
     * @ORM\Column(name="technician", type="boolean", nullable=true)
     */
    private $technician;

    /**
     * @var boolean
     *
     * @ORM\Column(name="project_manager", type="boolean", nullable=true)
     */
    private $projectManager;

    /**
     * @var boolean
     *
     * @ORM\Column(name="test_date", type="boolean", nullable=true)
     */
    private $testDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="prod_date", type="boolean", nullable=true)
     */
    private $prodDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="project_manager_hours", type="boolean", nullable=true)
     */
    private $projectManagerHours;

    /**
     * @var boolean
     *
     * @ORM\Column(name="technician_hours", type="boolean", nullable=true)
     */
    private $technicianHours;

    /**
     * @var boolean
     *
     * @ORM\Column(name="frequency", type="boolean", nullable=true)
     */
    private $frequency;

    /**
     * @var boolean
     *
     * @ORM\Column(name="origin", type="boolean", nullable=true)
     */
    private $origin;

    /**
     * @var boolean
     *
     * @ORM\Column(name="population", type="boolean", nullable=true)
     */
    private $population;

    /**
     * @var boolean
     *
     * @ORM\Column(name="comment_estimation", type="boolean", nullable=true)
     */
    private $commentEstimation;

    /**
     * @var boolean
     *
     * @ORM\Column(name="comment_test", type="boolean", nullable=true)
     */
    private $commentTest;

    /**
     * @var boolean
     *
     * @ORM\Column(name="comment_stand_by", type="boolean", nullable=true)
     */
    private $commentStandBy;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return DemandeUserColumns
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    
        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set title
     *
     * @param boolean $title
     *
     * @return DemandeUserColumns
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return boolean
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set date
     *
     * @param boolean $date
     *
     * @return DemandeUserColumns
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return boolean
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set type
     *
     * @param boolean $type
     *
     * @return DemandeUserColumns
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return boolean
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return DemandeUserColumns
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set urgence
     *
     * @param boolean $urgence
     *
     * @return DemandeUserColumns
     */
    public function setUrgence($urgence)
    {
        $this->urgence = $urgence;
    
        return $this;
    }

    /**
     * Get urgence
     *
     * @return boolean
     */
    public function getUrgence()
    {
        return $this->urgence;
    }

    /**
     * Set application
     *
     * @param boolean $application
     *
     * @return DemandeUserColumns
     */
    public function setApplication($application)
    {
        $this->application = $application;
    
        return $this;
    }

    /**
     * Get application
     *
     * @return boolean
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * Set applicant
     *
     * @param boolean $applicant
     *
     * @return DemandeUserColumns
     */
    public function setApplicant($applicant)
    {
        $this->applicant = $applicant;
    
        return $this;
    }

    /**
     * Get applicant
     *
     * @return boolean
     */
    public function getApplicant()
    {
        return $this->applicant;
    }

    /**
     * Set departament
     *
     * @param boolean $departament
     *
     * @return DemandeUserColumns
     */
    public function setDepartament($departament)
    {
        $this->departament = $departament;
    
        return $this;
    }

    /**
     * Get departament
     *
     * @return boolean
     */
    public function getDepartament()
    {
        return $this->departament;
    }

    /**
     * Set service
     *
     * @param boolean $service
     *
     * @return DemandeUserColumns
     */
    public function setService($service)
    {
        $this->service = $service;
    
        return $this;
    }

    /**
     * Get service
     *
     * @return boolean
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Set observer
     *
     * @param boolean $observer
     *
     * @return DemandeUserColumns
     */
    public function setObserver($observer)
    {
        $this->observer = $observer;
    
        return $this;
    }

    /**
     * Get observer
     *
     * @return boolean
     */
    public function getObserver()
    {
        return $this->observer;
    }

    /**
     * Set technician
     *
     * @param boolean $technician
     *
     * @return DemandeUserColumns
     */
    public function setTechnician($technician)
    {
        $this->technician = $technician;
    
        return $this;
    }

    /**
     * Get technician
     *
     * @return boolean
     */
    public function getTechnician()
    {
        return $this->technician;
    }

    /**
     * Set projectManager
     *
     * @param boolean $projectManager
     *
     * @return DemandeUserColumns
     */
    public function setProjectManager($projectManager)
    {
        $this->projectManager = $projectManager;
    
        return $this;
    }

    /**
     * Get projectManager
     *
     * @return boolean
     */
    public function getProjectManager()
    {
        return $this->projectManager;
    }

    /**
     * Set testDate
     *
     * @param boolean $testDate
     *
     * @return DemandeUserColumns
     */
    public function setTestDate($testDate)
    {
        $this->testDate = $testDate;
    
        return $this;
    }

    /**
     * Get testDate
     *
     * @return boolean
     */
    public function getTestDate()
    {
        return $this->testDate;
    }

    /**
     * Set prodDate
     *
     * @param boolean $prodDate
     *
     * @return DemandeUserColumns
     */
    public function setProdDate($prodDate)
    {
        $this->prodDate = $prodDate;
    
        return $this;
    }

    /**
     * Get prodDate
     *
     * @return boolean
     */
    public function getProdDate()
    {
        return $this->prodDate;
    }

    /**
     * Set projectManagerHours
     *
     * @param boolean $projectManagerHours
     *
     * @return DemandeUserColumns
     */
    public function setProjectManagerHours($projectManagerHours)
    {
        $this->projectManagerHours = $projectManagerHours;
    
        return $this;
    }

    /**
     * Get projectManagerHours
     *
     * @return boolean
     */
    public function getProjectManagerHours()
    {
        return $this->projectManagerHours;
    }

    /**
     * Set technicianHours
     *
     * @param boolean $technicianHours
     *
     * @return DemandeUserColumns
     */
    public function setTechnicianHours($technicianHours)
    {
        $this->technicianHours = $technicianHours;
    
        return $this;
    }

    /**
     * Get technicianHours
     *
     * @return boolean
     */
    public function getTechnicianHours()
    {
        return $this->technicianHours;
    }

    /**
     * Set frequency
     *
     * @param boolean $frequency
     *
     * @return DemandeUserColumns
     */
    public function setFrequency($frequency)
    {
        $this->frequency = $frequency;
    
        return $this;
    }

    /**
     * Get frequency
     *
     * @return boolean
     */
    public function getFrequency()
    {
        return $this->frequency;
    }

    /**
     * Set origin
     *
     * @param boolean $origin
     *
     * @return DemandeUserColumns
     */
    public function setOrigin($origin)
    {
        $this->origin = $origin;
    
        return $this;
    }

    /**
     * Get origin
     *
     * @return boolean
     */
    public function getOrigin()
    {
        return $this->origin;
    }

    /**
     * Set population
     *
     * @param boolean $population
     *
     * @return DemandeUserColumns
     */
    public function setPopulation($population)
    {
        $this->population = $population;
    
        return $this;
    }

    /**
     * Get population
     *
     * @return boolean
     */
    public function getPopulation()
    {
        return $this->population;
    }

    /**
     * Set commentEstimation
     *
     * @param boolean $commentEstimation
     *
     * @return DemandeUserColumns
     */
    public function setCommentEstimation($commentEstimation)
    {
        $this->commentEstimation = $commentEstimation;
    
        return $this;
    }

    /**
     * Get commentEstimation
     *
     * @return boolean
     */
    public function getCommentEstimation()
    {
        return $this->commentEstimation;
    }

    /**
     * Set commentTest
     *
     * @param boolean $commentTest
     *
     * @return DemandeUserColumns
     */
    public function setCommentTest($commentTest)
    {
        $this->commentTest = $commentTest;
    
        return $this;
    }

    /**
     * Get commentTest
     *
     * @return boolean
     */
    public function getCommentTest()
    {
        return $this->commentTest;
    }

    /**
     * Set commentStandBy
     *
     * @param boolean $commentStandBy
     *
     * @return DemandeUserColumns
     */
    public function setCommentStandBy($commentStandBy)
    {
        $this->commentStandBy = $commentStandBy;
    
        return $this;
    }

    /**
     * Get commentStandBy
     *
     * @return boolean
     */
    public function getCommentStandBy()
    {
        return $this->commentStandBy;
    }
}
