<?php

namespace Suivi\EtudesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * RecuperationList
 *
 * @ORM\Table(name="se_recuperation_list", indexes={@ORM\Index(name="tech_idx", columns={"user_id"}), @ORM\Index(name="manager_idx", columns={"validated_by"})})
 * @ORM\Entity(repositoryClass="Suivi\EtudesBundle\Entity\RecuperationListRepository")
 */
class RecuperationList {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AbsenceType
     *
     * @ORM\ManyToOne(targetEntity="AbsenceType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     * })
     * @Assert\NotBlank()
     */
    private $type;

    /**
     * @var \AbsenceTech
     *
     * @ORM\ManyToOne(targetEntity="ProjectTech")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
     * })
     * @Assert\NotBlank()
     */
    private $tech;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     * @Assert\NotBlank(
     *      message = "Cette valeur ne doit pas être vide."
     * )
     */
    private $createdAt = 'CURRENT_TIMESTAMP';

    /**
     * @var \Date
     *
     * @ORM\Column(name="start_date", type="date", nullable=false)
     * @Assert\NotBlank()
     */
    private $startDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="hours", type="integer", nullable=false)
     * @Assert\NotBlank(
     *      message = "Cette valeur ne doit pas être vide."
     * )
     * @Assert\GreaterThan (
     *      value = 0,
     *      message = "Cette valeur doit être supérieure à 0."
     * )
     */
    private $hours;

    /**
     * @var integer
     *
     * @ORM\Column(name="validated_by", type="integer", nullable=true)
     */
    private $validatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="validated_at", type="datetime", nullable=true)
     */
    private $validatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="refus_reason", type="text", length=65535, nullable=true)
     */
    private $refusReason;

    /**
     * @var string
     *
     * @ORM\Column(name="canceled", type="string", nullable=false)
     */
    private $canceled = '0';

    public function __construct() {
        $this->createdAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param \Suivi\EtudesBundle\Entity\AbsenceType $type
     *
     * @return RecuperationList
     */
    public function setType(\Suivi\EtudesBundle\Entity\AbsenceType $type = null) {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \Suivi\EtudesBundle\Entity\AbsenceType
     */
    public function getType() {
        return $this->type;
    }

    /**
     * Set tech
     *
     * @param \Suivi\EtudesBundle\Entity\ProjectTech $tech
     *
     * @return RecuperationList
     */
    public function setTech(\Suivi\EtudesBundle\Entity\ProjectTech $tech = null) {
        $this->tech = $tech;

        return $this;
    }

    /**
     * Get tech
     *
     * @return \Suivi\EtudesBundle\Entity\ProjectTech
     */
    public function getTech() {
        return $this->tech;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return RecuperationList
     */
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return RecuperationList
     */
    public function setStartDate($startDate) {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate() {
        return $this->startDate;
    }

    /**
     * Set hours
     *
     * @param integer $hours
     *
     * @return RecuperationList
     */
    public function setHours($hours) {
        $this->hours = $hours;

        return $this;
    }

    /**
     * Get hours
     *
     * @return integer
     */
    public function getHours() {
        return $this->hours;
    }

    /**
     * Set validatedBy
     *
     * @param integer $validatedBy
     *
     * @return RecuperationList
     */
    public function setValidatedBy($validatedBy) {
        $this->validatedBy = $validatedBy;

        return $this;
    }

    /**
     * Get validatedBy
     *
     * @return integer
     */
    public function getValidatedBy() {
        return $this->validatedBy;
    }

    /**
     * Set validatedAt
     *
     * @param \DateTime $validatedAt
     *
     * @return RecuperationList
     */
    public function setValidatedAt($validatedAt) {
        $this->validatedAt = $validatedAt;

        return $this;
    }

    /**
     * Get validatedAt
     *
     * @return \DateTime
     */
    public function getValidatedAt() {
        return $this->validatedAt;
    }

    /**
     * Set refusReason
     *
     * @param string $refusReason
     *
     * @return RecuperationList
     */
    public function setRefusReason($refusReason) {
        $this->refusReason = $refusReason;

        return $this;
    }

    /**
     * Get refusReason
     *
     * @return string
     */
    public function getRefusReason() {
        return $this->refusReason;
    }

    /**
     * Set canceled
     *
     * @param string $canceled
     *
     * @return RecuperationList
     */
    public function setCanceled($canceled) {
        $this->canceled = $canceled;

        return $this;
    }

    /**
     * Get canceled
     *
     * @return string
     */
    public function getCanceled() {
        return $this->canceled;
    }

}
