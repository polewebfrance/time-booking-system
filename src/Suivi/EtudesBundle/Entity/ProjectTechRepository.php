<?php

namespace Suivi\EtudesBundle\Entity;

use Doctrine\ORM\EntityRepository;

class ProjectTechRepository extends EntityRepository {

    public function getAll() {
        $data = $this->createQueryBuilder('tech')
                        ->select('u.id, u.displayname, p.name poleName, j.name jobName, tech.active, c.name country')
                        ->leftJoin('UserUserBundle:User', 'u', 'WITH', 'u.id=IDENTITY(tech.tech)')
                        ->leftJoin('SuiviEtudesBundle:ProjectPole', 'p', 'WITH', 'p.id=IDENTITY(tech.pole)')
                        ->leftJoin('SuiviEtudesBundle:ProjectJob', 'j', 'WITH', 'j.id=IDENTITY(tech.poste)')
                        ->leftJoin('SuiviEtudesBundle:ProjectCountry', 'c', 'WITH', 'c.id=IDENTITY(tech.country)')
                        ->orderBy('u.surname', 'ASC')
                        ->getQuery()->getResult();
        return array('data' => $data);
    }

    public function getHourlyTax($periode = null) {
        $data = $this->createQueryBuilder('tech')
                        ->select("IDENTITY(tech.tech) userId,CONCAT(user.givenname,' ', user.surname) userName,p.name pole,j.name poste"
                                . ",(CASE WHEN tax.tauxHoraire is null THEN tech.tauxHoraire ELSE tax.tauxHoraire END) as tauxHoraire,"
                                . "tech.capacite,country.name countryName")
                        ->leftJoin('UserUserBundle:User', 'user', 'WITH', 'user.id=IDENTITY(tech.tech)')
                        ->leftJoin('SuiviEtudesBundle:ProjectPole', 'p', 'WITH', 'p.id=IDENTITY(tech.pole)')
                        ->leftJoin('SuiviEtudesBundle:ProjectJob', 'j', 'WITH', 'j.id=IDENTITY(tech.poste)')
                        ->leftJoin('SuiviEtudesBundle:ProjetTauxHoraire', 'tax', 'WITH', 'IDENTITY(tech.tech)=tax.userId AND tax.period = :period')
                        ->leftJoin('SuiviEtudesBundle:ProjetUserCapacite', 'c', 'WITH', 'c.userId=IDENTITY(tech.tech) AND c.period = :period')
                        ->leftJoin('SuiviEtudesBundle:ProjectCountry', 'country', 'WITH', 'country.id=IDENTITY(tech.country)')
                        ->setParameter('period', $periode)
                        ->where("tech.active = '1'")
                        ->orderBy('user.surname', 'ASC')
                        ->getQuery()->getResult();
        return array('data' => $data);
    }

    public function getChargeDeTravail($period, $managerId = null) {
        $data = $this->createQueryBuilder('usr')
                        ->select('CONCAT(user.givenname,\' \', user.surname) userName', 'j.name poste', 'p.name pole'
                                , 'cap.period', '(cap.hours/100*cap.capacity) prevHours', 'cap.remainingHours', 'cap.hours capTotalHours', 'cap.capacity'
                                , 'ch.hours chargeHours', 'ch.hoursWorked chargeHoursWorked', 'd.titre request', 'd.id demandeId'
                        )
                        ->leftJoin('UserUserBundle:User', 'user', 'WITH', 'user.id=IDENTITY(usr.tech)')
                        ->leftJoin('SuiviEtudesBundle:ProjetUserCapacite', 'cap', 'WITH', 'user.id=cap.userId AND cap.period = :period')
                        ->leftJoin('SuiviEtudesBundle:ProjetCharge', 'ch', 'WITH', 'ch.userId=user.id and ch.period = cap.period')
                        ->leftJoin('SuiviEtudesBundle:Demande', 'd', 'WITH', 'd.id=ch.demandeId')
                        ->leftJoin('SuiviEtudesBundle:ProjectPole', 'p', 'WITH', 'p.id=IDENTITY(usr.pole)')
                        ->leftJoin('SuiviEtudesBundle:ProjectJob', 'j', 'WITH', 'j.id=IDENTITY(usr.poste)')
                        ->leftJoin('SuiviEtudesBundle:ProjectManager', 'm', 'WITH', 'IDENTITY(m.pole)=IDENTITY(usr.pole)')
                        ->where('usr.active = \'1\'')
                        ->andWhere("IDENTITY(m.manager) = :managerId")
                        ->orderBy('usr.pole', 'DESC')
                        ->addOrderBy('user.surname', 'ASC')
                        ->addOrderBy('cap.period')
                        ->setParameter('period', $period . "-01")
                        ->setParameter('managerId', $managerId)
                        ->getQuery()->getResult();
        $userCharge = array();
        foreach ($data as $row) {
            $date = null !== $row['period'] ? $row['period']->format('M-Y') : null;
            $userCharge[$row['userName']]['username'] = $row['userName'];
            $userCharge[$row['userName']]['poste'] = $row['poste'];
            $userCharge[$row['userName']]['pole'] = $row['pole'];
            if (null !== $row['remainingHours']) {
                $userCharge[$row['userName']]['details'][$date][$row['demandeId']] = array(
                    'remainingHours' => $row['remainingHours'],
                    'prevHours' => $row['prevHours'],
                    'chargeHours' => $row['chargeHours'],
                    'chargeHoursWorked' => $row['chargeHoursWorked'],
                    'request' => $row['request'],
                    'id' => $row['demandeId'],
                    'capTotalHours' => $row['capTotalHours'],
                    'capacity' => $row['capacity'],
                );
            }
        }
        return $userCharge;
    }

    // returns holidays by year and country
    public function showHolidays($year = null, $country = null) {
        $holidays = array();
        $firstDayOfYear = new \DateTime(date("$year-01-01"));
        $labourDay = new \DateTime(date("$year-05-01"));
        $stMaryDay = new \DateTime(date("$year-08-15"));
        $stAndreiDay = new \DateTime(date("$year-11-30"));
        $firstDayChristman = new \DateTime(date("$year-12-25"));
        if ($country == "ROU") {
            $romaniaDay = new \DateTime(date("$year-12-01"));
            $seccondDayOfYear = new \DateTime(date("$year-01-02"));
            $seccondChristman = new \DateTime(date("$year-12-26"));
            // easter
            $a = $year % 4;
            $b = $year % 7;
            $c = $year % 19;
            $d = (19 * $c + 15) % 30;
            $e = (2 * $a + 4 * $b - $d + 34) % 7;
            $month = floor(($d + $e + 114) / 31);
            $day = (($d + $e + 114) % 31) + 1;
            $firstEasterDay = new \DateTime(date('Y-m-d', mktime(0, 0, 0, $month, $day + 13, $year)));
            $nextEasterDay = new \DateTime(date('Y-m-d', mktime(0, 0, 0, $month, $day + 14, $year)));
            $firstRusaliiDay = new \DateTime(date('Y-m-d', mktime(0, 0, 0, $month, $day + 13 + 49, $year)));
            $nextRusaliiDay = new \DateTime(date('Y-m-d', mktime(0, 0, 0, $month, $day + 14 + 49, $year)));
            // end easter
            $holidays = array(
                $firstDayOfYear, $seccondDayOfYear, $firstEasterDay, $nextEasterDay, $firstRusaliiDay, $nextRusaliiDay,
                $labourDay, $stMaryDay, $stAndreiDay, $romaniaDay, $firstDayChristman, $seccondChristman
            );
        } else { # FRANCE
            $march21 = new \DateTime(date("$year-03-21"));
            $march21Seccond = new \DateTime(date("$year-03-21"));
            $daysToAdd = easter_days($year);
            $daysToAddNext = (easter_days($year)) + 1;
            $firstEasterDay = $march21->modify("+$daysToAdd day");
            $seccondEasterDay = new \DateTime(date('Y-m-d', strtotime($firstEasterDay->format('Y-m-d') . ' +1 day')));
            $ascencion = new \DateTime(date('Y-m-d', strtotime($firstEasterDay->format('Y-m-d') . ' +39 day')));
            $lundiPent = new \DateTime(date('Y-m-d', strtotime($firstEasterDay->format('Y-m-d') . ' +50 day')));
            $victoryDay = new \DateTime(date("$year-05-08"));
            $franceDay = new \DateTime(date("$year-07-14"));
            $toussaint = new \DateTime(date("$year-11-01"));
            $armistice = new \DateTime(date("$year-11-11"));
            $holidays = array(
                $firstDayOfYear, $firstEasterDay, $seccondEasterDay, $labourDay, $victoryDay,
                $ascencion, $lundiPent, $franceDay, $toussaint, $armistice, $stMaryDay, $firstDayChristman
            );
        }
        return $holidays;
    }

    /*
     * This function returns the difference between 2 datetime objects minus weekends
     */

    public function dayDifference($from, $to, $countryName, $year) {
        $current = clone $from;
        $sum = 0;
        while ($current < $to) {
            $endSlice = clone $current;
            $endSlice->setTime(0, 0, 0);
            $endSlice->modify('+1 day');
            if ($endSlice > $to) {
                $endSlice = clone $to;
            }
            $seconds = $endSlice->getTimestamp() - $current->getTimestamp();
            $currentDay = $current->format("D");
            $holidays = $this->showHolidays($year, $countryName);
            // if day is not weekend and not holliday then add to days
            if (($currentDay != 'Sat' && $currentDay != 'Sun') && !in_array($current, $holidays)) {
                $sum+=$seconds;
            }
            $current = $endSlice;
        }
        $days = ($sum / 60 / 60 / 24) + 1; #return days of absence
        return $days; # return days
    }

    public function getTechCapacityByManagerId($managerId = null, $periode = null) {
        $lastDayOfMonth = new \DateTime(date('Y-m-t', strtotime($periode)));
        $firstDayOfMonth = new \DateTime(date('Y-m-01', strtotime($periode)));
        $data = $this->createQueryBuilder('tech')
                        ->select('CONCAT(user.givenname,\' \', user.surname) displayname, p.name poleName, j.name jobName, (CASE WHEN c.capacity is null THEN tech.capacite ELSE c.capacity END) as capacity'
                                . ',(CASE WHEN c.hours is null THEN 0 ELSE c.hours END) as hours,country.short countryName,user.id userId'
                                . ',user.givenname lastname, user.surname firstname'
                                . ",CASE WHEN abs.endDate > :lastDayOfMonth THEN 0 ELSE abs.days END absenceDays, abs.startDate, abs.endDate, MIN(abs.startDate) minDate, MAX(abs.endDate) maxDate"
                                . ",abs.validatedBy,abs.canceled,abs.refusReason")
                        ->leftJoin('UserUserBundle:User', 'user', 'WITH', 'user.id=IDENTITY(tech.tech)')
                        ->leftJoin('SuiviEtudesBundle:ProjectPole', 'p', 'WITH', 'p.id=IDENTITY(tech.pole)')
                        ->leftJoin('SuiviEtudesBundle:ProjectJob', 'j', 'WITH', 'j.id=IDENTITY(tech.poste)')
                        ->leftJoin('SuiviEtudesBundle:ProjectCountry', 'country', 'WITH', 'country.id=IDENTITY(tech.country)')
                        ->leftJoin('SuiviEtudesBundle:ProjetUserCapacite', 'c', 'WITH', 'c.userId=IDENTITY(tech.tech) AND c.period = :period')
                        ->leftJoin('SuiviEtudesBundle:ProjectManager', 'm', 'WITH', 'IDENTITY(m.pole)=IDENTITY(tech.pole)')
                        ->leftJoin('SuiviEtudesBundle:AbsenceList', 'abs', 'WITH', 'IDENTITY(abs.tech)=IDENTITY(tech.tech)')
                        ->where("tech.active = '1'")
                        ->andWhere("IDENTITY(m.manager) = :managerId")
                        ->groupBy('user.id')
                        ->orderBy('user.givenname', 'ASC')
                        ->setParameter('period', $periode)
                        ->setParameter('managerId', $managerId)
                        ->setParameter('lastDayOfMonth', $lastDayOfMonth->format('Y-m-t'))
                        ->having("(abs.startDate <= :lastDayOfMonth and abs.endDate >= :period ) OR abs.startDate IS NULL")
                        ->getQuery()->getResult();
        # create a new array and add the right number of absences for that month
        $techData = array();
        foreach ($data as $row) {
            if (!array_key_exists($row['userId'], $techData)) {
                $techData[$row['userId']] = $row;
            }
            # add data only if absence request was validated and not canceled
            if ((null !== $row['validatedBy'] && '0' == $row['canceled'] && null == $row['refusReason'])) {
                $days = (int) $row['absenceDays'];
                $startDate = new \Datetime($row['minDate']) < $firstDayOfMonth ? $firstDayOfMonth : $row['startDate'];
                $endDate = new \Datetime($row['maxDate']) < $lastDayOfMonth ? new \Datetime($row['maxDate']) : $row['endDate'];
                $days = $this->dayDifference($startDate, $endDate, $row['countryName'], $startDate->format('Y'));
                $techData[$row['userId']]['absenceDaysCalculated'] = $days; # prevent error whe adding the first result then add to this result
            }
        }
        $techData = array_values($techData); # reindex array so that datatables can parse the json in controller
        return array('data' => $techData);
    }

    //get recuperation hours by manager
    public function getRecHours($managerId, $year) {
        $data = $this->createQueryBuilder('tech')
                        ->select('CONCAT(user.givenname,\' \', user.surname) userName,p.name pole,j.name poste, (CASE WHEN i.recHours IS NULL THEN \'N/A\' ELSE i.recHours END) AS recHours')
                        ->leftJoin('SuiviEtudesBundle:AbsenceTechInfo', 'i', 'WITH', 'IDENTITY(i.tech)=IDENTITY(tech.tech)')
                        ->leftJoin('UserUserBundle:User', 'user', 'WITH', 'user.id=IDENTITY(tech.tech)')
                        ->leftJoin('SuiviEtudesBundle:ProjectPole', 'p', 'WITH', 'p.id=IDENTITY(tech.pole)')
                        ->leftJoin('SuiviEtudesBundle:ProjectJob', 'j', 'WITH', 'j.id=IDENTITY(tech.poste)')
                        ->leftJoin('SuiviEtudesBundle:ProjectManager', 'm', 'WITH', 'IDENTITY(m.pole)=p.id')
                        ->where('IDENTITY(m.manager) = :managerId')
                        ->setParameter('managerId', $managerId)
                        ->andWhere('i.year = :year') #get info only for techs that have rec hours for this year
//                        ->andWhere('(i.year = :year OR i.year IS NULL)') #get info for all techs
                        ->setParameter('year', $year)
                        ->andWhere('tech.active = :active')
                        ->setParameter('active', '1')
                        ->andWhere('i.recHours > 0')
                        ->orderBy('user.surname', 'ASC')
                        ->getQuery()->getResult();
        return array('data' => $data);
    }

    function checkDateTime($dateString) {
        if (date('Y-m-d', strtotime($dateString)) == $dateString) {
            return true;
        } else {
            return false;
        }
    }

    // PONTAJ
    public function timekeeping($period, $poleIds, $country = 0, $departament = null) {
        $validDate = $this->checkDateTime($period);
        if (true === $validDate) {
            $year = date('Y', strtotime($period));
            $date = new \DateTime($period);
            $startDate = $date->modify('first day of this month'); #first day of the month
            $endDate = clone $date;
            $endDate = $endDate->modify("last day of this month"); #last day of the month
            $daysNo = $endDate->diff($startDate)->format("%a") + 1;
            // get all the active techs and there absence informations
            $qb = $this->createQueryBuilder('tech')
                    ->select('user.id userId,CONCAT(user.givenname,\' \', user.surname) userName, list.startDate, list.endDate,'
                            . 'type.abbrev abs, country.short countryName')
                    ->leftJoin('UserUserBundle:User', 'user', 'WITH', 'user.id=IDENTITY(tech.tech)')
                    ->leftJoin('SuiviEtudesBundle:ProjectPole', 'p', 'WITH', 'p.id=IDENTITY(tech.pole)')
                    ->leftJoin('SuiviEtudesBundle:AbsenceList', 'list', 'WITH', 'IDENTITY(list.tech)=IDENTITY(tech.tech)')
                    ->leftJoin('SuiviEtudesBundle:AbsenceType', 'type', 'WITH', 'type.id=IDENTITY(list.type)')
                    ->leftJoin('SuiviEtudesBundle:ProjectCountry', 'country', 'WITH', 'country.id=IDENTITY(tech.country)')
                    ->where('tech.active = :active')
                    ->setParameter('active', '1')
                    ->andWhere('p.id in (:poleIds)')
                    ->setParameter('poleIds', $poleIds)
                    ->andWhere("(list.startDate <= :endDate AND list.endDate >= :startDate) OR user.id IS NOT NULL")
                    ->andWhere('(list.validatedBy IS NOT NULL AND list.canceled = \'0\' AND list.refusReason IS NULL) OR IDENTITY(list.tech) IS NULL')
                    ->setParameter('startDate', $startDate->format('Y-m-d'))
                    ->setParameter('endDate', $endDate->format('Y-m-d'));
            if ($country > 0) {
                $qb
                        ->andWhere('country.id = :country')
                        ->setParameter('country', $country);
            }
            if ($departament > 0) {
                $qb
                        ->andWhere('p.id IN(:departament)')
                        ->setParameter('departament', $departament);
            }
            $activeTechs = $qb->orderBy('user.surname', 'ASC')
                    ->getQuery()->getResult();
            $data = $techsInfo = $daysInfo = array();
            $frenchDays = array(
                'Mon' => 'L',
                'Tue' => 'M',
                'Wed' => 'M',
                'Thu' => 'J',
                'Fri' => 'V',
                'Sat' => 'S',
                'Sun' => 'D'
            );
            for ($i = 1; $i <= $daysNo; $i++) {
                $newDate = clone $startDate;
                foreach ($activeTechs as $k => $row) {
                    $holidays = $this->showHolidays($year, $row['countryName']);
                    $data[$row['userId']]['name'] = $row['userName'];
                    $isAbsent = $row['startDate'] <= $newDate && $row['endDate'] >= $newDate && !in_array($newDate, $holidays); #check absence period
                    $isHolliday = in_array($newDate, $holidays);
                    if ($isHolliday === true) {
                        @$data[$row['userId']]['days'][$newDate->format('Y-m-d')]['JF'] .= $isAbsent; #is holliday
                    } else {
                        @$data[$row['userId']]['days'][$newDate->format('Y-m-d')][$row['abs']] .= $isAbsent; #absence name or not
                    }
                }
                // add info for each day
                $dayNo = (int) $newDate->format('d');
                $daysInfo[$dayNo] = $frenchDays[$newDate->format('D')];
                $newDate = $startDate->modify('+1 day');
            }

            foreach ($data as $tech) {
                foreach ($tech['days'] as $day => $dayInfo) {
                    $type = (array_search(true, $dayInfo) == true) ? array_search(true, $dayInfo) : '-';
                    # add holliday type
                    if (isset($dayInfo['JF'])) {
                        $type = 'JF';
                    }
                    $techsInfo[$tech['name']]['days'][] = $type;
                }
            }
            return array(
                'daysInfo' => $daysInfo,
                'techsInfo' => $techsInfo
            );
        }
        return null;
    }

    public function getTechListByManagerId($managerId) {
        $techs = $this->createQueryBuilder('t')
                ->innerJoin('UserUserBundle:User', 'u', 'WITH', 'u.id=IDENTITY(t.tech)')
                ->innerJoin('SuiviEtudesBundle:ProjectPole', 'p', 'WITH', 'p.id=IDENTITY(t.pole)')
                ->innerJoin('SuiviEtudesBundle:ProjectManager', 'm', 'WITH', 'IDENTITY(m.pole)=p.id AND IDENTITY(m.manager)=' . $managerId)
                ->where("t.active = '1'")
                ->andWhere("m.active = '1'")
                ->orderBy('u.givenname', 'ASC');
        return $techs;
    }

}
