<?php

namespace  Suivi\EtudesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * AbsenceTechInfo
 *
 * @ORM\Table(name="se_absence_tech_info", uniqueConstraints={@ORM\UniqueConstraint(name="absence_tech_and_year", columns={"user_id", "year"})}, indexes={@ORM\Index(name="absence_user_id_idx", columns={"user_id"})})
 * @ORM\Entity
 * @UniqueEntity(
 *      fields={"year", "tech"},
 *      message="Ce anneee est déjà utilisé."
 * )
 */
class AbsenceTechInfo {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="year", type="string", length=255,nullable=false)
     * @Assert\NotBlank()
     * @Assert\Range(
     *      min = 2000,
     *      max = 2099,
     *      minMessage = "L'année n'est pas valide",
     *      maxMessage = "L'année n'est pas valide"
     * )
     */
    private $year;

    /**
     * @var integer
     *
     * @ORM\Column(name="vacation_days", type="integer", nullable=false)
     * @Assert\NotBlank()
     * @Assert\Type("integer")
     */
    private $vacationDays;

    /**
     * @var integer
     *
     * @ORM\Column(name="days_left", type="integer", nullable=false)
     * @Assert\NotBlank()
     */
    private $daysLeft;

    /**
     * @var integer
     *
     * @ORM\Column(name="rec_hours", type="integer", nullable=true)
     * @Assert\Type("integer")
     */
    private $recHours = 0;

    /**
     * @var \AbsenceTech
     *
     * @ORM\ManyToOne(targetEntity="ProjectTech", inversedBy="techInfo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
     * })
     */
    private $tech;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set year
     *
     * @param string $year
     *
     * @return AbsenceTechInfo
     */
    public function setYear($year) {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return string
     */
    public function getYear() {
        return $this->year;
    }

    /**
     * Set vacationDays
     *
     * @param integer $vacationDays
     *
     * @return AbsenceTechInfo
     */
    public function setVacationDays($vacationDays) {
        $this->vacationDays = $vacationDays;
        if(null == $this->getDaysLeft()){
            $this->daysLeft = $vacationDays;
        }

        return $this;
    }

    /**
     * Get vacationDays
     *
     * @return integer
     */
    public function getVacationDays() {
        return $this->vacationDays;
    }

    /**
     * Set daysLeft
     *
     * @param integer $daysLeft
     *
     * @return AbsenceTechInfo
     */
    public function setDaysLeft($daysLeft) {
        $this->daysLeft = $daysLeft;

        return $this;
    }

    /**
     * Get daysLeft
     *
     * @return integer
     */
    public function getDaysLeft() {
        return $this->daysLeft;
    }

    /**
     * Set recHours
     *
     * @param integer $recHours
     *
     * @return AbsenceTechInfo
     */
    public function setRecHours($recHours) {
        $this->recHours = $recHours;

        return $this;
    }

    /**
     * Get recHours
     *
     * @return integer
     */
    public function getRecHours() {
        return $this->recHours;
    }

    /**
     * Set tech
     *
     * @param \Suivi\EtudesBundle\Entity\ProjectTech $tech
     *
     * @return AbsenceTechInfo
     */
    public function setTech(\Suivi\EtudesBundle\Entity\ProjectTech $tech = null) {
        $this->tech = $tech;

        return $this;
    }

    /**
     * Get tech
     *
     * @return \Suivi\EtudesBundle\Entity\ProjectTech 
     */
    public function getTech() {
        return $this->tech;
    }

}
