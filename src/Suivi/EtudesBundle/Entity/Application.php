<?php

namespace Suivi\EtudesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Suivi\EtudesBundle\Entity\Categorie;

/**
 * Application
 *
 * @ORM\Table(name="se_application")
 * @ORM\Entity(repositoryClass="Suivi\EtudesBundle\Entity\ApplicationRepository")
 */
class Application
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="Suivi\EtudesBundle\Entity\Categorie", inversedBy="applications")
     * @ORM\JoinColumn(nullable=false)
     */
    private $categorie;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Application
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set categorie
     *
     * @param \Suivi\EtudesBundle\Entity\Categorie $categorie
     * @return Application
     */
    public function setCategorie(\Suivi\EtudesBundle\Entity\Categorie $categorie)
    {
        $this->categorie = $categorie;

        $categorie->addApplication($this);
        
        return $this;
    }

    /**
     * Get categorie
     *
     * @return \Suivi\EtudesBundle\Entity\Categorie 
     */
    public function getCategorie()
    {
        return $this->categorie;
    }
    
    public function __toString()
    {
    	return $this->getName();
    }
}
