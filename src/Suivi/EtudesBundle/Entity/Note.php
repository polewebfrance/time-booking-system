<?php

namespace Suivi\EtudesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Note
 *
 * @ORM\Table(name="se_note", indexes={@ORM\Index(name="id_demande", columns={"id_demande"}), @ORM\Index(name="id_client", columns={"id_client"})})
 * @ORM\Entity
 */
class Note
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="note_delay", type="integer", nullable=false)
     */
    private $noteDelay;

    /**
     * @var integer
     *
     * @ORM\Column(name="note_quality", type="integer", nullable=false)
     */
    private $noteQuality;

    /**
     * @var integer
     *
     * @ORM\Column(name="note_production", type="integer", nullable=false)
     */
    private $noteProduction;

    /**
     * @var \User\UserBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_client", referencedColumnName="id")
     * })
     */
    private $idClient;

    /**
     * @var \Demande
     *
     * @ORM\ManyToOne(targetEntity="Demande")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_demande", referencedColumnName="id")
     * })
     */
    private $idDemande;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_creation", type="datetime", nullable=false)
     */
    private $dateCreation;

    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     *
     * @return Note
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set noteDelay
     *
     * @param integer $noteDelay
     *
     * @return Note
     */
    public function setNoteDelay($noteDelay)
    {
        $this->noteDelay = $noteDelay;
    
        return $this;
    }

    /**
     * Get noteDelay
     *
     * @return integer
     */
    public function getNoteDelay()
    {
        return $this->noteDelay;
    }

    /**
     * Set noteQuality
     *
     * @param integer $noteQuality
     *
     * @return Note
     */
    public function setNoteQuality($noteQuality)
    {
        $this->noteQuality = $noteQuality;
    
        return $this;
    }

    /**
     * Get noteQuality
     *
     * @return integer
     */
    public function getNoteQuality()
    {
        return $this->noteQuality;
    }

    /**
     * Set noteProduction
     *
     * @param integer $noteProduction
     *
     * @return Note
     */
    public function setNoteProduction($noteProduction)
    {
        $this->noteProduction = $noteProduction;
    
        return $this;
    }

    /**
     * Get noteProduction
     *
     * @return integer
     */
    public function getNoteProduction()
    {
        return $this->noteProduction;
    }

    /**
     * Set idClient
     *
     * @param \User\UserBundle\Entity\User $idClient
     *
     * @return Note
     */
    public function setIdClient(\User\UserBundle\Entity\User $idClient = null)
    {
        $this->idClient = $idClient;
    
        return $this;
    }

    /**
     * Get idClient
     *
     * @return \User\UserBundle\Entity\User
     */
    public function getIdClient()
    {
        return $this->idClient;
    }

    /**
     * Set idDemande
     *
     * @param \Suivi\EtudesBundle\Entity\Demande $idDemande
     *
     * @return Note
     */
    public function setIdDemande(\Suivi\EtudesBundle\Entity\Demande $idDemande = null)
    {
        $this->idDemande = $idDemande;
    
        return $this;
    }

    /**
     * Get idDemande
     *
     * @return \Suivi\EtudesBundle\Entity\Demande
     */
    public function getIdDemande()
    {
        return $this->idDemande;
    }
}
