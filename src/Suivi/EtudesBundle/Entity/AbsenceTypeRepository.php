<?php

namespace Suivi\EtudesBundle\Entity;

use Doctrine\ORM\EntityRepository;

class AbsenceTypeRepository extends EntityRepository {

    public function getAll() {
        $data = $this->createQueryBuilder('a')
                        ->select('a.id,a.name,a.abbrev,a.techVisible')
                        ->orderBy('a.name', 'ASC')
                        ->getQuery()->getResult();
        return array('data' => $data);
    }

}
