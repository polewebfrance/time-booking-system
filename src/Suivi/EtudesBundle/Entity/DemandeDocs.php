<?php

namespace Suivi\EtudesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DemandeDocs
 *
 * @ORM\Table(name="se_demande_docs", indexes={@ORM\Index(name="demande_id_idx", columns={"demande_id"})})
 * @ORM\Entity
 */
class DemandeDocs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="text", length=65535, nullable=false)
     */
    private $path;
    
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var \Demande
     *
     * @ORM\ManyToOne(targetEntity="Demande", inversedBy="demandeDocs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="demande_id", referencedColumnName="id")
     * })
     */
    private $demande;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return DemandeDocs
     */
    public function setPath($path)
    {
        $this->path = $path;
    
        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }
    
    /**
     * Set name
     *
     * @param string $name
     *
     * @return DemandeDocs
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set demande
     *
     * @param \Suivi\EtudesBundle\Entity\Demande $demande
     *
     * @return DemandeDocs
     */
    public function setDemande(\Suivi\EtudesBundle\Entity\Demande $demande = null)
    {
        $this->demande = $demande;
    
        return $this;
    }

    /**
     * Get demande
     *
     * @return \Suivi\EtudesBundle\Entity\Demande
     */
    public function getDemande()
    {
        return $this->demande;
    }
}
