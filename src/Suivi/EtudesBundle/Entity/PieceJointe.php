<?php

namespace Suivi\EtudesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * PieceJointe
 *
 * @ORM\Table(name="se_piecejointe")
 * @ORM\Entity(repositoryClass="Suivi\EtudesBundle\Entity\PieceJointeRepository")
 * @ORM\HasLifecycleCallbacks
 */
class PieceJointe {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;
    private $file;
    // On ajoute cet attribut pour y stocker le nom du fichier temporairement
    private $tempFilename;

    /**
     * @ORM\ManyToOne(targetEntity="Suivi\EtudesBundle\Entity\Demande", inversedBy="piecejointes")
     * @ORM\JoinColumn()
     * @ORM\JoinColumn(nullable=false, name="demande_id", referencedColumnName="id")
     */
    private $demande;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $alt
     * @return PieceJointe
     */
    public function setName($name) {
        $nomCorrige = str_replace(" ", "_", $name);
        $nomCorrige = str_replace("é", "e", $nomCorrige);
        $nomCorrige = str_replace("è", "e", $nomCorrige);
        $nomCorrige = str_replace("ê", "e", $nomCorrige);
        $nomCorrige = str_replace("à", "a", $nomCorrige);
        $nomCorrige = str_replace("ù", "u", $nomCorrige);
        $nomCorrige = str_replace("#", "", $nomCorrige);
        $nomCorrige = iconv("UTF-8", "ASCII//TRANSLIT", $nomCorrige);
        $this->name = $nomCorrige;
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    public function getFile() {
        return $this->file;
    }

    // On modifie le setter de File, pour prendre en compte l'upload d'un fichier lorsqu'il en existe déjà un autre
    public function setFile(UploadedFile $file) {
        $this->file = $file;

        // On vérifie si on avait déjà un fichier pour cette entité
        if (null !== $this->name) {
            // On réinitialise les valeurs des attributs url et alt
            $this->name = null;
        }
        $this->setName($this->file->getClientOriginalName());
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload() {
        // Si jamais il n'y a pas de fichier (champ facultatif)
        if (null === $this->file) {
            return;
        }
        $this->setName($this->file->getClientOriginalName());
        // var_dump($this->name);
        // die();
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload() {
        // Si jamais il n'y a pas de fichier (champ facultatif)
        if (null === $this->file) {
            return;
        }

        // Si on avait un ancien fichier, on le supprime
        if (null !== $this->tempFilename) {
            $oldFile = $this->getUploadRootDir() . '/' . $this->alt;
            if (file_exists($oldFile)) {
                unlink($oldFile);
            }
        }

        // On déplace le fichier envoyé dans le répertoire de notre choix
        $this->file->move(
                $this->getUploadRootDir(), // Le répertoire de destination
                $this->getName()   // Le nom du fichier à créer, ici « id.extension »
        );
    }

    /**
     * @ORM\PreRemove()
     */
    public function preRemoveUpload() {
        // On sauvegarde temporairement le nom du fichier, car il dépend de l'id
        $this->tempFilename = $this->getUploadRootDir() . '/' . $this->name;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload() {
        // En PostRemove, on n'a pas accès à l'id, on utilise notre nom sauvegardé
        if (file_exists($this->tempFilename)) {
            // On supprime le fichier
            unlink($this->tempFilename);
        }
    }

    public function getUploadDir() {
        $repName = $this->getDemande()->getId();
        // On retourne le chemin relatif vers l'image pour un navigateur
        return 'uploads/attachments/' . $repName;
    }

    protected function getUploadRootDir() {
        // On retourne le chemin relatif vers l'image pour notre code PHP
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    /**
     * Set demande
     *
     * @param \Suivi\EtudesBundle\Entity\Demande $demande
     * @return PieceJointe
     */
    public function setDemande(\Suivi\EtudesBundle\Entity\Demande $demande) {
        $this->demande = $demande;

        return $this;
    }

    /**
     * Get demande
     *
     * @return \Suivi\EtudesBundle\Entity\Demande 
     */
    public function getDemande() {
        return $this->demande;
    }

    function __toString() {
        return $this->getName();
    }

}
