<?php

namespace Suivi\EtudesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AbsenceList
 *
 * @ORM\Table(name="se_absence_list", indexes={@ORM\Index(name="type_idx", columns={"type_id"}), @ORM\Index(name="user_idx", columns={"user_id"}), @ORM\Index(name="manager_idx", columns={"validated_by"})})
 * @ORM\Entity(repositoryClass="Suivi\EtudesBundle\Entity\AbsenceListRepository")
 */
class AbsenceList {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     * @Assert\NotBlank(
     *      message = "Cette valeur ne doit pas être vide."
     * )
     */
    private $createdAt = 'CURRENT_TIMESTAMP';

    /**
     * @var \Date
     *
     * @ORM\Column(name="start_date", type="date", nullable=false)
     * @Assert\NotBlank()
     */
    private $startDate;

    /**
     * @var \Date
     *
     * @ORM\Column(name="end_date", type="datetime", nullable=false)
     * @Assert\NotBlank(
     *      message = "Cette valeur ne doit pas être vide."
     * )
     */
    private $endDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="days", type="integer", nullable=false)
     * @Assert\NotBlank(
     *      message = "Cette valeur ne doit pas être vide."
     * )
     * @Assert\GreaterThan (
     *      value = 0,
     *      message = "Cette valeur doit être supérieure à 0."
     * )
     */
    private $days;

    /**
     * @var integer
     *
     * @ORM\Column(name="validated_by", type="integer", nullable=true)
     */
    private $validatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="validated_at", type="datetime", nullable=true)
     */
    private $validatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="refus_reason", type="text", length=65535, nullable=true)
     */
    private $refusReason;

    /**
     * @var string
     *
     * @ORM\Column(name="canceled", type="string", nullable=false)
     */
    private $canceled = '0';

    /**
     * @var \AbsenceTech
     *
     * @ORM\ManyToOne(targetEntity="ProjectTech")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
     * })
     * @Assert\NotBlank()
     */
    private $tech;

    /**
     * @var \AbsenceType
     *
     * @ORM\ManyToOne(targetEntity="AbsenceType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     * })
     * @Assert\NotBlank()
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="validated_by", referencedColumnName="id")
     * })
     */
    private $manager;

    public function __construct() {
        $this->createdAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return AbsenceList
     */
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * Set start
     *
     * @param \Date $startDate
     *
     * @return AbsenceList
     */
    public function setStartDate($startDate) {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \Date
     */
    public function getStartDate() {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \Date $endDate
     *
     * @return AbsenceList
     */
    public function setEndDate($endDate) {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \Date
     */
    public function getEndDate() {
        return $this->endDate;
    }

    /**
     * Set days
     *
     * @param integer $days
     *
     * @return AbsenceList
     */
    public function setDays($days) {
        $this->days = $days;

        return $this;
    }

    /**
     * Get days
     *
     * @return integer
     */
    public function getDays() {
        return $this->days;
    }

    /**
     * Set validatedBy
     *
     * @param integer $validatedBy
     *
     * @return AbsenceList
     */
    public function setValidatedBy($validatedBy) {
        $this->validatedBy = $validatedBy;

        return $this;
    }

    /**
     * Get validatedBy
     *
     * @return integer
     */
    public function getValidatedBy() {
        return $this->validatedBy;
    }

    /**
     * Set validatedAt
     *
     * @param \DateTime $validatedAt
     *
     * @return AbsenceList
     */
    public function setValidatedAt($validatedAt) {
        $this->validatedAt = $validatedAt;

        return $this;
    }

    /**
     * Get validatedAt
     *
     * @return \DateTime
     */
    public function getValidatedAt() {
        return $this->validatedAt;
    }

    /**
     * Set refusReason
     *
     * @param string $refusReason
     *
     * @return AbsenceList
     */
    public function setRefusReason($refusReason) {
        $this->refusReason = $refusReason;

        return $this;
    }

    /**
     * Get refusReason
     *
     * @return string
     */
    public function getRefusReason() {
        return $this->refusReason;
    }

    /**
     * Set canceled
     *
     * @param string $canceled
     *
     * @return AbsenceList
     */
    public function setCanceled($canceled) {
        $this->canceled = $canceled;

        return $this;
    }

    /**
     * Get canceled
     *
     * @return string
     */
    public function getCanceled() {
        return $this->canceled;
    }

    /**
     * Set tech
     *
     * @param \Suivi\EtudesBundle\Entity\ProjectTech $tech
     *
     * @return AbsenceList
     */
    public function setTech(\Suivi\EtudesBundle\Entity\ProjectTech $tech = null) {
        $this->tech = $tech;

        return $this;
    }

    /**
     * Get tech
     *
     * @return \Suivi\EtudesBundle\Entity\ProjectTech
     */
    public function getTech() {
        return $this->tech;
    }

    /**
     * Set type
     *
     * @param \Suivi\EtudesBundle\Entity\AbsenceType $type
     *
     * @return AbsenceList
     */
    public function setType(\Suivi\EtudesBundle\Entity\AbsenceType $type = null) {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \Suivi\EtudesBundle\Entity\AbsenceType
     */
    public function getType() {
        return $this->type;
    }
    
    /**
     * Set manager
     *
     * @param \User\UserBundle\Entity\User $manager
     *
     * @return AbsenceList
     */
    public function setManager(\User\UserBundle\Entity\User $manager = null) {
        $this->manager = $manager;

        return $this;
    }
    
    /**
     * Get manager
     *
     * \User\UserBundle\Entity\User $manager
     */
    public function getManager() {
        return $this->manager;
    }

}
