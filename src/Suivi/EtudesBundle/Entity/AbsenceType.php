<?php

namespace Suivi\EtudesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * AbsenceType
 *
 * @ORM\Table(name="se_absence_type", uniqueConstraints={@ORM\UniqueConstraint(name="name_UNIQUE", columns={"name"}), @ORM\UniqueConstraint(name="abbrev_UNIQUE", columns={"abbrev"})})
 * @ORM\Entity(repositoryClass="Suivi\EtudesBundle\Entity\AbsenceTypeRepository")
 * @UniqueEntity(
 *      fields={"name"},
 *      message="Ce nom est déjà utilisé."
 * )
 * @UniqueEntity(
 *      fields={"abbrev"},
 *      message="Cette abréviation est déjà utilisé."
 * )
 */
class AbsenceType {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(
     *      max = 255,
     *      maxMessage = "Valeur maximale est {{ limit }} caractères"
     * )
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="abbrev", type="string", length=2, nullable=true)
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 2,
     *      max = 2,
     *      exactMessage = "Valeur doit être exactement {{ limit }} caractères"
     * )
     */
    private $abbrev;

    /**
     * @var integer
     *
     * @ORM\Column(name="substract", type="string", nullable=false)
     */
    private $substract = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="tech_visible", type="string", nullable=true)
     */
    private $techVisible = '0';

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return AbsenceType
     */
    public function setName($name) {
        $this->name = ucfirst($name);

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set abbrev
     *
     * @param string $abbrev
     *
     * @return AbsenceType
     */
    public function setAbbrev($abbrev) {
        $this->abbrev = strtoupper($abbrev);

        return $this;
    }

    /**
     * Get abbrev
     *
     * @return string
     */
    public function getAbbrev() {
        return $this->abbrev;
    }

    /**
     * Set substract
     *
     * @param integer $substract
     *
     * @return AbsenceType
     */
    public function setSubstract($substract) {
        $this->substract = $substract;

        return $this;
    }

    /**
     * Get substract
     *
     * @return integer
     */
    public function getSubstract() {
        return $this->substract;
    }

    /**
     * Set techVisible
     *
     * @param string $techVisible
     *
     * @return SeAbsenceType
     */
    public function setTechVisible($techVisible) {
        $this->techVisible = $techVisible;

        return $this;
    }

    /**
     * Get techVisible
     *
     * @return string
     */
    public function getTechVisible() {
        return $this->techVisible;
    }

}
