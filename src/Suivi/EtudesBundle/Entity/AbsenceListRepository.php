<?php

namespace Suivi\EtudesBundle\Entity;

use Doctrine\ORM\EntityRepository;

class AbsenceListRepository extends EntityRepository {

    public function findAllByYear($year) {
        $data = $this->createQueryBuilder('list')
                ->leftJoin('SuiviEtudesBundle:AbsenceTech', 'tech', 'WITH', 'list.tech=tech.id')
                ->leftJoin('SuiviEtudesBundle:AbsenceTechInfo', 'techInfo', 'WITH', 'list.tech=techInfo.tech')
                ->where('techInfo.year = :year')
                ->andWhere('tech.active = :active')
                ->setParameter('year', $year)
                ->setParameter('active', '1')
                ->getQuery()
                ->getResult();
        return $data;
    }

    public function findAllByTechAndYear($tech = null, $year = null) {
        $data = $this->createQueryBuilder('list')
                ->select('list.id,u.displayname,abs.name absenceType,DATE_FORMAT(list.startDate,\'%Y-%m-%d\') startDate'
                        . ',DATE_FORMAT(list.endDate,\'%Y-%m-%d\') endDate, list.days,list.refusReason,list.canceled'
                        . ', CASE WHEN list.validatedBy IS NULL THEN 0 ELSE u1.displayname END validatedBy')
                ->leftJoin('SuiviEtudesBundle:AbsenceTechInfo', 'techInfo', 'WITH', 'list.tech=techInfo.tech')
                ->leftJoin('UserUserBundle:User', 'u', 'WITH', 'u.id=IDENTITY(list.tech)')
                ->leftJoin('SuiviEtudesBundle:AbsenceType', 'abs', 'WITH', 'abs.id=IDENTITY(list.type)')
                ->leftJoin('UserUserBundle:User', 'u1', 'WITH', 'u1.id=list.validatedBy')
                ->where('list.tech = :tech')
                ->andWhere('techInfo.year = :year')
                ->setParameter('tech', $tech)
                ->setParameter('year', $year)
                ->orderBy('list.startDate', 'DESC')
                ->getQuery()
                ->getResult();
        return array('data' => $data);
    }

    public function findAllTechByPoleAndYear($depIds, $year) {
        $firstDayOfYear = new \DateTime(date("$year-01-01"));
        $lastDayOfYear = new \DateTime(date("$year-12-t"));
        $depIds = explode(',', $depIds);
        $data = $this->createQueryBuilder('list')
                ->select('list.id,u.displayname,t.name absenceType,DATE_FORMAT(list.startDate,\'%Y-%m-%d\') startDate'
                        . ',DATE_FORMAT(list.endDate,\'%Y-%m-%d\') endDate, list.days'
                        . ', CASE WHEN list.validatedBy IS NULL THEN 0 ELSE u1.displayname END validatedBy'
                        . ',list.refusReason,list.canceled')
                ->leftJoin('UserUserBundle:User', 'u', 'WITH', 'u.id=IDENTITY(list.tech)')
                ->leftJoin('SuiviEtudesBundle:AbsenceType', 't', 'WITH', 't.id=IDENTITY(list.type')
                ->leftJoin('SuiviEtudesBundle:AbsenceTechInfo', 'techInfo', 'WITH', 'list.tech=techInfo.tech')
                ->leftJoin('SuiviEtudesBundle:ProjectTech', 'tech', 'WITH', 'tech.tech=techInfo.tech')
                ->leftJoin('SuiviEtudesBundle:ProjectPole', 'pole', 'WITH', 'tech.pole=pole.id')
                ->leftJoin('UserUserBundle:User', 'u1', 'WITH', 'u1.id=list.validatedBy')
                ->where('pole.id IN(:depIds)')
                ->andWhere("list.startDate >= :firstDayOfYear AND list.endDate <= :lastDayOfYear")
                ->setParameter('depIds', $depIds)
                ->setParameter('firstDayOfYear', $firstDayOfYear->format('Y-m-d'))
                ->setParameter('lastDayOfYear', $lastDayOfYear->format('Y-m-d'))
                ->groupBy('list.id')
                ->orderBy('list.startDate', 'ASC')
                ->getQuery()
                ->getResult();
        return array('data' => $data);
    }

    // find absences starting from last month
    public function findAllTechByPole($depIds = null) {
        $now = new \DateTime(date("Y-m-01"));
        $lastDayOfYear = new \DateTime(date('Y-12-t'));
        $lastMonth = $now->modify('-1 month');
        $depIds = explode(',', $depIds);
        $data = $this->createQueryBuilder('list')
                ->select('list.id,u.displayname,t.name absenceType,DATE_FORMAT(list.startDate,\'%Y-%m-%d\') startDate'
                        . ',DATE_FORMAT(list.endDate,\'%Y-%m-%d\') endDate, list.days'
                        . ', CASE WHEN list.validatedBy IS NULL THEN 0 ELSE u1.displayname END validatedBy'
                        . ',list.refusReason,list.canceled')
                ->leftJoin('UserUserBundle:User', 'u', 'WITH', 'u.id=IDENTITY(list.tech)')
                ->leftJoin('SuiviEtudesBundle:AbsenceType', 't', 'WITH', 't.id=IDENTITY(list.type')
                ->leftJoin('SuiviEtudesBundle:AbsenceTechInfo', 'techInfo', 'WITH', 'list.tech=techInfo.tech')
                ->leftJoin('SuiviEtudesBundle:ProjectTech', 'tech', 'WITH', 'tech.tech=techInfo.tech')
                ->leftJoin('SuiviEtudesBundle:ProjectPole', 'pole', 'WITH', 'tech.pole=pole.id')
                ->leftJoin('UserUserBundle:User', 'u1', 'WITH', 'u1.id=list.validatedBy')
                ->where('pole.id IN(:depIds)')
                ->andWhere("list.startDate >= :lastMonth AND list.endDate <= :lastDayOfYear")
                ->setParameter('depIds', $depIds)
                ->setParameter('lastMonth', $lastMonth->format('Y-m-d'))
                ->setParameter('lastDayOfYear', $lastDayOfYear->format('Y-m-d'))
                ->groupBy('list.id')
                ->orderBy('list.startDate', 'ASC')
                ->getQuery()
                ->getResult();
        return array('data' => $data);
    }

}
