/**
 * Created by hachmi.halfaoui on 12/09/2017.
 */

    $("input[type=checkbox]").on("click", function () {
        if ($(this).is(":checked")) {
            $(this).parent().parent().addClass("active");
        }
        else {
            $(this).parent().parent().removeClass("active");
        }
    });

$(function () {
    $('.chosen-select').chosen();
    $('.chosen-select-deselect').chosen({allow_single_deselect: true});
});


$(document).ready(function () {
    $(document).on('change', '#user_id', function (e) {
        var idApplication = $('#application_id').val();
        var idUser = $('#user_id').val();
        var i = 0;
        var text = "";
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "get_role",
            data: {id: idApplication,idUser:idUser},
            beforeSend: function () {
                $("body").append("<div id='loadingBlock'>");
                $("#submit").prop('disabled', true);
            },
            success: function (data) {
                console.log(data);
                if(data.length >0) {
                    $("#application_id-error").css('display','none');
                    for (i; i < data.length; i++) {

                        text += '   <div class="col-sm-3">' +
                            '  <div class="roles-users  deselect ' +
                            (data[i]['test'] == 'oui' ? 'active' : ' ') +
                            ' ">' +
                            ' <a class="lien-roles-users"><span>' + data[i]['nom'] +
                            '</span> <input type="checkbox"  ' +
                            (data[i]['test'] == 'oui' ? 'checked' : ' ') +
                            '  name="roleList[]" value="' + data[i]['nom'] + '"> </a> </div> </div>'
                    }
                    $("#roleid").html("");
                    $("#roleid").append('   <label class="col-sm-2 control-label">Rôles</label>   <div class="col-sm-10">' +
                        text + '</div>')


                }else{
                    $("#roleid").html("");
                    $("#application_id-error").html("");
//                            $("#application_id-error").append('   <label class="col-sm-2 control-label">Veuillez selectionner une application</label>  ')

                }
            },
            complete: function () {

                $("#loadingBlock").remove();
                $("input[type=checkbox]").on("click", function () {
                    if ($(this).is(":checked") ) {
                        $(this).parent().parent().addClass("active");
                    }
                    else {
                        $(this).parent().parent().removeClass("active");
                    }
                });
            }
        });
    });
});


$(document).ready(function () {
    $(document).on('change', '#application_id', function (e) {
        var id = $('#application_id').val();
        var idUser = $('#user_id').val();
        console.log(id);
        var i = 0;
        var text = "";
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "get_role",
            data: {id: id,idUser:idUser},
            beforeSend: function () {
                $("body").append("<div id='loadingBlock'>");
                $("#submit").prop('disabled', true);
            },
            success: function (data) {
                console.log(data);
                if(data.length >0) {
                    $("#application_id-error").css('display','none');
                    for (i; i < data.length; i++) {

                        text += '   <div class="col-sm-3">' +
                            '  <div class="roles-users  deselect ' +
                            (data[i]['test'] == 'oui' ? 'active' : ' ') +
                            ' ">' +
                            ' <a class="lien-roles-users"><span>' + data[i]['nom'] +
                            '</span> <input type="checkbox"  ' +
                            (data[i]['test'] == 'oui' ? 'checked' : ' ') +
                            '  name="roleList[]" value="' + data[i]['nom'] + '"> </a> </div> </div>'
                    }
                    $("#roleid").html("");
                    $("#roleid").append('   <label class="col-sm-2 control-label">Rôles</label>   <div class="col-sm-10">' +
                        text + '</div>')


                }else{
                    $("#roleid").html("");
                    $("#application_id-error").html("");
//                            $("#application_id-error").append('   <label class="col-sm-2 control-label">Veuillez selectionner une application</label>  ')

                }
            },
            complete: function () {

                $("#loadingBlock").remove();
                $("input[type=checkbox]").on("click", function () {
                    if ($(this).is(":checked") ) {
                        $(this).parent().parent().addClass("active");
                    }
                    else {
                        $(this).parent().parent().removeClass("active");
                    }
                });
            }
        });
    });
});





$(document).ready(function () {
    var validator;
    validator = $("#form").submit(function () {
    }).validate({
        ignore: "",
        rules: {
            "application_name": "required"
        },
        messages: {
            "application_name": "Veuillez selectionner une application "

        }
    });
})

