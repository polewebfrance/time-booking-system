/**
 * Created by ons.allaya on 15/02/2017.
 */

function initDisplay1() {

    displayPreference();
    displayExclusion();
    //upListDirection();
    displaySer();
    upListSer();
    displayPole();
    // $('#divpole').hide();
    // $('#divposte').hide();
    displayPost(0);
    displaySociete();
    // displaySociete();
    displayContrat();
    displayStatut();
    displayPays();
    regions();
    displayRegion();
    $('#divregion').hide();
    $('#divprod').hide();
    displayCabinets();
    displayChargeRec();
    displayChargeRech();
    affecter();
    //affectationFields();
    //upListDirection1();



}

function displayPreference() {
    $.ajax({
        type: "POST",
        contentType: 'application/json; charset=UTF-8',
        url: "../spdemanderecrutement/preference",
        dataType: "json",
        success: function (html) {

            $('#selectingpeoplebundle_spdemanderecrutement_preference').html(html);
        },
        error: function (xhr, textStatus, errorThrown) {

        }
    });
}
function displayExclusion() {
    $.ajax({
        type: "POST",
        contentType: 'application/json; charset=UTF-8',
        url: "../spdemanderecrutement/preference",
        dataType: "json",
        success: function (html) {

            $('#selectingpeoplebundle_spdemanderecrutement_exclusion').html(html);
        },
        error: function (xhr, textStatus, errorThrown) {

        }
    });
}


function displaySer() {
    var direction = $('#selectingpeoplebundle_spdemanderecrutement_direction option:selected').val();
    if (direction != '') {

        upListSer();
    }
    else {

        $('#selectingpeoplebundle_spdemanderecrutement_service').append($('<option >', {
            value: 0,
            text: '--------------------------',
            selected: true
        }));
    }
}
function upListSer() {
    var direction = $('select#selectingpeoplebundle_spdemanderecrutement_direction option:selected').val();

    // $("select#selectingpeoplebundle_spdemanderecrutement_service option").remove();


    if (direction != "0") {
        $.ajax({
            async: false,
            dataType: "json",
            type: "POST",
            url: "../spdemanderecrutement/ser",
            data: {direction: direction},
            cache: false,
            error: function (jqXHR, textStatus, errorThrown) {

            },
            success: function (html) {
                $("select#selectingpeoplebundle_spdemanderecrutement_service").html(html);
                displayPost(0);
            }
        });
    }
}
function displayPole() {
    var service = $('#selectingpeoplebundle_spdemanderecrutement_service option:selected').val();
    if (service == '0') {

        $('#selectingpeoplebundle_spdemanderecrutement_pole').append($('<option >', {
            value: 0,
            text: '--------------------------',
            selected: true
        }));
        upListPost(2);
    }
    else {
        upListPole();

    }
}
function upListPole() {
    var service = $('select#selectingpeoplebundle_spdemanderecrutement_service option:selected').val();
    if (service != "0") {
        $.ajax({
            async: false,
            dataType: "json",
            type: "POST",
            url: "../spdemanderecrutement/polelist",
            data: {service: service},
            cache: false,
            error: function (jqXHR, textStatus, errorThrown) {
                displayPost(2);
                $("select#selectingpeoplebundle_spdemanderecrutement_pole option").remove();
                $('#selectingpeoplebundle_spdemanderecrutement_pole').append($('<option >', {
                    value: 0,
                    text: '--------------------------',
                    selected: true
                }));
            },
            success: function (html) {
                $("select#selectingpeoplebundle_spdemanderecrutement_pole").html(html);
            }
        });
    }
}
function displayPost(hierarchyLevel) {

    // var pole = $('#selectingpeoplebundle_spdemanderecrutement_pole option:selected').val();
    // if (pole == 0) {
    //     // $('#divpole').show();
    //
    //     $("select#selectingpeoplebundle_spdemanderecrutement_poste option").remove();
    //     $('#selectingpeoplebundle_spdemanderecrutement_poste').append($('<option >', {
    //
    //         text: '------ Choisir un pôle ------',
    //         selected: true
    //     }));
    // }
    // else {
        upListPost(hierarchyLevel);

    //}
}
function upListPost($hierarchyLevel) {
    var idBridge = [];
    if ($hierarchyLevel == "2") {
        idBridge.push($('select#selectingpeoplebundle_spdemanderecrutement_direction option:selected').val()); // direction
        idBridge.push($('select#selectingpeoplebundle_spdemanderecrutement_service option:selected').val()); // service
        idBridge.push('NULL'); // pole

    }
    else if ($hierarchyLevel == "3") {
        idBridge.push($('select#selectingpeoplebundle_spdemanderecrutement_direction option:selected').val()); // direction
        idBridge.push($('select#selectingpeoplebundle_spdemanderecrutement_service option:selected').val()); // service
        idBridge.push($('select#selectingpeoplebundle_spdemanderecrutement_pole option:selected').val()); // pole
    }

    $.ajax({
        async: false,
        dataType: "json",
        type: "POST",
        url: "../spdemanderecrutement/poste",
        data: {idBridge: idBridge},
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
            $("select#selectingpeoplebundle_spdemanderecrutement_poste option").remove();
                $('#selectingpeoplebundle_spdemanderecrutement_poste').append($('<option >', {

                    text: '------------------------',
                    selected: true
                }));
        },
        success: function (html) {
            $("select#selectingpeoplebundle_spdemanderecrutement_poste").html(html);
        }
    });
}

function getBdgId() {
    var idBridge = [];
    var divisionExist = 0; //var bool
    if ($('select#selectingpeoplebundle_spdemanderecrutement_pole option:selected').length >= 1) {
        divisionExist = 1;
    }
    if (divisionExist == 1) {
        idBridge.push($('select#selectingpeoplebundle_spdemanderecrutement_direction option:selected').val()); // direction
        idBridge.push($('select#selectingpeoplebundle_spdemanderecrutement_service option:selected').val()); // service
        idBridge.push($('select#selectingpeoplebundle_spdemanderecrutement_pole option:selected').val()); // pole
        idBridge.push($('select#selectingpeoplebundle_spdemanderecrutement_poste option:selected').val());// poste
    }
    else {
        idBridge.push($('select#selectingpeoplebundle_spdemanderecrutement_direction option:selected').val()); // direction
        idBridge.push($('select#selectingpeoplebundle_spdemanderecrutement_service option:selected').val()); // service
        idBridge.push('NULL'); // pole
        idBridge.push($('select#selectingpeoplebundle_spdemanderecrutement_poste option:selected').val());// poste
    }
    $.ajax({
        async: false,
        dataType: "json",
        type: "POST",
        url: "../spdemanderecrutement/getbridge",
        data: {idBridge: idBridge},
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (data) {
        }
    });

}

function displaySociete() {
    $.ajax({
        type: "POST",
        contentType: 'application/json; charset=UTF-8',
        url: "../spdemanderecrutement/societes",
        dataType: "json",
        success: function (html) {

            $('#selectingpeoplebundle_spdemanderecrutement_steId').html(html);
        },
        error: function (xhr, textStatus, errorThrown) {

        }
    });
}
function displayContrat() {
    $.ajax({
        type: "POST",
        contentType: 'application/json; charset=UTF-8',
        url: "../spdemanderecrutement/contrat",
        dataType: "json",
        success: function (html) {

            $('#selectingpeoplebundle_spdemanderecrutement_contratId').html(html);
        },
        error: function (xhr, textStatus, errorThrown) {

        }
    });
}
function displayStatut() {
    $.ajax({
        type: "POST",
        contentType: 'application/json; charset=UTF-8',
        url: "../spdemanderecrutement/statut",
        dataType: "json",
        success: function (html) {

            $('#selectingpeoplebundle_spdemanderecrutement_statutCollaborateurId').html(html);
        },
        error: function (xhr, textStatus, errorThrown) {

        }
    });
}
function displayPays() {
    $.ajax({
        type: "POST",
        contentType: 'application/json; charset=UTF-8',
        url: "../spdemanderecrutement/pays",
        dataType: "json",
        success: function (html) {

            $('#selectingpeoplebundle_spdemanderecrutement_paysId').html(html);
        },
        error: function (xhr, textStatus, errorThrown) {

        }
    });
}
function regions(){
    var pys = $('#selectingpeoplebundle_spdemanderecrutement_paysId option:selected').val();
    if (pys == '3') {
        $('#divregion').show();
        // displayRegion()

    }
    else{
        $('#divregion').hide();
    }

}
function displayRegion() {

    $.ajax({
        type: "POST",
        contentType: 'application/json; charset=UTF-8',
        url: "../spdemanderecrutement/region",
        dataType: "json",
        success: function (html) {

            $('#selectingpeoplebundle_spdemanderecrutement_regionId').html(html);
        },
        error: function (xhr, textStatus, errorThrown) {

        }

    });
}

function displayCabinets() {
    $.ajax({
        type: "POST",
        contentType: 'application/json; charset=UTF-8',
        url: "../cabinets",
        dataType: "json",
        success: function (html) {

            $('#selectingpeoplebundle_spdemanderecrutement_cabinet_ext').html(html);
        },
        error: function (xhr, textStatus, errorThrown) {


        }
    });
}
function displayChargeRec() {
    $.ajax({
        type: "POST",
        contentType: 'application/json; charset=UTF-8',
        url: "../chargerec",
        dataType: "json",
        success: function (html) {
 alert(success);
            $('#selectingpeoplebundle_spdemanderecrutement_charge_rec').html(html);
        },
        error: function (xhr, textStatus, errorThrown) {

alert(errorThrown);
        }
    });
}
function displayChargeRech() {
    $.ajax({
        type: "POST",
        contentType: 'application/json; charset=UTF-8',
        url: "../chargerech",
        dataType: "json",
        success: function (html) {

            $('#selectingpeoplebundle_spdemanderecrutement_charge_recherche').html(html);
        },
        error: function (xhr, textStatus, errorThrown) {


        }
    });
}
function affecter() {
    var sp = $('select#selectingpeoplebundle_spdemanderecrutement_sp option:selected').val();
    if (sp == "0") {
        $('#chrg_rec').hide();
        $('#cabnt_ext').show();
    }
    else{
        $('#chrg_rec').show();
        $('#cabnt_ext').hide();
    }

}

$(document).ready(function () {

    initDisplay1();



    $('#selectingpeoplebundle_spdemanderecrutement_direction').on("change", function () {
        var d = $('#selectingpeoplebundle_spdemanderecrutement_direction option:selected').val();
        if (d == '1') {
            $('#divprod').show();
        }
        else {
            $('#divprod').hide();
        }
        $('#selectingpeoplebundle_spdemanderecrutement_pole').append($('<option >', {
            value: 0,
            text: '--------------------------',
            selected: true
        }));
        $('#selectingpeoplebundle_spdemanderecrutement_poste').append($('<option >', {

            text: '--------------------------',
            selected: true
        }));


        displaySer();
        displayPole();
        displayPost(0);
        //$('#divpole').hide();
        //$('#divposte').hide();
    });
    $('#selectingpeoplebundle_spdemanderecrutement_service').on("change", function () {

        // $('#selectingpeoplebundle_spdemanderecrutement_poste').append($('<option >', {
        //
        //     text: '------ Choisir un pôle ------',
        //     selected: true
        // }));
        displayPole();
        displayPost(2);
        // $('#divposte').hide();
    });
    $('#selectingpeoplebundle_spdemanderecrutement_pole').on("change", function () {
        displayPost(3);
    });

    $('#selectingpeoplebundle_spdemanderecrutement_paysId').on("change", function () {
        regions();

    });
    $('#selectingpeoplebundle_spdemanderecrutement_sp').on("change", function () {
        affecter();
    })




})
