<?php

namespace RoleManagerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * AdmApplis
 *
 * @ORM\Table(name="adm_applis")
 * @ORM\Entity
 */
class AdmApplis
{
    /**
     * @var integer
     *
     * @ORM\Column(name="appliId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $appliid;

    /**
     * @var string
     *
     * @ORM\Column(name="appliCode", type="string", length=50, nullable=false)
     */
    private $applicode;

    /**
     * @var string
     *
     * @ORM\Column(name="appliName", type="string", length=255, nullable=false)
     */
    private $appliname;

    /**
     * @var string
     *
     * @ORM\Column(name="appliComment", type="text", length=65535, nullable=true)
     */
    private $applicomment;

    /**
     * @return int
     */
    public function getAppliid()
    {
        return $this->appliid;
    }

    /**
     * @param int $appliid
     */
    public function setAppliid($appliid)
    {
        $this->appliid = $appliid;
    }

    /**
     * @return string
     */
    public function getApplicode()
    {
        return $this->applicode;
    }

    /**
     * @param string $applicode
     */
    public function setApplicode($applicode)
    {
        $this->applicode = $applicode;
    }

    /**
     * @return string
     */
    public function getAppliname()
    {
        return $this->appliname;
    }

    /**
     * @param string $appliname
     */
    public function setAppliname($appliname)
    {
        $this->appliname = $appliname;
    }

    /**
     * @return string
     */
    public function getApplicomment()
    {
        return $this->applicomment;
    }

    /**
     * @param string $applicomment
     */
    public function setApplicomment($applicomment)
    {
        $this->applicomment = $applicomment;
    }





}

