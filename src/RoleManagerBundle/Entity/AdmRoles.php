<?php

namespace RoleManagerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AdmRoles
 *
 * @ORM\Table(name="adm_roles", indexes={@ORM\Index(name="FK_adm_roles_adm_applis", columns={"appliId_fk"})})
 * @ORM\Entity
 */
class AdmRoles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="roleId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $roleid;

    /**
     * @var string
     *
     * @ORM\Column(name="roleCode", type="string", length=100, nullable=false)
     */
    private $rolecode;

    /**
     * @var string
     *
     * @ORM\Column(name="roleName", type="string", length=255, nullable=true)
     */
    private $rolename;

    /**
     * @var string
     *
     * @ORM\Column(name="roleComment", type="text", length=65535, nullable=true)
     */
    private $rolecomment;

    /**
     * @var \AdmApplis
     *
     * @ORM\ManyToOne(targetEntity="AdmApplis")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="appliId_fk", referencedColumnName="appliId")
     * })
     */
    private $appliidFk;

    /**
     * @return int
     */
    public function getRoleid()
    {
        return $this->roleid;
    }

    /**
     * @param int $roleid
     */
    public function setRoleid($roleid)
    {
        $this->roleid = $roleid;
    }

    /**
     * @return string
     */
    public function getRolecode()
    {
        return $this->rolecode;
    }

    /**
     * @param string $rolecode
     */
    public function setRolecode($rolecode)
    {
        $this->rolecode = $rolecode;
    }

    /**
     * @return string
     */
    public function getRolename()
    {
        return $this->rolename;
    }

    /**
     * @param string $rolename
     */
    public function setRolename($rolename)
    {
        $this->rolename = $rolename;
    }

    /**
     * @return string
     */
    public function getRolecomment()
    {
        return $this->rolecomment;
    }

    /**
     * @param string $rolecomment
     */
    public function setRolecomment($rolecomment)
    {
        $this->rolecomment = $rolecomment;
    }

    /**
     * @return \AdmApplis
     */
    public function getAppliidFk()
    {
        return $this->appliidFk;
    }

    /**
     * @param \AdmApplis $appliidFk
     */
    public function setAppliidFk($appliidFk)
    {
        $this->appliidFk = $appliidFk;
    }





}

