<?php

namespace RoleManagerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends Controller
{
    /**
     * Method Name : indexAction
     * Utility: Modifier le role d'un utulisateur pour une application
     * @param
     * @return Response
     * Developper: Hachmi Halfaoui
     * Date of creation or last modification: 06/09/2017
     */
    public function indexAction()
    {
        /** @var
         * service doctrine entity manager
         * $em */
        $em = $this->getDoctrine()->getManager();

        /** @var
         * la liste des applications
         * $applications */
        $applications = $this->retournerListApplication();

        /** @var
         * Liste des utilisateur qui sont actifs
         * $users */
        $users = $em->getRepository('UserUserBundle:User')->findBy(array('enabled' => 1),array('usernameCanonical'=>'asc'));


        return $this->render('RoleManagerBundle:Default:role.html.twig', array('applications' => $applications, 'users' => $users));
    }


    /**
     * Method Name : newAction
     * Utility: Ajouter / Supprimer les roles d'un utlisateur
     * @param
     * @return Response
     * Developper: Hachmi Halfaoui
     * Date of creation or last modification: 07/09/2017
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var
         * Recuperer l'utilisateur
         * $user */
        $user = $em->getRepository('UserUserBundle:User')->find($request->get('user_name'));
        /** @var
         * Recuperer l'application avec l'id de l'aaplication
         * $applicationId */
        $applicationId = $em->getRepository('RoleManagerBundle:AdmApplis')->find($request->get('application_name'));

        /** @var
         * Recuperer tous les roles d'une application
         * $roles */
        $roles = $em->getRepository('RoleManagerBundle:AdmRoles')->findBy(array('appliidFk' => $applicationId));

        /** @var
         * Recuperer les roles selectionneés par l'admin
         * $listRoles */
        $listRoles = $request->get('roleList');

        /************** supprimer les roles non selcionnées ***************/
        foreach ($roles as $role) {
            if (count($listRoles) > 0) {
                if (!in_array($role->getRolecode(), $listRoles)) {
                    $user->removeRole($role->getRolecode());
                    $em->persist($user);
                    $em->flush();
                }
            } else {
                $user->removeRole($role->getRolecode());
                $em->persist($user);
                $em->flush();
            }

        }
        /************************** Fin ********************************/

        /**************  Ajouter les roles selectioneés ************************/
        if ($listRoles) {
            foreach ($listRoles as $r) {
                //Set the admin role
                $user->addRole($r);
                //Save it to the database
                $em->persist($user);
                $em->flush();
            }
        }
        /**************************** Fin ************************************/


        /**
         * message de succès
         */
        $this->get('session')->getFlashBag()->add('notice', 'Modification réussie');

        /** @var
         * la liste des applications
         * $applications */
        $applications = $this->retournerListApplication();
        /** @var
         * Liste des utilisateur
         * $users */
        $users = $em->getRepository('UserUserBundle:User')->findBy(array('enabled' => 1),array('usernameCanonical'=>'asc'));

        return $this->render('RoleManagerBundle:Default:role.html.twig', array('applications' => $applications, 'users' => $users));
    }


    /**
     * Method Name : roleAction
     * Utility: Recuperer les roles associées d'une application
     * @param
     * @return JsonResponse
     * Developper: Hachmi Halfaoui
     * Date of creation or last modification: 07/09/2017
     */
    public function roleAction(Request $request)
    {
        /** @var
         * service doctrine entity manager
         * $em */
        $em = $this->getDoctrine()->getManager();

        /** @var
         * La liste des nom des roles
         * $rolesName */
        $rolesName = array();
        /** @var
         * L'id de l'application
         * $applicationId */
        $applicationId = $request->get("id");

        /** @var
         * Utilisateur associé
         * $user */
        $user = $em->getRepository('UserUserBundle:User')->find($request->get('idUser'));
        /** @var
         * Liste des role de l'utilisateur
         * $userList */
        $userList = $user->getRoles();

        /** @var
         * Les roles associés a l'application
         * $roles */
        if($applicationId)
        $roles = $em->getRepository('RoleManagerBundle:AdmRoles')->findBy(array('appliidFk' => $applicationId));

        /** @var
         * Initialiser le compteur de la liste des role nom
         * $i */
        $i = 0;

        /*************************Recuperer les roles et Tester si l'utilisateur possede ces roles ***************************/
        if (count($roles) > 0)
            foreach ($roles as $role) {
                $rolesName[$i]['nom'] = $role->getRolecode();
                if (in_array($role->getRolecode(), $userList)) {
                    $rolesName[$i]['test'] = 'oui';
                } else {
                    $rolesName[$i]['test'] = 'non';
                }

                $i++;
            }
        /**************************** Fin ************************************/


        return new JsonResponse($rolesName);

    }


    /**
     * Method Name : retournerListApplication
     * Utility: Retourner la liste des applications qui ont au moins une role
     * @param
     * @return array
     * Developper: Hachmi Halfaoui
     * Date of creation or last modification: 13/09/2017
     */
    public function retournerListApplication()
    {
        /** @var
         * service doctrine entity manager
         * $em */
        $em = $this->getDoctrine()->getManager();

        /** @var
         * Recuperer toutes les roles
         * $roles */
        $roles = $em->getRepository('RoleManagerBundle:AdmRoles')->findAll();
        /** @var
         * Initialiser une listes des applications qui ont au moins un role
         * $applications */
        $applications = array();
        /** @var
         * Intialiser le compteur
         * $i */
        $i = 0;
        /*************Tester sur l'existance de liste  **************/
        if ($roles)
            foreach ($roles as $role) {
            /********************* Remplir la liste des applications    ********************/
                if (!in_array($role->getAppliidFk(), $applications)) {
                    $applications[$i] = $role->getAppliidFk();
                    $i++;
                }
            }
        return $applications;

    }

}
