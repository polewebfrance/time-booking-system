<?php

namespace AdminRH\VolontaireDimancheBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Administration\SocieteBundle\Entity\AdmMagasin;
use AdminRH\VolontaireDimancheBundle\Form\AdmMagasinType;
use Symfony\Component\HttpFoundation\Response;
use AdminRH\VolontaireDimancheBundle\Entity\VolontaireDimancheRepository;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="vd_default")
     * @Template()
     * 
     * Handles first page. Check if user is like ^P[0-9]{3}, if yes initialize 'shop' variable. Display page
     */
    public function indexAction()
    {
        //log current user in Monolog
        //$logger = $this->get('logger');
        //$logger->info('Current user is : ' . $this->getUser()->getUsername());
        
        
        //get the user. if user is a shop get the shop ID(the same with username) and show only rows for this ID
        $shop = NULL;
        $user = ucfirst($this->getUser()->getUsername());
        if(preg_match("/^P[0-9]{3}/i", $user)) {
            $shop = $user;
        }
        //$shop='P310';
        
        $entity = new AdmMagasin();
        $form   = $this->createCreateForm($entity);

        return $this->render('VolontaireDimancheBundle:Default:index.html.twig',
            array( 
                   'entity' => $entity,
                   'form'   => $form->createView(),
                   'shop'   =>  $shop
                ));
        
    }

    
    /**
     * @Route("/select-change", name="_select_change_AJAX")
     * 
     * AJAX called controller. Returns a table with data and pagination.
     */
    public function changeAction(Request $request)
    {
            $id = $request->query->get('id');
            
            // check if the selection from drop down is valid
            if ( NULL != $id){
                
                $page = $request->query->get('page');
                // if no page parameter was sent by GET method, use page =1
                if (is_null($page)){ $page= 1; }
                    
                $em = $this->getDoctrine()->getManager();
                $societe = $em->getRepository('AdministrationSocieteBundle:AdmMagasin')->findOneBy(array('magId'=>$id));
                $query = $em->getRepository('VolontaireDimancheBundle:VolontaireDimanche')->findContractBySociete($societe->getSociete());
                //$query = $em->getRepository('VolontaireDimancheBundle:ViewVolontaire')->findVolontaire($societe->getSociete());
                $paginator  = $this->get('knp_paginator');
                $pagination = $paginator->paginate(
                                $query, /* query NOT result */
                                $page/*page number*/,
                                20/*limit per page*/
                );                
                return $this->render('VolontaireDimancheBundle:Default:content.html.twig', array('pagination' => $pagination)); 
            } else {
                return $this->render('VolontaireDimancheBundle:Default:noShopSelected.html.twig');
            }
    }
    
    /**
     * @Route("/edit/volontaire", name="_edit_volontaire_AJAX")
     * 
     *  AJAX called controller. Saves to database status modification regarding volontaire du dimanche
     */
    public function editVolontaireAction(Request $request)
    {
            /*
             * Initially matricule was saved in vd_dimanche table, but because now
             * they want to have confirmation(yes/no) per each contract and not per each person,
             * instead of matricule I am saving the contract id. The column in Database was
             * renamed to contrat, but in HTML and AJAX it is still called matricule.
             */
            $contratId = $request->query->get('matricule');
            
            // check if the selection from drop down is valid
            if ( NULL != $contratId){
                $em = $this->getDoctrine()->getManager();
                $volunteer = $em->getRepository('VolontaireDimancheBundle:VolontaireDimanche')->findOneBy(array('contrat'=>$contratId));

                if ($volunteer->getVolontaire()==0) {
                    $volunteer->setVolontaire(1);
                } else {
                    $volunteer->setVolontaire(0);
                }
                $em->persist($volunteer);
                $em->flush();
                
                $response = array("matricule" => $volunteer->getContrat(), 'volontaire'=> $volunteer->getVolontaire(), "success" => true);
               
                return new Response(json_encode($response)); 
            }   
            
           $response = array("success" => false);
           return new Response(json_encode($response)); 
    }
    
    /**
     * @Route("/contracts-by-matricule/{matricule}", name="contracts_by_matricule")
     * 
     * List the contracts for each employee and their status regarding willingess to work in weekend.
     */
    public function contractsByMatricule(Request $request, $matricule)
    {

                
        $page = $request->query->get('page');
        // if no page parameter was sent by GET method, use page =1
        if (is_null($page)){ $page= 1; }

        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository('VolontaireDimancheBundle:Contrat')->findAllContractByMatricule($matricule);
        
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                        $query, /* query NOT result */
                        $page/*page number*/,
                        20/*limit per page*/
        );
        return $this->render('VolontaireDimancheBundle:Default:contract_by_matricule.html.twig', array('pagination' => $pagination)); 

    }
    
    
    /**
     * Creates a form to select a magasin
     *
     * @param AdmMagasin $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(\Administration\SocieteBundle\Entity\AdmMagasin $entity)
    {
        $form = $this->createForm(new AdmMagasinType(), $entity, array());
        return $form;
    }
}
