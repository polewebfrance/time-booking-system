<?php

/**
 * Description of AdmMagasinType
 *
 * @author pana_cr
 */

namespace AdminRH\VolontaireDimancheBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class AdmMagasinType extends AbstractType {
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('magId','entity', array(
                    'empty_value' => 'Sélectionnez un magasin',
                    'label' => false,
                    'class'=>'Administration\SocieteBundle\Entity\AdmMagasin', 
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('u')
                            ->orderBy('u.magLibelle', 'ASC');
                     },
                    'property'=>'magLibelle'
                )) ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Administration\SocieteBundle\Entity\AdmMagasin',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'adm_magasin';
    }
}
