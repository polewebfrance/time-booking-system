<?php

namespace AdminRH\VolontaireDimancheBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VolontaireDimanche
 *
 * @ORM\Table(name="vd_dimanche")
 * @ORM\Entity(repositoryClass="AdminRH\VolontaireDimancheBundle\Entity\VolontaireDimancheRepository")
 */
class VolontaireDimanche
{
    
    /**
     * @var string
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
	
	/**
     * @var string
     *
     * @ORM\Column(name="contrat", type="string", length=6, nullable=false)
     */
    private $contrat;
	
	
    /**
     * @var string
     *
     * @ORM\Column(name="volontaire", type="string", length=1, nullable=false)
     */
    private $volontaire;

    
   

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get contrat id
     *
     * @return string
     */
    public function getContrat()
    {
        return $this->contrat;
    }
	
	/**
     * Set contrat id
     *
     * @return string
     */
    public function setContrat($contrat)
    {
        $this->contrat=$contrat ;
		
		return $this;
    }
	
	   /**
     * Get Volontaire
     *
     * @return string
     */
    public function getVolontaire()
    {
        return $this->volontaire;
    }
	
	/**
     * Set Volontaire
     *
     * @return string
     */
    public function setVolontaire($volontaire)
    {
        $this->volontaire=$volontaire ;
		
		return $this;
    }

}
