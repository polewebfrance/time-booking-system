<?php

/**
 * Description of IndividuRepository
 *
 * @author pana_cr
 */

namespace AdminRH\VolontaireDimancheBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class VolontaireDimancheRepository extends EntityRepository
{
    public function findContractBySociete($societe)
    {

        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        
        $queryBuilder->select('contrat.id'
                            . ", IDENTITY(contrat.user) as matricule"
                            . ', individu.nom'
                            . ', individu.prenom'
                            . ', contrat.numcontrat'
                            . ', contrat.dateSortie'
                            . ', contrat.dateFin'
                            . ', v.volontaire' 
                     )
                     ->from('AdministrationSocieteBundle:PandoreIndividu', 'individu')
                     ->join('AdministrationSocieteBundle:PandoreContrat', 'contrat', Expr\Join::WITH, 'individu.id = contrat.user')
                     ->join('VolontaireDimancheBundle:VolontaireDimanche', 'v', Expr\Join::WITH, 'contrat.id = v.contrat')
                     ->where('contrat.societe = :societe_id ')
                     ->andWhere("contrat.dateSortie = :vide")
                     ->orderBy('individu.nom', 'ASC')
                     ->setParameter('societe_id', $societe)
                     ->setParameter('vide', '');

            
        $query = $queryBuilder->getQuery();
        return $query;

    }
    
    public function findAllContractByMatricule($matricule)
    {

        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        
        $queryBuilder->select('contrat.id'
                            . ',individu.matricule'
                            . ', individu.nom'
                            . ', individu.prenom'
                            . ', contrat.numcontrat'
                            . ', contrat.dateSortie'
                            . ', contrat.dateFincont'
                            . ', v.volontaire' 
                     )
                     ->from('VolontaireDimancheBundle:Individu', 'individu')
                     ->join('VolontaireDimancheBundle:Contrat', 'contrat', Expr\Join::WITH, 'individu.matricule = contrat.matricule')
                     ->join('VolontaireDimancheBundle:VolontaireDimanche', 'v', Expr\Join::WITH, 'contrat.id = v.contrat')
                     ->where('individu.matricule = :matricule ')
                     ->orderBy('individu.nom', 'ASC')
                     ->setParameter('matricule', $matricule);

            
        $query = $queryBuilder->getQuery();
        return $query;

    }
}
