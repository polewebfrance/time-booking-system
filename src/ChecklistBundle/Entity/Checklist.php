<?php

namespace ChecklistBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \DateTime;

/**
 * Checklist
 *
 * @ORM\Entity(repositoryClass="ChecklistBundle\Entity\ChecklistRepository")
 * @ORM\Table(name="chk_checklists")
 */
class Checklist
{
    /**
     * @ORM\ManyToMany(targetEntity="PointDeControle", inversedBy="checklists")
     * @ORM\JoinTable(name="checklist_points_de_controle")
     **/
    private $pointsDeControle;

    public function __construct() {
        $this->pointsDeControle = new \Doctrine\Common\Collections\ArrayCollection();
    }
	
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;
	
    /**
     * @var boolean
     *
     * @ORM\Column(name="desactive", type="boolean", options={"default":false})
     */
    private $desactive;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Magasin
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }	

    /**
     * Add pointsDeControle
     *
     * @param \ChecklistBundle\Entity\PointDeControle $pointsDeControle
     * @return Checklist
     */
    public function addPointsDeControle(\ChecklistBundle\Entity\PointDeControle $pointsDeControle)
    {
        $this->pointsDeControle[] = $pointsDeControle;

        return $this;
    }

    /**
     * Remove pointsDeControle
     *
     * @param \ChecklistBundle\Entity\PointDeControle $pointsDeControle
     */
    public function removePointsDeControle(\ChecklistBundle\Entity\PointDeControle $pointsDeControle)
    {
        $this->pointsDeControle->removeElement($pointsDeControle);
    }

    /**
     * Get pointsDeControle
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPointsDeControle()
    {
        return $this->pointsDeControle;
    }

    /**
     * Set desactive
     *
     * @param boolean $desactive
     * @return Checklist
     */
    public function setDesactive($desactive)
    {
        $this->desactive = $desactive;

        return $this;
    }

    /**
     * Get desactive
     *
     * @return boolean 
     */
    public function getDesactive()
    {
        return $this->desactive;
    }

    /**
     * Get temps theorique
     *
     * @return time
     */
    public function getTempsTheorique($nbCaisse)
	{
    	$tempsTheorique= new DateTime(date('Y-m-d 00:00:00'));
    	foreach ($this->getPointsDeControle() as $pointDeControle) {
    		$temporary = new DateTime(date('Y-m-d 00:00:00'));
    		$tempsTheorique->add($temporary->diff($pointDeControle->getdureeTotale($nbCaisse)));
    	}
    	return $tempsTheorique;
    }
    
}
