<?php

namespace ChecklistBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Magasin
 *
 * @ORM\Entity(repositoryClass="ChecklistBundle\Entity\MagasinRepository")
 * @ORM\Table(name="chk_magasins")
 */
class Magasin
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="numero", type="string", length=255)
     */
    private $numero;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=255)
     */
    private $telephone;
    /**
     * @var string
     *
     * @ORM\Column(name="feu", type="string", length=255)
     */
    private $feu;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=255)
     */
    private $adresse;
    
    /**
     * @var string
     *
     * @ORM\Column(name="code_postal", type="string", length=5)
     */
    private $codePostal;

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=150)
     */
    private $ville;    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="nombre_de_caisses", type="smallint")
     */
    private $nombreDeCaisses;


    /**
     * @ORM\Column(name="ouverture", type="date")
     */
    private $ouverture;

    /**
     * @ORM\Column(name="fermeture", type="date")
     */
   private $fermeture;
    
    /**
     * @var string
     *
     * @ORM\Column(name="societe", type="string", length=4)
	*/
   private $societe;

   /**
    * @var string
    *
    * @ORM\Column(name="repertoirephoto", type="string", length=255)
    */
   private $repertoirephoto;
   
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Magasin
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set numero
     *
     * @param string $numero
     * @return Magasin
     */
    public function setNumero($numero)
    {
        $this->numero = $nombre;

        return $this;
    }

    /**
     * Get numero
     *
     * @return string 
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return Magasin
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string 
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     * @return Magasin
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string 
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set nombreDeCaisses
     *
     * @param integer $nombreDeCaisses
     * @return Magasin
     */
    public function setNombreDeCaisses($nombreDeCaisses)
    {
        $this->nombreDeCaisses = $nombreDeCaisses;

        return $this;
    }

    /**
     * Get nombreDeCaisses
     *
     * @return integer 
     */
    public function getNombreDeCaisses()
    {
        return $this->nombreDeCaisses;
    }
    
    /**
     * Set ouverture
     *
     * @param \DateTime $ouverture
     * @return Magasin
     */
    public function setOuverture($ouverture)
    {
    	$this->ouverture = $ouverture;
    
    	return $this;
    }
    
    /**
     * Get ouverture
     *
     * @return \DateTime
     */
    public function getOuverture()
    {
    	return $this->ouverture;
    }
    
    /**
     * Set fermeture
     *
     * @param \DateTime $fermeture
     * @return Magasin
     */
    public function setFermeture($fermeture)
    {
    	$this->fermeture = $fermeture;
    
    	return $this;
    }
    
    /**
     * Get fermeture
     *
     * @return \DateTime
     */
    public function getFermeture()
    {
    	return $this->fermeture;
    }
    
    /**
     * Set societe
     *
     * @param string $societe
     * @return Magasin
     */
    public function setSociete($societe)
    {
    	$this->societe = $societe;
    
    	return $this;
    }
    
    /**
     * Get societe
     *
     * @return string
     */
    public function getSociete()
    {
    	return $this->societe;
    }
    

    /**
     * Set codePostal
     *
     * @param string $codePostal
     * @return Magasin
     */
    public function setCodePostal($codePostal)
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    /**
     * Get codePostal
     *
     * @return string 
     */
    public function getCodePostal()
    {
        return $this->codePostal;
    }

    /**
     * Set ville
     *
     * @param string $ville
     * @return Magasin
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string 
     */
    public function getVille()
    {
        return $this->ville;
    }
    
    
    /**
     * Set Repertoirephoto
     *
     * @param string $repertoirephoto
     * @return Magasin
     */
    public function setRepertoirephoto($repertoirephoto)
    {
    	$this->repertoirephoto = $repertoirephoto;
    
    	return $this;
    }
    
    /**
     * Get repertoirephoto
     *
     * @return string
     */
    public function getRepertoirephoto()
    {
    	return $this->repertoirephoto;
    }
    
    
    
    
    
    
    
    
    
    
    
}
