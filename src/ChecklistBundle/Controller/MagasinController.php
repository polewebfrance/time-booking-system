<?php
	
namespace ChecklistBundle\Controller;
	
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use ChecklistBundle\Form\MagasinType;
use ChecklistBundle\Form\VisitesType;
use Symfony\Component\HttpFoundation\JsonResponse;
use ChecklistBundle\Entity\Visites;
use \DateTime;
use UserUserBundle\Entity\Users;
	
class MagasinController extends Controller
{
    /**
     * @Route("/magasin/editer/{id}", name="checklist_magasin_editer")
     */
	public function editerMagasinAction($id, Request $request)
	{
		$message='';
		$em = $this->getDoctrine()->getManager();
		$magasin = $em->getRepository('ChecklistBundle:Magasin')->find($id);
		if (!$magasin) {
			throw $this->createNotFoundException(
				'Trouvé aucune magasin pour id '.$id
			);
		}
		$form = $this->createForm(new MagasinType, $magasin);
		$form->handleRequest($request);
		if ($form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$em->persist($magasin);
			$em->flush();
			$message = 'Modifications bien enregistrées!';
			$this->get('session')->getFlashBag()->add('magasinSuccess', $message);
		}
        return $this->render('ChecklistBundle:magasin:editerMagasin.html.twig', array(
            'form' => $form->createView(),
        ));
	}
		
	/**
	 * @Route("/magasin/verifDispo", name="checklist_magasin_verif_dispo")
	 */
	public function verifDispo()
	{
		$request = $this->container->get('request');
		$technicien = $request->request->get('technicien');
		$date = DateTime::createFromFormat('j/m/Y H:i:s', $request->request->get('date').' 00:00:00');
		$visite=$this->getDoctrine()
			->getRepository('ChecklistBundle:Visites')
			->getVisiteTechnicienDay($technicien, $date);
			if ($visite){
				return new JsonResponse($visite[0]->getMagasin()->getNom());
			} else {
				return new JsonResponse('pas de visite prevue');
			}
	}
			
    /**
     * @Route("/magasin/{id}", name="checklist_magasin_magasin")
     */
	public function magasinAction($id)
	{
		$magasin = $this->getDoctrine()
			->getRepository('ChecklistBundle:Magasin')
			->find($id);
		if (!$magasin) {
			throw $this->createNotFoundException(
				'Trouvé aucune magasin pour id '.$id
			);
		}
        return $this->render('ChecklistBundle:magasin:magasin.html.twig', array('magasin' => $magasin));
	}
		
    /**
     * @Route("/magasins", name="checklist_magasin_magasins")
     */
    public function magasinsAction()
    {
		$listeInfos = $this->getDoctrine()
			->getRepository('ChecklistBundle:Visites')
			->getListeMagasinManager();
		if (!$listeInfos) {
			throw $this->createNotFoundException(
				'Trouvé aucune magasin.'
			);
		}		
		$listeMagasin = array();
		$i=-1;		
		foreach ($listeInfos as $info){
			if(get_class($info) == 'ChecklistBundle\Entity\Magasin'){
				$i++;
				$listeMagasin[$i]= array('magasin'=>$info);
			}
			else if(get_class($info) == 'ChecklistBundle\Entity\Visites'){
				if ($info->getDate() < new \DateTime(date('Y-m-d 00:00:00'))){
					$listeMagasin[$i]['lastVisite']=$info;
				} else {
					$listeMagasin[$i]['nextVisite']=$info;
				}
			}
		}			
        return $this->render('ChecklistBundle:magasin:magasins.html.twig', array('magasins' => $listeMagasin));
    }
		
    /**
     * @Route("/magasins/annuleVisite/{id}", name="checklist_magasin_annuler_visite")
     */
    public function annuleVisiteAction($id)
    {
    	$magasin = $this->getDoctrine()
			->getRepository('ChecklistBundle:Visites')
			->deleteVisite($id);
    	$message = 'La visite a bien été annulé!';	
    	$this->get('session')->getFlashBag()->add('visiteSuccess', $message);
    	return $this->redirect($this->generateUrl('checklist_magasin_magasins'));
    }
		
    /**
     * @Route("/magasin/nouvelleVisite/{idMagasin}", name="checklist_magasin_nouvelle_visite")
     */
    public function nouvelleVisiteAction($idMagasin, Request $request)
    {
        $visite = new Visites();
		$visite->setDate(new \DateTime('tomorrow'));			
		$em = $this->getDoctrine()->getManager();
		$magasin = $em->getRepository('ChecklistBundle:Magasin')->find($idMagasin);
		$visite->setMagasin($magasin);			
		$visite->setHeureDeArivee(new DateTime(date('Y-m-d 00:00:00')));
		$visite->setHeureDeDepart(new DateTime(date('Y-m-d 00:00:00')));
		$visite->setTempsDeInteruption(new DateTime(date('Y-m-d 00:00:00')));
		$visite->setFlagAccomplie(0);
		$visite->setCommentaire('');
		$visite->setCommencer(0);
		$visite->setTerminer(0);
		$form = $this->createForm(new VisitesType($em), $visite);

		$form->handleRequest($request);
		$infoPlanning=$em->getRepository('ChecklistBundle:Visites')->getPlanning();			
		// mise en forme du planning
		//la première ligne du tableau contient les dates
		$planning = array(array(0=>NULL));
		for ($i=1; $i<16; $i++){
		$planning [0][$i]= new \DateTime(($i-1).'days  00:00:00');
		}
		$numeroLigne=0;
		//on remplis chaque ligne pour chaque
		foreach ($infoPlanning as $visitePlanning){
			// si le technicien n'est pas le même que celui de la ligne d'avant, on ajoute une ligne au tableau
			if ($visitePlanning->getTechnicien()->getDisplayName()!= $planning[$numeroLigne][0]){
				$numeroLigne++;
				$planning[$numeroLigne][0]=$visitePlanning->getTechnicien()->getDisplayName();
				$numeroColonne=1;
			}
			// si la date de la visite est égal à la date de la colonne, on indique le nom du magasin
			while ($planning [0][$numeroColonne]!=$visitePlanning->getDate()){
				$numeroColonne++;
			}
			$planning[$numeroLigne][$numeroColonne]=$visitePlanning->getMagasin()->getNom();
		}			
		if ($form->isValid()) {
			$checklitId = $form["checklists"]->getData()->getId();
			$checklist = $this->getDoctrine()
				->getRepository('ChecklistBundle:Checklist')
				->find($checklitId);
			$pointsDeControle = $checklist->getPointsDeControle();				
			$visite->setChecklists($form["checklists"]->getData());
			$visite->setTechnicien($form["technicien"]->getData());				
			$em->persist($visite);
			$em->flush();				
			$message = 'La visite a été correctement enregistrée!';	
			$this->get('session')->getFlashBag()->add('visiteSuccess', $message);				
			return $this->redirect($this->generateUrl('checklist_magasin_magasins'));
		}			
        return $this->render('ChecklistBundle:magasin:visite.html.twig', array(
            'form' => $form->createView(),
        	'planning' => $planning,
			'title' => 'Ajouter magasin' . $idMagasin
        ));
    }
		
    /**
     * @Route("/magasinsHotline", name="checklist_magasin_magasins_hotline")
     */
    public function magasinsHotlineAction()
    {
    	$listeInfos = $this->getDoctrine()
    	->getRepository('ChecklistBundle:Magasin')
    	->getListeMagasinHotline();
		if (!$listeInfos) {
			throw $this->createNotFoundException(
				'Trouvé aucune magasin.'
			);
		}					
		$listeMagasin = array();
		$i=-1;		
		foreach ($listeInfos as $info){
			if(get_class($info) == 'ChecklistBundle\Entity\Magasin'){
				$i++;
				$listeMagasin[$i]= array('magasin'=>$info);
			}
			else if(get_class($info) == 'ChecklistBundle\Entity\Visites'){
				$listeMagasin[$i]['visite']=$info;
			}
			else if(get_class($info) == 'ChecklistBundle\Entity\Resultat'){
				$listeMagasin[$i]['resultat']=$info;
			}
		}		
    	return $this->render('ChecklistBundle:magasin:magasinsHotline.html.twig', array('magasins' => $listeMagasin));
    }
		
}