<?php

namespace ChecklistBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class PointDeControleType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('nom', 'text', array(
				'required'  => true,
				'constraints' => array(
					new NotBlank(array('message' => 'Le nom ne peut pas être vide!'))
				)
			))
			->add('duree', 'time', array(
				'label'     => 'Temps théorique',
				'required'  => true,
			))
			
			->add('caisses', 'checkbox', array(
				'label'     => 'Caisses?',
				'required'  => false,
			))
			
			->add('type', 'choice', array(
				'choices'   => array(
					'1' => 'Contrôles Visuels',
					'2' => 'Installation de Logiciel',
					'3' => 'Action à Mener',
					'4' => 'Contrôle avec retour d’info'
				),
				'label'     => 'Type de Point de contrôle',
				'required'  => true,
				'constraints' => array(
					new NotBlank(array('message' => 'Le type ne peut pas être vide!'))
				),
				'multiple' => false,
				'expanded' => true,
			))
			
			->add('libelle', 'textarea', array(
				'label'     => 'Énoncé du point de contrôle',
			))
			
			->add('question', 'text', array(
				'label'     => 'Libellé de la question',
				'constraints' => array(
					new NotBlank(array('message' => 'Le libellé ne peut pas être vide!'))
				),
			))
			
			->add('action', 'text', array(
				'label'     => 'Action à valider',
			))
			
			->add('dureeParCaisse', 'time')
			
			->add('fischeDeProcedure', 'text')
			
			->add('save', 'submit', array(
				'label' => 'Enregistrer',
				'attr' => array('class' => 'btn btn-info', 'aria-label' => 'Left Align', 'type' => 'button'),
			))
		;		
	}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'ChecklistBundle\Entity\PointDeControle'
		));
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'checklist_point_de_controle';
	}
}