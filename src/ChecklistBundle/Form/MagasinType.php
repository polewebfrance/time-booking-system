<?php

namespace ChecklistBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\Type;

class MagasinType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder->add('nom', 'text', array(
					'read_only'=> true,					
					))					
            ->add('numero', 'text', array(
				'read_only'=> true,					
			))					
            ->add('telephone', 'text', array(
				'required'=> false,					
			))
			->add('adresse', 'text', array(
				'required'=> false,
			))								
			->add('code_postal', 'text', array(
				'required'=> false,
			))			
            ->add('ville', 'text', array(
				'required'=> false,					
			))				
			->add('repertoirephoto', 'text', array(
					'required'=> false,
			))
            ->add('nombreDeCaisses', 'integer', array(
				'required'=> false,
				'constraints' => array(
					new Type('integer', 'Le nombre de caisse est obligatoirement supérieur à zéro.'),
					new GreaterThan(array('value' => 0, 'message' => 'Le nombre de caisse est obligatoirement supérieur à zéro.'))
				)
			))			
			->add('ouverture', 'date', array(
				'widget' => 'single_text',
				'format' => 'dd/MM/yyyy',
				'read_only'=> true,
			))					
            ->add('save', 'submit', array('label' => 'Enregistrer les modifications'));
	}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'ChecklistBundle\Entity\Magasin'
		));
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'checklist_magasin';
	}
}