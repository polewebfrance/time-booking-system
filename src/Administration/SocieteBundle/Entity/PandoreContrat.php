<?php

namespace Administration\SocieteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Administration\SocieteBundle\Entity\PandoreSociete;

/**
 * PandoreContrat
 *
 * @ORM\Table(name="pandore.contrat")
 * @ORM\Entity(repositoryClass="Administration\SocieteBundle\Entity\PandoreContratRepository")
 */
class PandoreContrat
{
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="id", type="string", length=45, nullable=true)
	 * @ORM\Id
	 */
	private $id;
	
	
	/**
	 * @ORM\ManyToOne(targetEntity="Administration\SocieteBundle\Entity\PandoreIndividu")
	 * @ORM\JoinColumn(name="matricule", referencedColumnName="matricule")
	 */
	private $user;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="numcontrat", type="string", length=10, nullable=false)
	 */
	private $numcontrat;

	/**
	 * @ORM\ManyToOne(targetEntity="Administration\SocieteBundle\Entity\PandoreSociete")
	 * @ORM\JoinColumn(name="societe", referencedColumnName="id")
	 */
	private $societe;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="etab", type="string")
	 */
	private $etab;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="horaire", type="string", length=45, nullable=false)
	 */
	private $horaire;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="categ_sociopro", type="string", length=45, nullable=false)
	 */
	private $categSociopro;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="remuneration", type="string", length=45, nullable=false)
	 */
	private $remuneration;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="poste", type="string", length=100, nullable=false)
	 */
	private $poste;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="service", type="string", length=200, nullable=false)
	 */
	private $service;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="niveau", type="string", length=200, nullable=true)
	 */
	private $niveau;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="coef", type="string", length=200, nullable=true)
	 */
	private $coef;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="duree_pe", type="string", length=45, nullable=true)
	 */
	private $dureePe;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="absence", type="string", length=45, nullable=true)
	 */
	private $absence;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="date_anciennete", type="string", length=45, nullable=false)
	 */
	private $dateAnciennete;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="date_entree", type="string", length=45, nullable=false)
	 */
	private $dateEntree;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="date_sortie", type="string")
	 */
	private $dateSortie;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="date_fincont", type="string")
	 */
	private $dateFin;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="type_contrat", type="string")
	 */
	private $typeContrat;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="date_creation", type="string", length=45, nullable=true)
	 */
	private $dateCreation;
	
	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set id
	 *
	 * @param integer $id
	 *
	 * @return DdrcSociete
	 */
	public function setId($id)
	{
		$this->id = $siret;
	
		return $this;
	}

	/**
	 * Set user
	 *
	 * @param \Administration\SocieteBundle\Entity\PandoreIndividu $user
	 *
	 * @return PandoreContrat
	 */
	public function setUser(\Administration\SocieteBundle\Entity\PandoreIndividu $matricule = null)
	{
		$this->user = $user;
	
		return $this;
	}
	
	/**
	 * Get user
	 *
	 * @return \Administration\SocieteBundle\Entity\PandoreIndividu
	 */
	public function getUser()
	{
		return $this->user;
	}
	

	/**
	 * Set numcontrat
	 *
	 * @param string $numcontrat
	 *
	 * @return Contrat
	 */
	public function setNumcontrat($numcontrat)
	{
		$this->numcontrat = $numcontrat;
	
		return $this;
	}
	
	/**
	 * Get numcontrat
	 *
	 * @return string
	 */
	public function getNumcontrat()
	{
		return $this->numcontrat;
	}

	/**
	 * Set societe
	 *
	 * @param \Administration\SocieteBundle\Entity\PandoreSociete $societe
	 *
	 * @return PandoreContrat
	 */
	public function setSociete(\Administration\SocieteBundle\Entity\PandoreSociete $societe = null)
	{
		$this->societe = $societe;
	
		return $this;
	}
	
	/**
	 * Get societe
	 *
	 * @return \Administration\SocieteBundle\Entity\PandoreSociete
	 */
	public function getSociete()
	{
		return $this->societe;
	}
	
    /**
     * Set etab
     *
     * @param string $etab
     *
     * @return PandoreContrat
     */
    public function setEtab($etab)
    {
        $this->etab = $etab;
    
        return $this;
    }

    /**
     * Get etab
     *
     * @return string
     */
    public function getEtab()
    {
        return $this->etab;
    }
    
    /**
     * Set horaire
     *
     * @param string $horaire
     *
     * @return Contrat
     */
    public function setHoraire($horaire)
    {
    	$this->horaire = $horaire;
    
    	return $this;
    }
    
    /**
     * Get horaire
     *
     * @return string
     */
    public function getHoraire()
    {
    	return $this->horaire;
    }


    /**
     * Set categSociopro
     *
     * @param string $categSociopro
     *
     * @return Contrat
     */
    public function setCategSociopro($categSociopro)
    {
    	$this->categSociopro = $categSociopro;
    
    	return $this;
    }
    
    /**
     * Get categSociopro
     *
     * @return string
     */
    public function getCategSociopro()
    {
    	return $this->categSociopro;
    }
    

    /**
     * Set remuneration
     *
     * @param string $remuneration
     *
     * @return Contrat
     */
    public function setRemuneration($remuneration)
    {
    	$this->remuneration = $remuneration;
    
    	return $this;
    }
    
    /**
     * Get remuneration
     *
     * @return string
     */
    public function getRemuneration()
    {
    	return $this->remuneration;
    }
    
    /**
     * Set poste
     *
     * @param string $poste
     *
     * @return Contrat
     */
    public function setPoste($poste)
    {
    	$this->poste = $poste;
    
    	return $this;
    }
    
    /**
     * Get poste
     *
     * @return string
     */
    public function getPoste()
    {
    	return $this->poste;
    }
    
    /**
     * Set service
     *
     * @param string $service
     *
     * @return Contrat
     */
    public function setService($service)
    {
    	$this->service = $service;
    
    	return $this;
    }
    
    /**
     * Get service
     *
     * @return string
     */
    public function getService()
    {
    	return $this->service;
    }
    
    /**
     * Set niveau
     *
     * @param string $niveau
     *
     * @return Contrat
     */
    public function setNiveau($niveau)
    {
    	$this->niveau = $niveau;
    
    	return $this;
    }
    
    /**
     * Get niveau
     *
     * @return string
     */
    public function getNiveau()
    {
    	return $this->niveau;
    }
    
    /**
     * Set coef
     *
     * @param string $coef
     *
     * @return Contrat
     */
    public function setCoef($coef)
    {
    	$this->coef = $coef;
    
    	return $this;
    }
    
    /**
     * Get coef
     *
     * @return string
     */
    public function getCoef()
    {
    	return $this->coef;
    }
    
    /**
     * Set dureePe
     *
     * @param string $dureePe
     *
     * @return Contrat
     */
    public function setDureePe($dureePe)
    {
    	$this->dureePe = $dureePe;
    
    	return $this;
    }
    
    /**
     * Get dureePe
     *
     * @return string
     */
    public function getDureePe()
    {
    	return $this->dureePe;
    }
    
    /**
     * Set absence
     *
     * @param string $absence
     *
     * @return Contrat
     */
    public function setAbsence($absence)
    {
    	$this->absence = $absence;
    
    	return $this;
    }
    
    /**
     * Get absence
     *
     * @return string
     */
    public function getAbsence()
    {
    	return $this->absence;
    }
    
    /**
     * Set dateAnciennete
     *
     * @param string $dateAnciennete
     *
     * @return Contrat
     */
    public function setDateAnciennete($dateAnciennete)
    {
    	$this->dateAnciennete = $dateAnciennete;
    
    	return $this;
    }
    
    /**
     * Get dateAnciennete
     *
     * @return string
     */
    public function getDateAnciennete()
    {
    	return $this->dateAnciennete;
    }

    /**
     * Set dateEntree
     *
     * @param string $dateEntree
     *
     * @return Contrat
     */
    public function setDateEntree($dateEntree)
    {
    	$this->dateEntree = $dateEntree;
    
    	return $this;
    }
    
    /**
     * Get dateEntree
     *
     * @return string
     */
    public function getDateEntree()
    {
    	return $this->dateEntree;
    }
    
    /**
     * Set dateSortie
     *
     * @param $dateSortie
     *
     * @return PandoreContrat
     */
    public function setDateSortie($dateSortie)
    {
        $this->dateSortie = $dateSortie;
    
        return $this;
    }

    /**
     * Get dateSortie
     *
     * @return \DateTime
     */
    public function getDateSortie()
    {
        return $this->dateSortie;
    }

    /**
     * Set dateFincont
     *
     * @param $dateFincont
     *
     * @return PandoreContrat
     */
    public function setDateFincont($dateFincont)
    {
        $this->date_fincont = $dateFincont;
    
        return $this;
    }

    /**
     * Get dateFincont
     *
     * @return \DateTime
     */
    public function getDateFincont()
    {
    	return $this->date_fincont;
    }
    
    /**
     * Get typeContrat
     *
     * @return string
     */
    public function getTypeContrat()
    {
    	return $this->typeContrat;
    }
    
    /**
     * Set typeContrat
     *
     * @param $typeContrat
     *
     * @return PandoreContrat
     */
    public function setTypeContrat($typeContrat)
    {
    	$this->typeContrat = $typeContrat;
    
    	return $this;
    }
    
    /**
     * Set dateCreation
     *
     * @param string $dateCreation
     *
     * @return Contrat
     */
    public function setDateCreation($dateCreation)
    {
    	$this->dateCreation = $dateCreation;
    
    	return $this;
    }
    
    /**
     * Get dateCreation
     *
     * @return string
     */
    public function getDateCreation()
    {
    	return $this->dateCreation;
    }

    
    /**
     * Get dateCreation
     *
     * @return string
     */
    public function __toString()
    {
    	return $this->getUser()->getNom()  . ' ' . $this->getUser()->getPrenom() . ' - ' . $this->getUser()->getNumeroSecu() . ' - ' . $this->getSociete()->getId() . ' ' . $this->getSociete()->getRaison();;
    }	
	
}
