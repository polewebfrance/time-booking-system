<?php

namespace Administration\SocieteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AdmMagasin
 *
 * @ORM\Table(name="adm_magasin", indexes={@ORM\Index(name="fk_magasins_mag_societes_ste1_idx", columns={"ste_id"}), @ORM\Index(name="ste_id", columns={"ste_id"})})
 * @ORM\Entity
 */
class AdmMagasin
{
	
	/**
     * @var string
     *
     * @ORM\Column(name="mag_id", type="string", length=6)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $magId;
	
	
    /**
     * @var string
     *
     * @ORM\Column(name="mag_libelle", type="string", length=45, nullable=false)
     */
    private $magLibelle;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=30, nullable=true)
     */
    private $adresse;

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=30, nullable=true)
     */
    private $ville;

    /**
     * @var string
     *
     * @ORM\Column(name="code_postal", type="string", length=30, nullable=true)
     */
    private $codePostal;
    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=15, nullable=true)
     */
    private $telephone;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="mag_ouverture", type="date", nullable=false)
     */
    private $magOuverture;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="mag_fermeture", type="date", nullable=false)
     */
    private $magFermeture;

    /**
     * @var string
     *
     * @ORM\Column(name="ste_id", type="string", length=4, nullable=false)
     */
    private $societe;

    



    /**
     * Set magLibelle
     *
     * @param string $magLibelle
     *
     * @return AdmMagasin
     */
    public function setMagLibelle($magLibelle)
    {
        $this->magLibelle = $magLibelle;

        return $this;
    }

    /**
     * Get magLibelle
     *
     * @return string
     */
    public function getMagLibelle()
    {
        return $this->magLibelle;
    }

    /**
     * Set magOuverture
     *
     * @param \DateTime $magOuverture
     *
     * @return AdmMagasin
     */
    public function setMagOuverture($magOuverture)
    {
        $this->magOuverture = $magOuverture;

        return $this;
    }

    /**
     * Get magOuverture
     *
     * @return \DateTime
     */
    public function getMagOuverture()
    {
        return $this->magOuverture;
    }

    /**
     * Set magFermeture
     *
     * @param \DateTime $magFermeture
     *
     * @return AdmMagasin
     */
    public function setMagFermeture($magFermeture)
    {
        $this->magFermeture = $magFermeture;

        return $this;
    }

    /**
     * Get magFermeture
     *
     * @return \DateTime
     */
    public function getMagFermeture()
    {
        return $this->magFermeture;
    }

    /**
     * Set steId
     *
     * @param string $steId
     *
     * @return AdmMagasin
     */
    public function setSociete($steId)
    {
        $this->societe = $steId;

        return $this;
    }

    /**
     * Get steId
     *
     * @return string
     */
    public function getSociete()
    {
        return $this->societe;
    }

    /**
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * @param string $adresse
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;
    }

    /**
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @param string $telephone
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }




    /**
     * Get magId
     *
     * @return string
     */
    public function getMagId()
    {
        return $this->magId;
    }
}
