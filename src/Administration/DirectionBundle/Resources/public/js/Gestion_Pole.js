/**
 * Created by ons.allaya on 11/05/2017.
 */
function addPol() {
    var nv_pol = [];
    var pol_nom = $('#administration_directionbundle_pole_libelle').val();
    var pol_srv = $('#administration_directionbundle_pole_service').val();
    nv_pol.push(pol_nom);
    nv_pol.push(pol_srv);
    $.ajax({
        async: false,
        type: "POST",
        url: "../administration/ajoutpol",
        data: {nv_pol:nv_pol},
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (res) {
            var table = document.getElementById("dir_tab");
            var row = table.insertRow(table.length);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            var cell3 = row.insertCell(2);
            var cell4 = row.insertCell(3);
            cell1.innerHTML = res[0].id_pol;
            cell2.innerHTML = $('#administration_directionbundle_pole_libelle').val();
            $('#administration_directionbundle_pole_libelle').val('');
            cell3.innerHTML = res[0].srv;
            $('#administration_directionbundle_pole_service').val('');
            var checkbox = document.createElement('input');
            checkbox.type= 'checkbox';
            checkbox.id = res.id_pol;
            checkbox.checked = true;
            cell4.appendChild(checkbox);
        }
    });
}



//Initialisation de l'affichage
$(document).ready(function() {
    $('#administration_directionbundle_pole_ajouter').click(function(){
        addPol();
        $('#myModalAddPol').modal('hide');
    });



});
