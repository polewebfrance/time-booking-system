
function addDir() {
    var nv_dir = [];
    var dir_nom = $('#administration_directionbundle_direction_libelle').val();
    nv_dir.push(dir_nom);
    $.ajax({
        async: false,
        type: "POST",
        url: "../administration/ajoutdir",
        data: {nv_dir:nv_dir},
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (res) {
            var table = document.getElementById("dir_tab");
            var row = table.insertRow(table.length);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            var cell3 = row.insertCell(2);
            cell1.innerHTML = res;
            cell2.innerHTML = $('#administration_directionbundle_direction_libelle').val();
            $('#administration_directionbundle_direction_libelle').val('');
            var checkbox = document.createElement('input');
            checkbox.type= 'checkbox';
            checkbox.id = res;
            checkbox.checked = true;
            cell3.appendChild(checkbox);
        }
    });
}



    //Initialisation de l'affichage
    $(document).ready(function() {
        $('#administration_directionbundle_direction_ajouter').click(function(){
            addDir();
            $('#myModalAddDir').modal('hide');
        });



    });