/**
 * Created by ons.allaya on 11/05/2017.
 */

function addSrv() {
    var nv_srv = [];
    var srv_nom = $('#administration_directionbundle_service_libelle').val();
    var srv_dir = $('#administration_directionbundle_service_direction').val();
    nv_srv.push(srv_nom);
    nv_srv.push(srv_dir);
    $.ajax({
        async: false,
        type: "POST",
        url: "../administration/ajoutsrv",
        data: {nv_srv:nv_srv},
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (res) {

            var table = document.getElementById("dir_tab");
            var row = table.insertRow(table.length);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            var cell3 = row.insertCell(2);
            var cell4 = row.insertCell(3);
            cell1.innerHTML = res[0].id_srv;
            cell2.innerHTML = $('#administration_directionbundle_service_libelle').val();
            $('#administration_directionbundle_service_libelle').val('');
            cell3.innerHTML = res[0].dir;
            $('#administration_directionbundle_service_direction').val('');
            var checkbox = document.createElement('input');
            checkbox.type= 'checkbox';
            checkbox.id = res.id_srv;
            checkbox.checked = true;
            cell4.appendChild(checkbox);
        }
    });
}



//Initialisation de l'affichage
$(document).ready(function() {
    $('#administration_directionbundle_service_ajouter').click(function(){
        addSrv();
        $('#myModalAddSrv').modal('hide');
    });



});
