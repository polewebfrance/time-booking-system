<?php

namespace Administration\DirectionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Suivi\EtudesBundle\Entity\Demande;

/**
 * Direction
 *
 * @ORM\Table(name="adm_direction")
 * @ORM\Entity(repositoryClass="Administration\DirectionBundle\Entity\DirectionRepository")
 */
class Direction
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=255)
     */
    private $libelle;

    /**
     * @ORM\OneToMany(targetEntity="Administration\DirectionBundle\Entity\Service", mappedBy="direction")
     */
    private $services;

    /**
     * @var integer
     *
     * @ORM\Column(name="actif", type="integer")
     */
    private $actif;

    /**
     * @ORM\ManyToMany(targetEntity="Suivi\EtudesBundle\Entity\Demande", mappedBy="directions")
     *
     */
    public $demandes;


    /**
     * Add demande
     *
     * @param \Suivi\EtudesBundle\Entity\Demande $demande
     *
     * @return Demande
     */
    public function addDemandes(\Suivi\EtudesBundle\Entity\Demande $demande)
    {
        $this->demandes[] = $demande;

        return $this;
    }

    /**
     * Remove demande
     *
     * @param \Suivi\EtudesBundle\Entity\Demande $demande
     */
    public function removeDemandes(\Suivi\EtudesBundle\Entity\Demande $demande)
    {
        $this->demandes->removeElement($demande);
    }

    /**
     * @return mixed
     */
    public function getDemandes()
    {
        return $this->demandes;
    }







    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->services = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return Direction
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @return int
     */
    public function getActif()
    {
        return $this->actif;
    }

    /**
     * @param int $actif
     */
    public function setActif($actif)
    {
        $this->actif = $actif;
    }






    /**
     * Add services
     *
     * @param \Administration\DirectionBundle\Entity\Service $services
     * @return Direction
     */
    public function addService(\Administration\DirectionBundle\Entity\Service $services)
    {
        $this->services[] = $services;

        return $this;
    }

    /**
     * Remove services
     *
     * @param \Administration\DirectionBundle\Entity\Service $services
     */
    public function removeService(\Administration\DirectionBundle\Entity\Service $services)
    {
        $this->services->removeElement($services);
    }

    /**
     * Get services
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getServices()
    {
        return $this->services;
    }
    
    function __toString(){
    	return $this->getLibelle();
    }
}
