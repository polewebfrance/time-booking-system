<?php

namespace Administration\DirectionBundle\Controller;

use Administration\DirectionBundle\Entity\Service;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Administration\DirectionBundle\Form\ServiceType;
use Administration\DirectionBundle\Entity\Direction;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
class ServiceController extends Controller
{

    public function  listeservicesAction()
    {
        $form = $this->createForm(new ServiceType);
        $em = $this->getDoctrine()->getManager();
        $listeServices= $em->getRepository('AdministrationDirectionBundle:Service')->findAll();
        return $this->render('AdministrationDirectionBundle:Default:liste_services.html.twig', array(
            'listeServices' => $listeServices, 'form'=> $form->createView()
        ));
    }
    public function ajoutsrvAction(){
        $request = $this->getRequest();
        $nv_srv = $request->get('nv_srv');
        $srv = new Service();
        $srv->setLibelle($nv_srv[0]);
        $em = $this->getDoctrine()->getManager();
        $direction_srv= $em->getRepository('AdministrationDirectionBundle:Direction')->findOneBy(array('id'=>$nv_srv[1]));
        $srv->setDirection($direction_srv);
        $srv->setActif(1);
        $em = $this->getDoctrine()->getManager();
        $em->persist($srv);
        $em->flush();
        $res=array();
        $res[]=array("id_srv"=>$srv->getId(),"dir"=>$srv->getDirection()->getLibelle());
        $return = $res;
        $response = new Response();
        $data = json_encode($return);
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent($data);
        return $response;

    }
    public function activatesrvAction(){
        $form = $this->createForm(new ServiceType);
        $em = $this->getDoctrine()->getManager();
        $services= $em->getRepository('AdministrationDirectionBundle:Service')->findAll();
        foreach ($services as $service){
            $i=$service->getId();
            if(isset($_POST[$i])){
                $service->setActif(1);
            }
            else{ $service->setActif(0);}
            $em->persist($service);
            $em->flush();
        }
        return $this->render('AdministrationDirectionBundle:Default:liste_services.html.twig', array(
            'listeServices' => $services, 'form'=> $form->createView()
        ));
    }
}
