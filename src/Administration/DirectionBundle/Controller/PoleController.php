<?php

namespace Administration\DirectionBundle\Controller;

use Administration\DirectionBundle\Form\PoleType;
use SelectingPeopleBundle\Entity\SpPole;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;


class PoleController extends Controller
{
    public function  listepolesAction()
    {
        $form = $this->createForm(new PoleType());
        $em = $this->getDoctrine()->getManager();
        $listePoles= $em->getRepository('SelectingPeopleBundle:SpPole')->findAll();
        return $this->render('AdministrationDirectionBundle:Default:liste_poles.html.twig', array(
            'listepoles' => $listePoles, 'form'=> $form->createView()
        ));
    }
    public function ajoutpolAction(){
        $request = $this->getRequest();
        $nv_pol = $request->get('nv_pol');
        $pol = new SpPole();
        $pol->setPolLibelle($nv_pol[0]);
        $em = $this->getDoctrine()->getManager();
        $service_pol= $em->getRepository('AdministrationDirectionBundle:Service')->findOneBy(array('id'=>$nv_pol[1]));
        $pol->setSrv($service_pol);
        $pol->setActif(1);
        $em = $this->getDoctrine()->getManager();
        $em->persist($pol);
        $em->flush();
        $res=array();
        $res[]=array("id_pol"=>$pol->getPolId(),"srv"=>$pol->getSrv()->getLibelle());
        $return = $res;
        $response = new Response();
        $data = json_encode($return);
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent($data);
        return $response;
    }
    public function activatepolAction(){
        $form = $this->createForm(new PoleType);
        $em = $this->getDoctrine()->getManager();
        $poles= $em->getRepository('SelectingPeopleBundle:SpPole')->findAll();
        foreach ($poles as $pole){
            $i=$pole->getPolId();
            if(isset($_POST[$i])){
                $pole->setActif(1);
            }
            else{ $pole->setActif(0);}
            $em->persist($pole);
            $em->flush();
        }
        return $this->render('AdministrationDirectionBundle:Default:liste_poles.html.twig', array(
            'listepoles' => $poles, 'form'=> $form->createView()
        ));
    }
}