<?php

namespace Administration\DirectionBundle\Controller;
use Administration\DirectionBundle\Entity\Direction;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Administration\DirectionBundle\Form\DirectionType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
class DirectionController extends Controller
{

    public function  listedirectionsAction()
    {
    	$form = $this->createForm(new DirectionType);
        $em = $this->getDoctrine()->getManager();
        $listeDirections= $em->getRepository('AdministrationDirectionBundle:Direction')->findAll();

    	return $this->render('AdministrationDirectionBundle:Default:liste_directions.html.twig', array(
            'listeDirections' => $listeDirections, 'form'=> $form->createView()
    	));
    }

    public function ajoutdirAction(){
        $request = $this->getRequest();
        $nv_dir = $request->get('nv_dir');
        //if ($form->isSubmitted()) {
           // $donnee=$_POST;
            $dir = new Direction();
            $dir->setLibelle($nv_dir[0]);
            $dir->setActif(1);
            $em = $this->getDoctrine()->getManager();
            $em->persist($dir);
            $em->flush();
        $res=$dir->getId();
        $return = $res;
        $response = new Response();
        $data = json_encode($return);
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent($data);
        return $response;
       // }

    }

    public function activatedirAction(){
        $form = $this->createForm(new DirectionType);
        $em = $this->getDoctrine()->getManager();
        $directions= $em->getRepository('AdministrationDirectionBundle:Direction')->findAll();
        foreach ($directions as $direction){
            $i=$direction->getId();
            if(isset($_POST[$i])){
                $direction->setActif(1);
            }
            else{ $direction->setActif(0);}
            $em->persist($direction);
            $em->flush();
        }

        return $this->render('AdministrationDirectionBundle:Default:liste_directions.html.twig', array(
            'listeDirections' => $directions, 'form'=> $form->createView()
        ));

    }

}
