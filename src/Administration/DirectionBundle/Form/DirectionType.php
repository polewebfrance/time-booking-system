<?php

namespace Administration\DirectionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DirectionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('libelle','text', array('required'=> true,'attr'=>array('class'=>'form-control input-sm'),'label'=> 'Nom direction'))
            ->add('ajouter', 'button', array('attr' => array('class' => 'btn btn-default', 'value'=>'Ajouter')))
            ->add('actif','checkbox')
            ->add('enregistrer', 'submit', array('attr' => array('class' => 'btn btn-danger', 'value'=>'Enregistrer')))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Administration\DirectionBundle\Entity\Direction'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'administration_directionbundle_direction';
    }
}
