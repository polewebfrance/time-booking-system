<?php

namespace Administration\DirectionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ServiceType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('libelle','text', array('required'=> true,'attr'=>array('class'=>'form-control input-sm'),'label'=> 'Nom service'))
            ->add('actif','checkbox')
            ->add('direction', 'entity', array('label'=> 'Direction',
                'class' => 'AdministrationDirectionBundle:Direction',
                'property' => 'libelle',
                'empty_value' => '',
                'required' => false,'attr'=>array('class'=>'form-control input-sm'),
            ))
            ->add('ajouter', 'button', array('attr' => array('class' => 'btn btn-default', 'value'=>'Ajouter')))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Administration\DirectionBundle\Entity\Service'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'administration_directionbundle_service';
    }
}
