<?php

namespace Administration\HierarchieBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Hierarchie
 *
 * @ORM\Table(name="hierarchie.hierarchies_hie")
 * @ORM\Entity(repositoryClass="Administration\HierarchieBundle\Entity\HierarchieRepository")
 */
class Hierarchie
{
    /**
     * @var integer
     *
     * @ORM\Column(name="hie_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $hieId;

    /**
     * @var string
     *
     * @ORM\Column(name="hie_libelle", type="string", length=45, nullable=false)
     */
    private $hieLibelle;

    /**
     * @var string
     *
     * @ORM\Column(name="hie_description", type="string", length=100, nullable=true)
     */
    private $hieDescription;



    /**
     * Get hieId
     *
     * @return integer 
     */
    public function getHieId()
    {
        return $this->hieId;
    }

    /**
     * Set hieLibelle
     *
     * @param string $hieLibelle
     * @return Hierarchie
     */
    public function setHieLibelle($hieLibelle)
    {
        $this->hieLibelle = $hieLibelle;

        return $this;
    }

    /**
     * Get hieLibelle
     *
     * @return string 
     */
    public function getHieLibelle()
    {
        return $this->hieLibelle;
    }

    /**
     * Set hieDescription
     *
     * @param string $hieDescription
     * @return Hierarchie
     */
    public function setHieDescription($hieDescription)
    {
        $this->hieDescription = $hieDescription;

        return $this;
    }

    /**
     * Get hieDescription
     *
     * @return string 
     */
    public function getHieDescription()
    {
        return $this->hieDescription;
    }
}
