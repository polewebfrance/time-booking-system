<?php

namespace Administration\HierarchieBundle\Entity;

use Doctrine\ORM\EntityRepository;

class HierarchieRepository extends EntityRepository {

    public function getGroupsForHierarchy($hieId) {
        $data = $this->createQueryBuilder('hie')
                        ->select('grp.grpLibelle', 'gpfg.gpfIdfils', 'gpfg.gpfIdpere')
                        ->leftJoin('AdministrationHierarchieBundle:GroupePereFils', 'gpfg', 'WITH', 'gpfg.hieId = hie.hieId')
                        ->leftJoin('AdministrationHierarchieBundle:Groupe', 'grp', 'WITH', 'grp.grpId = gpfg.gpfIdfils')
                        ->where('gpfg.hieId = ?1')
                        ->orderBy('grp.grpLibelle', 'ASC')
                        ->setParameter(1, $hieId)
                        ->getQuery()->getResult();

        return $data;
    }

    public function buildTree($hieId) {
        $data = $this->createQueryBuilder('hie')
                        ->select('hie.hieLibelle as hierarchy', 'gpfg.gpfIdpere as parent', 'gpfg.gpfIdfils as child', 'grp.grpLibelle as groupName', 'rol.rolId', 'rol.rolLibelle as userType', 'usr.usrDateSortie as gone')
                        ->addSelect("CONCAT(usr.usrPrenom, ' ', usr.usrNom) as userName")
                        ->leftJoin('AdministrationHierarchieBundle:GroupePereFils', 'gpfg', 'WITH', 'gpfg.hieId = hie.hieId')
                        ->leftJoin('AdministrationHierarchieBundle:Groupe', 'grp', 'WITH', 'grp.grpId = gpfg.gpfIdfils')
                        ->leftJoin('AdministrationHierarchieBundle:UsrRoleGroupe', 'urg', 'WITH', 'urg.grpId = gpfg.gpfIdfils AND(urg.hieId = ' . $hieId . ')')
                        ->leftJoin('AdministrationHierarchieBundle:Role', 'rol', 'WITH', 'rol.rolId = urg.rolId')
                        ->leftJoin('AdministrationHierarchieBundle:Utilisateur', 'usr', 'WITH', 'usr.usrMatricule = urg.usrMatricule')
                        ->where('gpfg.hieId = ' . $hieId)
                        ->andWhere('gpfg.gpfIdfils != 100')
                        ->orderBy('grp.grpLibelle', 'ASC')
                        ->addOrderBy('rol.rolId', 'ASC')
                        ->addOrderBy('usr.usrPrenom', 'ASC')
                        ->getQuery()->getResult();
        return $data;
    }

    public function getGroupsNotInHierarchy($hieId) {
        $data = $this->createQueryBuilder('hie')
                        ->select('gpfg')
                        ->from('AdministrationHierarchieBundle:GroupePereFils', 'gpfg')
                        ->where('gpfg.hieId = ' . $hieId)
                        ->getQuery()->getResult();
        $idList = '';
        foreach ($data as $row) {
            $idList .= $row->getGpfIdfils() . ',';
        }
        $list = $this->createQueryBuilder('hie')
                        ->select('groupes.grpId', 'groupes.grpLibelle')
                        ->from('AdministrationHierarchieBundle:Groupe', 'groupes')
                        ->where('groupes.grpId not in ( ' . rtrim($idList, ",") . ')')
                        ->groupBy('groupes.grpId')
                        ->orderBy('groupes.grpLibelle', 'ASC')
                        ->getQuery()->getResult();
        return $list;
    }

    public function getHieForGroupAddGroup($grpId) {
        $data = $this->createQueryBuilder('hie')
                        ->select('gpfg')
                        ->from('AdministrationHierarchieBundle:GroupePereFils', 'gpfg')
                        ->where('gpfg.gpfIdfils = ' . $grpId)
                        ->getQuery()->getResult();

        $list = array();
        if (!empty($data)) {
            $idList = "";
            foreach ($data as $row) {
                $idList .= $row->getHieId() . ',';
            }
            $hies = $this->createQueryBuilder('hie')
                            ->where('hie.hieId not in ( ' . rtrim($idList, ",") . ')')
                            ->orderBy('hie.hieLibelle', 'ASC')
                            ->getQuery()->getResult();
        } else {
            $hies = $this->createQueryBuilder('hie')
                            ->orderBy('hie.hieLibelle', 'ASC')
                            ->getQuery()->getResult();
        }
        if (!empty($hies)) {
            foreach ($hies as $row) {
                $list[$row->getHieId()] = $row->getHieLibelle();
            }
        }
        return $list;
    }

    public function getGroupPereDropdown($hieId, $grpId) {
        $data = $this->createQueryBuilder('hie')
                        ->select('grp.grpLibelle', 'grp.grpId')
                        ->leftJoin('AdministrationHierarchieBundle:GroupePereFils', 'gpfg', 'WITH', 'gpfg.hieId = hie.hieId')
                        ->leftJoin('AdministrationHierarchieBundle:Groupe', 'grp', 'WITH', 'grp.grpId = gpfg.gpfIdfils')
                        ->where('gpfg.hieId = ?1')
                        ->andWhere('gpfg.gpfIdfils != ?2')
                        ->orderBy('grp.grpLibelle', 'ASC')
                        ->setParameters(array(1 => $hieId, 2 => $grpId))
                        ->getQuery()->getResult();
        $list = "";
        if (!empty($data)) {
            foreach ($data as $row) {
                $list[$row['grpLibelle']] = $row['grpId'];
            }
        }
        return $list;
    }

    public function getHieForGroupModifyGroup($grpId) {
        $data = $this->createQueryBuilder('hie')
                        ->select('gpfg')
                        ->innerJoin('AdministrationHierarchieBundle:GroupePereFils', 'gpfg')
                        ->where('gpfg.gpfIdfils = ' . $grpId)
                        ->getQuery()->getResult();

        $list = array();
        if (!empty($data)) {
            $idList = "";
            foreach ($data as $row) {
                $idList .= $row->getHieId() . ',';
            }
            $hies = $this->createQueryBuilder('hie')
                            ->where('hie.hieId in ( ' . rtrim($idList, ",") . ')')
                            ->getQuery()->getResult();
        }
        if (!empty($hies)) {
            foreach ($hies as $row) {
                $list[$row->getHieId()] = $row->getHieLibelle();
            }
        }
        return $list;
    }

    public function getGroupParent($hieId, $grpId) {
        $data = $this->createQueryBuilder('hie')
                        ->select('gpfg.gpfIdpere')
                        ->leftJoin('AdministrationHierarchieBundle:GroupePereFils', 'gpfg', 'WITH', 'gpfg.hieId = hie.hieId')
                        ->where('gpfg.hieId = ?1')
                        ->andWhere('gpfg.gpfIdfils = ?2')
                        ->setParameters(array(1 => $hieId, 2 => $grpId))
                        ->getQuery()->getResult();
        $list = "---";
        if (!empty($data)) {
            $row = $this->createQueryBuilder('hie')
                            ->select('grp')
                            ->from('AdministrationHierarchieBundle:Groupe', 'grp')
                            ->where('grp.grpId = ?1')
                            ->setParameter(1, $data[0]['gpfIdpere'])
                            ->getQuery()->getResult();
        }
        if (!empty($row)) {
            foreach ($row as $row) {
                $list = $row->getGrpLibelle();
            }
        }
        return $list;
    }

    public function findHierarchies($serial) {
        $data = $this->createQueryBuilder('hie')
                        ->select('hie.hieId', 'hie.hieLibelle')
                        ->orderBy('hie.hieLibelle', 'ASC')
                        ->getQuery()->getResult();
        return $data;
    }

    public function getHierachiesWhereUserIn($serial) {
        $data = $this->createQueryBuilder('hie')
                        ->select('hie.hieId', 'hie.hieLibelle')
                        ->leftJoin('AdministrationHierarchieBundle:UsrRoleGroupe', 'urg', 'WITH', 'urg.hieId = hie.hieId AND urg.usrMatricule = :serial')
                        ->setParameter('serial', $serial)
                        ->where('urg.hieId IS NOT NULL')
                        ->groupBy('urg.hieId')
                        ->getQuery()->getResult();
        return $data;
    }

    public function getUsrRoleGroup($serial, $hieId) {
        $data = $this->createQueryBuilder('hie')
                        ->select('grp.grpLibelle', 'rol.rolLibelle', 'grp.grpId', 'rol.rolId')
                        ->leftJoin('AdministrationHierarchieBundle:UsrRoleGroupe', 'urg', 'WITH', 'urg.hieId = hie.hieId AND urg.usrMatricule = ' . "'".$serial."'")
                        ->leftJoin('AdministrationHierarchieBundle:Groupe', 'grp', 'WITH', 'grp.grpId = urg.grpId')
                        ->leftJoin('AdministrationHierarchieBundle:Role', 'rol', 'WITH', 'rol.rolId = urg.rolId')
                        ->where('hie.hieId = ?1')
                        ->setParameter(1, $hieId)
                        ->getQuery()->getSingleResult();
        return $data;
    }

}
