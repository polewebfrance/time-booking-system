<?php

namespace Administration\HierarchieBundle\Entity;

use Doctrine\ORM\EntityRepository;

class UtilisateurRepository extends EntityRepository {

    public function findAllUsers() {
        $usersObj = $this->createQueryBuilder('usr')
                        ->select('usr.usrNom', 'usr.usrPrenom', 'usr.usrMatricule', 'usr.usrDateSortie', 'urg.usrMatricule urgIn')
                        ->leftJoin('AdministrationHierarchieBundle:UsrRoleGroupe', 'urg', 'WITH', 'usr.usrMatricule = urg.usrMatricule')
                        ->leftJoin('AdministrationHierarchieBundle:Utilisateur', 'usr1', 'WITH', 'usr1.usrMatricule = usr.usrMatricule ')
                        ->orderBy('usr.usrNom', 'ASC')
                        ->getQuery()->getResult();
        $users = array();
        if (!empty($usersObj)) {
            foreach ($usersObj as $row) {
                $users[$row['usrMatricule']] = $row['usrNom'] . ' ' . $row['usrPrenom'];
                // remove user if not working any more
                if (!empty($row['usrDateSortie']) && $row['usrDateSortie'] < date('Ymd') && empty($row['urgIn'])) {
                    unset($users[$row['usrMatricule']]);
                }
            }
        }
        return $users;
    }

    public function findAllActiveUsers() {
        $usersObj = $this->createQueryBuilder('usr')
                        ->select('usr.usrNom', 'usr.usrPrenom', 'usr.usrMatricule')
                        ->where('usr.usrDateEntree != \'\'')
                        ->andWhere('( usr.usrDateSortie = \'\' OR usr.usrDateSortie >= '.date('Ymd').' )')
                        ->orderBy('usr.usrNom', 'ASC')
                        ->getQuery()->getResult();
        $users = array();
        if (!empty($usersObj)) {
            foreach ($usersObj as $row) {
                $users[$row['usrMatricule']] = $row['usrNom'] . ' ' . $row['usrPrenom'];
            }
        }
        return $users;
    }
    
    public function findUsers() {
        $usersObj = $this->createQueryBuilder('usr')
                        ->select('usr.usrNom', 'usr.usrPrenom', 'usr.usrMatricule')
                        ->orderBy('usr.usrNom', 'ASC')
                        ->getQuery()->getResult();
        $users = array();
        if (!empty($usersObj)) {
            foreach ($usersObj as $row) {
                $users[$row['usrMatricule']] = $row['usrNom'] . ' ' . $row['usrPrenom'];
            }
        }
        return $users;
    }

    public function countCustomMatricule() {
        $total = $this->createQueryBuilder('usr')
                        ->select('COUNT(usr.usrMatricule) no')
                        ->where("usr.usrMatricule LIKE '%u%'")
                        ->getQuery()->getSingleResult();
        /* create serials */
        // get total custom serials + 1
        $no = $total['no'] + 1;
        // a serial has 6 chars so minus control letter "u" remain 5
        $chars = 5;
        // construct serial
        $matricule = 'u' . str_repeat(0, $chars - strlen($no)) . $no;
        return $matricule;
    }

}
