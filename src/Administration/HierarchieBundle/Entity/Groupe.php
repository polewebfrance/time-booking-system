<?php

namespace Administration\HierarchieBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Groupe
 *
 * @ORM\Table(name="hierarchie.groupes_grp", indexes={@ORM\Index(name="grp_id", columns={"grp_id"}), @ORM\Index(name="grp_libelle", columns={"grp_libelle"})})
 * @ORM\Entity
 */
class Groupe
{
    /**
     * @var integer
     *
     * @ORM\Column(name="grp_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $grpId;

    /**
     * @var string
     *
     * @ORM\Column(name="grp_libelle", type="string", length=100, nullable=false)
     */
    private $grpLibelle;

    /**
     * @var string
     *
     * @ORM\Column(name="grp_description", type="string", length=100, nullable=true)
     */
    private $grpDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="grp_permalink", type="string", length=100, nullable=true)
     */
    private $grpPermalink;



    /**
     * Get grpId
     *
     * @return integer 
     */
    public function getGrpId()
    {
        return $this->grpId;
    }

    /**
     * Set grpLibelle
     *
     * @param string $grpLibelle
     * @return Groupe
     */
    public function setGrpLibelle($grpLibelle)
    {
        $this->grpLibelle = $grpLibelle;

        return $this;
    }

    /**
     * Get grpLibelle
     *
     * @return string 
     */
    public function getGrpLibelle()
    {
        return $this->grpLibelle;
    }

    /**
     * Set grpDescription
     *
     * @param string $grpDescription
     * @return Groupe
     */
    public function setGrpDescription($grpDescription)
    {
        $this->grpDescription = $grpDescription;

        return $this;
    }

    /**
     * Get grpDescription
     *
     * @return string 
     */
    public function getGrpDescription()
    {
        return $this->grpDescription;
    }

    /**
     * Set grpPermalink
     *
     * @param string $grpPermalink
     * @return Groupe
     */
    public function setGrpPermalink($grpPermalink)
    {
        $this->grpPermalink = $grpPermalink;

        return $this;
    }

    /**
     * Get grpPermalink
     *
     * @return string 
     */
    public function getGrpPermalink()
    {
        return $this->grpPermalink;
    }
}
