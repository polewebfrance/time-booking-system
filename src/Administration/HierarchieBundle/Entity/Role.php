<?php

namespace Administration\HierarchieBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Role
 *
 * @ORM\Table(name="hierarchie.roles_rol")
 * @ORM\Entity
 */
class Role
{
    /**
     * @var integer
     *
     * @ORM\Column(name="rol_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $rolId;

    /**
     * @var string
     *
     * @ORM\Column(name="rol_libelle", type="string", length=45, nullable=false)
     */
    private $rolLibelle;



    /**
     * Get rolId
     *
     * @return integer 
     */
    public function getRolId()
    {
        return $this->rolId;
    }

    /**
     * Set rolLibelle
     *
     * @param string $rolLibelle
     * @return Role
     */
    public function setRolLibelle($rolLibelle)
    {
        $this->rolLibelle = $rolLibelle;

        return $this;
    }

    /**
     * Get rolLibelle
     *
     * @return string 
     */
    public function getRolLibelle()
    {
        return $this->rolLibelle;
    }
}
