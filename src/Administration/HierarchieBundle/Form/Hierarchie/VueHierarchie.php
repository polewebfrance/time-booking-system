<?php

namespace Administration\HierarchieBundle\Form\Hierarchie;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class VueHierarchie extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('hieLibelle', 'entity', array(
                    'class' => 'AdministrationHierarchieBundle:Hierarchie',
                    'property' => 'hieLibelle',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('table')
                                ->orderBy('table.hieLibelle', 'ASC');
                    },
                    'empty_value' => 'Choisissez une hiérarchie',
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Administration\HierarchieBundle\Entity\Hierarchie',
        ));
    }

    public function getName() {
        return 'hierarchy';
    }

}
