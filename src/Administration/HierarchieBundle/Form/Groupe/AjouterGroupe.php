<?php

namespace Administration\HierarchieBundle\Form\Groupe;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class AjouterGroupe extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add(
            'grpLibelle', 'text', array(
                'label' => 'Libellé*:',
                'required' => true,
                'constraints' => array(
                new NotBlank(array(
                    'message' => 'Libellé ne doit pas être vide.'
                        )),
        )));
        $builder->add(
            'grpDescription', 'text', array(
                'label' => 'Description courte:',
                'required' => false,
        ));
        $builder->add('Ajouter', 'submit');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Administration\HierarchieBundle\Entity\Groupe',
        ));
    }

    public function getName() {
        return '';
    }

}
