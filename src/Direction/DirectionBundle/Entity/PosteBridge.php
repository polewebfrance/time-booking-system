<?php

namespace Direction\DirectionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PosteBridge
 *
 * @ORM\Table(name="poste_bridge", indexes={@ORM\Index(name="fk_poste_has_poste_pst_id", columns={"pst_id"}), @ORM\Index(name="fk_poste_has_direction_dir_id", columns={"dir_id"}), @ORM\Index(name="fk_poste_has_service_srv_id", columns={"srv_id"}), @ORM\Index(name="fk_poste_has_pole_pol_id", columns={"pol_id"})})
 * @ORM\Entity
 */
class PosteBridge
{
    /**
     * @var integer
     *
     * @ORM\Column(name="bdg_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $bdgId;

    /**
     * @var \DirectionDir
     *
     * @ORM\ManyToOne(targetEntity="DirectionDir")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="dir_id", referencedColumnName="dir_id")
     * })
     */
    private $dir;

    /**
     * @var \PolePol
     *
     * @ORM\ManyToOne(targetEntity="PolePol")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pol_id", referencedColumnName="pol_id")
     * })
     */
    private $pol;

    /**
     * @var \PostePst
     *
     * @ORM\ManyToOne(targetEntity="PostePst")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pst_id", referencedColumnName="pst_id")
     * })
     */
    private $pst;

    /**
     * @var \ServiceSrv
     *
     * @ORM\ManyToOne(targetEntity="ServiceSrv")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="srv_id", referencedColumnName="srv_id")
     * })
     */
    private $srv;



    /**
     * Get bdgId
     *
     * @return integer 
     */
    public function getBdgId()
    {
        return $this->bdgId;
    }

    /**
     * Set dir
     *
     * @param \Direction\DirectionBundle\Entity\DirectionDir $dir
     * @return PosteBridge
     */
    public function setDir(\Direction\DirectionBundle\Entity\DirectionDir $dir = null)
    {
        $this->dir = $dir;
    
        return $this;
    }

    /**
     * Get dir
     *
     * @return \Direction\DirectionBundle\Entity\DirectionDir 
     */
    public function getDir()
    {
        return $this->dir;
    }

    /**
     * Set pol
     *
     * @param \Direction\DirectionBundle\Entity\PolePol $pol
     * @return PosteBridge
     */
    public function setPol(\Direction\DirectionBundle\Entity\PolePol $pol = null)
    {
        $this->pol = $pol;
    
        return $this;
    }

    /**
     * Get pol
     *
     * @return \Direction\DirectionBundle\Entity\PolePol 
     */
    public function getPol()
    {
        return $this->pol;
    }

    /**
     * Set pst
     *
     * @param \Direction\DirectionBundle\Entity\PostePst $pst
     * @return PosteBridge
     */
    public function setPst(\Direction\DirectionBundle\Entity\PostePst $pst = null)
    {
        $this->pst = $pst;
    
        return $this;
    }

    /**
     * Get pst
     *
     * @return \Direction\DirectionBundle\Entity\PostePst 
     */
    public function getPst()
    {
        return $this->pst;
    }

    /**
     * Set srv
     *
     * @param \Direction\DirectionBundle\Entity\ServiceSrv $srv
     * @return PosteBridge
     */
    public function setSrv(\Direction\DirectionBundle\Entity\ServiceSrv $srv = null)
    {
        $this->srv = $srv;
    
        return $this;
    }

    /**
     * Get srv
     *
     * @return \Direction\DirectionBundle\Entity\ServiceSrv 
     */
    public function getSrv()
    {
        return $this->srv;
    }
}
