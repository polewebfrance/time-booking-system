<?php

namespace Direction\DirectionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PostePst
 *
 * @ORM\Table(name="poste_pst")
 * @ORM\Entity
 */
class PostePst
{
    /**
     * @var integer
     *
     * @ORM\Column(name="pst_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $pstId;

    /**
     * @var string
     *
     * @ORM\Column(name="pst_libelle", type="string", length=45, nullable=false)
     */
    private $pstLibelle;

    /**
     * @var integer
     *
     * @ORM\Column(name="pst_salaire", type="integer", nullable=true)
     */
    private $pstSalaire;

    /**
     * @var integer
     *
     * @ORM\Column(name="pst_duree_pe", type="integer", nullable=false)
     */
    private $pstDureePe;



    /**
     * Get pstId
     *
     * @return integer 
     */
    public function getPstId()
    {
        return $this->pstId;
    }

    /**
     * Set pstLibelle
     *
     * @param string $pstLibelle
     * @return PostePst
     */
    public function setPstLibelle($pstLibelle)
    {
        $this->pstLibelle = $pstLibelle;
    
        return $this;
    }

    /**
     * Get pstLibelle
     *
     * @return string 
     */
    public function getPstLibelle()
    {
        return $this->pstLibelle;
    }

    /**
     * Set pstSalaire
     *
     * @param integer $pstSalaire
     * @return PostePst
     */
    public function setPstSalaire($pstSalaire)
    {
        $this->pstSalaire = $pstSalaire;
    
        return $this;
    }

    /**
     * Get pstSalaire
     *
     * @return integer 
     */
    public function getPstSalaire()
    {
        return $this->pstSalaire;
    }

    /**
     * Set pstDureePe
     *
     * @param integer $pstDureePe
     * @return PostePst
     */
    public function setPstDureePe($pstDureePe)
    {
        $this->pstDureePe = $pstDureePe;
    
        return $this;
    }

    /**
     * Get pstDureePe
     *
     * @return integer 
     */
    public function getPstDureePe()
    {
        return $this->pstDureePe;
    }
}
