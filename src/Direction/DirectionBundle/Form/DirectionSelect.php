<?php

namespace Suivi\EtudesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DirectionSelect extends AbstractType{
	
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('Direction', 'entity',array(
                'class'    => 'Direction\DirectionBundle:DirectionDir',
                'property' => 'name',
                'empty_value' => '',
				));
	}
}